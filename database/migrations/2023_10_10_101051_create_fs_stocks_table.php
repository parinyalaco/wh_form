<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFsStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_stocks', function (Blueprint $table) {
            $table->id();
            $table->integer('fs_wh_loc_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->integer('volumn')->nullable();
            $table->string('pallet', 50)->nullable();
            $table->text('desc')->nullable();
            $table->string('status', 50)->default('Action');
            $table->integer('stock_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->date('to_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fs_stocks');
    }
}
