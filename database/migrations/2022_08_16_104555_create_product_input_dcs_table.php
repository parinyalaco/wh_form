<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductInputDcsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_input_dcs', function (Blueprint $table) {
            $table->id();
            $table->integer('to_importfile_id'); 
            $table->date('input_date');
            $table->foreignId('material_id')->constrained(); 
            $table->foreignId('vendor_id')->constrained(); 
            $table->integer('po_quantity')->nullable(); 
            $table->integer('input_real')->nullable(); 
            $table->integer('input_list')->nullable();         
            $table->longText('note')->nullable(); 
            $table->string('active', 1)->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_input_dcs');
    }
}
