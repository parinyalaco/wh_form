<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRmSapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rm_saps', function (Blueprint $table) {
            $table->id();
            $table->foreignId('file_excel_id')->constrained();
            $table->date('sap_date');
            $table->foreignId('raw_material_id')->constrained(); 
            $table->integer('sap_no');
            $table->string('car_name',20);
            $table->foreignId('rm_farmer_id')->constrained(); 
            $table->foreignId('unit_id')->constrained();    //ใช้กรณีชื่อ field เป็นชื่อตารางต่อด้วย id เลยไม่ต้องระบุตารางที่ link หา
            // $table->unsignedBigInteger('unit_id');   //2 บรรทัดนี้ใช้กรณีชื่อ field ไม่สื่อถึงชื่อตาราง
		    // $table->foreign('unit_id')->references('id')->on('units');
            $table->integer('unit_num');
            $table->float('rm_weight');
            $table->float('rm_weight_agv');
            $table->float('rm_price');
            $table->foreignId('rm_type_car_id')->constrained(); 
            $table->time('time_in'); 
            $table->float('rm_weight_sum');
            $table->text('remark');
            $table->string('customer',100);
            $table->float('rm_weight_first');
            $table->float('rm_weight_last');
            $table->string('weight_type',10);
            $table->time('weight_time_in');
            $table->time('weight_time_out');
            $table->string('requester',10);
            $table->string('change_by',10);
            $table->date('change_date');
            $table->time('change_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rm_saps');
    }
}
