<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFsWhTranDsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_wh_tran_ds', function (Blueprint $table) {
            $table->id();
            $table->integer('fs_wh_tran_m_id')->nullable();
            $table->integer('fs_wh_loc_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->integer('volumn')->nullable();
            $table->string('pallet', 50)->nullable();
            $table->string('desc')->nullable();
            $table->string('status',50)->default('Action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fs_wh_tran_ds');
    }
}
