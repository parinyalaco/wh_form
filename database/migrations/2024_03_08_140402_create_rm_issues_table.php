<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRmIssuesTable extends Migration
{

    public function up()
    {
        Schema::create('rm_issues', function (Blueprint $table) {
            $table->id();
            $table->string('sap_no')->nullable();
            $table->integer('rm_manual_id')->nullable();
            $table->string('detail')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('rm_issues');
    }
}
