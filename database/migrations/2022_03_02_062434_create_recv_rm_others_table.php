<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecvRmOthersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recv_rm_others', function (Blueprint $table) {
            $table->id();
            $table->date('act_date');
            $table->integer('seq');
            $table->string('poduct_code', 50);
            $table->string('license_no',50);
            $table->string('location', 100)->nullable();
            $table->integer('car_no')->nullable();
            $table->integer('qty')->nullable();
            $table->integer('fail_qty')->nullable();
            $table->float('weight')->nullable();
            $table->datetime('left_time')->nullable();
            $table->datetime('arv_laco_time')->nullable();
            $table->datetime('start_move_time')->nullable();
            $table->datetime('end_move_time')->nullable();
            $table->integer('staff_no')->nullable();
            $table->string('car_type', 100)->nullable();
            $table->string('move_type', 100)->nullable();
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recv_rm_others');
    }
}
