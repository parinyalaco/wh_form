<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpdateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // ไว้สำหรับ lose ที่มาจาก stock (wait/pass)
        Schema::table(
            'loses',
            function (Blueprint $table) {
                $table->string('lose_stock')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('loses', 'lose_stock')) {
            Schema::table('loses', function (Blueprint $table) {
                $table->dropColumn('lose_stock');
            });
        }
    }
}
