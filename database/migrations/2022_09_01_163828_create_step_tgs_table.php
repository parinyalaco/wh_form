<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStepTgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('step_tgs', function (Blueprint $table) {
            $table->id();              
            $table->string('name');
            $table->string('refer_step');   //เก็บว่าอ้างถึงไปที่ step ไหนบ้าง เพื่อการดึงจำนวนมาเป็น %เป้า
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('step_tgs');
    }
}
