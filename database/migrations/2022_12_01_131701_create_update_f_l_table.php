<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpdateFLTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'f_logs',
            function (Blueprint $table) {
                $table->foreignId('fl_area_id')->nullable()->constrained('fl_areas');
                $table->foreignId('fl_broken_id')->nullable()->constrained('fl_brokens');
                $table->string('head_img')->nullable();
                $table->string('head_img_path')->nullable();
                $table->string('left_img')->nullable();
                $table->string('left_img_path')->nullable();
                $table->string('right_img')->nullable();
                $table->string('right_img_path')->nullable();
                $table->string('back_img')->nullable();
                $table->string('back_img_path')->nullable();
                $table->string('batt_img')->nullable();
                $table->string('batt_img_path')->nullable();
                $table->string('monitor_img')->nullable();
                $table->string('monitor_img_path')->nullable();
                $table->foreignId('user_id')->nullable()->constrained('users');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('f_logs', 'fl_area_id')) {
            Schema::table('f_logs', function (Blueprint $table) {
                $table->dropForeign(['fl_area_id']);
                $table->dropColumn('fl_area_id');
            });
        }
        if (Schema::hasColumn('f_logs', 'fl_broken_id')) {
            Schema::table('f_logs', function (Blueprint $table) {
                $table->dropForeign(['fl_broken_id']);
                $table->dropColumn('fl_broken_id');
            });
        }
        if (Schema::hasColumn('f_logs', 'head_img')) {
            Schema::table('f_logs', function (Blueprint $table) {
                $table->dropColumn('head_img');
            });
        } 
        if (Schema::hasColumn('f_logs', 'head_img_path')) {
            Schema::table('f_logs', function (Blueprint $table) {
                $table->dropColumn('head_img_path');
            });
        } 
        if (Schema::hasColumn('f_logs', 'left_img')) {
            Schema::table('f_logs', function (Blueprint $table) {
                $table->dropColumn('left_img');
            });
        } 
        if (Schema::hasColumn('f_logs', 'left_img_path')) {
            Schema::table('f_logs', function (Blueprint $table) {
                $table->dropColumn('left_img_path');
            });
        } 
        if (Schema::hasColumn('f_logs', 'right_img')) {
            Schema::table('f_logs', function (Blueprint $table) {
                $table->dropColumn('right_img');
            });
        } 
        if (Schema::hasColumn('f_logs', 'right_img_path')) {
            Schema::table('f_logs', function (Blueprint $table) {
                $table->dropColumn('right_img_path');
            });
        } 
        if (Schema::hasColumn('f_logs', 'back_img')) {
            Schema::table('f_logs', function (Blueprint $table) {
                $table->dropColumn('back_img');
            });
        } 
        if (Schema::hasColumn('f_logs', 'back_img_path')) {
            Schema::table('f_logs', function (Blueprint $table) {
                $table->dropColumn('back_img_path');
            });
        } 
        if (Schema::hasColumn('f_logs', 'batt_img')) {
            Schema::table('f_logs', function (Blueprint $table) {
                $table->dropColumn('batt_img');
            });
        } 
        if (Schema::hasColumn('f_logs', 'batt_img_path')) {
            Schema::table('f_logs', function (Blueprint $table) {
                $table->dropColumn('batt_img_path');
            });
        } 
        if (Schema::hasColumn('f_logs', 'monitor_img')) {
            Schema::table('f_logs', function (Blueprint $table) {
                $table->dropColumn('monitor_img');
            });
        } 
        if (Schema::hasColumn('f_logs', 'monitor_img_path')) {
            Schema::table('f_logs', function (Blueprint $table) {
                $table->dropColumn('monitor_img_path');
            });
        }
        if (Schema::hasColumn('f_logs', 'user_id')) {
            Schema::table('f_logs', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropColumn('user_id');
            });
        }  
    }
}
