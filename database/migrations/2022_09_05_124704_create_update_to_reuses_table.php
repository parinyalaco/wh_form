<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpdateToReusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'to_reuses',
            function (Blueprint $table) {
                $table->foreignId('stock_id')->nullable()->constrained('stocks');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('to_reuses', 'stock_id')) {
            Schema::table('to_reuses', function (Blueprint $table) {
                $table->dropForeign(['stock_id']);
                $table->dropColumn('stock_id');
            });
        }
    }
}
