<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHandheldImportDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('handheld_import_data', function (Blueprint $table) {
            $table->id();
            $table->string('mat_desc', 100);
            $table->string('mat', 100);
            $table->string('movement_type_txt', 100);
            $table->date('posting_date');
            $table->string('batch', 100);
            $table->datetime('entry_date');
            $table->string('mat_doc');
            $table->string('movement_type', 100);
            $table->string('storage_loc', 100);
            $table->integer('mat_doc_item');
            $table->float('qty_in_unit');
            $table->string('unit_entry',100);
            $table->float('amount_in_lc')->nullable();
            $table->string('ref',100)->nullable();
            $table->string('order', 100)->nullable();
            $table->string('username', 100)->nullable();
            $table->string('item_auto', 20)->nullable();
            $table->string('process_type', 100)->nullable();
            $table->string('process_status', 100)->nullable();
            $table->string('filename', 100);
            $table->string('status', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('handheld_import_data');
    }
}
