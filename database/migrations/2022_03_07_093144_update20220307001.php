<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update20220307001 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'recv_rm_others',
            function (Blueprint $table) {
                $table->integer('recv_rm_other_plan_m_id')->nullable();
            }
        );

        Schema::table(
            'recv_rm_other_plans',
            function (Blueprint $table) {
                $table->integer('recv_rm_other_plan_m_id')->nullable();
            }
        );

        Schema::table(
            'recv_rm_other_imports',
            function (Blueprint $table) {
                $table->integer('recv_rm_other_plan_m_id')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('recv_rm_others', 'recv_rm_other_plan_m_id')) {
            Schema::table('recv_rm_others', function (Blueprint $table) {
                $table->dropColumn('recv_rm_other_plan_m_id');
            });
        }

        if (Schema::hasColumn('recv_rm_other_plans', 'recv_rm_other_plan_m_id')) {
            Schema::table('recv_rm_other_plans', function (Blueprint $table) {
                $table->dropColumn('recv_rm_other_plan_m_id');
            });
        }

        if (Schema::hasColumn('recv_rm_other_imports', 'recv_rm_other_plan_m_id')) {
            Schema::table('recv_rm_other_imports', function (Blueprint $table) {
                $table->dropColumn('recv_rm_other_plan_m_id');
            });
        }
    }
}
