<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStBrokersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('st_brokers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('rm_broker_id')->constrained();
            $table->foreignId('st_product_id')->constrained();
            $table->integer('amount');
            $table->date('start_date');
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('st_brokers');
    }
}
