<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpdatealltableusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'to_reuses',
            function (Blueprint $table) {
                $table->foreignId('user_id')->nullable()->constrained('users');
            }
        );
        Schema::table(
            'washes',
            function (Blueprint $table) {
                $table->foreignId('user_id')->nullable()->constrained('users');
            }
        );
        Schema::table(
            'dries',
            function (Blueprint $table) {
                $table->foreignId('user_id')->nullable()->constrained('users');
            }
        );
        Schema::table(
            'shines',
            function (Blueprint $table) {
                $table->foreignId('user_id')->nullable()->constrained('users');
            }
        );
        Schema::table(
            'choose_boxes',
            function (Blueprint $table) {
                $table->foreignId('user_id')->nullable()->constrained('users');
            }
        );
        Schema::table(
            'stocks',
            function (Blueprint $table) {
                $table->foreignId('user_id')->nullable()->constrained('users');
            }
        );
        Schema::table(
            'loses',
            function (Blueprint $table) {
                $table->foreignId('user_id')->nullable()->constrained('users');
            }
        );
        Schema::table(
            'stock_processes',
            function (Blueprint $table) {
                $table->foreignId('user_id')->nullable()->constrained('users');
            }
        );
        Schema::table(
            'targets',
            function (Blueprint $table) {
                $table->foreignId('user_id')->nullable()->constrained('users');
            }
        );
        Schema::table(
            'target_manpowers',
            function (Blueprint $table) {
                $table->foreignId('user_id')->nullable()->constrained('users');
            }
        );
        Schema::table(
            'target_products',
            function (Blueprint $table) {
                $table->foreignId('user_id')->nullable()->constrained('users');
            }
        );
        Schema::table(
            'fs_hw_mains',
            function (Blueprint $table) {
                $table->foreignId('user_id')->nullable()->constrained('users');
            }
        );
        Schema::table(
            'fs_wh_tran_ms',
            function (Blueprint $table) {
                $table->foreignId('user_id')->nullable()->constrained('users');
            }
        );
        Schema::table(
            'fs_wh_tran_ds',
            function (Blueprint $table) {
                $table->foreignId('stock_id')->nullable()->constrained('stocks');
                $table->foreignId('user_id')->nullable()->constrained('users');
                $table->date('to_date')->nullable();
            }
        );
        Schema::table(
            'fs_wh_stocks',
            function (Blueprint $table) {
                $table->foreignId('stock_id')->nullable()->constrained('stocks');
                $table->foreignId('user_id')->nullable()->constrained('users');
                $table->date('to_date')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('to_reuses', 'user_id')) {
            Schema::table('to_reuses', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropColumn('user_id');
            });
        }
        if (Schema::hasColumn('washes', 'user_id')) {
            Schema::table('washes', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropColumn('user_id');
            });
        }
        if (Schema::hasColumn('dries', 'user_id')) {
            Schema::table('dries', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropColumn('user_id');
            });
        }
        if (Schema::hasColumn('shines', 'user_id')) {
            Schema::table('shines', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropColumn('user_id');
            });
        }
        if (Schema::hasColumn('choose_boxes', 'user_id')) {
            Schema::table('choose_boxes', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropColumn('user_id');
            });
        }
        if (Schema::hasColumn('stocks', 'user_id')) {
            Schema::table('stocks', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropColumn('user_id');
            });
        }
        if (Schema::hasColumn('loses', 'user_id')) {
            Schema::table('loses', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropColumn('user_id');
            });
        }
        if (Schema::hasColumn('stock_processes', 'user_id')) {
            Schema::table('stock_processes', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropColumn('user_id');
            });
        }
        if (Schema::hasColumn('targets', 'user_id')) {
            Schema::table('targets', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropColumn('user_id');
            });
        }
        if (Schema::hasColumn('target_manpowers', 'user_id')) {
            Schema::table('target_manpowers', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropColumn('user_id');
            });
        }
        if (Schema::hasColumn('target_products', 'user_id')) {
            Schema::table('target_products', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropColumn('user_id');
            });
        }
        if (Schema::hasColumn('fs_hw_mains', 'user_id')) {
            Schema::table('fs_hw_mains', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropColumn('user_id');
            });
        }
        // if (Schema::hasColumn('fs_hw_locs', 'user_id')) {
        //     Schema::table('fs_hw_locs', function (Blueprint $table) {
        //         $table->dropForeign(['user_id']);
        //         $table->dropColumn('user_id');
        //     });
        // }
        if (Schema::hasColumn('fs_wh_tran_ms', 'user_id')) {
            Schema::table('fs_wh_tran_ms', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropColumn('user_id');
            });
        }
        if (Schema::hasColumn('fs_wh_tran_ds', 'stock_id')) {
            Schema::table('fs_wh_tran_ds', function (Blueprint $table) {
                $table->dropForeign(['stock_id']);
                $table->dropColumn('stock_id');
            });
        }
        if (Schema::hasColumn('fs_wh_tran_ds', 'user_id')) {
            Schema::table('fs_wh_tran_ds', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropColumn('user_id');
            });
        }
        if (Schema::hasColumn('fs_wh_tran_ds', 'to_date')) {
            Schema::table('fs_wh_tran_ds', function (Blueprint $table) {
                $table->dropColumn('to_date');
            });
        }
        if (Schema::hasColumn('fs_wh_stocks', 'stock_id')) {
            Schema::table('fs_wh_stocks', function (Blueprint $table) {
                $table->dropForeign(['stock_id']);
                $table->dropColumn('stock_id');
            });
        }
        if (Schema::hasColumn('fs_wh_stocks', 'user_id')) {
            Schema::table('fs_wh_stocks', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropColumn('user_id');
            });
        }
        if (Schema::hasColumn('fs_wh_stocks', 'to_date')) {
            Schema::table('fs_wh_stocks', function (Blueprint $table) {
                $table->dropColumn('to_date');
            });
        }
    }
}
