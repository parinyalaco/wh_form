<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRmManualsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rm_manuals', function (Blueprint $table) {
            $table->id();
            $table->foreignId('file_excel_id')->constrained();  //ใช้กรณีชื่อ field เป็นชื่อตารางต่อด้วย id เลยไม่ต้องระบุตารางที่ link หา
            $table->date('manual_date');
            $table->string('sap_no')->nullable();
            $table->integer('manual_no')->nullable();
            $table->string('car_name', 20)->nullable();  //ทะเบียนรถ

            // $table->unsignedBigInteger('rm_car_id');     //2 บรรทัดนี้ใช้กรณีชื่อ field ไม่สื่อถึงชื่อตาราง
            // $table->foreign('rm_car_id')->references('id')->on('rm_cars');
            $table->string('rm_area_code', 10)->nullable();  //WE02
            $table->foreignId('rm_area_id')->constrained();
            $table->integer('manual_num')->nullable();
            $table->integer('bad_num')->nullable(); //กระสอบเสีย
            $table->float('manual_weight')->nullable();
            $table->time('time_out_area')->nullable();
            $table->time('time_to_laco')->nullable();
            $table->time('time_start_work')->nullable();
            $table->time('time_end_work')->nullable();
            $table->time('time_use')->nullable();
            $table->integer('staff_use')->nullable();
            $table->foreignId('rm_car_id')->nullable()->constrained(); //car_size
            $table->foreignId('rm_type_receive_id')->constrained();
            $table->integer('far_num')->nullable();
            $table->text('note')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rm_manuals');
    }
}
