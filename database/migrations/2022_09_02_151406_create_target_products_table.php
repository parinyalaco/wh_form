<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTargetProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('target_products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('target_id')->constrained('targets'); 
            // $table->foreignId('product_id')->constrained('products');
            // $table->foreignId('step_id')->constrained('steps'); 
            $table->string('step_tg_id')->constrained('step_tgs');
            $table->integer('quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('target_products');
    }
}
