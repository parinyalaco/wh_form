<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRmBrokerAreasTable extends Migration
{

    public function up()
    {
        Schema::create('rm_broker_areas', function (Blueprint $table) {
            $table->id();
            $table->integer('rm_broker_id');
            $table->integer('rm_area_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('rm_broker_areas');
    }
}
