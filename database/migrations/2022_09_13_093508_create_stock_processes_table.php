<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_processes', function (Blueprint $table) {
            $table->id();            
            $table->date('to_date');
            $table->foreignId('stock_id')->constrained('stocks');
            $table->integer('pass')->default(0);
            $table->integer('not_pass')->default(0); 
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_processes');
    }
}
