<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('f_logs', function (Blueprint $table) {
            $table->id();
            $table->date('log_date');

            $table->unsignedBigInteger('f_location_id');
		    $table->foreign('f_location_id')->references('id')->on('flocations');
            // $table->foreignId('f_location_id')->constrained();

            $table->unsignedBigInteger('forklift_id');
		    $table->foreign('forklift_id')->references('id')->on('forklifts');
            // $table->foreignId('forklift_id')->constrained();

            // $table->unsignedBigInteger('f_person_id');
		    // $table->foreign('f_person_id')->references('id')->on('fpeople');
            // $table->foreignId('f_person_id')->constrained()->nullable();
            $table->foreignId('fperson_id')->nullable()->constrained();

            $table->string('shift', 20);  //B or C 
            $table->string('type', 20);  //R , S or A
            $table->float('time')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('f_logs');
    }
}
