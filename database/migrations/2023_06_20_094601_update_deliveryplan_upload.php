<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDeliveryplanUpload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'imp_delivery_plans',
            function (Blueprint $table) {
                $table->date('cy_plan_date')->nullable();
                $table->date('cy_act_date')->nullable();
                $table->date('load_plan_date')->nullable();
                $table->date('load_act_date')->nullable();
                $table->date('ship_plan_date')->nullable();
                $table->date('ship_act_date')->nullable();
                $table->integer('vessel_line_id')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('imp_delivery_plans', 'cy_plan_date')) {
            Schema::table('imp_delivery_plans', function (Blueprint $table) {
                $table->dropColumn('cy_plan_date');
            });
        }
        if (Schema::hasColumn('imp_delivery_plans', 'cy_act_date')) {
            Schema::table('imp_delivery_plans', function (Blueprint $table) {
                $table->dropColumn('cy_act_date');
            });
        }
        if (Schema::hasColumn('imp_delivery_plans', 'load_plan_date')) {
            Schema::table('imp_delivery_plans', function (Blueprint $table) {
                $table->dropColumn('load_plan_date');
            });
        }
        if (Schema::hasColumn('imp_delivery_plans', 'load_act_date')) {
            Schema::table('imp_delivery_plans', function (Blueprint $table) {
                $table->dropColumn('load_act_date');
            });
        }
        if (Schema::hasColumn('imp_delivery_plans', 'ship_plan_date')) {
            Schema::table('imp_delivery_plans', function (Blueprint $table) {
                $table->dropColumn('ship_plan_date');
            });
        }
        if (Schema::hasColumn('imp_delivery_plans', 'ship_act_date')) {
            Schema::table('imp_delivery_plans', function (Blueprint $table) {
                $table->dropColumn('ship_act_date');
            });
        }
        if (Schema::hasColumn('imp_delivery_plans', 'vessel_line_id')) {
            Schema::table('imp_delivery_plans', function (Blueprint $table) {
                $table->dropColumn('vessel_line_id');
            });
        }
    }
}
