<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFsHwLocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_hw_locs', function (Blueprint $table) {
            $table->id();
            $table->integer('fs_wh_id')->nullable();
            $table->string('name');
            $table->string('desc')->nullable();
            $table->integer('row')->default(0);
            $table->integer('col')->default(0);
            $table->integer('level')->default(0);
            $table->integer('product_id')->default(0);
            $table->string('status',30)->default("Active");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fs_hw_locs');
    }
}
