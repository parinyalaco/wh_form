<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWhPlanActDSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wh_plan_act_d_s', function (Blueprint $table) {
            $table->id();
            // $table->foreignId('wh_plan_act_m_id')->constrained('wh_plan_act_m_s'); 
            $table->string('mov_allowed')->nullable();
            $table->string('reservation_no',10);
            $table->integer('reservation_item')->nullable();
            $table->date('requirement_date');
            $table->foreignId('wh_act_type_id')->constrained('wh_act_types');
            $table->foreignId('wh_act_material_id')->constrained('wh_act_materials');
            $table->float('entry_qty');
            $table->string('unloading_date');
            // $table->time('unloading_time');
            $table->string('item_text')->nullable();
            $table->string('recipient',20)->nullable();
            $table->string('storage_location',10)->nullable();
            $table->string('plant',10)->nullable();
            $table->float('base_qty')->nullable();
            $table->float('withdraw_qty')->nullable();
            $table->float('remain_qty')->nullable();
            $table->string('order', 50)->nullable();
            $table->string('order_type')->nullable();
            $table->date('start_date')->nullable();
            $table->date('finish_date')->nullable();
            $table->string('item_text_line1')->nullable();
            $table->string('requirement_type')->nullable();
            $table->string('batch')->nullable();
            $table->string('del')->nullable();
            $table->string('fls')->nullable();
            $table->string('cost_center')->nullable();
            $table->string('receiving_location');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wh_plan_act_d_s');
    }
}
