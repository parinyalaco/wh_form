<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRmFarmersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rm_farmers', function (Blueprint $table) {
            $table->id();
            $table->string('name',100); 
            $table->foreignId('rm_broker_id')->constrained(); 
            // $table->unsignedBigInteger('rm_broker_id');           
		    // $table->foreign('rm_broker_id')->references('id')->on('rm_brokers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rm_farmers');
    }
}
