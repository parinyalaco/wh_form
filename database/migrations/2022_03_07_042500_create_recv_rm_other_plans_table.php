<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecvRmOtherPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recv_rm_other_plans', function (Blueprint $table) {
            $table->id();
            $table->integer('recv_rm_other_import_id')->nullable();
            $table->integer('row')->nullable();
            $table->date('plan_date');
            $table->integer('qty')->default(0);
            $table->float('weight')->default(0.0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recv_rm_other_plans');
    }
}
