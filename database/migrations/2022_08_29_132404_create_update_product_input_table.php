<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpdateProductInputTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'product_inputs',
            function (Blueprint $table) {
                $table->string('po_no', 20)->nullable();
            }
        );
        Schema::table(
            'product_input_ens',
            function (Blueprint $table) {
                $table->string('po_no', 20)->nullable();
            }
        );
        Schema::table(
            'product_input_dcs',
            function (Blueprint $table) {
                $table->string('po_no', 20)->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('product_inputs', 'po_no')) {
            Schema::table('product_inputs', function (Blueprint $table) {
                $table->dropColumn('po_no');
            });
        }

        if (Schema::hasColumn('product_input_ens', 'po_no')) {
            Schema::table('product_input_ens', function (Blueprint $table) {
                $table->dropColumn('po_no');
            });
        }
        
        if (Schema::hasColumn('product_input_dcs', 'po_no')) {
            Schema::table('product_input_dcs', function (Blueprint $table) {
                $table->dropColumn('po_no');
            });
        }
    }
}
