<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChooseBoxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('choose_boxes', function (Blueprint $table) {
            $table->id();
            $table->date('to_date');
            $table->string('shift')->nullable();
            $table->foreignId('to_reuse_id')->constrained('to_reuses');
            $table->string('pallet_no')->nullable();
            $table->integer('quantity');
            $table->string('note')->nullable();
            $table->string('status', 1)->default('1');  //1=ใช้, 0=ไม่ใช้
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('choose_boxes');
    }
}
