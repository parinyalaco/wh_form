<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTargetManpowersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('target_manpowers', function (Blueprint $table) {
            $table->id();
            // $table->foreignId('target_id')->constrained('targets'); 
            $table->foreignId('target_product_id')->constrained('target_products'); 
            $table->foreignId('store_id')->constrained('stores');            
            $table->integer('quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('target_manpowers');
    }
}
