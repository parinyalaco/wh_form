<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManpowerDSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manpower_d_s', function (Blueprint $table) {
            $table->id();
            $table->integer('manpower_id');
            $table->integer('job_data_id');
            $table->integer('std_manpower_rate_d_id');
            $table->integer('act_rate');
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manpower_d_s');
    }
}
