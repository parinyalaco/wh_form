<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dries', function (Blueprint $table) {
            $table->id();
            $table->date('to_date');
            $table->string('shift')->nullable();
            $table->foreignId('wash_id')->constrained('washes');
            $table->integer('dry_no');      //ถังปั่นที่
            $table->integer('round_no');    //รอบ
            $table->integer('sort')->nullable();    //ลำดับที่ เผื่อมี
            $table->integer('quantity');
            $table->string('note')->nullable();
            $table->string('status', 1)->default('1');  //1=ใช้, 0=ไม่ใช้
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dries');
    }
}
