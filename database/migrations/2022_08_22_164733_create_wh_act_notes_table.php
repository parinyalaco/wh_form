<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWhActNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wh_act_notes', function (Blueprint $table) {
            $table->id();
            $table->date('entry_date'); 
            $table->string('store_loc', 20)->nullable();
            $table->foreignId('wh_act_material_id')->constrained('wh_act_materials');
            $table->string('note', 500)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wh_act_notes');
    }
}
