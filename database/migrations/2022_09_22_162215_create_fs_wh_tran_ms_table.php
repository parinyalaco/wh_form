<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFsWhTranMsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_wh_tran_ms', function (Blueprint $table) {
            $table->id();
            $table->string('tran_type',50);
            $table->string('desc')->nullable();
            $table->string('user',100)->nullable();
            $table->string('status',50)->default('Action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fs_wh_tran_ms');
    }
}
