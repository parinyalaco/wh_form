<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->id();
            $table->date('to_date');
            $table->string('shift')->nullable();
            $table->foreignId('to_reuse_id')->nullable()->constrained('to_reuses');
            $table->foreignId('dry_id')->nullable()->constrained('dries');
            $table->foreignId('shine_id')->nullable()->constrained('shines');
            $table->foreignId('choose_box_id')->nullable()->constrained('choose_boxes');
            $table->string('pallet_no')->nullable();
            $table->integer('quantity');
            $table->integer('wait');
            $table->integer('pass')->default(0);
            $table->integer('not_pass')->default(0);
            $table->string('status_pass')->default('0');   //0=process, 1=complete, 2=hold
            $table->string('note')->nullable();
            $table->string('status', 1)->default('1');  //1=ใช้, 0=ไม่ใช้
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}
