<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWhPlanActsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wh_plan_acts', function (Blueprint $table) {
            $table->id();
            $table->string('filename');
            // $table->integer('import_num')->default(0);
            $table->date('requirement_date');
            // $table->string('status',20)->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wh_plan_acts');
    }
}
