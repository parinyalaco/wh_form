<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWhPlanActMSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wh_plan_act_m_s', function (Blueprint $table) {
            $table->id();
            $table->foreignId('wh_plan_act_id')->constrained('wh_plan_acts'); 
            $table->date('plan_date');
            $table->time('plan_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wh_plan_act_m_s');
    }
}
