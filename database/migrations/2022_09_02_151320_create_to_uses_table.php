<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToUsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('to_uses', function (Blueprint $table) {
            $table->id();
            $table->date('to_date');
            $table->foreignId('stock_id')->constrained('stocks');
            $table->string('pallet_no')->nullable();
            $table->foreignId('store_id')->nullable()->constrained('stores');
            $table->integer('quantity');
            $table->string('note')->nullable();
            $table->string('status', 1)->default('1');  //1=ใช้, 0=ไม่ใช้
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('to_uses');
    }
}
