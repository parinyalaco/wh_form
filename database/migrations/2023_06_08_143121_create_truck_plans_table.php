<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTruckPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('truck_plans', function (Blueprint $table) {
            $table->id();
            $table->string('file_name')->nullable();
            $table->integer('truck_id');
            $table->integer('imp_delivery_plan_id');
            $table->string('order_name');
            $table->date('ship_date');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('status', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('truck_plans');
    }
}
