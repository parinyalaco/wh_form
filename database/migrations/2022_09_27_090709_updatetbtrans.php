<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Updatetbtrans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'fs_wh_tran_ms',
            function (Blueprint $table) {
                $table->dateTime('tran_dt')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('fs_wh_tran_ms', 'tran_dt')) {
            Schema::table('fs_wh_tran_ms', function (Blueprint $table) {
                $table->dropColumn('tran_dt');
            });
        }

    }
}
