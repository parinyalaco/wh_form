<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWhActivityDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wh_activity_details', function (Blueprint $table) {
            $table->id();
            // $table->foreignId('wh_activity_id')->constrained('wh_activities'); 
            $table->integer('row');
            $table->foreignId('wh_act_material_id')->constrained('wh_act_materials');
            $table->foreignId('wh_move_type_id')->constrained('wh_move_types');
            $table->date('posting_date')->nullable();
            $table->string('batch', 20)->nullable();
            $table->date('entry_date');            
            $table->time('entry_time');
            $table->string('mat_doc',20)->nullable();
            $table->foreignId('wh_act_type_id')->constrained('wh_act_types');            
            $table->string('store_loc', 20)->nullable();
            $table->string('mat_doc_item', 10)->nullable();
            $table->float('qty_entry')->nullable();
            $table->float('amount_lc')->nullable();
            $table->string('ref', 50)->nullable();
            $table->string('order', 50)->nullable();
            $table->string('username', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wh_activity_details');
    }
}
