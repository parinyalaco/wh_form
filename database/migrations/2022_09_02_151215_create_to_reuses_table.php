<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToReusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('to_reuses', function (Blueprint $table) {
            $table->id();
            $table->date('to_date');
            $table->foreignId('product_id')->constrained('products');
            // $table->string('stock_id')->nullable();
            $table->integer('quantity');
            $table->string('pallet_no')->nullable();
            $table->string('is_end', 1)->default('0');  //1=หมดแล้ว, 0=ยังไม่หมด      
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('to_reuses');
    }
}
