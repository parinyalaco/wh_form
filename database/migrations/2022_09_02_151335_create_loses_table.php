<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLosesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loses', function (Blueprint $table) {
            $table->id();
            $table->date('to_date');
            $table->string('shift')->nullable();
            $table->foreignId('step_id')->constrained('steps');            
            $table->integer('refer_id');
            $table->foreignId('lose_type_id')->constrained('lose_types');  
            $table->integer('quantity');
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loses');
    }
}
