<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRmIssuesDailyImgsTable extends Migration
{

    public function up()
    {
        Schema::create('rm_issues_daily_imgs', function (Blueprint $table) {
            $table->id();
            $table->integer('rm_issue_daily_id');
            $table->string('path');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('rm_issues_daily_imgs');
    }
}
