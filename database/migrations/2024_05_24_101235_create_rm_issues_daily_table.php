<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRmIssuesDailyTable extends Migration
{
    public function up()
    {
        Schema::create('rm_issues_daily', function (Blueprint $table) {
            $table->id();
            $table->date('issue_date');
            $table->integer('raw_material_id');
            $table->string('detail')->nullable();


            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('rm_issues_daily');
    }
}
