<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update20220302001 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'recv_rm_others',
            function (Blueprint $table) {
                $table->integer('row')->nullable();
                $table->integer('recv_rm_other_import_id')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('recv_rm_others', 'row')) {
            Schema::table('recv_rm_others', function (Blueprint $table) {
                $table->dropColumn('row');
            });
        }

        if (Schema::hasColumn('recv_rm_others', 'recv_rm_other_import_id')) {
            Schema::table('recv_rm_others', function (Blueprint $table) {
                $table->dropColumn('recv_rm_other_import_id');
            });
        }
    }
}
