<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStWithdrawsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('st_withdraws', function (Blueprint $table) {
            $table->id();
            $table->string('no_id',20);
            $table->foreignId('rm_broker_id')->constrained();
            $table->foreignId('st_product_id')->constrained();
            $table->date('withdraw_date');
            $table->integer('amount');
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('st_withdraws');
    }
}
