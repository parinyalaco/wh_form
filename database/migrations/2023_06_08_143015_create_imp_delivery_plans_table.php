<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImpDeliveryPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imp_delivery_plans', function (Blueprint $table) {
            $table->id();
            $table->string('file_name');
            $table->string('container');
            $table->string('csn');
            $table->string('order');
            $table->string('customer_po_no');
            $table->date('loading_date');
            $table->date('etd_date');
            $table->string('etd_place');
            $table->date('eta_date');
            $table->string('eta_place');
            $table->string('vl');
            $table->string('fv');
            $table->string('desc');
            $table->string('remark1');
            $table->string('place_port');
            $table->integer('schedule_truck_id')->nullable();
            $table->string('status',50);

            $table->string('weight')->nullable();
            $table->string('Booking_no')->nullable();

            $table->string('cut_off')->nullable();
            $table->string('time')->nullable();
            $table->string('tail_id')->nullable();
            $table->string('get_container')->nullable();
            $table->string('RT')->nullable();
            $table->string('remark')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imp_delivery_plans');
    }
}
