<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStdManpowerRateDSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('std_manpower_rate_d_s', function (Blueprint $table) {
            $table->id();
            $table->integer('std_manpower_rate_id');
            $table->integer('job_data_id');
            $table->integer('rate');
            $table->string('note')->nullable();
            $table->string('status', 50)->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('std_manpower_rate_d_s');
    }
}
