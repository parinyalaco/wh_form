<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\WhDep;

class JobDataSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $dataset = array(
            'FZ' => array(
                'B' => array(
                    'ผช.พนักงาน (รับ-จ่ายหน้างาน)',
                    'ขับรถหน้างาน',
                    'เช็คเกอร์',
                    'ธุรการ',
                    'ผช.พนักงาน (โหลด)ห้อง2-3,4',
                    'ขับรถโหลดสินค้าห้อง2-3,4',
                    'เช็คเกอร์ ห้อง 4',
                    'โหลดสินค้า เข้างานเวลา 07.30 - 16.30 น.',
                    'โหลดสินค้า เข้างานเวลา 13.00 - 22.00 น.'
                ),
                'C' => array(
                    'ผช.พนักงาน (รับ-จ่ายหน้างาน)',
                    'ขับรถหน้างาน',
                    'เช็คเกอร์',
                    'ธุรการ',
                    'ผช.พนักงาน (โหลด)ห้อง2-3,4',
                    'ขับรถโหลดสินค้าห้อง2-3,4',
                    'เช็คเกอร์ ห้อง 4',
                    'โหลดสินค้า เข้างานเวลา 19.30 - 04.30 น.'
                ),
            ),
            'RM&Store' => array(
                'B' => array(
                    'ผช.พนักงาน',
                    'ผช.งานเอกสารSAP ส่วนพัสดุ',
                    'พนักงานขับรถ รับ-จ่าย เพสต์',
                    'พนักงานขับรถ รับ-จ่าย พัสดุ',
                    'รับ-จ่าย พัสดุ',
                    'งานเอกสาร ,SAP ส่วนวัตถุดิบ',
                    'รับวัตถุดิบ'
                ),
                'C' => array(
                    'รับ - จ่าย วัตถุดิบ' 
                ),
            ),
            'FS' => array(
                'B' => array(
                    'ซักอบผ้า',
                    'งานห้องแต่งตัว , ตรวจ GMP', 
                    'เตรียมสารเคมี',
                    'คัดกล่อง',
                    'ทำถุงขยะใช้ภายในโรงงาน',
                    'ซักถุง',
                    'พับถุง ปั่นถุงและปั่นกระสอบ',
                    'ม้วนแพคกระสอบ',
                    'ขัดดูดกะ B',
                    'จัดเก็บอุปกรณ์',
                    'แม่บ้าน',
                    'พนักงานตั้งครรภ์มาจากแผนก PK',
                    'พนักงานตั้งครรภ์มาจากแผนก PF',
                    'พนักงาน PK',
                    'พนักงาน PF'
                ),
                'C' => array(
                    'ซักอบผ้า',
                    'งานห้องแต่งตัว , ตรวจ GMP',
                    'จัดเก็บอุปกรณ์',
                    'ขัดดูด',
                    'ซักและปั่นกระสอบ (Reuse)',
                    'แม่บ้าน'
                ),
            )
        );
        foreach($dataset as $whdepname => $objDeps){
            $whdepid = WhDep::where('name', $whdepname)->first()->id;
            foreach ($objDeps as $shiftname => $objJobs) {
                foreach ($objJobs as $jobname) {
                    DB::table('job_data')
                    ->insert([
                        'wh_dep_id' => $whdepid,
                        'shift' => $shiftname,
                        'name' => $jobname,
                        'desc' => $jobname,
                        'status' => 'Active'
                    ]);
                }
            }
        }
    }
}
