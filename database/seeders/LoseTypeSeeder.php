<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\FS\LoseType;

class LoseTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LoseType::create([
            'name' => 'ขาดกลาง',
        ]);
        LoseType::create([
            'name' => 'ขาดปาก',
        ]);
        LoseType::create([
            'name' => 'ขาดก้นถุง',
        ]);
        LoseType::create([
            'name' => 'สีจางซีด',
        ]);
        LoseType::create([
            'name' => 'เป็นสนิม',
        ]);
        LoseType::create([
            'name' => 'เสีย',
        ]);
        LoseType::create([
            'name' => 'นับผิด',
        ]);
        LoseType::create([
            'name' => 'auto',
        ]);
    }
}
