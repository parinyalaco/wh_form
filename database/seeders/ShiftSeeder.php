<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\FS\Shift;

class ShiftSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Shift::create([
            'name' => 'B',
            'start_time' => date('H:i', strtotime('07:30')),
            'end_time' => date('H:i', strtotime('18:30')),
        ]);
        Shift::create([
            'name' => 'C',
            'start_time' => date('H:i', strtotime('18:30')),
            'end_time' => date('H:i', strtotime('05:30')),
        ]);
    }
}
