<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\FS\Step;

class StepSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Step::create([
            'name' => 'Reuse',
        ]);
        Step::create([
            'name' => 'Wash',
            // 'to_target' => 'การซัก',
        ]);
        Step::create([
            'name' => 'Dry',
            // 'to_target' => 'การปั่น',
        ]);
        Step::create([
            'name' => 'Stock',
            // 'to_target' => 'การพับ',
        ]);
        Step::create([
            'name' => 'Use',
        ]);
        Step::create([
            'name' => 'Chose_box',
        ]);
        Step::create([
            'name' => 'Shine',
            // 'to_target' => 'การพับ',
        ]);
    }
}
