<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\FS\StepTg;

class StepTgSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StepTg::create([
            'name' => 'การซัก',
            'refer_step' => '2',
        ]);
        StepTg::create([
            'name' => 'การปั่น, การพับ',
            'refer_step' => '3,4',
        ]);
        StepTg::create([
            'name' => 'การคัด',
            'refer_step' => '6',
        ]);
        StepTg::create([
            'name' => 'การตาก',
            'refer_step' => '7',
        ]);
    }
}
