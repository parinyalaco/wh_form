<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ProductInput;
use App\Models\Material;
use App\Models\Vendor;

class ProductInputSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductInput::factory(20)->create();
    }
}
