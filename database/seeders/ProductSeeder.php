<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\FS\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => 'ถุง',
            'status_wash' => '1',
            'status_check' => '1',
        ]);
        Product::create([
            'name' => 'กล่อง',
        ]);
        Product::create([
            'name' => 'กระสอบสีขาว',
            'status_wash' => '1',
            'status_check' => '1',
        ]);
        Product::create([
            'name' => 'กระสอบสีเหลือง',
            'status_wash' => '1',
        ]);
        Product::create([
            'name' => 'กระสอบสีฟ้า',
            'status_wash' => '1',
        ]);
    }
}
