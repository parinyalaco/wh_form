<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserType;

class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // UserType::factory(2)->create();
        UserType::create([
            'name' => 'Admin',
        ]);
        UserType::create([
            'name' => 'WH',
        ]);
    }
}
