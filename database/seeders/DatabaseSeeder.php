<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {        
        $this->call([
            // UserTypeSeeder::class,
            // UserSeeder::class,
            // UnitSeeder::class,
            // ProductTypeSeeder::class,
            // MaterialSeeder::class,            
            // VendorSeeder::class,
            // ProductInputSeeder::class,
            StepSeeder::class,
            // ProductSeeder::class,
            // LoseTypeSeeder::class,
            ShiftSeeder::class,
            StepTgSeeder::class,
        ]);
    }
}
