<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WhDataSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wh_deps')
        ->insert(['name' => 'FZ', 'desc' => 'FZ', 'status' => 'Active']);

        DB::table('wh_deps')
        ->insert(['name' => 'RM&Store', 'desc' => 'RM&Store', 'status' => 'Active']);

        DB::table('wh_deps')
        ->insert(['name' => 'FS', 'desc' => 'FS', 'status' => 'Active']);
        
    }
}
