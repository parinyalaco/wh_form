<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class WhActSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wh_act_types')
        ->insert(['code'=> '101','name' => 'รับ', 'desc' => 'รับ', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '102', 'name' => 'รับ', 'desc' => 'รับ', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '201', 'name' => 'จ่าย', 'desc' => 'จ่าย', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '261', 'name' => 'จ่าย', 'desc' => 'จ่าย', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '262', 'name' => 'จ่าย', 'desc' => 'จ่าย', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '312', 'name' => 'เคลื่อนย้าย', 'desc' => 'เคลื่อนย้าย', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '312', 'name' => 'เคลื่อนย้าย', 'desc' => 'เคลื่อนย้าย', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '321', 'name' => 'เคลื่อนย้าย', 'desc' => 'เคลื่อนย้าย', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '322', 'name' => 'เคลื่อนย้าย', 'desc' => 'เคลื่อนย้าย', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '323', 'name' => 'เคลื่อนย้าย', 'desc' => 'เคลื่อนย้าย', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '325', 'name' => 'เคลื่อนย้าย', 'desc' => 'เคลื่อนย้าย', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '343', 'name' => 'เคลื่อนย้าย', 'desc' => 'เคลื่อนย้าย', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '350', 'name' => 'เคลื่อนย้าย', 'desc' => 'เคลื่อนย้าย', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '531', 'name' => 'รับ', 'desc' => 'รับ', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '532', 'name' => 'รับ', 'desc' => 'รับ', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '601', 'name' => 'ขาย', 'desc' => 'ขาย', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '602', 'name' => 'ขาย', 'desc' => 'ขาย', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '901', 'name' => 'จ่าย', 'desc' => 'จ่าย', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '905', 'name' => 'จ่าย', 'desc' => 'จ่าย', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '907', 'name' => 'จ่าย', 'desc' => 'จ่าย', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '911', 'name' => 'จ่าย', 'desc' => 'จ่าย', 'status' => 'Active']);

        DB::table('wh_act_types')
        ->insert(['code' => '919', 'name' => 'จ่าย', 'desc' => 'จ่าย', 'status' => 'Active']);
    }
}
