<?php
return [
    'mytest' => env('MY_test', 'test'), 
    // 'mytest' => env('MY_test', 'real'),
    'emaillist' => [
        'AH' => 'Anucha@Lannaagro.com',
        'BN' => 'Benya@Lannaagro.com',
        'WU' => 'Wanatsanun@lannaagro.com',
        'WT' => 'Wichchan@lannaagro.com',
        'TG' => 'chatchanaset@lannaagro.com',
        'PCH' => 'Pikul@Lannaagro.com',
        'WJ' => 'Warangkana@Lannaagro.com',
        'AY' => 'Akkarapon@Lannaagro.com',
        'DN' => 'Daraporn@Lannaagro.com',
        'WW' => 'Wanwalee@Lannaagro.com',
        'VW' => 'Varut@lannaagro.com',
        'OC' => 'Oraphan@Lannaagro.com',
        'PKP' => 'parinya.k@lannaagro.com',
        'PC' => 'Pimchanok@Lannaagro.com',
        'SJU' => 'Supattra2@Lannaagro.com', 
        'PSR' => 'piyawan@lannaagro.com',
        'PI' => 'Piphop@Lannaagro.com',
        'PK_STAFF' => 'PK_Staff@Lannaagro.com',
        'SAN' => 'Supaporn.a@Lannaagro.com',
        'SJB' => 'Sarawut@Lannaagro.com',
        'SPT' => 'Supakchaya@Lannaagro.com',
        'PFIQF_STAFF' => 'PfIQF_Staff@Lannaagro.com'
    ],
    'emailtestlist' => [
        'YS' => 'Yupa@lannaagro.com',
    ]
];