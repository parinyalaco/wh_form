<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Unit;

class WhActMaterialFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [            
            'name' => $this->faker->name(),
            'mat_desc' => $this->faker->name(),
            'unit_id' => rand(1, Unit::count()),
        ];
    }
}
