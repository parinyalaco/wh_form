<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Material;
use App\Models\Vendor;

class ProductInputFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'input_date' => $this->faker->date('Y-m-d'),
            'material_id' => rand(1, Material::count()),
            'vendor_id' => rand(1, Vendor::count()),
            'po_quantity' => $this->faker->randomDigit(),
            'input_real' => $this->faker->randomDigit(),
            'input_list' => $this->faker->randomNumber(1),
            'input_random' => $this->faker->randomNumber(1),
            'note' => $this->faker->paragraph(),
        ];
    }
}
