<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Unit;
use App\Models\ProductType;

class MaterialFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'po_shot_text' => $this->faker->name(),
            'unit_id' => rand(1, Unit::count()),
            'product_type_id' => rand(1, ProductType::count()),
        ];
    }
}
