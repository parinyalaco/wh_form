import "@toast-ui/calendar/toastui-calendar.css";
import "tui-date-picker/dist/tui-date-picker.min.css";
import "tui-time-picker/dist/tui-time-picker.min.css";

import React from "react";
import ReactDOM from "react-dom";

import { App } from "./App";

ReactDOM.render(
    <React.StrictMode>
        <App view="month" />
        {/* <App /> */}
    </React.StrictMode>,
    document.getElementById("app")
);
