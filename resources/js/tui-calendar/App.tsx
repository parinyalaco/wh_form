/* eslint-disable no-console */
import "./app.css";

import type {
    EventObject,
    ExternalEventTypes,
    Options,
} from "@toast-ui/calendar";

import type { ChangeEvent, MouseEvent } from "react";
import { useCallback, useEffect, useRef, useState } from "react";

import Calendar from "./src";
import { theme } from "./theme";
import { addDate, addHours, subtractDate } from "./utils";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-icons/font/bootstrap-icons.css";

type ViewType = "month" | "week" | "day";

const viewModeOptions = [
    {
        title: "Monthly",
        value: "month",
    },
    {
        title: "Weekly",
        value: "week",
    },
    {
        title: "Daily",
        value: "day",
    },
];

export function App({ view }: { view: ViewType }) {

    // const initialCalendars: Options["calendars"] = [
    //     {
    //         id: "0",
    //         name: "stand by",
    //         backgroundColor: "#eeeeee",
    //         borderColor: "#000000",
    //         dragBackgroundColor: "#eeeeee",
    //     },
    //     {
    //         id: "1",
    //         name: "L2 83-6175",
    //         backgroundColor: "#ff90f8",
    //         borderColor: "#000000",
    //         dragBackgroundColor: "#ff90f8",
    //     },
    //     {
    //         id: "2",
    //         name: "L1 83-7518",
    //         backgroundColor: "#949bff",
    //         borderColor: "#000000",
    //         dragBackgroundColor: "#949bff",
    //     },
    //     {
    //         id: "3",
    //         name: "L3 83-6176",
    //         backgroundColor: "#fae99d",
    //         borderColor: "#000000",
    //         dragBackgroundColor: "#fae99d",
    //     },
    //     {
    //         id: "4",
    //         name: "L4 83-6177",
    //         backgroundColor: "#81ff93",
    //         borderColor: "#000000",
    //         dragBackgroundColor: "#81ff93",
    //     },
    //     {
    //         id: "5",
    //         name: "L5 83-9441",
    //         backgroundColor: "#81cefd",
    //         borderColor: "#000000",
    //         dragBackgroundColor: "#81cefd",
    //     },
    //     {
    //         id: "99",
    //         name: "รถเช่า",
    //         backgroundColor: "#ff7979",
    //         borderColor: "#000000",
    //         dragBackgroundColor: "#ff7979",
    //     },
    // ];

    const [initialCalendars, setTrucks] = useState([]);
    const getTrucks = () => {
        fetch("https://lacopm5.lannaagro.com/wh_form/api/trucks", {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
            },
        })
            .then(function (response) {
                console.log(response);
                return response.json();
            })
            .then(function (myJson) {
                console.log(myJson);
                setTrucks(myJson);
            });
    };
    useEffect(() => {
        getTrucks();
    }, []);



    const [initialEvents, setData] = useState([]);
    const getData = () => {
        fetch("https://lacopm5.lannaagro.com/wh_form/api/calendar", {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
            },
        })
            .then(function (response) {
                console.log(response);
                return response.json();
            })
            .then(function (myJson) {
                console.log(myJson);
                setData(myJson);
            });
    };
    useEffect(() => {
        getData();
    }, []);

    const [selectedView, setSelectedView] = useState(view);
    const calendarRef = useRef<typeof Calendar>(null);
    const [selectedDateRangeText, setSelectedDateRangeText] = useState("");

    const onAfterRenderEvent: ExternalEventTypes["afterRenderEvent"] = (
        res
    ) => {
        console.group("onAfterRenderEvent");
        console.log("Event Info : ", res.id);
        console.groupEnd();
    };

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
      const getCalInstance = useCallback(() => calendarRef.current?.getInstance?.(), []);

    const updateRenderRangeText = useCallback(() => {
        const calInstance = getCalInstance();
        if (!calInstance) {
            setSelectedDateRangeText("");
        }

        const viewName = calInstance.getViewName();
        const calDate = calInstance.getDate();
        const rangeStart = calInstance.getDateRangeStart();
        const rangeEnd = calInstance.getDateRangeEnd();

        let year = calDate.getFullYear();
        let month = calDate.getMonth() + 1;
        let date = calDate.getDate();
        let dateRangeText: string;

        switch (viewName) {
            case "month": {
                dateRangeText = `${year}-${month}`;
                break;
            }

            case "week": {
                year = rangeStart.getFullYear();
                month = rangeStart.getMonth() + 1;
                date = rangeStart.getDate();
                const endMonth = rangeEnd.getMonth() + 1;
                const endDate = rangeEnd.getDate();

                const start = `${year}-${month < 10 ? "0" : ""}${month}-${
                    date < 10 ? "0" : ""
                }${date}`;
                const end = `${year}-${endMonth < 10 ? "0" : ""}${endMonth}-${
                    endDate < 10 ? "0" : ""
                }${endDate}`;
                dateRangeText = `${start} ~ ${end}`;
                break;
            }
            default:
                dateRangeText = `${year}-${month}-${date}`;
        }

        setSelectedDateRangeText(dateRangeText);
    }, [getCalInstance]);

    useEffect(() => {
        setSelectedView(view);
    }, [view]);
    const onChangeSelect = (ev: ChangeEvent<HTMLSelectElement>) => {
        setSelectedView(ev.target.value as ViewType);
    };

    const onClickDayName: ExternalEventTypes["clickDayName"] = (res) => {
        console.group("onClickDayName");
        console.log("Date : ", res.date);
        console.groupEnd();
    };

    const onClickNavi = (ev: MouseEvent<HTMLButtonElement>) => {
        if ((ev.target as HTMLButtonElement).tagName === "BUTTON") {
            const button = ev.target as HTMLButtonElement;
            const actionName = (
                button.getAttribute("data-action") ?? "month"
            ).replace("move-", "");
            getCalInstance()[actionName]();
            updateRenderRangeText();
        }
    };

    const onClickEvent: ExternalEventTypes["clickEvent"] = (res) => {
        console.group("onClickEvent");
        console.log("MouseEvent : ", res.nativeEvent);
        console.log("Event Info : ", res.event);
        console.groupEnd();
    };

    const onBeforeDeleteEvent: ExternalEventTypes["beforeDeleteEvent"] = (
        res
    ) => {
        console.group("onBeforeDeleteEvent");
        console.log("Event Info : ", res);
        console.groupEnd();

        const { id, calendarId } = res;

        fetch("https://lacopm5.lannaagro.com/wh_form/api/delete", {
            method: "POST",
            body: JSON.stringify(res),
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
        }).then(function (response) {
            response.json().then(function (resp) {
                console.log(resp);
            });
        });

        getCalInstance().deleteEvent(id, calendarId);
    };

    const onClickTimezonesCollapseBtn: ExternalEventTypes["clickTimezonesCollapseBtn"] =
        (timezoneCollapsed) => {
            console.group("onClickTimezonesCollapseBtn");
            console.log("Is Timezone Collapsed?: ", timezoneCollapsed);
            console.groupEnd();

            const newTheme = {
                "week.daygridLeft.width": "100px",
                "week.timegridLeft.width": "100px",
            };

            getCalInstance().setTheme(newTheme);
        };

    const onBeforeUpdateEvent: ExternalEventTypes["beforeUpdateEvent"] = (
        updateData
    ) => {
        console.group("onBeforeUpdateEvent");
        console.log(updateData);
        console.groupEnd();

        const targetEvent = updateData.event;
        const changes = { ...updateData.changes };

        fetch("http://lacopm5.lannaagro.com/wh_form/api/edit", {
            method: "POST",
            body: JSON.stringify(updateData),
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
        }).then(function (response) {
            response.json().then(function (resp) {
                console.log(resp);
            });
        });


        getCalInstance().updateEvent(
            targetEvent.id,
            targetEvent.calendarId,
            changes
        );
    };
    const onBeforeCreateEvent: ExternalEventTypes["beforeCreateEvent"] = (
        eventData
    ) => {
        const event = {
            calendarId: eventData.calendarId || "",
            id: String(Math.random()),
            title: eventData.title,
            isAllday: eventData.isAllday,
            start: eventData.start,
            end: eventData.end,
            category: eventData.isAllday ? "allday" : "time",
            dueDateClass: "",
            location: eventData.location,
            state: eventData.state,
            isPrivate: eventData.isPrivate,
        };
        console.log("onBeforeCreateEvent Info : ", eventData);

        fetch("http://lacopm5.lannaagro.com/wh_form/api/calendar", {
            method: "POST",
            body: JSON.stringify(event),
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
        }).then(function (response) {
            response.json().then(function (resp) {
                console.log(resp);
            });
        });

        getCalInstance().createEvents([event]);
    };

    return (
        <div className="m-3">


            <div className="d-flex justify-content m-3">
                <div className="form-outline me-3">
                    <select
                        onChange={onChangeSelect}
                        value={selectedView}
                        className="form-select border border-secondary rounded"
                    >
                        {viewModeOptions.map((option, index) => (
                            <option value={option.value} key={index}>
                                {option.title}
                            </option>
                        ))}
                    </select>
                </div>

                <div
                    className="btn-group"
                    role="group"
                    aria-label="Basic radio toggle button group"
                >
                    {/* <button
                        type="button"
                        className="btn btn-outline-secondary move-day bi bi-caret-left-fill"
                        data-action="move-prev"
                        onClick={onClickNavi}
                    ></button> */}
                       <button
                        type="button"
                        className="btn btn-outline-secondary"
                        data-action="move-prev"
                        onClick={onClickNavi}
                    >{'<'}</button>


                    <button
                        type="button"
                        className="btn btn-outline-secondary move-today"
                        data-action="move-today"
                        onClick={onClickNavi}
                    >
                        วันนี้
                    </button>

                    {/* <button
                        type="button"
                        className="btn btn-outline-secondary move-day bi bi-caret-right-fill"
                        data-action="move-next"
                        onClick={onClickNavi}
                    ></button> */}

                    <button
                        type="button"
                        className="btn btn-outline-secondary"
                        data-action="move-next"
                        onClick={onClickNavi}
                    >{'>'}</button>
                </div>
                <span className="render-range">
                    <h4>{selectedDateRangeText}</h4>
                </span>
            </div>

            <Calendar
                height="3000px"
                calendars={initialCalendars}
                month={{ startDayOfWeek: 0,
                    narrowWeekend: true,
                    visibleEventCount: 20,
                    // visibleWeeksCount: 5,
                }}
                events={initialEvents}
                template={{
                    milestone(event) {
                        return `<span style="color: #fff; background-color: ${event.backgroundColor};">${event.title}</span>`;
                    },
                    allday(event) {
                        return `[All day] ${event.title}`;
                    },
                }}
                theme={theme}
                timezone={{
                    zones: [
                        {
                            timezoneName: "Asia/Bangkok",
                            displayLabel: "Bangkok",
                            tooltip: "UTC+07:00",
                        },
                        {
                            timezoneName: "Asia/Tokyo",
                            displayLabel: "Tokyo",
                            tooltip: "UTC+09:00",
                        },
                    ],
                }}
                useDetailPopup={true}
                useFormPopup={true}
                view={selectedView}
                week={{
                    timezonesCollapsed: false,
                    eventView: true,
                    taskView: true,
                    // workweek: true,
                }}
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                ref={calendarRef}
                onAfterRenderEvent={onAfterRenderEvent}
                onBeforeDeleteEvent={onBeforeDeleteEvent}
                onClickDayname={onClickDayName}
                onClickEvent={onClickEvent}
                onClickTimezonesCollapseBtn={onClickTimezonesCollapseBtn}
                onBeforeUpdateEvent={onBeforeUpdateEvent}
                onBeforeCreateEvent={onBeforeCreateEvent}
            />
        </div>
    );
}
