@extends('layouts.app-master')

@push('styles')
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Prompt:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap');

        body,
        html {
            height: 100%;
            margin: 0;
            font-family: "Prompt", sans-serif;
        }

        .tab {
            overflow: hidden;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2);
            border-radius: 5px 5px 0px 0px;
            background: white;
        }

        .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
            font-size: 17px;
        }

        .tab button:hover {
            background-color: #0a9443;
            color: #fff;
        }

        .tab button.active {
            background-color: #0a9443;
            color: #fff;
        }

        .tabcontent {
            display: none;
            padding: 5px 12px;
            background-color: #fff;
            border-radius: 0px 0px 5px 5px;
            border-top: 2px solid #ccc;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2);
        }

        .fa-trash-alt {
            color: black;
            transition: color 0.1s;
        }

        .fa-trash-alt:hover {
            color: red;
            cursor: pointer;
        }
    </style>
@endpush

@section('content')
    <div class="app-content p-md-4" style="font-size: 15px">
        <div class="app-container-xl">
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            @if ($message = Session::get('error'))
                <div class="alert alert-warning">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="row my-2">
                <div class="col">
                    <h2>งาน WHRM</h2>
                </div>
            </div>


            <div class="tab">
                <button id="bean-button" class="tablinks" onclick="openTab('bean')" style="width: 200px">
                    <h5>
                        ถั่วแระ</h5>
                </button>
                <button id="banana-button" class="tablinks" onclick="openTab('banana')" style="width: 200px">
                    <h5>
                        กล้วย</h5>
                </button>
            </div>
            <div id="bean" class="tabcontent">
                @include('TEST.beanTabContent')
            </div>

            <div id="banana" class="tabcontent">
                @include('TEST.bananaTabContent')
            </div>
        </div>

    </div>
@endsection

@push('script')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            var lastOpenedTab = localStorage.getItem('lastOpenedTab');
            if (lastOpenedTab) {
                openTab(lastOpenedTab);
            }
        });

        function openTab(tabName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(tabName).style.display = "block";
            document.getElementById(tabName + "-button").className += " active";
            localStorage.setItem('lastOpenedTab', tabName);
        }

        $(document).ready(function() {
            var maxFields = 5; // Maximum number of input fields for uploading images

            $('.add_button').click(function() {
                var wrapperId = '.field_wrapper_' + $(this).data('id');
                var fieldCount = $(wrapperId + ' input[type="file"]').length;

                if (fieldCount < maxFields) {
                    var newFieldHTML = '<div class="form-control mt-2"><input type="file" name="images[' +
                        fieldCount +
                        ']" accept="image/*"/><a href="javascript:void(0);" class="remove_button">ลบ</a></div>';
                    $(wrapperId).append(newFieldHTML);
                }
            });

            $(document).on('click', '.remove_button', function(e) {
                e.preventDefault();
                $(this).parent('div').remove();
            });
        });
    </script>

    <script type="text/javascript">
        function myFunction(a) {
            console.log(a);
            if (a == '1') {
                document.getElementById('bean_st_date').style.display = "block";
                document.getElementById('bean_date_to').style.display = "none";
                document.getElementById('bean_ed_date').style.display = "none";
                document.getElementById('bean_month_date').style.display = "none";
                document.getElementById('bean_crop_id').style.display = "none";
                document.getElementById('bean_crop_name').style.display = "none";
            } else if (a == '3') {
                document.getElementById('bean_st_date').style.display = "block";
                document.getElementById('bean_date_to').style.display = "block";
                document.getElementById('bean_ed_date').style.display = "block";
                document.getElementById('bean_month_date').style.display = "none";
                document.getElementById('bean_crop_id').style.display = "block";
                document.getElementById('bean_crop_name').style.display = "block";
            } else {
                document.getElementById('bean_st_date').style.display = "block";
                document.getElementById('bean_date_to').style.display = "block";
                document.getElementById('bean_ed_date').style.display = "block";
                document.getElementById('bean_month_date').style.display = "none";
                document.getElementById('bean_crop_id').style.display = "none";
                document.getElementById('bean_crop_name').style.display = "none";
            }
        };


        function bananaReportFunction(e) {
            console.log(e);
            if (e == '1') {
                document.getElementById('banana_st_date').style.display = "block";
                document.getElementById('banana_date_to').style.display = "none";
                document.getElementById('banana_ed_date').style.display = "none";
                document.getElementById('banana_month_date').style.display = "none";
                document.getElementById('banana_crop_id').style.display = "none";
                document.getElementById('banana_crop_name').style.display = "none";
            } else if (e == '3') {
                document.getElementById('banana_st_date').style.display = "block";
                document.getElementById('banana_date_to').style.display = "block";
                document.getElementById('banana_ed_date').style.display = "block";
                document.getElementById('banana_month_date').style.display = "none";
                document.getElementById('banana_crop_id').style.display = "block";
                document.getElementById('banana_crop_name').style.display = "block";
            } else {
                document.getElementById('banana_st_date').style.display = "block";
                document.getElementById('banana_date_to').style.display = "block";
                document.getElementById('banana_ed_date').style.display = "block";
                document.getElementById('banana_month_date').style.display = "none";
                document.getElementById('banana_crop_id').style.display = "none";
                document.getElementById('banana_crop_name').style.display = "none";
            }
        };
    </script>



    <script type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            var max_fields = 5;

            // Event delegation to handle dynamically created buttons
            $(document).on('click', '.add_field_button', function(e) {
                e.preventDefault();

                var rmIssueId = $(this).data('rm-issue-id');
                var wrapper = $('.input_fields_wrap[data-rm-issue-id="' + rmIssueId + '"]');
                var x = wrapper.children('div').length;

                if (x < max_fields) { // max input box allowed
                    x++; // text box increment
                    wrapper.append(
                        '<div class="my-1"><input type="file" name="issue_img[' + x +
                        ']" class="form-control"/>' +
                        ' <a href="#" class="remove_field">ลบ</a></div>'
                    ); // add input boxes.
                }
            });

            $(document).on('click', '.remove_field', function(e) {
                e.preventDefault();
                $(this).parent('div').remove();
            });
        });
    </script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var buttons = document.querySelectorAll('.issue-daily-button');

            buttons.forEach(function(button) {
                var date = button.getAttribute('data-date');
                var rawMaterialId = button.getAttribute('data-raw-material-id');
                var url = `/check_issue_daily/${date}/${rawMaterialId}`;

                fetch(url)
                    .then(response => response.json())
                    .then(data => {
                        if (data.exists) {
                            button.style.display = 'inline-block';
                        }
                    })
                    .catch(error => console.error('Error:', error));
            });
        });
    </script>
@endpush
