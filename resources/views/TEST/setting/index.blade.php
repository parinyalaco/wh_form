@extends('layouts.app-master')

@push('styles')
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Prompt:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap');

        body,
        html {
            height: 100%;
            margin: 0;
            font-family: "Prompt", sans-serif;
        }
    </style>
@endpush

@section('content')
    <div class="container-fluid mt-2">

        <h4>WhRm setting</h4>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-warning">
                <p>{{ $message }}</p>
            </div>
        @endif

        {{-- Edit --}}
        <button type="button" class="btn app-btn-secondary my-2" data-toggle="modal" data-target="#BrokerAreaManage">
            <i class="fas fa-chalkboard-teacher "></i>&nbsp; จัดการ broker Area
        </button>

        <!--Report Modal -->
        <div class="modal fade" id="BrokerAreaManage" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">จัดการ broker Area</h5>
                        <button type="button" class="close btn btn-danger" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">X</span>
                        </button>
                    </div>
                    <form method="POST" action="{{ route('whrmSetting.store') }}" accept-charset="UTF-8"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col">
                                    <label>Brokers</label>
                                    <select name="broker" class="form-select">
                                        @foreach ($rmBrokers as $rmBrokerId => $rmBrokerName)
                                            <option value="{{ $rmBrokerId }}">
                                                {{ $rmBrokerName }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <label>Areas</label>
                                    <select name="area" class="form-select">
                                        @foreach ($rmAears as $rmAearId => $rmAearName)
                                            <option value="{{ $rmAearId }}">
                                                {{ $rmAearName }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                            <button type="submit" class="btn app-btn-primary">บันทึก</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{-- Add Broker --}}
        <button type="button" class="btn app-btn-secondary my-2" data-toggle="modal" data-target="#AddBroker">
            <i class="	fab fa-black-tie"></i>&nbsp; เพิ่ม broker
        </button>

        <!--Report Modal -->
        <div class="modal fade" id="AddBroker" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">เพิ่ม broker </h5>
                        <button type="button" class="close btn btn-danger" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">X</span>
                        </button>
                    </div>
                    <form method="POST" action="{{ route('whrmSettingAddBroker') }}" accept-charset="UTF-8"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <label>ชื่อ</label>
                            <input type="text" name="name" class="form-control" required>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                            <button type="submit" class="btn app-btn-primary">บันทึก</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{-- Add Area --}}
        <button type="button" class="btn app-btn-secondary my-2" data-toggle="modal" data-target="#addArea">
            <i class="	fa fa-map"></i>&nbsp; เพิ่ม Area
        </button>

        <!--Report Modal -->
        <div class="modal fade" id="addArea" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">เพิ่ม Area</h5>
                        <button type="button" class="close btn btn-danger" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">X</span>
                        </button>
                    </div>
                    <form method="POST" action="{{ route('whrmSettingAddArea') }}" accept-charset="UTF-8"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <label>ชื่อ</label>
                            <input type="text" name="name" class="form-control" required>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                            <button type="submit" class="btn app-btn-primary">บันทึก</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @if (isset($rm_broker_areas))
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>ชื่อ Broker </th>
                        <th>พื้นที่</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rm_broker_areas as $rm_broker_area)
                        <tr>
                            <td>{{ $rm_broker_area->rm_broker->name }}</td>
                            <td>{{ $rm_broker_area->rm_area->name }}</td>
                            <td>
                                {{-- Edit --}}
                                <button type="button" class="btn app-btn-secondary" data-toggle="modal"
                                    data-target="#brokerAreaEdit-{{ $rm_broker_area->id }}">
                                    <i class="fas fa-file-download"></i>&nbsp; จัดการ
                                </button>

                                <!--Report Modal -->
                                <div class="modal fade" id="brokerAreaEdit-{{ $rm_broker_area->id }}" tabindex="-1"
                                    role="dialog" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">จัดการ broker Area</h5>
                                                <button type="button" class="close btn btn-danger" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">X</span>
                                                </button>
                                            </div>
                                            <form method="POST"
                                                action="{{ route('whrmSetting.update', $rm_broker_area->id) }}"
                                                accept-charset="UTF-8" enctype="multipart/form-data">
                                                @csrf
                                                @method('PUT')
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col">
                                                            <label>Brokers</label>
                                                            <select name="broker" class="form-select">
                                                                @foreach ($brokers as $brokerId => $brokerName)
                                                                    <option value="{{ $brokerId }}"
                                                                        @if ($brokerId == $rm_broker_area->rm_broker->id) selected @endif>
                                                                        {{ $brokerName }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label>Areas</label>
                                                            <select name="area" class="form-select">
                                                                @foreach ($areas as $areaId => $areaName)
                                                                    <option value="{{ $areaId }}"
                                                                        @if ($areaId == $rm_broker_area->rm_area->id) selected @endif>
                                                                        {{ $areaName }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">ปิด</button>
                                                    <button type="submit" class="btn app-btn-primary">บันทึก</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>


                            </td>
                        </tr>
                    @endforeach
                </tbody>


            </table>
        @endif
    </div>
@endsection
