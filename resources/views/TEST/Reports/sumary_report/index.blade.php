@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4 container-fulid">
        <h4>Report : สรุปการลงวัตถุดิบ วันที่ : {{ date('d/m/Y', strtotime($sent_date . '+543 year')) }}</h4>
        <hr>

        @if (isset($reports[$rmMat]))
            @include('TEST.Reports.sumary_report.content')
        @endif

        {{-- @if (isset($reports['banana']))
            @include('TEST.Reports.sumary_report.banana')
        @endif --}}
    </div>
@endsection
