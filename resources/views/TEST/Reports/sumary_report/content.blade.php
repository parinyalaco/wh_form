<div class="app-content">
    <h5> สรุปการลงวัตถุดิบ{{ $reports[$rmMat]['type'] }} รับเข้าวันที่
        {{ date('d/m/Y', strtotime($sent_date . '+543 year')) }} </h5>

    <p> 1. RM รับเข้าทั้งหมด {{ number_format($reports[$rmMat]['totelWeight'], 2) }} Kg. </p>
    <p> 2. รถขนส่งเข้าโรงงานทั้งหมด {{ $reports[$rmMat]['car_in'] }} คัน </p>
    <p>- คันแรกถึง LACO เวลา {{ $reports[$rmMat]['first'] }} น.</p>
    <p>- คันสุดท้ายถึง LACO เวลา {{ $reports[$rmMat]['last'] }} น.</p>
    <p>- ลงวัตถุดิบคันสุดท้ายเสร็จเวลา {{ $reports[$rmMat]['finish'] }} น.</p>
    <hr>
    <div class="row">
        <div class="col">
            <strong>ช่วงเวลารถเข้า</strong>
            <table class="table table-bordered ">
                <thead style="background-color: #B1D3F5">
                    <tr style="text-align: center">
                        <th>เวลา</th>
                        <th>วันที่ {{ date('d/m/Y', strtotime($sent_date . '+543 year')) }}</th>
                        <th>น้ำหนักวัตถุดิบสุทธิ</th>
                    </tr>
                </thead>
                @foreach ($reports[$rmMat]['entryTime'] as $key => $entryTime)
                    <tr @if ($key == 'Grand Total') style="background-color:#B1D3F5 " @endif>
                        <th>{{ $key }}</th>
                        <th style="text-align: right">{{ $entryTime['count'] }}</td>
                        <th style="text-align: right">{{ number_format($entryTime['weight'], 2) }}</th>

                    </tr>
                @endforeach
            </table>
        </div>
        <div class="col">
            <strong>แผนภาพ</strong>
            <canvas id="beanChart1" style="width:100%; height:300px"></canvas>
        </div>
    </div>
    <hr>
    <strong>สรุปการลงวัตถุดิบ</strong>
    <table class="table table-bordered">

        <tr style="text-align: center">
            <th style="vertical-align: middle ;background-color: #F5E4D9" rowspan="3">วันที่</th>
            <th colspan="12" style="background-color: #abf0af">ประเภทการลงวัตถุดิบ</th>
        </tr>

        <tr style="text-align: center">
            @foreach ($reports[$rmMat]['typeReceive'] as $type => $item)
                @switch($type)
                    @case('พนักงานยก')
                        <th colspan="4" style="background-color: #F6BC96">
                            {{ $type }} นาที</th>
                    @break

                    @case('เครื่องเรียงกระสอบ')
                        <th colspan="4" style="background-color: #B1D3F5">
                            {{ $type }} นาที</th>
                    @break

                    @case('total')
                        <th colspan="4" style="background-color: #eed6fc">
                            {{ $type }} นาที</th>
                    @break

                    @default
                @endswitch
            @endforeach
        </tr>
        <tr style="text-align: center">
            @foreach ($reports[$rmMat]['typeReceive'] as $type => $item)
                @foreach ($reports[$rmMat]['typeReceive'][$type] as $time => $item)
                    @switch($type)
                        @case('พนักงานยก')
                            <th style="background-color: #F6BC96">
                                {{ $time }} นาที</th>
                        @break

                        @case('เครื่องเรียงกระสอบ')
                            <th style="background-color: #B1D3F5">
                                {{ $time }} นาที</th>
                        @break

                        @case('total')
                            <th style="background-color: #eed6fc">
                                {{ $time }} นาที</th>
                        @break

                        @default
                    @endswitch
                @endforeach
            @endforeach
        </tr>
        <tr>
            <th style="text-align: center;">
                {{ date('d/m/Y', strtotime($sent_date . '+543 year')) }}</th>
            @foreach ($reports[$rmMat]['typeReceive'] as $type => $ite1m)
                @foreach ($reports[$rmMat]['typeReceive'][$type] as $time => $ite2m)
                    @switch($type)
                        @case('พนักงานยก')
                            <th style="text-align: right">
                                {{ $reports[$rmMat]['typeReceive'][$type][$time] }} </th>
                        @break

                        @case('เครื่องเรียงกระสอบ')
                            <th style="text-align: right">
                                {{ $reports[$rmMat]['typeReceive'][$type][$time] }} </th>
                        @break

                        @case('total')
                            <th style="text-align: right">
                                {{ $reports[$rmMat]['typeReceive'][$type][$time] }} </th>
                        @break

                        @default
                    @endswitch
                @endforeach
            @endforeach
        </tr>
    </table>
    <hr>

    <div class="row">
        <div class="col">
            @if (!empty($reports[$rmMat]['dailyIssue']))
                <strong>ปัญหาที่พบ ประจำวัน</strong>

                <table class="table table-bordered">

                    <thead>
                        <tr style="background-color: #f7cfcf">
                            <th style="width:200px">วันที่</th>
                            <th style="text-align: center">ปัญหาที่พบ</th>
                        </tr>
                    </thead>
                    <tr>
                        <td>{{ $reports[$rmMat]['dailyIssue']['issue_date'] }}</td>
                        <td>{{ $reports[$rmMat]['dailyIssue']['detail'] }}


                            @if (!empty($reports[$rmMat]['dailyIssue']['imgs']))
                                <hr>
                                @foreach ($reports[$rmMat]['dailyIssue']['imgs'] as $img)
                                    <img src="{{ url('show_issue_img/' . $img) }}" alt="Issue Image" draggable="false"
                                        width="500px" class="m-1">
                                @endforeach
                            @endif
                        </td>
                    </tr>
                </table>
            @endif
        </div>
        <div class="col">
            @if (!empty($reports[$rmMat]['issue']))
                <strong>วัตถุดิบที่มีปัญหา</strong>

                <table class="table table-bordered">
                    <thead style="background-color: #B1D3F5">

                        <tr style="background-color: #f7cfcf">
                            <th style="width:200px">SAP No.</th>
                            <th style="width:100px">ทะเบียนรถ</th>
                            <th style="text-align: center">ปัญหาที่พบ</th>
                        </tr>
                    </thead>

                    @foreach ($reports[$rmMat]['issue'] as $key => $item)
                        <tr>
                            <td>{{ $item['sap_no'] }}</td>
                            <td>{{ $item['car_name'] }}</td>
                            <td>
                                @if (!empty($item['sub_issues']))
                                    @foreach ($item['sub_issues'] as $sub_issue)
                                        {{ $sub_issue['detail'] }}
                                        <br>
                                        @if (!empty($sub_issue['imgs']))
                                            @foreach ($sub_issue['imgs'] as $img)
                                                <img src="{{ url('show_issue_img/' . $img) }}" alt="Issue Image"
                                                    draggable="false" width="500px" class="m-1">
                                            @endforeach
                                            <hr>
                                        @endif
                                    @endforeach
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
            @endif
        </div>
    </div>

</div>

@php
    $tb_receipt_gp = [
        'พนักงานยก' => array_values($reports[$rmMat]['typeReceive']['พนักงานยก']),
        'เครื่องเรียงกระสอบ' => array_values($reports[$rmMat]['typeReceive']['เครื่องเรียงกระสอบ']),
        'total' => array_values($reports[$rmMat]['typeReceive']['total']),
    ];
    $col_rang = array_keys($reports[$rmMat]['typeReceive']['พนักงานยก']);
@endphp

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels"></script>

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels"></script>

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            const chk_arr1 = <?php echo json_encode($tb_receipt_gp); ?>;
            const col_rang = <?php echo json_encode($col_rang); ?>;

            const maxYValue = Math.max(...chk_arr1['พนักงานยก'], ...chk_arr1['เครื่องเรียงกระสอบ'], ...chk_arr1[
                'total']);

            new Chart("beanChart1", {
                type: 'bar',
                data: {
                    labels: col_rang,
                    datasets: [{
                            label: 'พนักงานยก',
                            data: chk_arr1['พนักงานยก'],
                            borderColor: '#000000',
                            backgroundColor: '#F6BC96',
                        },
                        {
                            label: 'เครื่องเรียงกระสอบ',
                            data: chk_arr1['เครื่องเรียงกระสอบ'],
                            borderColor: '#000000',
                            backgroundColor: '#B1D3F5',
                        },
                        {
                            label: 'total',
                            data: chk_arr1['total'],
                            borderColor: '#000000',
                            backgroundColor: '#eed6fc',
                        }
                    ]
                },
                options: {
                    responsive: true,
                    plugins: {
                        legend: {
                            display: true,
                            position: 'bottom' // กำหนดตำแหน่งของ Legend ให้อยู่ด้านล่าง
                        },
                        datalabels: {
                            anchor: 'end',
                            align: 'top',
                            formatter: (value, context) => value,
                            font: {
                                weight: 'bold'
                            }
                        }
                    },
                    scales: {
                        y: {
                            beginAtZero: true,
                            suggestedMax: maxYValue +
                                2 // กำหนดให้แกน y แสดงค่าสูงสุดที่มากกว่าค่าที่แสดงอยู่สองค่า
                        }
                    }
                },
                plugins: [ChartDataLabels]
            });
        });
    </script>
@endpush
