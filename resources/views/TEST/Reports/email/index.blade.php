@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4 container-fulid">
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-warning">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="row">
            <div class="col">
                <h4>ตัวอย่าง E-Mail : สรุปการลงวัตถุดิบ วันที่ : {{ date('d/m/Y', strtotime($sent_date . '+543 year')) }}
                </h4>

            </div>
            <div class="col-auto">

                <a class="btn app-btn-secondary" onClick="return confirm('คุณต้องการส่งเมลล์ใช่รือไม่ ?');"
                    href="{{ route('whRmEmail_send_mail', [$rmMat, $sent_date]) }}">
                    <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;
                    ส่ง E-Mail
                </a>
            </div>
        </div>
        <hr>
        @if (isset($reports[$rmMat]))
            @include('TEST.Reports.sumary_report.content')
        @endif

    </div>
@endsection
