<div class="row">
    <div class="col">
        <h3 class="mt-2">การรับ-ถั่วแระ</h3>
    </div>
    <div class="col-auto">
        <div class="row my-2">
            <div class="col-auto">
                <a class="btn app-btn-secondary" data-toggle="modal" data-target="#beanModalUpload">
                    <i class="fas fa-file-upload"></i>&nbsp;
                    Upload Excel ถั่วแระ
                </a>

                <div class="modal fade" id="beanModalUpload">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-header">
                                <h4 class="modal-title"> Import ไฟล์ Xlsx ของถั่ว</h4>
                                <button type="button" class="close btn btn-close" data-dismiss="modal">
                                </button>
                            </div>
                            <div class="modal-body">
                                <form enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="input-group my-2">
                                        <input type="file" name="file_sap" class="form-control" accept=".xlsx"
                                            required>
                                    </div>
                            </div>

                            <div class="modal-footer">

                                <input type="button" class="btn btn-success" value="Import Sap"
                                    onClick="this.form.action='{{ route('upload_whrm_sap') }}'; submit()">

                                <input type="button" class="btn btn-warning" value="Import Manual"
                                    onClick="this.form.action='{{ route('upload_whrm_manual') }}';submit()">
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <button type="button" class="btn app-btn-secondary" data-toggle="modal" data-target="#BeanReportModal">
                    <i class="fas fa-file-download"></i>&nbsp; Report
                </button>

                <!--Report Modal -->
                <div class="modal fade" id="BeanReportModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">รายงาน การรับ-ถั่วแระ</h5>
                                <button type="button" class="close btn btn-danger" data-dismiss="modal"
                                    aria-label="Close">
                                    <span aria-hidden="true">X</span>
                                </button>
                            </div>
                            <form method="GET" action="{{ route('sumary_report_rmMat', 'bean') }}"
                                accept-charset="UTF-8" class="table-search-form row gx-1 align-items-center"
                                role="search" target="_blank" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">

                                    <label>ประเภทรายงาน :</label>

                                    <select name="report_id" class="form-select col" onchange="myFunction(this.value)"
                                        required>
                                        <option value="" selected>====
                                            เลือกประเภทรายงาน ====</option>
                                        <option value="1">1. สรุปการลงวัตถุดิบถั่วแระ
                                        </option>
                                        <option value="2">2. Export Manual</option>
                                        <option value="3">3. ข้อมูลปิดครอบ </option>

                                    </select>
                                    <label> วันที่ :</label>

                                    <input type="date" class="form-control col" id="bean_st_date" name="st_date"
                                        value="{{ date('Y-m-d') }}">
                                    <input type="month" class="form-control col" id="bean_month_date"
                                        name="month_date" value="{{ date('Y-m') }}" style="display: none">

                                    <div id="bean_date_to">
                                        ถึงวันที่ :
                                    </div>

                                    <input type="date" class="form-control col" id="bean_ed_date" name="ed_date"
                                        value="{{ date('Y-m-d') }}">

                                    <div id="bean_tr_crop">

                                        <div id="bean_crop_name">Crop : </div>
                                        </th>

                                        <select id="bean_crop_id" name="crop_id" class="form-select">
                                            <option value="" selected>==== เลือก Crop
                                                ====</option>
                                            @foreach ($crops as $key)
                                                <option value="{{ $key->id }}">
                                                    {{ $key->details }}</option>
                                            @endforeach
                                        </select>

                                    </div>


                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                                    <button type="submit" class="btn app-btn-primary">Report</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <button type="button" class="btn app-btn-secondary" data-toggle="modal"
                    data-target="#BeanEmailModal">
                    <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;
                    Sent E-Mail
                </button>

                <!-- Send Mail Modal -->
                <div class="modal fade" id="BeanEmailModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">ส่ง E-Mail ของ การรับ-ถั่วแระ</h5>
                                <button type="button" class="close btn btn-danger" data-dismiss="modal"
                                    aria-label="Close">
                                    <span aria-hidden="true">X</span>
                                </button>
                            </div>

                            <form method="GET" action="{{ route('whRmEmail_example_mail', ['bean']) }}"
                                accept-charset="UTF-8" class="table-search-form row gx-1 align-items-center"
                                role="search" target="_blank">
                                <div class="modal-body">

                                    <label> วันที่ :</label>

                                    <input type="date" class="form-control col" id="sent_date" name="sent_date"
                                        value="{{ date('Y-m-d') }}">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-dismiss="modal">ปิด</button>
                                    <button type="submit" class="btn app-btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<hr>

<div class="row">
    <div class="col">
        <form method="GET">
            @csrf
            <div class="form-group row mb-3">
                <label for="name" class="col-auto form-label">วันที่ </label>
                <div class="col-auto">
                    <input type="date" class="form-control" id="date_from" name="date_from"
                        value="{{ date('Y-m-d', strtotime($date_from)) }}">
                </div>
                <label for="name" class="col-auto form-label">ถึงวันที่ </label>
                <div class="col-auto">
                    <input type="date" class="form-control" id="date_date" name="date_to"
                        value="{{ date('Y-m-d', strtotime($date_to)) }}">
                </div>

                <div class="col">
                    <input type="button" class="btn btn-success" value="Search"
                        onClick="this.form.action='#'; submit()">

                    <input type="button" class="btn btn-warning" value="Export Manual"
                        onClick="this.form.action='{{ route('test_export_manual_bean') }}'; submit()">
                </div>

            </div>
        </form>
    </div>


</div>

<table class="table my-2">
    <thead>
        <tr>
            <td></td>
            <th style="width:200px">วันที่</th>
            <th>No.</th>
            <th>Sap No.</th>
            <th>ทะเบียนรถ</th>
            <th>เวลาเข้า</th>
            <th>ลงเสร็จ</th>
            <th>ใช้เวลา</th>
            <th>จำนวน</th>
            <th>น้ำหนักสุทธิ</th>
            <th>หมายเหตุ</th>
            <th style="width: 500px">ปัญหา</th>
        </tr>
    </thead>
    <tbody>

        @foreach ($rm_manuals as $index => $rm_manual)
            @if (isset($rm_manual->rm_sap))
                @if ($index == 0 || $rm_manual->manual_date != $rm_manuals[$index - 1]->manual_date)
                    <tr @if (empty($rm_manual->file_excel_id)) class="bg-danger" @endif>
                        <td rowspan="{{ $rm_manuals->where('manual_date', $rm_manual->manual_date)->count() + 1 }}">
                            <form action="{{ route('delete_rm', [$rm_manual->manual_date]) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn" onclick="confirm('ต้องการลบ ?')"><i
                                        class="far fa-trash-alt"></i></button>
                            </form>
                        </td>

                        <td rowspan="{{ $rm_manuals->where('manual_date', $rm_manual->manual_date)->count() + 1 }}"
                            style="text-align: center">
                            {{ $rm_manual->manual_date }}<br>

                            <button class="btn app-btn-secondary issue-daily-button"
                                data-date="{{ $rm_manual->manual_date }}"
                                data-raw-material-id="{{ $rm_manual->rm_sap->raw_material_id }}" data-toggle="modal"
                                data-target="#BeancreateIssueDailyModal-{{ $rm_manual->manual_date }}"
                                style="display: none;">
                                <i class="fas fa-clipboard"></i>&nbsp;บันทึกปัญหารายวัน
                            </button>

                            <!--Report Modal -->
                            <div class="modal fade" id="BeancreateIssueDailyModal-{{ $rm_manual->manual_date }}"
                                tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">
                                                บันทึกปัญหารายวัน ของถั่ว {{ $rm_manual->manual_date }}
                                            </h5>
                                            <button type="button" class="close btn btn-danger" data-dismiss="modal"
                                                aria-label="Close">
                                                <span aria-hidden="true">X</span>
                                            </button>
                                        </div>
                                        <form method="POST"
                                            action="{{ route('create_issue_daily', [$rm_manual->manual_date, $rm_manual->rm_sap->raw_material_id]) }}"
                                            accept-charset="UTF-8"
                                            class="table-search-form row gx-1 align-items-center"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <div class="modal-body">
                                                <label>รายละเอียด</label>
                                                <input type="text" class="form-control col" name="detail"
                                                    required>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">ปิด</button>
                                                <button type="submit" class="btn app-btn-primary">บันทึก</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </td>
                    </tr>
                @endif

                <tr @if (empty($rm_manual->file_excel_id)) class="bg-danger" @endif>
                    <td>{{ $rm_manual->manual_no }}</td>
                    <td>{{ $rm_manual->sap_no }}</td>
                    <td>{{ $rm_manual->car_name }}</td>
                    <td>
                        {{ !empty($rm_manual->time_to_laco) ? date('H:i:s', strtotime($rm_manual->time_to_laco)) : '' }}
                    </td>
                    <td>{{ !empty($rm_manual->time_end_work) ? date('H:i:s', strtotime($rm_manual->time_end_work)) : '' }}
                    </td>
                    <td>{{ !empty($rm_manual->time_use) ? date('H:i:s', strtotime($rm_manual->time_use)) : '' }}
                    </td>
                    <td>{{ $rm_manual->manual_num }}</td>
                    <td>{{ number_format($rm_manual->manual_weight, 2) }}</td>
                    <td>{{ $rm_manual->note }}</td>
                    <td style="width: 500px">
                        <div class="row">
                            <div class="col">
                                {{ $rm_manual->rm_issue->detail ?? '-' }}
                            </div>
                            <div class="col-auto">
                                <button type="button" class="btn app-btn-secondary mx-2" data-toggle="modal"
                                    data-target="#recordBeanIssueModal{{ $rm_manual->id }}">
                                    <i class="fas fa-clipboard"></i> &nbsp;บันทึกปัญหา
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="recordBeanIssueModal{{ $rm_manual->id }}" tabindex="-1"
                                    role="dialog" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">บันทึกปัญหา</h5>
                                                <button type="button" class="close btn btn-danger"
                                                    data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">X</span>
                                                </button>
                                            </div>
                                            <form action="{{ route('banana_add_new_issue', [$rm_manual->sap_no]) }}"
                                                method="POST" enctype="multipart/form-data">
                                                @csrf
                                                <div class="modal-body">

                                                    <label>ปัญหา</label>
                                                    <input type="text" name="issue" class="form-control"
                                                        required>

                                                    <label>รูปภาพ</label>
                                                    <input type="file" name="issue_banana_img" accept=".png,.jpg"
                                                        class="form-control">

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">ปิด</button>
                                                    <button type="submit" class="btn app-btn-primary">บันทึก</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </td>
                </tr>
            @endif
        @endforeach

    </tbody>
</table>
{{ $rm_manuals->appends(['rm_manual_bean_page' => '', 'date_from' => $date_from, 'date_to' => $date_to])->links() }}

<br>


<div class="row">
    <div class="col">
        @if (isset($bean_issue_dailys))
            <h4>ข้อมูลถั่วรับเข้า รายวัน</h4>
            <table class="table">
                <thead>
                    <tr>
                        <td style="width:20px"></td>
                        <th style="width:125px">วันที่</th>
                        <th>ปัญหาที่พบ</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($bean_issue_dailys as $bean_issue_daily)
                        <tr>
                            <td>
                                <form
                                    action="{{ route('delete_daily_issue', [$bean_issue_daily->issue_date, $bean_issue_daily->raw_material_id]) }}"
                                    method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn" onclick="confirm('ต้องการลบ ?')"><i
                                            class="far fa-trash-alt"></i></button>
                                </form>

                            </td>

                            <td>{{ $bean_issue_daily->issue_date }}</td>
                            <td>{{ $bean_issue_daily->detail }} &nbsp;

                                <button type="button" class="btn " data-toggle="modal"
                                    data-target="#recordBeanDailyIssueEditModal{{ $bean_issue_daily->id }}">
                                    &nbsp; <i class="far fa-edit"></i>
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="recordBeanDailyIssueEditModal{{ $bean_issue_daily->id }}"
                                    tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">แก้ไข ข้อมูลถั่วรับเข้า
                                                    รายวัน </h5>
                                                <button type="button" class="close btn btn-danger"
                                                    data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">X</span>
                                                </button>
                                            </div>
                                            <form
                                                action="{{ route('edit_issue_daily_detail', [$bean_issue_daily->id]) }}"
                                                method="POST" enctype="multipart/form-data">
                                                @csrf
                                                <div class="modal-body">

                                                    <label>ปัญหา</label>
                                                    <input type="text" name="detail" class="form-control"
                                                        value="{{ $bean_issue_daily->detail }}" required>


                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">ปิด</button>
                                                    <button type="submit" class="btn app-btn-primary">บันทึก</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>


                                <hr>

                                @if (count($bean_issue_daily->rm_issues_daily_imgs) > 0)
                                    <div
                                        style="display: grid; grid-template-columns: repeat(auto-fill, minmax(150px, 1fr)); gap: 5px;">
                                        @foreach ($bean_issue_daily->rm_issues_daily_imgs as $rm_issues_daily_img)
                                            <div style="margin: 5px;">
                                                <img src="{{ url('show_issue_img/' . $rm_issues_daily_img->path) }}"
                                                    alt="Issue Image" draggable="false" width="150px"
                                                    class="m-1">

                                                <form
                                                    action="{{ route('delete_issue_daily_img', [$rm_issues_daily_img->id]) }}"
                                                    method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger w-100"
                                                        onclick="confirm('ต้องการลบ ?')">ลบ</button>
                                                </form>
                                            </div>
                                        @endforeach
                                    </div>
                                    <hr>
                                @endif

                                <form action="{{ route('upload_issue_daily_img', [$bean_issue_daily->id]) }}"
                                    method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="input_fields_wrap">

                                        <input type="file" name="issue_img[1]" class="form-control">
                                        <button type="submit" class="btn btn-success my-2">เพิ่มรูป</button>
                                    </div>

                                </form>

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif

    </div>
    <div class="col">
        @if (isset($rm_manuals))
            <h4>ข้อมูลถั่วรับเข้า ที่มีปัญหา</h4>
            <table class="table">
                <thead>
                    <tr>
                        <td style="width:20px"></td>
                        <th style="width:125px">วันที่</th>
                        <th style="width:200px">SAP No.</th>
                        <th style="width:100px">ทะเบียนรถ</th>
                        <th>ปัญหาที่พบ</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rm_manuals as $rm_manual)
                        @php
                            $check_rmIssue = count($rm_manual->rm_issues);
                        @endphp
                        @if ($check_rmIssue > 0)
                            <tr>
                                <td rowspan="{{ $check_rmIssue + 2 }}">
                                    <form
                                        action="{{ route('delete_issue', [$rm_manual->manual_date, $rm_manual->sap_no]) }}"
                                        method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn" onclick="confirm('ต้องการลบ ?')"><i
                                                class="far fa-trash-alt"></i></button>
                                    </form>

                                </td>
                                <td rowspan="{{ $check_rmIssue + 2 }}"> {{ $rm_manual->manual_date }}</td>
                                <td rowspan="{{ $check_rmIssue + 2 }}"> {{ $rm_manual->sap_no }}</td>
                                <td rowspan="{{ $check_rmIssue + 2 }}"> {{ $rm_manual->car_name }}</td>
                            </tr>
                            <tr>
                                <td> <strong>บันทึกปัญหา</strong>
                                    <form action="{{ route('bean_add_new_issue', [$rm_manual->sap_no]) }}"
                                        method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="row my-2">
                                            <div class="col">
                                                <label>ปัญหา</label>
                                                <input type="text" name="issue" class="form-control" required>
                                            </div>
                                            <div class="col">
                                                <label>รูปภาพ</label>
                                                <input type="file" name="issue_img" accept=".png,.jpg"
                                                    class="form-control">
                                            </div>
                                        </div>

                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-success">บันทึก</button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                            @foreach ($rm_manual->rm_issues as $rm_issue)
                                <tr>
                                    <td>
                                        <div class="row">
                                            <div class="col">
                                                {{ $rm_issue->detail ?? '' }}
                                            </div>
                                            <div class="col-auto">
                                                <form action="{{ route('bean_delete_issue', [$rm_issue->id]) }}"
                                                    method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-danger"
                                                        onclick="confirm('ต้องการลบ ?')">ลบปัญหา</button>
                                                </form>
                                            </div>
                                        </div>

                                        <hr>
                                        <div
                                            style="display: grid; grid-template-columns: repeat(auto-fill, minmax(150px, 1fr)); gap: 5px;">
                                            @if (count($rm_issue->rm_issues_imgs) > 0)
                                                @foreach ($rm_issue->rm_issues_imgs as $rm_issues_img)
                                                    <div style="margin: 5px;">
                                                        <img src="{{ url('show_issue_img/' . $rm_issues_img->path) }}"
                                                            alt="Issue Image" draggable="false" width="150px"
                                                            class="m-1">

                                                        <form
                                                            action="{{ route('delete_issue_img', [$rm_issues_img->id]) }}"
                                                            method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class="btn btn-danger w-100"
                                                                onclick="confirm('ต้องการลบ ?')">ลบ</button>
                                                        </form>
                                                    </div>
                                                @endforeach
                                            @endif

                                        </div>

                                        <form action="{{ route('test_upload_bean_issue_img', [$rm_issue->id]) }}"
                                            method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="input_fields_wrap" data-rm-issue-id="{{ $rm_issue->id }}">
                                                <div class="my-2">

                                                    <button class="add_field_button btn btn-primary"
                                                        data-rm-issue-id="{{ $rm_issue->id }}">Add+</button>
                                                    <button type="submit" class="btn btn-success">เพิ่มรูป</button>
                                                </div>

                                                <div>
                                                    <input type="file" name="issue_img[1]" class="form-control">
                                                </div>
                                                <br>
                                            </div>

                                        </form>
                                    </td>

                                </tr>
                            @endforeach
                        @endif
                    @endforeach

                </tbody>
            </table>
        @endif
    </div>
</div>
