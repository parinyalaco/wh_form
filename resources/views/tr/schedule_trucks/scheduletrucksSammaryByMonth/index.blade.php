<!DOCTYPE html>
<html lang="en">

<head>
    <title>WH Form</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="WH Form">
    <meta name="author" content="IT LACO">
    <link rel="shortcut icon" href="{{ asset('pic/download.png') }}">

    <!-- FontAwesome JS-->
    <script rel="stylesheet" src="{{ asset('assets/plugins/fontawesome/js/all.min.js') }}"></script>

    <!-- App CSS -->
    <link id="theme-style" rel="stylesheet" href="{{ asset('assets/css/portal.css') }}">


    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <style>
        /* CSS สำหรับปรับแต่งแถบเลื่อน */
        ::-webkit-scrollbar {
            width: 10px;
            height: 2px;
        }

        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        ::-webkit-scrollbar-thumb {
            background: #0A9443;
            border-radius: 10px;
        }

        ::-webkit-scrollbar-thumb:hover {
            background: #555;
        }

        @media print {
            @page {
                size: 55% A4 landscape;
            }

            .d-print-none {
                display: none;
            }

            .page-break {
                page-break-after: always;
            }
        }
    </style>



<body>
    <div class="container-fluid m-2" style="font-size: 13px;background-color: #ffffff">
        <button onclick="window.print()" class="btn btn-sm btn-primary m-2 d-print-none" style="color: aliceblue">
            พิมพ์เอกสาร </button>
        <div style="display: flex; flex-wrap: wrap;">

            @foreach ($dataMonth as $fileName => $info)
                <div class="card m-2">
                    <div style="text-align: center; margin-bottom: 20px; width: 1868px;" class="my-2">

                        <h4>สรุปแผนการใช้หัวลาก - {{ $fileName }}</h4>
                    </div>
                </div>

                <div class="card m-2">
                    <div style="text-align: center; margin-bottom: 20px; width: 500px;" class="my-2">
                        <h5>สรุปแผนการใช้หัวลาก</h5>
                        <canvas id="deliveryPlansChart" class="p-2"></canvas>

                    </div>
                </div>
                <div class="card m-2">
                    <div style="text-align: center; margin-bottom: 20px; width: 800px;" class="my-2">
                        <h5>สรุปจำนวนเที่ยวรถหัวลาก </h5>
                        <canvas id="deliveryPlansChartTruck" width="610" height="420" class="p-2"></canvas>
                    </div>
                </div>


                <div class="card m-2">
                    <div class="card m-2">
                        <div style="text-align: center; margin-bottom: 20px; width: 514px;" class="my-2">
                            <h5>สรุปจำนวนเที่ยวรถหัวลาก</h5>
                            <table class="table table-striped">
                                <thead>
                                    <tr class="table-primary">
                                        <th>Truck Name</th>
                                        <th style="text-align: right">จำนวนเที่ยว</th>
                                        <th style="text-align: right">ร้อยละ %</th>
                                    </tr>
                                </thead>

                                @foreach (['Rent', 'Laco'] as $group)
                                    <tr>
                                        <th>{{ $group }}</td>
                                        <th style="text-align: right">{{ $info['trucks'][$group]['count'] }}</th>
                                        <th style="text-align: right">{{ $info['trucks'][$group]['percentage'] }} %</th>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="card m-2">
                        <div style="text-align: center; margin-bottom: 20px; width: 514px;" class="my-2">

                            <h5 class="m-2">สรุปจำนวนเที่ยวรถหัวลาก </h5>
                            <table class="table table-striped">
                                <thead>
                                    <tr class="table-primary">
                                        <th>Truck Name</th>
                                        <th style="text-align: right">จำนวนเที่ยว</th>
                                        <th style="text-align: right">ร้อยละ %</th>
                                    </tr>
                                </thead>

                                @foreach (['Rent', 'Laco'] as $group)
                                    @foreach ($info['trucks'][$group]['trucks'] as $truckName => $truckInfo)
                                        <tr>
                                            <td>&emsp;{{ $truckName }}</td>
                                            <td style="text-align: right">{{ $truckInfo['count'] }}</td>
                                            <td style="text-align: right">{{ $truckInfo['percentage'] }} %</td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            </table>
                        </div>
                    </div>
            @endforeach
        </div>
    </div>





    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels"></script>

    <script>
        var ctxMont = document.getElementById('deliveryPlansChart');
        var data = {
            labels: ['Rent', 'Lacco'],
            datasets: [{
                data: [
                    {{ $info['trucks']['Rent']['percentage'] }},
                    {{ $info['trucks']['Laco']['percentage'] }}
                ],
                backgroundColor: ['#ff6384', '#36a2eb'],
                hoverBackgroundColor: ['#ff6384', '#36a2eb']
            }]
        };

        var options = {
            responsive: true,
            plugins: {
                legend: {
                    display: false
                },
                tooltip: {
                    callbacks: {
                        label: function(tooltipItem) {
                            return tooltipItem.label + ': ' + tooltipItem.raw.toFixed(2) + '%';
                        }
                    }
                },
                datalabels: {
                    color: '#000',
                    font: {
                        size: 15,
                    },
                    formatter: function(value, context) {

                        const label = context.chart.data.labels[context.dataIndex];
                        return label + '\n' + value.toFixed(2) + '%';
                    }
                }
            }
        };

        new Chart(ctxMont, {
            type: 'pie',
            data: data,
            options: options,
            plugins: [ChartDataLabels],
        });
    </script>


    <script>
        @foreach ($dataMonth as $fileName => $info)
            var ctxTruck = document.getElementById('deliveryPlansChartTruck');
            var data = {
                labels: [
                    @foreach (['Rent', 'Laco'] as $group)
                        @foreach ($info['trucks'][$group]['trucks'] as $truckName => $truckInfo)
                            '{{ $truckName }}',
                        @endforeach
                    @endforeach
                ],
                datasets: [{
                    data: [
                        @foreach (['Rent', 'Laco'] as $group)
                            @foreach ($info['trucks'][$group]['trucks'] as $truckName => $truckInfo)
                                '{{ $truckInfo['count'] }}',
                            @endforeach
                        @endforeach
                    ],

                    backgroundColor: [
                        @foreach (['Rent', 'Laco'] as $group)
                            @foreach ($info['trucks'][$group]['trucks'] as $truckName => $truckInfo)
                                '{{ $truckInfo['color'] }}',
                            @endforeach
                        @endforeach
                    ],
                    percentage: [ // add counts array to store count data
                        @foreach (['Rent', 'Laco'] as $group)
                            @foreach ($info['trucks'][$group]['trucks'] as $truckName => $truckInfo)
                                '{{ $truckInfo['percentage'] }}',
                            @endforeach
                        @endforeach
                    ]
                }]
            };

            var options = {
                responsive: true,
                plugins: {
                    legend: {
                        position: false,
                    },
                    tooltip: {
                        callbacks: {
                            label: function(tooltipItem) {
                                return tooltipItem.label + ': ' + tooltipItem.raw.toFixed(2) + '%';
                            }
                        }
                    },

                    datalabels: {
                        formatter: function(value, context) {
                            const percentage = context.dataset.percentage[context
                                .dataIndex]; // get count data from counts array
                            const count = context.dataset.data[context
                                .dataIndex]; // get count data from counts array
                            return 'จำนวน:' + count + '\n' + percentage + '%';
                        },
                        color: '#000',
                        font: {
                            size: 15,
                        }
                    }



                }
            };

            var pieChart = new Chart(ctxTruck, {
                type: 'bar',
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        @endforeach
    </script>
</body>
