@extends('layouts.app-master')

@section('content')
    {{-- <div class="container"> --}}
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            <div class="page-heading row">
                    <h3 class="col">{{ __('TR') }} -> Truck Schedules LOCK "{{$filename}}"</h3>
                </div>
            <div class="card-body">
                @if (Session::has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('error') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('success') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                <div class="row g-3 mb-4 align-items-center justify-content-between">
                    <div class="col-auto">
                        <div class="page-utilities">
                            <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                                <div class="col-auto">
                                    <a class="btn app-btn-secondary" href="{{ url('/TR/scheduletrucks/'.$filename.'/create') }}">
                                        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
                                        Add
                                    </a>
                                </div>
                            </div>
                            <!--//row-->
                        </div>
                        <!--//table-utilities-->
                    </div>
                    <!--//col-auto-->
                </div>
                <!--//row onclick="getURL($key);" -->

                <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                    <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#"
                        role="tab" aria-controls="orders-all" aria-selected="true"></a>
                </nav>

                <div class="tab-content" id="orders-table-tab-content">
                    <div class="app-card app-card-orders-table shadow-sm mb-5">
                        <div class="app-card-body">
                            <table class="table app-table-hover mb-0 text-left">
                                <thead>
                                    <tr>
                                        <th class="cell">No.</th>
                                        <th class="cell">Name</th>
                                        <th class="cell">Desc</th>
                                        <th class="cell">Status</th>
                                        <th class="cell"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($scheduletrucks as $scheduletruck)
                                        <tr>
                                            <td class="cell">{{ $loop->iteration }}</td>
                                            <td class="cell">{{ $scheduletruck->truck->name }}</td>
                                            <td class="cell">{{ $scheduletruck->start_date }} - {{ $scheduletruck->end_date }}</td>
                                            <td class="cell">{{ $scheduletruck->status }}</td>
                                            <td class="cell">
                                                <div class="row">
                                                    <div class="col-auto mx-2">
                                                        <a class="btn btn-primary"
                                                            href="{{ url('/TR/scheduletrucks/'.$filename.'/'.$scheduletruck->id.'/edit') }}">Edit</a>
                                                    </div>
                                                    <div class="col-auto">
                                                        <form action="{{ url('/TR/scheduletrucks/'.$filename.'/'.$scheduletruck->id) }}"
                                                            method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class="btn btn-danger">Delete</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!--//app-card-body-->
                    </div>
                    <!--//app-card-->
                </div>
                <div class="pagination-wrapper"> {!! $scheduletrucks->appends($_GET)->render() !!} </div>
                <!--//tab-content-->
            </div>
            {{-- </div> --}}
        </div>
    </div>
@endsection
