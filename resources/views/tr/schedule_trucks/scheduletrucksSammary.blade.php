@extends('layouts.app-master')

@section('content')
    <div class="container-fluid m-2" style="font-size: 13px">
        <h4 class="d-print-none">สรุปการใช้ รถบรรทุก</h4>
        <form action="{{ route('scheduletrucksSumary') }}" method="get" enctype="multipart/form-data" class="d-print-none">
            @csrf
            <label>เลือกปี</label>
            <div class="input-group w-25 ">
                <select name="year" class="form-select">
                    @foreach ($selectYears as $selectYear)
                        <option value="{{ $selectYear }}" @if ($year == $selectYear) selected @endif>
                            {{ $selectYear }}</option>
                    @endforeach

                </select>
                <div class="input-group-append">
                    <button class="input-group-text" id="basic-addon2">เลือกปี</button>
                </div>
            </div>
        </form>
        <hr>
        <h2>สรุปการใช้ รถบรรทุก แต่ละเดือน</h2>


        <div style="display: flex; flex-wrap: wrap;">
            @foreach ($dataMonth as $index => $month)
                <div class="card m-2">
                    <div style="text-align: center; margin-bottom: 20px; width: 452px;" class="my-2">
                        <h5><a href="{{route('scheduletrucksSumaryMonth',$month['month'])}}" target="_blank">{{ $month['month'] }} </a></h5>
                        <canvas id="deliveryPlansChartBar{{ $index }}" width="610" height="300"></canvas>
                    </div>

                    <div class="mx-2">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>รถบรรทุก</th>
                                    <th style="text-align: center">จำนวนการใช้งาน</th>
                                    <th style="text-align: right">ร้อยละ</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($month['trucks'] as $truckName => $truckData)
                                    <tr>
                                        <td>{{ $truckName }}</td>
                                        <td style="text-align: center">{{ $truckData['count'] }}</td>
                                        <td style="text-align: right">{{ $truckData['percentage'] }} %</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endforeach
        </div>


        <hr>
        <h2>สรุปการใช้ รถบรรทุก แต่ละคัน</h2>
        <p>กราฟแสดง จำนวนการใช้รถของแต่ละ คัน</p>
        <div style="display: flex; flex-wrap: wrap;">
            @foreach ($dataTruck as $index => $pies)
                <div class="card m-2">
                    <div style="text-align: center; margin-bottom: 20px; width: 358px;" class="my-2">
                        <h5>{{ $pies['truckName'] }}</h5>
                        <canvas id="deliveryPlansChartTruck{{ $index }}" height="300"></canvas>
                    </div>

                    <div class="mx-2">
                        <table class="table ">
                            <tr>
                                <th>เดือน</th>
                                <th style="text-align: center">จำนวนการใช้งาน </th>
                                <th style="text-align: right">ร้อยละ </th>

                            </tr>
                            @foreach ($pies['deliveryPlans'] as $fileName => $monthData)
                                <tr>
                                    <td>{{ $fileName }}</td>
                                    <td style="text-align: center">{{ $monthData['count'] }} </td>
                                    <td style="text-align: right">{{ $monthData['percentage'] }} %</td>

                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            @endforeach
        </div>
    </div>


    @push('script')
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels"></script>


        <script>
            document.addEventListener("DOMContentLoaded", function() {
                @foreach ($dataMonth as $index => $bar)
                    var ctxBar{{ $index }} = document.getElementById(
                        'deliveryPlansChartBar{{ $index }}').getContext('2d');

                    var dataBar{{ $index }} = {
                        labels: [
                            @foreach ($bar['trucks'] as $truckName => $truckData)
                                '{{ $truckName }}',
                            @endforeach
                        ],

                        datasets: [{
                            data: [
                                @foreach ($bar['trucks'] as $truckName => $truckData)
                                    {{ $truckData['percentage'] }},
                                @endforeach
                            ],
                            backgroundColor: [
                                @foreach ($bar['trucks'] as $truckName => $truckData)
                                    '{{ $truckData['color'] }}',
                                @endforeach
                            ],

                            borderWidth: 1,
                            counts: [ // add counts array to store count data
                                @foreach ($bar['trucks'] as $truckName => $truckData)
                                    {{ $truckData['count'] }},
                                @endforeach
                            ]
                        }]


                    };

                    var optionsBar{{ $index }} = {
                        responsive: true,
                        plugins: {
                            legend: {
                                // position: 'top',
                                position: false,
                            },
                            tooltip: {
                                callbacks: {
                                    label: function(tooltipItem) {
                                        return tooltipItem.label + ': ' + tooltipItem.raw.toFixed(2) + '%';
                                    }
                                }
                            },
                            datalabels: {
                                formatter: function(value, context) {
                                    const count = context.dataset.counts[context
                                        .dataIndex]; // get count data from counts array
                                    return value.toFixed(2) + '%' + '\n' + 'จำนวน:' + count;
                                },
                                color: '#000',
                                font: {
                                    size: 12,
                                }
                            }
                        },
                        scales: {
                            y: {
                                beginAtZero: true,
                                ticks: {
                                    callback: function(value) {
                                        return value.toFixed(2) + '%';
                                    }
                                }
                            }
                        }
                    };
                    var optionsBar{{ $index }} = {
                        responsive: true,
                        plugins: {
                            legend: {
                                position: 'top',
                            },
                            tooltip: {
                                callbacks: {
                                    label: function(tooltipItem) {
                                        return tooltipItem.label + ': ' + tooltipItem.raw.toFixed(2) + '%';
                                    }
                                }
                            },
                            datalabels: {
                                formatter: function(value, context) {
                                    const count = context.dataset.counts[context
                                        .dataIndex]; // get count data from counts array
                                    return value.toFixed(2) + '%' + '\n' + 'จำนวน:' + count;
                                },
                                color: '#000',
                                font: {
                                    size: 12,
                                }
                            }
                        }
                    };

                    var barChart{{ $index }} = new Chart(ctxBar{{ $index }}, {
                        type: 'pie',
                        data: dataBar{{ $index }},
                        options: optionsBar{{ $index }},
                        plugins: [ChartDataLabels]
                    });
                @endforeach
            });
        </script>


        <script>
            @foreach ($dataTruck as $index => $pie)
                var ctxPie{{ $index }} = document.getElementById('deliveryPlansChartTruck{{ $loop->index }}');
                var data{{ $index }} = {
                    labels: [
                        @foreach ($pie['deliveryPlans'] as $fileName => $monthData)
                            '{{ $fileName }}',
                        @endforeach
                    ],
                    datasets: [{
                        data: [
                            @foreach ($pie['deliveryPlans'] as $fileName => $monthData)
                                {{ $monthData['percentage'] }},
                            @endforeach
                        ],

                        backgroundColor: [
                            '#fff2c2',
                            '#ffd1fb',
                            '#b8f1ff',
                            '#cf95fe',
                            '#b8ffcd',
                            '#ffd1d1',
                            '#c9c9c9',
                            '#a3e7d8',
                            '#f9a3a4',
                            '#e1a3f9',
                            '#a3a3f9',
                            '#f9dfa3'
                        ]
                    }]
                };

                var options{{ $index }} = {
                    responsive: true,
                    plugins: {
                        legend: {
                            position: false,
                        },
                        tooltip: {
                            callbacks: {
                                label: function(tooltipItem) {
                                    return tooltipItem.label + ': ' + tooltipItem.raw.toFixed(2) + '%';
                                }
                            }
                        },
                        datalabels: {
                            formatter: function(value, context) {
                                const truckName = context.chart.data.labels[context.dataIndex];
                                const monthCount =
                                    {{ json_encode(array_values(array_column($pie['deliveryPlans'], 'count'))) }}[
                                        context.dataIndex];


                                return [value.toFixed(2) + '%', 'จน.' + monthCount];
                            },
                            color: '#000',
                            font: {
                                size: 12,
                            }
                        }
                    }
                };

                var pieChart{{ $index }} = new Chart(ctxPie{{ $index }}, {
                    type: 'bar',
                    data: data{{ $index }},
                    options: options{{ $index }},
                    plugins: [ChartDataLabels]
                });
            @endforeach
        </script>
    @endpush
@endsection
