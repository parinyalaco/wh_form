@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="page-heading row">
                <div class="pull-right col-auto my-2">
                    <a class="btn btn-secondary" href="{{ url('/TR/scheduletrucks/'.$filename.'/') }}">Back</a>
                </div>
                <h1 class="col">{{ __('Truck Schedules') }} -> EDIT LOCK</h1>
            </div>

            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>

            <div class="tab-content" id="orders-table-tab-content">
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <form method="POST" action="{{ url('/TR/scheduletrucks/'.$filename.'/'.$scheduleTruck->id) }}">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-4">
                                    <label for="truck_id" class="form-label text-right">{{ __('Truck') }}</label>
                                    <select name="truck_id" id="truck_id" class="form-select" required>
                                        <option value="">---เลือก---</option>
                                        @foreach ($trucks as $tkey=>$titem)
                                            <option value="{{$tkey}}"
                                            @if ($scheduleTruck->truck_id == $tkey)
                                                selected
                                            @endif
                                            >{{ $titem }}</option>
                                        @endforeach
                                    </select>
                                    @error('status')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>


                                <div class="col-4">
                                    <label for="start_date" class="form-label text-right">{{ __('start_date') }}</label>
                                     <input id="start_date" type="date"
                                            class="form-control @error('desc') is-invalid @enderror" name="start_date"
                                            value="{{ $scheduleTruck->start_date }}" autocomplete="desc">
                                    @error('start_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-4">
                                    <label for="end_date" class="form-label text-right">{{ __('end_date') }}</label>
                                     <input id="end_date" type="date"
                                            class="form-control @error('desc') is-invalid @enderror" name="end_date"
                                            value="{{ $scheduleTruck->end_date }}" autocomplete="desc">
                                    @error('end_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-9">
                                    <label for="desc" class="col-form-label text-md-end">{{ __('รายละเอียด') }}</label>

                                    <div class="col-md-12">
                                        <input id="desc" type="text"
                                            class="form-control @error('desc') is-invalid @enderror" name="desc"
                                            value="{{ $scheduleTruck->order_name }}" autocomplete="desc">

                                        @error('desc')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-3 align-bottom">
                                    <label for="submit" class="col-form-label text-md-end">&nbsp;</label>
                                    <div class="col-md-12">
                                    <button type="submit" class="btn btn-success">
                                        {{ __('Save') }}
                                    </button>
                                     </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--//app-card-body-->
                </div>
                <!--//app-card-->
            </div>
            <!--//tab-content-->
        </div>
        <!--//container-fluid-->
    </div>
    <!--//app-content-->
    {{-- <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>      --}}
@endsection
