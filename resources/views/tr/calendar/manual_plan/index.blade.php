@extends('layouts.app-master')
@push('styles')
    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }

        .pagination {
            margin: 0;
        }

        .pagination li:hover {
            cursor: pointer;
        }

        .header_wrap {
            padding: 30px 0;
        }

        .num_rows {
            width: 20%;
            float: left;
        }

        .tb_search {
            width: 20%;
            float: right;
        }

        .pagination {
            width: 70%;
            float: left;
        }

        .rows_count {
            width: 20%;
            float: right;
            text-align: right;

        }

        .active {
            background-color: #93d6ff;
            border-radius: 5px;
            color: rgb(0, 0, 0);
            font-weight: 1000;
            font-size: 18px;
        }


        /* table sorting */
        .table-sortable th {
            cursor: pointer;

        }

        .table-sortable .th-sort-asc::after {
            content: "\25b4";

        }

        .table-sortable .th-sort-desc::after {
            content: "\25be";
        }

        .table-sortable .th-sort-asc::after,
        .table-sortable .th-sort-desc::after {}

        .table-sortable .th-sort-asc,
        .table-sortable .th-sort-desc {
            background: rgba(0, 0, 0, 0.1);
            background-color: #B2D3DB;
        }

        /* Header table Fixed */
        .tableFixHead {
            overflow: auto;
            height: 540px;
        }

        .tableFixHead thead th {
            position: sticky;
            top: 0;
            background-color: #c7c7c7;
        }


        /* Hide */
        #columns-dropdown {
            list-style: none;

        }
    </style>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css"
        type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"
        type="text/javascript"></script>
@endpush

@section('content')
    <div class="container-fluid">
        <div class="row mt-3">
            <div class="col">
                <form action="{{ route('manual_plan') }}" method="get">
                    <div class="input-group">
                        <select class="form-control" name="file_name" required>
                            <option value=''>-- เลือกแผน เพื่อวางแผน --</option>

                            @foreach ($filenames as $file_name)
                                <option value='{{ $file_name->file_name }}'
                                    @if ($keyword == $file_name->file_name) selected @endif>{{ $file_name->file_name }}</option>
                            @endforeach

                        </select>
                        <div class="input">
                            <button type="submit" class="btn btn-primary rounded-sm mx-1" style="width: 150px">
                                เลือกแผน</button>
                        </div>

                </form>
                @if (isset($keyword))
                    <form action="{{ route('DeliveryPlan') }}" target="__blank">
                        @csrf
                        <input type="text" value="{{ $keyword }}" name="file_name" hidden required>
                        <button type="submit" class="btn btn-success rounded-sm mx-1" style="width: 150px">Export
                            excel</button>
                    </form>
                @endif
            </div>
            @if (isset($keyword))
                <div class="m-3">
                    <h5>แผนที่เลือก : <strong> {{ $keyword }}</strong></h5>
                </div>
            @endif

        </div>
        <div class="col">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        @foreach ($trucks as $truck)
                            <th style="background-color: {{ $truck->color }}">{{ $truck->name }}</th>
                        @endforeach

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        @if (isset($import_delivery_plans))
                            @foreach ($count_trucks as $count_truck)
                                <td>{{ $count_truck }}</td>
                            @endforeach
                        @endif
                    </tr>
                </tbody>
            </table>
        </div>

        @if (session('success'))
            {{-- <div class="alert alert-success m-4">{{ session('success') }}</div> --}}
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                {{ session('success') }}
            </div>
        @endif

        <div class="app-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header_wrap">
                        <div class="num_rows">

                            <div class="form-group">
                                <select class  ="form-control" name="state" id="maxRows">
                                    <option value="10">10</option>
                                    <option value="15">15</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="70">70</option>
                                    <option value="100">100</option>
                                    <option value="5000">Show ALL Rows</option>
                                </select>

                            </div>
                        </div>
                        <div class="tb_search">
                            <input type="text" id="search_input_all" onkeyup="FilterkeyWord_all_table()"
                                placeholder="Search.." class="form-control">
                        </div>
                    </div>

                    <br>

                    <div id="columns-dropdown">
                        <input type="checkbox" data-col="col-loading_date" class="col-checkbox mx-1" />loading_date

                        <input type="checkbox" data-col="col-order" class="col-checkbox mx-1" />order
                        <input type="checkbox" data-col="col-Weight" class="col-checkbox mx-1" />Weight
                        <input type="checkbox" data-col="col-etd_date" class="col-checkbox mx-1" />etd_date

                        <input type="checkbox" data-col="col-etd_place" class="col-checkbox mx-1 " />etd_place

                        <input type="checkbox" data-col="col-eta_date" class="col-checkbox  mx-1" />eta_date
                        <input type="checkbox" data-col="col-eta_place" class="col-checkbox mx-1" />eta_place

                        <input type="checkbox" data-col="col-Vessel_Liner" class="col-checkbox mx-1" />Vessel
                        Liner

                        <input type="checkbox" data-col="col-Forward_Vendor" class="col-checkbox mx-1" />Forward
                        Vendor

                        <input type="checkbox" data-col="col-des" class="col-checkbox mx-1" />Description

                        <input type="checkbox" data-col="col-remark1" class="col-checkbox mx-1" />remark1
                        <input type="checkbox" data-col="col-place_port" class="col-checkbox mx-1" />place_port

                        <input type="checkbox" data-col="col-Booking_No" class="col-checkbox mx-1" />Booking_No

                        <input type="checkbox" data-col="col-cy_plan_date" class="col-checkbox mx-1" />cy_plan_date
                        <input type="checkbox" data-col="col-Cut_Off" class="col-checkbox mx-1" />Cut Off

                        <input type="checkbox" data-col="col-Time" class="col-checkbox mx-1" />Time
                        <input type="checkbox" data-col="col-H" class="col-checkbox mx-1" />หัว
                        <input type="checkbox" data-col="col-T" class="col-checkbox mx-1" />หาง
                        <input type="checkbox" data-col="col-laco_cy" class="col-checkbox mx-1" />หัวรับตู้

                        <input type="checkbox" data-col="col-RT" class="col-checkbox mx-1" />RT
                        <input type="checkbox" data-col="col-Remark" class="col-checkbox mx-1" />Remark

                    </div>


                    <form action="{{ route('manual_plan_store') }}" method="POST">
                        @csrf
                        <div class="tableFixHead">
                            <table class="table     table-sortable table-striped table-hover " id= "table-id">
                                <thead>
                                    <tr>
                                        <th class="col-loading_date">loading_date</th>
                                        <th class="col-order">order</th>
                                        <th class="col-Weight">Weight</th>

                                        <th class="col-etd_date">etd_date</th>
                                        <th class="col-etd_place">etd_place</th>
                                        <th class="col-eta_date">eta_date</th>
                                        <th class="col-eta_place">eta_place</th>
                                        <th class="col-Vessel_Liner">Vessel Liner</th>
                                        <th class="col-Forward_Vendor">Forward Vendor</th>
                                        <th class="col-des">Description</th>


                                        <th class="col-remark1">remark1</th>
                                        <th class="col-place_port">place_port</th>

                                        <th class="col-Booking_No">Booking No</th>
                                        <th class="col-Cut_Off">Cut Off</th>
                                        <th class="col-Time">Time</th>
                                        <th class="col-cy_plan_date">cy_plan_date</th>

                                        <th class="col-T">หาง</th>
                                        <th class="col-RT">RT</th>

                                        <th class="col-H">หัว H</th>

                                        <th class="col-laco_cy">LACO:CY</th>

                                        <th class="col-Remark">Remark</th>

                                    </tr>
                                </thead>

                                <tbody>
                                    @php
                                        $i = 0;
                                    @endphp

                                    @if (isset($import_delivery_plans))
                                        @foreach ($import_delivery_plans as $import_delivery_plan)
                                            <tr
                                                style="background-color: {{ $import_delivery_plan->truck->color ?? '' }}; ">
                                                <td class="col-loading_date">
                                                    {{ date('d/m/Y', strtotime($import_delivery_plan->loading_date)) }}
                                                </td>


                                                <td class="col-order">{{ $import_delivery_plan->order }} </td>
                                                <td class="col-Weight">
                                                    <p class="thousands">{{ $import_delivery_plan->weight ?? '' }}</p>
                                                </td>

                                                <td class="col-etd_date">
                                                    {{ date('d/m/Y', strtotime($import_delivery_plan->etd_date)) }} </td>
                                                <td class="col-etd_place">{{ $import_delivery_plan->etd_place }} </td>
                                                <td class="col-eta_date">
                                                    {{ date('d/m/Y', strtotime($import_delivery_plan->eta_date)) }} </td>
                                                <td class="col-eta_place">{{ $import_delivery_plan->eta_place }} </td>
                                                <td class="col-Vessel_Liner">
                                                    <p style="width: 150px">{{ $import_delivery_plan->vl }}</p>
                                                </td>
                                                <td class="col-Forward_Vendor">
                                                    <p style="width: 200px">{{ $import_delivery_plan->fv }} </p>
                                                </td>
                                                <td class="col-des">
                                                    <p style="width: 200px">
                                                        {{ $import_delivery_plan->desc }}
                                                    </p>
                                                </td>
                                                <td class="col-remark1">{{ $import_delivery_plan->remark1 }} </td>
                                                <td class="col-place_port">{{ $import_delivery_plan->place_port }} </td>


                                                {{-- =================================== plan =================================== --}}

                                                {{-- Booking No --}}
                                                <td class="col-Booking_No">
                                                    <input type="text" class="form-control" style="width:200px"
                                                        name="data[{{ $import_delivery_plan->id }}][booking_no]"
                                                        value="{{ $import_delivery_plan->booking_no }}">
                                                </td>

                                                {{-- Cut Off --}}

                                                <td class="col-Cut_Off">
                                                    <input type="text" class="form-control cutoff" autocomplete="off"
                                                        style="width:65px"
                                                        name="data[{{ $import_delivery_plan->id }}][cut_off]"
                                                        value="{{ date($import_delivery_plan->cut_off) }}">

                                                </td>

                                                {{-- Time --}}
                                                <td class="col-Time">
                                                    <input type="time" id="appt" class="form-control"
                                                        name="data[{{ $import_delivery_plan->id }}][time]"
                                                        value="{{ $import_delivery_plan->time }}">

                                                </td>

                                                {{-- CY Plan Date --}}
                                                <td class="col-cy_plan_date">
                                                    <input type="date" class="form-control" style="width: 150px"
                                                        name="data[{{ $import_delivery_plan->id }}][cy_plan_date]"
                                                        value="{{ date('Y-m-d', strtotime($import_delivery_plan->cy_plan_date)) }}">
                                                </td>

                                                {{-- Tail Select --}}
                                                <td class="col-T">
                                                    <select name="data[{{ $import_delivery_plan->id }}][tail_id]"
                                                        class="form-select" style="width: 150px">
                                                        <option selected value="">เลือก หาง</option>

                                                        @foreach ($tails as $tail)
                                                            <option value="{{ $tail->id }}"
                                                                @if ($import_delivery_plan->tail_id == $tail->id) selected @endif>
                                                                {{ $tail->name }} </option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                {{-- Return Container Date --}}
                                                <td class="col-RT">
                                                    <input type="date" class="form-control"
                                                        style="width: 150px"
                                                        name="data[{{ $import_delivery_plan->id }}][RT]" data-date=""
                                                        @if (!empty($import_delivery_plan->RT)) value="{{ date('Y-m-d', strtotime($import_delivery_plan->RT)) }}"
                                                    @else
                                                    value="{{ date('Y-m-d') }}" @endif
                                                        value="{{ date('Y-m-d') }}">
                                                </td>

                                                {{-- Truck Select --}}
                                                <td class="col-H">
                                                    <select
                                                        name="data[{{ $import_delivery_plan->id }}][schedule_truck_id]"
                                                        class="form-select" style="width: 150px">
                                                        <option selected value="">เลือก หัวลาก</option>

                                                        @foreach ($trucks as $truck)
                                                            <option value="{{ $truck->id }}"
                                                                @if ($import_delivery_plan->schedule_truck_id == $truck->id) selected @endif>
                                                                {{ $truck->name }}</option>
                                                        @endforeach

                                                    </select>
                                                </td>

                                                {{-- LACO:CY --}}
                                                <td class="col-laco_cy">
                                                    <select name="data[{{ $import_delivery_plan->id }}][get_container]"
                                                        class="form-select" style="width: 150px">
                                                        <option selected value="">เลือก หัวลาก</option>

                                                        @foreach ($trucks as $truck)
                                                            <option value="{{ $truck->id }}"
                                                                @if ($import_delivery_plan->get_container == $truck->id) selected @endif>
                                                                {{ $truck->name }}</option>
                                                        @endforeach

                                                    </select>

                                                </td>

                                                {{-- RemarK --}}
                                                <td class="col-Remark">
                                                    <input type="text" class="form-control" style="width:200px"
                                                        name="data[{{ $import_delivery_plan->id }}][remark]"
                                                        value="{{ $import_delivery_plan->remark }}">

                                                </td>

                                            </tr>
                                        @endforeach
                                    @endif

                                </tbody>

                            </table>
                        </div>

                        <nav class="m-2">
                            <ul class="pagination"></ul>
                        </nav>

                        <div class="rows_count">Showing 11 to 20 of 91 entries</div>

                        <button type="submit" class="btn btn-success w-100 mt-2">บันทึก</button>
                    </form>
                </div>
            </div>
        </div>


    </div>
    <script src="{{ asset('assets/js/tr-table.js') }}"></script>



    <script type="text/javascript">
        $(function() {
            $(".cutoff").datepicker({
                format: "d/m"
            });
        });




        function formatNumber(n) {
            if (!isFinite(n)) {
                return n;
            }

            var s = "" + n,
                abs = Math.abs(n),
                _, i;

            if (abs >= 1000) {
                _ = ("" + abs).split(/\./);
                i = _[0].length % 3 || 3;

                _[0] = s.slice(0, i + (n < 0)) +
                    _[0].slice(i).replace(/(\d{3})/g, ',$1');

                s = _.join('.');
            }

            return s;
        }

        $('.thousands').each(function() {
            $(this).html(formatNumber($(this).html()))
        })
    </script>

    <script>
        window.setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function() {
                $(this).remove();
            });
        }, 3000);
    </script>

    <script></script>
@endsection
