@extends('layouts.app-master')

@section('content')
    <div class="m-4">
        <div class="row mt-3">
            <div class="col">
                <h1> 📅 Trucks Plans Calendar</h1>
            </div>
            <div class="col">
                {{-- <form action="{{ route('DeliveryPlan') }}">
                    @csrf
                    <div class="input-group">
                        <select class="form-control" name="file_name" required>
                            <option value=''>-- เลือกแผน Export Execl --</option>
                            @foreach ($filenames as $file_name)
                                <option value='{{ $file_name->file_name }}'>{{ $file_name->file_name }}</option>
                            @endforeach

                        </select>
                        <div class="input">
                            <button type="submit" class="btn btn-success rounded-sm mx-1">Export
                                excel</button>
                        </div>
                    </div>
                </form> --}}
            </div>
        </div>
    </div>
    <div id="app"></div>
    <script src="{{ asset('js/app.js') }}" type="module" defer></script>
@endsection
