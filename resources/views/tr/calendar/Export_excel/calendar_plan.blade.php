<table>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="height: 25px;">รายงานแผนการเดินรถ {{ $planMonth }} รายละเอียดดังนี้ </td>
    </tr>

    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="height: 45px; border:1px solid black;">No</td>
        <td style="border:1px solid black;">Container</td>
        <td style="border:1px solid black;">CNS</td>
        <td style="border:1px solid black;">Booking No.</td>
        <td style="border:1px solid black;">order</td>

        <td style="border:1px solid black;">Loading Date</td>
        <td style="border:1px solid black;">ETD Date</td>
        <td style="border:1px solid black;">ETD Place</td>
        <td style="border:1px solid black;">ETA Date</td>
        <td style="border:1px solid black;">ETA Place</td>

        <td style="border:1px solid black;">Vessel Liner</td>
        <td style="border:1px solid black;">Forward Vendor</td>
        <td style="border:1px solid black;">Description</td>
        <td style="border:1px solid black;">Remark1</td>

        <td style="border:1px solid black;">Place/Port</td>

        <td style="border:1px solid black;">Cut Off</td>
        <td style="border:1px solid black;">Time</td>
        <td style="border:1px solid black;">CY Plan Date</td>

        <th style="border:1px solid black;">หาง</th>
        <th style="border:1px solid black;">RT</th>
        <th style="border:1px solid black;">หัว H</th>
        <th style="border:1px solid black;">LACO:CY</th>
        <th style="border:1px solid black;">Remark</th>
    </tr>
    @php
        $i = 1;

    @endphp

    @foreach ($import_delivery_plans as $import_delivery_plan)
        <tr>
            <td
                style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};height: 40px;border:1px solid black;">
                {{ $i++ }}</td>

            <td
                style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};height: 40px;border:1px solid black;">
                {{ $import_delivery_plan->container }}</td>

            <td
                style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};height: 40px;border:1px solid black;">
                {{ $import_delivery_plan->csn }}</td>

            <td
                style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};height: 40px;border:1px solid black;">
                {{ $import_delivery_plan->booking_no }}</td>

            <td style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};border:1px solid black;">
                {{ $import_delivery_plan->order }}</td>

            <td style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};border:1px solid black;">
                {{ date('d/m/Y', strtotime($import_delivery_plan->loading_date)) }}
            </td>


            <td style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};border:1px solid black;">
                {{ date('d/m/Y', strtotime($import_delivery_plan->etd_date)) }}</td>

            <td style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};border:1px solid black;">
                {{ $import_delivery_plan->etd_place }}</td>

            <td style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};border:1px solid black;">
                {{ date('d/m/Y', strtotime($import_delivery_plan->eta_date)) }} </td>

            <td style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};border:1px solid black;">
                {{ $import_delivery_plan->eta_place }}</td>

            <td style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};border:1px solid black;">
                {{ $import_delivery_plan->vl }}</td>

            <td style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};border:1px solid black;">
                {{ $import_delivery_plan->fv }}</td>

            <td style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};border:1px solid black;">
                {{ $import_delivery_plan->desc }}</td>

            <td style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};border:1px solid black;">
                {{ $import_delivery_plan->remark1 }}</td>


            <td style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};border:1px solid black;">
                {{ $import_delivery_plan->place_port }}</td>



            <td style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};border:1px solid black;">
                {{ $import_delivery_plan->cut_off ?? '-' }}</td>

            <td style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};border:1px solid black;">
                {{ $import_delivery_plan->time ?? '-' }}</td>

            <td style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};border:1px solid black;">
                {{ date('d/m/Y', strtotime($import_delivery_plan->cy_plan_date ?? '')) }}</td>


            {{-- หางลาก --}}
            <td style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};border:1px solid black;">
                {{ $import_delivery_plan->tail->name ?? '' }} </td>

            <td style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};border:1px solid black;">
                {{ date('d/m/Y', strtotime($import_delivery_plan->RT ?? '')) }}</td>


            <td style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};border:1px solid black;">
                {{ $import_delivery_plan->truck->name ?? '' }}</td>


            <td style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};border:1px solid black;">
                {{ $import_delivery_plan->laco_cy->name ?? '' }}</td>


            <td style="background-color: {{ $import_delivery_plan->truck->color ?? '' }};border:1px solid black;">
                {{ $import_delivery_plan->remark ?? '' }}</td>
        </tr>
    @endforeach


    <tr>
        <td></td>
    </tr>
    <tr>
        <th colspan="7" style="border:1px solid black;">จำนวนเที่ยวรถของแผน {{ $planMonth }}</th>
    </tr>

    <tr>
        @foreach ($trucks as $truck)
            <th style="height: 25px;background-color: {{ $truck->color }};border:1px solid black;">{{ $truck->name }}</th>
        @endforeach

    </tr>

    <tr>
        @if (isset($import_delivery_plans))
            @foreach ($count_trucks as $count_truck)
                <td style="height: 25px;border:1px solid black;">{{ $count_truck }}</td>
            @endforeach
        @endif
    </tr>




</table>
