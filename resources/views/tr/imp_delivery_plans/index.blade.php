@extends('layouts.app-master')

@section('content')
    {{-- <div class="container"> --}}
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            <div class="page-heading row">
                <h3 class="col">{{ __('TR') }} -> Delivery plan</h3>
            </div>
            <div class="card-body">
                @if (Session::has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('error') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('success') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                <div class="row g-3 mb-4 align-items-center justify-content-between">
                    <div class="col-auto">
                        <div class="page-utilities">
                            <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                                <div class="col-auto">
                                    <a class="btn app-btn-secondary" href="{{ route('importdlps.uploadXls') }}">
                                        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
                                        Upload
                                    </a>
                                </div>
                            </div>
                            <!--//row-->
                        </div>
                        <!--//table-utilities-->
                    </div>
                    <!--//col-auto-->
                </div>
                <!--//row onclick="getURL($key);" -->

                <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                    <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#"
                        role="tab" aria-controls="orders-all" aria-selected="true"></a>
                </nav>

                <div class="tab-content" id="orders-table-tab-content">
                    <div class="app-card app-card-orders-table shadow-sm mb-5">
                        <div class="app-card-body">
                            <table class="table app-table-hover mb-0 text-left">
                                <thead>
                                    <tr>
                                        <th class="cell">No.</th>
                                        <th class="cell">File</th>
                                        <th class="cell">จำนวน</th>
                                        <th class="cell"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($impdeliveryplans as $impdeliveryplan)
                                        <tr>
                                            <td class="cell">{{ $loop->iteration }}</td>
                                            <td class="cell">{{ $impdeliveryplan->file_name }}</td>
                                            <td class="cell">{{ $impdeliveryplan->alldata }}</td>
                                            <td class="cell">
                                                <div class="row">
                                                    <div class="col-auto mx-2">
                                                        <a class="btn btn-primary"
                                                            href="{{ url('/TR/scheduletrucks/' . $impdeliveryplan->file_name . '/') }}">Manage
                                                            Lock</a>
                                                        <a class="btn btn-primary"
                                                            href="{{ route('importdlps.show', $impdeliveryplan->file_name ?? '') }}">Show</a>
                                                        <a class="btn btn-secondary"
                                                            href="{{ route('importdlps.process', $impdeliveryplan->file_name ?? '') }}">process</a>
                                                        <a class="btn btn-success"
                                                            href="{{ route('importdlps.planview', $impdeliveryplan->file_name ?? '') }}">planview</a>
                                                        <a class="btn btn-success"
                                                            href="{{ route('importdlps.exportplan', $impdeliveryplan->file_name ?? '') }}">Export</a>
                                                    </div>
                                                    <div class="col-auto">
                                                        <form
                                                            action="{{ route('importdlps.destroy', $impdeliveryplan->file_name ?? '') }}"
                                                            method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class="btn btn-danger">Delete</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!--//app-card-body-->
                    </div>
                    <!--//app-card-->
                </div>
                <div class="pagination-wrapper"> {!! $impdeliveryplans->appends($_GET)->render() !!} </div>
                <!--//tab-content-->
            </div>
            {{-- </div> --}}
        </div>
    </div>
@endsection
