@extends('layouts.app-master')
@section('content')
<div class="app-content p-md-4">
	<header class="ex-header bg-gray">
        <div class="app-container">
             @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="row justify-content-between">
                <div class="col-auto my-4">
                    <div class="page-heading row">
                        <div class="pull-right col-auto">
                            <span class="nav-item">
                                <a class="btn-sm app-btn-secondary" href="{{ route('importdlps.index') }}">
                                    <i class="fas fa-long-arrow-alt-left"></i>
                                    Back
                                </a>
                            </span>
                        </div>
                        <h3 class="col">{{ __('Delvery Plan') }} -> Import</h3>
                    </div>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->

    </header> <!-- end of ex-header -->

    <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
        <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
            aria-controls="orders-all" aria-selected="true"></a>
    </nav>

    <div class="app-container mt-4">
        <div class="row">
            <div class="col-md-12">
                <div id="example3.1" style="height: 800px;"></div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
  google.charts.load("current", {packages:["timeline"]});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {

    var container = document.getElementById('example3.1');
    var chart = new google.visualization.Timeline(container);
    var dataTable = new google.visualization.DataTable();
    dataTable.addColumn({ type: 'string', id: 'Position' });
    dataTable.addColumn({ type: 'string', id: 'Name' });
    dataTable.addColumn({ type: 'date', id: 'Start' });
    dataTable.addColumn({ type: 'date', id: 'End' });
    dataTable.addRows([
        @foreach ($scheduletrucks as $item)
        @php
        $sdatearr = explode("-",$item->start_date);
        $edatearr = explode("-",$item->end_date);
        @endphp
            [ '{{ $item->truck->name ?? "EXT"}}', '{!! $item->order_name !!} {!! $item->status !!}', new Date("{{$item->start_date}}"), new Date("{{$item->end_date}}") ],
        @endforeach
    ]);

     var options = {
      };

    chart.draw(dataTable,options);


  }
</script>

@endsection
