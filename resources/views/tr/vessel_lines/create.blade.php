@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="page-heading row">
                <div class="pull-right col-auto my-2">
                    <a class="btn btn-secondary" href="{{ route('vessel_lines.index') }}">Back</a>
                </div>
                <h1 class="col">{{ __('vessel_lines') }} -> Create</h1>
            </div>

            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>

            <div class="tab-content" id="orders-table-tab-content">
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <form method="POST" action="{{ route('vessel_lines.store') }}">
                            @csrf
                            <div class="row">
                                <div class="col-6">
                                    <label for="name" class="col-form-label text-right">{{ __('Name') }}</label>

                                    <input id="name" type="text"
                                        class="form-control @error('name') is-invalid @enderror" name="name"
                                        value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <label for="shift" class="form-label text-right">{{ __('Status') }}</label>
                                    <select name="status" id="status" class="form-select" required>
                                        <option value="">---เลือก---</option>
                                        <option value="Active">Active</option>
                                        <option value="Inactive">Inactive</option>
                                    </select>
                                    @error('status')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-9">
                                    <label for="desc" class="col-form-label text-md-end">{{ __('รายละเอียด') }}</label>

                                    <div class="col-md-12">
                                        <input id="desc" type="text"
                                            class="form-control @error('desc') is-invalid @enderror" name="desc"
                                            value="{{ old('desc') }}" autocomplete="desc">

                                        @error('desc')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-3 align-bottom">
                                    <label for="submit" class="col-form-label text-md-end">&nbsp;</label>
                                    <div class="col-md-12">
                                    <button type="submit" class="btn btn-success">
                                        {{ __('Save') }}
                                    </button>
                                     </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--//app-card-body-->
                </div>
                <!--//app-card-->
            </div>
            <!--//tab-content-->
        </div>
        <!--//container-fluid-->
    </div>
    <!--//app-content-->
    {{-- <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>      --}}
@endsection
