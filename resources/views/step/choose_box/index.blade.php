@extends('layouts.app-master')

@section('content')
    {{-- <div class="row justify-content-center" style="width: 100%">
        <div class="col-md-11"> --}}
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            <div class="card-body">
                @if(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('error') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('success') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                <div class="row g-3 align-items-center justify-content-between">
                    <div class="col-auto">
                        <div class="row">
                            {{-- <div class="pull-right col-auto my-2">
                                <a class="btn btn-secondary" href="{{ route('to_reuse.index') }}">Back</a>
                            </div> --}}
                            <h1 class="app-page-title col mb-0">การคัด</h1>
                            <div class="pull-right col-auto mb-0">
                                <div class="row" id="show_div">
                                </div>                              
                            </div>
                            {{-- <div class="pull-right col-auto mb-0">
                                <div class="row">
                                    <h1 class="app-page-title col-auto">
                                        <a style="color:red"> ( สินค้าค้างคัด : </a>
                                    </h1> 
                                    <div class="col-auto">         
                                        <div class="row" id="show_div">
                                        </div>
                                    </div>
                                    <h1 class="app-page-title col-auto"><a class="col" style="color:red"> )</a></h1>
                                </div>                              
                            </div> --}}
                        </div>
                    </div>
                    <div class="col-auto">
                        <div class="page-utilities">
                            <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                                <div class="col-auto">
                                    <a class="btn app-btn-secondary" href="{{ route('choose_box.create') }}">
                                        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
                                        Add
                                    </a>
                                </div>
                            </div><!--//row-->
                        </div><!--//table-utilities-->
                    </div><!--//col-auto-->
                </div><!--//row onclick="getURL($key);" -->
            
                <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                    <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                        aria-controls="orders-all" aria-selected="true"></a>
                </nav> 
                
                <div class="col-auto">
                    <form method="GET" action="{{ url('/choose_box') }}" id="myForm" accept-charset="UTF-8" role="search">
                        <div class="row mx-1">
                            <select class="form-select col" name="set_pd" id="set_pd">
                                <option value="">..สินค้า..</option>
                                @foreach($pd as $key => $value)
                                    <option value="{{ $key }}" @if(Request::get('set_pd')==$key) selected @endif>{{ $value }}</option>
                                @endforeach
                            </select>
                            <label for="name" class="col-auto form-label my-2">วันที่ </label>
                            <input type="date" class="form-control form-control-dark col" id="st_search" name="st_search" 
                                value="{{ Request::get('st_search') }}">
                            &nbsp;
                            <label for="name" class="col-auto form-label my-2">ถึงวันที่ </label>
                            <input type="date" class="form-control form-control-dark col" id="ed_search" name="ed_search" 
                                value="{{ Request::get('ed_search') }}">
                            <span class="input-group-append col">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        <br/>
                    </form>
                </div><!--//col-auto-->
                
                <div class="tab-content" id="orders-table-tab-content">                
                    <div class="app-card app-card-orders-table shadow-sm mb-5">
                        <div class="app-card-body">
                            <table class="table app-table-hover mb-0 text-left">
                                <thead>
                                    <tr>
                                        <th class="cell">No.</th>
                                        <th class="cell">วันที่</th>
                                        <th class="cell">กะ</th>
                                        <th class="cell">สินค้า</th>    
                                        {{-- <th class="cell">Pallet No.</th> --}}
                                        <th class="cell">จำนวน</th> 
                                        <th class="cell">นำไปใช้</th>
                                        <th class="cell">หักลบ</th>
                                        <th class="cell">คงเหลือ</th> 
                                        <th class="cell"></th>
                                    </tr>
                                </thead>
                                <tbody>                                
                                    @php 
                                        $count_row = 0;
                                    @endphp
                                    @foreach ($to_show as $kdate=>$vdate)
                                        @foreach ($vdate as $kpd=>$vpd)
                                            @foreach ($vpd as $ksh=>$vsh)
                                                <tr>                                             
                                                    <td class="cell">{{ ++$count_row }}</td>
                                                    <td class="cell">{{ $kdate }}</td>
                                                    <td class="cell">{{ $ksh }}</td>
                                                    <td class="cell">{{ $pd[$kpd] }}</td>
                                                    {{-- <td class="cell">{{ $key->pallet_no }}</td> --}}
                                                    @php
                                                        $arr = array('quantity','stock_qty','lose_qty');
                                                        foreach($arr as $karr=>$varr){
                                                            if(!empty($to_show[$kdate][$kpd][$ksh][$varr]))  
                                                                $set[$varr] = $to_show[$kdate][$kpd][$ksh][$varr];
                                                            else    $set[$varr] = 0;
                                                        }
                                                        $balance = $set['quantity'] - $set['stock_qty'] - $set['lose_qty'];
                                                    @endphp
                                                    <td class="cell">{{ $set['quantity'] }}</td>
                                                    <td class="cell">@if(!empty($set['stock_qty'])){{ $set['stock_qty'] }}@else-@endif</td> 
                                                    <td class="cell">@if(!empty($set['lose_qty'])){{ $set['lose_qty'] }}@else-@endif</td> 
                                                    <td class="cell">@if(!empty($balance)){{ $balance }}@else-@endif</td> 
                                                    <td class="cell">
                                                        <div class="row">
                                                            @if($balance == $set['quantity'])
                                                                <div class="col-auto mx-2">
                                                                    <a class="btn btn-primary" href="{{ url('/choose_box_edit?date='.$kdate.'&shift='.$ksh.'&pd='.$kpd) }}">Edit</a>
                                                                </div>
                                                                <div class="col-auto">
                                                                    <a class="btn btn-danger" href="{{ url('/choose_box_del?page=choose_box&date='.$kdate.'&shift='.$ksh.'&pd='.$kpd) }}"
                                                                        onclick="return confirm('คุณแน่ใจมั้ยว่าต้องการลบการคัด{{ $pd[$kpd] }} วันที่ {{ $kdate }} กะ {{ $ksh }} จำนวน {{ $set['quantity'] }}?')">Delete</a>
                                                                </div>
                                                            @endif
                                                            @if($balance >0)
                                                                <div class="col-auto mx-2">
                                                                    <a class="btn btn-danger" href="{{ url('lose/create?page=choose_box&date='.$kdate.'&shift='.$ksh.'&pd_id='.$kpd.'&bal='.$balance) }}">หักลบ</a>
                                                                </div>
                                                            @endif
                                                            <div class="col-auto mx-2">
                                                                <a class="btn btn-info" href="{{ url('/choose_box_detail?date='.$kdate.'&shift='.$ksh.'&pd='.$kpd) }}">รายละเอียด</a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endforeach
                                    @endforeach
                                </tbody>
                            </table>  
                        </div><!--//app-card-body-->		
                    </div><!--//app-card-->                  
                </div><!--//tab-content-->
            </div>
        </div>
    </div>  
    <script>
        window.onload = set_div
        function set_div() {
            const to_show = <?=json_encode($to_choose_box);?>;
            const pd = <?=json_encode($pd);?>;
            var to_div = new Array();  
            console.log(to_show);
            
            var set_date = new Date();
            Object.keys(pd).forEach(id => {
                to_div[id] = 0;
                if(to_show[id]){

                    Object.keys(to_show[id]).forEach(kdate => {
                        // console.log(key, to_show[id][key]);
                        if(new Date(kdate) <= set_date){
                            // Object.keys(to_show[id][kdate]).forEach(key => {
                                to_div[id] += parseFloat(to_show[id][kdate])
                                // console.log('-', id, to_show[id][key], to_div[id]);
                            // });
                        }                        
                    });
                } 
            });
            console.log(to_div);  
            const result = to_div.filter(Number);
            var text = "";
            if(result.length>0){
                text += '<h1 class="app-page-title col-auto"><a style="color:red"> ( สินค้าค้างคัด : </a></h1><div class="col-auto"><div class="row">';
                
                Object.keys(to_div).forEach(key => {
                    if(to_div[key]>0){
                        console.log(key, to_div[key]);
                        text += '<div class="col-auto"><a class="btn btn-info" id="set_topd['+key+']" name="set_topd['+key+']" href="'+window.location.href+'/create?pd='+key+'&num='+to_div[key]+'">'+pd[key]+'  '+to_div[key]+'</a></div>';
                    }
                }); 
                text += '</div></div><h1 class="app-page-title col-auto"><a class="col" style="color:red"> )</a></h1>';
            }
            document.getElementById("show_div").innerHTML = text;          
        }
    </script>    
@endsection
