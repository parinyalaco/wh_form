@extends('layouts.app-master')

@section('content')
{{-- <div class="container"> --}}
    {{-- <div class="row justify-content-center" style="width: 100%">
        <div class="col-md-11"> --}}
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            {{-- <div class="card"> --}}
                @if(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('error') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('success') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                {{-- <div class="card-header">{{ __('Product') }}</div> --}}

                    <div class="row g-3 mb-4 align-items-center justify-content-between">
                        <div class="col-auto">
                            <div class="row">
                                <div class="pull-right col-auto my-2">
                                    <a class="btn btn-secondary" href="{{ route('choose_box.index') }}">Back</a>
                                </div>
                                <div class="pull-right col-auto">
                                    <h1>รายละเอียด การคัด -></h1>
                                    <div class="mx-5 my-2">
                                        <h3 class="app-page-title">วันที่รับเข้า : <a style="color:gray">{{ $query[1][0]['to_date'] }}</a>  
                                            สินค้า : <a style="color:gray">{{ $query[1][0]['name'] }}</a>  
                                            จำนวน : <a style="color:gray">{{ $query[1][0]['quantity'] }}</a> <br>
                                            Note : <a style="color:gray">{{ $query[1][0]['note'] }}</a> </h3>
                                    </div>
                            </div>
                        </div>
                    </div><!--//row onclick="getURL($key);" -->
                
                    <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                        <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                            aria-controls="orders-all" aria-selected="true"></a>
                    </nav>   

                    @if(count($query[2])>0)
                        <div class="row g-3 mb-4 align-items-center justify-content-between">
                            <h3 class="app-page-title col mb-0">การพับ</h3>
                        </div>                    
                        <div class="tab-content" id="orders-table-tab-content">                
                            <div class="app-card app-card-orders-table shadow-sm mb-5">
                                <div class="app-card-body">
                                    <table class="table app-table-hover mb-0 text-left">
                                        <thead>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell">Date</th>
                                                <th class="cell">shift</th>
                                                <th class="cell">Pallet No</th>
                                                <th class="cell">จำนวน</th>
                                                <th class="cell">หมายเหตุ</th>  
                                                <th class="cell"></th>  
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($to_show['stock'] as $kdate=>$vdate)
                                                @foreach ($vdate as $ksh=>$vsh)
                                                    @foreach ($vsh as $kpl=>$vpl)
                                                        <tr>                                             
                                                            <td class="cell">{{ $loop->iteration }}</td>
                                                            <td class="cell">{{ $kdate }}</td>
                                                            <td class="cell">{{ $ksh }}</td>
                                                            <td class="cell">{{ $kpl }}</td>
                                                            <td class="cell">{{ $to_show['stock'][$kdate][$ksh][$kpl]['quantity'] }}</td>
                                                            <td class="cell">{{ $to_show['stock'][$kdate][$ksh][$kpl]['note'] }}</td> 
                                                            <td class="cell">
                                                                @if(empty($to_show['stock'][$kdate][$ksh][$kpl]['to_use_qty']) && empty($to_show['stock'][$kdate][$ksh][$kpl]['lose_qty'])) 
                                                                    <div class="row">
                                                                        <div class="col-auto">
                                                                            <a class="btn btn-danger" href="{{ url('/stock_del?page=choose_box&id='.$to_show['stock'][$kdate][$ksh][$kpl]['id']) }}"
                                                                                onclick="return confirm('คุณแน่ใจมั้ยว่าต้องการลบการพับ{{ $query[1][0]['name'] }} วันที่ {{ $kdate }} \n Pallet No. {{ $kpl }} จำนวน {{ $to_show['stock'][$kdate][$ksh][$kpl]['quantity'] }}?')">X</a>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </td>                                               
                                                        </tr>
                                                    @endforeach
                                                @endforeach
                                            @endforeach                                            
                                        </tbody>
                                    </table>                           
                                </div><!--//app-card-body-->		
                            </div><!--//app-card-->                  
                        </div><!--//tab-content-->
                    @endif

                    @if(count($query[3])>0)
                        <div class="row g-3 mb-4 align-items-center justify-content-between">
                            <h3 class="app-page-title col mb-0">สูญเสีย</h3>
                        </div>                    
                        <div class="tab-content" id="orders-table-tab-content">                
                            <div class="app-card app-card-orders-table shadow-sm mb-5">
                                <div class="app-card-body">
                                    <table class="table app-table-hover mb-0 text-left">
                                        <thead>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell">Date</th>
                                                {{-- <th class="cell">shift</th> --}}
                                                <th class="cell">สาเหตุ</th>
                                                <th class="cell">จำนวน</th>
                                                <th class="cell">หมายเหตุ</th>                                                  
                                                <th class="cell"></th> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($query[3] as $key)
                                                <tr>                                             
                                                    <td class="cell">{{ $loop->iteration }}</td>
                                                    <td class="cell">{{ $key->to_date }}</td>
                                                    {{-- <td class="cell">{{ $key->shift }}</td> --}}
                                                    <td class="cell">{{ $key->name }}</td>
                                                    <td class="cell">{{ $key->quantity }}</td>
                                                    <td class="cell">{{ $key->note }}</td>  
                                                    <td class="cell">
                                                        <div class="row">
                                                            <div class="col-auto">
                                                                <form action="{{ route('lose.destroy', $key->id) }}" method="POST">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <button type="submit" class="btn btn-danger">X</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </td>                                               
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>                           
                                </div><!--//app-card-body-->		
                            </div><!--//app-card-->                  
                        </div><!--//tab-content-->
                    @endif
                </div>
            {{-- </div> --}}
        </div>
    </div>
{{-- </div> --}}
@endsection
