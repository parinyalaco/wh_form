@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="page-heading row">        
                {{-- <div class="pull-right col-auto my-2">
                    <a class="btn btn-secondary" href="{{ route('stock.index') }}">Back</a>
                </div> --}}
                {{-- <h1 class="col">{{ __('Stock') }} -> ผลตรวจ</h1> --}}
                <div class="row">
                    <div class="pull-right col-auto my-2">
                        <a class="btn btn-secondary" href="{{ route('stock.index') }}">Back</a>
                    </div>
                    <div class="pull-right col-auto">
                        <h1>การพับ -> ผลตรวจ</h1>
                        <div class="mx-5 my-2">
                            <h3 class="app-page-title">วันที่รับเข้า : <a style="color:gray">{{ $query['date'] }}</a>  
                                กะ : <a style="color:gray">{{ $query['shift'] }}</a>  
                                Pallet No. : <a style="color:gray">{{ $query['pallet_no'] }}</a> <br>
                                สินค้า : <a style="color:gray">{{ $pd_name[0]['name'] }}</a>  
                                รอผลตรวจ : <a style="color:gray">{{ $query['bal_wait'] }}</a> </h3>
                            {{-- <h3>Pallet No. : {{ $query['pallet_no'] }}</h3> --}}
                        </div>
                    </div>
                </div>
            </div>
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>  
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <form method="POST" action="{{ route('stock.process') }}">
                            @csrf
                            @method('PUT')
                            <input id="bal_wait" type="hidden" name="bal_wait" value="{{ $query['bal_wait'] }}">
                            <input id="date" type="hidden" name="date" value="{{ $query['date'] }}">
                            <input id="shift" type="hidden" name="shift" value="{{ $query['shift'] }}">
                            <input id="pallet_no" type="hidden" name="pallet_no" value="{{ $query['pallet_no'] }}">
                            <input id="pd" type="hidden" name="pd" value="{{ $query['pd'] }}">
                            <div class="row mb-3">
                                <label for="to_date" class="col-md-4 col-form-label text-md-end">{{ __('วันที่') }}</label>
    
                                <div class="col-md-6">
                                    <input id="to_date" type="date" class="form-control @error('to_date') is-invalid @enderror" 
                                        name="to_date" value="@if(empty(old('to_date'))){{ date('Y-m-d') }}@else{{ old('to_date') }}@endif" 
                                        required autocomplete="to_date" autofocus>
    
                                    @error('to_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>  
                            <div class="row mb-3">
                                <label for="pass" class="col-md-4 col-form-label text-md-end">{{ __('ผ่าน') }}</label>
    
                                <div class="col-md-6">
                                    <input id="pass" type="text" class="form-control @error('pass') is-invalid @enderror" 
                                        name="pass" value="{{ old('pass') }}" autocomplete="pass" required>
    
                                    @error('pass')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div><div class="row mb-3">
                                <label for="not_pass" class="col-md-4 col-form-label text-md-end">{{ __('ไม่ผ่าน') }}</label>
    
                                <div class="col-md-6">
                                    <input id="not_pass" type="text" class="form-control @error('not_pass') is-invalid @enderror" 
                                        name="not_pass" value="{{ old('not_pass') }}" autocomplete="not_pass" required>
    
                                    @error('not_pass')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="note" class="col-md-4 col-form-label text-md-end">{{ __('หมายเหตุ') }}</label>
    
                                <div class="col-md-6">
                                    <input id="note" type="text" class="form-control @error('note') is-invalid @enderror" 
                                        name="note" value="{{ old('note') }}" autocomplete="note">
    
                                    @error('note')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label class="col-md-4"></label>
    
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>                          

                        </form>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->     
@endsection
