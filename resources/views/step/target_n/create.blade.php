@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="page-heading row">        
                <div class="pull-right col-auto my-2">
                    <a class="btn btn-secondary" href="{{ route('target.index') }}">Back</a>
                </div>
                {{-- <h1 class="col">{{ __('Product') }} -> Create</h1> --}}
                <h1 class="col">Target FS -> Create</h1>
            </div>
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>  
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <form method="POST" action="{{ route('target.store') }}">
                            @csrf
                                                        
                            <div class="row mb-3">
                                <label for="shift" class="col-md-4 col-form-label text-md-end">{{ __('กะ') }}</label>
    
                                <div class="col-md-6">
                                    <select class="form-select col" name="shift" id="shift" required onchange="set_time(this.value);">
                                        <option value="">..กรุณาเลือก..</option>
                                        @foreach($shift as $key=>$value)
                                            <option value="{{ $key }}"@if(old('shift')==$key) selected @endif>{{ $key }}</option>
                                        @endforeach
                                    </select>
    
                                    @error('shift')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="start_time" class="col-md-4 col-form-label text-md-end">{{ __('วัน เวลา') }}</label>
    
                                <div class="col-md-3">
                                    <input id="start_time" type="datetime-local" name="start_time" 
                                        class="form-control @error('start_time') is-invalid @enderror" 
                                        value="{{ old('start_time') }}" required autocomplete="start_time" onchange="change_date(this.value);">
                                        {{-- onchange="st_date(this.value);" --}}
                                    
                                    @error('start_time')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-3">
                                    <input id="end_time" type="datetime-local" name="end_time" 
                                        class="form-control @error('end_time') is-invalid @enderror" 
                                        value="{{ old('end_time') }}" required autocomplete="end_time">

                                    @error('end_time')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div> 
                            <div class="row mb-3">
                                <label for="note" class="col-md-4 col-form-label text-md-end">{{ __('หมายเหตุ') }}</label>
    
                                <div class="col-md-6">
                                    <input id="note" type="text" class="form-control @error('note') is-invalid @enderror" 
                                        name="note" value="{{ old('note') }}" autocomplete="note">
    
                                    @error('note')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label class="col-md-4"></label>
    
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->   
    <script>
        function set_time(set) {
            const shift = <?=json_encode($shift);?>;
            console.log(shift);
            var to_day = new Date();  
            var to_date = to_day.getDate();  
            var this_day = String(to_date).padStart(2, '0')  
            var to_month = to_day.getMonth()+1;  
            var this_month = String(to_month).padStart(2, '0')
            var this_year = to_day.getFullYear(); 

            var st_time = shift[set]['st'].split(":");
            var st_h = st_time[0] 
            var st_m = st_time[1]

            var ed_time = shift[set]['ed'].split(":");
            var ed_h = ed_time[0] 
            var ed_m = ed_time[1]
            console.log(ed_h);
            var to_add = new Date(); 
            if(ed_h<12){
               to_add.setDate(to_add.getDate() + 1)  
            } 
            var to_add_d = to_add.getDate(); 
            var add_day = String(to_add_d).padStart(2, '0') 
            var to_add_m = to_add.getMonth()+1;  
            var add_month = String(to_add_m).padStart(2, '0')
            var add_year = to_add.getFullYear();

            st_date = this_year+'-'+this_month+'-'+this_day+'\T'+st_h+':'+st_m
            ed_date = add_year+'-'+add_month+'-'+add_day+'\T'+ed_h+':'+ed_m 

            document.getElementById("start_time").value = st_date
            document.getElementById("end_time").value = ed_date
        }
        function change_date(set) {
            var shift = document.getElementById("shift").value
            var ed_set = document.getElementById("end_time").value
            console.log(ed_set);
            var ed_time = new Date(ed_set);
            var ed_h = ed_time.getHours(); 
            var set_h = String(ed_h).padStart(2, '0') 
            var ed_m = ed_time.getMinutes();
            var set_m = String(ed_m).padStart(2, '0')

            var to_day = new Date(set); 
            if(shift==='C'){
                to_day.setDate(to_day.getDate() + 1)  
            }  
            var to_date = to_day.getDate(); 
            var this_day = String(to_date).padStart(2, '0')  
            var to_month = to_day.getMonth()+1;  
            var this_month = String(to_month).padStart(2, '0')
            var this_year = to_day.getFullYear(); 

            ed_set = this_year+'-'+this_month+'-'+this_day+'\T'+set_h+':'+set_m

            document.getElementById("end_time").value = ed_set
        }
    </script>     
@endsection
