@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif

            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <div class="row">
                        <div class="pull-right col-auto my-2">
                            <a class="btn btn-secondary" href="{{ route('target.index') }}">Back</a>
                        </div>
                        <div class="pull-right col-auto">
                            <h1>Target FS -> รายละเอียด</h1>
                            <div class="my-2">
                                <h3>วัน เวลา : 
                                    <a style="color:gray">
                                        {{ date('Y-m-d', strtotime($query[2]->start_time)) }} 
                                        ({{ date('H:i', strtotime($query[2]->start_time)) }}
                                        -{{ date('H:i', strtotime($query[2]->end_time)) }})
                                    </a> 
                                    กะ : <a style="color:gray">{{ $query[2]->shift }}</a></h3>
                                <h3>Note : <a style="color:gray">{{ $query[2]->note }}</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pull-right col-auto mt-auto">
                    <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                        <a class="btn btn-danger col mx-4" href="#" onclick="clear_inupt();">Clear</a>
                        <a class="btn btn-success col" href="#" data-bs-target="#exampleModal" data-bs-toggle="modal"
                            onclick="clone_input('{{ $query[2]->id }}')">
                            Clone
                        </a> 
                        {{-- <button type="button" class="btn btn-warning" onclick="show_input({{ $loadm->id }},'{{ $loadm->order_no }}')" 
                            data-bs-toggle="modal" data-bs-target="#exampleModal" 
                            data-mdb-whatever="{{ $loadm->id }}">
                            คืนสถานะ
                        </button>                              --}}
                    </div><!--//row-->
                </div>
            </div><!--//row onclick="getURL($key);" -->

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <form action="{{ route('target.clone_data') }}" method="POST">
                        @csrf
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Clone จาก</h5>
                                <div id="test"></div>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="row mb-3">
                                    <label for="date" class="col-md-4 col-form-label text-md-end">{{ __('วันที่') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="date" type="date" class="form-control @error('date') is-invalid @enderror" 
                                            name="date" value="{{ date('Y-m-d') }}" required autocomplete="date" autofocus>
                                        
                                        @error('date')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row mb-3">                                
                                    <label for="shift" class="col-md-4 col-form-label text-md-end">{{ __('กะ') }}</label>
        
                                    <div class="col-md-6">
                                        <select class="form-select col" name="shift" id="shift" required>
                                            <option value="">..กรุณาเลือก..</option>
                                            <option value="B">B</option>
                                            <option value="C">C</option>
                                        </select>

                                        @error('shift')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                {{-- <textarea id="reason" class="form-control col" name="reason" required></textarea> --}}
                                <input type="hidden" id="tg_id" class="form-control col" name="tg_id">
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Clone</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>   

            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <form method="GET" name="form" action="{{ route('target.save_det', $query[2]->id) }}">
                            @csrf             
                            <table class="table app-table-hover mb-0 text-left">
                                <thead>
                                    <tr style="text-align: center">
                                        <th class="cell" rowspan="2"></th>
                                        <th class="cell" rowspan="2">งาน</th>
                                        <th class="cell" colspan="{{ count($data['store']) }}">แรงงาน</th>                               
                                        <th class="cell" rowspan="2">เป้าหมาย</th>
                                        <th class="cell" rowspan="2"></th>  
                                    </tr>
                                    <tr style="text-align: center">
                                        @foreach($data['store'] as $key=>$value)
                                            <th class="cell">{{ $value }}</th>
                                        @endforeach            
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data['step'] as $kstep=>$vstep)
                                        <tr>                                             
                                            <td class="cell">{{ $loop->iteration }}</td>
                                            <td class="cell">
                                                {{ $vstep }}
                                            </td>
                                            @foreach($data['store'] as $key=>$value)
                                                <td class="cell">
                                                    <input class="form-control" type="number" id='store_id[{{ $kstep }}][{{ $key }}]' 
                                                        name='store_id[{{ $kstep }}][{{ $key }}]' onClick="this.select();" 
                                                        value="@if(!empty($data['tg_man'][$kstep][$key])){{ $data['tg_man'][$kstep][$key] }}@endif" 
                                                        onchange="cal_num('{{ $kstep }}');">
                                                </td>
                                            @endforeach                                          
                                            <td class="cell">
                                                <input class="form-control" type="number" id='step_id[{{ $kstep }}]' 
                                                    name='step_id[{{ $kstep }}]' onClick="this.select();"
                                                    value="@if(!empty($data['tg_pd'][$kstep])){{ $data['tg_pd'][$kstep] }}@endif" 
                                                    onchange="cal_num('{{ $kstep }}');">
                                                <input type="hidden" id="sum_step[{{ $kstep }}]" value="">
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>                                             
                                        <td class="cell"></td>
                                        <td class="cell"></td>
                                        @foreach($data['store'] as $key=>$value)
                                            <td class="cell"></td>
                                        @endforeach                                          
                                        <td class="cell">
                                            <button type="button" class="btn btn-success" onclick="chk_val();">{{ __('Save') }}</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>                               
                        </form>                        
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->

        </div>
    </div>
    <script>
        function cal_num(step) {
            const store = <?=json_encode($data['store']);?>;
            // console.log(store.length)
            var tg = document.getElementById("step_id["+step+"]").value
            var man = 0
            Object.keys(store).forEach(id => {
                var st = document.getElementById("store_id["+step+"]["+id+"]").value
                console.log('['+step+']['+id+']='+st);
                if(st){
                    man += Number(st)
                }
            }); 
            var cal = man*tg
            console.log('man=',man,'tg=',tg,'cal=',cal);
            if(cal){
                document.getElementById("sum_step["+step+"]").value = cal
            }else{
                document.getElementById("sum_step["+step+"]").value = 'Nan'
            }
        }

        function chk_val() {
            const step = <?=json_encode($data['step']);?>;
            var sum_tg = true
            Object.keys(step).forEach(id => {
                var chk = document.getElementById("sum_step["+id+"]").value 
                if(chk === 'Nan'){
                    alert("กรุณาระบุค่าในงาน "+step[id]+" ให้ครบถ้วน!");
                    // return false;
                    sum_tg = false
                }                  
            });
            if(sum_tg===true){
                document.form.submit();
            }
        }  

        function clear_inupt() {
            const step = <?=json_encode($data['step']);?>;
            const store = <?=json_encode($data['store']);?>;
            Object.keys(step).forEach(id => {
                document.getElementById("sum_step["+id+"]").value = ''
                document.getElementById("step_id["+id+"]").value = ''
                Object.keys(store).forEach(store_id => {
                    document.getElementById("store_id["+id+"]["+store_id+"]").value = ''
                });
            });
        }

        function clone_input(id) {
            console.log(id);
            document.getElementById('tg_id').value = id;
            // let box = document.getElementById('test');
            // box.innerText = 'Clone จากวันที่ '+order;
            // document.getElementById('test').value = 'กรุณาระบุเหตุผลที่ต้องการคืนค่า ใบ order ที่ '+order;
        }
    </script>     
@endsection
