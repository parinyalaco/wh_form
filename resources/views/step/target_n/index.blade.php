@extends('layouts.app-master')

@section('content')
    {{-- <div class="row justify-content-center" style="width: 100%">
        <div class="col-md-11"> --}}
    <div class="app-content p-md-4">
        <div class="app-container-xl">

            <div class="card-body">
                @if(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('error') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('success') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                <div class="row g-3 align-items-center justify-content-between">
                    <div class="col-auto">
                        <div class="row">
                            <h1 class="app-page-title col mb-0">Target FS</h1>
                        </div>
                    </div>
                    <div class="col-auto">
                        <div class="page-utilities">
                            <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                                <div class="col-auto">
                                    <a class="btn app-btn-secondary" href="{{ route('target.create') }}">
                                        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
                                        Add
                                    </a>
                                </div>
                            </div><!--//row-->
                        </div><!--//table-utilities-->
                    </div><!--//col-auto-->
                </div><!--//row onclick="getURL($key);" -->
            
                <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                    <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                        aria-controls="orders-all" aria-selected="true"></a>
                </nav>            
                
                <div class="col-auto">
                    <form method="GET" action="{{ route('target.index') }}" accept-charset="UTF-8"
                    class="table-search-form row gx-1 align-items-center" role="search">
                        <div class="form-group row mb-3">
                            <select class="form-select col" name="set_sh" id="set_sh">
                                <option value="">..กะ..</option>
                                @foreach($sh as $key => $value)
                                    <option value="{{ $key }}" @if(Request::get('set_sh')==$key) selected @endif>{{ $value }}</option>
                                @endforeach
                            </select>
                            <label for="name" class="col-auto form-label">วันที่ </label>
                            <div class="col-auto">
                                <input type="date" class="form-control" id="st_search" name="st_search" @if(!empty($st_search)) value="{{ date("Y-m-d", strtotime($st_search)) }}" @endif>											
                            </div>
                            <label for="name" class="col-auto form-label">ถึงวันที่ </label>
                            <div class="col-auto">
                                <input type="date" class="form-control" id="ed_search" name="ed_search" @if(!empty($ed_search)) value="{{ date("Y-m-d", strtotime($ed_search)) }}" @endif>											
                            </div>
                            <div class="col-auto">
                                <button type="submit" class="btn btn-success">Search</button>
                            </div>
                        </div>
                    </form>
                    
                </div><!--//col-->

                <div class="tab-content" id="orders-table-tab-content">                
                    <div class="app-card app-card-orders-table shadow-sm mb-5">
                        <div class="app-card-body">
                            <table class="table app-table-hover mb-0 text-left">
                                <thead>
                                    <tr>
                                        <th class="cell">No.</th>
                                        <th class="cell">วันที่</th>
                                        <th class="cell">เวลา</th>
                                        <th class="cell">กะ</th>    
                                        <th class="cell">จำนวนคน</th>
                                        <th class="cell">จำนวนงาน</th>
                                        <th class="cell"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($query as $key)    
                                            <tr>                                             
                                                <td class="cell">{{ $loop->iteration }}</td>
                                                <td class="cell">{{ date('Y-m-d', strtotime($key->start_time)) }}</td>
                                                <td class="cell">{{ date('H:i', strtotime($key->start_time)) }} - {{ date('H:i', strtotime($key->end_time)) }}</td>
                                                <td class="cell">{{ $key->shift }}</td>
                                                <td class="cell">
                                                    @if(!empty($to_target[$key->id])){{ array_sum($to_target[$key->id]) }}@endif
                                                </td>
                                                <td class="cell">
                                                    @if(!empty($to_target[$key->id])){{ count($to_target[$key->id]) }}@endif
                                                </td>
                                                <td class="row">
                                                    <div class="col-auto mx-2">
                                                        <a class="btn btn-primary" href="{{ route('target.edit', $key->id) }}">Edit</a>
                                                    </div>
                                                    <div class="col-auto">
                                                        <form action="{{ route('target.destroy', $key->id) }}" method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class="btn btn-danger">Delete</button>
                                                        </form>
                                                    </div>
                                                    <div class="col-auto mx-2">
                                                        <a class="btn btn-info" href="{{ route('target.show', $key->id) }}">รายละเอียด</a>
                                                    </div>
                                                    <div class="col-auto mx-2">
                                                        <a class="btn btn-info" href="{{ route('target.report', $key->id) }}">รายงาน</a>
                                                    </div>
                                                </td>
                                            </tr>
                                        {{-- @endforeach    --}}
                                    @endforeach
                                </tbody>
                            </table>  
                        </div><!--//app-card-body-->		
                    </div><!--//app-card-->                  
                </div><!--//tab-content-->
            </div>
        </div>
    </div>    
@endsection
