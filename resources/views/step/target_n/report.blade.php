@extends('layouts.app-master')
{{-- <style>
    table, th, td {
        border: 1px solid black;
    }
</style> --}}
@section('content')
    <div class="app-content p-md-4">
        <div class="app-content">
            <div class="app-container-xl">
                {{-- @if(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('error') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('success') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif --}}
                <div class="page-heading row"> 
                    <div class="pull-right col-auto">
                        <span class="nav-item">
                            <a class="btn-sm app-btn-secondary" href="{{ route('target.index') }}">
                                <i class="fas fa-long-arrow-alt-left"></i>
                                Back
                            </a>
                        </span>
                    </div>
                    <h3 class="col">{{ __('Target FS') }} -> Report : เป้าการทำงาน ประจำวันที่ {{ $th_date }}</h3>
                </div>
                <nav id="" class="orders-table-tab app-nav-tabs flex-sm-row mb-4">	
                    <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" 
                        href="#" role="tab" aria-controls="orders-all" aria-selected="true"></a>
                </nav>			   
                
                <div class="tab-content" id="orders-table-tab-content">
                    <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
                        <div class="app-card app-card-orders-table shadow-sm mb-5">
                            <div class="app-card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="tb_show">
                                        <tr style="text-align: center">
                                            <th class="cell" rowspan="2" style="width: 5%"></th>
                                            <th class="cell" rowspan="2">งาน</th>  
                                            <th class="cell" rowspan="2">ชนิด</th>
                                            <th class="cell" colspan="{{ count($store) }}">Manpower</th>                                              
                                            <th class="cell" rowspan="2">เป้า</th>                                              
                                            <th class="cell" rowspan="2">Action</th>                                              
                                            <th class="cell" rowspan="2">diff</th>                                              
                                            <th class="cell" rowspan="2">%</th>  
                                        </tr>
                                        <tr style="text-align: center">
                                            @foreach($store as $key => $value)
                                                <th class="cell" style="width: 5%">{{ $store[$key] }}</th>
                                            @endforeach
                                        </tr>                                        
                                        @php
                                            $i = 0;
                                        @endphp
                                        @if(!empty($data['step_tg']))
                                            @foreach($data['step_tg'] as $key => $value)
                                                @php
                                                    $a = 0;
                                                    $b = 0;
                                                @endphp
                                                <tr>
                                                    <td class="cell" rowspan="{{ $row_span[$key] }}">{{ ++$i }}</td>
                                                    <td class="cell" rowspan="{{ $row_span[$key] }}">{{ $step_tg['name'][$key] }}</td>
                                                    {{-- @foreach($step_tg['ref_to'][$key] as $kstep => $vstep) --}}
                                                    <td class="cell">
                                                        @if(!empty($to_pd[$key]))
                                                            @foreach($to_pd[$key] as $kpd => $vpd)  
                                                                @if($a==0)                                                      
                                                                    {{ $pd[$kpd] }}
                                                                @endif
                                                                @php
                                                                    $a++;
                                                                @endphp
                                                            @endforeach
                                                        @endif
                                                    </td>   
                                                    
                                                    @foreach($store as $kstore => $vstore)
                                                        <td class="cell" rowspan="{{ $row_span[$key] }}" style="text-align: right">
                                                            @if(!empty($data['store'][$key][$kstore]))
                                                                {{ $data['store'][$key][$kstore] }}
                                                            @endif
                                                        </td>
                                                    @endforeach
                                                    <td class="cell" rowspan="{{ $row_span[$key] }}" style="text-align: right;">{{ number_format($value) }}</td>
                                                    <td class="cell" style="text-align: right;">
                                                        @if(!empty($to_pd[$key]))
                                                            @foreach($to_pd[$key] as $kpd => $vpd) 
                                                                @if($b==0)
                                                                    {{ number_format($vpd) }}
                                                                @endif
                                                                @php
                                                                    $b++;
                                                                @endphp
                                                            @endforeach
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    @php
                                                        if(!empty($to_pd[$key]))    $diff = $value-array_sum($to_pd[$key]);
                                                        else $diff = 0;
                                                    @endphp
                                                    <td class="cell" rowspan="{{ $row_span[$key] }}" style="text-align: right; @if($diff<0) color: red @endif">
                                                        @if(!empty($diff))
                                                            {{ number_format($diff) }}
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    @php
                                                        if(!empty($to_pd[$key]))   $p_diff = ($value-array_sum($to_pd[$key]))/$value*100;
                                                        else $p_diff = 0;
                                                    @endphp
                                                    <td class="cell" rowspan="{{ $row_span[$key] }}" style="text-align: right;">
                                                        @if(!empty($p_diff))
                                                            {{ number_format($p_diff,2) }} %
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    @php
                                                        $a = 0;
                                                        $b = 0;
                                                    @endphp 
                                                </tr> 
                                                @if(!empty($to_pd[$key]))
                                                    @foreach($to_pd[$key] as $kpd => $vpd)   
                                                        @if($a>0) 
                                                            <tr>                                                                                                                 
                                                                <td class="cell">{{ $pd[$kpd] }}</td> 
                                                                <td class="cell" style="text-align: right;">{{ number_format($vpd) }}</td>                                                                    
                                                            </tr>
                                                        @endif
                                                        @php
                                                            $a++;
                                                        @endphp
                                                    @endforeach                                                       
                                                @endif       
                                            @endforeach
                                        @else
                                            <tr style="text-align: center">
                                                <th class="cell" colspan="8">ไม่พบข้อมูล</th>
                                            </tr>
                                        @endif
                                    </table>
                                </div><!--//table-responsive-->                            
                            </div><!--//app-card-body-->          
                        </div><!--//app-card-->                    
                    </div><!--//tab-pane-->			        
                </div><!--//tab-content-->
            </div><!--//container-fluid-->
        </div><!--//app-content-->	    
        
    </div><!--//app-wrapper-->    
@endsection