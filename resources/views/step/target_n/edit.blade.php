@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="page-heading row">        
                <div class="pull-right col-auto my-2">
                    <a class="btn btn-secondary" href="{{ route('target.index') }}">Back</a>
                </div>
                {{-- <h1 class="col">{{ __('Product') }} -> Create</h1> --}}
                <h1 class="col">Target FS -> Edit</h1>
                    {{-- <div class="pull-right col-auto my-2">
                        <h3>วันที่รับเข้า : {{ $query2[0]['to_date'] }}</h3>
                        <h3>สินค้า : {{ $query2[0]['name'] }}</h3>
                    </div> --}}
            </div>
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>  
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <form method="POST" action="{{ route('target.update', $query->id) }}">
                            @csrf
                            @method('PUT')                            
                            <div class="row mb-3">
                                <label for="shift" class="col-md-4 col-form-label text-md-end">{{ __('กะ') }}</label>
    
                                <div class="col-md-6">
                                    <select class="form-select col" name="shift" id="shift" required onchange="set_time(this.value);">
                                        <option value="">..กรุณาเลือก..</option>
                                        @foreach($shift as $key=>$value)
                                            <option value="{{ $key }}"@if($query->shift==$key) selected @endif>{{ $key }}</option>
                                        @endforeach
                                    </select>
    
                                    @error('shift')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="start_time" class="col-md-4 col-form-label text-md-end">{{ __('วัน เวลา') }}</label>
    
                                <div class="col-md-3">
                                    <input id="start_time" name="start_time" type="datetime-local" 
                                        class="form-control @error('start_time') is-invalid @enderror" 
                                        value="{{ date('Y-m-d\TH:i', strtotime($query->start_time)) }}" 
                                        required autocomplete="start_time" onchange="change_date(this.value);">
                                    
                                    @error('start_time')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-3">
                                    <input id="end_time" name="end_time" type="datetime-local" 
                                        class="form-control @error('end_time') is-invalid @enderror" 
                                        value="{{ date('Y-m-d\TH:i', strtotime($query->end_time)) }}" 
                                        required autocomplete="end_time">

                                    @error('end_time')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div> 
                            <div class="row mb-3">
                                <label for="note" class="col-md-4 col-form-label text-md-end">{{ __('หมายเหตุ') }}</label>
    
                                <div class="col-md-6">
                                    <input id="note" type="text" class="form-control @error('note') is-invalid @enderror" 
                                        name="note" value="{{ $query->note }}" autocomplete="note">
    
                                    @error('note')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label class="col-md-4"></label>
    
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->   
    <script>
        function set_time(set) {
            const shift = <?=json_encode($shift);?>;
            // console.log(shift);
            var set_st = document.getElementById("start_time").value
            var to_day = new Date(set_st);  
            var to_date = to_day.getDate();  
            var this_day = String(to_date).padStart(2, '0')  
            var to_month = to_day.getMonth()+1;  
            var this_month = String(to_month).padStart(2, '0')
            var this_year = to_day.getFullYear(); 

            var st_time = shift[set]['st'].split(":");
            var st_h = st_time[0] 
            var st_m = st_time[1]

            var ed_time = shift[set]['ed'].split(":");
            var ed_h = ed_time[0] 
            var ed_m = ed_time[1]
            console.log(ed_h);
            var to_add = new Date(set_st); 
            if(ed_h<12){
               to_add.setDate(to_add.getDate() + 1)  
            } 
            var to_add_d = to_add.getDate(); 
            var add_day = String(to_add_d).padStart(2, '0')  
            var to_add_m = to_add.getMonth()+1;  
            var add_month = String(to_add_m).padStart(2, '0')
            var add_year = to_add.getFullYear();

            st_date = this_year+'-'+this_month+'-'+this_day+'\T'+st_h+':'+st_m
            ed_date = add_year+'-'+add_month+'-'+add_day+'\T'+ed_h+':'+ed_m 

            document.getElementById("start_time").value = st_date
            document.getElementById("end_time").value = ed_date
        }

        function change_date(set) {
            // const shift = <?=json_encode($shift);?>;
            // const st_date = <?=json_encode($query->start_time);?>;
            // console.log(set);
            var shift = document.getElementById("shift").value
            var ed_set = document.getElementById("end_time").value
            console.log(ed_set);
            var ed_time = new Date(ed_set);
            var ed_h = ed_time.getHours(); 
            var set_h = String(ed_h).padStart(2, '0') 
            var ed_m = ed_time.getMinutes();
            var set_m = String(ed_m).padStart(2, '0')

            var to_day = new Date(set); 
            if(shift==='C'){
                to_day.setDate(to_day.getDate() + 1)  
            }  
            var to_date = to_day.getDate(); 
            var this_day = String(to_date).padStart(2, '0')  
            var to_month = to_day.getMonth()+1;  
            var this_month = String(to_month).padStart(2, '0')
            var this_year = to_day.getFullYear(); 

            // var st_time = shift[set]['st'].split(":");
            // var st_h = st_time[0] 
            // var st_m = st_time[1]

            // var ed_time = shift[set]['ed'].split(":");
            // var ed_h = ed_time[0] 
            // var ed_m = ed_time[1]
            // // console.log(ed_h);
            // var to_add = new Date(st_date); 
            // if(ed_h<12){
            //     to_add.setDate(to_add.getDate() + 1)  
            // } 
            // var add_day = to_add.getDate();  
            // var to_add_m = to_add.getMonth()+1;  
            // var add_month = String(to_add_m).padStart(2, '0')
            // var add_year = to_add.getFullYear();

            ed_set = this_year+'-'+this_month+'-'+this_day+'\T'+set_h+':'+set_m
            // ed_set = add_year+'-'+add_month+'-'+add_day+'\T'+ed_h+':'+ed_m 

            // document.getElementById("start_time").value = st_set
            document.getElementById("end_time").value = ed_set
        }
    </script>  
@endsection
