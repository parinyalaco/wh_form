@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="page-heading row">        
                <div class="pull-right col-auto my-2">
                    <a class="btn btn-secondary" 
                        href="@if($query['page']=='index'){{ route('to_reuse.index') }}
                        @elseif($query['page']=='show'){{ route('to_reuse.show_det',['date'=>$query['date'],'pd_id'=>$query['pd_id']]) }}
                        @elseif($query['page']=='wash'){{ route('wash.index') }}
                        @elseif($query['page']=='dry'){{ route('dry.index') }}
                        @elseif($query['page']=='stock'){{ route('stock.index') }}
                        @elseif($query['page']=='use'){{ route('to_use.index') }}
                        @elseif($query['page']=='shine'){{ route('shine.index') }}
                        @elseif($query['page']=='choose_box'){{ route('choose_box.index') }}@endif">
                        Back
                    </a>
                </div>
                <h1 class="col">
                    {{ __('หักลบ') }} -> Create
                    <a style="color:gray" class="app-page-title">(หักลบได้ไม่เกิน : {{ $query['bal'] }})</a>
                </h1>
            </div>
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>  
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <form method="POST" action="{{ route('lose.store') }}">
                            @csrf
                            <input type="hidden" id="page" name="page" value="{{ $query['page'] }}">
                            <input type="hidden" id="date" name="date" value="{{ $query['date'] }}">
                            <input type="hidden" id="pd_id" name="pd_id" value="{{ $query['pd_id'] }}">
                            <input type="hidden" id="balance" name="balance" value="{{ $query['bal'] }}">

                            @if($query['page']=='show')
                                <input type="hidden" id="id" name="id" value="{{ $query['id'] }}">
                            @endif
                            @if(!empty($query['shift']))
                                <input type="hidden" id="shift" name="shift" value="{{ $query['shift'] }}">
                            @endif
                            @if(!empty($query['pallet_no']))
                                <input type="hidden" id="pallet_no" name="pallet_no" value="{{ $query['pallet_no'] }}">
                            @endif
                            @if($query['page']=='dry')
                                <input type="hidden" id="dry_no" name="dry_no" value="{{ $query['dry_no'] }}">
                                <input type="hidden" id="round_no" name="round_no" value="{{ $query['round_no'] }}">
                            @endif

                            @if($query['page']=='stock')   
                                <input type="hidden" id="type" name="type" value="{{ $query['type'] }}">
                            @endif

                            <div class="row mb-3">
                                <label for="lose_type_id" class="col-md-4 col-form-label text-md-end">{{ __('หักลบจาก') }}</label>
    
                                <div class="col-md-6">
                                    <select class="form-select col" name="lose_type_id" id="lose_type_id" required>
                                        <option value="">..กรุณาเลือก..</option>
                                        @foreach($lt as $key)
                                            <option value="{{ $key->id }}"@if(old('lose_type_id')==$key) selected @endif>{{ $key->name }}</option>
                                        @endforeach
                                    </select>
    
                                    @error('lose_type_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="quantity" class="col-md-4 col-form-label text-md-end">{{ __('จำนวน') }}</label>
    
                                <div class="col-md-6">
                                    <input id="quantity" type="number" class="form-control @error('quantity') is-invalid @enderror" 
                                        name="quantity" value="0" max="{{ $query['bal'] }}" min="1" autocomplete="quantity" required>
    
                                    @error('quantity')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="note" class="col-md-4 col-form-label text-md-end">{{ __('หมายเหตุ') }}</label>
    
                                <div class="col-md-6">
                                    <input id="note" type="text" class="form-control @error('note') is-invalid @enderror" 
                                        name="note" value="{{ old('note') }}" autocomplete="note">
    
                                    @error('note')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label class="col-md-4"></label>
    
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->   
    {{-- <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>      --}}
@endsection
