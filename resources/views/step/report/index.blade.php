@extends('layouts.app-master')

@section('content')
    {{-- <div class="row justify-content-center" style="width: 100%">
        <div class="col-md-11"> --}}
    <div class="app-content p-md-4">
        <div class="app-container-xl">

            <div class="card-body">
                @if(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('error') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('success') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                <div class="row g-3 align-items-center justify-content-between">
                    <div class="col-auto">
                        <div class="row">
                            <h1 class="app-page-title col mb-0">รายงาน</h1>
                        </div>
                    </div>
                </div>
            
                <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row my-3">
                    <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                        aria-controls="orders-all" aria-selected="true"></a>
                </nav>  			   
                
                <div class="tab-content" id="orders-table-tab-content">
                    <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
                        <div class="app-card app-card-orders-table shadow-sm mb-5">
                            
                            <form method="GET" action="{{ route('fs_report.fs_to_report') }}" accept-charset="UTF-8"
                                class="table-search-form row gx-1 align-items-center" role="search">
                                <div class="app-card-body">
                                    <div class="table-responsive">
                                        <table class="table app-table-hover mb-0 text-left yajra-datatable" id="tb_show">
                                            <thead>
                                                <tr>
                                                    <th class="cell" style="width: 5%"></th>
                                                    <th class="cell" style="text-align:right; width: 20%;">ประเภทรายงาน : </th>
                                                    <th class="cell" colspan="3" style="width: 65%">
                                                        <select name="report_id" class="form-select col" required onchange="myFunction(this.value);">
                                                            <option value="" selected>==== เลือกประเภทรายงาน ====</option>
                                                            <option value="1">1.ยอดสินค้าจากคลังสินค้า ณ ปัจจุบัน</option>
                                                            <option value="2">2.ประวัตินำจ่ายของแต่ละตำแหน่ง</option>
                                                            {{-- <option value="3">3.กราฟแสดงรายการรับ</option>
                                                            <option value="4">4.รายงานสรุปการสุ่ม</option>
                                                            <option value="5">5.กราฟแสดงข้อมูลตามประเภทสินค้า</option> --}}
                                                        </select>
                                                    </th>
                                                    <th class="cell"style="width: 10%"></th>
                                                </tr>
                                                <tr>
                                                    <th class="cell"></th>
                                                    <th class="cell" style="text-align:right;"><div id="date_st">วันที่ : </div></th>
                                                    <th class="cell" style="width: 30%">
                                                        <input type="date" class="form-control col" id="st_date" name="st_date">
                                                        {{-- <input type="month" class="form-control col" id="month_date" name="month_date" value="{{ date("Y-m") }}" style="display: none"> --}}
                                                    </th>
                                                    <th class="cell" style="text-align: center" style="width: 6%">
                                                        <div id="date_to">ถึงวันที่ : </div>
                                                    </th>
                                                    <th class="cell" style="width: 29%">
                                                        <input type="date" class="form-control col" id="ed_date" name="ed_date">
                                                    </th>
                                                    <th class="cell"></th>
                                                </tr>
                                                <tr>
                                                    <th class="cell"></th>
                                                    <th class="cell" style="text-align:right;"><div id="fs_name">คลังสินค้า : </div></th>
                                                    <th class="cell" colspan="4">
                                                        <select name="fs_id" class="form-select col" id="fs_id" onchange="que_loc(this.value);">
                                                            <option value="" selected>==== เลือกคลังสินค้า ====</option>
                                                            @foreach($fs as $key)
                                                                <option value="{{ $key->id }}">{{ $key->name }} ({{ $key->row }}/{{ $key->col }}/{{ $key->level }})</option>
                                                            @endforeach
                                                        </select>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th class="cell"></th>
                                                    <th class="cell" style="text-align:right;"><div id="loc_name">แถว : </div></th>
                                                    <th class="cell" colspan="4">
                                                        <div id="loc_div">  
                                                            <select name="loc_id" class="form-select col" id="loc_id">                                                          
                                                                <option value="" selected>==== เลือกแถว ====</option>
                                                            </select>
                                                        </div>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th class="cell"></th>
                                                    <th class="cell"></th>
                                                    <th class="cell">
                                                        <button class="btn app-btn-primary" type="submit">
                                                            Report
                                                        </button>
                                                    </th>
                                                    <th class="cell"></th>
                                                    <th class="cell">
                                                    </th>
                                                    <th class="cell"></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div><!--//table-responsive-->                                
                                </div><!--//app-card-body-->                           
                            </form>                                    
                        </div><!--//app-card-->                    
                    </div><!--//tab-pane-->			        
                </div><!--//tab-content-->
            </div><!--//container-fluid-->
        </div><!--//app-content-->	
    </div><!--//app-wrapper-->
    <script type="text/javascript"> 
        function que_loc(fs) {
            const to_loc= <?=json_encode($loc);?>; 
            var loc = '<select name="loc_id" class="form-select col" id="loc_id"><option value="" selected>==== เลือกแถว ====</option>';
            if(to_loc[fs]){
                Object.keys(to_loc[fs]).forEach(key => {
                    loc += '<option value="'+key+'">'+to_loc[fs][key]+'</option>'
                });
            }
            loc += '</select>'            
            document.getElementById("loc_div").innerHTML = loc; 
        }

        function myFunction(a) {        
            console.log(a);
            if(a=='1'){             
                document.getElementById('date_st').style.display = "none";
                document.getElementById('st_date').style.display = "none";
                document.getElementById('date_to').style.display = "none";
                document.getElementById('ed_date').style.display = "none";
                document.getElementById('fs_name').style.display = "block";                
                document.getElementById('fs_id').style.display = "block";  
                document.getElementById('loc_id').style.display = "block";                
                document.getElementById('loc_div').style.display = "block";         
                document.getElementById('fs_id').required = false;   
                document.getElementById('loc_id').required = false;
            }else if(a=='2'){
                document.getElementById('date_st').style.display = "block";
                document.getElementById('st_date').style.display = "block";
                document.getElementById('date_to').style.display = "block";
                document.getElementById('ed_date').style.display = "block";
                document.getElementById('fs_name').style.display = "block";                
                document.getElementById('fs_id').style.display = "block";   
                document.getElementById('loc_id').style.display = "block";                
                document.getElementById('loc_div').style.display = "block";            
                document.getElementById('fs_id').required = true;   
                document.getElementById('loc_id').required = true;
            }else{
                document.getElementById('st_date').style.display = "block";
                document.getElementById('date_to').style.display = "block";
                document.getElementById('ed_date').style.display = "block";
                document.getElementById('month_date').style.display = "none";
                document.getElementById('crop_id').style.display = "none";           
                document.getElementById('crop_name').style.display = "none";          
            }
        }
    </script>    
@endsection