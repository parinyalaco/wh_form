@extends('layouts.app-master')

@section('content')
{{-- <div class="container"> --}}
    {{-- <div class="row justify-content-center" style="width: 100%">
        <div class="col-md-11"> --}}
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            {{-- <div class="card"> --}}
                @if(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('error') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('success') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                    <div class="row g-3 mb-4 align-items-center justify-content-between">
                        <div class="col-auto">
                            <div class="row">
                                <div class="pull-right col-auto my-2">
                                    <a class="btn btn-secondary" href="{{ route('to_reuse.show_det',['date'=>$query[1][0]['to_date'],'pd_id'=>$query[1][0]['product_id']]) }}">Back</a>
                                </div>
                                <div class="pull-right col-auto">
                                    <h1>รายละเอียด รับเข้า -></h1>
                                    <div class="mx-5 my-2">
                                        <h3 class="app-page-title">วันที่รับเข้า : <a style="color:gray">{{ $query[1][0]['to_date'] }}</a>  
                                            สินค้า : <a style="color:gray">{{ $query[1][0]['name'] }}</a>  
                                            จำนวน : <a style="color:gray">{{ $query[1][0]['quantity'] }}</a><br>
                                            Pallet no. : <a style="color:gray">{{ $query[1][0]['pallet_no'] }}</a><br>
                                            Note : <a style="color:gray">{{ $query[1][0]['note'] }}</a>
                                        </h3>
                                    </div>
                            </div>
                        </div>
                    </div><!--//row onclick="getURL($key);" -->
                
                    <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                        <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                            aria-controls="orders-all" aria-selected="true"></a>
                    </nav>   

                    @if(count($query[2])>0)
                        <div class="row g-3 mb-4 align-items-center justify-content-between">
                            <h3 class="app-page-title col mb-0">การซัก</h3>
                        </div>                    
                        <div class="tab-content" id="orders-table-tab-content">                
                            <div class="app-card app-card-orders-table shadow-sm mb-5">
                                <div class="app-card-body">
                                    <table class="table app-table-hover mb-0 text-left">
                                        <thead>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell">Date</th>
                                                <th class="cell">shift</th>
                                                <th class="cell">จำนวน</th>
                                                <th class="cell">หมายเหตุ</th>  
                                                <th class="cell"></th>  
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($to_show['wash'] as $kdate=>$vdate)
                                                @foreach ($vdate as $ksh=>$vsh)
                                                    <tr>                                             
                                                        <td class="cell">{{ $loop->iteration }}</td>
                                                        <td class="cell">{{ $kdate }}</td>
                                                        <td class="cell">{{ $ksh }}</td>
                                                        <td class="cell">{{ $to_show['wash'][$kdate][$ksh]['quantity'] }}</td>
                                                        <td class="cell">{{ $to_show['wash'][$kdate][$ksh]['note'] }}</td> 
                                                        <td class="cell">
                                                            @if(empty($to_show['wash'][$kdate][$ksh]['dry_qty']) && empty($to_show['wash'][$kdate][$ksh]['shine_qty']) && empty($to_show['wash'][$kdate][$ksh]['lose_qty'])) 
                                                                <div class="row">
                                                                    <div class="col-auto">
                                                                        <a class="btn btn-danger" href="{{ url('/wash_del?page=reuse&id='.$to_show['wash'][$kdate][$ksh]['id']) }}"
                                                                            onclick="return confirm('คุณแน่ใจมั้ยว่าต้องการลบการซัก{{ $query[1][0]['name'] }} วันที่ {{ $kdate }} กะ {{ $ksh }} จำนวน {{ $to_show['wash'][$kdate][$ksh]['quantity'] }}?')">X</a>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </td>                                               
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        </tbody>
                                    </table>                           
                                </div><!--//app-card-body-->		
                            </div><!--//app-card-->                  
                        </div><!--//tab-content-->
                    @endif

                    @if(count($query[4])>0)
                        <div class="row g-3 mb-4 align-items-center justify-content-between">
                            <h3 class="app-page-title col mb-0">การคัด</h3>
                        </div>                    
                        <div class="tab-content" id="orders-table-tab-content">                
                            <div class="app-card app-card-orders-table shadow-sm mb-5">
                                <div class="app-card-body">
                                    <table class="table app-table-hover mb-0 text-left">
                                        <thead>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell">Date</th>
                                                <th class="cell">shift</th>
                                                {{-- <th class="cell">Pallet No.</th> --}}
                                                <th class="cell">จำนวน</th>
                                                <th class="cell">หมายเหตุ</th> 
                                                <th class="cell"></th>  
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($to_show['choose_box'] as $kdate=>$vdate)
                                                @foreach ($vdate as $ksh=>$vsh)
                                                    <tr>                                             
                                                        <td class="cell">{{ $loop->iteration }}</td>
                                                        <td class="cell">{{ $kdate }}</td>
                                                        <td class="cell">{{ $ksh }}</td>
                                                        <td class="cell">{{ $to_show['choose_box'][$kdate][$ksh]['quantity'] }}</td>
                                                        <td class="cell">{{ $to_show['choose_box'][$kdate][$ksh]['note'] }}</td> 
                                                        <td class="cell">
                                                            @if(empty($to_show['choose_box'][$kdate][$ksh]['stock_qty']) && empty($to_show['choose_box'][$kdate][$ksh]['lose_qty'])) 
                                                                <div class="row">
                                                                    <div class="col-auto">
                                                                        <a class="btn btn-danger" href="{{ url('/choose_box_del?page=reuse&id='.$to_show['choose_box'][$kdate][$ksh]['id']) }}"
                                                                            onclick="return confirm('คุณแน่ใจมั้ยว่าต้องการลบการซัก{{ $query[1][0]['name'] }} วันที่ {{ $kdate }} กะ {{ $ksh }} จำนวน {{ $to_show['choose_box'][$kdate][$ksh]['quantity'] }}?')">X</a>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </td>                                               
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        </tbody>
                                    </table>                           
                                </div><!--//app-card-body-->		
                            </div><!--//app-card-->                  
                        </div><!--//tab-content-->
                    @endif

                    @if(count($query[3])>0)
                        <div class="row g-3 mb-4 align-items-center justify-content-between">
                            <h3 class="app-page-title col mb-0">สูญเสีย</h3>
                        </div>                    
                        <div class="tab-content" id="orders-table-tab-content">                
                            <div class="app-card app-card-orders-table shadow-sm mb-5">
                                <div class="app-card-body">
                                    <table class="table app-table-hover mb-0 text-left">
                                        <thead>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell">Date</th>
                                                <th class="cell">สาเหตุ</th>
                                                <th class="cell">จำนวน</th>
                                                <th class="cell">หมายเหตุ</th>                                                  
                                                <th class="cell"></th> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($query[3] as $key)
                                                <tr>                                             
                                                    <td class="cell">{{ $loop->iteration }}</td>
                                                    <td class="cell">{{ $key->to_date }}</td>
                                                    <td class="cell">{{ $key->name }}</td>
                                                    <td class="cell">{{ $key->quantity }}</td>
                                                    <td class="cell">{{ $key->note }}</td>  
                                                    <td class="cell">
                                                        <div class="row">
                                                            {{-- <div class="col-auto mx-2"> 
                                                                <a class="text-danger" href="{{ route('lose.destroy', $key->id) }}" title="ลบ">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
                                                                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z"/>
                                                                    </svg>
                                                                </a>
                                                            </div>  --}}
                                                            <div class="col-auto">
                                                                <form action="{{ route('lose.destroy', $key->id) }}" method="POST">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <button type="submit" class="btn btn-danger">X</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </td>                                               
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>                           
                                </div><!--//app-card-body-->		
                            </div><!--//app-card-->                  
                        </div><!--//tab-content-->
                    @endif
                </div>
            {{-- </div> --}}
        </div>
    </div>
{{-- </div> --}}
@endsection
