@extends('layouts.app-master')

@section('content')
    {{-- <div class="row justify-content-center" style="width: 100%">
        <div class="col-md-11"> --}}
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            {{-- <div class="card-body"> --}}
                @if(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('error') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('success') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                <div class="row g-3 align-items-center justify-content-between">
                    <div class="col-auto">
                        <div class="row">
                            <div class="pull-right col-auto my-2">
                                <a class="btn btn-secondary" href="{{ route('to_reuse.index') }}">Back</a>
                            </div>
                            <h1 class="col">รับเข้า -></h1>
                            <div class="pull-right col-auto my-3">
                                <h1 class="app-page-title">วันที่รับเข้า : <a style="color:gray">{{ $date }}</a> ; 
                                    สินค้า : <a style="color:gray">{{ $pd->name }}</a> ; 
                                    จำนวน : <a style="color:gray">{{ $sum_lot }}</a></h1>
                                {{-- <h1 class="app-page-title">สินค้า : {{ $pd->name }}</h1> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-auto">
                        <div class="page-utilities">
                            <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                                <div class="col-auto">
                                    <a class="btn app-btn-secondary" href="{{ url('to_reuse/create?page=show&date='.$date.'&pd_id='.$pd->id) }}">
                                        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
                                        Add
                                    </a>
                                </div>
                            </div><!--//row-->
                        </div><!--//table-utilities-->
                    </div><!--//col-auto-->
                </div><!--//row onclick="getURL($key);" -->
            
                <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                    <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                        aria-controls="orders-all" aria-selected="true"></a>
                </nav>            
                
                <div class="tab-content" id="orders-table-tab-content">                
                    <div class="app-card app-card-orders-table shadow-sm mb-5">
                        <div class="app-card-body">
                            <table class="table app-table-hover mb-0 text-left">
                                <thead>
                                    <tr>
                                        <th class="cell">No.</th>
                                        <th class="cell">วันที่ซัก</th>
                                        <th class="cell">Pallet No.</th>
                                        <th class="cell">รับเข้า</th>
                                        <th class="cell">นำไปใช้</th>
                                        <th class="cell">หักลบ</th>
                                        <th class="cell">คงเหลือ</th>
                                        <th class="cell">สถานะ(หมด)</th>  
                                        <th class="cell"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($to_show as $key=>$value)
                                        <tr>                                             
                                            <td class="cell">{{ $loop->iteration }}</td>
                                            <td class="cell">{{ $to_show[$key]['to_date'] }}</td>
                                            <td class="cell">{{ $to_show[$key]['pallet_no'] }}</td>
                                            <td class="cell">{{ $to_show[$key]['quantity'] }}</td>
                                            @php
                                                if($pd->status_wash==1) {
                                                    $balance = $to_show[$key]['quantity'] - $to_show[$key]['lose_qty'] - $to_show[$key]['wash_qty'];
                                                    $to_use = $to_show[$key]['wash_qty'];
                                                }else{
                                                    $balance = $to_show[$key]['quantity'] - $to_show[$key]['lose_qty'] - $to_show[$key]['choose_box_qty'];
                                                    $to_use = $to_show[$key]['choose_box_qty'];
                                                }
                                                // $balance = $to_show[$key]['quantity'] - $to_show[$key]['lose_qty'] - $to_show[$key]['wash_qty'] - $to_show[$key]['stock_qty'];
                                                $no_lose = $balance + $to_show[$key]['enable_chk'];
                                                $to_lose = $to_show[$key]['lose_qty'] + $to_show[$key]['enable_chk'];
                                            @endphp      
                                            <td class="cell">@if(!empty($to_use)){{ $to_use }}@else-@endif</td>
                                            <td class="cell">@if(!empty($to_lose)){{ $to_lose }}@else-@endif</td>                                      
                                            <td class="cell">@if(!empty($balance)){{ $balance }}@else-@endif</td>
                                            <td class="cell">
                                                <input class="form-check-input set_public" type="checkbox" 
                                                    value="" @if($balance==0) checked @endif @if($no_lose==0) disabled @endif 
                                                    onchange="handleChange(event, '{{ $key }}')"> 
                                            </td>
                                            <td class="cell">
                                                <div class="row">                                                
                                                    @if($balance == $to_show[$key]['quantity'] && empty($to_show[$key]['stock_id']))
                                                        <div class="col-auto mx-2">
                                                            <a class="btn btn-primary" href="{{ route('to_reuse.edit', $key) }}">Edit</a>
                                                        </div>
                                                        <div class="col-auto">
                                                            <form action="{{ route('to_reuse.destroy', $key) }}" method="POST">
                                                                @csrf
                                                                @method('DELETE')
                                                                <button type="submit" class="btn btn-danger" onclick="return confirm('คุณแน่ใจมั้ยว่าต้องการลบการรับเข้า{{ $pd->name }} วันที่ {{ $date }} \n Pallet No. {{ $to_show[$key]['pallet_no'] }} จำนวน {{ $to_show[$key]['quantity'] }}?')">Delete</button>
                                                            </form>
                                                        </div>
                                                    @endif
                                                    @if($balance >0)
                                                        <div class="col-auto mx-2">
                                                            <a class="btn btn-danger" href="{{ url('lose/create?page=show&id='.$key.'&date='.$to_show[$key]['to_date'].'&pd_id='.$to_show[$key]['product_id'].'&bal='.$balance) }}">หักลบ</a>
                                                        </div>
                                                    @endif
                                                    <div class="col-auto mx-2">
                                                        <a class="btn btn-info" href="{{ route('to_reuse.detail',$key) }}">รายละเอียด</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>  
                        </div><!--//app-card-body-->		
                    </div><!--//app-card-->                  
                </div><!--//tab-content-->
            {{-- </div> --}}
        </div>
    </div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function handleChange(e, id) {
            const {checked} = e.target;
            if(checked){
                chk = 'check';
            }else{
                chk = 'not';
            }
            console.log(chk);
            // var tex_search = document.getElementById("txt_search").value;
            // var set_col = document.getElementById("set_col").value;
            // console.log(tex_search);

            // var link_url = window.location.pathname+"/chk_public?id="+id+"&chk="+chk+"&txt_search="+tex_search+"&set_col="+set_col;
            // var link_url = window.location.pathname+"/set_toend?chk="+chk+"&id="+id+"&page=show";
            // console.log(window.location);
            var cut_link = window.location.href.split('/').slice(0,-3).join('/');
            console.log(cut_link);
            var link_url = cut_link+"/set_toend?chk="+chk+"&id="+id+"&page=show";
            // window.location = window.location.pathname+link_url;


            $.get(link_url,function (data) {
                alert(data);
                location.reload();
            });
        }
    </script>
@endsection
