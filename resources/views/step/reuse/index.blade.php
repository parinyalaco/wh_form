@extends('layouts.app-master')

@section('content')
    {{-- <div class="row justify-content-center" style="width: 100%">
        <div class="col-md-11"> --}}
    <div class="app-content p-md-4">
        <div class="app-container-xl">

            <div class="card-body">
                @if(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('error') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('success') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                <div class="row g-3 align-items-center justify-content-between">
                    <div class="col-auto">
                        <div class="row">
                            <h1 class="app-page-title col mb-0">รับเข้า</h1>
                            {{-- <div class="pull-right col-auto mb-0">
                                <div class="row">
                                    <h1 class="app-page-title col-auto">
                                        <a style="color:red"> ( สินค้าซักซ้ำ : </a>
                                    </h1> 
                                    <div class="col-auto">         
                                        <div class="row" id="show_div">
                                        </div>
                                    </div>
                                    <h1 class="app-page-title col-auto"><a class="col" style="color:red"> )</a></h1>
                                </div>                              
                            </div> --}}
                        </div>
                    </div>
                    <div class="col-auto">
                        <div class="page-utilities">
                            <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                                <div class="col-auto">
                                    <a class="btn app-btn-secondary" href="{{ url('to_reuse/create?page=index') }}">
                                        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
                                        Add
                                    </a>
                                </div>
                            </div><!--//row-->
                        </div><!--//table-utilities-->
                    </div><!--//col-auto-->
                </div><!--//row onclick="getURL($key);" -->
            
                <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row my-3">
                    <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                        aria-controls="orders-all" aria-selected="true"></a>
                </nav>  
                {{-- <div class="tab-content"> --}}
                <div class="col-auto">
                    <form method="GET" action="{{ url('/to_reuse') }}" id="myForm" accept-charset="UTF-8" role="search">
                        <div class="row mx-1">
                            <select class="form-select col" name="set_pd" id="set_pd">
                                <option value="">..สินค้า..</option>
                                @if(!empty($pd['name']))
                                    @foreach($pd['name'] as $key => $value)
                                        <option value="{{ $key }}" @if(Request::get('set_pd')==$key) selected @endif>{{ $value }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <label for="name" class="col-auto form-label my-2">วันที่ </label>
                            <input type="date" class="form-control form-control-dark col" id="st_search" name="st_search" 
                                value="{{ Request::get('st_search') }}">
                            &nbsp;
                            <label for="name" class="col-auto form-label my-2">ถึงวันที่ </label>
                            <input type="date" class="form-control form-control-dark col" id="ed_search" name="ed_search" 
                                value="{{ Request::get('ed_search') }}">
                            <span class="input-group-append col">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        <br/>
                    </form>
                </div><!--//col-auto-->           
                
                <div class="tab-content" id="orders-table-tab-content">                
                    <div class="app-card app-card-orders-table shadow-sm mb-5">
                        <div class="app-card-body">
                            <table class="table app-table-hover mb-0 text-left">
                                <thead>
                                    <tr>
                                        <th class="cell">วันที่</th>
                                        <th class="cell">สินค้า</th>
                                        <th class="cell">รับเข้า</th>
                                        <th class="cell">นำไปใช้</th>
                                        <th class="cell">หักลบ</th>
                                        <th class="cell">คงเหลือ</th>
                                        <th class="cell">สถานะ(หมด)</th>  
                                        <th class="cell"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($to_show as $kdate=>$vdate)
                                        @foreach ($vdate as $kpd=>$vpd)
                                            <tr>                                             
                                                <td class="cell">{{ $kdate }}</td>
                                                <td class="cell">{{ $pd['name'][$kpd] }}</td>
                                                <td class="cell">{{ $to_show[$kdate][$kpd]['quantity'] }}</td>
                                                @php
                                                    $arr = array('lose_qty','wash_qty','choose_box_qty','enable_chk');
                                                    foreach($arr as $karr=>$varr){
                                                        if(!empty($to_show[$kdate][$kpd][$varr]))  $set[$varr] = $to_show[$kdate][$kpd][$varr];
                                                        else    $set[$varr] = 0;
                                                    } 
                                                    if($pd['wash'][$kpd]==1) {
                                                        $balance = $to_show[$kdate][$kpd]['quantity'] - $set['lose_qty'] - $set['wash_qty'];
                                                        $to_use = $set['wash_qty'];
                                                    }else{
                                                        $balance = $to_show[$kdate][$kpd]['quantity'] - $set['lose_qty'] - $set['choose_box_qty'];
                                                        $to_use = $set['choose_box_qty'];
                                                    }                                           
                                                    // $balance = $key->quantity - $set['lose_qty'] - $set['wash_qty'] - $set['stock_qty'];
                                                    $no_lose = $balance + $set['enable_chk'];
                                                    $to_lose = $set['lose_qty'];
                                                @endphp
                                                <td class="cell">@if(!empty($to_use)){{ $to_use }}@else-@endif</td>
                                                <td class="cell">@if(!empty($to_lose)){{ $to_lose }}@else-@endif</td>
                                                <td class="cell">@if(!empty($balance)){{ $balance }}@else-@endif</td>
                                                <td class="cell">
                                                    <input class="form-check-input set_public" type="checkbox" 
                                                        value="" @if($balance==0) checked @endif @if($no_lose==0) disabled @endif 
                                                        onchange="handleChange(event, '{{ $kdate }}', '{{ $kpd }}')"> 
                                                </td>
                                                <td class="row">
                                                    <div class="col-auto mx-2">
                                                        <a class="btn btn-primary" href="{{ route('to_reuse.show_det',['date'=>$kdate,'pd_id'=>$kpd]) }}">Detail</a>
                                                    </div>
                                                    @if($balance >0)
                                                        <div class="col-auto mx-2">
                                                            <a class="btn btn-danger" href="{{ url('lose/create?page=index&date='.$kdate.'&pd_id='.$kpd.'&bal='.$balance) }}">หักลบ</a>
                                                        </div>
                                                    @endif
                                                    {{-- <div class="col-auto mx-2">
                                                        <a class="btn btn-success" href="{{ route('wash.wash_index',$key->id) }}">ซัก</a>
                                                    </div>
                                                    <div class="col-auto mx-2">
                                                        <a class="btn btn-success" href="{{ route('stock.index',$key->id) }}">พับ</a>
                                                    </div> --}}
                                                </td>
                                            </tr>
                                        @endforeach     
                                    @endforeach
                                </tbody>
                            </table>  
                            {{-- <div class="pagination-wrapper"> {!! $to_show->appends(['set_pd' => Request::get('set_pd'), 'st_search' => Request::get('st_search'), 'ed_search' => Request::get('ed_search')])->render() !!} </div>                          --}}
                        </div><!--//app-card-body-->		
                    </div><!--//app-card-->                  
                </div><!--//tab-content-->
            </div>
        </div>
    </div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function handleChange(e, date, pd_id) {
            const {checked} = e.target;
            if(checked){
                chk = 'check';
            }else{
                chk = 'not';
            }
            console.log(chk+'--'+date+'--'+pd_id);
            // var tex_search = document.getElementById("txt_search").value;
            // var set_col = document.getElementById("set_col").value;
            // console.log(tex_search);

            // var link_url = window.location.pathname+"/chk_public?id="+id+"&chk="+chk+"&txt_search="+tex_search+"&set_col="+set_col;
            var link_url = window.location.pathname+"/set_toend?chk="+chk+"&date="+date+"&pd_id="+pd_id+"&page=index";
            console.log(link_url);
            // console.log(window.location.pathname );
            // window.location = window.location.pathname+link_url;


            $.get(link_url,function (data) {
                alert(data);
                location.reload();
            });
        }
    </script>
    {{-- <script>
        window.onload = set_div
        function set_div() {
            const to_show = <?=json_encode($to_stock);?>;
            const pd = <?=json_encode($pd['name']);?>;
            var to_div = new Array();  
            // console.log(to_show);
            
            var set_date = new Date();
            Object.keys(pd).forEach(id => {
                to_div[id] = 0;
                if(to_show[id]){
                    Object.keys(to_show[id]).forEach(kdate => {
                        // console.log(kdate, to_show[id][kdate]);
                        if(new Date(kdate) <= set_date){
                            Object.keys(to_show[id][kdate]).forEach(key => {
                                to_div[id] += parseFloat(to_show[id][kdate][key])
                                // console.log('-', id, to_show[id][kdate], to_div[id]);
                            });
                        }                        
                    });
                } 
            });
            console.log(to_div);  

            var text = "";
            Object.keys(to_div).forEach(key => {
                if(to_div[key]>0){
                    console.log(key, to_div[key]);
                    text += '<div class="col-auto"><a class="btn btn-info" id="set_topd['+key+']" name="set_topd['+key+']" href="'+window.location.href+'/create?pd='+key+'&num='+to_div[key]+'">'+pd[key]+'  '+to_div[key]+'</a></div>';
                }
            });
            document.getElementById("show_div").innerHTML = text;          
        }
    </script>  --}}
@endsection
