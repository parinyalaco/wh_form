@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="page-heading row">        
                <div class="pull-right col-auto my-2">
                    <a class="btn btn-secondary" 
                        href="@if($query['page']=='show'){{ route('to_reuse.show_det',['date'=>$query['date'],'pd_id'=>$query['pd_id']]) }}@else{{ route('to_reuse.index') }}@endif">
                        Back
                    </a>
                </div>
                <h1 class="col">{{ __('รับเข้า') }} -> Create</h1>
            </div>
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>  
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <form method="POST" action="{{ route('to_reuse.store') }}">
                            @csrf
                            <input type="hidden" id="page" name="page" value="{{ $query['page'] }}">
                            @php
                                if($query['page']=='show'){
                                    $set_date = $query['date'];
                                    $set_pd = $query['pd_id'];
                                }else{
                                    if(empty(old('to_date')))   $set_date = date('Y-m-d');
                                    else   $set_date = old('to_date');
                                    $set_pd = old('product_id');
                                }
                            @endphp
                            @if($query['page']=='show')
                                <input type="hidden" id="to_date" name="to_date" value="{{ $set_date }}">
                                <input type="hidden" id="product_id" name="product_id" value="{{ $set_pd }}">
                            @endif
                            <div class="row mb-3">
                                <label for="to_date" class="col-md-4 col-form-label text-md-end">{{ __('วันที่') }}</label>
    
                                <div class="col-md-6">
                                    <input id="to_date" type="date" class="form-control @error('to_date') is-invalid @enderror" 
                                        name="to_date" value="{{ $set_date }}" required autocomplete="to_date" autofocus
                                        @if($query['page']=='show') disabled @endif>
    
                                    @error('to_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="product_id" class="col-md-4 col-form-label text-md-end">{{ __('สินค้า') }}</label>
    
                                <div class="col-md-6">
                                    <select class="form-select col" name="product_id" id="product_id" required @if($query['page']=='show') disabled @endif>
                                        <option value="">..กรุณาเลือกสินค้า..</option>
                                        @foreach($pd as $key)
                                            <option value="{{ $key->id }}" @if($set_pd==$key->id) selected @endif>{{ $key->name }}</option>
                                        @endforeach
                                    </select>

                                    @error('product_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="pallet_no" class="col-md-4 col-form-label text-md-end">{{ __('Pallet no.') }}</label>
    
                                <div class="col-md-6">
                                    <input id="pallet_no" type="text" class="form-control @error('pallet_no') is-invalid @enderror" 
                                        name="pallet_no" value="{{ old('pallet_no') }}" autocomplete="pallet_no" required>
    
                                    @error('pallet_no')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="quantity" class="col-md-4 col-form-label text-md-end">{{ __('จำนวน') }}</label>
    
                                <div class="col-md-6">
                                    <input id="quantity" type="number" class="form-control @error('quantity') is-invalid @enderror" 
                                        name="quantity" value="{{ old('quantity') }}" min="1" autocomplete="quantity" required>
    
                                    @error('quantity')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="note" class="col-md-4 col-form-label text-md-end">{{ __('หมายเหตุ') }}</label>
    
                                <div class="col-md-6">
                                    <input id="note" type="text" class="form-control @error('note') is-invalid @enderror" 
                                        name="note" value="{{ old('note') }}" autocomplete="note">
    
                                    @error('note')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label class="col-md-4"></label>
    
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->   
    {{-- <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>      --}}
@endsection
