@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="page-heading row">        
                <div class="pull-right col-auto my-2">
                    <a class="btn btn-secondary" href="{{ route('wash.index') }}">Back</a>
                </div>
                <h1 class="col">
                    {{ __('การซัก') }} -> Edit
                    <a style="color:gray" class="app-page-title">(ซักได้ไม่เกิน : {{ $set_max }})</a>
                </h1>
            </div>
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>  
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <form method="POST" action="{{ route('wash.wash_update') }}">
                            @csrf
                            @method('PUT')
                            
                            <input type="hidden" name="to_date" id="to_date" value="{{ $query[0]['to_date'] }}">
                            <input type="hidden" name="shift" id="shift" value="{{ $query[0]['shift'] }}">
                            <input type="hidden" name="product_id" id="product_id" value="{{ $query[0]['product_id'] }}">

                            <div class="row mb-3">
                                <label for="show_date" class="col-md-4 col-form-label text-md-end">{{ __('วันที่') }}</label>
    
                                <div class="col-md-6">
                                    <input id="show_date" type="date" class="form-control" 
                                        name="show_date" value="{{ $query[0]['to_date'] }}" disabled>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="show_shift" class="col-md-4 col-form-label text-md-end">{{ __('กะ') }}</label>
    
                                <div class="col-md-6">
                                    <select class="form-select col" name="show_shift" id="show_shift" disabled>
                                        <option value="">..กรุณาเลือก..</option>
                                        <option value="B"@if($query[0]['shift']=='B') selected @endif>กะ B</option>
                                        <option value="C"@if($query[0]['shift']=='C') selected @endif>กะ C</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="pd_id" class="col-md-4 col-form-label text-md-end">{{ __('สินค้า') }}</label>
    
                                <div class="col-md-6">
                                    <select class="form-select col" name="pd_id" id="pd_id" disabled>
                                        <option value="">..กรุณาเลือกสินค้า..</option>
                                        @foreach($pd_name as $key)
                                            <option value="{{ $key->id }}" @if($query[0]['product_id']==$key->id) selected @endif>{{ $key->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="quantity" class="col-md-4 col-form-label text-md-end">{{ __('จำนวน') }}</label>
    
                                <div class="col-md-6">
                                    <input id="quantity" type="number" class="form-control @error('quantity') is-invalid @enderror" 
                                        name="quantity" value="{{ $query[0]['quantity'] }}" min="1" max="{{ $set_max }}" autocomplete="quantity" required>
    
                                    @error('quantity')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="note" class="col-md-4 col-form-label text-md-end">{{ __('หมายเหตุ') }}</label>
    
                                <div class="col-md-6">
                                    <input id="note" type="text" class="form-control @error('note') is-invalid @enderror" 
                                        name="note" value="{{ $query[0]['note'] }}" autocomplete="note">
    
                                    @error('note')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label class="col-md-4"></label>
    
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>                          

                        </form>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->     
@endsection
