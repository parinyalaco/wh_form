@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="page-heading row">        
                <div class="pull-right col-auto my-2">
                    <a class="btn btn-secondary" href="{{ route('wash.index') }}">Back</a>
                </div>
                <h1 class="col">การซัก ->Create</h1>
            </div>
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>  
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <form method="POST" action="{{ route('wash.store') }}">
                            @csrf
                            <input id="product_id" type="hidden" name="product_id">
                            {{-- <div class="row"> --}}
                                {{-- <div class="col-md-4">
                                    <div class="row mb-3">
                                        <label id="set_date" class="col-md-12 col-form-label text-md-center">รายการสินค้าคงเหลือ ณ วันที่ {{ date('d/m/Y') }}</label>
                                    </div>  

                                    <div id='show_div'></div>
                                </div> --}}

                                {{-- <div class="col-md-8"> --}}
                                    <div class="row mb-3">
                                        <label for="to_date" class="col-md-4 col-form-label text-md-end">{{ __('วันที่') }}</label>
            
                                        <div class="col-md-6">
                                            <input id="to_date" type="date" class="form-control @error('to_date') is-invalid @enderror" 
                                                name="to_date" required autocomplete="to_date" autofocus
                                                value="@if(empty(old('to_date'))){{ date('Y-m-d') }}@else{{ old('to_date') }}@endif" 
                                                onchange="set_div();">
            
                                            @error('to_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="shift" class="col-md-4 col-form-label text-md-end">{{ __('กะ') }}</label>
            
                                        <div class="col-md-6">
                                            <select class="form-select col" name="shift" id="shift" required onchange="set_div();">
                                                <option value="">..กรุณาเลือก..</option>
                                                <option value="B"@if(old('shift')=='B') selected @endif>กะ B</option>
                                                <option value="C"@if(old('shift')=='C') selected @endif>กะ C</option>
                                            </select>
            
                                            @error('shift')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="pd_id" class="col-md-4 col-form-label text-md-end">{{ __('สินค้า') }}</label>
            
                                        <div class="col-md-6">  
                                            <div class="row" id='show_div'>
                                            </div>       
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="quantity" class="col-md-4 col-form-label text-md-end">{{ __('จำนวน') }}</label>
            
                                        <div class="col-md-6">
                                            <input id="quantity" type="number" class="form-control @error('quantity') is-invalid @enderror" 
                                                name="quantity" value="{{ old('quantity') }}" min="1" autocomplete="quantity" required>
            
                                            @error('quantity')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="note" class="col-md-4 col-form-label text-md-end">{{ __('หมายเหตุ') }}</label>
            
                                        <div class="col-md-6">
                                            <input id="note" type="text" class="form-control @error('note') is-invalid @enderror" 
                                                name="note" value="{{ old('note') }}" autocomplete="note">
            
                                            @error('note')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label class="col-md-4"></label>
            
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-success">
                                                {{ __('Save') }}
                                            </button>
                                        </div>
                                    </div>
                                {{-- </div> --}}
                            {{-- </div> --}}
                        </form>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->   
    <script>
        // window.onload = set_div
        window.onload = function(event) {
            set_div()
            const to_req = <?=json_encode($requestData);?>; 
            if(to_req['pd']!==undefined){
                set_to_save(to_req['pd'], to_req['num'])
            }
        }

        function chk_date_shift(to_div) {  
            console.log(to_div);          
            var pd_id = document.getElementById("product_id").value;
            var qty = document.getElementById("quantity").value;
            console.log(pd_id);
            if(to_div[pd_id]<qty){
                set_null();
            }else{                
                document.getElementById("quantity").max = to_div[pd_id]
                set_colur(pd_id, to_div[pd_id])
            }
        }

        function set_div() {
            const to_show = <?=json_encode($to_show);?>;
            const pd = <?=json_encode($pd);?>;
            var to_div = new Array();  
            // console.log(to_show);
            
            var chk_date = document.getElementById("to_date").value;
            // console.log(chk_date);
            var set_date = new Date(chk_date);

            // document.getElementById("set_date").innerHTML = "รายการสินค้าคงเหลือ ณ วันที่ "+set_date.getDate()+"/"+(set_date.getMonth()+1)+"/"+set_date.getFullYear();

            // Object.keys(pd).forEach(id => console.log('=>', id))
            // for (var i = 0; i < pd.length; i++) {
                // console.log(pd[i]['id'], pd[i]['name']);
                // console.log(to_show[pd[i]['id']]);
            Object.keys(pd).forEach(id => {
                to_div[id] = 0;
                if(to_show[id]){

                    Object.keys(to_show[id]).forEach(key => {
                        // console.log(key, to_show[id][key]);
                        if(new Date(key) <= set_date){
                            to_div[id] += parseFloat(to_show[id][key])
                        }                        
                    });
                } 
            });
            // console.log(to_div);  

            var text = "";
            Object.keys(to_div).forEach(key => {
                if(to_div[key]>0){
                    // console.log(key, to_div[key]);
                    // text += '<div class="row mb-3"><div class="col-md-2"></div><div class="col-md-10"><div class="col-md-9"><a class="btn btn-info" id="set_topd['+key+']" name="set_topd['+key+']" onclick="set_to_save('+key+','+to_div[key]+');">'+pd[key]+'  '+to_div[key]+'</a></div></div></div>';
                    // text += '<div class="col-auto"><a class="btn btn-info" id="set_topd['+key+']" name="set_topd['+key+']" onclick="set_to_save('+key+','+to_div[key]+');">'+pd[key]+'  '+to_div[key]+'</a></div>'
                    text += '<div class="col-auto"><a class="btn btn-secondary" style="background-color: #D2D3D2" id="set_topd['+key+']" name="set_topd['+key+']" onclick="set_to_save('+key+','+to_div[key]+');">'+pd[key]+'  '+to_div[key]+'</a></div>'
                }
            });
            document.getElementById("show_div").innerHTML = text;  
            chk_date_shift(to_div); 
        }

        function set_colur(pd, num) {
            console.log('set',pd, num);
            const pd_all = <?=json_encode($pd);?>;
            Object.keys(pd_all).forEach(key => {
                var to_div = document.getElementById("set_topd["+key+"]")
                if(to_div){
                    if(key==pd){
                        document.getElementById("set_topd["+key+"]").style.backgroundColor = "#15a362"
                    }else{
                        document.getElementById("set_topd["+key+"]").style.backgroundColor = "#D2D3D2"
                    }
                }  
            }); 

        }
        function set_to_save(pd, num) {
            // console.log(pd, num);
            // const pd_all = <?=json_encode($pd);?>;
            // Object.keys(pd_all).forEach(key => {
            //     var to_div = document.getElementById("set_topd["+key+"]")
            //     if(to_div){
            //         if(key==pd){
            //             document.getElementById("set_topd["+key+"]").style.backgroundColor = "#15a362"
            //         }else{
            //             document.getElementById("set_topd["+key+"]").style.backgroundColor = "#D2D3D2"
            //         }
            //     }  

            // });            
            set_colur(pd, num)
            document.getElementById("product_id").value = pd
            // document.getElementById("pd_id").value = pd
            document.getElementById("quantity").value = num
            document.getElementById("quantity").max = num
            document.getElementById("quantity").select();
        }

        function set_null() {
            document.getElementById("product_id").value = ''
            // document.getElementById("pd_id").value = ''
            document.getElementById("quantity").value = ''
            document.getElementById("quantity").removeAttribute("max");
        }
    </script>     
@endsection
