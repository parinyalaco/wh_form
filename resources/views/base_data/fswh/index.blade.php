@extends('layouts.app-master')

@section('content')
{{-- <div class="container"> --}}
    <div class="app-content p-md-4">
        <div class="app-container-xl">
    {{-- <div class="row justify-content-center" style="width: 100%">
        <div class="col-md-11"> --}}
            {{-- <div class="card"> --}}
                {{-- <div class="card-header">{{ __('Product') }}</div> --}}

                <div class="card-body">
                    @if(Session::has('error'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>{{ Session::get('error') }}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{ Session::get('success') }}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif

                    <div class="row g-3 mb-4 align-items-center justify-content-between">
                        <div class="col-auto">
                            <h1 class="app-page-title mb-0">FS Warehouse</h1>
                        </div>
                        <div class="col-auto">
                            <div class="page-utilities">
                                <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                                    <div class="col-auto">
                                        <a class="btn btn-secondary" href="{{ route('FsReports.fsstockuseRpt') }}">Report</a>
                                        <a class="btn app-btn-secondary" href="{{ route('FsWhs.create') }}">
                                            <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
                                            Add
                                        </a>
                                    </div>
                                </div><!--//row-->
                            </div><!--//table-utilities-->
                        </div><!--//col-auto-->
                    </div><!--//row onclick="getURL($key);" -->

                    <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                        <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                            aria-controls="orders-all" aria-selected="true"></a>
                    </nav>

                    <div class="tab-content" id="orders-table-tab-content">
                        <div class="app-card app-card-orders-table shadow-sm mb-5">
                            <div class="app-card-body">
                                <table class="table app-table-hover mb-0 text-left">
                                    <thead>
                                        <tr>
                                            <th class="cell">No.</th>
                                            <th class="cell">Name</th>
                                            <th class="cell">รายละเอียด</th>
                                            <th class="cell">Row / Column / ชั้น</th>
                                            <th class="cell">จำนวนที่วางสินค้า</th>
                                            <th class="cell"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($query as $key)
                                            <tr>
                                                <td class="cell">{{ $loop->iteration }}</td>
                                                <td class="cell">{{ $key->name }}</td>
                                                <td class="cell">{{ $key->desc }}</td>
                                                <td class="cell">{{ $key->row }} / {{ $key->col }} / {{ $key->level }}</td>
                                                <td class="cell">
                                                @if ($key->fswhlocs->count() == 0)
                                                    <a class="btn btn-primary" href="{{ route('FsWhs.autoGenLoc', $key->id) }}">Auto Gen</a>
                                                @else
                                                Old >
                                                     <a class="btn btn-primary" href="{{ route('FsStocks.mapfswh', $key->id) }}">{{ $key->fswhlocs->count() }}</a>
                                                  New >  <a class="btn btn-secondary" href="{{ route('FsStocks.viewstock', $key->id) }}">{{ $key->fswhlocs()->count() }}</a>

                                                @endif


                                                </td>
                                                <td class="cell">
                                                    <div class="row">
                                                        @if ($key->fswhlocs->count() == 0)
                                                            <div class="col-auto mx-2">
                                                                <a class="btn btn-primary" href="{{ route('FsWhs.edit', $key->id) }}">Edit</a>
                                                            </div>
                                                        @endif
                                                        @if (empty($count_tran[$key->id]))
                                                            <div class="col-auto">
                                                                <form action="{{ route('FsWhs.destroy', $key->id) }}" method="POST">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                                </form>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div><!--//app-card-body-->
                        </div><!--//app-card-->
                    </div><!--//tab-content-->
                </div>
            {{-- </div> --}}
        </div>
    </div>
{{-- </div> --}}
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function handleChange(e, id,type) {
            const {checked} = e.target;
            if(checked){
                chk = 'check';
            }else{
                chk = 'not';
            }
            console.log(chk);
            // var tex_search = document.getElementById("txt_search").value;
            // var set_col = document.getElementById("set_col").value;
            // console.log(tex_search);

            // var link_url = window.location.pathname+"/chk_public?id="+id+"&chk="+chk+"&txt_search="+tex_search+"&set_col="+set_col;
            var link_url = window.location.pathname+"/set_status?chk="+chk+"&id="+id+"&set="+type;
            console.log(link_url);
            // console.log(window.location.pathname );
            // window.location = window.location.pathname+link_url;

            $.get(link_url,function (data) {
                console.log(data);
                alert(data);
            });
        }
    </script>
@endsection
