@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="page-heading row">
                <div class="pull-right col-auto my-2">
                    <a class="btn btn-secondary" href="{{ route('FsWhs.index') }}">Back</a>
                </div>
                <h1 class="col">FS Warehouse -> Create</h1>
            </div>

            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>

            <div class="tab-content" id="orders-table-tab-content">
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <form method="POST" action="{{ route('FsWhs.store') }}">
                            @csrf
                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="desc" class="col-md-4 col-form-label text-md-end">{{ __('รายละเอียด') }}</label>

                                <div class="col-md-6">
                                    <input id="desc" type="text" class="form-control @error('desc') is-invalid @enderror" name="desc" value="{{ old('desc') }}" autocomplete="desc">

                                    @error('desc')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                {{-- <label for="status_wash" class="col-md-4 col-form-label text-md-end">{{ __('ต้องซัก') }}</label> --}}
                                <label for="row" class="col-md-4 col-form-label text-md-end">{{ __('จำนวนแถว') }}</label>
                                <div class="col-md-6">
                                    <input id="row" class="form-control @error('row') is-invalid @enderror" name="row" value="{{ old('row') }}" 
                                        type="number" onchange="to_cal();" required>

                                    @error('row')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                {{-- <label for="status_wash" class="col-md-4 col-form-label text-md-end">{{ __('ต้องซัก') }}</label> --}}
                                <label for="col" class="col-md-4 col-form-label text-md-end">{{ __('จำนวนคอลั่ม') }}</label>
                                <div class="col-md-6">
                                    <input id="col" class="form-control @error('col') is-invalid @enderror" name="col" value="{{ old('col') }}" 
                                        type="number" onchange="to_cal();" required>

                                    @error('col')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                {{-- <label for="status_wash" class="col-md-4 col-form-label text-md-end">{{ __('ต้องซัก') }}</label> --}}
                                <label for="level" class="col-md-4 col-form-label text-md-end">{{ __('จำนวนชั้น') }}</label>
                                <div class="col-md-6">
                                    <input id="level" class="form-control @error('level') is-invalid @enderror" name="level" value="{{ old('level') }}" 
                                        type="number" onchange="to_cal();" required>

                                    @error('level')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                {{-- <label for="status_wash" class="col-md-4 col-form-label text-md-end">{{ __('ต้องซัก') }}</label> --}}
                                <label for="all_loc" class="col-md-4 col-form-label text-md-end">{{ __('จำนวนช่องที่วางสินค้าทั้งหมด') }}</label>
                                <div class="col-md-6">
                                    <input id="all_loc" class="form-control" name="all_loc" value="{{ old('all_loc') }}" type="hidden">
                                    <input id="show_loc" class="form-control" name="show_loc" value="{{ old('show_loc') }}" type="number" disabled>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label class="col-md-4"></label>

                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success">
                                        {{ __('เพิ่ม') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div><!--//app-card-body-->
                </div><!--//app-card-->
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
    <script>
        function to_cal() {
            var row = document.getElementById("row").value
            var col = document.getElementById("col").value
            var level = document.getElementById("level").value
            console.log(row, col, level);
            if(row && col && level){
                document.getElementById("all_loc").value = row*col*level
                document.getElementById("show_loc").value = row*col*level
            }
        }
    </script>     
@endsection
