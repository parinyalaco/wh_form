<!DOCTYPE html>
<html lang="en">
  <head>
    <title>WH Form</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="WH Form">
    <meta name="author" content="IT LACO">
    <link rel="shortcut icon" href="{{ asset('pic/download.png') }}">

    <!-- FontAwesome JS-->
    <script defer src="{{ asset('assets/plugins/fontawesome/js/all.min.js') }}"></script>

    <!-- App CSS -->
    <link id="theme-style" rel="stylesheet" href="{{ asset('assets/css/portal.css') }}">

  </head>

  <body class="app">
    <header class="app-header fixed-top">

    </header><!--//app-header-->

    <div class="app-wrapper">
      <main class="container">
        @yield('content')
      </main>
      <footer class="app-footer">
        <div class="container text-center py-3">
          <!--/* This template is free as long as you keep the footer attribution link. If you'd like to use the template without the attribution link, you can buy the commercial license via our website: themes.3rdwavemedia.com Thank you for your support. :) */-->
          <small class="copyright">Designed with <i class="fas fa-heart" style="color: #fb866a;"></i> by <a class="app-link" href="http://themes.3rdwavemedia.com" target="_blank">Xiaoying Riley</a> for developers</small>
        </div>
      </footer><!--//app-footer-->
    </div><!--//app-wrapper-->


    <!-- Javascript -->
    <script src="assets/plugins/popper.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>

    <!-- Charts JS -->
    <script src="assets/plugins/chart.js/chart.min.js"></script>
    {{-- <script src="assets/js/index-charts.js"></script>  --}}

    <!-- Page Specific JS -->
    <script src="assets/js/app.js"></script>

  </body>

</html>
