{{-- @section('menu') --}}
<header class="app-header fixed-top">
    <div class="app-header">
        <div class="container-fluid py-2">
            <div class="app-header-content">
                <div class="row justify-content-between align-items-center px-3">
                    {{-- <div class="col-auto">
                            <a id="sidepanel-toggler" class="sidepanel-toggler d-inline-block d-xl-none" href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30" role="img">
                                    <title>Menu</title>
                                    <path stroke="currentColor" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2" d="M4 7h22M4 15h22M4 23h22"></path>
                                </svg>
                            </a>
                        </div><!--//col--> --}}

                    <nav class="navbar-light bg-light w-auto">


                        <div class="app-branding" style="background-color: #fff">
                            <button class="navbar-toggler w-auto collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#navbarToggleExternalContent"
                                aria-controls="navbarToggleExternalContent" aria-expanded="false"
                                aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <a class="app-logo" href="{{ route('home.index') }}">
                                <img class="logo-icon me-2" src="{{ asset('pic/download.png') }}" alt="logo">
                                <span class="logo-text">WH Form</span>
                            </a>
                        </div>
                    </nav>

                    <div class="search-mobile-trigger d-sm-none col">
                        <i class="search-mobile-trigger-icon fas fa-search"></i>
                    </div><!--//col-->

                    <div class="app-utilities col-auto">
                        <div class="app-utility-item app-user-dropdown dropdown">
                            <a class="dropdown-toggle" id="user-dropdown-toggle" data-bs-toggle="dropdown"
                                href="#" role="button" aria-expanded="false">
                                {{ auth()->user()->name }}
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="user-dropdown-toggle">
                                {{-- <li><a class="dropdown-item" href="account.html">Account</a></li>
                                    <li><a class="dropdown-item" href="settings.html">Settings</a></li>
                                    <li><hr class="dropdown-divider"></li> --}}
                                <li><a class="dropdown-item" href="{{ route('logout.perform') }}">Log Out</a></li>
                            </ul>
                        </div><!--//app-user-dropdown-->
                    </div><!--//app-utilities-->
                </div><!--//row-->
            </div><!--//app-header-content-->
        </div><!--//container-fluid-->
    </div><!--//app-header-inner-->

    {{-- id="app-sidepanel" class="app-sidepanel" --}}
    <div class="collapse" id="navbarToggleExternalContent">
        <div id="sidepanel-drop" class="sidepanel-drop"></div>
        <div class="sidepanel-inner d-flex flex-column">
            <a href="#" id="sidepanel-close" class="sidepanel-close d-xl-none">&times;</a>

            <nav id="app-nav-main" class="app-nav app-nav-main flex-grow-1 navbar-nav-scroll" style="width: 20%">
                <ul class="app-menu list-unstyled accordion" id="menu-accordion">
                    <li class="nav-item">
                        <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                        <a class="nav-link {{ request()->is('/') ? 'active' : '' }}" href="{{ route('home.index') }}">
                            <span class="nav-icon">
                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-house-door"
                                    fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.646 1.146a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 .146.354v7a.5.5 0 0 1-.5.5H9.5a.5.5 0 0 1-.5-.5v-4H7v4a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5v-7a.5.5 0 0 1 .146-.354l6-6zM2.5 7.707V14H6v-4a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v4h3.5V7.707L8 2.207l-5.5 5.5z" />
                                    <path fill-rule="evenodd"
                                        d="M13 2.5V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
                                </svg>
                            </span>
                            <span class="nav-link-text">Overview</span>
                        </a><!--//nav-link-->
                    </li><!--//nav-item-->
                    @if (auth()->user()->user_type_id == 1 || auth()->user()->user_type_id == 2)
                        <li class="nav-item has-submenu">
                            <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                            <a class="nav-link submenu-toggle {{ request()->is('setting*') ? 'active' : '' }}"
                                href="#" data-bs-toggle="collapse" data-bs-target="#submenu-1"
                                aria-expanded="false" aria-controls="submenu-1">
                                <span class="nav-icon">
                                    <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                        fill="currentColor" class="bi bi-gear" viewBox="0 0 16 16">
                                        <path
                                            d="M8 4.754a3.246 3.246 0 1 0 0 6.492 3.246 3.246 0 0 0 0-6.492zM5.754 8a2.246 2.246 0 1 1 4.492 0 2.246 2.246 0 0 1-4.492 0z" />
                                        <path
                                            d="M9.796 1.343c-.527-1.79-3.065-1.79-3.592 0l-.094.319a.873.873 0 0 1-1.255.52l-.292-.16c-1.64-.892-3.433.902-2.54 2.541l.159.292a.873.873 0 0 1-.52 1.255l-.319.094c-1.79.527-1.79 3.065 0 3.592l.319.094a.873.873 0 0 1 .52 1.255l-.16.292c-.892 1.64.901 3.434 2.541 2.54l.292-.159a.873.873 0 0 1 1.255.52l.094.319c.527 1.79 3.065 1.79 3.592 0l.094-.319a.873.873 0 0 1 1.255-.52l.292.16c1.64.893 3.434-.902 2.54-2.541l-.159-.292a.873.873 0 0 1 .52-1.255l.319-.094c1.79-.527 1.79-3.065 0-3.592l-.319-.094a.873.873 0 0 1-.52-1.255l.16-.292c.893-1.64-.902-3.433-2.541-2.54l-.292.159a.873.873 0 0 1-1.255-.52l-.094-.319zm-2.633.283c.246-.835 1.428-.835 1.674 0l.094.319a1.873 1.873 0 0 0 2.693 1.115l.291-.16c.764-.415 1.6.42 1.184 1.185l-.159.292a1.873 1.873 0 0 0 1.116 2.692l.318.094c.835.246.835 1.428 0 1.674l-.319.094a1.873 1.873 0 0 0-1.115 2.693l.16.291c.415.764-.42 1.6-1.185 1.184l-.291-.159a1.873 1.873 0 0 0-2.693 1.116l-.094.318c-.246.835-1.428.835-1.674 0l-.094-.319a1.873 1.873 0 0 0-2.692-1.115l-.292.16c-.764.415-1.6-.42-1.184-1.185l.159-.291A1.873 1.873 0 0 0 1.945 8.93l-.319-.094c-.835-.246-.835-1.428 0-1.674l.319-.094A1.873 1.873 0 0 0 3.06 4.377l-.16-.292c-.415-.764.42-1.6 1.185-1.184l.292.159a1.873 1.873 0 0 0 2.692-1.115l.094-.319z" />
                                    </svg>
                                </span>
                                <span class="nav-link-text">Setting</span>
                                <span class="submenu-arrow">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-chevron-down"
                                        fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd"
                                            d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
                                    </svg>
                                </span><!--//submenu-arrow-->
                            </a><!--//nav-link-->
                            <div id="submenu-1" class="collapse submenu submenu-1" data-bs-parent="#menu-accordion">
                                <ul class="submenu-list list-unstyled">
                                    @if (auth()->user()->user_type_id == 1)
                                        <li class="submenu-item"><a
                                                class="submenu-link {{ request()->is('*user_type*') ? 'active' : '' }}"
                                                href="{{ route('user_type.index') }}">ประเภทผู้ใช้</a></li>
                                    @endif
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*forklift*') ? 'active' : '' }}"
                                            href="{{ route('forklift.index') }}">Floklift</a></li>
                                    @if (auth()->user()->user_type_id == 1)
                                        <li class="submenu-item"><a
                                                class="submenu-link {{ request()->is('*location*') ? 'active' : '' }}"
                                                href="{{ route('location.index') }}">หน่วยงานย่อย</a></li>
                                    @endif
                                    @if (auth()->user()->user_type_id == 1)
                                        <li class="submenu-item"><a
                                                class="submenu-link {{ request()->is('*people*') ? 'active' : '' }}"
                                                href="{{ route('people.index') }}">พนักงาน</a></li>
                                    @endif
                                    @if (auth()->user()->user_type_id == 1)
                                        <li class="submenu-item"><a
                                                class="submenu-link {{ request()->is('*WhAct_type*') ? 'active' : '' }}"
                                                href="{{ route('WhAct_type.index') }}">สถานะงานรับ-จ่ายสินค้า</a></li>
                                    @endif
                                </ul>
                            </div>
                        </li><!--//nav-item-->
                    @endif
                    @if (auth()->user()->user_type_id == 1 || auth()->user()->user_type_id == 2)
                        <li class="nav-item">
                            <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                            <a class="nav-link {{ request()->is('pd_input*') ? 'active' : '' }}"
                                href="{{ route('pd_input.index') }}">
                                <span class="nav-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                        fill="currentColor" class="bi bi-cart4" viewBox="0 0 16 16">
                                        <path
                                            d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l.5 2H5V5H3.14zM6 5v2h2V5H6zm3 0v2h2V5H9zm3 0v2h1.36l.5-2H12zm1.11 3H12v2h.61l.5-2zM11 8H9v2h2V8zM8 8H6v2h2V8zM5 8H3.89l.5 2H5V8zm0 5a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z" />
                                    </svg>
                                </span>
                                <span class="nav-link-text">Product Input</span>
                            </a><!--//nav-link-->
                        </li><!--//nav-item-->
                        <li class="nav-item">
                            <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                            <a class="nav-link {{ request()->is('FL*') ? 'active' : '' }}"
                                href="{{ route('FL.index') }}">
                                <span class="nav-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                        fill="currentColor" class="bi bi-truck-flatbed" viewBox="0 0 16 16">
                                        <path
                                            d="M11.5 4a.5.5 0 0 1 .5.5V5h1.02a1.5 1.5 0 0 1 1.17.563l1.481 1.85a1.5 1.5 0 0 1 .329.938V10.5a1.5 1.5 0 0 1-1.5 1.5H14a2 2 0 1 1-4 0H5a2 2 0 1 1-4 0 1 1 0 0 1-1-1v-1h11V4.5a.5.5 0 0 1 .5-.5zM3 11a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm9 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm1.732 0h.768a.5.5 0 0 0 .5-.5V8.35a.5.5 0 0 0-.11-.312l-1.48-1.85A.5.5 0 0 0 13.02 6H12v4a2 2 0 0 1 1.732 1z" />
                                    </svg>
                                </span>
                                <span class="nav-link-text">ชั่วโมงการใช้งานรถ FL</span>
                            </a><!--//nav-link-->
                        </li><!--//nav-item-->
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('wh_act*') ? 'active' : '' }}"
                                href="{{ route('wh_act.index') }}">
                                <span class="nav-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                        fill="currentColor" class="bi bi-basket" viewBox="0 0 16 16">
                                        <path
                                            d="M5.757 1.071a.5.5 0 0 1 .172.686L3.383 6h9.234L10.07 1.757a.5.5 0 1 1 .858-.514L13.783 6H15a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1v4.5a2.5 2.5 0 0 1-2.5 2.5h-9A2.5 2.5 0 0 1 1 13.5V9a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h1.217L5.07 1.243a.5.5 0 0 1 .686-.172zM2 9v4.5A1.5 1.5 0 0 0 3.5 15h9a1.5 1.5 0 0 0 1.5-1.5V9H2zM1 7v1h14V7H1zm3 3a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 4 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 6 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 8 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 1 .5-.5zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 1 .5-.5z" />
                                    </svg>
                                </span>
                                <span class="nav-link-text">งานรับ-จ่ายประจำวัน</span>
                            </a><!--//nav-link-->
                        </li><!--//nav-item-->
                    @endif
                    @if (auth()->user()->user_type_id == 1 || auth()->user()->user_type_id == 3)
                        {{-- <li class="nav-item">
                            <a class="nav-link {{ request()->is('wh_rm*') ? 'active' : '' }}"
                                href="{{ route('wh_rm.index') }}">
                                <span class="nav-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                        fill="currentColor" class="bi bi-basket" viewBox="0 0 16 16">
                                        <path
                                            d="M5.757 1.071a.5.5 0 0 1 .172.686L3.383 6h9.234L10.07 1.757a.5.5 0 1 1 .858-.514L13.783 6H15a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1v4.5a2.5 2.5 0 0 1-2.5 2.5h-9A2.5 2.5 0 0 1 1 13.5V9a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h1.217L5.07 1.243a.5.5 0 0 1 .686-.172zM2 9v4.5A1.5 1.5 0 0 0 3.5 15h9a1.5 1.5 0 0 0 1.5-1.5V9H2zM1 7v1h14V7H1zm3 3a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 4 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 6 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 8 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 1 .5-.5zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 1 .5-.5z" />
                                    </svg>
                                </span>
                                <span class="nav-link-text">งาน WHRM</span>

                            </a><!--//nav-link-->

                        </li><!--//nav-item--> --}}


                        <li class="nav-item has-submenu">
                            <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                            <a class="nav-link submenu-toggle {{ request()->is('st_wh*') ? 'active' : '' }}"
                                href="#" data-bs-toggle="collapse" data-bs-target="#submenu-WHRM"
                                aria-expanded="false" aria-controls="submenu-WHRM">
                                <span class="nav-icon">
                                    <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                        fill="currentColor" class="bi bi-basket" viewBox="0 0 16 16">
                                        <path
                                            d="M5.757 1.071a.5.5 0 0 1 .172.686L3.383 6h9.234L10.07 1.757a.5.5 0 1 1 .858-.514L13.783 6H15a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1v4.5a2.5 2.5 0 0 1-2.5 2.5h-9A2.5 2.5 0 0 1 1 13.5V9a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h1.217L5.07 1.243a.5.5 0 0 1 .686-.172zM2 9v4.5A1.5 1.5 0 0 0 3.5 15h9a1.5 1.5 0 0 0 1.5-1.5V9H2zM1 7v1h14V7H1zm3 3a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 4 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 6 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 8 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 1 .5-.5zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 1 .5-.5z" />
                                    </svg>
                                </span>
                                <span class="nav-link-text">งาน WHRM</span>
                                <span class="submenu-arrow">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16"
                                        class="bi bi-chevron-down" fill="currentColor"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd"
                                            d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
                                    </svg>
                                </span><!--//submenu-arrow-->
                            </a><!--//nav-link-->
                            <div id="submenu-WHRM" class="collapse submenu submenu-2"
                                data-bs-parent="#menu-accordion">
                                <ul class="submenu-list list-unstyled">
                                    <li class="submenu-item"><a
                                            class="submenu-link
                                            {{ request()->is('wh_rm*') ? 'active' : '' }}"
                                            href="{{ route('wh_rm.index') }}">งาน WHRM</a>
                                    </li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('test') ? 'active' : '' }}"
                                            href="{{ route('test') }}">งาน
                                            รับวัตถุดิบ (ทดสอบระบบ)</a>
                                    </li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('whrmSetting') ? 'active' : '' }}"
                                            href="{{ route('whrmSetting.index') }}">
                                            WhRm Brokers Setting (ทดสอบระบบ)</a>
                                    </li>

                                </ul>
                            </div>
                        </li><!--//nav-item-->

                        <li class="nav-item has-submenu">
                            <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                            <a class="nav-link submenu-toggle {{ request()->is('st_wh*') ? 'active' : '' }}"
                                href="#" data-bs-toggle="collapse" data-bs-target="#submenu-2"
                                aria-expanded="false" aria-controls="submenu-2">
                                <span class="nav-icon">
                                    <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                        fill="currentColor" class="bi bi-gear" viewBox="0 0 16 16">
                                        <path
                                            d="M8 4.754a3.246 3.246 0 1 0 0 6.492 3.246 3.246 0 0 0 0-6.492zM5.754 8a2.246 2.246 0 1 1 4.492 0 2.246 2.246 0 0 1-4.492 0z" />
                                        <path
                                            d="M9.796 1.343c-.527-1.79-3.065-1.79-3.592 0l-.094.319a.873.873 0 0 1-1.255.52l-.292-.16c-1.64-.892-3.433.902-2.54 2.541l.159.292a.873.873 0 0 1-.52 1.255l-.319.094c-1.79.527-1.79 3.065 0 3.592l.319.094a.873.873 0 0 1 .52 1.255l-.16.292c-.892 1.64.901 3.434 2.541 2.54l.292-.159a.873.873 0 0 1 1.255.52l.094.319c.527 1.79 3.065 1.79 3.592 0l.094-.319a.873.873 0 0 1 1.255-.52l.292.16c1.64.893 3.434-.902 2.54-2.541l-.159-.292a.873.873 0 0 1 .52-1.255l.319-.094c1.79-.527 1.79-3.065 0-3.592l-.319-.094a.873.873 0 0 1-.52-1.255l.16-.292c.893-1.64-.902-3.433-2.541-2.54l-.292.159a.873.873 0 0 1-1.255-.52l-.094-.319zm-2.633.283c.246-.835 1.428-.835 1.674 0l.094.319a1.873 1.873 0 0 0 2.693 1.115l.291-.16c.764-.415 1.6.42 1.184 1.185l-.159.292a1.873 1.873 0 0 0 1.116 2.692l.318.094c.835.246.835 1.428 0 1.674l-.319.094a1.873 1.873 0 0 0-1.115 2.693l.16.291c.415.764-.42 1.6-1.185 1.184l-.291-.159a1.873 1.873 0 0 0-2.693 1.116l-.094.318c-.246.835-1.428.835-1.674 0l-.094-.319a1.873 1.873 0 0 0-2.692-1.115l-.292.16c-.764.415-1.6-.42-1.184-1.185l.159-.291A1.873 1.873 0 0 0 1.945 8.93l-.319-.094c-.835-.246-.835-1.428 0-1.674l.319-.094A1.873 1.873 0 0 0 3.06 4.377l-.16-.292c-.415-.764.42-1.6 1.185-1.184l.292.159a1.873 1.873 0 0 0 2.692-1.115l.094-.319z" />
                                    </svg>
                                </span>
                                <span class="nav-link-text">Stock</span>
                                <span class="submenu-arrow">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16"
                                        class="bi bi-chevron-down" fill="currentColor"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd"
                                            d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
                                    </svg>
                                </span><!--//submenu-arrow-->
                            </a><!--//nav-link-->
                            <div id="submenu-2" class="collapse submenu submenu-2" data-bs-parent="#menu-accordion">
                                <ul class="submenu-list list-unstyled">
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*st_product*') ? 'active' : '' }}"
                                            href="{{ route('st_product.index') }}">set สินค้า</a></li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*st_stock*') ? 'active' : '' }}"
                                            href="{{ route('st_stock.index') }}">set คงคลัง</a></li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*st_broker*') ? 'active' : '' }}"
                                            href="{{ route('st_broker.index') }}">set Broker</a></li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*st_receive*') ? 'active' : '' }}"
                                            href="{{ route('st_pd_stock') }}">สินค้า</a></li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*st_withdraw*') ? 'active' : '' }}"
                                            href="{{ route('st_withdraw_stock') }}">Broker</a></li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*st_report*') ? 'active' : '' }}"
                                            href="{{ route('st_report_view') }}">รายงาน</a></li>
                                </ul>
                            </div>
                        </li><!--//nav-item-->

                        {{-- <li class="nav-item">
                                <a class="nav-link {{ request()->is('st_wh_begin*') ? 'active' : '' }}" href="{{ route('st_wh_begin.index') }}">
                                    <span class="nav-icon">
                                        <i class="fab fa-product-hunt"></i>
                                    </span>
                                    <span class="nav-link-text">กระสอบ</span>
                                </a><!--//nav-link-->
                            </li><!--//nav-item--> --}}
                    @endif

                    @if (auth()->user()->user_type_id == 1 || auth()->user()->user_type_id == 4)
                        <li class="nav-item has-submenu">
                            <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                            <a class="nav-link submenu-toggle {{ request()->is('base_data*') ? 'active' : '' }}"
                                href="#" data-bs-toggle="collapse" data-bs-target="#submenu-3"
                                aria-expanded="false" aria-controls="submenu-3">
                                <span class="nav-icon">
                                    <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                        fill="currentColor" class="bi bi-gear" viewBox="0 0 16 16">
                                        <path
                                            d="M8 4.754a3.246 3.246 0 1 0 0 6.492 3.246 3.246 0 0 0 0-6.492zM5.754 8a2.246 2.246 0 1 1 4.492 0 2.246 2.246 0 0 1-4.492 0z" />
                                        <path
                                            d="M9.796 1.343c-.527-1.79-3.065-1.79-3.592 0l-.094.319a.873.873 0 0 1-1.255.52l-.292-.16c-1.64-.892-3.433.902-2.54 2.541l.159.292a.873.873 0 0 1-.52 1.255l-.319.094c-1.79.527-1.79 3.065 0 3.592l.319.094a.873.873 0 0 1 .52 1.255l-.16.292c-.892 1.64.901 3.434 2.541 2.54l.292-.159a.873.873 0 0 1 1.255.52l.094.319c.527 1.79 3.065 1.79 3.592 0l.094-.319a.873.873 0 0 1 1.255-.52l.292.16c1.64.893 3.434-.902 2.54-2.541l-.159-.292a.873.873 0 0 1 .52-1.255l.319-.094c1.79-.527 1.79-3.065 0-3.592l-.319-.094a.873.873 0 0 1-.52-1.255l.16-.292c.893-1.64-.902-3.433-2.541-2.54l-.292.159a.873.873 0 0 1-1.255-.52l-.094-.319zm-2.633.283c.246-.835 1.428-.835 1.674 0l.094.319a1.873 1.873 0 0 0 2.693 1.115l.291-.16c.764-.415 1.6.42 1.184 1.185l-.159.292a1.873 1.873 0 0 0 1.116 2.692l.318.094c.835.246.835 1.428 0 1.674l-.319.094a1.873 1.873 0 0 0-1.115 2.693l.16.291c.415.764-.42 1.6-1.185 1.184l-.291-.159a1.873 1.873 0 0 0-2.693 1.116l-.094.318c-.246.835-1.428.835-1.674 0l-.094-.319a1.873 1.873 0 0 0-2.692-1.115l-.292.16c-.764.415-1.6-.42-1.184-1.185l.159-.291A1.873 1.873 0 0 0 1.945 8.93l-.319-.094c-.835-.246-.835-1.428 0-1.674l.319-.094A1.873 1.873 0 0 0 3.06 4.377l-.16-.292c-.415-.764.42-1.6 1.185-1.184l.292.159a1.873 1.873 0 0 0 2.692-1.115l.094-.319z" />
                                    </svg>
                                </span>
                                <span class="nav-link-text">FS Base Data</span>
                                <span class="submenu-arrow">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16"
                                        class="bi bi-chevron-down" fill="currentColor"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd"
                                            d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
                                    </svg>
                                </span><!--//submenu-arrow-->
                            </a><!--//nav-link-->
                            <div id="submenu-3" class="collapse submenu submenu-3" data-bs-parent="#menu-accordion">
                                <ul class="submenu-list list-unstyled">
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*shift*') ? 'active' : '' }}"
                                            href="{{ route('shift.index') }}">ตั้งค่ากะ</a></li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*product*') ? 'active' : '' }}"
                                            href="{{ route('product.index') }}">สินค้า</a></li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*FsWhs*') ? 'active' : '' }}"
                                            href="{{ url('/setting/FsWhs') }}">คลังสินค้า FS</a></li>
                                    {{-- <li class="submenu-item"><a class="submenu-link {{ request()->is('*step*') ? 'active' : '' }}" href="{{ route('step.index') }}">Step</a></li> --}}
                                    {{-- <li class="submenu-item"><a class="submenu-link {{ request()->is('*for_tg*') ? 'active' : '' }}" href="{{ route('for_tg.index') }}">Step สำหรับเป้าการทำงาน</a></li> --}}
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*store*') ? 'active' : '' }}"
                                            href="{{ route('store.index') }}">แผนก</a></li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*lose_type*') ? 'active' : '' }}"
                                            href="{{ route('lose_type.index') }}">สาเหตุการหักลบ</a></li>
                                </ul>
                            </div>
                        </li><!--//nav-item-->

                        <li class="nav-item has-submenu">
                            <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                            <a class="nav-link submenu-toggle {{ request()->is('to_reuse*') || request()->is('wash*') || request()->is('dry*') || request()->is('stock*') || request()->is('to_use*') ? 'active' : '' }}"
                                href="#" data-bs-toggle="collapse" data-bs-target="#submenu-4"
                                aria-expanded="false" aria-controls="submenu-4">
                                <span class="nav-icon">
                                    <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                        fill="currentColor" class="bi bi-gear" viewBox="0 0 16 16">
                                        <path
                                            d="M8 4.754a3.246 3.246 0 1 0 0 6.492 3.246 3.246 0 0 0 0-6.492zM5.754 8a2.246 2.246 0 1 1 4.492 0 2.246 2.246 0 0 1-4.492 0z" />
                                        <path
                                            d="M9.796 1.343c-.527-1.79-3.065-1.79-3.592 0l-.094.319a.873.873 0 0 1-1.255.52l-.292-.16c-1.64-.892-3.433.902-2.54 2.541l.159.292a.873.873 0 0 1-.52 1.255l-.319.094c-1.79.527-1.79 3.065 0 3.592l.319.094a.873.873 0 0 1 .52 1.255l-.16.292c-.892 1.64.901 3.434 2.541 2.54l.292-.159a.873.873 0 0 1 1.255.52l.094.319c.527 1.79 3.065 1.79 3.592 0l.094-.319a.873.873 0 0 1 1.255-.52l.292.16c1.64.893 3.434-.902 2.54-2.541l-.159-.292a.873.873 0 0 1 .52-1.255l.319-.094c1.79-.527 1.79-3.065 0-3.592l-.319-.094a.873.873 0 0 1-.52-1.255l.16-.292c.893-1.64-.902-3.433-2.541-2.54l-.292.159a.873.873 0 0 1-1.255-.52l-.094-.319zm-2.633.283c.246-.835 1.428-.835 1.674 0l.094.319a1.873 1.873 0 0 0 2.693 1.115l.291-.16c.764-.415 1.6.42 1.184 1.185l-.159.292a1.873 1.873 0 0 0 1.116 2.692l.318.094c.835.246.835 1.428 0 1.674l-.319.094a1.873 1.873 0 0 0-1.115 2.693l.16.291c.415.764-.42 1.6-1.185 1.184l-.291-.159a1.873 1.873 0 0 0-2.693 1.116l-.094.318c-.246.835-1.428.835-1.674 0l-.094-.319a1.873 1.873 0 0 0-2.692-1.115l-.292.16c-.764.415-1.6-.42-1.184-1.185l.159-.291A1.873 1.873 0 0 0 1.945 8.93l-.319-.094c-.835-.246-.835-1.428 0-1.674l.319-.094A1.873 1.873 0 0 0 3.06 4.377l-.16-.292c-.415-.764.42-1.6 1.185-1.184l.292.159a1.873 1.873 0 0 0 2.692-1.115l.094-.319z" />
                                    </svg>
                                </span>
                                <span class="nav-link-text">FS Step</span>
                                <span class="submenu-arrow">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16"
                                        class="bi bi-chevron-down" fill="currentColor"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd"
                                            d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
                                    </svg>
                                </span><!--//submenu-arrow-->
                            </a><!--//nav-link-->
                            <div id="submenu-4" class="collapse submenu submenu-4" data-bs-parent="#menu-accordion">
                                <ul class="submenu-list list-unstyled">
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*target*') ? 'active' : '' }}"
                                            href="{{ route('target.index') }}">เป้าการทำงาน</a></li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*to_reuse*') ? 'active' : '' }}"
                                            href="{{ route('to_reuse.index') }}">รับเข้า</a></li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*wash*') ? 'active' : '' }}"
                                            href="{{ route('wash.index') }}">การซัก</a></li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*dry*') ? 'active' : '' }}"
                                            href="{{ route('dry.index') }}">การปั่น</a></li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*shine*') ? 'active' : '' }}"
                                            href="{{ route('shine.index') }}">การตาก</a></li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*choose_box*') ? 'active' : '' }}"
                                            href="{{ route('choose_box.index') }}">การคัด</a></li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*stock*') ? 'active' : '' }}"
                                            href="{{ route('stock.index') }}">การพับ</a></li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*fs_report*') ? 'active' : '' }}"
                                            href="{{ route('fs_report.index') }}">รายงาน</a></li>
                                </ul>
                            </div>
                        </li><!--//nav-item-->
                    @endif


                    @if (auth()->user()->user_type_id == 1 || auth()->user()->user_type_id == 5)
                        <li class="nav-item has-submenu">
                            <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                            <a class="nav-link submenu-toggle {{ request()->is('st_wh*') ? 'active' : '' }}"
                                href="#" data-bs-toggle="collapse" data-bs-target="#submenu-5"
                                aria-expanded="false" aria-controls="submenu-5">
                                <span class="nav-icon">
                                    <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                        fill="currentColor" class="bi bi-gear" viewBox="0 0 16 16">
                                        <path
                                            d="M8 4.754a3.246 3.246 0 1 0 0 6.492 3.246 3.246 0 0 0 0-6.492zM5.754 8a2.246 2.246 0 1 1 4.492 0 2.246 2.246 0 0 1-4.492 0z" />
                                        <path
                                            d="M9.796 1.343c-.527-1.79-3.065-1.79-3.592 0l-.094.319a.873.873 0 0 1-1.255.52l-.292-.16c-1.64-.892-3.433.902-2.54 2.541l.159.292a.873.873 0 0 1-.52 1.255l-.319.094c-1.79.527-1.79 3.065 0 3.592l.319.094a.873.873 0 0 1 .52 1.255l-.16.292c-.892 1.64.901 3.434 2.541 2.54l.292-.159a.873.873 0 0 1 1.255.52l.094.319c.527 1.79 3.065 1.79 3.592 0l.094-.319a.873.873 0 0 1 1.255-.52l.292.16c1.64.893 3.434-.902 2.54-2.541l-.159-.292a.873.873 0 0 1 .52-1.255l.319-.094c1.79-.527 1.79-3.065 0-3.592l-.319-.094a.873.873 0 0 1-.52-1.255l.16-.292c.893-1.64-.902-3.433-2.541-2.54l-.292.159a.873.873 0 0 1-1.255-.52l-.094-.319zm-2.633.283c.246-.835 1.428-.835 1.674 0l.094.319a1.873 1.873 0 0 0 2.693 1.115l.291-.16c.764-.415 1.6.42 1.184 1.185l-.159.292a1.873 1.873 0 0 0 1.116 2.692l.318.094c.835.246.835 1.428 0 1.674l-.319.094a1.873 1.873 0 0 0-1.115 2.693l.16.291c.415.764-.42 1.6-1.185 1.184l-.291-.159a1.873 1.873 0 0 0-2.693 1.116l-.094.318c-.246.835-1.428.835-1.674 0l-.094-.319a1.873 1.873 0 0 0-2.692-1.115l-.292.16c-.764.415-1.6-.42-1.184-1.185l.159-.291A1.873 1.873 0 0 0 1.945 8.93l-.319-.094c-.835-.246-.835-1.428 0-1.674l.319-.094A1.873 1.873 0 0 0 3.06 4.377l-.16-.292c-.415-.764.42-1.6 1.185-1.184l.292.159a1.873 1.873 0 0 0 2.692-1.115l.094-.319z" />
                                    </svg>
                                </span>
                                <span class="nav-link-text">TR</span>
                                <span class="submenu-arrow">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16"
                                        class="bi bi-chevron-down" fill="currentColor"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd"
                                            d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
                                    </svg>
                                </span><!--//submenu-arrow-->
                            </a><!--//nav-link-->
                            <div id="submenu-5" class="collapse submenu submenu-2" data-bs-parent="#menu-accordion">
                                <ul class="submenu-list list-unstyled">
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*trucks*') ? 'active' : '' }}"
                                            href="{{ route('trucks.index') }}">set รถบรรทุก</a></li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*tails*') ? 'active' : '' }}"
                                            href="{{ route('tails.index') }}">set หางรถ</a></li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*vessel_lines*') ? 'active' : '' }}"
                                            href="{{ route('vessel_lines.index') }}">set Vessel Line</a></li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*importdlps*') ? 'active' : '' }}"
                                            href="{{ route('importdlps.index') }}">Delivery Plan</a></li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*manual_plan*') ? 'active' : '' }}"
                                            href="{{ route('manual_plan') }}">Manual Plan</a></li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*calendar*') ? 'active' : '' }}"
                                            href="{{ route('calendar.index') }}" target="_blank">Calendar Plan</a>
                                    </li>
                                    <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*scheduletrucksSammary*') ? 'active' : '' }}"
                                            href="{{ route('scheduletrucksSumary') }}" target="_blank">Schedule
                                            Trucks Sammary</a></li>

                                    {{-- <li class="submenu-item"><a
                                            class="submenu-link {{ request()->is('*test_tr*') ? 'active' : '' }}"
                                            href="{{ route('test_tr') }}" target="_blank">Test</a>
                                    </li> --}}


                                </ul>
                            </div>
                        </li><!--//nav-item-->
                    @endif

                    @if (auth()->user()->user_type_id == 1)
                        <li class="nav-item">
                            <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                            <a class="nav-link {{ request()->is('user*') ? 'active' : '' }}"
                                href="{{ route('user.index') }}">
                                <span class="nav-icon">
                                    <i class="far fa-address-card"></i>
                                </span>
                                <span class="nav-link-text">User</span>
                            </a><!--//nav-link-->
                        </li><!--//nav-item-->
                    @endif
                </ul><!--//app-menu-->
            </nav><!--//app-nav-->
        </div><!--//sidepanel-inner-->

    </div><!--//app-sidepanel-->
</header><!--//app-header-->
{{-- @endsection --}}
