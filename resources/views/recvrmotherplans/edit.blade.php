@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Recv Banana Main Plan {{ $recvrmotherplan->id }}</div>
                    <div class="panel-body">

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($recvrmotherplan, [
                            'method' => 'PATCH',
                            'url' => ['/recvrmotherplans', $recvrmotherplan->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('recvrmotherplans.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection