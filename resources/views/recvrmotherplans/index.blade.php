@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Recv Banana Main Plan</div>
                    <div class="panel-body">

                        <a href="{{ url('/recvrmotherplans/create') }}" class="btn btn-primary btn-xs" title="Add New crop">Add</a>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>WH Plan NAme</th>
                                        <th>Total Plan weight</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($recvrmotherplans as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->total_plan }}</td>
                                        <td>{{ $item->status }}</td>
                                        <td>
                                            <a href="{{ url('/recvrmotherplans/' . $item->id) }}" class="btn btn-success btn-xs" title="View job data">View</a>
                                            <a href="{{ url('/recvrmotherplans/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit job data">Edit</a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/recvrmotherplans', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('Delete', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete  job data',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $recvrmotherplans->render('vendor.pagination.bootstrap-4') !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection