@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Recv Banana Main Plan {{ $recvrmotherplan->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('recvrmotherplans/' . $recvrmotherplan->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit job data">Edit</a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['recvrmotherplans', $recvrmotherplan->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete job data',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $recvrmotherplan->id }}</td>
                                    </tr>
                                    <tr>
                                        <th>Name</th><td>{{ $recvrmotherplan->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Total Plan</th><td>{{ $recvrmotherplan->total_plan }}</td>
                                    </tr>
                                    <tr>
                                        <th>Status</th><td>{{ $recvrmotherplan->status }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection