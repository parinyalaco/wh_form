@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="page-heading row">
                <div class="pull-right col-auto my-2">
                    <a class="btn btn-secondary" href="{{ route('FsStocks.index') }}">Back</a>
                </div>
                <h1 class="col">FS Warehouse Transactions -> Edit # {{ $query->id }}</h1>
            </div>

            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>

            <div class="tab-content" id="orders-table-tab-content">
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <form method="POST" action="{{ route('FsStocks.update', $query->id) }}">
                            @csrf
                            @method('PUT')
                            <div class="row mb-3">
                                <label for="tran_dt" class="col-md-4 col-form-label text-md-end">{{ __('วันเวลา') }}</label>

                                <div class="col-md-6">
                                    <input id="tran_dt" type="datetime-local" class="form-control @error('tran_dt') is-invalid @enderror" name="tran_dt"
                                        value="{{ date('Y-m-d H:i:s',strtotime($query->tran_dt)) }}"
                                    required autofocus>

                                    @error('tran_dt')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                {{-- <label for="status_wash" class="col-md-4 col-form-label text-md-end">{{ __('ต้องซัก') }}</label> --}}
                                <label for="tran_type" class="col-md-4 col-form-label text-md-end">{{ __('ประเภท') }}</label>
                                <div class="col-md-6">
                                    <select name="tran_type" class="form-control" id="tran_type">
                                        @foreach ($transList as $optionKey => $optionValue)
                                            <option value="{{ $optionKey }}" {{ ($query->tran_type == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                                        @endforeach
                                    </select>
                                    @error('tran_type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="desc" class="col-md-4 col-form-label text-md-end">{{ __('รายละเอียด') }}</label>

                                <div class="col-md-6">
                                    <input id="desc" type="text" class="form-control @error('desc') is-invalid @enderror" name="desc" value="{{ $query->desc }}" autocomplete="desc">

                                    @error('desc')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                {{-- <label for="status_wash" class="col-md-4 col-form-label text-md-end">{{ __('ต้องซัก') }}</label> --}}
                                <label for="user" class="col-md-4 col-form-label text-md-end">{{ __('ผู้ทำ') }}</label>
                                <div class="col-md-6">
                                    <input id="user" type="text" class="form-control @error('user') is-invalid @enderror" name="user" value="{{ $query->user }}" type="number">

                                    @error('user')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-md-4"></label>

                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success">
                                        {{ __('แก้ไข') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div><!--//app-card-body-->
                </div><!--//app-card-->
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
    {{-- <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>      --}}
@endsection
