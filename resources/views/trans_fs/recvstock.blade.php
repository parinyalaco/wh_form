@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="page-heading row">
                <div class="pull-right col-auto my-2">
                    {{-- <a class="btn btn-secondary" href="{{ route('FsStocks.index') }}">Back</a> --}}
                    <a class="btn btn-secondary" href="{{ route('FsStocks.viewstock',$data->fs_wh_id) }}">Back</a>
                </div>
                <div class="col">
                    <h1 class="row">FS Warehouse Transactions -> Detail # {{ $data->fswh->name }} / {{ $data->name }}</h1>
                    <div class="row" id="show_div">
                    </div>
                </div>
            </div>
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>

            <div class="tab-content" id="orders-table-tab-content">
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <div class="row">
                            <div class="col-md-6"><b>WH : </b>{{ $data->fswh->name }}</div>
                            <div class="col-md-6"><b>Location : </b>{{ $data->name }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content" id="orders-table-tab-content">
                <form method="POST" action="{{ route('FsStocks.recvToStockAction', $data->id) }}">
                    @csrf
                    {{-- new = เพิ่มใหม่, tran = ตัดจาก step->stock --}}
                    <input type="hidden" name="desc" id="desc" value="new">
                    <input type="hidden" name="old_pd" id="old_pd" value="{{ $old_pd }}">
                    <input type="hidden" name="old_pl" id="old_pl" value="{{ $old_pl }}">
                    <div class="row">
                        <div class="col-md-3">
                            <select name="productid" class="form-control" id="productid" onchange="set_null();" @if(!empty($old_pd)) disabled @else required @endif>
                                <option value="">...สินค้า...</option>
                                @foreach ($productList as $optionKey => $optionValue)
                                    <option value="{{ $optionKey }}" @if(!empty($old_pd) && $old_pd==$optionKey) selected @endif>{{ $optionValue }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <input type="text" name="pallet" id="pallet" class="form-control"
                                placeholder="ตำแหน่ง Pallet" @if (!empty($old_pl))value="{{ $old_pl }}" disabled @else required @endif >
                        </div>
                        <div class="col-md-3">
                            <input type="date" name="to_date" id="to_date" class="form-control" placeholder="วันที่" value="{{ date('Y-m-d') }}" required>
                        </div>
                        <div class="col-md-3">
                            <input type="number" name="volumn" id="volumn" class="form-control" min="1" placeholder="จำนวนของ" required>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-md-4"></label>

                        <div class="col-md-6"><br />
                            <button type="submit" class="btn btn-success">
                                {{ __('บันทึก') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="tab-content" id="orders-table-tab-content">
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <div class="row">
                            <div class="col-md-3"><b>สินค้า</b></div>
                            <div class="col-md-3"><b>Pallet</b></div>
                            <div class="col-md-3"><b>วันที่</b></div>
                            <div class="col-md-3"><b>จำนวน</b></div>
                        </div>
                        @if(!empty($query))
                            @foreach($query as $item)
                                <div class="row">
                                    <div class="col-md-3"><a>{{ $item->product->name }}</a></div>
                                    <div class="col-md-3"><a>{{ $item->pallet }}</a></div>
                                    <div class="col-md-3"><a>{{ $item->to_date }}</a></div>
                                    <div class="col-md-3">
                                        <a>{{ $item->volumn }}</a>
                                        {{-- <a class="btn btn-secondary" href="{{ route('FsStocks.deleteDetailAction', $item->id) }}">X</a> --}}
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="row">
                                <div class="col-md-12"><b>ไม่มีข้อมูล</b></div>
                            </div>
                        @endif

                    </div>

                </div>


            </div>
        </div>
    </div>
    <script>
        window.onload = function(event) {
            // /set_div()
        }

        function set_to_save(pd, num) {
            set_colur(pd, num)
            document.getElementById("productid").value = pd
            document.getElementById("desc").value = 'tran'
            document.getElementById("volumn").value = num
            document.getElementById("volumn").max = num
            document.getElementById("volumn").select();
        }
        function set_colur(pd, num) {
            // console.log('set',pd, num);
            const pd_all = <?=json_encode($pd);?>;
            Object.keys(pd_all).forEach(key => {
                var to_div = document.getElementById("set_topd["+key+"]")
                if(to_div){
                    if(key==pd){
                        document.getElementById("set_topd["+key+"]").style.backgroundColor = "#15a362"
                    }else{
                        document.getElementById("set_topd["+key+"]").style.backgroundColor = "#D2D3D2"
                    }
                }
            });
        }
        function set_null() {
            document.getElementById("desc").value = 'new'
            document.getElementById("volumn").value = ''
            document.getElementById("volumn").removeAttribute("max");
        }
    </script>
@endsection
