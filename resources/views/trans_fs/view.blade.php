@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="page-heading row">
                <div class="pull-right col-auto my-2">
                    <a class="btn btn-secondary" href="{{ route('FsStocks.index') }}">Back</a>
                </div>
                <h1 class="col">FS Warehouse Transactions -> Detail # {{ $query->id }}</h1>
            </div>

            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>

            <div class="tab-content" id="orders-table-tab-content">
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <div class="row">
                            <div class="col-md-4"><b>วันเวลา : </b>{{ date('Y-m-d H:i:s', strtotime($query->tran_dt)) }}
                            </div>
                            <div class="col-md-4"><b>ประเภท : </b>{{ $query->tran_type }}</div>
                            <div class="col-md-4"><b>ผู้ทำ : </b>{{ $query->tran_tyuserpe }}</div>
                            <div class="col-md-12">{{ $query->desc }}</div>
                        </div>
                    </div>
                </div>
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <div class="row">
                            <div class="col-md-3"><b>Location</b></div>
                            <div class="col-md-3"><b>สินค้า</b></div>
                            <div class="col-md-3"><b>Pallet</b></div>
                            <div class="col-md-3"><b>จำนวน</b></div>
                        </div>
                        @foreach ($query->fswhtrands()->get() as $item)
                        <div class="row">
                            <div class="col-md-3"><b>{{ $item->fsloc->name }}</b></div>
                            <div class="col-md-3"><b>{{ $item->product->name }}</b></div>
                            <div class="col-md-3"><b>{{ $item->pallet }}</b></div>
                            <div class="col-md-3"><b>{{ $item->volumn }}</b> <a class="btn btn-secondary" href="{{ route('FsStocks.deleteDetailAction',$item->id) }}">X</a></div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="pull-right col-auto my-2">
                <button class="btn btn-secondary" id="addsub">+ เพิ่ม</button>
            </div>
            <div class="tab-content" id="orders-table-tab-content">
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <form method="POST" action="{{ route('FsStocks.addDetailAction', $query->id) }}">
                            @csrf
                            <input type="hidden" name="locnum" id="locnum" value="0" />
                             <div class="row mb-3">
                                {{-- <label for="status_wash" class="col-md-4 col-form-label text-md-end">{{ __('ต้องซัก') }}</label> --}}
                                <label for="wh_fs_id" class="col-md-4 col-form-label text-md-end">{{ __('ห้อง') }}</label>
                                <div class="col-md-6">
                                    <select name="wh_fs_id" class="form-control" id="wh_fs_id">
                                        @foreach ($fswhList as $optionKey => $optionValue)
                                            <option value="{{ $optionKey }}" >{{ $optionValue }}</option>
                                        @endforeach
                                    </select>
                                    @error('wh_fs_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div id="subdata"></div>
                            <div class="row mb-3">
                                <label class="col-md-4"></label>

                                <div class="col-md-6"><br/>
                                    <button type="submit" class="btn btn-success">
                                        {{ __('บันทึก') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--//app-card-body-->
                </div>
                <!--//app-card-->
            </div>
            <!--//tab-content-->
        </div>
        <!--//container-fluid-->
    </div>
    <!--//app-content-->
    <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
    <script>
        $(document).ready(function() {
            $('#addsub').on('click', function() {
                var locno = $('#locnum').val();
                $('#subdata').append(
                    '<div class="row"><div class="col-md-3"><input type="text" name="location' + locno +
                    '" id="location' + locno +
                    '"  class="form-control autocomplete" placeholder="ค้นหาตำแหน่ง" data-loc-id="' + locno +
                    '" ><input type="hidden" name="locationid' +
                    locno + '"  id="locationid' +
                    locno + '"></div><div class="col-md-3"><select name="productid' + locno +
                    '" class="form-control" id="productid' + locno +
                    '">@foreach ($productList as $optionKey => $optionValue)<option value="{{ $optionKey }}" >{{ $optionValue }}</option> @endforeach </select></div><div class="col-md-3"><input type="text" name="pallet' + locno +
                    '" id="pallet' + locno +
                    '"  class="form-control"  placeholder="ตำแหน่ง Pallet"  data-loc-id="' + locno +
                    '" ></div><div class="col-md-3"><input type="number" name="volumn' + locno +
                    '" id="volumn' + locno +
                    '"  class="form-control"  placeholder="จำนวนของ" data-loc-id="' + locno +
                    '" ></div></div>');
                locno++;
                $('#locnum').val(locno);
            });

            $('#subdata').on('focus', '.autocomplete', function (event) {
                $(this).autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "/FsStocks/searchLoc/" + $('#wh_fs_id').val(),
                            dataType: "json",
                            data: JSON.stringify(request),
                            success: function(data) {
                                response($.map(data.data, function (el) {
                                    return {
                                        label: el.name,
                                        value: el.name,
                                        id: el.id
                                    };
                                }));
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var id = $(this).attr('data-loc-id');
                        $("#locationid"+id).val(ui.item.id);
                    }
                });
            });
        });
    </script>
@endsection
