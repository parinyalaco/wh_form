@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="page-heading row">
                <div class="pull-right col-auto my-2">
                    {{-- <a class="btn btn-secondary" href="{{ route('FsStocks.index') }}">Back</a> --}}
                    <a class="btn btn-secondary" href="{{ route('FsStocks.viewstock',$data->fs_wh_id) }}">Back</a>
                </div>
                <h1 class="col">FS Warehouse Transactions -> จ่ายของ # {{ $data->id }}</h1>
            </div>

            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>

            <div class="tab-content" id="orders-table-tab-content">
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <div class="row">
                            {{-- <div class="col-md-6"><b>WH : </b>{{ $data->fsloc->fswh->name }}</div>
                            <div class="col-md-6"><b>Location : </b>{{ $data->fsloc->name }}</div> --}}
                            <div class="col-md-6"><b>WH : </b>{{ $data->fswh->name }}</div>
                            <div class="col-md-6"><b>Location : </b>{{ $data->name }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content" id="orders-table-tab-content">
                <form method="POST" action="{{ route('FsStocks.deliverFromStockAction', $data->id) }}">
                    @csrf
                    <input type="hidden" name="old_pl" id="old_pl" value="{{ $pd_pl[0] }}">
                    <input type="hidden" name="old_pd" id="old_pd" value="{{ $pd_pl[1] }}">
                    <div class="row">
                        <div class="col-md-2">
                            {{-- {{ $data->product->name }} --}}
                            <input type="text" class="form-control" name="pd" id="pd" value="{{ $pd_pl[3][$pd_pl[1]] }}" disabled>
                        </div>
                        <div class="col-md-2">
                            {{-- {{ $data->pallet }} --}}
                            <input type="text" class="form-control" name="pd" id="pd" value="{{ $pd_pl[0] }}" disabled>
                        </div>
                        <div class="col-md-3">
                            <input type="date" name="to_date" id="to_date" class="form-control" placeholder="วันที่" value="{{ date('Y-m-d') }}" required>
                        </div>
                        <div class="col-md-2">
                            <input type="number" min=1 max={{ $pd_pl[2] }} value="{{ $pd_pl[2] }}" name="volumn" id="volumn" class="form-control" placeholder="จำนวนของ">
                        </div>
                         <div class="col-md-3">
                            <input type="text" name="desc" id="desc" class="form-control" placeholder="จ่ายให้" required>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-md-4"></label>

                        <div class="col-md-6"><br />
                            <button type="submit" class="btn btn-success">
                                {{ __('บันทึก') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="tab-content" id="orders-table-tab-content">
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <div class="row">
                            <div class="col-md-3"><b>สินค้า</b></div>
                            <div class="col-md-3"><b>Pallet</b></div>
                            <div class="col-md-3"><b>วันที่</b></div>
                            <div class="col-md-3"><b>จำนวน</b></div>
                        </div>
                        {{-- @if ($fsHwLoc->fswhstocks()->count() > 0) --}}
                            {{-- @foreach ($fsHwLoc->fswhstocks()->get() as $item) --}}
                            {{-- @if ($item->id <> $data->id) --}}
                        @if(!empty($query))
                            @foreach($query as $item)
                                <div class="row">
                                    <div class="col-md-3"><a>{{ $item->product->name }}</a></div>
                                    <div class="col-md-3"><a>{{ $item->pallet }}</a></div>
                                    <div class="col-md-3"><a>{{ $item->to_date }}</a></div>
                                    <div class="col-md-3">
                                        <a>{{ $item->volumn }}</a>
                                        {{-- <a class="btn btn-secondary" href="{{ route('FsStocks.deleteDetailAction', $item->id) }}">X</a> --}}
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="row">
                                <div class="col-md-12"><b>ไม่มีข้อมูล</b></div>
                            </div>
                        @endif

                    </div>

                </div>


            </div>
        </div>
    </div>
@endsection
