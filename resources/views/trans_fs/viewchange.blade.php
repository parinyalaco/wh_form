@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="page-heading row">
                <div class="pull-right col-auto my-2">
                    <a class="btn btn-secondary" href="{{ route('FsWhs.index') }}">Back</a>
                </div>
                <div class="col">
                    <h1 class="row">ย้ายตำแหน่งจาก {{ $dataLoc->name }} FS Warehouse Layout # {{ $data->id }}</h1>
                </div>
            </div>
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>

            <div class="tab-content" id="orders-table-tab-content">
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <div class="row">
                            <div class="col-md-3"><b>Name : </b>{{ $data->name }}</div>
                            <div class="col-md-3"><b>Row : </b>{{ $data->row }}</div>
                            <div class="col-md-3"><b>Col : </b>{{ $data->col }}</div>
                            <div class="col-md-3"><b>Level : </b>{{ $data->level }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pull-right col-auto my-2">
            </div>
            <div class="tab-content" id="orders-table-tab-content">
                <div class="row">
                    <div class="col-md-6">
                    <div id="canvas-view"></div>
                    </div>
                    <div class="col-md-6">
                    <div id="dspbox"></div>
                    </div>
                </div>
            </div>
            <!--//tab-content-->
        </div>
        <!--//container-fluid-->
    </div>

    <!--//app-content-->
    <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
    <script src="https://unpkg.com/konva@8.3.12/konva.min.js"></script>
    <script>
        $(document).ready(function() {
            var width = {{ ($data->col * 60) + 22 }};
            var height = {{ $data->row * 75 }};

            var stage = new Konva.Stage({
                container: 'canvas-view',
                width: width,
                height: height,
            });

            var layer = new Konva.Layer();

            this_path = window.location.href
            exp_path = this_path.split("FsStocks")
            // console.log(exp_path);

            @for ($i = 1; $i <= $data->col; $i++)
                var coltxt{{$i}} = new Konva.Text({
                    x: {{ ($i * 53) + 16  }},
                    y: 10,
                    text: {{ $i }},
                    fontSize: 15,
                    fontFamily: 'Calibri',
                    fill: 'black',
                });
                layer.add(coltxt{{$i}});
            @endfor

            @for ($i = 1; $i <= $data->row; $i++)
                var rowtxt{{$i}} = new Konva.Text({
                    x: 22,
                    y: {{ ($i * 70)- 20  }} ,
                    text: String.fromCharCode(96 + {{ $i }}).toUpperCase(),
                    fontSize: 15,
                    fontFamily: 'Calibri',
                    fill: 'black',
                });
                layer.add(rowtxt{{$i}});
            @endfor

            var txt_sum = '<h3>ยอดรวมทั้งหมดใน {{ $data->name }} </h3>';
            @if (!empty($query))
                @foreach ($query as $stockdata)
                    txt_sum += '<div><a style="color:black">{{ $stockdata->name }} จำนวน {{ number_format($stockdata->volumn) }}</a></div>';
                @endforeach
                txt_sum += '<div><a style="color:red">__________________________________________________________</a></div>';
            @endif
            $('#dspbox').html(txt_sum);

            @foreach ($data->fswhlocs()->get() as $item)

                var group{{ $item->id }} = new Konva.Group({
                    x: {{ $item->col * 53 }},
                    y: {{ ($item->row * 70) + (($data->level - $item->level + 1) * 15) - 50 }} ,
                    width: 50,
                    height: 15,
                });

                group{{ $item->id }}.add(new Konva.Rect({
                    width: 50,
                    height: 15,
                    @if ($item->fsstocks()->count() > 0)
                    @if ($item->name == $dataLoc->name)
                        fill: 'orange',
                    @else
                        fill: 'yellow',
                    @endif
                    @else
                        fill: 'white',
                    @endif

                    stroke: 'black',
                    strokeWidth: 1,
                }));

                group{{ $item->id }}.add(new Konva.Text({
                    x: 7,
                    y: 2,
                    text: '{{ $item->name }}',
                    fontSize: 15,
                    fontFamily: 'Calibri',
                    fill: 'black',
                }));

                //Add Events
                group{{ $item->id }}.on('pointerclick', function () {
                    var txtdesc = '';
                    @if ( $item->fsstocks()->count() > 0)
                        @foreach ($item->fsstocks as $fsstock)
                                    txtdesc += '{{ $fsstock->product->name  }} ที่ Pallet : {{ $fsstock->pallet }} จำนวน {{ number_format($fsstock->volumn) }} (วันที่ {{ $fsstock->updated_at }}) <br/>';
                        @endforeach
                       // locDisplay('{{ $item->name }}',{{ $item->fsstocks()->count() }},txtdesc,{{ $item->id }},exp_path,txt_sum);
                    @else
                        locDisplay('{{ $item->name }}',0,txtdesc,{{ $item->id }},exp_path,txt_sum);
                    @endif
                });
                layer.add(group{{ $item->id }});
            @endforeach
            // add the layer to the stage
            stage.add(layer);
        });
        function locDisplay(titel,no,txtdesc,id,exp_path,txt_sum){
            if(no > 0){
                $('#dspbox').html(txt_sum+'<h3>ตำแหน่ง ' + titel + '</h3><div>'+txtdesc+'</div><h4><a href="'+exp_path[0]+'FsStocks/recvToStock/' + id + '" class="btn btn-secondary" >ยืนยันตำแหน่ง</a><a href="'+exp_path[0]+'FsStocks/deliverFromStock/' + id + '" class="btn btn-danger mx-2">จ่ายออก</a><a href="'+exp_path[0]+'FsStocks/changelocation/' + id + '/{{$data->id}}" class="btn btn-danger mx-2">สลับตำแหน่ง</a></h4>');
            }else{
                $('#dspbox').html(txt_sum+'<h3>ตำแหน่ง ' + titel + '</h3><div>ไม่มีสินค้าวาง</div><h4><a href="'+exp_path[0]+'FsStocks/changelocationconfirm/{{$dataLoc->id}}/{{$data->id}}/' + id + '" class="btn btn-secondary">ยืนยันตำแหน่ง</a></h4>');
            }
        }
    </script>
@endsection
