<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>รายงาน</title>
    <style>
        table {
            border: solid 2px black;
        }
        thead tr th{
            border: solid 1px black;
            font-weight: bold;
            font-style: italic;
        }
        tbody tr td{
            border: solid 1px black;
        }
    </style>
</head>
<body style="font-family: Cordia New; font-size:160%;">
    <p><strong>TO..ALL</strong></p>
    <p><strong>สรุปการรับวัตถุดิบกล้วยหอมดิบ วันที่ 11/1/2565 {{ $summarydata['date'] }}</strong></p>
    <p><strong>1.ประมาณการรับเข้าทั้งหมด {{ number_format($summarydata['limitsum'],0,'.',',') }} Kg.</strong></p>
    <p><strong>2.RM รับเข้าทั้งหมด {{ number_format($summarydata['realsum'],0,'.',',') }} Kg.</strong></p>
    <p><strong>3.รถขนส่งเข้าโรงงานทั้งหมด {{$summarydata['car']}} คัน</strong><br/>
        @if (!empty($summarydata['firstcar']))
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- คันแรกถึง LACO เวลา {{ date('H:i',strtotime($summarydata['firstcar'])) }} น.<br/>        
        @endif
        @if (!empty($summarydata['lastcar']))
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- คันสุดท้ายถึง LACO เวลา {{ date('H:i',strtotime($summarydata['lastcar'])) }} น.<br/>    
        @endif
        @if (!empty($summarydata['lastmove']))
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- ลงวัตถุดิบคันสุดท้ายเสร็จเวลา เวลา {{ date('H:i',strtotime($summarydata['lastmove'])) }} น.    
        @endif
    </p> 
    @if (!empty($summarydata['graph'][1]['url']))
    <p><img src="{{ url($summarydata['graph'][1]['url']) }}" alt="" srcset=""></p>    
    @endif
    @if (!empty($summarydata['graph'][2]['url']))
    <p><img src="{{ url($summarydata['graph'][2]['url']) }}" alt="" srcset=""></p>    
    @endif
    @if (!empty($summarydata['round']))
    <p><strong>ช่วงเวลารถเข้า</strong>
        <table>
            <thead>
                <tr>
                    <th>ช่วงเวลา</th>
                    <th>จำนวนรถ</th>
                    <th>จำนวนตะกร้า</th>
                    <th>จำนวนน้ำหนัก(Kg)</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($summarydata['round'] as $key=>$item)
                    <tr>
                        <td>{{$key}}:00</td>
                        <td>{{ $item['car_no'] }}</td>
                        <td>{{ number_format($item['sumpack'],0,'.',',') }}</td>
                        <td>{{ number_format($item['sumweight'],0,'.',',') }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </p>    
    @endif
    @if (!empty($summarydata['worker']))
    <p><strong>สรุปการลงวัตถุดิบ</strong>
        <table>
            <thead>
                <tr>
                    <th rowspan="3">วันที่</th>
                    <th colspan="{{ sizeof($params) }}">ประเภทวัตถุดิบ</th>
                </tr>
                <tr>
                    <th colspan="{{ sizeof($params) }}">พนักงานทั้งหมดเฉลี่ย {{  $summarydata['worker']/$summarydata['car'] }} คน</th>
                </tr>
                <tr>
                    @foreach ($params as $key=>$item)
                        <th>{{$item}}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $summarydata['date'] }}</td>
                    @foreach ($params as $key=>$item)
                        <td>{{$summarydata['move'][$key]}}</td>
                    @endforeach
                </tr>
            </tbody>
        </table>
    </p>    
    @endif
    @if (!empty($summarydata['graph'][3]['url']))
    <p><img src="{{ url($summarydata['graph'][3]['url']) }}" alt="" srcset=""></p><br>    
    @endif
    <p><strong>Laco Warehouse System</strong><br/></p>
</body>
</html>