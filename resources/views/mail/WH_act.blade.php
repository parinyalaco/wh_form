<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>รายงาน</title>
    <style>
        table {
            border: solid 2px black;
            border-collapse: collapse;
        }
        thead tr th{
            border: solid 1px black;
            border-collapse: collapse;
            font-weight: bold;
            /* font-style: italic; */
        }
        tbody tr td{
            border: solid 1px black;
            border-collapse: collapse;
        }
    </style>
</head>
<body style="font-family: Cordia New; font-size:160%;">
    <p><strong>TO..ALL</strong></p>
    <p><strong>รายงานสรุปงานประจำวัน แผนกคลังสินค้าแช่แข็ง วันที่ {{ $show_date }}</strong></p>
    <p><strong>1.งานจ่ายสินค้า</strong></p>
    @if(!empty($tb_1['จ่าย']))
        <table style="font-size: 12px; width: 100%">
            <thead>
                <tr style="background-color:#F4B084">
                    <th class="cell" rowspan="3">ลำดับที่</th>
                    <th class="cell" rowspan="3">แผนก</th>
                    <th class="cell" rowspan="3">รหัสสินค้า</th>
                    <th class="cell" colspan="3">เวลา (น.)</th>
                    <th class="cell" colspan="7">จำนวน (Kg.)</th>
                    <th class="cell" rowspan="3">หมายเหตุ</th>
                </tr>  
                <tr style="text-align: center; background-color:#F4B084;">
                    <th class="cell" rowspan="2">Plan</th>
                    <th class="cell" rowspan="2">Actual-B</th>
                    <th class="cell" rowspan="2">Actual-C</th>

                    <th class="cell" rowspan="2">Plan</th>
                    <th class="cell" colspan="2">Actual-B</th>
                    <th class="cell" colspan="2">Actual-C</th>
                    <th class="cell" colspan="2">รวม</th>
                </tr>
                <tr style="text-align: center; background-color:#F4B084;">
                    @for($i=0;$i<3;$i++)
                        <th class="cell">Q</th>
                        <th class="cell">P</th>
                    @endfor
                </tr>
            </thead>
            <tbody>
                @php
                    $sum_pd = array();
                    $sum_act = array();
                    $sum_pd_pl = array();
                    $sum_act_pl = array();
                    $c_show = array();
                    $i = 0;
                @endphp
                @foreach ($tb_1['จ่าย'] as $kst=>$vst)
                    @foreach ($vst as $kpd=>$vpd)
                        <tr>
                            <td class="cell">{{ ++$i }}</td>
                            @if(empty($c_show[$kst]))
                                <td class="cell" rowspan="{{ count($vst) }}" style="text-align: center;">{{ $kst }}</td>
                                @php
                                    $c_show[$kst] = 1;
                                @endphp
                            @endif
                            <td class="cell">{{ $kpd }}</td>
                            <td class="cell">@if(!empty($tb_1['จ่าย'][$kst][$kpd]['time']['plan'])){{ $tb_1['จ่าย'][$kst][$kpd]['time']['plan'] }}@endif</td>
                            <td class="cell" style="background-color:#FFD966">@if(!empty($tb_1['จ่าย'][$kst][$kpd]['time']['actual_b'])){{ $tb_1['จ่าย'][$kst][$kpd]['time']['actual_b'] }}@endif</td>
                            <td class="cell" style="background-color:#A9D08E">@if(!empty($tb_1['จ่าย'][$kst][$kpd]['time']['actual_c'])){{ $tb_1['จ่าย'][$kst][$kpd]['time']['actual_c'] }}@endif</td>
                            <td class="cell" style="text-align: right">
                                @if(!empty($tb_1['จ่าย'][$kst][$kpd]['qty']['plan']))
                                    {{ number_format($tb_1['จ่าย'][$kst][$kpd]['qty']['plan'],2) }}
                                    @php
                                        $to_save = $tb_1['จ่าย'][$kst][$kpd]['qty']['plan'];
                                        if(!empty($sum_act['plan']))   $sum_act['plan'] += $to_save;
                                        else    $sum_act['plan'] = $to_save;
                                    @endphp
                                @endif
                            </td>
                            <td class="cell" style="text-align: right; background-color:#FFD966">
                                @if(!empty($tb_1['จ่าย'][$kst][$kpd]['qty']['actual_b']))
                                    {{ number_format($tb_1['จ่าย'][$kst][$kpd]['qty']['actual_b'],2) }}
                                    @php
                                        $to_save = $tb_1['จ่าย'][$kst][$kpd]['qty']['actual_b'];
                                        if(!empty($sum_pd[$kpd]))   $sum_pd[$kpd] += $to_save;
                                        else    $sum_pd[$kpd] = $to_save;

                                        if(!empty($sum_act['actual_b']))   $sum_act['actual_b'] += $to_save;
                                        else    $sum_act['actual_b'] = $to_save;
                                    @endphp
                                @endif
                            </td>
                            <td class="cell" style="text-align: right; background-color:#FFD966">
                                @if(!empty($tb_1['จ่าย'][$kst][$kpd]['pl']['actual_b']))
                                    {{ number_format($tb_1['จ่าย'][$kst][$kpd]['pl']['actual_b'],0) }}
                                    @php
                                        $to_save = $tb_1['จ่าย'][$kst][$kpd]['pl']['actual_b'];
                                        if(!empty($sum_pd_pl[$kpd]))   $sum_pd_pl[$kpd] += $to_save;
                                        else    $sum_pd_pl[$kpd] = $to_save;
    
                                        if(!empty($sum_act_pl['actual_b']))   $sum_act_pl['actual_b'] += $to_save;
                                        else    $sum_act_pl['actual_b'] = $to_save;
                                    @endphp
                                @endif
                            </td>
                            <td class="cell" style="text-align: right; background-color:#A9D08E">
                                @if(!empty($tb_1['จ่าย'][$kst][$kpd]['qty']['actual_c']))
                                    {{ number_format($tb_1['จ่าย'][$kst][$kpd]['qty']['actual_c'],2) }}
                                    @php
                                        $to_save = $tb_1['จ่าย'][$kst][$kpd]['qty']['actual_c'];
                                        if(!empty($sum_pd[$kpd]))   $sum_pd[$kpd] += $to_save;
                                        else    $sum_pd[$kpd] = $to_save;

                                        if(!empty($sum_act['actual_c']))   $sum_act['actual_c'] += $to_save;
                                        else    $sum_act['actual_c'] = $to_save;
                                    @endphp
                                @endif
                            </td>
                            <td class="cell" style="text-align: right; background-color:#A9D08E">
                                @if(!empty($tb_1['จ่าย'][$kst][$kpd]['pl']['actual_c']))
                                    {{ number_format($tb_1['จ่าย'][$kst][$kpd]['pl']['actual_c'],0) }}
                                    @php
                                        $to_save = $tb_1['จ่าย'][$kst][$kpd]['pl']['actual_c'];
                                        if(!empty($sum_pd_pl[$kpd]))   $sum_pd_pl[$kpd] += $to_save;
                                        else    $sum_pd_pl[$kpd] = $to_save;
    
                                        if(!empty($sum_act_pl['actual_c']))   $sum_act_pl['actual_c'] += $to_save;
                                        else    $sum_act_pl['actual_c'] = $to_save;
                                    @endphp
                                @endif
                            </td>
                            <td class="cell" style="text-align: right">
                                @if(!empty($sum_pd[$kpd]))
                                    {{ number_format($sum_pd[$kpd],2) }}
                                    @php
                                        if(!empty($sum_act['sum']))   $sum_act['sum'] += $sum_pd[$kpd];
                                        else    $sum_act['sum'] = $sum_pd[$kpd];
                                    @endphp
                                @endif
                            </td> 
                            <td class="cell" style="text-align: right;">
                                @if(!empty($sum_pd_pl[$kpd]))
                                    {{ number_format($sum_pd_pl[$kpd],0) }}
                                    @php
                                        if(!empty($sum_act_pl['sum']))   $sum_act_pl['sum'] += $sum_pd_pl[$kpd];
                                        else    $sum_act_pl['sum'] = $sum_pd_pl[$kpd];
                                    @endphp
                                @endif
                            </td>
                            <td class="cell">
                                @if(!empty($tb_1['จ่าย'][$kst][$kpd]['note'])){{ $tb_1['จ่าย'][$kst][$kpd]['note'] }}@endif
                            </td>
                        </tr>
                    @endforeach
                @endforeach
                <tr>
                    <td class="cell" colspan="6" style="text-align: right">รวม</td>
                    <td class="cell" style="text-align: right">@if(!empty($sum_act['plan'])){{ number_format($sum_act['plan'],2) }}@endif</td>
                    <td class="cell" style="text-align: right">@if(!empty($sum_act['actual_b'])){{ number_format($sum_act['actual_b'],2) }}@endif</td>
                    <td class="cell" style="text-align: right">@if(!empty($sum_act_pl['actual_b'])){{ number_format($sum_act_pl['actual_b'],0) }}@endif</td>
                    <td class="cell" style="text-align: right">@if(!empty($sum_act['actual_c'])){{ number_format($sum_act['actual_c'],2) }}@endif</td>
                    <td class="cell" style="text-align: right">@if(!empty($sum_act_pl['actual_c'])){{ number_format($sum_act_pl['actual_c'],0) }}@endif</td>
                    <td class="cell" style="text-align: right">@if(!empty($sum_act['sum'])){{ number_format($sum_act['sum'],2) }}@endif</td>
                    <td class="cell" style="text-align: right">@if(!empty($sum_act_pl['sum'])){{ number_format($sum_act_pl['sum'],0) }}@endif</td>
                </tr>
            </tbody>
        </table>
    @else
        <h5 class="col">ไม่พบข้อมูลวันที่ {{ $show_date }}</h5>
    @endif
    <br>
    <p><strong>2.กราฟเปรียบเทียบปริมาณคาดการณ์กับการจ่ายสินค้าจริง</strong></p>
    @if(!empty($tb_1['จ่าย']))
        <table style="width: 99%">
            <tr>
                <td>
                    @if(!empty($graph['link']['PK']))<img src="{{ $message->embed($graph['path']['PK']) }}" width="90%" />@endif
                </td>
                <td>
                    @if(!empty($graph['link']['PF']))<img src="{{ $message->embed($graph['path']['PF']) }}" width="90%" />@endif
                </td>
            </tr>
        </table>
    <br>
    @else
        <h5 class="col">ไม่พบข้อมูลวันที่ {{ $show_date }}</h5>
    @endif
    <br>
    <p><strong>3.งานรับสินค้า</strong></p>
    @if(!empty($tb_1['รับ']))
        <table style="width: 50%">
            <thead>
                <tr style="text-align: center; background-color:#F4B084;">
                    <th class="cell" rowspan="2">ลำดับที่ </th>
                    <th class="cell" rowspan="2">แผนก</th>
                    <th class="cell" rowspan="2">รหัสสินค้า</th>
                    <th class="cell" colspan="2">B (Kg.)</th>
                    <th class="cell" colspan="2">C (Kg.)</th>
                    <th class="cell" colspan="2">รวม</th>
                </tr>
                <tr style="text-align: center; background-color:#F4B084;">
                    @for($i=0;$i<3;$i++)
                        <th class="cell">Q</th>
                        <th class="cell">P</th>
                    @endfor
                </tr>
            </thead>
            <tbody>
                @php
                    $sum_pd = array();
                    $sum_act = array();
                    $sum_pd_pl = array();
                    $sum_act_pl = array();
                    $c_show = array();
                    $i = 0;
                @endphp
                @foreach ($tb_1['รับ'] as $kst=>$vst)
                    @foreach ($vst as $kpd=>$vpd)
                        <tr>
                            <td class="cell">{{ ++$i }}</td>
                            @if(empty($c_show[$kst]))
                                <td class="cell" rowspan="{{ count($vst) }}" style="text-align: center;">{{ $kst }}</td>
                                @php
                                    $c_show[$kst] = 1;
                                @endphp
                            @endif
                            <td class="cell">{{ $kpd }}</td>
                            <td class="cell" style="text-align: right; background-color:#FFD966">
                                @if(!empty($tb_1['รับ'][$kst][$kpd]['qty']['actual_b']))
                                    {{ number_format($tb_1['รับ'][$kst][$kpd]['qty']['actual_b'],2) }}
                                    @php
                                        $to_save = $tb_1['รับ'][$kst][$kpd]['qty']['actual_b'];
                                        if(!empty($sum_pd[$kpd]))   $sum_pd[$kpd] += $to_save;
                                        else    $sum_pd[$kpd] = $to_save;

                                        if(!empty($sum_act['B']))   $sum_act['B'] += $to_save;
                                        else    $sum_act['B'] = $to_save;
                                    @endphp
                                @endif
                            </td>
                            <td class="cell" style="text-align: right; background-color:#FFD966">
                                @if(!empty($tb_1['รับ'][$kst][$kpd]['pl']['actual_b']))
                                    {{ number_format($tb_1['รับ'][$kst][$kpd]['pl']['actual_b'],0) }}
                                    @php
                                        $to_save = $tb_1['รับ'][$kst][$kpd]['pl']['actual_b'];
                                        if(!empty($sum_pd_pl[$kpd]))   $sum_pd_pl[$kpd] += $to_save;
                                        else    $sum_pd_pl[$kpd] = $to_save;
    
                                        if(!empty($sum_act_pl['B']))   $sum_act_pl['B'] += $to_save;
                                        else    $sum_act_pl['B'] = $to_save;
                                    @endphp
                                @endif
                            </td>
                            <td class="cell" style="text-align: right; background-color:#A9D08E">
                                @if(!empty($tb_1['รับ'][$kst][$kpd]['qty']['actual_c']))
                                    {{ number_format($tb_1['รับ'][$kst][$kpd]['qty']['actual_c'],2) }}
                                    @php
                                        $to_save = $tb_1['รับ'][$kst][$kpd]['qty']['actual_c'];
                                        if(!empty($sum_pd[$kpd]))   $sum_pd[$kpd] += $to_save;
                                        else    $sum_pd[$kpd] = $to_save;

                                        if(!empty($sum_act['C']))   $sum_act['C'] += $to_save;
                                        else    $sum_act['C'] = $to_save;
                                    @endphp
                                @endif
                            </td>  
                            <td class="cell" style="text-align: right; background-color:#A9D08E">
                                @if(!empty($tb_1['รับ'][$kst][$kpd]['pl']['actual_c']))
                                    {{ number_format($tb_1['รับ'][$kst][$kpd]['pl']['actual_c'],0) }}
                                    @php
                                        $to_save = $tb_1['รับ'][$kst][$kpd]['pl']['actual_c'];
                                        if(!empty($sum_pd_pl[$kpd]))   $sum_pd_pl[$kpd] += $to_save;
                                        else    $sum_pd_pl[$kpd] = $to_save;
    
                                        if(!empty($sum_act_pl['C']))   $sum_act_pl['C'] += $to_save;
                                        else    $sum_act_pl['C'] = $to_save;
                                    @endphp
                                @endif
                            </td>
                            <td class="cell" style="text-align: right">
                                @if(!empty($sum_pd[$kpd]))
                                    {{ number_format($sum_pd[$kpd],2) }}
                                    @php
                                        if(!empty($sum_act['sum']))   $sum_act['sum'] += $sum_pd[$kpd];
                                        else    $sum_act['sum'] = $sum_pd[$kpd];
                                    @endphp
                                @endif
                            </td> 
                            <td class="cell" style="text-align: right">
                                @if(!empty($sum_pd_pl[$kpd]))
                                    {{ number_format($sum_pd_pl[$kpd],0) }}
                                    @php
                                        if(!empty($sum_act_pl['sum']))   $sum_act_pl['sum'] += $sum_pd_pl[$kpd];
                                        else    $sum_act_pl['sum'] = $sum_pd_pl[$kpd];
                                    @endphp
                                @endif
                            </td>                     
                        </tr>
                    @endforeach
                @endforeach
                <tr>
                    <td class="cell" colspan="3" style="text-align: right">รวม</td>
                    <td class="cell" style="text-align: right">@if(!empty($sum_act['B'])){{ number_format($sum_act['B'],2) }}@endif</td>
                    <td class="cell" style="text-align: right">@if(!empty($sum_act_pl['B'])){{ number_format($sum_act_pl['B'],0) }}@endif</td>
                    <td class="cell" style="text-align: right">@if(!empty($sum_act['C'])){{ number_format($sum_act['C'],2) }}@endif</td>
                    <td class="cell" style="text-align: right">@if(!empty($sum_act_pl['C'])){{ number_format($sum_act_pl['C'],0) }}@endif</td>
                    <td class="cell" style="text-align: right">@if(!empty($sum_act['sum'])){{ number_format($sum_act['sum'],2) }}@endif</td>
                    <td class="cell" style="text-align: right">@if(!empty($sum_act_pl['sum'])){{ number_format($sum_act_pl['sum'],0) }}@endif</td>
                </tr>
            </tbody>
        </table>
    @else
        <h5 class="col">ไม่พบข้อมูลวันที่ {{ $show_date }}</h5>
    @endif
    <br>  
    <p>Laco Warehouse System<br/></p>
    
</body>
</html>
