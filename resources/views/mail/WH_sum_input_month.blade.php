<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>รายงาน</title>
    <style>
        table {
            border: solid 2px black;
        }
        thead tr th{
            border: solid 1px black;
            font-weight: bold;
            font-style: italic;
        }
        tbody tr td{
            border: solid 1px black;
        }
    </style>
</head>
<body style="font-family: Cordia New; font-size:160%;">
    <p><strong>TO..ALL</strong></p>
    <p><strong>รายงานรับเข้าสินค้าประจำเดือน {{ $show_month }}</strong></p>
    <p><strong>1.สรุปสินค้ารับเข้าสิ้นเดือน</strong></p>
    @if(count($tbl_add)>0)
        <table style="width: 80%; text-align:center; font-size:18px;">
            <thead>
                <tr style="background-color:orange;">
                    <th class="cell" style="width: 25%;">รับเข้า</th>
                    <th class="cell">สินค้าทั้งหมด</th>
                    <th class="cell">รับจริง</th>
                    <th class="cell">ตรงแผน</th>
                    <th class="cell">ไม่ตรงตรงแผน</th>
                </tr>
            </thead>    
            <tbody> 
                @php
                    $arr_sum = array('all', 'real', 'plan');
                    foreach ($arr_sum as $key => $value) {
                        $to_sum[$value] = 0;
                    }                        
                @endphp
                @foreach($st as $kst => $vst)
                    <tr>
                        <td class="cell">{{ $vst }}</td>
                        <td class="cell">{{ $tbl_add[$kst]['all'] }}</td>
                        <td class="cell">{{ $tbl_add[$kst]['real'] }}</td>
                        <td class="cell">{{ $tbl_add[$kst]['plan'] }}</td>     
                        <td class="cell">{{ $tbl_add[$kst]['all']-$tbl_add[$kst]['plan'] }}</td>           
                    </tr>
                    @php
                        foreach ($arr_sum as $key => $value) {
                            $to_sum[$value] += $tbl_add[$kst][$value];
                        }                         
                    @endphp
                @endforeach  
                <tr style="background-color:orange">
                    <th class="cell">TOTAL</th>
                    @foreach($arr_sum as $key => $value)
                        <th class="cell">{{ $to_sum[$value] }}</th>
                    @endforeach    
                    <td class="cell">{{ $to_sum['all']-$to_sum['plan'] }}</td>            
                </tr>
            </tbody>
        </table>    
    @endif
    <br>
    <p><strong>2.กราฟสรุปสินค้ารับเข้าสิ้นเดือน</strong></p>
    {{-- <img src="{{ url($gp['link']) }}" alt="">  --}}
    {{-- @if(count($gp['pie'])>0)<img src="{{ $message->embed($gp['pie']['path']) }}" />@endif --}}
    <table>
        <tbody>
            <tr>
                @foreach ($st as $kst=>$vst)
                    <td style="text-align: center">รับเข้า {{ $vst }}</td>
                @endforeach 
            </tr>        
            <tr>
                @foreach ($st as $kst=>$vst)
                    <td style="text-align: center">
                        @if(!empty($gp[$kst]['pie']))<img src="{{ $message->embed($gp[$kst]['pie']['path']) }}" />@endif
                        {{-- @if(!empty($gp[$kst]['pie']))<img src="{{ $gp[$kst]['pie']['path'] }}" />@endif --}}
                    </td>
                @endforeach 
            </tr>
        </tbody>
    </table>
    <br>
    <p><strong>3.รายงานรับเข้าสินค้า LACO แยกตามประเภทสินค้า</strong></p>
    @if(count($tbl_1)>0)
        <table>
            <thead>
                <tr style="background-color: yellow">
                    <th class="cell">Row Labels</th>
                    <th class="cell">Sum of รายการ</th>
                    <th class="cell">Sum of ผลสุ่ม</th>
                </tr>
            </thead>    
            <tbody>            
                @foreach ($tbl_1['name'] as $key => $value) 
                    <tr>
                        <td class="cell">{{ $value }}</td>
                        <td class="cell" style="text-align: right">{{ $tbl_1['list'][$key] }}</td>
                        <td class="cell" style="text-align: right">{{ $tbl_1['random'][$key] }}</td>            
                    </tr>
                @endforeach
                {{-- <tr>
                    <td class="cell">total</td>
                    <td class="cell" style="text-align: right">@if(count($tbl_1['list'])>0){{ array_sum($tbl_1['list']) }}@endif</td>
                    <td class="cell" style="text-align: right">@if(count($tbl_1['random'])>0){{ array_sum($tbl_1['random']) }}@endif</td>            
                </tr> --}}
                <tr style="background-color: yellow">
                    <th class="cell">Grand Total</th>
                    <th class="cell" style="text-align: right">@if(count($tbl_1['list'])>0){{ array_sum($tbl_1['list']) }}@endif</th>
                    <th class="cell" style="text-align: right">@if(count($tbl_1['random'])>0){{ array_sum($tbl_1['random']) }}@endif</th>            
                </tr>
            </tbody>
        </table>    
    @endif
    <br>
    <p><strong>4.กราฟแสดงรายงานรับเข้าสินค้า แยกตามประเภทสินค้า</strong></p>
    {{-- <img src="{{ url($gp['link']) }}" alt="">  --}}
    @if(!empty($gp['laco']['bar']))<img src="{{ $message->embed($gp['laco']['bar']['path']) }}" />@endif
    {{-- @if(!empty($gp['laco']['bar']))<img src="{{ $gp['laco']['bar']['path'] }}" />@endif --}}
    <br>
    <p><strong>5.รายงานสรุปการสุ่ม</strong></p>
    @if(count($tbl_2)>0)
        <table>
            <thead>
                <tr style="background-color: yellow">
                    <th class="cell">ทั้งหมดรับเข้า</th>
                    <th class="cell">สุ่มผ่าน</th>
                    <th class="cell">ค้างสุ่ม</th>
                    <th class="cell">ค้างสุ่ม</th>
                    <th class="cell" rowspan="2">Remark</th>
                </tr>
                <tr style="background-color: yellow">
                    <th class="cell">(รายการ)</th>
                    <th class="cell">(รายการ)</th>
                    <th class="cell">(รายการ)</th>
                    <th class="cell">(เปอร์เซนต์ %)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="cell" @if(!empty($tbl_2['pd'])) rowspan="{{ count($tbl_2['pd']) }}" @endif
                    style="text-align: right">@if(!empty($tbl_2['all'][0])){{ $tbl_2['all'][0] }}@endif</td>
                    <td class="cell" @if(!empty($tbl_2['pd'])) rowspan="{{ count($tbl_2['pd']) }}" @endif
                    style="text-align: right">@if(!empty($tbl_2['rd_pass'][0])){{ $tbl_2['rd_pass'][0] }}@endif</td>
                    @php
                        if(!empty($tbl_2['all'][0])){
                            $diff_rd = $tbl_2['all'][0]-$tbl_2['rd_pass'][0];
                            if($diff_rd>0){
                                $p_rd = $diff_rd/$tbl_2['all'][0]*100;
                            }else{
                                $p_rd = 0;
                            }
                        }
                    @endphp
                    <td class="cell" @if(!empty($tbl_2['pd'])) rowspan="{{ count($tbl_2['pd']) }}" @endif
                    style="text-align: right">@if(!empty($diff_rd)){{ $diff_rd }}@endif</td>
                    <td class="cell" @if(!empty($tbl_2['pd'])) rowspan="{{ count($tbl_2['pd']) }}" @endif
                    style="text-align: right">@if(!empty($p_rd)){{ number_format($p_rd,2) }}@endif</td>
                    @php
                        $i=0;
                    @endphp
                    @if(count($tbl_2['pd'])>0)
                        @foreach ($tbl_2['pd'] as $key=>$value) 
                            @if($i>0) <tr> @endif
                            <td class="cell">{{ $mat[$value]['po_shot'] }}</td>
                            </tr>
                            @php
                                $i++;
                            @endphp
                        @endforeach
                    @endif
            </tbody>
        </table>
    @endif
    <br>
    <p>Laco Warehouse System<br/></p>
    
</body>
</html>
