<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>รายงาน</title>
    <style>
        table {
            border: solid 2px black;
        }
        thead tr th{
            border: solid 1px black;
            font-weight: bold;
            font-style: italic;
        }
        tbody tr td{
            border: solid 1px black;
        }
    </style>
</head>
<body style="font-family: Cordia New; font-size:160%;">
    @php
        if($type_tbl=='laco')
            $type = 'LACO';
        elseif($type_tbl=='en')
            $type = 'อะไหล่ช่าง';
        else
            $type = "DC วังน้อย";
    @endphp
    <p><strong>TO..ALL</strong></p>
    <p><strong>รายงานการรับเข้า({{ $type }}) ระหว่างวันที่ {{ $st_show }} ถึงวันที่ {{ $ed_show }}</strong></p>
    <p><strong>1.รายงานสรุปรับเข้า</strong></p>
    <table>
        <thead>
            <tr>
                <th class="cell" rowspan="2">วันที่</th>
                <th class="cell">รับทั้งหมด</th>
                <th class="cell" colspan="2">รับจริง</th>
                <th class="cell" colspan="2">% (เปอร์เซ็นต์)</th>
            </tr>
            <tr>
                <th class="cell">จำนวน</th>
                @for($i=0;$i<2;$i++)
                    <th class="cell">ตรงแผน</th>
                    <th class="cell">ไม่ตรงแผน</th>
                @endfor
            </tr>
        </thead>
        <tbody>
            @foreach ($data_show as $key=>$value)
                <tr>
                    <td class="cell">{{ $key }}</td>
                    <td class="cell">{{ $data_show[$key]['all'] }}</td>
                    <td class="cell">{{ $data_show[$key]['in_plan'] }}</td>
                    <td class="cell">@if(!empty($data_show[$key]['out_plan'])){{ $data_show[$key]['out_plan'] }}@endif</td>
                    <td class="cell">{{ number_format($data_show[$key]['in_plan']/$data_show[$key]['all']*100,2) }}</td>
                    <td class="cell">@if(!empty($data_show[$key]['out_plan'])){{ number_format($data_show[$key]['out_plan']/$data_show[$key]['all']*100,2) }}@endif</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <p><strong>2.รายงานสินค้าไม่ตรงตามแผน</strong></p>
    <table>
        <thead>
            <tr>
                <th class="cell" rowspan="2">วันที่</th>
                <th class="cell">แผนรับเข้า</th>
                <th class="cell">รับจริง</th>
                <th class="cell" rowspan="2">ไม่ตรงตามแผน</th>
                <th class="cell" rowspan="2">หมายเหตุ</th>
            </tr>
            <tr>
                <th class="cell">(รายการ)</th>
                <th class="cell">(รายการ)</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data_show as $key=>$value)
                <tr>
                    @if(!empty($show_mat[2][$key]))
                        <td class="cell" rowspan="{{ count($show_mat[2][$key]) }}">{{ $key }}</td>
                        <td class="cell" rowspan="{{ count($show_mat[2][$key]) }}">{{ $data_show[$key]['in_po'] }}</td>
                        <td class="cell" rowspan="{{ count($show_mat[2][$key]) }}">{{ $data_show[$key]['in_real'] }}</td>
                        @php $i=0; @endphp
                        @foreach ($show_mat[2][$key] as $kmat=>$vmat)
                            @if($i<>0)<tr>@endif
                            <td class="cell">{{ $mat[$kmat] }}</td>
                            <td class="cell">{{ $vmat }}</td>
                            @if($i<(count($show_mat[2][$key])-1))</tr>@endif
                            @php
                                $i++;
                            @endphp
                        @endforeach
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
    <br>    
    <p><strong>3.กราฟแสดงรายการรับ</strong></p>
    {{-- <img src="{{ $chart_3 }}"> --}}
    {{-- <img src="{{ url($chart_name['pie']['link']) }}" alt="">     --}}
    <img src="{{ $message->embed($chart_name['pie']['path']) }}" />
    <br>
    @if($type_tbl=='laco')
        <p><strong>4.รายงานสรุปการสุ่ม</strong></p>
        <table>
            <thead>
                <tr>
                    <th class="cell" rowspan="2">วันที่</th>
                    <th class="cell">ทั้งหมดรับเข้า</th>
                    <th class="cell">สุ่มผ่าน</th>
                    <th class="cell">ค้างสุ่ม</th>
                    <th class="cell" rowspan="2">% ค้างสุ่ม</th>
                    <th class="cell" rowspan="2">Remark</th>
                </tr>
                <tr>
                    <th class="cell">(รายการ)</th>
                    <th class="cell">(รายการ)</th>
                    <th class="cell">(รายการ)</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data_show4 as $key=>$value)
                    <tr>
                        <td class="cell" @if(!empty($show_mat[4][$key])) rowspan="{{ count($show_mat[4][$key]) }}" @endif>
                            {{ $key }}</td>
                        <td class="cell" @if(!empty($show_mat[4][$key])) rowspan="{{ count($show_mat[4][$key]) }}" @endif>
                            {{ $data_show4[$key]['all'] }}</td>
                        <td class="cell" @if(!empty($show_mat[4][$key])) rowspan="{{ count($show_mat[4][$key]) }}" @endif>
                            @if(!empty($data_show4[$key]['in_pass'])){{ $data_show4[$key]['in_pass'] }} @endif
                        </td>
                        <td class="cell" @if(!empty($show_mat[4][$key])) rowspan="{{ count($show_mat[4][$key]) }}" @endif>
                            @if(!empty($data_show4[$key]['not_pass'])){{ $data_show4[$key]['not_pass'] }} @else 0 @endif
                        </td>
                        <td class="cell" @if(!empty($show_mat[4][$key])) rowspan="{{ count($show_mat[4][$key]) }}" @endif>
                            @if(!empty($data_show4[$key]['not_pass']))
                                {{ number_format($data_show4[$key]['not_pass']/$data_show4[$key]['all']*100,2) }}
                            @endif
                        </td>
                        @php $i=0; @endphp
                        @if(!empty($show_mat[4][$key]))
                            @foreach ($show_mat[4][$key] as $kmat=>$vmat)
                                @if($i<>0)<tr>@endif
                                <td class="cell">{{ $vmat }}</td>
                                @if($i<(count($show_mat[4][$key])-1))</tr>@endif
                                @php
                                    $i++;
                                @endphp
                            @endforeach
                        @else
                            <td class="cell"></td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
        <br>
        <p><strong>5.กราฟแสดงข้อมูลตามประเภทสินค้า</strong></p>
        {{-- <img src="{{ $chart_5 }}"> --}}
        {{-- <img src="{{ url($chart_name['bar']['link']) }}" alt=""> --}}
        <img src="{{ $message->embed($chart_name['bar']['path']) }}" />
        <br>
    @endif
    <p>Laco Warehouse System<br/></p>
    
</body>
</html>
