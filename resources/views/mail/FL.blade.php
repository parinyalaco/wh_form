<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>รายงาน</title>
    <style>
        /* table {
            border: solid 2px black;
        }
        thead tr th{
            border: solid 1px black;
            font-weight: bold;
            font-style: italic;
        }
        tbody tr td{
            border: solid 1px black;
        } */
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
    
</head>
<body style="font-family: Cordia New; font-size:160%;">
    <p><strong>TO..ALL</strong></p>
    <p><strong>รายงานยอดชั่วโมงการใช้งานรถโฟร์คลิฟท์ระหว่างวันที่ {{ date ("d/m/Y", strtotime($st_date)) }} ถึงวันที่ {{ date ("d/m/Y", strtotime($ed_date)) }}</strong></p>
    <p><strong>1.ตารางแสดงชั่วโมงการใช้งานรถโฟร์คลิฟท์(รถเช่า) - (หน่วย : ชั่วโมง)</strong></p>
    <div class="table-responsive">      
        <table style="width: 70%">
            <thead>
                <tr>
                    <th class="cell" rowspan="2" style='font-size:80%'>พื้นที่ใช้งาน</th>
                    <th class="cell" rowspan="2" style='font-size:80%'>หมายเลขรถโฟร์คลิฟท์</th>
                    @php
                        // ต้องประกาศก่อนการใช้งานทุกครั้ง ประกาศครั้งเดียวไม่ได้
                        $begin = new DateTime($st_date);
                        $end   = new DateTime($ed_date);
                        $shift = array('B','C');
                    @endphp
                    @for($i = $begin; $i <= $end; $i->modify('+1 day'))
                        <th class="cell" colspan="2" style="background-color:#E3E3DB">{{ $i->format("d/m") }}</th>
                    @endfor
                    {{-- <th class="cell" rowspan="2" style="background-color:#E3E3DB">AVG กะ B</th>
                    <th class="cell" rowspan="2" style="background-color:#E3E3DB">AVG กะ C</th> --}}
                </tr>
                <tr>
                    @for($i = 0; $i <= $days; $i++)
                        <th class="cell" style="background-color:#E3E3DB">B</th>
                        <th class="cell" style="background-color:#E3E3DB">C</th>
                    @endfor
                </tr>                                    
            </thead>
            <tbody>
                @foreach ($fl_log_locate as $kloc=>$vloc)
                    <tr>
                        <td class="cell" rowspan="{{  count($fl_log_locate[$kloc]) }}">{{ $locate_data[$kloc] }}</td>
                        @php
                            $loop_loc = 0;
                        @endphp
                        @foreach ($vloc as $kfl=>$vfl)
                            @if($loop_loc>0)
                                <tr>
                            @endif                                                
                            {{-- <td class="cell">@if(!empty($fl_data[$kfl])){{ $fl_data[$kfl] }}@endif</td> --}}
                            <td class="cell">{{ $kfl }}</td>
                            @php
                                $begin = new DateTime($st_date);
                                $end   = new DateTime($ed_date);
                            @endphp
                            @for($i = $begin; $i <= $end; $i->modify('+1 day'))
                                @foreach($shift as $key => $value)
                                    @if(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['S'])) 
                                        <td style="color: white; background-color: black;">
                                            @if($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['S'] <> 'S')
                                                {{ $fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['S'] }}
                                            @endif
                                        </td>
                                    @elseif(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['A']))
                                        <td style="background-color: red;">
                                            @if($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['A'] <> 'A')
                                                {{ $fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['A'] }}
                                            @endif
                                        </td>                                                          
                                    @else
                                        <td style="background-color: white;">
                                            @if(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['R']))
                                                {{ $fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['R'] }}
                                            @endif
                                        </td>
                                    @endif
                                @endforeach
                                {{-- <td style="text-align: center; background-color:@if(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['B']['S'])) black @elseif(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['B']['A'])) red @else white @endif;" >
                                    @if(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['B']['R']))
                                        {{ $fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['B']['R'] }}
                                    @endif
                                </td>
                                <td style="text-align:center; background-color:@if(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['C']['S'])) black @elseif(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['C']['A'])) red @else white @endif" >
                                    @if(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['C']['R']))
                                        {{ $fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['C']['R'] }}
                                    @endif
                                </td> --}}
                            @endfor
                            @php
                                $loop_loc++;
                                if(!empty($total_val['sum'][$kloc][$kfl]['B']))    $avg_val['B'] = $total_val['sum'][$kloc][$kfl]['B']/$total_val['count'][$kloc][$kfl]['B'];
                                else    $avg_val['B'] = 0;
                                if(!empty($total_val['sum'][$kloc][$kfl]['C']))    $avg_val['C'] = $total_val['sum'][$kloc][$kfl]['C']/$total_val['count'][$kloc][$kfl]['C'];
                                else    $avg_val['C'] = 0;
                            @endphp
                            {{-- <td style="text-align: right; background-color:#92D050">@if(!empty($avg_val['B'])){{ number_format($avg_val['B'],1) }}@endif</td>
                            <td style="text-align: right; background-color:#92D050">@if(!empty($avg_val['C'])){{ number_format($avg_val['C'],1) }}@endif</td> --}}
                            </tr>
                        @endforeach                                        
                @endforeach               
            </tbody>
        </table>
        <br>
        <table style="width: 70%">
            <thead>
                <tr>
                    <th class="cell" colspan="12" style='font-size:80%'>แสดงชั่วโมงการใช้แบบเต็มจำนวนชั่วโมง</th>                    
                </tr>
                <tr>
                    <th class="cell" rowspan="2" style='font-size:80%'>พื้นที่ใช้งาน</th>
                    <th class="cell" rowspan="2" style='font-size:80%'>หมายเลขรถโฟร์คลิฟท์</th>
                    @php
                        // ต้องประกาศก่อนการใช้งานทุกครั้ง ประกาศครั้งเดียวไม่ได้
                        $begin = new DateTime($st_date);
                        $end   = new DateTime($ed_date);
                        $shift = array('B','C');
                    @endphp
                    @for($i = $begin; $i <= $end; $i->modify('+1 day'))
                        <th class="cell" colspan="2" style="background-color:#E3E3DB">{{ $i->format("d/m") }}</th>
                    @endfor
                </tr>
                <tr>
                    @for($i = 0; $i <= $days; $i++)
                        <th class="cell" style="background-color:#E3E3DB">B</th>
                        <th class="cell" style="background-color:#E3E3DB">C</th>
                    @endfor
                </tr>                                    
            </thead>
            <tbody>
                @foreach ($fl_mile as $kloc=>$vloc)
                    <tr>
                        <td class="cell" rowspan="{{ count($fl_mile[$kloc]) }}">{{ $locate_data[$kloc] }}</td>
                        @php
                            $loop_loc = 0;
                        @endphp
                        @foreach ($vloc as $kfl=>$vfl)
                            @if($loop_loc>0)
                                <tr>
                            @endif                                                
                            <td class="cell">{{ $kfl }}</td>
                            @php
                                $begin = new DateTime($st_date);
                                $end   = new DateTime($ed_date);
                            @endphp
                            @for($i = $begin; $i <= $end; $i->modify('+1 day'))
                                @foreach($shift as $key => $value)
                                    @if(!empty($fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['S'])) 
                                        <td style="color: white; background-color: black;">
                                            @if($fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['S'] <> 'S')
                                                {{ $fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['S'] }}
                                            @endif
                                        </td>
                                    @elseif(!empty($fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['A']))
                                        <td style="background-color: red;">
                                            @if($fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['A'] <> 'A')
                                                {{ $fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['A'] }}
                                            @endif
                                        </td>                                                          
                                    @else
                                        <td style="background-color: white;">
                                            @if(!empty($fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['R']))
                                                {{ $fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['R'] }}
                                            @endif
                                        </td>
                                    @endif
                                @endforeach
                            @endfor
                            </tr>
                        @endforeach                                        
                @endforeach
            </tbody>
        </table>
        <br>
        <table style="width: 60%">
            <tr>
                <td style="width: 10%"><u>หมายเหตุ</u></td>
                <td style="background-color: white; width: 10%"></td>
                <td style="width: 20%">ใช้งาน</td>
            {{-- </tr>
            <tr> --}}
                <td style="background-color: black; width: 10%"></td>
                <td style="width: 20%">จอด</td>
            {{-- </tr>
            <tr> --}}
                <td style="background-color: red; width: 10%"></td>
                <td style="width: 20%">เสียต้องจอด</td>
            </tr>
        </table>
    </div><!--//table-responsive-->
    <br>
    <p><strong>2.กราฟแสดงสัดส่วนการใช้รถประจำวันที่ {{ date ("d/m/Y", strtotime($ed_date)) }}</strong></p>
    {{-- @if(!empty($gp['gp1']['link'][0]))<img src="{{ url($gp['gp1']['path'][0]) }}" alt="">@endif --}}
    @if(!empty($gp['gp1']['link'][0]))<img src="{{ $message->embed($gp['gp1']['path'][0]) }}" />@endif
    <br>
    <p><strong>3.กราฟแสดงชั่วโมงการใช้รถประจำวัน {{ date ("d/m/Y", strtotime($ed_date)) }}</strong></p>
    {{-- <img src="{{ url($gp['gp2']['link'][0]) }}" alt=""> --}}
    <img src="{{ $message->embed($gp['gp2']['path'][0]) }}" />
    <br>
    <p><strong>4.กราฟแสดงชั่วโมงการใช้รถโดยเฉลี่ยในเดือน {{ date ("m/Y", strtotime($ed_date)) }}</strong></p>
    {{-- <img src="{{ url($gp['gp3']['link'][0]) }}" alt=""> --}}
    <img src="{{ $message->embed($gp['gp3']['path'][0]) }}" />
    <table style="width: 100%">
        <tbody>
            <tr>
                <th class="cell">พื้นที่ใช้งาน</th>
                @foreach ($fl_log_locate as $kloc=>$vloc)                
                    <td class="cell" colspan="{{  count($fl_log_locate[$kloc]) }}" style="text-align: center">{{ $locate_data[$kloc] }}</td>  
                @endforeach
            </tr> 
            <tr>
                <th class="cell">หมายเลขรถโฟร์คลิฟท์</th>           
                @foreach ($fl_log_locate as $kloc=>$vloc)
                    @foreach ($vloc as $kfl=>$vfl)                                                                                
                        {{-- <td class="cell">@if(!empty($fl_data[$kfl])){{ $fl_data[$kfl] }}@endif</td>   --}}
                        <td class="cell">{{ $kfl }}</td> 
                    @endforeach
                @endforeach
            </tr> 
            <tr>
                <th class="cell" style="background-color:#E3E3DB">AVG กะ B</th>
                @foreach ($fl_log_locate as $kloc=>$vloc)
                    @foreach ($vloc as $kfl=>$vfl)                      
                        @php
                            // if(!empty($month_total['sum'][$kloc][$kfl]['B']))    $avg_val['B'] = $month_total['sum'][$kloc][$kfl]['B']/$month_total['count'][$kloc][$kfl]['B'];
                            // else    $avg_val['B'] = 0;
                        @endphp
                        {{-- <td style="text-align: right;">@if(!empty($avg_val['B'])){{ number_format($avg_val['B'],1) }}@endif</td>            --}}
                        <td style="text-align: right;">@if(!empty($month_average['B'][$kfl])){{ number_format($month_average['B'][$kfl],1) }}@endif</td>            
                    @endforeach                                        
                @endforeach
            </tr>
            <tr>                
                <th class="cell" style="background-color:#E3E3DB">AVG กะ C</th>
                @foreach ($fl_log_locate as $kloc=>$vloc)
                    @foreach ($vloc as $kfl=>$vfl)                      
                        @php
                            // if(!empty($month_total['sum'][$kloc][$kfl]['C']))    $avg_val['C'] = $month_total['sum'][$kloc][$kfl]['C']/$month_total['count'][$kloc][$kfl]['C'];
                            // else    $avg_val['C'] = 0;
                        @endphp
                        {{-- <td style="text-align: right;">@if(!empty($avg_val['C'])){{ number_format($avg_val['C'],1) }}@endif</td>    --}}
                        <td style="text-align: right;">@if(!empty($month_average['C'][$kfl])){{ number_format($month_average['C'][$kfl],1) }}@endif</td>                     
                    @endforeach                                        
                @endforeach
            </tr>
        </tbody>
    </table>
    <br>
    <p><strong>5.กราฟแสดงชั่วโมงการใช้รถแยกคันในเดือน {{ date ("m/Y", strtotime($ed_date)) }}</strong></p>
    @foreach ($gp['gp4']['link'] as $key=>$value)
        {{-- <img src="{{ url($gp['gp4']['link'][$key]) }}" alt=""> --}}
        <img src="{{ $message->embed($gp['gp4']['path'][$key]) }}" />
        <br>
    @endforeach
    
    <br>
    <p>Laco Warehouse System<br/></p>
    
</body>

</html>
