<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>รายงาน</title>
    <style>
        table {
            border: solid 2px black;
            border-collapse: collapse;
        }
        thead tr th{
            border: solid 1px black;
            border-collapse: collapse;
            font-weight: bold;
            /* font-style: italic; */
        }
        tbody tr td{
            border: solid 1px black;
            border-collapse: collapse;
        }
        tbody tr th{
            border: solid 1px black;
            border-collapse: collapse;
            font-weight: bold;
        }
    </style>
</head>
<body style="font-family: Cordia New; font-size:160%;">
    <p><strong>TO..ALL</strong></p>
    <p><strong>สรุปการลงวัตถุดิบถั่วแระ {{ $crop }} รับเข้าวันที่ {{ $sent_date }}</strong></p>
    <p>1. RM รับเข้าทั้งหมด {{ number_format($to_report['weight'][1],2) }} Kg.</p>
    <p>2. รถขนส่งเข้าโรงงานทั้งหมด {{ count($to_report['car']) }} คัน</p>
    <p style="text-indent: 2.5em;"> - คันแรกถึง LACO เวลา {{ date('H:i:s',strtotime($to_report['time']['min'])) }} น.</p>
    <p style="text-indent: 2.5em;"> - คันสุดท้ายถึง LACO เวลา {{ date('H:i:s',strtotime($to_report['time']['max'])) }} น.</p>
    <p style="text-indent: 2.5em;"> - ลงวัตถุดิบคันสุดท้ายเสร็จเวลา {{ date('H:i:s',strtotime($to_report['time']['end_work'])) }} น.</p>
    
    <p style="text-indent: 2.5em;"> - ช่วงเวลารถเข้า</p>
    
    @if(!empty($to_report['range']))    
        <table style="width: 30%">
            <thead>
                <tr style="text-align: center; background-color:#b1d3f5">
                    <th class="cell">เวลา</th>
                    <th class="cell">วันที่ {{ $sent_date }}</th>
                    <th class="cell">น้ำหนักวัตถุดิบสุทธิ</th>
                </tr>
            </thead>
            <tbody>           
                @if(!empty($to_report['range']))
                    @foreach ($to_report['range'] as $ktime=>$vtime)
                        <tr>
                            <td class="cell">{{ $ktime }}</td>
                            <td class="cell" style="text-align: right;">{{ $vtime }}</td>
                            <td class="cell" style="text-align: right;">{{ number_format($to_report['range_weight'][$ktime],2) }}</td>
                        </tr>                                                            
                    @endforeach
                @endif
                <tr style="background-color:#b1d3f5">
                    <th class="cell">Grand Total</th>
                    <th class="cell" style="text-align: right;">{{ array_sum($to_report['range']) }}</th>
                    <th class="cell" style="text-align: right;">{{ number_format(array_sum($to_report['range_weight']),2) }}</th>
                </tr>
            </tbody>
        </table>
    @else
        <p style="text-indent: 2.5em;">
            <h5 class="col">ไม่พบข้อมูลวันที่ {{ $sent_date }}</h5>
        </p>
    @endif   
    <br>
    <p style="text-indent: 2.5em;"> - สรุปการลงวัตถุดิบ</p>
    
    @if(!empty($tb_receipt))
        <table style="width: 80%">
            <thead>
                <tr>
                    <th class="cell" style="background-color:#f3d5c2" rowspan="3">วันที่ </th>
                    <th class="cell" style="text-align: center; background-color:#659855" colspan="12">ประเภทการลงวัตถุดิบ</th>
                </tr>
                <tr>
                    @php
                        $col_color = ['#e8a071', '#bddff1', '#574b5e'];
                        $col_rang = ['< 10 นาที', '< 15 นาที', '< 20 นาที', '> 20 นาที'];
                        $tr = ['พนักงานยก', 'เครื่องเรียงกระสอบ'];
                        $tr_data = ['พนักงานยก', 'เครื่องเรียง'];
                        $i=0;
                    @endphp
                    @foreach ($tr as $ktr=>$vtr)
                        <th class="cell" style="text-align: center; background-color:{{ $col_color[$i] }}" colspan="4">{{ $vtr }}</th>
                        @php
                            $i++;
                        @endphp
                    @endforeach    
                    <th class="cell" style="text-align: center; background-color:#574b5e" colspan="4">total</th>
                </tr>
                <tr>
                    @for($i=0;$i<3;$i++)
                        @foreach ($col_rang as $key=>$val)
                            <th class="cell" style="background-color:{{ $col_color[$i] }}">{{ $val }}</th>
                        @endforeach
                    @endfor
                </tr>
            </thead>
            <tbody>
                @php
                    $i=0;
                    foreach ($col_rang as $key=>$val) {
                        $sum_tr[$val] = 0;
                    }
                @endphp
                <tr>
                    <th class="cell">{{ $sent_date }}</th>                                                           
                    @foreach ($tr_data as $ktr=>$vtr)
                        @foreach ($col_rang as $key=>$val)         
                            <th class="cell" style="text-align: right; background-color:{{ $col_color[$i] }}">@if(!empty($tb_receipt[$vtr][$val])){{ $tb_receipt[$vtr][$val] }}@else{{ 0 }}@endif</th>
                            @php
                                if(!empty($tb_receipt[$vtr][$val]))
                                    $sum_tr[$val] += $tb_receipt[$vtr][$val];
                            @endphp
                        @endforeach   
                        @php
                            $i++;
                        @endphp 
                    @endforeach
                    @foreach ($col_rang as $key=>$val)
                        <th class="cell" style="text-align: right; background-color:#574b5e">{{ $sum_tr[$val] }}</th>
                    @endforeach
                </tr>
            </tbody>
        </table>
        {{-- <img src="{{ url($gp['link']) }}" alt="">  --}}
        @if(count($gp['bar1'])>0)<img src="{{ $message->embed($gp['bar1']['path']) }}" />@endif
        
    @else
        <p style="text-indent: 2.5em;">
            <h5 class="col">ไม่พบข้อมูลวันที่ {{ $sent_date }}</h5>
        </p>
    @endif
    
    <br>  
    {{-- <p> - สรุปการตรวจรับกระสอบบรรจุถั่วแระ</p>      
    <img src="{{ url($gp['link']) }}" alt=""> 
    @if(count($gp['pie'])>0)<img src="{{ $message->embed($gp['pie']['path']) }}" />@endif
    <img src="{{ url($gp['link']) }}" alt=""> 
    @if(count($gp['bar2'])>0)<img src="{{ $message->embed($gp['bar2']['path']) }}" />@endif
    <br>  --}}
    @if(count($mail_problem)>0 || count($show_img)>0) 
        <p><strong>ปัญหาที่พบ</strong></p>
        @if(!empty($mail_problem))
            <p style="text-indent: 2.5em;">1. น้ำหนักค่าเฉลี่ย/กระสอบเกิน 18.00 Kg</p>    
            <table style="width: 80%">
                <thead>
                    <tr style="text-align: center; background-color:#f3d5c2">
                        <th class="cell">ชื่อวัตถุดิบ </th>
                        <th class="cell">Broker Name</th>
                        <th class="cell">Farmer name</th>
                        <th class="cell">จำนวน</th>
                        {{-- <th class="cell">ภาชนะ</th> --}}
                        <th class="cell">นน.สุทธิ</th>
                        <th class="cell">Weight Package Net</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($mail_problem)>0)
                        @foreach ($mail_problem as $key=>$value)
                            <tr>
                                @php
                                    $ep_pb = explode('@;', $value);
                                @endphp
                                @foreach ($ep_pb as $kid=>$vid)
                                    <th class="cell" @if($kid>=3)style="text-align: right;"@endif>@if($kid==4||$kid==5){{ number_format($vid,2) }}@else{{ $vid }}@endif</th>
                                @endforeach                                                                
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
            <br>
        @endif
        @if(!empty($show_img))            
            @foreach($show_img[$sent_date] as $key => $value)
                <img src="{{ $message->embed(config('myconfig.directory.whrm.completes.img').'/'.$value) }}" width="800"/>
            @endforeach            
        @endif
    @endif
    <p>Laco Warehouse System<br/></p>
    
</body>
</html>
