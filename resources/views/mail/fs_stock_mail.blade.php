<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>รายงาน</title>
        <style>
            table {
                border: solid 2px black;
            }

            thead tr th {
                border: solid 1px black;
                font-weight: bold;
                font-style: italic;
            }

            tbody tr td {
                border: solid 1px black;
            }
        </style>
    </head>

    <body style="font-family: Cordia New; font-size:160%;">
        <p><strong>TO..ALL</strong></p>
        <p><strong>รายงาน Stock FS ประจำวันที่ {{ date('d/m/Y') }}</strong></p>
        @foreach ($data as $key => $item)
            <p><strong>FS {{ $item->name }}</strong></p>
            @if (isset($chartname[$key]['bar']['link']))
            <p><img src="{{ $message->embed($chartname[$key]['bar']['path']) }}"></p>
            @else
            <p>ไม่มีข้อมูล</p>
            @endif
            @if (isset($queries[$key]))
                <table style="width: 80%; text-align:center; font-size:18px;">
                    <thead>
                        <tr style="background-color:orange;">
                            <th class="cell">สินค้า</th>
                            <th class="cell">จำนวน</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($queries[$key] as $fsdata)
                            <tr>
                                <td>{{ $fsdata->name }}</td>
                                <td>{{ number_format($fsdata->volumn) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <p>ไม่มีข้อมูล</p>
            @endif
        @endforeach
        <p>* {{ $note }}</p>
        <br>
        <p>Laco Warehouse System<br /></p>
    </body>
</html>
