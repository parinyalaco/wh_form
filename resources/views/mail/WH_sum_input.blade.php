<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>รายงาน</title>
    <style>
        table {
            border: solid 2px black;
            border-collapse: collapse;
        }
        thead tr th{
            border: solid 1px black;
            border-collapse: collapse;
            font-weight: bold;
        }
        tbody tr td{
            border: solid 1px black;
            border-collapse: collapse;
        }
    </style>
</head>
<body style="font-family: Cordia New; font-size:160%;">
    <p><strong>TO..ALL</strong></p>
    <p><strong>รายงานการรับเข้า ระหว่างวันที่ {{ $st_show }} ถึงวันที่ {{ $ed_show }}</strong></p>
    <p><strong>1.รายงานสรุปรับเข้า</strong></p>
    <table>
        <thead>
            <tr>
                <th class="cell" rowspan="2">รับเข้า</th>
                <th class="cell" rowspan="2">วันที่</th>
                <th class="cell">รับทั้งหมด</th>
                <th class="cell" colspan="2">รับจริง</th>
                <th class="cell" colspan="2">% (เปอร์เซ็นต์)</th>
            </tr>
            <tr>
                <th class="cell">จำนวน</th>
                @for($i=0;$i<2;$i++)
                    <th class="cell">ตรงแผน</th>
                    <th class="cell">ไม่ตรงแผน</th>
                @endfor
            </tr>
        </thead>
        <tbody>
            @foreach ($st_type as $kst=>$vst)
                <tr>
                    <td class="cell" colspan="7">{{ $vst }}</td>
                </tr>
                @foreach ($data_show[$kst] as $key=>$value)
                    <tr>
                        <td class="cell"></td>
                        <td class="cell">{{ $key }}</td>
                        <td class="cell" style="text-align:right;">{{ $data_show[$kst][$key]['all'] }}</td>
                        <td class="cell" style="text-align:right;">{{ $data_show[$kst][$key]['in_plan'] }}</td>
                        <td class="cell" style="text-align:right;">@if(!empty($data_show[$kst][$key]['out_plan'])){{ $data_show[$kst][$key]['out_plan'] }}@endif</td>
                        <td class="cell" style="text-align:right;">@if(!empty($data_show[$kst][$key]['in_plan'])){{ number_format($data_show[$kst][$key]['in_plan']/$data_show[$kst][$key]['all']*100,2) }}@endif</td>
                        <td class="cell" style="text-align:right;">@if(!empty($data_show[$kst][$key]['out_plan'])){{ number_format($data_show[$kst][$key]['out_plan']/$data_show[$kst][$key]['all']*100,2) }}@endif</td>
                    </tr>
                @endforeach
            @endforeach
        </tbody>
    </table>
    <br>
    <p><strong>2.รายงานสินค้าไม่ตรงตามแผน</strong></p>
    <table>
        <thead>
            <tr>
                <th class="cell" rowspan="2">รับเข้า</th>
                <th class="cell" rowspan="2">วันที่</th>
                <th class="cell">แผนรับเข้า</th>
                <th class="cell">รับจริง</th>
                <th class="cell">เลขที่ PO</th>
                <th class="cell" rowspan="2">ไม่ตรงตามแผน</th>
                <th class="cell" rowspan="2">หมายเหตุ</th>
            </tr>
            <tr>
                <th class="cell">(รายการ)</th>
                <th class="cell">(รายการ)</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($st_type as $kst=>$vst)
                @if(!empty($show_mat[$kst][2]))
                    <tr>
                        <td class="cell" colspan="7">{{ $vst }}</td>
                    </tr>
                    @foreach ($data_show[$kst] as $key=>$value)
                        @if(!empty($show_mat[$kst][2][$key]))
                            <tr>                     
                                <td class="cell"></td>                            
                                <td class="cell">{{ $key }}</td>
                                <td class="cell">{{ $data_show[$kst][$key]['in_po'] }}</td>
                                <td class="cell">{{ $data_show[$kst][$key]['in_real'] }}</td>  
                                @php $a=0; @endphp                            
                                @foreach ($show_mat[$kst][2][$key] as $kpo=>$vpo)
                                    @if($a>0)
                                        <tr>
                                        @for($i=0; $i<4; $i++)
                                            <td class="cell"></td>
                                        @endfor
                                    @endif
                                    <td class="cell">{{ $kpo }}</td>
                                    @php $b=0; @endphp 
                                    @foreach ($vpo as $kmat=>$vmat)   
                                        @if($b>0)
                                            <tr>
                                            @for($i=0; $i<5; $i++)
                                                <td class="cell"></td>
                                            @endfor
                                        @endif                                    
                                        <td class="cell">{{ $mat[$kmat] }}</td>
                                        <td class="cell">{{ $vmat }}</td>
                                        @php $b++; @endphp
                                        </tr>
                                    @endforeach
                                    @php $a++; @endphp
                                @endforeach
                        @endif                    
                    @endforeach
                @endif
            @endforeach
        </tbody>
    </table>
    <br>    
    <p><strong>3.กราฟแสดงรายการรับ</strong></p>
    {{-- <img src="{{ $chart_3 }}"> --}}
    {{-- <img src="{{ url($chart_name['pie']['link']) }}" alt="">     --}}
    {{-- <img src="{{ $message->embed($chart_name['pie']['path']) }}" /> --}}
    <table>
        <tbody>
            <tr>
                @foreach ($st_type as $kst=>$vst)
                    <td style="text-align: center">รับเข้า {{ $vst }}</td>
                @endforeach 
            </tr>        
            <tr>
                @foreach ($st_type as $kst=>$vst)
                    <td style="text-align: center">
                        @if(!empty($chart_name[$kst]['pie']['path']))
                            <img src="{{ $message->embed($chart_name[$kst]['pie']['path']) }}"/>
                            {{-- <img src="{{ $chart_name[$kst]['pie']['path'] }}"/> --}}
                        @endif
                    </td>
                @endforeach 
            </tr>
        </tbody>
    </table>
    <br>
    <p><strong>4.รายงานสรุปการสุ่มรับเข้า LACO</strong></p>
    <table>
        <thead>
            <tr>
                <th class="cell" rowspan="2">วันที่</th>
                <th class="cell">ทั้งหมดรับเข้า</th>
                <th class="cell">สุ่มผ่าน</th>
                <th class="cell">ค้างสุ่ม</th>
                <th class="cell" rowspan="2">% ค้างสุ่ม</th>
                <th class="cell" rowspan="2">Remark</th>
            </tr>
            <tr>
                <th class="cell">(รายการ)</th>
                <th class="cell">(รายการ)</th>
                <th class="cell">(รายการ)</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data_show4['laco'] as $key=>$value)
                <tr>
                    <td class="cell" @if(!empty($show_mat['laco'][4][$key])) rowspan="{{ count($show_mat['laco'][4][$key]) }}" @endif>
                        {{ $key }}</td>
                    <td class="cell" style="text-align:right;"
                        @if(!empty($show_mat['laco'][4][$key])) rowspan="{{ count($show_mat['laco'][4][$key]) }}" @endif>
                        {{ $data_show4['laco'][$key]['all'] }}</td>
                    <td class="cell" style="text-align:right;"
                        @if(!empty($show_mat['laco'][4][$key])) rowspan="{{ count($show_mat['laco'][4][$key]) }}" @endif>
                        @if(!empty($data_show4['laco'][$key]['in_pass'])){{ $data_show4['laco'][$key]['in_pass'] }} @endif
                    </td>
                    <td class="cell" style="text-align:right;"
                        @if(!empty($show_mat['laco'][4][$key])) rowspan="{{ count($show_mat['laco'][4][$key]) }}" @endif>
                        @if(!empty($data_show4['laco'][$key]['not_pass'])){{ $data_show4['laco'][$key]['not_pass'] }} @else 0 @endif
                    </td>
                    <td class="cell" style="text-align:right;"
                        @if(!empty($show_mat['laco'][4][$key])) rowspan="{{ count($show_mat['laco'][4][$key]) }}" @endif>
                        @if(!empty($data_show4['laco'][$key]['not_pass']))
                            {{ number_format($data_show4['laco'][$key]['not_pass']/$data_show4['laco'][$key]['all']*100,2) }}
                        @endif
                    </td>
                    @php $i=0; @endphp
                    @if(!empty($show_mat[4][$key]))
                        @foreach ($show_mat[4][$key] as $kmat=>$vmat)
                            @if($i<>0)<tr>@endif
                            <td class="cell">{{ $vmat }}</td>
                            @if($i<(count($show_mat[4][$key])-1))</tr>@endif
                            @php
                                $i++;
                            @endphp
                        @endforeach
                    @else
                        <td class="cell"></td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <p><strong>5.กราฟแสดงข้อมูลตามประเภทสินค้ารายการรับเข้า LACO</strong></p>
    {{-- <img src="{{ $chart_5 }}"> --}}
    {{-- <img src="{{ url($chart_name['bar']['link']) }}" alt=""> --}}
    <img src="{{ $message->embed($chart_name['laco']['bar']['path']) }}" />
    {{-- <img src="{{ $chart_name['laco']['bar']['path'] }}" /> --}}
    <br>
    <p>Laco Warehouse System<br/></p>
    
</body>
</html>
