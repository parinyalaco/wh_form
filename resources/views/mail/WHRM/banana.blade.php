<h5> สรุปการลงวัตถุดิบ{{ $reports['banana']['type'] }} รับเข้าวันที่
    {{ date('d/m/Y', strtotime($sent_date . '+543 year')) }} </h5>

<p> 1. RM รับเข้าทั้งหมด {{ number_format($reports['banana']['totelWeight'], 2) }} Kg. </p>
<p> 2. รถขนส่งเข้าโรงงานทั้งหมด {{ $reports['banana']['car_in'] }} คัน </p>
<p>- คันแรกถึง LACO เวลา {{ $reports['banana']['first'] }} น.</p>
<p>- คันสุดท้ายถึง LACO เวลา {{ $reports['banana']['last'] }} น.</p>
<p>- ลงวัตถุดิบคันสุดท้ายเสร็จเวลา {{ $reports['banana']['finish'] }} น.</p>

<strong>ช่วงเวลารถเข้า</strong>
<table style="width: 75%">
    <thead style="background-color: #B1D3F5">
        <tr style="text-align: center">
            <th>เวลา</th>
            <th>วันที่ 13-05-2024</th>
            <th>น้ำหนักวัตถุดิบสุทธิ</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($reports['banana']['entryTime'] as $key => $entryTime)
            <tr @if ($key == 'Grand Total') style="background-color:#B1D3F5 " @endif>
                <th>{{ $key }}</th>
                <th style="text-align: right">{{ $entryTime['count'] }}</td>
                <th style="text-align: right">{{ number_format($entryTime['weight'], 2) }}</th>

            </tr>
        @endforeach
    </tbody>

</table>

<strong>สรุปการลงวัตถุดิบ</strong>
<table style="width: 100%">
    <thead>
        <tr style="text-align: center">
            <th style="vertical-align: middle ;background-color: #F5E4D9" rowspan="3">วันที่</th>
            <th colspan="12" style="background-color: #abf0af">ประเภทการลงวัตถุดิบ</th>
        </tr>
    </thead>
    <thead>
        <tr style="text-align: center">
            @foreach ($reports['banana']['typeReceive'] as $type => $item)
                @switch($type)
                    @case('พนักงานยก')
                        <th colspan="4" style="background-color: #F6BC96">
                            {{ $type }} นาที</th>
                    @break

                    @case('เครื่องเรียงกระสอบ')
                        <th colspan="4" style="background-color: #B1D3F5">
                            {{ $type }} นาที</th>
                    @break

                    @case('total')
                        <th colspan="4" style="background-color: #eed6fc">
                            {{ $type }} นาที</th>
                    @break

                    @default
                @endswitch
            @endforeach
        </tr>
    </thead>
    <thead>
        <tr style="text-align: center">
            @foreach ($reports['banana']['typeReceive'] as $type => $item)
                @foreach ($reports['banana']['typeReceive'][$type] as $time => $item)
                    @switch($type)
                        @case('พนักงานยก')
                            <th style="background-color: #F6BC96">
                                {{ $time }} นาที</th>
                        @break

                        @case('เครื่องเรียงกระสอบ')
                            <th style="background-color: #B1D3F5">
                                {{ $time }} นาที</th>
                        @break

                        @case('total')
                            <th style="background-color: #eed6fc">
                                {{ $time }} นาที</th>
                        @break

                        @default
                    @endswitch
                @endforeach
            @endforeach
        </tr>
    </thead>
    <thead>
        <tr>
            <td style="text-align: center;background-color: #F5E4D9">
                {{ date('d/m/Y', strtotime($sent_date . '+543 year')) }}</td>
            @foreach ($reports['banana']['typeReceive'] as $type => $ite1m)
                @foreach ($reports['banana']['typeReceive'][$type] as $time => $ite2m)
                    @switch($type)
                        @case('พนักงานยก')
                            <th style="background-color: #F6BC96;text-align: center">
                                {{ $reports['banana']['typeReceive'][$type][$time] }} นาที</th>
                        @break

                        @case('เครื่องเรียงกระสอบ')
                            <th style="background-color: #B1D3F5;text-align: center">
                                {{ $reports['banana']['typeReceive'][$type][$time] }} นาที</th>
                        @break

                        @case('total')
                            <th style="background-color: #eed6fc;text-align: center">
                                {{ $reports['banana']['typeReceive'][$type][$time] }} นาที</th>
                        @break

                        @default
                    @endswitch
                @endforeach
            @endforeach
        </tr>
    </thead>
</table>
<img src="{{ $message->embed(storage_path($chartPath)) }}" alt="Chart">
<hr>
@if (!empty($reports['banana']['dailyIssue']))
    <strong>ปัญหาที่พบ ประจำวัน</strong>

    <table style="width: 100%">
        <thead>
            <tr style="background-color: #f7cfcf">
                <th style="width:200px">วันที่</th>
                <th style="text-align: center">ปัญหาที่พบ</th>
            </tr>
        </thead>
        <tr>
            <td>{{ $reports['banana']['dailyIssue']['issue_date'] }}</td>
            <td>{{ $reports['banana']['dailyIssue']['detail'] }}
                <hr>

                @if (!empty($reports['banana']['dailyIssue']['imgs']))
                    @foreach ($reports['banana']['dailyIssue']['imgs'] as $img)
                        <img src="{{ $message->embed(storage_path($img)) }}" alt="img" style="width: 100px;">
                        &nbsp;
                    @endforeach
                @endif
            </td>
        </tr>
    </table>
@endif
<hr>
@if (!empty($reports['banana']['issue']))
    <strong>วัตถุดิบที่มีปัญหา</strong>

    <table class="table table-bordered">
        <thead style="background-color: #B1D3F5">

            <tr style="background-color: #f7cfcf">
                <th style="width:200px">SAP No.</th>
                <th style="width:100px">ทะเบียนรถ</th>
                <th style="text-align: center">ปัญหาที่พบ</th>
            </tr>
        </thead>

        @foreach ($reports['banana']['issue'] as $key => $item)
            <tr>
                <td>{{ $item['sap_no'] }}</td>
                <td>{{ $item['car_name'] }}</td>
                <td>
                    @if (!empty($item['sub_issues']))
                        @foreach ($item['sub_issues'] as $sub_issue)
                            {{ $sub_issue['detail'] }}
                            <br>
                            @if (!empty($sub_issue['imgs']))
                                @foreach ($sub_issue['imgs'] as $img)
                                    <img src="{{ $message->embed(storage_path($img)) }}" alt="img">&nbsp;
                                @endforeach
                            @endif
                            <hr>
                        @endforeach
                        <hr>
                    @endif


                </td>
            </tr>
        @endforeach
    </table>
@endif
