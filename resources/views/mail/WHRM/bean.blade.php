<h2><strong> สรุปการลงวัตถุดิบ{{ $reports['bean']['type'] }} รับเข้าวันที่
        {{ date('d/m/Y', strtotime($sent_date . '+543 year')) }}</strong> </h2>

<p> 1. RM รับเข้าทั้งหมด {{ number_format($reports['bean']['totelWeight'], 2) }} Kg.
<p> 2. รถขนส่งเข้าโรงงานทั้งหมด {{ $reports['bean']['car_in'] }} คัน </p>
<p>- คันแรกถึง LACO เวลา {{ $reports['bean']['first'] }} น.</p>
<p>- คันสุดท้ายถึง LACO เวลา {{ $reports['bean']['last'] }} น.</p>
<p>- ลงวัตถุดิบคันสุดท้ายเสร็จเวลา {{ $reports['bean']['finish'] }} น.</p>

<strong>ช่วงเวลารถเข้า</strong>
<table style="width:75%">
    <thead style="background-color: #B1D3F5">
        <tr style="text-align: center">
            <th>เวลา</th>
            <th>วันที่ 13-05-2024</th>
            <th>น้ำหนักวัตถุดิบสุทธิ</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($reports['bean']['entryTime'] as $key => $entryTime)
            <tr @if ($key == 'Grand Total') style="background-color:#B1D3F5 " @endif>
                <th>{{ $key }}</th>
                <th style="text-align: right">{{ $entryTime['count'] }}</td>
                <th style="text-align: right">{{ number_format($entryTime['weight'], 2) }}</th>
            </tr>
        @endforeach
    </tbody>

</table>

<strong>สรุปการลงวัตถุดิบ</strong>
<table style="width: 100%">
    <thead>
        <tr style="text-align: center">
            <th style="vertical-align: middle ;background-color: #F5E4D9" rowspan="3">วันที่</th>
            <th colspan="12" style="background-color: #abf0af">ประเภทการลงวัตถุดิบ</th>
        </tr>
    </thead>
    <thead>

        <tr style="text-align: center">
            @foreach ($reports['bean']['typeReceive'] as $type => $item)
                @switch($type)
                    @case('พนักงานยก')
                        <th colspan="4" style="background-color: #F6BC96">
                            {{ $type }} นาที</th>
                    @break

                    @case('เครื่องเรียงกระสอบ')
                        <th colspan="4" style="background-color: #B1D3F5">
                            {{ $type }} นาที</th>
                    @break

                    @case('total')
                        <th colspan="4" style="background-color: #eed6fc">
                            {{ $type }} นาที</th>
                    @break

                    @default
                @endswitch
            @endforeach
        </tr>
    </thead>
    <thead>
        <tr style="text-align: center">
            @foreach ($reports['bean']['typeReceive'] as $type => $item)
                @foreach ($reports['bean']['typeReceive'][$type] as $time => $item)
                    @switch($type)
                        @case('พนักงานยก')
                            <th style="background-color: #F6BC96">
                                {{ $time }} นาที</th>
                        @break

                        @case('เครื่องเรียงกระสอบ')
                            <th style="background-color: #B1D3F5">
                                {{ $time }} นาที</th>
                        @break

                        @case('total')
                            <th style="background-color: #eed6fc">
                                {{ $time }} นาที</th>
                        @break

                        @default
                    @endswitch
                @endforeach
            @endforeach
        </tr>
    </thead>

    <tbody>
        <tr>
            <td style="text-align: center;background-color: #F5E4D9">
                {{ date('d/m/Y', strtotime($sent_date . '+543 year')) }}</td>
            @foreach ($reports['bean']['typeReceive'] as $type => $ite1m)
                @foreach ($reports['bean']['typeReceive'][$type] as $time => $ite2m)
                    @switch($type)
                        @case('พนักงานยก')
                            <th style="background-color: #F6BC96;text-align: center">
                                {{ $reports['bean']['typeReceive'][$type][$time] }} นาที</th>
                        @break

                        @case('เครื่องเรียงกระสอบ')
                            <th style="background-color: #B1D3F5;text-align: center">
                                {{ $reports['bean']['typeReceive'][$type][$time] }} นาที</th>
                        @break

                        @case('total')
                            <th style="background-color: #eed6fc;text-align: center">
                                {{ $reports['bean']['typeReceive'][$type][$time] }} นาที</th>
                        @break

                        @default
                    @endswitch
                @endforeach
            @endforeach
        </tr>
    </tbody>
</table>
<br>

<img src="{{ $message->embed(storage_path($chartPath)) }}" alt="Chart">

<hr>
@if (!empty($reports['bean']['dailyIssue']))
    <strong>ปัญหาที่พบ ประจำวัน</strong>

    <table style="width: 100%">
        <thead>
            <tr style="background-color: #f7cfcf">
                <th style="width:200px">วันที่</th>
                <th style="text-align: center">ปัญหาที่พบ</th>
            </tr>
        </thead>
        <tbody>
            <tr style="background-color: #d3d3d3">
                <td style="vertical-align: top">{{ $reports['bean']['dailyIssue']['issue_date'] }}</td>
                <td style="vertical-align: top">{{ $reports['bean']['dailyIssue']['detail'] }}
                    <hr>

                    @if (!empty($reports['bean']['dailyIssue']['imgs']))
                        @foreach ($reports['bean']['dailyIssue']['imgs'] as $img)
                            @if (!empty($img))
                                <img src="{{ $message->embed(storage_path($img)) }}" alt="img"
                                    style="width: 100px;">
                                &nbsp;
                            @endif
                        @endforeach
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
@endif
<hr>
@if (!empty($reports['bean']['issue']))
    <strong>วัตถุดิบที่มีปัญหา</strong>

    <table style="width: 100%">
        <thead style="background-color: #f3f3f3">

            <tr style="background-color: #f7cfcf">
                <th style="width:200px">SAP No.</th>
                <th style="width:100px">ทะเบียนรถ</th>
                <th style="text-align: center">ปัญหาที่พบ</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($reports['bean']['issue'] as $key => $item)
                <tr style="background-color: #f3f3f3">
                    <td style="vertical-align: top">{{ $item['sap_no'] }}</td>
                    <td style="vertical-align: top">{{ $item['car_name'] }}</td>
                    <td>
                        @if (!empty($item['sub_issues']))
                            @foreach ($item['sub_issues'] as $sub_issue)
                                {{ $sub_issue['detail'] }}
                                <br>
                                @if (!empty($sub_issue['imgs']))
                                    @foreach ($sub_issue['imgs'] as $img)
                                        @if (!empty($img))
                                            <img src="{{ $message->embed(storage_path($img)) }}" alt="img">&nbsp;
                                        @endif
                                    @endforeach
                                @endif
                                <hr>
                            @endforeach
                            <hr>
                        @endif


                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endif
