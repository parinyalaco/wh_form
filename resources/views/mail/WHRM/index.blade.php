<!DOCTYPE html>
<html>
<meta charset="UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<head>
    <title>Chart Email</title>
    <style>
        table {
            border: solid 2px #818181;
            border-collapse: collapse;
        }

        thead tr th {
            border: solid 2px #818181;
            border-collapse: collapse;
            font-weight: bold;
            height: 30px;
        }

        tbody tr td {
            border: solid 2px #818181;
            border-collapse: collapse;
            height: 30px;
        }

        tbody tr th {
            border: solid 2px #818181;
            border-collapse: collapse;
            font-weight: bold;
            height: 30px;
        }

        thead tr,
        tbody tr {
            height: 20px;
        }

        .image-container {
            display: flex;
            flex-wrap: wrap;
        }

        .image-container img {
            margin: 10px;
            width: 150px;
            height: auto;
        }
    </style>

</head>

<body style="font-family: Cordia New; font-size:160%;">

    <div style="width: 1000px">
        <p><strong>TO..ALL</strong></p>

        @if (isset($reports['bean']))
            @include('mail.WHRM.bean')
        @endif

        @if (isset($reports['banana']))
            @include('mail.WHRM.banana')
        @endif
    </div>

    <p>Laco Warehouse System<br /></p>
</body>

</html>
