@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <h1 class="app-page-title mb-0">รายงานสรุปงาน Stock FS</h1>
                </div>
                <div class="col-auto">
                    <div class="page-utilities">
                        <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                            <div class="col-auto">
                                <a class="btn app-btn-secondary" href="{{ route('FsReports.fsstockuseemail') }}">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;
                                    Sent Mail
                                </a>
                                {{-- <a class="btn app-btn-secondary" href="{{ route('wh_act.create') }}">
                                    <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
                                    Add
                                </a> --}}
                            </div>
                        </div><!--//row-->
                    </div><!--//table-utilities-->
                </div><!--//col-auto-->
            </div><!--//row onclick="getURL($key);" -->

            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>
            @foreach ($data as $key => $item)
            <div class="row">
                <div class="col-6">
                    <h3>{{  $item->name }}</h3>
                </div>
            </div>
            <div class="row">

                <div class="col-6">
                    @if (isset($chartname[$key]['bar']['link']))
                        <img src="/whform/{{$chartname[$key]['bar']['link']}}" >
                    @else
                        ไม่มีข้อมูล
                    @endif
                </div>
                <div class="col-6">
                    @if (isset($queries[$key]))
                    <div class="tab-content" id="orders-table-tab-content">
                        <div class="app-card app-card-orders-table shadow-sm mb-5">
                            <div class="app-card-body">
                                <table class="table app-table-hover mb-0 text-left">
                                    <thead>
                                        <tr>
                                            <th class="cell">สินค้า</th>
                                            <th class="cell" >จำนวน</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($queries[$key] as $fsdata)
                                            <tr>
                                                <td>{{ $fsdata->name }}</td>
                                                <td>{{ number_format($fsdata->volumn) }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div><!--//app-card-body-->
                        </div><!--//app-card-->
                    </div><!--//tab-content-->
                    @else
                        ไม่มีข้อมูล
                    @endif

                </div>

            </div>
            @endforeach


        </div><!--//container-fluid-->
    </div><!--//app-content-->
@endsection
