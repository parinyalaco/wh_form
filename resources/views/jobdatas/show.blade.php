@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Job Data {{ $jobdata->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('jobdatas/' . $jobdata->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit job data"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['jobdatas', $jobdata->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete job data',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $jobdata->id }}</td>
                                    </tr>
                                    <tr>
                                        <th>Name</th><td>{{ $jobdata->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Desc</th><td>{{ $jobdata->desc }}</td>
                                    </tr>
                                    <tr>
                                        <th>Status</th><td>{{ $jobdata->status }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection