@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <div class="d-flex flex-row">
                        <span class="nav-item">
                            <a class="btn-sm app-btn-secondary" href="{{ route('pd_report') }}">
                                <i class="fas fa-long-arrow-alt-left"></i>
                                Back
                            </a>
                        </span> 
                        <h1 class="app-page-title mb-0">รายงานสินค้าไม่ตรงตามแผน</h1>
                    </div>
                </div>                
            </div><!--//row onclick="getURL($key);" -->            
            
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav> 
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <div class="table-responsive">
                            <table class="table app-table-hover mb-0 text-left">
                                <thead>
                                    <tr>
                                        <th class="cell" rowspan="2">วันที่</th>
                                        <th class="cell">แผนรับเข้า</th>
                                        <th class="cell">รับจริง</th>
                                        <th class="cell" rowspan="2">PO No.</th>
                                        <th class="cell" rowspan="2">ไม่ตรงตามแผน</th>
                                        <th class="cell" rowspan="2">หมายเหตุ</th>
                                    </tr>
                                    <tr>
                                        <th class="cell">(รายการ)</th>
                                        <th class="cell">(รายการ)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($to_show as $key=>$value)
                                        @foreach ($value as $kpo=>$vpo)
                                            @if(!empty($to_show_mat[$key][$kpo]))
                                                <tr>
                                                    <td class="cell" rowspan="{{ count($to_show_mat[$key][$kpo]) }}">{{ $key }}</td>
                                                    <td class="cell" rowspan="{{ count($to_show_mat[$key][$kpo]) }}">{{ $to_show[$key][$kpo]['all'] }}</td>
                                                    <td class="cell" rowspan="{{ count($to_show_mat[$key][$kpo]) }}">{{ $to_show[$key][$kpo]['in_real'] }}</td>
                                                    <td class="cell">{{ $kpo }}</td>
                                                    @php $i=0; @endphp
                                                    @foreach ($to_show_mat[$key][$kpo] as $kmat=>$vmat)
                                                        @if($i<>0)<tr>@endif
                                                        <td class="cell">{{ $mat[$kmat] }}</td>
                                                        <td class="cell">{{ $vmat }}</td>
                                                        </tr>
                                                        @php
                                                            $i++;
                                                        @endphp
                                                    @endforeach
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div><!--//table-responsive-->
                    
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->     
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
@endsection