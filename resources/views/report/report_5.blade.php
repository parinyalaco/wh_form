@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-3">
        <div class="app-container-xl">
            
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <div class="d-flex flex-row">
                        <span class="nav-item">
                            <a class="btn-sm app-btn-secondary" href="{{ route('pd_report') }}">
                                <i class="fas fa-long-arrow-alt-left"></i>
                                Back
                            </a>
                        </span> 
                        <h1 class="app-page-title mb-0">กราฟแสดงข้อมูลตามประเภทสินค้า วันที่ {{ $graph_date }}</h1>
                    </div>
                </div>                
            </div><!--//row onclick="getURL($key);" -->   
            
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>
                          
            <div class="app-card app-card-orders-table shadow-sm mb-5">
                <div class="d-flex justify-content-center">
                    <canvas id="myChart" style="width:100%;max-width:600px"></canvas>
                </div><!--//app-card-body-->		
            </div><!--//app-card-->  
        </div><!--//container-fluid-->
    </div><!--//app-content-->
    <script src="{{ asset('assets/js/Chart.js') }}"></script>
    <script>
        window.chartColors = {
            green: '#75c181', // rgba(117,193,129, 1)
            blue: '#5b99ea', // rgba(91,153,234, 1)
            gray: '#a9b5c9',
            text: '#252930',
            border: '#e7e9ed'
        };

        new Chart("myChart", {
            type: 'bar',

            data: {
                labels: <?php echo ($pd_type); ?>,
                datasets: [{
                    label: 'Sum of รายการ',
                    backgroundColor: "rgba(117,193,129,0.8)", 
                    hoverBackgroundColor: "rgba(117,193,129,1)",			
                    
                    data: <?php echo ($val_all); ?>
                }, 
                {
                    label: 'Sum of ผลสุ่ม',
                    backgroundColor: "rgba(91,153,234,0.8)", 
                    hoverBackgroundColor: "rgba(91,153,234,1)",
                    
                    
                    data: <?php echo ($val_rd); ?>
                }
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'bottom',
                    align: 'end',
                },

                tooltips: {
                    mode: 'index',
                    intersect: false,
                    titleMarginBottom: 10,
                    bodySpacing: 10,
                    xPadding: 16,
                    yPadding: 16,
                    borderColor: window.chartColors.border,
                    borderWidth: 1,
                    backgroundColor: '#fff',
                    bodyFontColor: window.chartColors.text,
                    titleFontColor: window.chartColors.text,
                    callbacks: {
                        label: function(tooltipItem, data) {	                 
                            return tooltipItem.value + ' รายการ';   
                        }
                    },
                    

                },
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: {
                            drawBorder: false,
                            color: window.chartColors.border,
                        },

                    }],
                    yAxes: [{
                        display: true,
                        gridLines: {
                            drawBorder: false,
                            color: window.chartColors.borders,
                        },
                        ticks: {
                            beginAtZero: true,
                            userCallback: function(value, index, values) {
                                return value;  
                            }
                        },

                        
                    }]
                }
                
            }
        });
    </script>
@endsection