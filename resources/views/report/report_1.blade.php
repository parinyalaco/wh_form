@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <div class="d-flex flex-row">
                        <span class="nav-item">
                            <a class="btn-sm app-btn-secondary" href="{{ route('pd_report') }}">
                                <i class="fas fa-long-arrow-alt-left"></i>
                                Back
                            </a>
                        </span> 
                        <h1 class="app-page-title mb-0">รายงานสรุปรับเข้า</h1>
                    </div>
                </div>
                <div class="col-auto">
                    <div class="page-utilities">
                        <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                        </div><!--//row-->
                    </div><!--//table-utilities-->
                </div><!--//col-auto-->
            </div><!--//row onclick="getURL($key);" -->     
            
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>                    
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <div class="table-responsive">
                            <table class="table app-table-hover mb-0 text-left">
                                <thead>
                                    <tr>
                                        <th class="cell" rowspan="2">วันที่</th>
                                        <th class="cell" colspan="2">รับทั้งหมด</th>
                                        <th class="cell" colspan="2">รับจริง</th>
                                        <th class="cell" colspan="2">% (เปอร์เซ็นต์)</th>
                                    </tr>
                                    <tr>
                                        <th class="cell">จำนวน</th>
                                        @for($i=0;$i<2;$i++)
                                            <th class="cell">ตรงแผน</th>
                                            <th class="cell">ไม่ตรงแผน</th>
                                        @endfor
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($to_show as $key=>$value)
                                        <tr>
                                            <td class="cell">{{ $key }}</td>
                                            <td class="cell">{{ $to_show[$key]['all'] }}</td>
                                            <td class="cell">{{ $to_show[$key]['in_plan'] }}</td>
                                            <td class="cell">@if(!empty($to_show[$key]['out_plan'])){{ $to_show[$key]['out_plan'] }}@endif</td>
                                            <td class="cell">{{ number_format($to_show[$key]['in_plan']/$to_show[$key]['all']*100,2) }}</td>
                                            <td class="cell">@if(!empty($to_show[$key]['out_plan'])){{ number_format($to_show[$key]['out_plan']/$to_show[$key]['all']*100,2) }}@endif</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div><!--//table-responsive-->
                    
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->     
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
@endsection