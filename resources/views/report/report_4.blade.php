@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <div class="d-flex flex-row">
                        <span class="nav-item">
                            <a class="btn-sm app-btn-secondary" href="{{ route('pd_report') }}">
                                <i class="fas fa-long-arrow-alt-left"></i>
                                Back
                            </a>
                        </span> 
                        <h1 class="app-page-title mb-0">รายงานสรุปการสุ่ม</h1>
                    </div>
                </div>                
            </div><!--//row onclick="getURL($key);" -->  
            
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav> 
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <div class="table-responsive">
                            <table class="table app-table-hover mb-0 text-left">
                                <thead>
                                    <tr>
                                        <th class="cell" rowspan="2">วันที่</th>
                                        <th class="cell">ทั้งหมดรับเข้า</th>
                                        <th class="cell">สุ่มผ่าน</th>
                                        <th class="cell">ค้างสุ่ม</th>
                                        <th class="cell" rowspan="2">% ค้างสุ่ม</th>
                                        <th class="cell" rowspan="2">Remark</th>
                                    </tr>
                                    <tr>
                                        <th class="cell">(รายการ)</th>
                                        <th class="cell">(รายการ)</th>
                                        <th class="cell">(รายการ)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($to_show as $key=>$value)
                                        <tr>
                                            <td class="cell" @if(!empty($to_show_mat[$key])) rowspan="{{ count($to_show_mat[$key]) }}" @endif>
                                                {{ $key }}</td>
                                            <td class="cell" @if(!empty($to_show_mat[$key])) rowspan="{{ count($to_show_mat[$key]) }}" @endif>
                                                {{ $to_show[$key]['all'] }}</td>
                                            <td class="cell" @if(!empty($to_show_mat[$key])) rowspan="{{ count($to_show_mat[$key]) }}" @endif>
                                                @if(!empty($to_show[$key]['in_pass'])){{ $to_show[$key]['in_pass'] }} @endif
                                            </td>
                                            <td class="cell" @if(!empty($to_show_mat[$key])) rowspan="{{ count($to_show_mat[$key]) }}" @endif>
                                                @if(!empty($to_show[$key]['not_pass'])){{ $to_show[$key]['not_pass'] }} @else 0 @endif
                                            </td>
                                            <td class="cell" @if(!empty($to_show_mat[$key])) rowspan="{{ count($to_show_mat[$key]) }}" @endif>
                                                @if(!empty($to_show[$key]['not_pass']))
                                                    {{ number_format($to_show[$key]['not_pass']/$to_show[$key]['all']*100,2) }}
                                                @endif
                                            </td>
                                            @php $i=0; @endphp
                                            @if(!empty($to_show_mat[$key]))
                                                @foreach ($to_show_mat[$key] as $kmat=>$vmat)
                                                    @if($i<>0)<tr>@endif
                                                    <td class="cell">{{ $mat[$kmat] }}</td>
                                                    </tr>
                                                    @php
                                                        $i++;
                                                    @endphp
                                                @endforeach
                                            @else
                                                <td class="cell"></td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div><!--//table-responsive-->
                    
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->     
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
@endsection