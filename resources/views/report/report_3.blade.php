@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-3">
        <div class="app-container-xl">
            
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <div class="d-flex flex-row">
                        <span class="nav-item">
                            <a class="btn-sm app-btn-secondary" href="{{ route('pd_report') }}">
                                <i class="fas fa-long-arrow-alt-left"></i>
                                Back
                            </a>
                        </span> 
                        <h1 class="app-page-title mb-0">รายการรับ วันที่ {{ $graph_date }}</h1>
                    </div>
                </div>                
            </div><!--//row onclick="getURL($key);" -->            
            
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>

            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <canvas id="myChart" style="width:100%;max-width:600px"></canvas>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->     
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
    <script src="{{ asset('assets/js/Chart.js') }}"></script>
    <script>
        var xValues = ["ตรงแผน", "ไม่ตรงตามแผน"];
        var yValues = [{{ round($in_plan, 2) }}, {{ round($out_plan, 2) }}
        ];
        var barColors = [
            "#FF6600",
            "#3bb767"
        ];

        new Chart("myChart", {
            type: "pie",
            data: {
                labels: xValues,
                datasets: [{
                    backgroundColor: barColors,
                    data: yValues
                }]
            },
            options: {
                title: {
                    display: true,
                    text: "รายการรับ วันที่ {{ $graph_date }}"
                },
                legend: {
                    display: true,
                    position: "bottom"
                }
            }
        });
    </script>
@endsection