@extends('layouts.app-master')
<style type="text/css">
.overlay {
    background-color:#EFEFEF;
    position: fixed;
    width: 100%;
    height: 100%;
    z-index: 1000;
    top: 0px;
    left: 0px;
    opacity: .5; /* in FireFox */ 
    filter: alpha(opacity=50); /* in IE */
}
</style>
@section('content')
<div class="app-content p-md-4">
	<header class="ex-header bg-gray">
        <div class="app-container">
             @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="row justify-content-between">
                <div class="col-auto my-4">  
                    <div class="page-heading row">        
                        <div class="pull-right col-auto">
                            <span class="nav-item">
                                <a class="btn-sm app-btn-secondary" href="{{ route('wh_rm.index') }}">
                                    <i class="fas fa-long-arrow-alt-left"></i>
                                    Back
                                </a>
                            </span>
                        </div>                        
                        <h3 class="col">{{ __('งาน WHRM') }} -> Import</h3>
                    </div>          
                </div> <!-- end of col -->           
            </div> <!-- end of row -->
        </div> <!-- end of container -->

    </header> <!-- end of ex-header -->  
   
    <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
        <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
            aria-controls="orders-all" aria-selected="true"></a>
    </nav>

    <div class="app-container mt-4">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    
                    {{-- <div class="card-header">Questions - Import</div> --}}
                    <div class="card-body">                        
                        <div class="row g-4 settings-section">
	                
                            <div class="col-12 col-md-4">
                                <div class="app-card app-card-settings shadow-sm p-4">
                                    <div class="app-card-body">
                                        <form action="{{ route('wh_rm_import_sap') }}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group mb-4" style="max-width: 500px; margin: 0 auto;">
                                                <h3 class="col">{{ __('ไฟล์ Sap') }}</h3>
                                                <div class="custom-file text-left">
                                                    <input type="file" name="file_sap" class="custom-file-input" id="file_sap">
                                                </div>
                                            
                                                <br>
                                                <button onClick="hide_page()" class="btn btn-success">Import</button>
                                                {{-- <a class="btn btn-warning" href="#">Export</a> --}}
                                                <div class="col-auto" style="float: right; margin-right: 10%; width:50%"><a href="{{ asset('assets/file/sap.xlsx') }}">ตัวอย่างไฟล์..</a></div>
                                            </div>
                                        </form>  
                                    </div><!--//app-card-body--> 
                                </div><!--//app-card-->
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="app-card app-card-settings shadow-sm p-4"> 
                                    <div class="app-card-body">
                                        <form action="{{ route('wh_rm_import_manual') }}" method="POST" enctype="multipart/form-data">
                                            @csrf                                            
                                            <div class="form-group mb-4" style="max-width: 500px; margin: 0 auto;">
                                                <h3 class="col">{{ __('ไฟล์ Manual') }}</h3>                                                
                                                <div class="custom-file text-left">
                                                    <input type="file" name="file_manual" class="custom-file-input" id="file_manual">
                                                </div>
                                            
                                                <br>
                                                <button onClick="hide_page()" class="btn btn-success">Import</button>
                                                {{-- <a class="btn btn-warning" href="{{ route('wh_rm_export_manual') }}">Export</a> --}}
                                                {{-- <div class="col-auto" style="float: right; margin-right: 10%; width:50%"><a href="#">ตัวอย่างไฟล์..</a></div> --}}
                                            </div>
                                        </form>  
                                    </div><!--//app-card-body-->
                                </div><!--//app-card-->
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="app-card app-card-settings shadow-sm p-4"> 
                                    <div class="app-card-body">
                                        <form action="{{ route('wh_rm_import_comment') }}" method="POST" enctype="multipart/form-data">
                                            @csrf                                            
                                            <div class="form-group mb-4" style="max-width: 500px; margin: 0 auto;">
                                                <h3 class="col">{{ __('ไฟล์รูป') }}</h3>                                                
                                                <div class="custom-file text-left">
                                                    <input type="file" name="file_comment" class="custom-file-input" id="file_comment">
                                                </div>
                                            
                                                <br>
                                                <button onClick="hide_page()" class="btn btn-success">Import</button>
                                                <div class="row">
                                                    <div class="col-auto" style="float: right;">ชื่อไฟล์เป็น วัน-เดือน-ปี ค.ศ.-ลำดับรูป</div>
                                                    <div class="custom-file text-left">เช่น 01-06-2022-1</div>                                                    
                                                    <div class="text-danger">หากไม่กำหนดวันที่จะถือว่ารูปเป็นของวันที่ก่อนหน้า</div>
                                                </div>
                                            </div>
                                        </form>  
                                    </div><!--//app-card-body-->
                                </div><!--//app-card-->
                            </div>
                        </div><!--//row-->

                        <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                            <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                                aria-controls="orders-all" aria-selected="true"></a>
                        </nav>

                        <div class="row g-4 settings-section">	
                            @if (count($mail_img)>0)
                                <div style="float: left;">ตัวอย่างรูป</div>
                                <div class="row">
                                    @foreach($mail_img as $key)
                                        <div class="col w-25" style="float: left; margin-right: 10px;">
                                            <img src="{{ url($completedirectory.$key->image) }}"  height='100px'/> 
                                            <a class="btn app-btn-secondary" href="{{ route('wh_rm_img', $key->id) }}" >                                                        
                                                {{ $key->image }} x
                                            </a>  
                                        </div>                                
                                    @endforeach
                                </div> 

                            @endif                

                        </div><!--//row-->

                        {{-- <hr class="my-4">
                        <div class="row g-4 settings-section">
                            <h1 class="app-page-title">ตัวอย่างสำหรับการ Import</h1>
                            <img src="{{asset('/pic/quiz.jpg')}}"  class="photo" width="100" height="250" data-toggle="modal" data-target="#exampleModal">
                        </div><!--//row-->  --}}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"> 
    function hide_page()
    {
        var file_manual = document.getElementById('file_manual').value;
        var file_sap = document.getElementById('file_sap').value;        
        var file_comment = document.getElementById('file_comment').value;
        // console.log(file_name);
        if(file_manual !="" || file_sap !="" || file_comment !=""){
            // console.log('file');
            var div= document.createElement("div");
            div.className += "overlay";
            document.body.appendChild(div);
        }
    }
</script>
@endsection