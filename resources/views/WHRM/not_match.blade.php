@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            <div class="page-heading row">        
                <div class="pull-right col-auto">
                    <a class="btn btn-success" href="{{ route('wh_rm_import_export') }}">Back</a>
                </div>
                <h3 class="col">{{ __('งาน WHRM') }} -> ข้อมูลไม่ถูกต้อง</h3>
            </div>
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>              
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <table class="table app-table-hover mb-0 text-left">
                            <thead>
                                <tr>
                                    <th class="cell"></th>
                                    <th class="cell">รายการ</th>
                                    <th class="cell">บรรทัดที่</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $num_row = 0;
                                @endphp
                                @if(!empty($er['no-row']))
                                    @foreach($er['no-row'] as $key => $value)
                                        <tr>
                                            <td>{{ ++$num_row }}</td>                                                                        
                                            <td>จำนวนบรรทัดของข้อมูล manual ไม่เท่ากับข้อมูล SAP</td>
                                            <td> 
                                                วันที่ {{ $key }} SAP มีข้อมูล {{ $er['no-row'][$key][0] }} บรรทัด Manual มีข้อมูล {{ $er['no-row'][$key][1] }} บรรทัด                                           
                                            </td>
                                        </tr>
                                    @endforeach
                                    
                                @endif 
                                @if(!empty($er['no-data']))
                                    <tr>
                                        <td>{{ ++$num_row }}</td>                                                                        
                                        <td>พบค่าว่างในบางช่อง</td>
                                        <td>  
                                            @foreach ($er['no-data'] as $key=>$value)
                                                {{ ($key+1).', ' }}
                                            @endforeach    
                                        </td>
                                    </tr>
                                @endif
                                @if(!empty($er['no-match']))
                                    <tr>
                                        <td>{{ ++$num_row }}</td>                                                                        
                                        <td>ข้อมูลไม่ตรงกับข้อมูล SAP ที่มี</td>
                                        <td>  
                                            @foreach ($er['no-match'] as $key=>$value)
                                                {{ ($key+1).', ' }}
                                            @endforeach    
                                        </td>
                                    </tr>
                                @endif                                  
                            </tbody>
                        </table>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
@endsection