@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <h1 class="app-page-title mb-0">งาน WHRM </h1>
                </div>
                <div class="col-auto">
                    <div class="page-utilities">
                        <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                            <div class="col-auto">
                                <a class="btn app-btn-secondary" href="{{ route('wh_rm_import_export') }}">
                                    <i class="fas fa-file-upload"></i>&nbsp;
                                    Upload Excel
                                </a>
                                <a class="btn app-btn-secondary" href="{{ route('wh_rm_report') }}">
                                    <i class="fas fa-file-download"></i>&nbsp;
                                    Report
                                </a>
                                <a class="btn app-btn-secondary" href="{{ route('wh_rm_mail') }}">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;
                                    Sent Mail
                                </a>
                                {{-- <a class="btn app-btn-secondary" href="{{ route('wh_act.create') }}">
                                    <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
                                    Add
                                </a> --}}
                            </div>
                        </div><!--//row-->
                    </div><!--//table-utilities-->
                </div><!--//col-auto-->
            </div><!--//row onclick="getURL($key);" -->

            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>

            <div class="col-auto">
                {{-- <form method="GET" action="{{ route('wh_rm.index') }}" accept-charset="UTF-8"
                class="table-search-form row gx-1 align-items-center" role="search"> --}}
                <form method="GET">
                    <div class="form-group row mb-3">
                        <label for="name" class="col-auto form-label">วันที่ </label>
                        <div class="col-auto">
                            <input type="date" class="form-control" id="date_from" name="date_from" @if(!empty($date_from)) value="{{ date("Y-m-d", strtotime($date_from)) }}" @endif>
                        </div>
                        <label for="name" class="col-auto form-label">ถึงวันที่ </label>
                        <div class="col-auto">
                            <input type="date" class="form-control" id="date_to" name="date_to" @if(!empty($date_to)) value="{{ date("Y-m-d", strtotime($date_to)) }}" @endif>
                        </div>
                        {{-- <label for="dep_id" class="col-auto form-label">แผนก</label>
                        <div class="col-auto">
                            <select name="dep_id" class="form-select">
                                <option value=""@if(empty($dep_id)) selected @endif>ไม่ระบุ</option>
                                <option value="PF" @if($dep_id=='PF') selected @endif>PF</option>
                                <option value="PK" @if($dep_id=='PK') selected @endif>PK</option>
                            </select>
                        </div> --}}
                        <div class="col-auto">
                            {{-- <button type="submit" class="btn btn-success">Search</button>
                            <a class="btn btn-warning" href="{{ route('wh_rm_export_manual') }}">Export Manual</a> --}}

                            <input type="button" class="btn btn-success" value="Search" onClick="this.form.action='{{ route('wh_rm.index') }}'; submit()">
                            <input type="button" class="btn btn-warning" value="Export Manual" onClick="this.form.action='{{ route('wh_rm_export_manual') }}'; submit()">
                        </div>
                    </div>
                </form>
            </div><!--//col-->

            <div class="tab-content" id="orders-table-tab-content">
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <table class="table app-table-hover mb-0 text-left">
                            <thead>
                                <tr style="text-align: center">
                                    <th class="cell">วันที่</th>
                                    <th class="cell">No.</th>
                                    {{-- <th class="cell" rowspan="2">วัตถุดิบ</th> --}}
                                    {{-- <th class="cell" rowspan="2">ประเภทการลง</th> --}}
                                    <th class="cell">ทะเบียนรถ</th>
                                    <th class="cell">เวลาเข้า</th>
                                    <th class="cell">ลงเสร็จ</th>
                                    <th class="cell">ใช้เวลา</th>
                                    <th class="cell">จำนวน</th>
                                    <th class="cell">น้ำหนักสุทธิ</th>
                                    <th class="cell">หมายเหตุ</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $check_date = "";
                                @endphp
                                @foreach ($sap_show as $kdate=>$vdate)
                                    @foreach ($vdate as $kno=>$vno)
                                        {{-- @foreach ($vno as $kcar=>$vcar)                     --}}
                                            <tr @if(empty($mn_show[$kdate][$kno]['time_use'])) style="background-color: rgb(247, 138, 138)" @endif>
                                                @if($check_date == "" || $check_date != $kdate)
                                                    <td class="cell" rowspan="{{ count($sap_show[$kdate]) }}" style="vertical-align: top;">
                                                        {{-- <div class="row">
                                                            <div class="col-8">{{ $kdate }}</div>
                                                        </div> --}}
                                                        <div class="row">
                                                            @if(auth()->user()->user_type_id==1)
                                                                <div class="col-4">
                                                                    <a onclick="return confirm(&quot;การลบจะทำให้ข้อมูล sap และ manual หายไป คุณแน่ใจมั้ยว่าต้องการลบรายการของวันที่ {{ $kdate }} ?&quot;)" title="Delete" href="{{ route('wh_rm_del',$kdate) }}">
                                                                        <i class="far fa-trash-alt"></i>
                                                                    </a>
                                                                </div>
                                                            @endif
                                                            <div class="col-8">{{ $kdate }}</div>
                                                        </div>
                                                    </td>
                                                @endif
                                                @php
                                                    $check_date = $kdate;
                                                @endphp
                                                <td class="cell" style="text-align: center">{{ $kno }}</td>
                                                {{-- <td class="cell">
                                                    @if(!empty($sap_show[$kdate][$kno]['rm']))
                                                        {{ $sap_show[$kdate][$kno]['rm'] }}
                                                    @endif
                                                </td>    --}}
                                                {{-- <td class="cell">{{ $mn_show[$kdate][$kno]['rm_type'] }}</td>  --}}
                                                <td class="cell" style="text-align: center">
                                                    @if(!empty($sap_show[$kdate][$kno]['car']))
                                                        {{ $sap_show[$kdate][$kno]['car'] }}
                                                        @if(!empty($mn_show[$kdate][$kno]['car']) && $mn_show[$kdate][$kno]['car'] <> $sap_show[$kdate][$kno]['car'])
                                                            -{{ $mn_show[$kdate][$kno]['car'] }}
                                                        @endif
                                                    @endif
                                                    {{-- {{ $kcar }} --}}
                                                </td>
                                                <td class="cell" style="text-align: center">
                                                    @if(!empty($sap_show[$kdate][$kno]['time_in']))
                                                        {{ date('H:i:s',strtotime($sap_show[$kdate][$kno]['time_in'])) }}
                                                        @if(!empty($mn_show[$kdate][$kno]['time_in']) && date('H:i:s',strtotime($mn_show[$kdate][$kno]['time_in'])) <> date('H:i:s',strtotime($sap_show[$kdate][$kno]['time_in'])))
                                                            -{{ date('H:i:s',strtotime($mn_show[$kdate][$kno]['time_in'])) }}
                                                        @endif
                                                    @endif
                                                    {{-- {{ $sap_show[$kdate][$kno]['time_in'] }}</td>   --}}
                                                <td class="cell" style="text-align: center">
                                                    @if(!empty($mn_show[$kdate][$kno]['time_end']))
                                                        {{ date('H:i:s',strtotime($mn_show[$kdate][$kno]['time_end'])) }}
                                                    @endif
                                                </td>
                                                <td class="cell" style="text-align: center">
                                                    @if(!empty($mn_show[$kdate][$kno]['time_use']))
                                                        {{ date('H:i:s',strtotime($mn_show[$kdate][$kno]['time_use'])) }}
                                                    @endif
                                                </td>
                                                <td class="cell" style="text-align: right">
                                                    @if(!empty($sap_show[$kdate][$kno]['unit_num']))
                                                        {{ $sap_show[$kdate][$kno]['unit_num'].' '.$sap_show[$kdate][$kno]['unit'] }}
                                                    @endif
                                                </td>
                                                <td class="cell" style="text-align: right">
                                                        {{ number_format($sap_show[$kdate][$kno]['rm_weight'],2) }}
                                                </td>
                                                @if(empty($mn_show[$kdate][$kno]['time_use']))
                                                    <td class="cell" style="background-color: rgb(247, 138, 138)">ไม่มีข้อมูล manual</td>
                                                @else
                                                    <td class="cell">
                                                        @if(!empty($mn_show[$kdate][$kno]['note']))
                                                            {{ $mn_show[$kdate][$kno]['note'] }}
                                                        @endif
                                                    </td>
                                                @endif
                                            </tr>
                                        {{-- @endforeach --}}
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div><!--//app-card-body-->
                </div><!--//app-card-->
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
@endsection
