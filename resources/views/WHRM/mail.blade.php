@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-content">
            <div class="app-container-xl">
                @if(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('error') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('success') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <div class="page-heading row">
                    <div class="pull-right col-auto">
                        <span class="nav-item">
                            <a class="btn-sm app-btn-secondary" href="{{ route($path_back) }}">
                                <i class="fas fa-long-arrow-alt-left"></i>
                                Back
                            </a>
                        </span>
                    </div>
                    @if($path_back == 'wh_rm_report')
                        <h3 class="col">{{ __('งาน WHRM') }} -> Report : สรุปการลงวัตถุดิบถั่วแระ</h3>
                    @else
                        <h3 class="col">{{ __('งาน WHRM') }} -> Mail</h3>
                    @endif
                </div>
                <nav id="" class="orders-table-tab app-nav-tabs flex-sm-row mb-4">
                    <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab"
                        href="#" role="tab" aria-controls="orders-all" aria-selected="true"></a>
                </nav>

                @if($path_back == 'wh_rm.index')
                    <form method="GET" action="{{ route('wh_rm_mail') }}" id="myForm" accept-charset="UTF-8" role="search">
                        <div class="row mx-1">
                            <label for="name" class="col-auto form-label my-2">วันที่ </label>
                            <input type="date" class="form-control col" id="date_mail" name="date_mail"
                                value="@if(empty(Request::get('date_mail'))){{ date('Y-m-d') }}@else{{ Request::get('date_mail') }}@endif">

                            <span class="input-group-append col">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        <br/>
                    </form>
                @endif
                <div class="tab-content" id="orders-table-tab-content">
                    <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
                        <div class="app-card app-card-orders-table shadow-sm mb-5">
                            <div class="app-card-body">
                                <div class="table-responsive">
                                    <table class="table table-borderless" id="tb_show">
                                        @if($path_back != 'wh_rm_report')
                                            <tr>
                                                <th class="cell" style="width: 5%">ตัวอย่าง</th>
                                                <th class="cell" style="text-align:left; width: 5%">TO..ALL</th>
                                                <th class="cell" style="width: 5%"></th>
                                                <th class="cell" style="text-align: center" style="width: 45%"></th>
                                                <th class="cell" style="width: 25%"></th>
                                                <th class="cell" style="width: 15%">
                                                    <a class="btn app-btn-secondary" onClick="return confirm('คุณต้องการส่งเมลล์ใช่รือไม่ ?');" href="{{ route('wh_rm_sent_mail',$sent_date) }}" >
                                                        <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;
                                                        ส่งเมลล์
                                                    </a>
                                                </th>
                                            </tr>
                                        @endif
                                        <tr>
                                            <th class="cell" style="width: 5%"></th>
                                            <th class="cell" colspan="5">สรุปการลงวัตถุดิบถั่วแระ {{ $crop }} รับเข้าวันที่ {{ $format_setdate }}</th>
                                            {{-- <th class="cell" style="width: 15%">
                                                @if($att_file != '')
                                                    <a class="btn app-btn-secondary" href="{{ asset('storage/app/public/WhRM/completes/problem/'.$att_file) }}" >
                                                        ไฟล์แนบ
                                                    </a>
                                                    @if($path_back != 'wh_rm_report')
                                                        <a class="btn app-btn-secondary" href="{{ route('wh_rm_file', $format_setdate) }}" >
                                                            x
                                                        </a>
                                                    @endif
                                                @endif
                                            </th> --}}
                                        </tr>
                                        @php
                                            $col_color = ['#f6bc96', '#b1d3f5', '#847a8a'];
                                            $col_rang = ['< 10 นาที', '< 15 นาที', '< 20 นาที', '> 20 นาที'];
                                            $tr = ['พนักงานยก', 'เครื่องเรียงกระสอบ'];
                                            $tr_data = ['พนักงานยก', 'เครื่องเรียง'];
                                        @endphp
                                        @if(count($to_report)>0)
                                            @if(!empty($to_report['weight'][1]))
                                                <tr>
                                                    <th class="cell" style="width: 5%"></th>
                                                    <th class="cell" colspan="5">1. RM รับเข้าทั้งหมด {{ number_format($to_report['weight'][1],2) }} Kg.</th>
                                                </tr>
                                            @endif
                                            @if(!empty($to_report['car']))
                                                <tr>
                                                    <th class="cell"></th>
                                                    <th class="cell" colspan="5">2. รถขนส่งเข้าโรงงานทั้งหมด {{ count($to_report['car']) }} คัน</th>
                                                </tr>
                                            @endif
                                            @if(!empty($to_report['time']['min']))
                                                <tr>
                                                    <th class="cell" colspan="2"></th>
                                                    <th class="cell" colspan="4">- คันแรกถึง LACO เวลา {{ date('H:i:s',strtotime($to_report['time']['min'])) }} น.</th>
                                                </tr>
                                            @endif
                                            @if(!empty($to_report['time']['max']))
                                                <tr>
                                                    <th class="cell" colspan="2"></th>
                                                    <th class="cell" colspan="4">- คันสุดท้ายถึง LACO เวลา {{ date('H:i:s',strtotime($to_report['time']['max'])) }} น. </th>
                                                </tr>
                                            @endif
                                            @if(!empty($to_report['time']['end_work']))
                                                <tr>
                                                    <th class="cell" colspan="2"></th>
                                                    <th class="cell" colspan="4">- ลงวัตถุดิบคันสุดท้ายเสร็จเวลา {{ date('H:i:s',strtotime($to_report['time']['end_work'])) }} น.</th>
                                                </tr>
                                            @endif
                                            <tr>
                                                <th class="cell" colspan="2"></th>
                                                <th class="cell" colspan="4">ช่วงเวลารถเข้า</th>
                                            </tr>
                                            <tr>
                                                <th class="cell" colspan="2"></th>
                                                <th class="cell" colspan="4">
                                                    <table class="table table-bordered border-dark-3" style="width: 50%">
                                                        <thead>
                                                            <tr style="text-align: center">
                                                                <th class="cell" style="background-color:#b1d3f5">เวลา</th>
                                                                <th class="cell" style="background-color:#b1d3f5">วันที่ {{ $format_setdate }}</th>
                                                                <th class="cell" style="background-color:#b1d3f5">น้ำหนักวัตถุดิบสุทธิ</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if(!empty($to_report['range']))
                                                                @foreach ($to_report['range'] as $ktime=>$vtime)
                                                                    <tr>
                                                                        <td class="cell">{{ $ktime }}</td>
                                                                        <td class="cell" style="text-align: right;">{{ $vtime }}</td>
                                                                        <td class="cell" style="text-align: right;">{{ number_format($to_report['range_weight'][$ktime],2) }}</td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                            <tr style="text-align: right; background-color:#b1d3f5">
                                                                <td class="cell">Grand Total</td>
                                                                <td class="cell">{{ array_sum($to_report['range']) }}</td>
                                                                <td class="cell">{{ number_format(array_sum($to_report['range_weight']),2) }}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th class="cell" colspan="2"></th>
                                                <th class="cell" colspan="4">สรุปการลงวัตถุดิบ</th>
                                            </tr>
                                            <tr>
                                                <th class="cell" colspan="2"></th>
                                                <th class="cell" colspan="4">
                                                    <table class="table table-bordered border-dark-3" style="width: 100%">
                                                        <thead>
                                                            <tr style="text-align: center;" >
                                                                <th class="cell" style="background-color:#f5e4d9" rowspan="3">วันที่ </th>
                                                                <th class="cell" style="background-color:#60c93c" colspan="12">ประเภทการลงวัตถุดิบ</th>
                                                            </tr>
                                                            <tr>
                                                                @php
                                                                    $i=0;
                                                                @endphp
                                                                @foreach ($tr as $ktr=>$vtr)
                                                                    <th class="cell" style="text-align: center; background-color:{{ $col_color[$i] }}" colspan="4">{{ $vtr }}</th>
                                                                    @php
                                                                        $i++;
                                                                    @endphp
                                                                @endforeach
                                                                <th class="cell" style="text-align: center; background-color:#847a8a" colspan="4">total</th>
                                                            </tr>
                                                            <tr>
                                                                @for($i=0;$i<3;$i++)
                                                                    @foreach ($col_rang as $key=>$val)
                                                                        <th class="cell" style="background-color:{{ $col_color[$i] }}">{{ $val }}</th>
                                                                    @endforeach
                                                                @endfor
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @php
                                                                $i=0;
                                                                foreach ($col_rang as $key=>$val) {
                                                                    $sum_tr[$val] = 0;
                                                                }
                                                            @endphp
                                                            <tr>
                                                                <th class="cell">{{ $format_setdate }}</th>
                                                                @foreach ($tr_data as $ktr=>$vtr)
                                                                    @foreach ($col_rang as $key=>$val)
                                                                        <th class="cell" style="text-align: right;">@if(!empty($tb_receipt[$vtr][$val])){{ $tb_receipt[$vtr][$val] }}@else{{ 0 }}@endif</th>
                                                                        @php
                                                                            if(!empty($tb_receipt[$vtr][$val]))
                                                                                $sum_tr[$val] += $tb_receipt[$vtr][$val];
                                                                        @endphp
                                                                    @endforeach
                                                                    @php
                                                                        $i++;
                                                                    @endphp
                                                                @endforeach
                                                                @foreach ($col_rang as $key=>$val)
                                                                    <th class="cell" style="text-align: right;">{{ $sum_tr[$val] }}</th>
                                                                @endforeach
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th class="cell" colspan="2"></th>
                                                <th class="cell" colspan="4">
                                                    <canvas id="myChart1" style="width:100%;max-width:600px"></canvas>
                                                </th>
                                            </tr>
                                            @if(count($mail_problem)>0 || count($show_img)>0)
                                                <tr>
                                                    <th class="cell" colspan="1"></th>
                                                    <th class="cell" colspan="5">ปัญหาที่พบ</th>
                                                </tr>
                                                {{-- @php
                                                    $pb_sort = 1;
                                                @endphp --}}
                                                @if(count($mail_problem)>0)
                                                    <tr>
                                                        <th class="cell" colspan="2"></th>
                                                        <th class="cell" colspan="4">1. น้ำหนักค่าเฉลี่ย/กระสอบเกิน 18.00 Kg</th>
                                                    </tr>
                                                    <tr>
                                                        <th class="cell" colspan="2"></th>
                                                        <th class="cell" colspan="4">
                                                            <table class="table table-bordered border-dark-3" style="width: 100%">
                                                                <thead>
                                                                    <tr style="text-align: center; background-color:#f3d5c2">
                                                                        <th class="cell">ชื่อวัตถุดิบ </th>
                                                                        <th class="cell">Broker Name</th>
                                                                        <th class="cell">Farmer name</th>
                                                                        <th class="cell">จำนวน</th>
                                                                        {{-- <th class="cell">ภาชนะ</th> --}}
                                                                        <th class="cell">นน.สุทธิ</th>
                                                                        <th class="cell">Weight Package Net</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach ($mail_problem as $key=>$value)
                                                                        <tr>
                                                                            @php
                                                                                $ep_pb = explode('@;', $value);
                                                                            @endphp
                                                                            @foreach ($ep_pb as $kid=>$vid)
                                                                                <th class="cell" @if($kid>=3)style="text-align: right;"@endif>@if($kid==4||$kid==5){{ number_format($vid,2) }}@else{{ $vid }}@endif</th>
                                                                            @endforeach
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                @endif
                                                @if(count($show_img)>0)
                                                    @foreach($show_img[$format_setdate] as $key => $value)
                                                        <tr>
                                                            <th class="cell" colspan="2" style="text-align:right; vertical-align: top">
                                                                @if($path_back != 'wh_rm_report')
                                                                    <a class="text-danger" href="{{ route('wh_rm_img', $key) }}" >x</a>
                                                                @endif
                                                            </th>
                                                            {{-- <th class="cell" colspan="4"><img src="{{ $value }}" width="60%"/></th> --}}
                                                            <th class="cell" colspan="4">
                                                                @php
                                                                    $exp = explode('/', $value);
                                                                @endphp
                                                                <img src="{{ route('show_img', array('path_1' => $exp[0], 'path_2' => $exp[1])) }}" width="60%"/>
                                                            </th>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            @endif
                                        @endif
                                    </table>
                                </div><!--//table-responsive-->
                            </div><!--//app-card-body-->
                        </div><!--//app-card-->
                    </div><!--//tab-pane-->
                </div><!--//tab-content-->
            </div><!--//container-fluid-->
        </div><!--//app-content-->

    </div><!--//app-wrapper-->
    <script src="{{ asset('assets/js/Chart.js') }}"></script>
    <script>
        const chk_arr1 = <?php echo json_encode($tb_receipt_gp); ?>;
        console.log(chk_arr1);
        new Chart("myChart1", {
            type: 'bar',
            data: {
                labels: <?php echo json_encode($col_rang); ?>,
                datasets: [
                    {
                        label: 'พนักงานยก',
                        data: chk_arr1['พนักงานยก'],
                        // data: [14,0,0,0],
                        borderColor: '#000000',
                        backgroundColor: '#B06126',
                    },
                    {
                        label: 'เครื่องเรียง',
                        data: chk_arr1['เครื่องเรียง'],
                        borderColor: '#000000',
                        backgroundColor: '#4171a1',
                    },
                    {
                        label: 'total',
                        data: chk_arr1['total'],
                        borderColor: '#000000',
                        backgroundColor: '#4d7b52',
                    }
                ]
            },
            options: {
                responsive: true,
                legend: {
                    display: true, //show legend at the top of a chart
                    position: 'bottom'
                },
            }
        });
    </script>
@endsection
