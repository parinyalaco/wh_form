@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="page-heading row">        
                <div class="pull-right col-auto">
                    <a class="btn btn-success" href="{{ route('st_stock.index') }}">Back</a>
                </div>
                <h3 class="col">{{ __('Set คงคลัง') }} -> Create</h3>
            </div>
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>  
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <form method="POST" action="{{ route('st_stock.store') }}">
                            @csrf
                            <div class="form-group row mb-2">
                                <label for="st_product_id" class="col-md-2 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <select name="st_product_id" class="form-select" required>
                                        <option value="">ไม่ระบุ</option>
                                        @foreach ($st_pd as $key)
                                            <option value="{{ $key->id }}" @if(old('st_product_id')==$key->id) selected @endif>{{ $key->name }}</option>
                                        @endforeach
                                    </select>

                                    @if($errors->has('st_product_id'))
                                        <div class="error">{{ $errors->first('st_product_id') }}</div>
                                    @endif

                                    {{-- @error('st_product_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror --}}
                                </div>
                            </div>   

                            <div class="form-group row mb-2">
                                <label for="amount" class="col-md-2 col-form-label text-md-right">{{ __('จำนวนคงคลัง') }}</label>

                                <div class="col-md-6">
                                    <input type='number' id="amount" class="form-control" name="amount" value="" required>

                                    @if($errors->has('amount'))
                                        <div class="error" role="alert">{{ $errors->first('amount') }}</div>
                                    @endif
                                    {{-- @error('amount')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror --}}
                                </div>
                            </div> 

                            <div class="form-group row mb-2">
                                <label for="start_date" class="col-md-2 col-form-label text-md-right">{{ __('เริ่มต้นใช้') }}</label>

                                <div class="col-md-6">
                                    <input type='date' id="start_date" class="form-control" name="start_date" value="" required>
                                    
                                    @if($errors->has('start_date'))
                                        <div class="error" role="alert">{{ $errors->first('start_date') }}</div>
                                    @endif
                                    {{-- @error('start_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror --}}
                                </div>
                            </div> 

                            <div class="form-group row mb-2">
                                <div class="col-md-6 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->        
@endsection
