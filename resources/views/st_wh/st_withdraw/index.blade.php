@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="page-heading row justify-content-between">        
                <div class="pull-right col-auto">
                    <div class="row">
                        <div class="pull-right col-auto">
                            <a class="btn btn-success" href="{{ route('st_withdraw_stock') }}">Back</a>
                        </div>
                        <div class="col-auto">
                            <h1 class="app-page-title mb-0">{{ $tran['type_show'] }} -> {{ $st_bk[0]['bk_name'] }} : {{ $st_bk[0]['pd_name'] }}</h1>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-auto">
                    <h1 class="app-page-title mb-0">รับเข้า -> {{ $st_wh[0]['name'] }}</h1>
                </div> --}}
                <div class="col-auto">
                    <div class="page-utilities">
                        <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                            <div class="col-auto">
                                @if($tran['type'] != 'detail')
                                    <a class="btn app-btn-secondary" href="{{ url('/st_wh/st_withdraw/create/'.$tran['type'].'/'.$st_bk[0]['id']) }}">
                                        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
                                        Add
                                    </a>
                                 @endif
                            </div>
                        </div><!--//row-->
                    </div><!--//table-utilities-->
                </div><!--//col-auto-->
            </div>
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>             
            
            <div class="col-auto">
                <form method="GET">
                    <div class="form-group row mb-3">
                        <label for="bk_id" class="col-auto form-label">Crop</label>
                        <div class="col-auto">
                            <select id="crop_id" name="crop_id" class="form-select">
                                <option value="" selected>==== เลือก Crop ====</option>
                                @foreach($crop as $key=>$value)
                                    <option value="{{ $key }}" @if(Request::get('crop_id')==$key) selected @endif>{{ $value }}</option>
                                @endforeach                                                            
                            </select>											
                        </div>                      
                        <label for="name" class="col-auto form-label">วันที่ </label>
                        <div class="col-auto">
                            <input type="date" class="form-control" id="date_from" name="date_from" @if(!empty(Request::get('date_from'))) value="{{ date("Y-m-d", strtotime(Request::get('date_from'))) }}" @endif>											
                        </div>
                        <label for="name" class="col-auto form-label">ถึงวันที่ </label>
                        <div class="col-auto">
                            <input type="date" class="form-control" id="date_to" name="date_to" @if(!empty(Request::get('date_to'))) value="{{ date("Y-m-d", strtotime(Request::get('date_from'))) }}" @endif>											
                        </div>
                        <div class="col-auto">
                            <input type="button" class="btn btn-success" value="Search" 
                                onClick="this.form.action='{{ route('st_withdraw_index',['type' => $tran['type'], 'id' => $st_bk[0]['id']]) }}'; submit()">
                        </div>
                    </div>
                </form>                
            </div><!--//col-->

            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <table class="table app-table-hover mb-0 text-left">
                            <thead>
                                <tr>
                                    <th class="cell">No.</th>
                                    <th class="cell">Crop</th>
                                    <th class="cell">เลขที่เอกสาร</th>
                                    <th class="cell">วันที่</th>
                                    <th class="cell">จำนวน</th>    
                                    <th class="cell"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($st_tran as $key)
                                    <tr>                                             
                                        <td class="cell">{{ $loop->iteration }}</td>
                                        <td class="cell">@if(!empty($key->crop_id)){{ $crop[$key->crop_id] }}@endif</td>
                                        <td class="cell">{{ $key->no_id }}</td>
                                        <td class="cell">{{ date('d/m/Y',strtotime($key->tran_date)) }}</td>
                                        <td class="cell">{{ number_format($key->amount) }}</td>
                                        <td class="row">
                                            @if($tran['type'] != 'detail')
                                                <div class="col-auto mx-2">
                                                    <a class="btn btn-primary" href="{{ url('/st_wh/st_withdraw/edit/'.$tran['type'].'/'.$key->id) }}">Edit</a>
                                                </div>
                                                <div class="col-auto">
                                                    <a class="btn btn-danger" href="{{ url('/st_wh/st_withdraw/del/'.$tran['type'].'/'.$key->id) }}">Delete</a>

                                                    {{-- <form action="{{ route('st_receive.destroy', $key->id) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                    </form> --}}
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
                       
        </div><!--//container-fluid-->
    </div><!--//app-content-->
@endsection