@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-content">
            <div class="app-container-xl">
                {{-- @if(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('error') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ Session::get('success') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif --}}
                <div class="page-heading row"> 
                    <div class="pull-right col-auto">
                        <span class="nav-item">
                            <a class="btn-sm app-btn-secondary" href="{{ route('st_report_view') }}">
                                <i class="fas fa-long-arrow-alt-left"></i>
                                Back
                            </a>
                        </span>
                    </div>
                    {{-- @if($path_back == 'wh_rm_report') --}}
                        <h3 class="col">{{ __('งาน WHRM') }} -> Report : รายงาน Stock {{ $pd_name }} ประจำวันที่ {{ $th_date }}</h3>
                    {{-- @else
                        <h3 class="col">{{ __('งาน WHRM') }} -> Mail</h3>
                    @endif --}}
                </div>
                <nav id="" class="orders-table-tab app-nav-tabs flex-sm-row mb-4">	
                    <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" 
                        href="#" role="tab" aria-controls="orders-all" aria-selected="true"></a>
                </nav>			   
                
                <div class="tab-content" id="orders-table-tab-content">
                    <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
                        <div class="app-card app-card-orders-table shadow-sm mb-5">
                            <div class="app-card-body">
                                <div class="table-responsive">
                                    <table class="table table-borderless" id="tb_show">
                                        <tr>
                                            <th class="cell" style="width: 5%"></th>
                                            <th class="cell" colspan="5">Stock {{ $pd_name }}</th>          
                                        </tr>
                                        <tr>
                                            <th class="cell" colspan="2"></th>
                                            <th class="cell" colspan="4">
                                                <table class="table table-bordered border-dark-3" style="width: 25%">
                                                    <tr>
                                                        <td class="cell" style="background-color:#f7be5c">ยอดกระสอบ</td>
                                                        <td class="cell" style="text-align:right">@if(!empty($tbl_1['bg_amount'])){{ number_format($tbl_1['bg_amount']) }}@endif</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="cell" style="background-color:#f7be5c">จำนวนเบิก/BK</td>
                                                        <td class="cell" style="text-align:right">@if(!empty($tbl_1['wh_td'])){{ number_format($tbl_1['wh_td']) }}@endif</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="cell" style="background-color:#f7be5c">จำนวนคืน/BK</td>
                                                        <td class="cell" style="text-align:right">@if(!empty($tbl_1['rt_td'])){{ number_format($tbl_1['rt_td']) }}@endif</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="cell" style="background-color:#f7be5c">คงเหลื่อ LACO</td>
                                                        <td class="cell" style="text-align:right">@if(!empty($tbl_1['total'])){{ number_format($tbl_1['total']) }}@endif</td>
                                                    </tr>
                                                </table>
                                            </th>                                                    
                                        </tr>
                                        <tr>
                                            <th class="cell" colspan="2"></th>
                                            <th class="cell" colspan="4">
                                                <canvas id="myChart1" style="width:100%;max-width:600px"></canvas>
                                            </th>                                                    
                                        </tr>
                                        @if(count($set_amount)>0)
                                            <tr>
                                                <th class="cell" colspan="2"></th>
                                                <th class="cell" colspan="4">ปริมาณการใช้ {{ $pd_name }} วันที่ {{ $th_date }}</th>                                                    
                                            </tr>
                                            <tr>
                                                <th class="cell" colspan="6">
                                                    <table class="table table-bordered border-dark-3" style="width: 100%">
                                                        <thead>
                                                            <tr style="text-align: center; background-color:#f3d5c2">
                                                                <th class="cell">ลำดับที่ </th>
                                                                <th class="cell">เขต </th>
                                                                <th class="cell">CODE </th>
                                                                <th class="cell">ชื่อหัวหน้ากลุ่มเกษตรกร</th>
                                                                <th class="cell">จำนวนเบิก</th>
                                                                <th class="cell">จำนวนคืน</th>
                                                                <th class="cell">ปริมาณการใช้/วัน</th>
                                                                <th class="cell">คงเหลือ BK</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>                                                            
                                                            @foreach ($set_amount as $key=>$value)
                                                                <tr>
                                                                    <td class="cell" style="text-align:center">{{ $loop->iteration }}</td>
                                                                    <td class="cell"></td>
                                                                    <td class="cell"></td>
                                                                    <td class="cell">@if(!empty($set_bk[$key])){{ $set_bk[$key] }}@endif</td>
                                                                    <td class="cell" style="text-align: right">@if(!empty($wd_bk[$key])){{ number_format($wd_bk[$key]) }}@endif</td>
                                                                    <td class="cell"></td>
                                                                    <td class="cell" style="text-align: right">@if(!empty($rt_bk[$key])){{ number_format($rt_bk[$key]) }}@endif</td>
                                                                    @php
                                                                        $total[$key] = $set_amount[$key]['st']-$set_amount[$key]['wd']+$set_amount[$key]['rt'];
                                                                    @endphp
                                                                    <td class="cell" style="text-align: right">@if(!empty($total[$key])){{ number_format($total[$key]) }}@endif</td>                                                              
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                        <tfoot>
                                                            <tr style="background-color: yellow">
                                                                <td class="cell" colspan="4" style="text-align: right">รวม</td>
                                                                <td class="cell" style="text-align: right">@if(!empty($wd_bk)){{ number_format(array_sum($wd_bk)) }}@endif</td>
                                                                <td class="cell"></td>
                                                                <td class="cell" style="text-align: right">@if(!empty($rt_bk)){{ number_format(array_sum($rt_bk)) }}@endif</td>
                                                                <td class="cell" style="text-align: right">@if(!empty($total)){{ number_format(array_sum($total)) }}@endif</td>                                                              
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </th>                                                   
                                            </tr> 
                                        @endif                                                
                                    </table>
                                </div><!--//table-responsive-->                            
                            </div><!--//app-card-body-->          
                        </div><!--//app-card-->                    
                    </div><!--//tab-pane-->			        
                </div><!--//tab-content-->
            </div><!--//container-fluid-->
        </div><!--//app-content-->	    
        
    </div><!--//app-wrapper-->
    <script src="{{ asset('assets/js/Chart.js') }}"></script>
    <script>  
        const chk_arr1 = <?php echo json_encode($val_rang); ?>; 
        const labels = <?php echo json_encode($col_rang); ?>;
        
        const data = {
            labels: labels,
            datasets: [{
                label: 'My First Dataset',
                data: chk_arr1,
                backgroundColor: [
                    '#2BB92B',
                    '#2BB92B',
                    '#2BB92B',
                    '#2BB92B'
                ],
                borderColor: [
                    '#000000',
                    '#000000',
                    '#000000',
                    '#000000'
                ],
                borderWidth: 1
            }]
        };

        console.log(chk_arr1);
        new Chart("myChart1", {
            type: 'bar',
            data: data,
            options: {
                responsive: true,
                legend: {
                    display: true, //show legend at the top of a chart
                    position: 'bottom'
                },
            }
        });
    </script>
@endsection