@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-content">
            <div class="app-container-xl">
                <div class="page-heading row"> 
                    <h3 class="col">{{ __('Stock') }} -> Report</h3>
                </div>
                <nav id="" class="orders-table-tab app-nav-tabs flex-sm-row mb-4">	
                    <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" 
                        href="#" role="tab" aria-controls="orders-all" aria-selected="true"></a>
                </nav>			   
                
                <div class="tab-content" id="orders-table-tab-content">
                    <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
                        <div class="app-card app-card-orders-table shadow-sm mb-5">
                            
                            <form method="GET" action="{{ route('st_report_show') }}" accept-charset="UTF-8"
                                class="table-search-form row gx-1 align-items-center" role="search">
                                <div class="app-card-body">
                                    <div class="table-responsive">
                                        <table class="table app-table-hover mb-0 text-left yajra-datatable" id="tb_show">
                                            <thead>
                                                <tr>
                                                    <th class="cell" style="width: 5%"></th>
                                                    <th class="cell" style="text-align:right; width: 20%;">ประเภทรายงาน : </th>
                                                    <th class="cell" colspan="3" style="width: 65%">
                                                        <select name="report_id" class="form-select col" onchange="myFunction(this.value)" required>
                                                            <option value="" selected>==== เลือกประเภทรายงาน ====</option>
                                                            <option value="1">1. รายการเคลื่อนไหว Stock ตามช่วงเวลา</option>
                                                            <option value="2">2. รายการเคลื่อนไหว Broker ตามช่วงเวลา</option>
                                                            <option value="3">3. รายงาน Stock ประจำวัน </option>
                                                            {{-- <option value="3">3. รายการไม่</option> --}}
                                                        </select>
                                                    </th>
                                                    <th class="cell"style="width: 10%"></th>
                                                </tr>
                                                <tr>
                                                    <th class="cell" style="width: 5%"></th>
                                                    <th class="cell" style="text-align:right; width: 20%">วันที่ : </th>
                                                    <th class="cell" style="width: 30%">
                                                        <input type="date" class="form-control col" id="st_date" name="st_date" value="{{ date("Y-m-d") }}">
                                                        <input type="month" class="form-control col" id="month_date" name="month_date" value="{{ date("Y-m") }}" style="display: none">
                                                    </th>
                                                    <th class="cell" style="text-align: center" style="width: 6%">
                                                        <div id="date_to">
                                                            ถึงวันที่ : 
                                                        </div>
                                                    </th>
                                                    <th class="cell" style="width: 29%">
                                                        <input type="date" class="form-control col" id="ed_date" name="ed_date" value="{{ date("Y-m-d") }}">
                                                    </th>
                                                    <th class="cell" style="width: 10%"></th>
                                                </tr>
                                                <tr id="tr_bk">
                                                    <th class="cell" style="width: 5%"></th>
                                                    <th class="cell" style="text-align:right; width: 20%"><div id="bk_name">Broker : </div></th>
                                                    <th class="cell" style="width: 30%">
                                                        <select id="bk_id" name="bk_id" class="form-select">
                                                            <option value="" selected>==== เลือก Broker ====</option>
                                                            @foreach($bk as $key)
                                                                <option value="{{ $key->id }}">{{ $key->name }}</option>
                                                            @endforeach                                                            
                                                        </select>
                                                    </th>
                                                    <th class="cell" colspan="3" style="width: 45%"></th>
                                                </tr>
                                                <tr id="tr_pd">
                                                    <th class="cell" style="width: 5%"></th>
                                                    <th class="cell" style="text-align:right; width: 20%"><div id="pd_name">Stock : </div></th>
                                                    <th class="cell" style="width: 30%">
                                                        <select id="pd_id" name="pd_id" class="form-select">
                                                            <option value="" selected>==== เลือก Stock ====</option>
                                                            @foreach($pd as $key)
                                                                <option value="{{ $key->id }}">{{ $key->name }}</option>
                                                            @endforeach                                                            
                                                        </select>
                                                    </th>
                                                    <th class="cell" colspan="3" style="width: 45%"></th>
                                                </tr>
                                                <tr>
                                                    <th class="cell"></th>
                                                    <th class="cell"></th>
                                                    <th class="cell">
                                                        <button class="btn app-btn-primary" type="submit">
                                                            Report
                                                        </button>
                                                    </th>
                                                    <th class="cell"></th>
                                                    <th class="cell">
                                                    </th>
                                                    <th class="cell"></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div><!--//table-responsive-->
                                
                                </div><!--//app-card-body-->                           
                            </form>
                                    
                        </div><!--//app-card-->                    
                    </div><!--//tab-pane-->			        
                </div><!--//tab-content--> 
            </div><!--//container-fluid-->
        </div><!--//app-content-->	 
    </div><!--//app-wrapper-->  
    <script type="text/javascript"> 
        function myFunction(a) {        
            console.log(a);
            if(a=='1'){
                document.getElementById('st_date').style.display = "block";
                document.getElementById('st_date').required = true;
                document.getElementById('date_to').style.display = "block";
                document.getElementById('ed_date').style.display = "block";                
                document.getElementById('ed_date').required = true;
                document.getElementById('month_date').style.display = "none";              
                document.getElementById('bk_id').style.display = "none";                
                document.getElementById('bk_id').required = false;              
                document.getElementById('bk_name').style.display = "none";         
                document.getElementById('pd_id').style.display = "none";                
                document.getElementById('pd_id').required = false;              
                document.getElementById('pd_name').style.display = "none";
            }else if(a=='2'){
                document.getElementById('st_date').style.display = "block";
                document.getElementById('st_date').required = true;
                document.getElementById('date_to').style.display = "block";
                document.getElementById('ed_date').style.display = "block";               
                document.getElementById('ed_date').required = true;
                document.getElementById('month_date').style.display = "none";              
                document.getElementById('bk_id').style.display = "block";                  
                document.getElementById('bk_id').required = false;               
                document.getElementById('bk_name').style.display = "block";         
                document.getElementById('pd_id').style.display = "block";                
                document.getElementById('pd_id').required = false;               
                document.getElementById('pd_name').style.display = "block";                
            }else if(a=='3'){
                document.getElementById('st_date').style.display = "block";
                document.getElementById('st_date').required = true;
                document.getElementById('date_to').style.display = "none";
                document.getElementById('ed_date').style.display = "none";            
                document.getElementById('ed_date').required = false;
                document.getElementById('month_date').style.display = "none";           
                document.getElementById('bk_id').style.display = "none";                
                document.getElementById('bk_id').required = false;              
                document.getElementById('bk_name').style.display = "none";         
                document.getElementById('pd_id').style.display = "block";                
                document.getElementById('pd_id').required = false;               
                document.getElementById('pd_name').style.display = "block";  
            }else{
                document.getElementById('st_date').style.display = "block";
                document.getElementById('st_date').required = true;
                document.getElementById('date_to').style.display = "block";
                document.getElementById('ed_date').style.display = "block";              
                document.getElementById('ed_date').required = true;
                document.getElementById('month_date').style.display = "none";              
                document.getElementById('bk_id').style.display = "none";                  
                document.getElementById('bk_id').required = false;                  
                document.getElementById('bk_name').style.display = "none";         
                document.getElementById('pd_id').style.display = "none";                
                document.getElementById('pd_id').required = false;                     
                document.getElementById('pd_name').style.display = "none";
            }
        }
    </script>  
@endsection