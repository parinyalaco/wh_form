@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <h1 class="app-page-title mb-0">Broker</h1>
                </div>                
            </div><!--//row onclick="getURL($key);" -->
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>             
            
            <div class="col-auto">
                <form method="GET">
                    <div class="form-group row mb-3">
                        <label for="bk_id" class="col-auto form-label">Broker</label>
                        <div class="col-auto">
                            <select name="bk_id" class="form-select">
                                <option value=""@if(empty($bk_id)) selected @endif>ไม่ระบุ</option>
                                @foreach ($bk as $key)
                                    <option value="{{ $key->id }}" @if($bk_id==$key->id) selected @endif>{{ $key->name }}</option>                                    
                                @endforeach
                            </select>											
                        </div>
                        <label for="pd_id" class="col-auto form-label">สินค้า</label>
                        <div class="col-auto">
                            <select name="pd_id" class="form-select">
                                <option value=""@if(empty($pd_id)) selected @endif>ไม่ระบุ</option>                                
                                @foreach ($pd as $key)
                                    <option value="{{ $key->id }}" @if($pd_id==$key->id) selected @endif>{{ $key->name }}</option>                                    
                                @endforeach
                            </select>											
                        </div>
                        <div class="col-auto">
                            <input type="button" class="btn btn-success" value="Search" onClick="this.form.action='{{ route('st_withdraw_stock') }}'; submit()">
                        </div>
                    </div>
                </form>                
            </div><!--//col-->

            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <table class="table app-table-hover mb-0 text-left">
                            <thead>
                                <tr>
                                    <th class="cell">No.</th>
                                    <th class="cell">Broker</th>
                                    <th class="cell">สินค้า</th>
                                    <th class="cell">จำนวน</th>    
                                    <th class="cell"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no_row = 0;
                                @endphp
                                @foreach ($sum_bk as $kbk=>$vbk)
                                    @foreach ($vbk as $kpd=>$vpd)
                                        <tr>                                             
                                            <td class="cell">{{ ++$no_row }}</td>
                                            <td class="cell">{{ $kbk }}</td>
                                            <td class="cell">{{ $kpd }}</td>
                                            <td class="cell">{{ number_format($sum_bk[$kbk][$kpd]['tt']) }}</td>
                                            <td class="row">
                                                <div class="col-auto mx-2">
                                                    <a class="btn btn-primary" href="{{ url('/st_wh/st_withdraw/index/withdraw/'.$sum_bk[$kbk][$kpd]['id']) }}">เบิก</a>
                                                </div>   
                                                <div class="col-auto mx-2">
                                                    <a class="btn btn-warning" href="{{ url('/st_wh/st_withdraw/index/return/'.$sum_bk[$kbk][$kpd]['id']) }}">คืน</a>
                                                </div>
                                                <div class="col-auto mx-2">
                                                    <a class="btn btn-info" href="{{ route('st_withdraw_show', $sum_bk[$kbk][$kpd]['id']) }}">รายละเอียด</a>
                                                </div> 
                                                {{-- <div class="col-auto mx-2">
                                                    <a class="btn btn-danger" href="{{ url('/st_wh/st_withdraw/change/'.$sum_bk[$kbk][$kpd]['id']) }}">เปลี่ยน</a>
                                                </div>   --}}
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>                          
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->

            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <h1 class="app-page-title mb-0">รายการที่ไม่สามารถจับกลุ่มกับข้อมูล Broker ด้านบนได้</h1>
                </div>                
            </div><!--//row onclick="getURL($key);" -->
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <table class="table app-table-hover mb-0 text-left">
                            <thead>
                                <tr>
                                    <th class="cell">No.</th>
                                    <th class="cell">Broker</th>
                                    <th class="cell">สินค้า</th>
                                    <th class="cell">จำนวน</th>    
                                    <th class="cell"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no_row = 0;
                                @endphp
                                @foreach ($sum_rt as $kbk=>$vbk)
                                    @foreach ($vbk as $kpd=>$vpd)
                                        <tr>                                             
                                            <td class="cell">{{ ++$no_row }}</td>
                                            <td class="cell">{{ $kbk }}</td>
                                            <td class="cell">{{ $kpd }}</td>
                                            <td class="cell">{{ $sum_rt[$kbk][$kpd]['rt'] }}</td>
                                            <td class="row">  
                                                <div class="col-auto mx-2">
                                                    <a class="btn btn-info" href="{{ url('/st_wh/st_withdraw/detail/'.$sum_rt[$kbk][$kpd]['bk'].'/'.$sum_rt[$kbk][$kpd]['pd']) }}">Detail</a>
                                                </div>  
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>                          
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->

        </div><!--//container-fluid-->
    </div><!--//app-content-->
@endsection