@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <h1 class="app-page-title mb-0">Stock</h1>
                </div>                
            </div><!--//row onclick="getURL($key);" -->
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>             
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <table class="table app-table-hover mb-0 text-left">
                            <thead>
                                <tr>
                                    <th class="cell">No.</th>
                                    <th class="cell">สินค้า</th>
                                    <th class="cell">จำนวน</th>    
                                    <th class="cell"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($sum_wh as $key=>$value)
                                    <tr>                                             
                                        <td class="cell">{{ $loop->iteration }}</td>
                                        <td class="cell">{{ $key }}</td>
                                        <td class="cell">{{ number_format($sum_wh[$key]['tt']) }}</td>
                                        <td class="row">
                                            <div class="col-auto mx-2">
                                                <a class="btn btn-info" href="{{ url('/st_wh/st_receive/index/receive/'.$sum_wh[$key]['pd']) }}">Receive</a>
                                            </div>    
                                            <div class="col-auto mx-2">
                                                <a class="btn btn-danger" href="{{ url('/st_wh/st_receive/index/change/'.$sum_wh[$key]['pd']) }}">ชำรุุด</a>
                                            </div>
                                            <div class="col-auto mx-2">
                                                <a class="btn btn-info" href="{{ route('st_receive_show', $sum_wh[$key]['pd']) }}">รายละเอียด</a>
                                            </div> 
                                            {{-- <div class="col-auto">
                                                <form action="{{ route('st_broker.destroy', $key->id) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </form>
                                            </div> --}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>                           
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
            
        </div><!--//container-fluid-->
    </div><!--//app-content-->
@endsection