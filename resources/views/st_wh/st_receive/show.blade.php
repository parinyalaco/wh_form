@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="page-heading row justify-content-between">        
                <div class="pull-right col-auto">
                    <div class="row">
                        <div class="pull-right col-auto">
                            <a class="btn btn-success" href="{{ route('st_pd_stock') }}">Back</a>
                        </div>
                        <div class="col-auto">
                            <h1 class="app-page-title mb-0">รายละเอียด -> {{ $st_wh[0]['name'] }}</h1>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-auto">
                    <div class="page-utilities">
                        <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                            <div class="col-auto">
                                <a class="btn app-btn-secondary" href="{{ route('st_receive_export', $st_wh[0]['st_product_id']) }}">
                                    <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
                                    Excel
                                </a>
                            </div>
                        </div><!--//row-->
                    </div><!--//table-utilities-->
                </div><!--//col-auto--> --}}
            </div>
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>             
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <table class="table app-table-hover mb-0 text-left">
                            <thead>
                                <tr>
                                    <th class="cell">วันที่</th>
                                    <th class="cell">รับเข้า</th>
                                    <th class="cell">ชำรุด</th>
                                    <th class="cell">เบิก</th>
                                    <th class="cell">คืน</th>
                                    <th class="cell">ยกมา/คงเหลือ</th>    
                                    <th class="cell"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th class="cell">{{ date('d/m/Y',strtotime($st_wh[0]['start_date'])) }}</th>
                                    <th class="cell"></th>
                                    <th class="cell"></th>
                                    <th class="cell"></th>
                                    <th class="cell"></th>
                                    <th class="cell">{{ number_format($st_wh[0]['amount']) }}</th>    
                                    <th class="cell"></th>
                                </tr>
                                @php
                                    $cal_amount = $st_wh[0]['amount'];
                                @endphp
                                @foreach ($to_show as $key=>$value)
                                    <tr>                                         
                                        <td class="cell">{{ date('d/m/Y',strtotime($key)) }}</td>
                                        <td class="cell">
                                            @if(!empty($to_show[$key]['rc']))
                                                {{ number_format($to_show[$key]['rc']) }}
                                                @php
                                                    $cal_amount += $to_show[$key]['rc'];
                                                @endphp
                                            @endif
                                        </td>
                                        <td class="cell">
                                            @if(!empty($to_show[$key]['ch']))
                                                {{ number_format($to_show[$key]['ch']) }}
                                                @php
                                                    $cal_amount -= $to_show[$key]['ch'];
                                                @endphp
                                            @endif
                                        </td>
                                        <td class="cell">
                                            @if(!empty($to_show[$key]['wd']))
                                                {{ number_format($to_show[$key]['wd']) }}
                                                @php
                                                    $cal_amount -= $to_show[$key]['wd'];
                                                @endphp
                                            @endif
                                        </td>
                                        <td class="cell">
                                            @if(!empty($to_show[$key]['rt']))
                                                {{ number_format($to_show[$key]['rt']) }}
                                                @php
                                                    $cal_amount += $to_show[$key]['rt'];
                                                @endphp
                                            @endif
                                        </td>                                      
                                        <td class="cell">{{ number_format($cal_amount) }}</td>
                                        <td class="row">
                                            {{-- @if($tran['type'] != 'show')
                                                <div class="col-auto mx-2">
                                                    <a class="btn btn-primary" href="{{ url('/st_wh/st_withdraw/edit/'.$tran['type'].'/'.$key->id) }}">Edit</a>
                                                </div>
                                                <div class="col-auto">
                                                    <a class="btn btn-danger" href="{{ url('/st_wh/st_withdraw/del/'.$tran['type'].'/'.$key->id) }}">Delete</a>
                                                </div>
                                            @endif --}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
                       
        </div><!--//container-fluid-->
    </div><!--//app-content-->
@endsection