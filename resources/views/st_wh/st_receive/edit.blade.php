@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            <div class="page-heading row">        
                <div class="pull-right col-auto">
                    <div class="row">
                        <div class="pull-right col-auto">
                            <a class="btn btn-success" href="{{ url('/st_wh/st_receive/index/'.$tran['type'].'/'.$st_rc->st_product_id) }}">Back</a>
                        </div>
                        <div class="col-auto">
                            <h1 class="app-page-title mb-0">{{ $tran['type_show'] }} -> {{ $st_wh[0]['name'] }} -> Edit</h1>
                        </div>
                    </div>
                </div>
            </div>
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>  
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <form method="POST" onSubmit="return confirm('วันที่เริ่มต้นของข้อมูล เป็นวันที่ {{ date('d/m/Y',strtotime($st_wh[0]['start_date'])) }} คุณแน่ใจมั้ยว่าต้องการเพิ่มข้อมูลนี้?');" action="{{ url('/st_wh/st_receive/update/'.$tran['type']).'/'.$st_rc->id }}">
                            @csrf
                            {{-- @method('PUT') --}}
                            <input type='hidden' id="st_product_id" class="form-control" name="st_product_id" value="{{ $st_rc->st_product_id }}" required>

                            <div class="form-group row mb-2">
                                <label for="no_id" class="col-md-2 col-form-label text-md-right">{{ __('เลขที่บิล') }}</label>

                                <div class="col-md-6">
                                    <input type='text' id="no_id" class="form-control" name="no_id" value="{{ $st_rc->no_id }}" required>
                                </div>
                            </div> 

                            <div class="form-group row mb-2">
                                <label for="tran_date" class="col-md-2 col-form-label text-md-right">{{ __('วันที่รับเข้า') }}</label>

                                <div class="col-md-6">
                                    <input type='date' id="tran_date" class="form-control" name="tran_date" value="{{ $st_rc->tran_date }}" required> 
                                </div>
                            </div>

                            <div class="form-group row mb-2">
                                <label for="amount" class="col-md-2 col-form-label text-md-right">{{ __('จำนวน') }}</label>

                                <div class="col-md-6">
                                    <input type='number' id="amount" class="form-control" name="amount" value="{{ $st_rc->amount }}" required>
                                </div>
                            </div>

                            <div class="form-group row mb-2">
                                <div class="col-md-6 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->     
@endsection
