@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="page-heading row">        
                <div class="pull-right col-auto">
                    <a class="btn btn-success" href="{{ route('st_broker.index') }}">Back</a>
                </div>
                <h3 class="col">{{ __('Set Broker') }} -> Create</h3>
            </div>
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>  
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm py-4 px-4">
                    <div class="app-card-body">
                        <form method="POST" action="{{ route('st_broker.store') }}">
                            @csrf
                            <div class="form-group row mb-2">
                                <label for="rm_broker_id" class="col-md-2 col-form-label text-md-right">{{ __('Broker') }}</label>

                                <div class="col-md-6">
                                    <select name="rm_broker_id" class="form-select"  required>
                                        <option value="">ไม่ระบุ</option>
                                        @foreach ($rm_bk as $key)
                                            <option value="{{ $key->id }}" @if(old('rm_broker_id')==$key->id) selected @endif>{{ $key->name }}</option>
                                        @endforeach
                                    </select>

                                    {{-- <input id="custom_field1" name="custom_field1" type="text" list="custom_field1_datalist" class="form-control">
                                    <datalist id="custom_field1_datalist">
                                        @foreach ($rm_bk as $key)
                                            <option value="{{ $key->id }}" @if(old('rm_broker_id')==$key->id) selected @endif>{{ $key->name }}</option>
                                        @endforeach
                                    </datalist> --}}

                                    {{-- <select name="custom_field1" class="form-control select2" placeholder="Select City" required>
                                        <option value="">ไม่ระบุ</option>
                                        @foreach ($rm_bk as $key)
                                            <option value="{{ $key->id }}" @if(old('rm_broker_id')==$key->id) selected @endif>{{ $key->name }}</option>
                                        @endforeach
                                    </select> --}}

                                    @if($errors->has('rm_broker_id'))
                                        <div class="text-danger">{{ $errors->first('rm_broker_id') }}</div>
                                    @endif
                                    {{-- @error('st_product_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror --}}
                                </div>
                            </div>

                            <div class="form-group row mb-2">
                                <label for="st_product_id" class="col-md-2 col-form-label text-md-right">{{ __('Product') }}</label>

                                <div class="col-md-6">
                                    <select name="st_product_id" class="form-select" required>
                                        <option value="">ไม่ระบุ</option>
                                        @foreach ($st_pd as $key)
                                            <option value="{{ $key->id }}" @if(old('st_product_id')==$key->id) selected @endif>{{ $key->name }}</option>
                                        @endforeach
                                    </select>

                                    @if($errors->has('st_product_id'))
                                        <div class="text-danger">{{ $errors->first('st_product_id') }}</div>
                                    @endif
                                    {{-- @error('st_product_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror --}}
                                </div>
                            </div>   

                            <div class="form-group row mb-2">
                                <label for="amount" class="col-md-2 col-form-label text-md-right">{{ __('จำนวนคงคลัง') }}</label>

                                <div class="col-md-6">
                                    <input type='number' id="amount" class="form-control" name="amount" value="" required>

                                    @if($errors->has('amount'))
                                        <div class="error" role="alert">{{ $errors->first('amount') }}</div>
                                    @endif
                                    {{-- @error('amount')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror --}}
                                </div>
                            </div> 

                            <div class="form-group row mb-2">
                                <label for="start_date" class="col-md-2 col-form-label text-md-right">{{ __('วันที่เริ่มต้นใช้') }}</label>

                                <div class="col-md-6">
                                    <input type='date' id="start_date" class="form-control" name="start_date" value="" required>
                                    
                                    @if($errors->has('start_date'))
                                        <div class="error" role="alert">{{ $errors->first('start_date') }}</div>
                                    @endif
                                    {{-- @error('start_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror --}}
                                </div>
                            </div> 

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->   
    {{-- <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>      --}}
@endsection
