@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            <div class="page-heading row">        
                <div class="pull-right col-auto">
                    <a class="btn btn-success" href="{{ route('WhAct_type_import_export') }}">Back</a>
                </div>
                <h3 class="col">{{ __('ชั่วโมงการใช้งานรถ FL') }} -> ข้อมูลไม่ถูกต้อง</h3>
            </div>
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>              
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <table class="table app-table-hover mb-0 text-left">
                            <thead>
                                <tr>
                                    <th class="cell"></th>
                                    <th class="cell">ข้อมูล</th>
                                    <th class="cell">บรรทัดที่</th>
                                    <th class="cell">หมายเหตุ</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $loop_row = 0;
                                @endphp
                                @if(!empty($chk_file['all'][0]))
                                    <tr>                                                                        
                                        <td>{{ ++$loop_row }}</td>                                          
                                        <td>{{ $chk_file['all'][0] }}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @endif
                                @if(!empty($chk_file['code']))                                    
                                    <tr>
                                        <td>{{ ++$loop_row }}</td> 
                                        <td>code</td>
                                        <td>
                                            @foreach ($chk_file['code'] as $key=>$value)
                                                {{ $key }}, 
                                            @endforeach
                                        </td> 
                                        <td>คอลัมน์ code เป็นค่าว่าง</td>                                      
                                    </tr> 
                                @endif
                                @if(!empty($chk_file['duplicate']))                                    
                                    <tr>
                                        <td>{{ ++$loop_row }}</td> 
                                        <td>code ซ้ำ</td>
                                        <td>
                                            @foreach ($chk_file['duplicate'] as $key=>$value)
                                                {{ $key }}, 
                                            @endforeach
                                        </td> 
                                        <td>คอลัมน์ code มีค่าซ้ำ</td>                                      
                                    </tr>
                                @endif
                                @if(!empty($chk_file['name']))                                    
                                    <tr>
                                        <td>{{ ++$loop_row }}</td> 
                                        <td>name</td>
                                        <td>
                                            @foreach ($chk_file['name'] as $key=>$value)
                                                {{ $key }}, 
                                            @endforeach
                                        </td> 
                                        <td>คอลัมน์ name เป็นค่าว่าง</td>                                      
                                    </tr>
                                @endif                                
                            </tbody>
                        </table>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
@endsection