@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <h1 class="app-page-title mb-0">Forklift</h1>
                </div>
                <div class="col-auto">
                    <div class="page-utilities">
                        <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                            <div class="col-auto">						    
                                <a class="btn app-btn-secondary" href="{{ route('forklift_import_export') }}">
                                    <i class="fas fa-file-upload"></i>&nbsp;
                                    Upload Excel
                                </a>
                                <a class="btn app-btn-secondary" href="{{ route('forklift.create') }}">
                                    <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
                                    Add
                                </a>
                            </div>
                        </div><!--//row-->
                    </div><!--//table-utilities-->
                </div><!--//col-auto-->
            </div><!--//row onclick="getURL($key);" -->
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>             
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <div class="table-responsive">
                            <table class="table app-table-hover mb-0 text-left">
                                <thead>
                                    <tr>
                                        <th class="cell">No.</th>
                                        <th class="cell">Name</th>
                                        <th class="cell">เจ้าของ</th>
                                        <th class="cell">ชั่วโมงเริ่มต้น</th>
                                        <th class="cell">หมายเหตุ</th>
                                        <th class="cell"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($forklift as $key)
                                        <tr>                                             
                                            <td class="cell">{{ $loop->iteration }}</td>
                                            <td class="cell">{{ $key->name }}</td>
                                            <td class="cell">{{ $key->type }}</td>
                                            <td class="cell" style="text-align:right">{{ number_format($key->time,1) }}</td>
                                            <td class="cell">{{ $key->desc }}</td>
                                            <td class="row">
                                                @if(auth()->user()->user_type_id==1)
                                                    <div class="col-auto mx-2">
                                                        <a class="btn btn-primary" href="{{ route('forklift.edit', $key->id) }}">Edit</a>
                                                    </div>
                                                @endif
                                                <div class="col-auto">
                                                    <form action="{{ route('forklift.destroy', $key->id) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div><!--//table-responsive-->                            
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
@endsection