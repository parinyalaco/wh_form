@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-3">
        <div class="app-container-xl">
            
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <div class="d-flex flex-row">
                        <span class="nav-item">
                            <a class="btn-sm app-btn-secondary" href="{{ route('FL_report') }}">
                                <i class="fas fa-long-arrow-alt-left"></i>
                                Back
                            </a>
                        </span> 
                        <h1 class="app-page-title mb-0">กราฟแสดงชั่วโมงการใช้รถโดยเฉลี่ยในเดือน {{ $month_date.'/'.$year_date }}</h1>
                    </div>
                </div>                
            </div><!--//row onclick="getURL($key);" -->            
            
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>

            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <canvas id="myChart" style="width:100%;max-width:600px"></canvas>
                    </div><!--//app-card-body-->
                    <table class="table table-bordered border-dark-3" style="width: 100%">
                        <tbody>
                            <tr>
                                <th class="cell">พื้นที่ใช้งาน</th>
                                @foreach ($fl_log_locate as $kloc=>$vloc)                
                                    <td class="cell" colspan="{{  count($fl_log_locate[$kloc]) }}" style="text-align: center">{{ $locate_data[$kloc] }}</td>  
                                @endforeach
                            </tr> 
                            <tr>
                                <th class="cell">หมายเลขรถโฟร์คลิฟท์</th>           
                                @foreach ($fl_log_locate as $kloc=>$vloc)
                                    @foreach ($vloc as $kfl=>$vfl)                                                                 
                                        <td class="cell">{{ $kfl }}</td> 
                                    @endforeach
                                @endforeach
                            </tr> 
                            <tr>
                                <th class="cell" style="background-color:#E3E3DB">AVG กะ B</th>
                                @foreach ($fl_log_locate as $kloc=>$vloc)
                                    @foreach ($vloc as $kfl=>$vfl)                      
                                        @php
                                            if(!empty($total_val['sum'][$kloc][$kfl]['B']))    $avg_val['B'] = $total_val['sum'][$kloc][$kfl]['B']/$total_val['count'][$kloc][$kfl]['B'];
                                            else    $avg_val['B'] = 0;
                                        @endphp
                                        <td style="text-align: right;">@if(!empty($avg_val['B'])){{ number_format($avg_val['B'],1) }}@endif</td>                     
                                    @endforeach                                        
                                @endforeach
                            </tr>
                            <tr>                
                                <th class="cell" style="background-color:#E3E3DB">AVG กะ C</th>
                                @foreach ($fl_log_locate as $kloc=>$vloc)
                                    @foreach ($vloc as $kfl=>$vfl)                      
                                        @php
                                            if(!empty($total_val['sum'][$kloc][$kfl]['C']))    $avg_val['C'] = $total_val['sum'][$kloc][$kfl]['C']/$total_val['count'][$kloc][$kfl]['C']; 
                                            else    $avg_val['C'] = 0;
                                        @endphp
                                        <td style="text-align: right;">@if(!empty($avg_val['C'])){{ number_format($avg_val['C'],1) }}@endif</td>                        
                                    @endforeach                                        
                                @endforeach
                            </tr>
                        </tbody>
                    </table>		
                </div><!--//app-card-->     
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
    <script src="{{ asset('assets/js/Chart.js') }}"></script>
    <script>
        const chk_arr4 = <?php echo json_encode($gp_4); ?>;
        new Chart("myChart", {
            type: 'bar',
            data: {
                labels: <?php echo json_encode($to_show); ?>,
                datasets: [
                    {
                        label: 'กะ B',
                        data: chk_arr4['B'],
                        borderColor: '#FFFFFF',
                        backgroundColor: '#0912CF',
                    },
                    {
                        label: 'กะ C',
                        data: chk_arr4['C'],
                        borderColor: '#FFFFFF',
                        backgroundColor: '#C8CAF5',
                    }
                ]
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        position: 'top'
                    }
                },
                title: {
                    display: true,
                    text: 'กราฟแสดงชั่วโมงการใช้รถโดยเฉลี่ยในเดือน {{ $month_date.'/'.$year_date }}'
                }
            }
        });
    </script>
@endsection