@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            <div class="page-heading row"> 
                <div class="pull-right col-auto">
                    <span class="nav-item">
                        <a class="btn-sm app-btn-secondary" href="{{ route('FL_report') }}">
                            <i class="fas fa-long-arrow-alt-left"></i>
                            Back
                        </a>
                    </span>
                </div>
                <h3 class="col">ตารางแสดงชั่วโมงการใช้งานรถโฟร์คลิฟท์(รถเช่า)-(หน่วย : ชั่วโมง) ระหว่างวันที่ {{ $st_date }} ถึงวันที่ {{ $ed_date }}</h3>
            </div>     
            
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>                    
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <div class="table-responsive">
                            @php
                                $begin = new DateTime($st_date);
                                $end   = new DateTime($ed_date);
                                $shift = array('B','C');
                            @endphp
                            <table class="table app-table-hover mb-0 text-left">
                                <thead>
                                    <tr>
                                        {{-- $begin = new DateTime( "2015-07-03" );
                                        $end   = new DateTime( "2015-07-09" );

                                        for($i = $begin; $i <= $end; $i->modify('+1 day')){
                                            echo $i->format("Y-m-d");
                                        } --}}
                                        <th class="cell"></th>
                                        <th class="cell"></th>
                                        @for($i = $begin; $i <= $end; $i->modify('+1 day'))
                                            <th class="cell" colspan="2">{{ $i->format("d/m") }}</th>
                                        @endfor
                                        <th class="cell" rowspan="2">AVG กะ B</th>
                                        <th class="cell" rowspan="2">AVG กะ C</th>
                                    </tr>
                                    <tr>
                                        <th class="cell"></th>
                                        <th class="cell"></th>
                                        @for($i = 0; $i <= $days; $i++)
                                            <th class="cell">B</th>
                                            <th class="cell">C</th>
                                        @endfor
                                    </tr>                                    
                                </thead>
                                <tbody>
                                    @foreach ($fl_log_locate as $kloc=>$vloc)
                                        <tr>
                                            <td class="cell" rowspan="{{ count($fl_log_locate[$kloc]) }}">{{ $locate_data[$kloc] }}</td>
                                            @php
                                                $loop_loc = 0;
                                            @endphp
                                            @foreach ($vloc as $kfl=>$vfl)
                                                @if($loop_loc>0)
                                                    <tr>
                                                @endif                                                
                                                {{-- <td class="cell">@if(!empty($fl_data[$kfl])){{ $fl_data[$kfl] }}@endif</td> --}}
                                                <td class="cell">{{ $kfl }}</td>
                                                @php
                                                    $begin = new DateTime($st_date);
                                                    $end   = new DateTime($ed_date);                                                    
                                                @endphp                                                
                                                @for($i = $begin; $i <= $end; $i->modify('+1 day')) 
                                                    @foreach($shift as $key => $value)
                                                        @if(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['S'])) 
                                                            <td class="table-dark">
                                                                @if($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['S'] <> 'S')
                                                                    {{ $fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['S'] }}
                                                                @endif
                                                            </td>
                                                        @elseif(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['A']))
                                                            <td class="table-danger">
                                                                @if($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['A'] <> 'A')
                                                                    {{ $fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['A'] }}
                                                                @endif
                                                            </td>                                                          
                                                        @else
                                                            <td class="cell">
                                                                @if(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['R']))
                                                                    {{ $fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['R'] }}
                                                                @endif
                                                            </td>
                                                        @endif
                                                    @endforeach
                                                    
                                                    {{-- <td class="@if(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['B']['S'])) table-dark @elseif(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['B']['A'])) table-danger @else cell @endif" >
                                                        @if(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['B']['R']))
                                                            {{ $fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['B']['R'] }}
                                                        @elseif(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['B']['A']))
                                                            @if($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['B']['A'] <> 'A')
                                                                {{ $fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['B']['A'] }}
                                                            @endif
                                                        @endif
                                                    </td>
                                                    <td class="@if(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['C']['S'])) table-dark @elseif(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['C']['A'])) table-danger @else cell @endif" >
                                                        @if(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['C']['R']))
                                                            {{ $fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['C']['R'] }}
                                                        @elseif(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['C']['A']))
                                                        @if($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['C']['A'] <> 'A')
                                                            {{ $fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['C']['A'] }}
                                                        @endif
                                                        @endif
                                                    </td> --}}
                                                @endfor
                                                @php
                                                    $loop_loc++;
                                                    if(!empty($total_val['sum'][$kloc][$kfl]['B']))    $avg_val['B'] = $total_val['sum'][$kloc][$kfl]['B']/$total_val['count'][$kloc][$kfl]['B'];
                                                    else $avg_val['B'] = 0;
                                                    if(!empty($total_val['sum'][$kloc][$kfl]['C']))    $avg_val['C'] = $total_val['sum'][$kloc][$kfl]['C']/$total_val['count'][$kloc][$kfl]['C'];
                                                    else $avg_val['C'] = 0;
                                                @endphp
                                                <td class="cell">@if(!empty($avg_val['B'])){{ number_format($avg_val['B'],1) }}@endif</td>
                                                <td class="cell">@if(!empty($avg_val['C'])){{ number_format($avg_val['C'],1) }}@endif</td>
                                                </tr>
                                            @endforeach                                        
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">            
                    <div class="app-card-body">
                        <div class="table-responsive">
                            @php
                                $begin = new DateTime($st_date);
                                $end   = new DateTime($ed_date);
                                $shift = array('B','C');
                            @endphp
                            <table class="table app-table-hover mb-0 text-left">
                                <thead>
                                    <tr>
                                        <th class="cell" colspan="{{ ($days*2)+4 }}">แสดงชั่วโมงการใช้แบบเต็มจำนวนชั่วโมง</th>                                        
                                    </tr>
                                    <tr>
                                        <th class="cell"></th>
                                        <th class="cell"></th>
                                        @for($i = $begin; $i <= $end; $i->modify('+1 day'))
                                            <th class="cell" colspan="2">{{ $i->format("d/m") }}</th>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <th class="cell"></th>
                                        <th class="cell"></th>
                                        @for($i = 0; $i <= $days; $i++)
                                            <th class="cell">B</th>
                                            <th class="cell">C</th>
                                        @endfor
                                    </tr>                                    
                                </thead>
                                <tbody>
                                    @foreach ($fl_mile as $kloc=>$vloc)
                                    <tr>
                                        <td class="cell" rowspan="{{  count($fl_mile[$kloc]) }}">{{ $locate_data[$kloc] }}</td>
                                        @php
                                            $loop_loc = 0;
                                        @endphp
                                        @foreach ($vloc as $kfl=>$vfl)
                                            @if($loop_loc>0)
                                                <tr>
                                            @endif                                                
                                            <td class="cell">{{ $kfl }}</td>
                                            @php
                                                $begin = new DateTime($st_date);
                                                $end   = new DateTime($ed_date);                                                    
                                            @endphp                                                
                                            @for($i = $begin; $i <= $end; $i->modify('+1 day')) 
                                                @foreach($shift as $key => $value)
                                                    @if(!empty($fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['S'])) 
                                                        <td class="table-dark">
                                                            @if($fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['S'] <> 'S')
                                                                {{ $fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['S'] }}
                                                            @endif
                                                        </td>
                                                    @elseif(!empty($fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['A']))
                                                        <td class="table-danger">
                                                            @if($fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['A'] <> 'A')
                                                                {{ $fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['A'] }}
                                                            @endif
                                                        </td>                                                          
                                                    @else
                                                        <td class="cell">
                                                            @if(!empty($fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['R']))
                                                                {{ $fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['R'] }}
                                                            @endif
                                                        </td>
                                                    @endif
                                                @endforeach
                                            @endfor
                                            </tr>
                                        @endforeach                                        
                                    @endforeach
                                </tbody>
                            </table>
                        </div><!--//table-responsive-->                    
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->     
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
@endsection