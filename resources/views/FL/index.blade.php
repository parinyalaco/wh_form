@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <h1 class="app-page-title mb-0">ชั่วโมงการใช้งานรถ FL</h1>
                </div>
                <div class="col-auto">
                    <div class="page-utilities">
                        <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                            <div class="col-auto">						    
                                <a class="btn app-btn-secondary" href="{{ route('FL_import_export') }}">
                                    <i class="fas fa-file-upload"></i>&nbsp;
                                    Upload Excel
                                </a>                                
                                <a class="btn app-btn-secondary" href="{{ route('FL_report') }}">
                                    <i class="fas fa-file-download"></i>&nbsp;
                                    Report
                                </a>
                                <a class="btn app-btn-secondary" href="{{ route('FL_mail') }}">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;
                                    Sent Mail
                                </a>
                                <a class="btn app-btn-secondary" href="{{ route('FL.create') }}">
                                    <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
                                    Add
                                </a>
                            </div>
                        </div><!--//row-->
                    </div><!--//table-utilities-->
                </div><!--//col-auto-->
            </div><!--//row onclick="getURL($key);" -->
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>  
            
            <div class="col-auto">
                <form method="GET" action="{{ route('FL.index') }}" accept-charset="UTF-8"
                class="table-search-form row gx-1 align-items-center" role="search">
                    <div class="form-group row mb-3">
                        <label for="name" class="col-auto form-label">วันที่ </label>
                        <div class="col-auto">
                            <input type="date" class="form-control" id="date_from" name="date_from" @if(!empty($date_from)) value="{{ date("Y-m-d", strtotime($date_from)) }}" @endif>											
                        </div>
                        <label for="name" class="col-auto form-label">ถึงวันที่ </label>
                        <div class="col-auto">
                            <input type="date" class="form-control" id="date_to" name="date_to" @if(!empty($date_to)) value="{{ date("Y-m-d", strtotime($date_to)) }}" @endif>											
                        </div>
                        <label for="fl_id" class="col-auto form-label">รถโฟล์คลิฟท์</label>
                        <div class="col-auto">
                            <select name="fl_id" class="form-select">
                                <option value=""@if(empty($fl_id)) selected @endif>ไม่ระบุ</option>
                                @foreach ($to_fl as $key=>$value)
                                    <option value="{{ $key }}" @if($fl_id==$key) selected @endif>{{ $value }}</option>
                                @endforeach
                            </select>											
                        </div>
                        <div class="col-auto">
                            <button type="submit" class="btn btn-success">Search</button>
                        </div>
                    </div>
                </form>                
            </div><!--//col-->
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        {{-- <div class="table-responsive"> --}}
                            <table class="table app-table-hover mb-0 text-left">
                                <thead style="text-align: center">
                                    <tr>
                                        <th class="cell" rowspan="2">วันที่</th>
                                        <th class="cell" rowspan="2">หน่วยงานย่อย</th>
                                        <th class="cell" rowspan="2">รถโฟล์คลิฟท์</th>
                                        <th class="cell" rowspan="2">กะ</th>
                                        <th class="cell" rowspan="2">สถานะ</th>
                                        <th class="cell" rowspan="2">ผู้ใช้งาน</th>
                                        <th class="cell" colspan="3">ชั่วโมงทำงาน</th>
                                        <th class="cell" rowspan="2">สาเหตุการหยุดใช้งาน</th>
                                        <th class="cell" rowspan="2"></th>
                                    </tr>
                                    <tr>
                                        <th class="cell">เริ่มงาน</th>
                                        <th class="cell">จบงาน</th>
                                        <th class="cell">สุทธิ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($fl_list as $kdate=>$vdate)
                                        <tr>
                                            @php $loop_date = 0; @endphp                                             
                                            <td class="cell" rowspan="{{ $count_row['date'][$kdate] }}" style="vertical-align: top;">
                                                <div class="row">
                                                    @if(auth()->user()->user_type_id==1)
                                                        <div class="col-4">
                                                            <a onclick="return confirm(&quot;คุณแน่ใจมั้ยว่าต้องการลบรายการของวันที่ {{ $kdate }} ?&quot;)" title="Delete" href="{{ route('FL_del',$kdate) }}">
                                                                <i class="far fa-trash-alt"></i>
                                                            </a>
                                                        </div>
                                                    @endif
                                                    <div class="col-8">{{ $kdate }}</div>
                                                </div>
                                            </td>
                                            @foreach ($vdate as $klocate=>$vlocate)
                                                @php $loop_locate = 0; @endphp
                                                @if($loop_date <> 0)<tr>@endif 
                                                <td class="cell" rowspan="{{ $count_row['locate'][$kdate][$klocate] }}" style="vertical-align: top;">{{ $to_locate[$klocate] }}</td>
                                                @foreach ($vlocate as $kfl=>$vfl)                                                    
                                                    @if($loop_locate <> 0)<tr>@endif 
                                                    <td class="cell" rowspan="{{ $count_row['fl'][$kdate][$klocate][$kfl] }}" style="vertical-align: top;">{{ $to_fl[$kfl] }}</td>
                                                    @foreach ($vfl as $ksh=>$vsh)
                                                        @php $loop_fl = 0; @endphp
                                                        @foreach ($fl_list[$kdate][$klocate][$kfl][$ksh]['id'] as $kloop=>$vloop)
                                                            @if($loop_fl <> 0)<tr>@endif 
                                                            <td class="cell">{{ $ksh }}</td>                                                      
                                                            <td class="cell">{{ $fl_list[$kdate][$klocate][$kfl][$ksh]['type'][$kloop] }}</td>  
                                                            <td class="cell">@if(!empty($fl_list[$kdate][$klocate][$kfl][$ksh]['fperson_id'][$kloop])){{ $fl_list[$kdate][$klocate][$kfl][$ksh]['fperson_id'][$kloop] }}@endif</td>                                                        
                                                            <td class="cell" style="text-align:right">
                                                                @if($fl_list[$kdate][$klocate][$kfl][$ksh]['time_before'][$kloop]>=0)
                                                                    {{ number_format($fl_list[$kdate][$klocate][$kfl][$ksh]['time_before'][$kloop],1) }}
                                                                @endif
                                                            </td>
                                                            <td class="cell" style="text-align:right">
                                                                @if(!empty($fl_list[$kdate][$klocate][$kfl][$ksh]['time'][$kloop]))
                                                                    {{ number_format($fl_list[$kdate][$klocate][$kfl][$ksh]['time'][$kloop],1) }}
                                                                @endif
                                                            </td>
                                                            <td class="cell" style="text-align:right">
                                                                @if(!empty($fl_list[$kdate][$klocate][$kfl][$ksh]['time'][$kloop]) && $fl_list[$kdate][$klocate][$kfl][$ksh]['time_before'][$kloop]>=0)
                                                                    @php
                                                                        $diff_time = $fl_list[$kdate][$klocate][$kfl][$ksh]['time'][$kloop]-$fl_list[$kdate][$klocate][$kfl][$ksh]['time_before'][$kloop];
                                                                    @endphp
                                                                    {{ number_format($diff_time,1) }}
                                                                @endif
                                                            </td>                                                        
                                                            <td class="cell">{{ $fl_list[$kdate][$klocate][$kfl][$ksh]['broken'][$kloop].$fl_list[$kdate][$klocate][$kfl][$ksh]['note'][$kloop] }}</td>
                                                            <td class="row">
                                                                <div class="col">
                                                                    <a class="btn btn-primary" href="{{ route('FL.edit', $fl_list[$kdate][$klocate][$kfl][$ksh]['id'][$kloop]) }}">
                                                                        แก้ไข
                                                                    </a>
                                                                </div>
                                                                @if(auth()->user()->user_type_id==1)
                                                                    <div class="col">
                                                                        <form action="{{ route('FL.destroy', $fl_list[$kdate][$klocate][$kfl][$ksh]['id'][$kloop]) }}" method="POST">
                                                                            @csrf
                                                                            @method('DELETE')
                                                                            <button type="submit" class="btn btn-danger" 
                                                                            onclick="return confirm(&quot;คุณแน่ใจมั้ยว่าต้องการลบรายการนี้?&quot;)" title="ลบ" >ลบ</button>
                                                                        </form>
                                                                    </div>
                                                                @endif
                                                            </td>
                                                            @php $loop_fl++; @endphp
                                                            </tr>
                                                        @endforeach  
                                                    @endforeach  
                                                    @php $loop_locate++; @endphp                                                  
                                                @endforeach   
                                                @php $loop_date++; @endphp 
                                            @endforeach    
                                    @endforeach
                                </tbody>
                            </table>
                        {{-- </div><!--//table-responsive-->                             --}}
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
@endsection