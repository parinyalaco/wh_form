@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-3">
        <div class="app-container-xl">
            
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <div class="d-flex flex-row">
                        <span class="nav-item">
                            <a class="btn-sm app-btn-secondary" href="{{ route('FL_report') }}">
                                <i class="fas fa-long-arrow-alt-left"></i>
                                Back
                            </a>
                        </span> 
                        <h1 class="app-page-title mb-0">กราฟแสดงสัดส่วนการใช้รถประจำวันที่ {{ date ("d/m/Y", strtotime($st_date)) }}</h1>
                    </div>
                </div>                
            </div><!--//row onclick="getURL($key);" -->            
            
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>

            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <canvas id="myChart" style="width:100%;max-width:600px"></canvas>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->     
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
    <script src="{{ asset('assets/js/Chart.js') }}"></script>
    <script>
        const chk_arr = <?php echo json_encode($gp_2); ?>;
        new Chart("myChart", {
            type: 'bar',
            data: {
                labels: ["กะ B", "กะ C"],
                datasets: [
                    {
                        label: 'R',
                        data: [chk_arr['R']['B'], chk_arr['R']['C']],
                        backgroundColor: '#13AC31',
                    },
                    {
                        label: 'S',
                        data: [chk_arr['S']['B'], chk_arr['S']['C']],
                        backgroundColor: '#FAFA1C',
                    },
                    {
                        label: 'A',
                        data: [chk_arr['A']['B'], chk_arr['A']['C']],
                        backgroundColor: '#E3E3DB',
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'สัดส่วนการใช้งานรถ FL (รถเช่า) วันที่ {{ $st_date }}'
                },            
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                },
                legend: {
                    display: false,
                },
                maintainAspectRatio: false,
            }
        });
    </script>
@endsection