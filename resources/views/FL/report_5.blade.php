@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-3">
        <div class="app-container-xl">
            
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <div class="d-flex flex-row">
                        <span class="nav-item">
                            <a class="btn-sm app-btn-secondary" href="{{ route('FL_report') }}">
                                <i class="fas fa-long-arrow-alt-left"></i>
                                Back
                            </a>
                        </span> 
                        <h1 class="app-page-title mb-0">กราฟแสดงชั่วโมงการใช้รถแยกคันในเดือน {{ $month_date.'/'.$year_date }}</h1>
                    </div>
                </div>                
            </div><!--//row onclick="getURL($key);" -->            
            
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>

            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    @foreach ($to_show_gp5 as $key=>$value)
                        @foreach ($value as $key1=>$value2)
                            <div class="app-card-body">
                                <canvas id="myChart_{{ $key }}{{ $key1 }}" style="width:100%;max-width:600px"></canvas>
                            </div>
                        @endforeach    
                    @endforeach
                    {{-- <div class="app-card-body">
                        <canvas id="myChart" style="width:100%;max-width:600px"></canvas>
                    </div><!--//app-card-body-->		 --}}
                </div><!--//app-card-->     
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
    <script src="{{ asset('assets/js/Chart.js') }}"></script>
    <script>
        const arr_fl = <?php echo json_encode($to_show_gp5); ?>;
        const day_loop = <?php echo json_encode($date_in_month); ?>;
        // console.log(day_loop);
        const label_show = [];
        for (let index = 1; index <= day_loop; index++) {
            label_show.push(index);
        }        
        Object.keys(arr_fl).forEach(key => {
            // console.log(key); // 👉️ "name", "country"
            // console.log(arr_fl[key]); // 👉️ "Tom", "Chile"
            Object.keys(arr_fl[key]).forEach(key1 => {              
                new Chart("myChart_"+key+key1, {
                    type: 'bar',
                    data: {
                        labels: label_show,
                        datasets: [
                            {
                                label: 'กะ B',
                                data: arr_fl[key][key1]['B'],
                                borderColor: '#FFFFFF',
                                backgroundColor: '#639FFA',
                            },
                            {
                                label: 'กะ C',
                                data: arr_fl[key][key1]['C'],
                                borderColor: '#FFFFFF',
                                backgroundColor: '#639FFA',
                            }
                        ]
                    },
                    options: {
                        responsive: true,
                        plugins: {
                            legend: {
                                position: 'top'
                            }
                        },
                        title: {
                            display: true,
                            text: key+' (พื้นที่ปฏิบัติงาน '+key1+') ในเดือน {{ $month_date.'/'.$year_date }}'
                        }
                    }
                });
            });
        });
    </script>
@endsection