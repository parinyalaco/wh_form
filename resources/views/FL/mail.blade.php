@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-content">
            <div class="app-container-xl">
                <div class="page-heading row"> 
                    <div class="pull-right col-auto">
                        <span class="nav-item">
                            <a class="btn-sm app-btn-secondary" href="{{ route('FL.index') }}">
                                <i class="fas fa-long-arrow-alt-left"></i>
                                Back
                            </a>
                        </span>
                    </div>
                    <h3 class="col">{{ __('ชั่วโมงการใช้งานรถ FL') }} -> Mail</h3>
                </div>
                <nav id="" class="orders-table-tab app-nav-tabs flex-sm-row mb-4">	
                    <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" 
                        href="#" role="tab" aria-controls="orders-all" aria-selected="true"></a>
                </nav>			   
                
                <div class="tab-content" id="orders-table-tab-content">
                    <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
                        <div class="app-card app-card-orders-table shadow-sm mb-5">
                            <div class="app-card-body">
                                <div class="table-responsive">
                                    <table class="table table-borderless" id="tb_show">
                                        <thead>   
                                            <tr>
                                                <th class="cell" style="width: 5%">ตัวอย่าง</th>
                                                <th class="cell" style="text-align:left; width: 20%">TO..ALL</th>
                                                <th class="cell" style="width: 30%"></th>
                                                <th class="cell" style="text-align: center" style="width: 5%"></th>
                                                <th class="cell" style="width: 25%"></th>
                                                <th class="cell" style="width: 15%">
                                                    <a class="btn app-btn-secondary" onClick="return confirm('คุณต้องการส่งเมลล์ใช่รือไม่ ?');" href="{{ route('FL_sent_mail') }}" 
                                                        @if(count($gp_2)==0)style="pointer-events: none"@endif>
                                                        <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;
                                                        ส่งเมลล์
                                                    </a>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell" colspan="5">รายงานยอดชั่วโมงการใช้งานรถโฟร์คลิฟท์ระหว่างวันที่ {{ date ("d/m/Y", strtotime($st_date)) }} ถึงวันที่ {{ date ("d/m/Y", strtotime($ed_date)) }}</th>                                                    
                                            </tr>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell" colspan="5">1.ตารางแสดงชั่วโมงการใช้งานรถโฟร์คลิฟท์(รถเช่า) - (หน่วย : ชั่วโมง)</th>                                                    
                                            </tr>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell" colspan="5">
                                                    <table class="table table-bordered border-dark-3" style="width: 70%">
                                                        <thead>
                                                            <tr>
                                                                <th class="cell" style="width: 20%" rowspan="2">พื้นที่ใช้งาน</th>
                                                                <th class="cell" style="width: 25%" rowspan="2">หมายเลขรถโฟร์คลิฟท์</th>
                                                                @php
                                                                    // ต้องประกาศก่อนการใช้งานทุกครั้ง ประกาศครั้งเดียวไม่ได้
                                                                    $begin = new DateTime($st_date);
                                                                    $end   = new DateTime($ed_date);
                                                                @endphp
                                                                @for($i = $begin; $i <= $end; $i->modify('+1 day'))
                                                                    <th class="cell" colspan="2" style="text-align: center; background-color:#E3E3DB">{{ $i->format("d/m") }}</th>
                                                                @endfor
                                                                {{-- <th class="cell" rowspan="2" style="background-color:#E3E3DB">AVG กะ B</th>
                                                                <th class="cell" rowspan="2" style="background-color:#E3E3DB">AVG กะ C</th> --}}
                                                            </tr>
                                                            <tr>
                                                                @php
                                                                    $begin = new DateTime($st_date);
                                                                    $end   = new DateTime($ed_date);
                                                                    $shift = array('B','C');
                                                                @endphp
                                                                @for($i = $begin; $i <= $end; $i->modify('+1 day'))
                                                                    <th class="cell" style="text-align: center; width: {{ $cell_w }}%; background-color:#E3E3DB">B</th>
                                                                    <th class="cell" style="text-align: center; width: {{ $cell_w }}%; background-color:#E3E3DB">C</th>
                                                                @endfor
                                                            </tr>                                    
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($fl_log_locate as $kloc=>$vloc)
                                                                <tr>
                                                                    <td class="cell" rowspan="{{  count($fl_log_locate[$kloc]) }}">{{ $locate_data[$kloc] }}</td>
                                                                    @php
                                                                        $loop_loc = 0;
                                                                    @endphp
                                                                    @foreach ($vloc as $kfl=>$vfl)
                                                                        @if($loop_loc>0)
                                                                            <tr>
                                                                        @endif                                                
                                                                        <td class="cell">{{ $kfl }}</td>
                                                                        @php
                                                                            $begin = new DateTime($st_date);
                                                                            $end   = new DateTime($ed_date);
                                                                        @endphp
                                                                        @for($i = $begin; $i <= $end; $i->modify('+1 day'))
                                                                            @foreach($shift as $key => $value)
                                                                                @if(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['S'])) 
                                                                                    <td class="table-dark">
                                                                                        @if($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['S'] <> 'S')
                                                                                            {{ $fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['S'] }}
                                                                                        @endif
                                                                                    </td>
                                                                                @elseif(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['A']))
                                                                                    <td class="table-danger">
                                                                                        @if($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['A'] <> 'A')
                                                                                            {{ $fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['A'] }}
                                                                                        @endif
                                                                                    </td>                                                          
                                                                                @else
                                                                                    <td class="cell">
                                                                                        @if(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['R']))
                                                                                            {{ $fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")][$value]['R'] }}
                                                                                        @endif
                                                                                    </td>
                                                                                @endif
                                                                            @endforeach
                                                                            {{-- <td style="text-align: center; background-color:@if(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['B']['S'])) black @elseif(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['B']['A'])) red @else white @endif;" >
                                                                                @if(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['B']['R']))
                                                                                    {{ $fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['B']['R'] }}
                                                                                @endif
                                                                            </td>
                                                                            <td style="text-align: center; background-color:@if(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['C']['S'])) black @elseif(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['C']['A'])) red @else white @endif" >
                                                                                @if(!empty($fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['C']['R']))
                                                                                    {{ $fl_log_locate[$kloc][$kfl][$i->format("Y-m-d")]['C']['R'] }}
                                                                                @endif
                                                                            </td> --}}
                                                                        @endfor
                                                                        </tr>
                                                                    @endforeach                                        
                                                            @endforeach                                                           
                                                        </tbody>
                                                    </table>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell" colspan="5">
                                                    <table class="table table-bordered border-dark-3" style="width: 80%">
                                                        <thead>
                                                            <tr>
                                                                <th class="cell" colspan="12">แสดงชั่วโมงการใช้แบบเต็มจำนวนชั่วโมง</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="cell" style="width: 20%" rowspan="2">พื้นที่ใช้งาน</th>
                                                                <th class="cell" style="width: 25%" rowspan="2">หมายเลขรถโฟร์คลิฟท์</th>
                                                                @php
                                                                    // ต้องประกาศก่อนการใช้งานทุกครั้ง ประกาศครั้งเดียวไม่ได้
                                                                    $begin = new DateTime($st_date);
                                                                    $end   = new DateTime($ed_date);
                                                                @endphp
                                                                @for($i = $begin; $i <= $end; $i->modify('+1 day'))
                                                                    <th class="cell" colspan="2" style="text-align: center; background-color:#E3E3DB">{{ $i->format("d/m") }}</th>
                                                                @endfor
                                                            </tr>
                                                            <tr>
                                                                @php
                                                                    $begin = new DateTime($st_date);
                                                                    $end   = new DateTime($ed_date);
                                                                    $shift = array('B','C');
                                                                @endphp
                                                                @for($i = $begin; $i <= $end; $i->modify('+1 day'))
                                                                    <th class="cell" style="text-align: center; width: {{ $cell_w }}%; background-color:#E3E3DB">B</th>
                                                                    <th class="cell" style="text-align: center; width: {{ $cell_w }}%; background-color:#E3E3DB">C</th>
                                                                @endfor
                                                            </tr>                                    
                                                        </thead>
                                                        <tbody>                                                            
                                                            @foreach ($fl_mile as $kloc=>$vloc)
                                                                <tr>
                                                                    <td class="cell" rowspan="{{  count($fl_mile[$kloc]) }}">{{ $locate_data[$kloc] }}</td>
                                                                    @php
                                                                        $loop_loc = 0;
                                                                    @endphp
                                                                    @foreach ($vloc as $kfl=>$vfl)
                                                                        @if($loop_loc>0)
                                                                            <tr>
                                                                        @endif                                                
                                                                        <td class="cell">{{ $kfl }}</td>
                                                                        @php
                                                                            $begin = new DateTime($st_date);
                                                                            $end   = new DateTime($ed_date);
                                                                        @endphp
                                                                        @for($i = $begin; $i <= $end; $i->modify('+1 day'))
                                                                            @foreach($shift as $key => $value)
                                                                                @if(!empty($fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['S'])) 
                                                                                    <td class="table-dark">
                                                                                        @if($fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['S'] <> 'S')
                                                                                            {{ $fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['S'] }}
                                                                                        @endif
                                                                                    </td>
                                                                                @elseif(!empty($fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['A']))
                                                                                    <td class="table-danger">
                                                                                        @if($fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['A'] <> 'A')
                                                                                            {{ $fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['A'] }}
                                                                                        @endif
                                                                                    </td>                                                          
                                                                                @else
                                                                                    <td class="cell">
                                                                                        @if(!empty($fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['R']))
                                                                                            {{ $fl_mile[$kloc][$kfl][$i->format("Y-m-d")][$value]['R'] }}
                                                                                        @endif
                                                                                    </td>
                                                                                @endif
                                                                            @endforeach
                                                                        @endfor
                                                                        </tr>
                                                                    @endforeach                                        
                                                            @endforeach
                                                        </tbody>
                                                    </table> 
                                                </th>
                                            </tr>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell" colspan="5">
                                                    <table class="table table-bordered border-dark-3" style="width: 50%">
                                                        <tr>
                                                            <td style="width: 10%"><u>หมายเหตุ</u></td>
                                                            <td class="cell" style="width: 10%"></td>
                                                            <td style="width: 20%">ใช้งาน</td>
                                                            <td class="table-dark" style="width: 10%"></td>
                                                            <td style="width: 20%">จอด</td>
                                                            <td class="table-danger" style="width: 10%"></td>
                                                            <td style="width: 20%">เสียต้องจอด</td>
                                                        </tr>
                                                    </table>
                                                </th>                                                    
                                            </tr>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell" colspan="5">2.กราฟแสดงสัดส่วนการใช้รถประจำวันที่ {{ date ("d/m/Y", strtotime($ed_date)) }}</th>                                                    
                                            </tr>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell" colspan="5">
                                                    <canvas id="myChart2" style="width:100%;max-width:600px"></canvas>
                                                </th>                                                    
                                            </tr>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell" colspan="5">3.กราฟแสดงชั่วโมงการใช้รถประจำวัน {{ date ("d/m/Y", strtotime($ed_date)) }}</th>                                                    
                                            </tr><tr>
                                                <th class="cell"></th>
                                                <th class="cell" colspan="5">
                                                    <canvas id="myChart3" style="width:100%;max-width:600px"></canvas>
                                                </th>                                                    
                                            </tr>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell" colspan="5">4.กราฟแสดงชั่วโมงการใช้รถโดยเฉลี่ยในเดือน {{ date ("m/Y", strtotime($ed_date)) }}</th>                                                    
                                            </tr>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell" colspan="5">
                                                    <canvas id="myChart4" style="width:100%;max-width:600px"></canvas>
                                                </th>                                                    
                                            </tr>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell" colspan="5">
                                                    <table class="table table-bordered border-dark-3" style="width: 100%">
                                                        <tbody>
                                                            <tr>
                                                                <th class="cell">พื้นที่ใช้งาน</th>
                                                                @foreach ($fl_log_locate as $kloc=>$vloc)                
                                                                    <td class="cell" colspan="{{  count($fl_log_locate[$kloc]) }}" style="text-align: center">{{ $locate_data[$kloc] }}</td>  
                                                                @endforeach
                                                            </tr> 
                                                            <tr>
                                                                <th class="cell">หมายเลขรถโฟร์คลิฟท์</th>           
                                                                @foreach ($fl_log_locate as $kloc=>$vloc)
                                                                    @foreach ($vloc as $kfl=>$vfl)                                                                                
                                                                        <td class="cell">{{ $kfl }}</td>  
                                                                    @endforeach
                                                                @endforeach
                                                            </tr> 
                                                            <tr>
                                                                <th class="cell" style="background-color:#E3E3DB">AVG กะ B</th>
                                                                @foreach ($fl_log_locate as $kloc=>$vloc)
                                                                    @foreach ($vloc as $kfl=>$vfl)                      
                                                                        @php
                                                                            // if(!empty($to_gp4['sum'][$kloc][$kfl]['B']))    $avg_val['B'] = $to_gp4['sum'][$kloc][$kfl]['B']/$to_gp4['count'][$kloc][$kfl]['B'];
                                                                            // else    $avg_val['B'] = 0;
                                                                        @endphp
                                                                        {{-- <td style="text-align: right;">@if(!empty($avg_val['B'])){{ number_format($avg_val['B'],1) }}@endif</td>    --}}
                                                                        <td style="text-align: right;">@if(!empty($togp4['B'][$kfl])){{ number_format($togp4['B'][$kfl],1) }}@endif</td>                  
                                                                    @endforeach                                        
                                                                @endforeach
                                                            </tr>
                                                            <tr>                
                                                                <th class="cell" style="background-color:#E3E3DB">AVG กะ C</th>
                                                                @foreach ($fl_log_locate as $kloc=>$vloc)
                                                                    @foreach ($vloc as $kfl=>$vfl)                      
                                                                        @php
                                                                            // if(!empty($to_gp4['sum'][$kloc][$kfl]['C']))    $avg_val['C'] = $to_gp4['sum'][$kloc][$kfl]['C']/$to_gp4['count'][$kloc][$kfl]['C'];
                                                                            // else    $avg_val['C'] = 0;
                                                                        @endphp
                                                                        {{-- <td style="text-align: right;">@if(!empty($avg_val['C'])){{ number_format($avg_val['C'],1) }}@endif</td> --}}
                                                                        <td style="text-align: right;">@if(!empty($togp4['C'][$kfl])){{ number_format($togp4['C'][$kfl],1) }}@endif</td>                        
                                                                    @endforeach                                        
                                                                @endforeach
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </th>                                                    
                                            </tr>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell" colspan="5">5.กราฟแสดงชั่วโมงการใช้รถแยกคันในเดือน {{ date ("m/Y", strtotime($ed_date)) }}</th>                                                    
                                            </tr>
                                            @foreach ($to_show_gp5 as $key=>$value)
                                                @foreach ($value as $key1=>$value2)
                                                    <tr>
                                                        <th class="cell"></th>
                                                        <th class="cell" colspan="5">
                                                            <canvas id="myChart_{{ $key }}{{ $key1 }}" style="width:100%;max-width:600px"></canvas>
                                                        </th>                                                    
                                                    </tr>
                                                @endforeach    
                                            @endforeach
                                        </thead>
                                    </table>
                                </div><!--//table-responsive-->
                            
                            </div><!--//app-card-body-->          
                        </div><!--//app-card-->                    
                    </div><!--//tab-pane-->			        
                </div><!--//tab-content-->
                
                
                
            </div><!--//container-fluid-->
        </div><!--//app-content-->	    
        
    </div><!--//app-wrapper-->  
    <script src="{{ asset('assets/js/Chart.js') }}"></script>
    <script>
        const chk_arr = <?php echo json_encode($gp_2); ?>;
        console.log(chk_arr);
        console.log('chk_arr');
        new Chart("myChart2", {
            type: 'bar',
            data: {
                labels: ["กะ B", "กะ C"],
                datasets: [
                    {
                        label: 'R',
                        data: [chk_arr['R']['B'], chk_arr['R']['C']],
                        backgroundColor: '#13AC31',
                    },
                    {
                        label: 'S',
                        data: [chk_arr['S']['B'], chk_arr['S']['C']],
                        backgroundColor: '#FAFA1C',
                    },
                    {
                        label: 'A',
                        data: [chk_arr['A']['B'], chk_arr['A']['C']],
                        backgroundColor: '#E3E3DB',
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'สัดส่วนการใช้งานรถ FL (รถเช่า) วันที่ {{ $ed_date }}'
                },            
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                },
                legend: {
                    display: false,
                },
                maintainAspectRatio: false,
            }
        });
        // console.log(<?php echo json_encode($to_show); ?>);
        const chk_arr3 = <?php echo json_encode($gp_3); ?>;
        new Chart("myChart3", {
            type: 'bar',
            data: {
                labels: <?php echo json_encode($to_show); ?>,
                datasets: [
                    {
                        label: 'กะ B',
                        data: chk_arr3['B'],
                        borderColor: '#000000',
                        backgroundColor: '#F5A50F',
                    },
                    {
                        label: 'กะ C',
                        data: chk_arr3['C'],
                        borderColor: '#000000',
                        backgroundColor: '#F5A50F',
                    }
                ]
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        position: 'top'
                    },
                },
                title: {
                    display: true,
                    text: 'กราฟแสดงชั่วโมงการใช้รถประจำวันที่ {{ $ed_date }}'
                }
            }
        });
        const chk_arr4 = <?php echo json_encode($gp_4); ?>;
        console.log(chk_arr4);
        new Chart("myChart4", {
            type: 'bar',
            data: {
                labels: <?php echo json_encode($to_show4); ?>,
                datasets: [
                    {
                        label: 'กะ B',
                        data: chk_arr4['B'],
                        borderColor: '#FFFFFF',
                        backgroundColor: '#0912CF',
                    },
                    {
                        label: 'กะ C',
                        data: chk_arr4['C'],
                        borderColor: '#FFFFFF',
                        backgroundColor: '#C8CAF5',
                    }
                ]
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        position: 'top'
                    }
                },
                title: {
                    display: true,
                    text: 'กราฟแสดงชั่วโมงการใช้รถโดยเฉลี่ยในเดือน {{ date("m/Y") }}'
                }
            }
        });

        const arr_fl = <?php echo json_encode($to_show_gp5); ?>;
        const day_loop = <?php echo json_encode($date_in_month); ?>;
        // console.log(day_loop);
        const label_show = [];
        for (let index = 1; index <= day_loop; index++) {
            label_show.push(index);
        }        
        Object.keys(arr_fl).forEach(key => {
            // console.log(key); // 👉️ "name", "country"
            // console.log(arr_fl[key]); // 👉️ "Tom", "Chile"
            Object.keys(arr_fl[key]).forEach(key1 => {              
                new Chart("myChart_"+key+key1, {
                    type: 'bar',
                    data: {
                        labels: label_show,
                        datasets: [
                            {
                                label: 'กะ B',
                                data: arr_fl[key][key1]['B'],
                                borderColor: '#FFFFFF',
                                backgroundColor: '#639FFA',
                            },
                            {
                                label: 'กะ C',
                                data: arr_fl[key][key1]['C'],
                                borderColor: '#FFFFFF',
                                backgroundColor: '#639FFA',
                            }
                        ]
                    },
                    options: {
                        responsive: true,
                        plugins: {
                            legend: {
                                position: 'top'
                            }
                        },
                        title: {
                            display: true,
                            text: key+' (พื้นที่ปฏิบัติงาน '+key1+') ในเดือน {{ date('m/Y') }}'
                        }
                    }
                });
            });
        });  
        
    </script>
@endsection