@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-3">
        <div class="app-container-xl">
            
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <div class="d-flex flex-row">
                        <span class="nav-item">
                            <a class="btn-sm app-btn-secondary" href="{{ route('FL_report') }}">
                                <i class="fas fa-long-arrow-alt-left"></i>
                                Back
                            </a>
                        </span> 
                        <h1 class="app-page-title mb-0">กราฟแสดงชั่วโมงการใช้รถประจำวัน {{ date ("d/m/Y", strtotime($st_date)) }}</h1>
                    </div>
                </div>                
            </div><!--//row onclick="getURL($key);" -->            
            
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>

            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <canvas id="myChart" style="width:100%;max-width:600px"></canvas>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->     
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
    <script src="{{ asset('assets/js/Chart.js') }}"></script>
    <script>
        const chk_arr3 = <?php echo json_encode($gp_3); ?>;
        const label3 = <?php echo json_encode($to_show); ?>;
        console.log(label3);
        new Chart("myChart", {
            type: 'bar',
            data: {
                labels: label3,
                datasets: [
                    {
                        label: 'กะ B',
                        data: chk_arr3['B'],
                        borderColor: '#000000',
                        backgroundColor: '#F5A50F',
                    },
                    {
                        label: 'กะ C',
                        data: chk_arr3['C'],
                        borderColor: '#000000',
                        backgroundColor: '#F5A50F',
                    }
                ]
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        position: 'top'
                    },
                },
                title: {
                    display: true,
                    text: 'กราฟแสดงชั่วโมงการใช้รถประจำวันที่ {{ date ("d/m/Y", strtotime($st_date)) }}'
                }
            }
        });
    </script>
@endsection