@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            <div class="page-heading row">        
                <div class="pull-right col-auto">
                    <a class="btn btn-success" href="{{ route('FL_import_export') }}">Back</a>
                </div>
                <h3 class="col">{{ __('ชั่วโมงการใช้งานรถ FL') }} -> ข้อมูลไม่ถูกต้อง</h3>
            </div>
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>              
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <table class="table app-table-hover mb-0 text-left">
                            <thead>
                                <tr>
                                    <th class="cell"></th>
                                    <th class="cell">ข้อมูล</th>
                                    <th class="cell">บรรทัดที่</th>
                                    <th class="cell">หมายเหตุ</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $loop_row = 1;
                                @endphp
                                @if(!empty($chk['log_date']))
                                    <tr>                                                                        
                                        <td>{{ $loop_row }}</td>  
                                        @foreach ($chk['log_date'] as $key=>$value)
                                            <td>{{ $key }}</td> 
                                            <td>
                                                @php
                                                    $i=0;
                                                @endphp
                                                @foreach ($value as $krow=>$vrow)
                                                    @if($i<>0)
                                                        {{ ', ' }}
                                                    @endif
                                                    {{ $krow+1 }}
                                                    @php
                                                        $i++;
                                                    @endphp
                                                @endforeach
                                            </td> 
                                            <td>
                                                @foreach ($value as $krow=>$vrow)
                                                    {{ $vrow }}
                                                @endforeach
                                            </td>
                                        @endforeach                                         
                                    </tr> 
                                    @php
                                        $loop_row++;
                                    @endphp 
                                @endif
                                @if(!empty($chk['forklift_id']))
                                    @foreach ($chk['forklift_id'] as $key=>$value)
                                        <tr>                                                                        
                                            <td>{{ $loop_row }}</td>                                          
                                            <td>{{ $key }}</td> 
                                            <td>
                                                @php
                                                    $i=0;
                                                @endphp
                                                @foreach ($value as $krow=>$vrow)
                                                    @if($i<>0)
                                                        {{ ', ' }}
                                                    @endif
                                                    {{ $krow+1 }}
                                                    @php
                                                        $i++;
                                                    @endphp
                                                @endforeach
                                            </td> 
                                            <td>ไม่พบข้อมูลรถ FL</td>                                                                                 
                                        </tr>
                                        @php
                                            $loop_row++;
                                        @endphp 
                                    @endforeach                                     
                                @endif
                                @if(!empty($chk['shift']))
                                    @foreach ($chk['shift'] as $key=>$value)
                                        <tr>                                                                        
                                            <td>{{ $loop_row }}</td>                                          
                                            <td>{{ $key }}</td> 
                                            <td>
                                                @php
                                                    $i=0;
                                                @endphp
                                                @foreach ($value as $krow=>$vrow)
                                                    @if($i<>0)
                                                        {{ ', ' }}
                                                    @endif
                                                    {{ $krow+1 }}
                                                    @php
                                                        $i++;
                                                    @endphp
                                                @endforeach
                                            </td> 
                                            <td>กะ ไม่ถูกต้อง</td>                                                                                 
                                        </tr>
                                        @php
                                            $loop_row++;
                                        @endphp 
                                    @endforeach                                     
                                @endif
                                @if(!empty($chk['type']))
                                    @foreach ($chk['type'] as $key=>$value)
                                        <tr>                                                                        
                                            <td>{{ $loop_row }}</td>                                          
                                            <td>{{ $key }}</td> 
                                            <td>
                                                @php
                                                    $i=0;
                                                @endphp
                                                @foreach ($value as $krow=>$vrow)
                                                    @if($i<>0)
                                                        {{ ', ' }}
                                                    @endif
                                                    {{ $krow+1 }}
                                                    @php
                                                        $i++;
                                                    @endphp
                                                @endforeach
                                            </td> 
                                            <td>สถานะการใช้งาน ไม่ถูกต้อง</td>                                                                                 
                                        </tr>
                                        @php
                                            $loop_row++;
                                        @endphp 
                                    @endforeach                                     
                                @endif
                                @if(!empty($chk['id']))
                                    @foreach ($chk['id'] as $key=>$value)
                                        <tr>                                                                        
                                            <td>{{ $loop_row }}</td>                                          
                                            <td>{{ $key }}</td> 
                                            <td>
                                                @php
                                                    $i=0;
                                                @endphp
                                                @foreach ($value as $krow=>$vrow)
                                                    @if($i<>0)
                                                        {{ ', ' }}
                                                    @endif
                                                    {{ $krow+1 }}
                                                    @php
                                                        $i++;
                                                    @endphp
                                                @endforeach
                                            </td> 
                                            <td>มีการบันทึกค่านี้แล้ว</td>                                                                                 
                                        </tr>
                                        @php
                                            $loop_row++;
                                        @endphp 
                                    @endforeach                                     
                                @endif
                                @if(!empty($chk['time']))
                                    @foreach ($chk['time'] as $key=>$value)
                                        <tr>                                                                        
                                            <td>{{ $loop_row }}</td>                                          
                                            <td>{{ $key }}</td> 
                                            <td>
                                                @php
                                                    $i=0;
                                                @endphp
                                                @foreach ($value as $krow=>$vrow)
                                                    @if($i<>0)
                                                        {{ ', ' }}
                                                    @endif
                                                    {{ $krow+1 }}
                                                    @php
                                                        $i++;
                                                    @endphp
                                                @endforeach
                                            </td> 
                                            <td>ชั่วโมงทำงาน {{ $vrow }} ไม่ถูกต้อง</td>                                                                                 
                                        </tr>
                                        @php
                                            $loop_row++;
                                        @endphp 
                                    @endforeach                                     
                                @endif
                                @if(!empty($chk['excel']))
                                    @foreach ($chk['excel'] as $key=>$value)
                                        <tr>                                                                        
                                            <td>{{ $loop_row }}</td>                                          
                                            <td>{{ $key }}</td> 
                                            <td>
                                                @php
                                                    $i=0;
                                                @endphp
                                                @foreach ($value as $krow=>$vrow)
                                                    @if($i<>0)
                                                        {{ ', ' }}
                                                    @endif
                                                    {{ $krow+1 }}
                                                    @php
                                                        $i++;
                                                    @endphp
                                                @endforeach
                                            </td> 
                                            <td>มีค่าซ้ำกัน</td>                                                                                 
                                        </tr>
                                        @php
                                            $loop_row++;
                                        @endphp 
                                    @endforeach                                     
                                @endif
                                @if(!empty($chk['chk']))
                                    @foreach ($chk['chk'] as $key=>$value)
                                        <tr>                                                                        
                                            <td>{{ $loop_row }}</td>                                          
                                            <td>{{ $key }}</td>                                        
                                            <td>
                                                @php
                                                    $i=0;
                                                @endphp
                                                @foreach ($value as $krow=>$vrow)
                                                    @if($i<>0)
                                                        {{ ', ' }}
                                                    @endif
                                                    {{ $krow+1 }}
                                                    @php
                                                        $i++;
                                                    @endphp
                                                @endforeach
                                            </td> 
                                            <td>ต้องมีชั่วโมงทำงาน</td>                                                                                 
                                        </tr>
                                        @php
                                            $loop_row++;
                                        @endphp 
                                    @endforeach                                     
                                @endif
                                @if(!empty($chk['col']))
                                    @foreach ($chk['col'] as $key=>$value)
                                        <tr>                                                                        
                                            <td>{{ $loop_row }}</td>                                          
                                            <td>{{ $key }}</td>  
                                            @foreach ($value as $krow=>$vrow)
                                                <td>{{ $krow+1 }}</td> 
                                                <td>{{ $vrow }}</td>
                                            @endforeach                                                                    
                                        </tr>
                                        @php
                                            $loop_row++;
                                        @endphp 
                                    @endforeach                                     
                                @endif
                            </tbody>
                        </table>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
@endsection