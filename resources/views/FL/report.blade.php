@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-content">
            <div class="app-container-xl">
                <div class="page-heading row"> 
                    <div class="pull-right col-auto">
                        <span class="nav-item">
                            <a class="btn-sm app-btn-secondary" href="{{ route('FL.index') }}">
                                <i class="fas fa-long-arrow-alt-left"></i>
                                Back
                            </a>
                        </span>
                    </div>
                    <h3 class="col">{{ __('ชั่วโมงการใช้งานรถ FL') }} -> Report</h3>
                </div>
                <nav id="" class="orders-table-tab app-nav-tabs flex-sm-row mb-4">	
                    <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" 
                        href="#" role="tab" aria-controls="orders-all" aria-selected="true"></a>
                </nav>			   
                
                <div class="tab-content" id="orders-table-tab-content">
                    <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
                        <div class="app-card app-card-orders-table shadow-sm mb-5">
                            
                            <form method="GET" action="{{ route('FL_show') }}" accept-charset="UTF-8"
                                class="table-search-form row gx-1 align-items-center" role="search">
                                <div class="app-card-body">
                                    <div class="table-responsive">
                                        <table class="table app-table-hover mb-0 text-left yajra-datatable" id="tb_show">
                                            <thead>
                                                <tr>
                                                    <th class="cell" style="width: 5%"></th>
                                                    <th class="cell" style="text-align:right; width: 20%;">ประเภทรายงาน : </th>
                                                    <th class="cell" colspan="3" style="width: 65%">
                                                        <select name="report_id" class="form-select col" onchange="myFunction(this.value)" required>
                                                            <option value="" selected>==== เลือกประเภทรายงาน ====</option>
                                                            <option value="1">1.ชั่วโมงการใช้งานรถแยกกะการทำงาน </option>
                                                            <option value="2">2.กราฟแสดงสัดส่วนการใช้รถประจำวัน</option>
                                                            <option value="3">3.กราฟแสดงชั่วโมงการใช้รถประจำวัน </option>
                                                            <option value="4">4.กราฟแสดงชั่วโมงการใช้รถโดยเฉลี่ยในเดือนนั้น</option>
                                                            <option value="5">5.กราฟแสดงชั่วโมงการใช้รถแยกคันในเดือนนั้น</option> 
                                                            <option value="6">6.ตารางแสดงปัญหาเมื่อมีเกิดการจอดรถจากรถเสีย (ยังไม่มีตัวอย่าง)</option>
                                                        </select>
                                                    </th>
                                                    <th class="cell"style="width: 10%"></th>
                                                </tr>
                                                <tr>
                                                    <th class="cell" style="width: 5%"></th>
                                                    <th class="cell" style="text-align:right; width: 20%">วันที่ : </th>
                                                    <th class="cell" style="width: 30%">
                                                        <input type="date" class="form-control col" id="st_date" name="st_date" value="{{ date("Y-m-d") }}">
                                                        <input type="month" class="form-control col" id="month_date" name="month_date" value="{{ date("Y-m") }}" style="display: none">
                                                    </th>
                                                    <th class="cell" style="text-align: center" style="width: 5%">
                                                        <div id="date_to">
                                                            ถึงวันที่ : 
                                                        </div>
                                                    </th>
                                                    <th class="cell" style="width: 30%">
                                                        <input type="date" class="form-control col" id="ed_date" name="ed_date" value="{{ date("Y-m-d") }}">
                                                    </th>
                                                    <th class="cell" style="width: 10%"></th>
                                                </tr>
                                                <tr>
                                                    <th class="cell"></th>
                                                    <th class="cell"></th>
                                                    <th class="cell">
                                                        <button class="btn app-btn-primary" type="submit">
                                                            Report
                                                        </button>
                                                    </th>
                                                    <th class="cell"></th>
                                                    <th class="cell">
                                                    </th>
                                                    <th class="cell"></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div><!--//table-responsive-->
                                
                                </div><!--//app-card-body-->                           
                            </form>
                                    
                        </div><!--//app-card-->                    
                    </div><!--//tab-pane-->			        
                </div><!--//tab-content-->
                
                
                
            </div><!--//container-fluid-->
        </div><!--//app-content-->	    
        
    </div><!--//app-wrapper-->  
    <script type="text/javascript"> 
        function myFunction(a) {        
            console.log(a);
            if(a=='1'){                
                document.getElementById('st_date').style.display = "block";
                document.getElementById('date_to').style.display = "block";
                document.getElementById('ed_date').style.display = "block";
                document.getElementById('month_date').style.display = "none";
            }else if(a=='2' || a=='3'){
                document.getElementById('st_date').style.display = "block";
                document.getElementById('date_to').style.display = "none";
                document.getElementById('ed_date').style.display = "none";
                document.getElementById('month_date').style.display = "none";
            }else if(a=='4' || a=='5'){
                document.getElementById('st_date').style.display = "none";
                document.getElementById('date_to').style.display = "none";
                document.getElementById('ed_date').style.display = "none";
                document.getElementById('month_date').style.display = "block";
            }else{
                document.getElementById('st_date').style.display = "block";
                document.getElementById('date_to').style.display = "block";
                document.getElementById('ed_date').style.display = "block";
                document.getElementById('month_date').style.display = "none";
            }
        }
    </script>  
@endsection