@extends('layouts.app-master')
<style>
    label.cameraButton {
        display: inline-block;
        /* margin: 1em 0; */

        /* Styles to make it look like a button */
        padding: 0.5em;
        border: 1px solid #000;
        border-color: #EEE #CCC #CCC #EEE;
        background-color: #DDD;
    }

    /* Look like a clicked/depressed button */
    label.cameraButton:active {
        border-color: #CCC #EEE #EEE #CCC;
    }

    /* This is the part that actually hides the 'Choose file' text box for camera inputs */
    label.cameraButton input[accept*="camera"] {
        display: none;
    }
</style>
@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="page-heading row">        
                <div class="pull-right col-auto">
                    <span class="nav-item">
                        <a class="btn-sm app-btn-secondary" href="{{ route('FL.index') }}">
                            <i class="fas fa-long-arrow-alt-left"></i>
                            Back
                        </a>
                    </span>
                </div>
                <h3 class="col">{{ __('ชั่วโมงการใช้งานรถ FL') }} -> Edit</h3>
            </div>
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>  
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <form method="POST" action="{{ route('FL.update', $fl_log->id) }}" enctype="multipart/form-data" onkeydown="return event.key != 'Enter';">
                            @csrf
                            @method('PUT')

                            <div class="row">   
                                <div class="form-group col-md-2 mb-2 ms-4">
                                    <label for="log_date" class="mt-2 form-label text-right">{{ __('วันที่') }}</label>

                                    <input id="log_date" type="date" class="form-control @error('log_date') is-invalid @enderror"
                                        name="log_date" value="{{ $fl_log->log_date }}" onchange="clear_all('date')" required autocomplete="log_date" autofocus>
                                    @error('log_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-1 mb-2">
                                    <label for="shift" class="mt-2 form-label text-right">{{ __('กะ') }}</label>

                                    <select name="shift" id="shift" class="form-select" onchange="clear_all('shift')" required>
                                        <option value="">กะ</option>
                                        <option value="B" @if($fl_log->shift=='B') selected @endif>B</option>
                                        <option value="C" @if($fl_log->shift=='C') selected @endif>C</option>
                                    </select>
                                    @error('shift')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div> 

                                <div class="form-group col-md-2 mb-2">
                                    <label for="forklift_id" class="mt-2 form-label text-right">{{ __('FL.No') }}</label>
                                    <select class="forklift_id form-control" name="forklift_id" id="forklift_id" onchange="clear_all('fl'); set_loc(this.value)" required>  
                                        {{-- <option value="">Forklift</option>                                     
                                        <option value="{{ $fkl_show[0]['id'] }}" selected >{{ $fkl_show[0]['name'] }}</option> --}}
                                    </select>
                                    
                                    @error('forklift_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div> 

                                <div class="form-group col-md-2 mb-2">
                                    <label for="f_location_id" class="mt-2 form-label text-right">{{ __('ส่วนงานรับผิดชอบดูแล') }}</label>

                                    <select name="f_location_id" id="f_location_id" class="form-select" required>
                                        <option value="">ไม่ระบุ</option>
                                        @foreach ($locate as $key)
                                            <option value="{{ $key->id }}" @if($fl_log->f_location_id==$key->id) selected @endif>{{ $key->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('f_location_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-2 mb-2">
                                    <label for="type" class="mt-2 form-label text-right">{{ __('สถานะ') }}</label>
    
                                    <select name="type" id="type" class="form-select" onchange="myFunction(this.value)" required>
                                        <option value="">ไม่ระบุ</option>
                                        <option value="R" @if($fl_log->type=='R') selected @endif>ใช้งาน</option>
                                        <option value="S" @if($fl_log->type=='S') selected @endif>ไม่ใช้งาน</option>
                                        <option value="A" @if($fl_log->type=='A') selected @endif>รถเสีย</option>
                                    </select>
                                    
                                    @error('type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row" id="use" hidden>                                 
                                <div class="form-group col-md-3 mb-2 ms-4"></div>
                                <div class="form-group col-md-2 mb-2">
                                    <label for="fl_area_name" class="mt-2 form-label text-right">{{ __('พื้นที่ปฏิบัติงาน') }}</label>
                                    <input hidden type="text" id="fl_area_id" name="fl_area_id" value="{{ $fl_log->fl_area_id }}"/>
                                    <input type="text" id="fl_area_name" name="fl_area_name" placeholder="Search" onClick="this.select();" 
                                        class="form-control" width="600px" value="@if(!empty($fa[0]['name'])){{ $fa[0]['name'] }}@endif" autocomplete='off'/>
                                    
                                    @error('fl_area_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-3 mb-2">
                                    <label for="fperson_name" class="mt-2 form-label text-right">{{ __('ผู้ใช้งาน') }}</label>
                                    <input hidden type="text" id="fperson_id" name="fperson_id" value="{{ $fl_log->fperson_id }}"/>
                                    <input type="text" id="fperson_name" name="fperson_name" placeholder="Search" onClick="this.select();" 
                                        class="form-control" width="600px" value="@if(!empty($pp[0]['name'])){{ $pp[0]['name'] }}@endif" autocomplete='off'/>
                                </div>

                                <div class="form-group col-md-3 mb-2">
                                    @php
                                        $set_min = (float)$get_log['time'][$fl_log->forklift_id]+0.1;
                                        $set_max = (float)$get_log['time'][$fl_log->forklift_id]+12;
                                    @endphp
                                    <label for="time" class="mt-2 form-label text-right" id="time_name">ชั่วโมงทำงาน (min: {{ $set_min }} max: {{ $set_max }})</label>
    
                                    <input id="time" type="number" class="form-control" name="time" onClick="this.select();" 
                                        value="{{ number_format((float)$fl_log->time, 1, '.', '') }}" step="any" 
                                        min="{{ $set_min }}" max="{{ $set_max }}">
                                </div> 
                            </div>
                            <div class="row" id="broken" hidden>  
                                <div class="form-group col-md-3 mb-2 ms-4"></div>
                                <div class="form-group col-md-4 mb-2">
                                    <label for="fl_broken_name" class="mt-2 form-label text-right">{{ __('อาการเสีย') }}</label>
                                    <input hidden type="text" id="fl_broken_id" name="fl_broken_id" value="{{ $fl_log->fl_broken_id }}" />
                                    <input type="text" id="fl_broken_name" name="fl_broken_name" placeholder="Search" onClick="this.select();" 
                                        class="form-control" width="600px" value="@if(!empty($fb[0]['name'])){{ $fb[0]['name'] }}@endif" autocomplete='off'/>
                                        
                                    @error('fl_broken_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div> 
                            </div>                             
                            <div class="row ms-4" id="pic">  
                                @php
                                    $pic_name = array('รูปด้านหน้ารถ', 'รูปด้านข้าง(ซ้าย)', 'รูปด้านข้าง(ขวา)', 'รูปด้านหลัง', 'รูปแบตเตอรี่', 'รูปหน้าจอ');
                                    $pic_field = array('head_img', 'left_img', 'right_img', 'back_img', 'batt_img', 'monitor_img');
                                    // $pic_show = array('head_show', 'left_show', 'right_show', 'back_show', 'batt_show', 'monitor_show');
                                    $pic_path = array('head_img_path', 'left_img_path', 'right_img_path', 'back_img_path', 'batt_img_path', 'monitor_img_path');
                                @endphp
                                @foreach($pic_name as $key => $value)
                                    @php
                                        $pic_val = $pic_field[$key];
                                    @endphp
                                    <div class="form-group col-md-2 mb-2">
                                        <label class="cameraButton">{{ $value }}
                                            <input type="file" accept="capture=camera; image/png, image/jpeg, image/jpg;" 
                                                name="show_{{ $key }}" id="show_{{ $key }}" 
                                                onchange="show_img(event,'{{ $key }}')"/>
                                            <input hidden type="text" id="img_{{ $key }}" name="img_{{ $key }}" value="{{ $fl_log->$pic_val }}" required>                    
                                        </label>  
                                        @php
                                            $path_val = $pic_path[$key];
                                            $fs = '';
                                            if($fl_log->$path_val){
                                                $size_img = getimagesize($fl_log->$path_val);
                                                $i_w = $size_img[0];
                                                $i_h = $size_img[1];    
                                                if($i_w<$i_h){
                                                    $fs = 'height=135px'; 
                                                }else{
                                                    $fs = 'width=100px'; 
                                                } 
                                                $path = $fl_log->$path_val;
                                                $type = pathinfo($path, PATHINFO_EXTENSION);
                                                $data = file_get_contents($path);
                                                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);  
                                            }                                        
                                        @endphp
                                        <div class="row my-2"> 
                                            <div id="content_{{ $key }}"> 
                                                @if(!empty($fl_log->$path_val))                                                     
                                                    <div class="alert alert-dismissible fade show" role="alert">
                                                        <img id="output_{{ $key }}" name="output_{{ $key }}" {{ $fs }} 
                                                            src="{{ $base64 }}" />
                                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" onclick="del_img(event)"></button>
                                                    </div>
                                                @endif
                                            </div>  
                                        </div>
                                    </div> 
                                @endforeach                                
                            </div>

                            <div class="form-group row mb-2">
                                <div class="col-md-12 offset-md-6 mb-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->  
    
    <script type="text/javascript">
        window.onload = function(){ 
            var obj = <?php echo json_encode($fl_log); ?>;
            // console.log(obj);
            document.getElementById('forklift_id').innerHTML = '<option value="{{ $fkl_show[0]['id'] }}" selected >{{ $fkl_show[0]['name'] }}</option>'
            myFunction(obj['type']);   
            set_loc(obj['forklift_id'])   
            if(obj['type'] != 'R'){          
                let ele_time = document.getElementById('time');
                ele_time.min = null;
                ele_time.max = null;
            }       
        };

        function show_img(event, id) {
            console.log(event);
            if(file = event.target.files[0]){
                getBase64(file, function(base64Data){
                    // console.log("Base64 of file is", base64Data); 
                    base64_path = base64Data;
                });
                // console.log(file.type);
                if(file.type=='image/jpeg' || file.type=='image/jpg' || file.type=='image/png'){
                    var size_show = '';
                    let img = new Image()
                    img.src = window.URL.createObjectURL(event.target.files[0])
                    img.onload = () => {
                        // console.log('width : '+img.width+' height :'+img.height);
                        if(img.width>img.height)
                            size_show = 'width="100px" ';
                        else    
                            size_show = 'height="135px" ';

                        var element = document.getElementById("content_"+id);   
                        var add_att; 
                        add_att = '<div class="alert alert-dismissible fade show" role="alert">';
                        add_att += '<img id="output_'+id+'" src="'+base64_path+'" '+size_show+' />'
                        add_att += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" onclick="del_img(event,\''+id+'\')"></button>';
                        add_att += '</div>';
                        element.innerHTML = add_att;
                        
                        var element2 = document.getElementById('img_'+id);
                        var element3 = document.getElementById('show_'+id);
                        element2.value = element3.value;

                    }
                }else{
                    alert('กรุณาเลือกรูปที่มีนามสกุลเป็น JPG, JPEG, PNG เท่านั้น');
                }                
            }
        };

        function getBase64 (file, callback) {
            const reader = new FileReader();
            reader.addEventListener('load', () => callback(reader.result));
            reader.readAsDataURL(file);
        }

        function del_img(event, id) {
            console.log('event');
            console.log(event);
            var ele_show = document.getElementById('show_'+id);
            ele_show.value = null; 
            var ele_img = document.getElementById('img_'+id);
            ele_img.value = null; 
        }

        function myFunction(a) {        
            console.log(a);   
            if(a=='A'){ //รถเสีย
                document.getElementById('use').hidden = true;
                document.getElementById('fl_area_name').required = false;
                document.getElementById('fperson_name').required = false;
                document.getElementById('time').required = false;   

                document.getElementById('broken').hidden = false;
                document.getElementById('fl_broken_name').required = true;  
            }else if(a=='R'){   //ใช้งาน
                document.getElementById('use').hidden = false;
                document.getElementById('fl_area_name').required = true;
                document.getElementById('fperson_name').required = true;
                document.getElementById('time').required = true;                

                document.getElementById('broken').hidden = false;
                document.getElementById('fl_broken_name').required = false;
            }else{  //ไม่ใช้งาน
                document.getElementById('use').hidden = true;
                document.getElementById('fl_area_name').required = false;
                document.getElementById('fperson_name').required = false;
                document.getElementById('time').required = false;

                document.getElementById('broken').hidden = true;
                document.getElementById('fl_broken_name').required = false;
            }
        }   
        
        function set_loc(fl) {
            // $('.forklift_id').val(fl);
            var obj = <?php echo json_encode($get_log); ?>;
            var set_min = parseFloat(parseFloat(obj['time'][fl])+0.1).toFixed(1);
            var set_max = parseFloat(parseFloat(obj['time'][fl])+12).toFixed(1);
            let ele_loc = document.getElementById('f_location_id');
            ele_loc.value = obj['locate'][fl];
            let ele_time = document.getElementById('time');
            // ele_time.value = null
            ele_time.min = set_min;
            ele_time.max = set_max;
            document.getElementById('time_name').innerHTML = 'ชั่วโมงทำงาน (min: '+set_min+' max: '+set_max+')'
        } 

        function clear_all(ctr) {
            if(ctr=='date'){
                document.getElementById('shift').value = ''
                document.getElementById('forklift_id').innerHTML = '<option value="" selected >Forklift</option>'
                document.getElementById('f_location_id').value = ''
                document.getElementById('type').value = ''
            }else{
                if(ctr=='shift'){
                    document.getElementById('forklift_id').innerHTML = '<option value="" selected >Forklift</option>'
                    document.getElementById('f_location_id').value = ''
                    document.getElementById('type').value = ''
                }else{
                    if(ctr=='fl'){
                        document.getElementById('type').value = ''
                    }
                }
            }
            
            document.getElementById('fl_area_name').value = ''
            document.getElementById('fperson_name').value = ''
            document.getElementById('time').value = ''
            document.getElementById('fl_broken_name').value = ''
            myFunction('null')
        }
        
    </script>   
    
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('assets/js/select2.min.js') }}"></script>
    <script type="text/javascript">
        $('.forklift_id').select2({
            placeholder: 'Forklift',
            ajax: {
                url: '{{ url('FL/atc_search') }}',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
    </script>
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js" ></script>
    <script type="text/javascript">        
        var route_area = "{{ url('FL/cmb_area') }}";
        var route_person = "{{ url('FL/cmb_person') }}";
        var route_broken = "{{ url('FL/cmb_broken') }}";
        $('#fl_area_name').typeahead({
            source: function (query, process) {
                return $.get(route_area, {
                    query: query
                }, function (data) {
                    return process(data);
                });
            },
            afterSelect: function (data) {
                //print the id to developer tool's console
                console.log(data.id);
                if(data.id){
                    document.getElementById('fl_area_id').value = data.id
                }
            }            
        });
        $('#fperson_name').typeahead({
            source: function (query, process) {
                return $.get(route_person, {
                    query: query
                }, function (data) {
                    console.log(data);
                    return process(data);
                });
            },
            afterSelect: function (data) {
                //print the id to developer tool's console
                console.log(data.id);
                if(data.id){
                    document.getElementById('fperson_id').value = data.id
                }
            }  
        });
        $('#fl_broken_name').typeahead({
            source: function (query, process) {
                return $.get(route_broken, {
                    query: query
                }, function (data) {
                    return process(data);
                });
            },
            afterSelect: function (data) {
                //print the id to developer tool's console
                console.log(data.id);
                if(data.id){
                    document.getElementById('fl_broken_id').value = data.id
                }
            }            
        });
    </script>
@endsection
