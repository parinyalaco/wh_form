@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <h1 class="app-page-title mb-0">Product Input</h1>
                </div>
                <div class="col-auto">
                    <div class="page-utilities">
                        <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                            <div class="col-auto">						    
                                <a class="btn app-btn-secondary" href="{{ route('pd_import_export') }}">
                                    <i class="fas fa-file-upload"></i>&nbsp;
                                    Upload Excel
                                </a>
                                <a class="btn app-btn-secondary" href="{{ route('pd_report') }}">
                                    <i class="fas fa-file-download"></i>&nbsp;
                                    Report
                                </a>
                            </div>
                        </div><!--//row-->
                    </div><!--//table-utilities-->
                </div><!--//col-auto-->
            </div><!--//row onclick="getURL($key);" -->
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav> 
            
            <div class="col-auto">
                <form method="GET" action="{{ route('pd_input.index') }}" accept-charset="UTF-8"
                class="table-search-form row gx-1 align-items-center" role="search">
                    <div class="form-group row mb-3">
                        <label for="name" class="col-auto form-label">วันที่ </label>
                        <div class="col-auto">
                            <input type="date" class="form-control" id="date_from" name="date_from" @if(!empty($date_from)) value="{{ date("Y-m-d", strtotime($date_from)) }}" @endif>											
                        </div>
                        <label for="name" class="col-auto form-label">ถึงวันที่ </label>
                        <div class="col-auto">
                            <input type="date" class="form-control" id="date_to" name="date_to" @if(!empty($date_to)) value="{{ date("Y-m-d", strtotime($date_to)) }}" @endif>											
                        </div>
                        <div class="col-auto">
                            <button type="submit" class="btn btn-success">Search</button>
                        </div>
                    </div>
                </form>
                
            </div><!--//col-->
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <div class="table-responsive">
                            <table class="table app-table-hover mb-0 text-left">
                                <thead>
                                    <tr>
                                        <th class="cell">วันที่</th>
                                        <th class="cell"></th>
                                        <th class="cell">Material</th>
                                        <th class="cell">PO Short text</th>
                                        <th class="cell">Vendor Description</th>
                                        <th class="cell">PO Quantity</th>
                                        <th class="cell">Order Unit</th>
                                        <th class="cell">รับจริง</th>
                                        <th class="cell">Dif</th>
                                        <th class="cell">รายการ</th>
                                        <th class="cell">ผลสุ่ม</th>
                                        <th class="cell">ประเภท</th>
                                        <th class="cell">หมายเหตุ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($to_show as $key=>$value)
                                        <tr> 
                                            @php $loop_date = 0; @endphp                                               
                                            <td class="cell" rowspan="{{ count($to_show[$key]) }}" style="vertical-align: top; border: 1px solid black">
                                                {{-- <div class="row"> --}}
                                                    <div class="col-4">
                                                        <a onclick="return confirm(&quot;คุณแน่ใจมั้ยว่าต้องการลบรายการของวันที่ {{ $key }} ?&quot;)" title="Delete" href="{{ url('pd_input/list_delete/date/'.$key) }}">
                                                            <i class="far fa-trash-alt"></i>
                                                        </a>
                                                    </div>
                                                    <div class="col-8">{{ $key }}</div>
                                                {{-- </div>   --}}
                                            </td>
                                            @foreach ($value as $kid=>$vid)
                                                @if($loop_date <> 0)<tr>@endif                           
                                                <td class="cell">
                                                    <a onclick="return confirm(&quot;คุณแน่ใจมั้ยว่าต้องการลบรายการนี้ ?&quot;)" title="Delete" href="{{ url('pd_input/list_delete/row/'.$kid) }}">
                                                        <i class="far fa-trash-alt"></i>
                                                    </a>
                                                </td>
                                                <td class="cell">{{ $material[$to_show[$key][$kid]['material_id']]['name'] }}</td>
                                                <td class="cell">{{ $material[$to_show[$key][$kid]['material_id']]['po_shot_text'] }}</td>
                                                <td class="cell">{{ $vendor[$to_show[$key][$kid]['vendor_id']] }}</td>
                                                <td class="cell" style="text-align:right">{{ number_format($to_show[$key][$kid]['po_quantity'],3) }}</td>
                                                <td class="cell">{{ $material[$to_show[$key][$kid]['material_id']]['unit'] }}</td>
                                                <td class="cell" style="text-align:right">@if($to_show[$key][$kid]['input_real']>0){{ number_format($to_show[$key][$kid]['input_real'],3) }}@endif</td>
                                                <td class="cell" style="text-align:right">{{ number_format($to_show[$key][$kid]['input_real']-$to_show[$key][$kid]['po_quantity'],3) }}</td>
                                                <td class="cell" style="text-align:right">@if($to_show[$key][$kid]['input_list']){{ $to_show[$key][$kid]['input_list'] }}@endif</td>
                                                <td class="cell" style="text-align:right">@if($to_show[$key][$kid]['input_random']){{ $to_show[$key][$kid]['input_random'] }}@endif</td>
                                                <td class="cell">{{ $material[$to_show[$key][$kid]['material_id']]['pd_type'] }}</td>
                                                <td class="cell">{{ $to_show[$key][$kid]['note'] }}</td>
                                                @php $loop_date++; @endphp
                                                </tr>
                                            @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div><!--//table-responsive-->                            
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
@endsection