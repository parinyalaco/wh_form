@extends('layouts.app-master')

@section('content')
<div class="app-content p-md-4">
	<header class="ex-header bg-gray">
        <div class="app-container">
             @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="row justify-content-between">
                <div class="col-auto my-4">                    
                    <div class="d-flex flex-row">
                        <span class="nav-item">
                            <a class="btn-sm app-btn-secondary" href="{{ url('pd_input') }}">
                                <i class="fas fa-long-arrow-alt-left"></i>
                                Back
                            </a>
                        </span>  
                        <h1 class="app-page-title mb-0">Product Input - Import</h1>
                    </div> <!-- end of col -->                 
                </div> <!-- end of col -->           
            </div> <!-- end of row -->
        </div> <!-- end of container -->

    </header> <!-- end of ex-header -->  
   
    <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
        <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
            aria-controls="orders-all" aria-selected="true"></a>
    </nav>

    <div class="app-container mt-4">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    
                    {{-- <div class="card-header">Questions - Import</div> --}}
                    <div class="card-body">                        
                        {{-- <div class="row g-4 settings-section">
	                
                            <div class="col-12">
                                <div class="app-card app-card-settings shadow-sm p-4">                                    
                                    <div class="app-card-body col-md-4">
                                        <form action="{{ route('pd_import') }}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group mb-4" style="max-width: 500px; margin: 0 auto;">
                                                <div class="custom-file text-left">
                                                    <input type="file" name="file_upload" class="custom-file-input" id="file_upload">
                                                </div>
                                            
                                                <br>
                                                <button class="btn btn-success">Import</button> --}}
                                                {{-- <a class="btn btn-warning" href="{{ route('quiz_export') }}">Export</a> --}}
                                                {{-- <div class="col-auto" style="float: right; margin-right: 10%; width:50%"><a href="{{ asset('assets/file/test.xlsx') }}">ตัวอย่างไฟล์..</a></div>
                                            </div>
                                        </form>  
                                    </div><!--//app-card-body-->                                    
                                </div><!--//app-card-->
                            </div>
                        </div><!--//row--> --}}
                    </div>

                    <div class="card-body">                        
                        <div class="row g-4 settings-section">
                            @php
                                $type_tbl = array('ไฟล์สินค้า LACO', 'ไฟล์สินค้า อะไหล่ช่าง', 'ไฟล์สินค้า DC วังน้อย');
                                $param = array('laco', 'en', 'dc');
                            @endphp
                            @foreach($type_tbl as $key => $value)
                                <div class="col-12 col-md-4">
                                    <div class="app-card app-card-settings shadow-sm p-4">
                                        <div class="app-card-body">
                                            <form action="{{ route('pd_import',$param[$key]) }}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                <div class="form-group mb-4" style="max-width: 500px; margin: 0 auto;">
                                                    <h3 class="col">{{ $value }}</h3>
                                                    <div class="custom-file text-left">
                                                        <input type="file" name="file_upload" class="custom-file-input" id="file_upload">
                                                    </div>
                                                
                                                    <br>
                                                    <button onClick="hide_page()" class="btn btn-success">Import</button>
                                                    <div class="col-auto" style="float: right; margin-right: 10%; width:50%"><a href="{{ asset('assets/file/'.$param[$key].'.xlsx') }}">ตัวอย่างไฟล์..</a></div>
                                                </div>
                                            </form>  
                                        </div><!--//app-card-body--> 
                                    </div><!--//app-card-->
                                </div>
                            @endforeach
                        </div><!--//row-->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection