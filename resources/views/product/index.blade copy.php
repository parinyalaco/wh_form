@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <h1 class="app-page-title mb-0">Product Input</h1>
                </div>
                <div class="col-auto">
                    <div class="page-utilities">
                        <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                            {{-- <div class="col-auto">
                                <form class="table-search-form row gx-1 align-items-center">
                                    <div class="col-auto">
                                        <input type="text" id="search-orders" name="searchorders" class="form-control search-orders" placeholder="Search">
                                    </div>
                                    <div class="col-auto">
                                        <button type="submit" class="btn app-btn-secondary">Search</button>
                                    </div>
                                </form>
                                
                            </div><!--//col-->
                            <div class="col-auto">
                                
                                <select class="form-select w-auto" >
                                    <option selected value="option-1">All</option>
                                    <option value="option-2">This week</option>
                                    <option value="option-3">This month</option>
                                    <option value="option-4">Last 3 months</option>                                    
                                </select>
                            </div> --}}
                            <div class="col-auto">						    
                                <a class="btn app-btn-secondary" href="{{ route('pd_import_export') }}">
                                    <i class="fas fa-file-upload"></i>&nbsp;
                                    Upload Excel
                                </a>
                                <a class="btn app-btn-secondary" href="{{ route('pd_report') }}">
                                    <i class="fas fa-file-download"></i>&nbsp;
                                    Report
                                </a>
                            </div>
                        </div><!--//row-->
                    </div><!--//table-utilities-->
                </div><!--//col-auto-->
            </div><!--//row onclick="getURL($key);" -->
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>

            <nav id="orders-table-tab" class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                @foreach ($main_import as $key=>$value)
                    <a class="flex-sm-fill text-sm-center nav-link @if($max_id==$key) active @endif" id="tab-{{ $key }}" 
                    data-bs-toggle="tab" href="#tab{{ $key }}" role="tab" aria-controls="tab{{ $key }}" aria-selected="true">
                        {{ $main_import[$key]['name'] }}
                    </a>
                    <a onclick="return confirm(&quot;คุณแน่ใจมั้ยว่าต้องการลบ File นี้ ?&quot;)" title="Delete" href="{{ route('pd_delete',$key) }}">
                        <i class="far fa-trash-alt"></i>
                    </a>
                @endforeach                
            </nav>
            
            
            <div class="tab-content" id="orders-table-tab-content">
                @foreach ($to_show as $key=>$value)
                    <div class="tab-pane fade show @if($max_id==$key) active @endif" id="tab{{ $key }}" role="tabpanel" aria-labelledby="tab-{{ $key }}">
                        <div class="app-card app-card-orders-table shadow-sm mb-5">
                            <div class="app-card-body">
                                <div class="table-responsive">
                                    <table class="table app-table-hover mb-0 text-left">
                                        <thead>
                                            <tr>
                                                <th class="cell">ลำดับ</th>
                                                <th class="cell">วันที่</th>
                                                <th class="cell">Material</th>
                                                <th class="cell">PO Short text</th>
                                                <th class="cell">Vendor Description</th>
                                                <th class="cell">PO Quantity</th>
                                                <th class="cell">Order Unit</th>
                                                <th class="cell">รับจริง</th>
                                                <th class="cell">Dif</th>
                                                <th class="cell">รายการ</th>
                                                <th class="cell">ผลสุ่ม</th>
                                                <th class="cell">ประเภท</th>
                                                <th class="cell">หมายเหตุ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($value as $kid=>$vid)
                                                <tr>
                                                    <td class="cell">{{ $loop->iteration }}</td>
                                                    <td class="cell">{{ $to_show[$key][$kid]['date'] }}</td>
                                                    <td class="cell">{{ $material[$to_show[$key][$kid]['material_id']]['name'] }}</td>
                                                    <td class="cell">{{ $material[$to_show[$key][$kid]['material_id']]['po_shot_text'] }}</td>
                                                    <td class="cell">{{ $vendor[$to_show[$key][$kid]['vendor_id']] }}</td>
                                                    <td class="cell">{{ $to_show[$key][$kid]['po_quantity'] }}</td>
                                                    <td class="cell">{{ $material[$to_show[$key][$kid]['material_id']]['unit'] }}</td>
                                                    <td class="cell">{{ $to_show[$key][$kid]['input_real'] }}</td>
                                                    <td class="cell">{{ $to_show[$key][$kid]['input_real']-$to_show[$key][$kid]['po_quantity'] }}</td>
                                                    <td class="cell">@if($to_show[$key][$kid]['input_list']==1){{ $to_show[$key][$kid]['input_list'] }}@endif</td>
                                                    <td class="cell">@if($to_show[$key][$kid]['input_random']==1){{ $to_show[$key][$kid]['input_random'] }}@endif</td>
                                                    <td class="cell">{{ $material[$to_show[$key][$kid]['material_id']]['pd_type'] }}</td>
                                                    <td class="cell">{{ $to_show[$key][$kid]['note'] }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div><!--//table-responsive-->                            
                            </div><!--//app-card-body-->		
                        </div><!--//app-card-->      
                    </div><!--//tab-pane-->
                @endforeach
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
@endsection