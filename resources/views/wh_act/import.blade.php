@extends('layouts.app-master')
<style type="text/css">
    .overlay {
        background-color:#EFEFEF;
        position: fixed;
        width: 100%;
        height: 100%;
        z-index: 1000;
        top: 0px;
        left: 0px;
        opacity: .5; /* in FireFox */ 
        filter: alpha(opacity=50); /* in IE */
    }
</style>
@section('content')
<div class="app-content p-md-4">
	<header class="ex-header bg-gray">
        <div class="app-container">
            @if ($errors->any())
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
             @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="row justify-content-between">
                <div class="col-auto my-4">  
                    <div class="page-heading row">        
                        <div class="pull-right col-auto">
                            <span class="nav-item">
                                <a class="btn-sm app-btn-secondary" href="{{ route('wh_act.index') }}">
                                    <i class="fas fa-long-arrow-alt-left"></i>
                                    Back
                                </a>
                            </span>
                        </div>                        
                        <h3 class="col">{{ __('งานรับ-จ่ายประจำวัน') }} -> Import</h3>
                    </div>          
                </div> <!-- end of col -->           
            </div> <!-- end of row -->
        </div> <!-- end of container -->

    </header> <!-- end of ex-header -->  
   
    <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
        <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
            aria-controls="orders-all" aria-selected="true"></a>
    </nav>

    <div class="app-container mt-4">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    
                    {{-- <div class="card-header">Questions - Import</div> --}}
                    <div class="card-body">                        
                        <div class="row g-4 settings-section">
	                
                            <div class="col-12 col-md-6">
                                <div class="app-card app-card-settings shadow-sm p-4">
                                    
                                    <div class="app-card-body">
                                        <form action="{{ route('wh_act_import_plan') }}" method="POST" enctype="multipart/form-data">
                                            @csrf                                            
                                            <div class="form-group mb-4" style="max-width: 500px; margin: 0 auto;">
                                                <h3 class="col">{{ __('แผนงาน') }}</h3>                                                
                                                <div class="custom-file text-left">
                                                    <input type="file" name="file_plan" class="custom-file-input" id="file_plan">
                                                </div>
                                            
                                                <br>
                                                <button onClick="hide_page()" class="btn btn-success">Import</button>
                                                <a class="btn btn-danger" onclick="show_input('plan')" 
                                                    data-bs-toggle="modal" data-bs-target="#exampleModal" >Delete</a>
                                                {{-- <a class="btn btn-warning" href="{{ route('quiz_export') }}">Export</a> --}}
                                                <div class="col-auto" style="float: right; margin-right: 10%; width:50%"><a href="{{ asset('assets/file/plan.xlsx') }}">ตัวอย่างไฟล์..</a></div>
                                            </div>
                                        </form>  
                                    </div><!--//app-card-body-->
                                    
                                </div><!--//app-card-->
                            </div>

                            <div class="col-12 col-md-6">
                                <div class="app-card app-card-settings shadow-sm p-4">
                                    
                                    <div class="app-card-body">
                                        <form action="{{ route('wh_act_import_act') }}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group mb-4" style="max-width: 500px; margin: 0 auto;">
                                                <h3 class="col">{{ __('รายการรับ-จ่ายจริง') }}</h3>
                                                <div class="custom-file text-left">
                                                    <input type="file" name="file_act" class="custom-file-input" id="file_act">
                                                </div>
                                            
                                                <br>
                                                <button onClick="hide_page()" class="btn btn-success">Import</button>
                                                {{-- href="{{ route('wh_act_del_import',['type'=>'act','date'=>$userId]) }}" --}}
                                                <a class="btn btn-danger" onclick="show_input('act')" 
                                                    data-bs-toggle="modal" data-bs-target="#exampleModal" >Delete</a>
                                                    {{-- data-mdb-whatever="{{ $loadm->id }}" --}}
                                                <div class="col-auto" style="float: right; margin-right: 10%; width:50%"><a href="{{ asset('assets/file/act.xlsx') }}">ตัวอย่างไฟล์..</a></div>
                                            </div>
                                        </form>  
                                    </div><!--//app-card-body-->
                                    
                                </div><!--//app-card-->
                            </div>
                        </div><!--//row-->

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <form action="{{ route('wh_act_del_import') }}" method="POST">
                                    @csrf
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                                            <div id="test"></div>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            {{-- <textarea id="reason" class="form-control col" name="reason" required></textarea> --}}
                                            <input type="date" id="date" class="form-control col" name="date" required>
                                            <input type="hidden" id="type" class="form-control col" name="type">
                                        </div>
                                        <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        {{-- <hr class="my-4">
                        <div class="row g-4 settings-section">
                            <h1 class="app-page-title">ตัวอย่างสำหรับการ Import</h1>
                            <img src="{{asset('/pic/quiz.jpg')}}"  class="photo" width="100" height="250" data-toggle="modal" data-target="#exampleModal">
                        </div><!--//row-->  --}}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript"> 
    function hide_page()
    {
        var file_plan = document.getElementById('file_plan').value;
        var file_act = document.getElementById('file_act').value;
        // console.log(file_name);
        if(file_plan !="" || file_act !=""){
            // console.log('file');
            var div= document.createElement("div");
            div.className += "overlay";
            document.body.appendChild(div);
        }
    }

    function show_input(type) {
        console.log(type);
        document.getElementById('type').value = type;
        if(type==='plan') var h_text = 'แผนรับเข้า';
        else var h_text = 'รับเข้าจริง';
        let box = document.getElementById('test');
        box.innerHTML = '<a>กรุณาระบุวันที่ต้องการยกเลิก </a><a style="color:red">'+h_text+'</a>';
        // document.getElementById('test').value = 'กรุณาระบุเหตุผลที่ต้องการคืนค่า ใบ order ที่ '+order;
    }
</script>
@endsection