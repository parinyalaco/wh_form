@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-content">
            <div class="app-container-xl">
                <div class="page-heading row"> 
                    <div class="pull-right col-auto">
                        <span class="nav-item">
                            <a class="btn-sm app-btn-secondary" href="{{ route('wh_act.index') }}">
                                <i class="fas fa-long-arrow-alt-left"></i>
                                Back
                            </a>
                        </span>
                    </div>
                    <h3 class="col">{{ __('งานรับ-จ่ายประจำวัน') }} -> Mail</h3>
                </div>
                <nav id="" class="orders-table-tab app-nav-tabs flex-sm-row mb-4">	
                    <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" 
                        href="#" role="tab" aria-controls="orders-all" aria-selected="true"></a>
                </nav>			   
                
                <div class="tab-content" id="orders-table-tab-content">
                    <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
                        <div class="app-card app-card-orders-table shadow-sm mb-5">
                            <div class="app-card-body">
                                <div class="table-responsive">
                                    <table class="table table-borderless" id="tb_show">
                                        <thead>   
                                            <tr>
                                                <th class="cell" style="width: 5%">ตัวอย่าง</th>
                                                <th class="cell" style="text-align:left; width: 20%">TO..ALL</th>
                                                <th class="cell" style="width: 30%"></th>
                                                <th class="cell" style="text-align: center" style="width: 5%"></th>
                                                <th class="cell" style="width: 25%"></th>
                                                <th class="cell" style="width: 15%">
                                                    <a class="btn app-btn-secondary" onClick="return confirm('คุณต้องการส่งเมลล์ใช่รือไม่ ?');" href="{{ route('wh_act_sent_mail') }}" >
                                                        <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;
                                                        ส่งเมลล์
                                                    </a>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell" colspan="5">รายงานสรุปงานประจำวัน แผนกคลังสินค้าแช่แข็ง วันที่ {{ $show_date }}</th>                                                    
                                            </tr>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell" colspan="5">1. งานจ่ายสินค้า</th>                                                    
                                            </tr>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell" colspan="5">
                                                    <table class="table table-bordered border-dark-3" style="font-size: 12px; width: 100%" id="tbl_1">
                                                        <thead>
                                                            <tr style="text-align: center; background-color:#F4B084;">
                                                                <th class="cell" rowspan="3">ลำดับที่</th>
                                                                <th class="cell" rowspan="3">แผนก</th>
                                                                <th class="cell" rowspan="3">รหัสสินค้า</th>
                                                                <th class="cell" colspan="3">เวลา (น.)</th>
                                                                <th class="cell" colspan="7">จำนวน (Kg.)</th>
                                                                <th class="cell" rowspan="3">หมายเหตุ</th>
                                                            </tr> 
                                                            <tr style="text-align: center; background-color:#F4B084;">
                                                                <th class="cell" rowspan="2">Plan</th>
                                                                <th class="cell" rowspan="2">Actual-B</th>
                                                                <th class="cell" rowspan="2">Actual-C</th>

                                                                <th class="cell" rowspan="2">Plan</th>
                                                                <th class="cell" colspan="2">Actual-B</th>
                                                                <th class="cell" colspan="2">Actual-C</th>
                                                                <th class="cell" colspan="2">รวม</th>
                                                            </tr>
                                                            <tr style="text-align: center; background-color:#F4B084;">
                                                                @for($i=0;$i<3;$i++)
                                                                    <th class="cell">Q</th>
                                                                    <th class="cell">P</th>
                                                                @endfor
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @php
                                                                $sum_pd = array();
                                                                $sum_act = array();
                                                                $sum_pd_pl = array();
                                                                $sum_act_pl = array();
                                                                $c_show = array();
                                                                $i = 0;
                                                            @endphp
                                                            @if(!empty($tb_1['จ่าย']))
                                                                @foreach ($tb_1['จ่าย'] as $kst=>$vst)
                                                                    @foreach ($vst as $kpd=>$vpd)
                                                                        <tr>
                                                                            <td class="cell">{{ ++$i }}</td>
                                                                            @if(empty($c_show[$kst]))
                                                                                <td class="cell" rowspan="{{ count($vst) }}" style="text-align: center;">{{ $kst }}</td>
                                                                                @php
                                                                                    $c_show[$kst] = 1;
                                                                                @endphp
                                                                            @endif
                                                                            <td class="cell">{{ $kpd }}</td>
                                                                            <td class="cell">@if(!empty($tb_1['จ่าย'][$kst][$kpd]['time']['plan'])){{ $tb_1['จ่าย'][$kst][$kpd]['time']['plan'] }}@endif</td>
                                                                            <td class="cell" style="background-color:#FFD966">@if(!empty($tb_1['จ่าย'][$kst][$kpd]['time']['actual_b'])){{ $tb_1['จ่าย'][$kst][$kpd]['time']['actual_b'] }}@endif</td>
                                                                            <td class="cell" style="background-color:#A9D08E">@if(!empty($tb_1['จ่าย'][$kst][$kpd]['time']['actual_c'])){{ $tb_1['จ่าย'][$kst][$kpd]['time']['actual_c'] }}@endif</td>
                                                                            <td class="cell" style="text-align: right">
                                                                                @if(!empty($tb_1['จ่าย'][$kst][$kpd]['qty']['plan']))
                                                                                    {{ number_format($tb_1['จ่าย'][$kst][$kpd]['qty']['plan'],2) }}
                                                                                    @php
                                                                                        $to_save = $tb_1['จ่าย'][$kst][$kpd]['qty']['plan'];
                                                                                        if(!empty($sum_act['plan']))   $sum_act['plan'] += $to_save;
                                                                                        else    $sum_act['plan'] = $to_save;
                                                                                    @endphp
                                                                                @endif
                                                                            </td>
                                                                            <td class="cell" style="text-align: right; background-color:#FFD966">
                                                                                @if(!empty($tb_1['จ่าย'][$kst][$kpd]['qty']['actual_b']))
                                                                                    {{ number_format($tb_1['จ่าย'][$kst][$kpd]['qty']['actual_b'],2) }}
                                                                                    @php
                                                                                        $to_save = $tb_1['จ่าย'][$kst][$kpd]['qty']['actual_b'];
                                                                                        if(!empty($sum_pd[$kpd]))   $sum_pd[$kpd] += $to_save;
                                                                                        else    $sum_pd[$kpd] = $to_save;
                                                    
                                                                                        if(!empty($sum_act['actual_b']))   $sum_act['actual_b'] += $to_save;
                                                                                        else    $sum_act['actual_b'] = $to_save;
                                                                                    @endphp
                                                                                @endif
                                                                            </td>
                                                                            <td class="cell" style="text-align: right; background-color:#FFD966">
                                                                                @if(!empty($tb_1['จ่าย'][$kst][$kpd]['pl']['actual_b']))
                                                                                    {{ number_format($tb_1['จ่าย'][$kst][$kpd]['pl']['actual_b'],0) }}
                                                                                    @php
                                                                                        $to_save = $tb_1['จ่าย'][$kst][$kpd]['pl']['actual_b'];
                                                                                        if(!empty($sum_pd_pl[$kpd]))   $sum_pd_pl[$kpd] += $to_save;
                                                                                        else    $sum_pd_pl[$kpd] = $to_save;
                                                    
                                                                                        if(!empty($sum_act_pl['actual_b']))   $sum_act_pl['actual_b'] += $to_save;
                                                                                        else    $sum_act_pl['actual_b'] = $to_save;
                                                                                    @endphp
                                                                                @endif
                                                                            </td>
                                                                            <td class="cell" style="text-align: right; background-color:#A9D08E">
                                                                                @if(!empty($tb_1['จ่าย'][$kst][$kpd]['qty']['actual_c']))
                                                                                    {{ number_format($tb_1['จ่าย'][$kst][$kpd]['qty']['actual_c'],2) }}
                                                                                    @php
                                                                                        $to_save = $tb_1['จ่าย'][$kst][$kpd]['qty']['actual_c'];
                                                                                        if(!empty($sum_pd[$kpd]))   $sum_pd[$kpd] += $to_save;
                                                                                        else    $sum_pd[$kpd] = $to_save;
                                                    
                                                                                        if(!empty($sum_act['actual_c']))   $sum_act['actual_c'] += $to_save;
                                                                                        else    $sum_act['actual_c'] = $to_save;
                                                                                    @endphp
                                                                                @endif
                                                                            </td>
                                                                            <td class="cell" style="text-align: right; background-color:#A9D08E">
                                                                                @if(!empty($tb_1['จ่าย'][$kst][$kpd]['pl']['actual_c']))
                                                                                    {{ number_format($tb_1['จ่าย'][$kst][$kpd]['pl']['actual_c'],0) }}
                                                                                    @php
                                                                                        $to_save = $tb_1['จ่าย'][$kst][$kpd]['pl']['actual_c'];
                                                                                        if(!empty($sum_pd_pl[$kpd]))   $sum_pd_pl[$kpd] += $to_save;
                                                                                        else    $sum_pd_pl[$kpd] = $to_save;
                                                    
                                                                                        if(!empty($sum_act_pl['actual_c']))   $sum_act_pl['actual_c'] += $to_save;
                                                                                        else    $sum_act_pl['actual_c'] = $to_save;
                                                                                    @endphp
                                                                                @endif
                                                                            </td>
                                                                            <td class="cell" style="text-align: right">
                                                                                @if(!empty($sum_pd[$kpd]))
                                                                                    {{ number_format($sum_pd[$kpd],2) }}
                                                                                    @php
                                                                                        if(!empty($sum_act['sum']))   $sum_act['sum'] += $sum_pd[$kpd];
                                                                                        else    $sum_act['sum'] = $sum_pd[$kpd];
                                                                                    @endphp
                                                                                @endif
                                                                            </td> 
                                                                            <td class="cell" style="text-align: right;">
                                                                                @if(!empty($sum_pd_pl[$kpd]))
                                                                                    {{ number_format($sum_pd_pl[$kpd],0) }}
                                                                                    @php
                                                                                        if(!empty($sum_act_pl['sum']))   $sum_act_pl['sum'] += $sum_pd_pl[$kpd];
                                                                                        else    $sum_act_pl['sum'] = $sum_pd_pl[$kpd];
                                                                                    @endphp
                                                                                @endif
                                                                            </td>
                                                                            <td class="cell">
                                                                                <input class="form-control" onfocus="this.select();" id="reason[{{ $i }}]" name="reason[{{ $i }}]"
                                                                                    value="@if(!empty($tb_1['จ่าย'][$kst][$kpd]['note'])){{ $tb_1['จ่าย'][$kst][$kpd]['note'] }}@endif"
                                                                                    onchange="note_change({{ $i }})" onkeypress="return key_up(event,{{ $i}},'{{ $kst }}','{{ $kpd }}','@if(!empty($tb_1['จ่าย'][$kst][$kpd]['note'])){{ $tb_1['จ่าย'][$kst][$kpd]['note'] }}@endif');">
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                @endforeach
                                                            @endif
                                                            <tr>
                                                                <td class="cell" colspan="6" style="text-align: right">รวม</td>
                                                                <td class="cell" style="text-align: right">@if(!empty($sum_act['plan'])){{ number_format($sum_act['plan'],2) }}@endif</td>
                                                                <td class="cell" style="text-align: right">@if(!empty($sum_act['actual_b'])){{ number_format($sum_act['actual_b'],2) }}@endif</td>
                                                                <td class="cell" style="text-align: right">@if(!empty($sum_act_pl['actual_b'])){{ number_format($sum_act_pl['actual_b'],0) }}@endif</td>
                                                                <td class="cell" style="text-align: right">@if(!empty($sum_act['actual_c'])){{ number_format($sum_act['actual_c'],2) }}@endif</td>
                                                                <td class="cell" style="text-align: right">@if(!empty($sum_act_pl['actual_c'])){{ number_format($sum_act_pl['actual_c'],0) }}@endif</td>
                                                                <td class="cell" style="text-align: right">@if(!empty($sum_act['sum'])){{ number_format($sum_act['sum'],2) }}@endif</td>
                                                                <td class="cell" style="text-align: right">@if(!empty($sum_act_pl['sum'])){{ number_format($sum_act_pl['sum'],0) }}@endif</td>
                                                                <td class="cell" colspan="6" style="text-align: right"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </th>                                                    
                                            </tr>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell" colspan="5">2.กราฟเปรียบเทียบปริมาณคาดการณ์กับการจ่ายสินค้าจริง</th>                                                    
                                            </tr>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell" colspan="5">
                                                    <div class="row">
                                                    <canvas class="col" id="myChart_PK" style="width:40%;max-width:500px"></canvas>
                                                    <canvas class="col" id="myChart_PF" style="width:40%;max-width:500px"></canvas>
                                                </th>                                                    
                                            </tr>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell" colspan="5">3. งานรับสินค้า</th>                                                    
                                            </tr>
                                            <tr>
                                                <th class="cell"></th>
                                                <th class="cell" colspan="5">
                                                    <table class="table table-bordered border-dark-3" style="width: 80%">
                                                        <thead>
                                                            <tr style="text-align: center; background-color:#F4B084;">
                                                                <th class="cell" rowspan="2">ลำดับที่ </th>
                                                                <th class="cell" rowspan="2">แผนก</th>
                                                                <th class="cell" rowspan="2">รหัสสินค้า</th>
                                                                <th class="cell" colspan="2">B (Kg.)</th>
                                                                <th class="cell" colspan="2">C (Kg.)</th>
                                                                <th class="cell" colspan="2">รวม</th>
                                                            </tr>
                                                            <tr style="text-align: center; background-color:#F4B084;">
                                                                @for($i=0;$i<3;$i++)
                                                                    <th class="cell">Q</th>
                                                                    <th class="cell">P</th>
                                                                @endfor
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @php
                                                                $sum_pd = array();
                                                                $sum_act = array();
                                                                $sum_pd_pl = array();
                                                                $sum_act_pl = array();
                                                                $c_show = array();
                                                                $i = 0;
                                                            @endphp
                                                            @if(!empty($tb_1['รับ']))
                                                                @foreach ($tb_1['รับ'] as $kst=>$vst)
                                                                    @foreach ($vst as $kpd=>$vpd)
                                                                        <tr>
                                                                            <td class="cell">{{ ++$i }}</td>
                                                                            @if(empty($c_show[$kst]))
                                                                                <td class="cell" rowspan="{{ count($vst) }}" style="text-align: center;">{{ $kst }}</td>
                                                                                @php
                                                                                    $c_show[$kst] = 1;
                                                                                @endphp
                                                                            @endif
                                                                            <td class="cell">{{ $kpd }}</td>
                                                                            <td class="cell" style="text-align: right; background-color:#FFD966">
                                                                                @if(!empty($tb_1['รับ'][$kst][$kpd]['qty']['actual_b']))
                                                                                    {{ number_format($tb_1['รับ'][$kst][$kpd]['qty']['actual_b'],2) }}
                                                                                    @php
                                                                                        $to_save = $tb_1['รับ'][$kst][$kpd]['qty']['actual_b'];
                                                                                        if(!empty($sum_pd[$kpd]))   $sum_pd[$kpd] += $to_save;
                                                                                        else    $sum_pd[$kpd] = $to_save;
                                                    
                                                                                        if(!empty($sum_act['B']))   $sum_act['B'] += $to_save;
                                                                                        else    $sum_act['B'] = $to_save;
                                                                                    @endphp
                                                                                @endif
                                                                            </td>
                                                                            <td class="cell" style="text-align: right; background-color:#FFD966">
                                                                                @if(!empty($tb_1['รับ'][$kst][$kpd]['pl']['actual_b']))
                                                                                    {{ number_format($tb_1['รับ'][$kst][$kpd]['pl']['actual_b'],0) }}
                                                                                    @php
                                                                                        $to_save = $tb_1['รับ'][$kst][$kpd]['pl']['actual_b'];
                                                                                        if(!empty($sum_pd_pl[$kpd]))   $sum_pd_pl[$kpd] += $to_save;
                                                                                        else    $sum_pd_pl[$kpd] = $to_save;
                                                    
                                                                                        if(!empty($sum_act_pl['B']))   $sum_act_pl['B'] += $to_save;
                                                                                        else    $sum_act_pl['B'] = $to_save;
                                                                                    @endphp
                                                                                @endif
                                                                            </td>
                                                                            <td class="cell" style="text-align: right; background-color:#A9D08E">
                                                                                @if(!empty($tb_1['รับ'][$kst][$kpd]['qty']['actual_c']))
                                                                                    {{ number_format($tb_1['รับ'][$kst][$kpd]['qty']['actual_c'],2) }}
                                                                                    @php
                                                                                        $to_save = $tb_1['รับ'][$kst][$kpd]['qty']['actual_c'];
                                                                                        if(!empty($sum_pd[$kpd]))   $sum_pd[$kpd] += $to_save;
                                                                                        else    $sum_pd[$kpd] = $to_save;
                                                    
                                                                                        if(!empty($sum_act['C']))   $sum_act['C'] += $to_save;
                                                                                        else    $sum_act['C'] = $to_save;
                                                                                    @endphp
                                                                                @endif
                                                                            </td> 
                                                                            <td class="cell" style="text-align: right; background-color:#A9D08E">
                                                                                @if(!empty($tb_1['รับ'][$kst][$kpd]['pl']['actual_c']))
                                                                                    {{ number_format($tb_1['รับ'][$kst][$kpd]['pl']['actual_c'],0) }}
                                                                                    @php
                                                                                        $to_save = $tb_1['รับ'][$kst][$kpd]['pl']['actual_c'];
                                                                                        if(!empty($sum_pd_pl[$kpd]))   $sum_pd_pl[$kpd] += $to_save;
                                                                                        else    $sum_pd_pl[$kpd] = $to_save;
                                                    
                                                                                        if(!empty($sum_act_pl['C']))   $sum_act_pl['C'] += $to_save;
                                                                                        else    $sum_act_pl['C'] = $to_save;
                                                                                    @endphp
                                                                                @endif
                                                                            </td>
                                                                            <td class="cell" style="text-align: right">
                                                                                @if(!empty($sum_pd[$kpd]))
                                                                                    {{ number_format($sum_pd[$kpd],2) }}
                                                                                    @php
                                                                                        if(!empty($sum_act['sum']))   $sum_act['sum'] += $sum_pd[$kpd];
                                                                                        else    $sum_act['sum'] = $sum_pd[$kpd];
                                                                                    @endphp
                                                                                @endif
                                                                            </td>
                                                                            <td class="cell" style="text-align: right">
                                                                                @if(!empty($sum_pd_pl[$kpd]))
                                                                                    {{ number_format($sum_pd_pl[$kpd],0) }}
                                                                                    @php
                                                                                        if(!empty($sum_act_pl['sum']))   $sum_act_pl['sum'] += $sum_pd_pl[$kpd];
                                                                                        else    $sum_act_pl['sum'] = $sum_pd_pl[$kpd];
                                                                                    @endphp
                                                                                @endif
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                @endforeach
                                                            @endif
                                                            <tr>
                                                                <td class="cell" colspan="3" style="text-align: right">รวม</td>
                                                                <td class="cell" style="text-align: right">@if(!empty($sum_act['B'])){{ number_format($sum_act['B'],2) }}@endif</td>
                                                                <td class="cell" style="text-align: right">@if(!empty($sum_act_pl['B'])){{ number_format($sum_act_pl['B'],0) }}@endif</td>
                                                                <td class="cell" style="text-align: right">@if(!empty($sum_act['C'])){{ number_format($sum_act['C'],2) }}@endif</td>
                                                                <td class="cell" style="text-align: right">@if(!empty($sum_act_pl['C'])){{ number_format($sum_act_pl['C'],0) }}@endif</td>
                                                                <td class="cell" style="text-align: right">@if(!empty($sum_act['sum'])){{ number_format($sum_act['sum'],2) }}@endif</td>
                                                                <td class="cell" style="text-align: right">@if(!empty($sum_act_pl['sum'])){{ number_format($sum_act_pl['sum'],0) }}@endif</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </th>                                                    
                                            </tr>                                            
                                        </thead>
                                    </table>
                                </div><!--//table-responsive-->
                            
                            </div><!--//app-card-body-->          
                        </div><!--//app-card-->                    
                    </div><!--//tab-pane-->			        
                </div><!--//tab-content-->
                
                
                
            </div><!--//container-fluid-->
        </div><!--//app-content-->	    
        
    </div><!--//app-wrapper--> 
    <script src="{{ asset('assets/js/Chart.js') }}"></script>
    <script>
        const chk_arr = <?php echo json_encode($graph_hv); ?>;
        // console.log(chk_arr);
        // const label_show = [];     
        Object.keys(chk_arr).forEach(key => {
            // console.log(key); // 👉️ "name", "country"
            // console.log(arr_fl[key]); // 👉️ "Tom", "Chile"
            // Object.keys(arr_fl[key]).forEach(key1 => {              
                new Chart("myChart_"+key, {
                    type: 'bar',
                    data: {
                        labels: chk_arr[key]['head'],
                        datasets: [
                            {
                                label: 'plan',
                                data: chk_arr[key]['plan'],
                                borderColor: '#1065FA',
                                backgroundColor: '#1065FA',
                            },
                            {
                                label: 'actual',
                                data: chk_arr[key]['actual'],
                                borderColor: '#21A80B',
                                backgroundColor: '#21A80B',
                            },
                            {
                                label: 'diff',
                                data: chk_arr[key]['diff'],
                                borderColor: '#F4150B',
                                backgroundColor: '#F4150B',
                            }
                        ]
                    },
                    options: {
                        responsive: true,
                        plugins: {
                            legend: {
                                position: 'top'
                            }
                        },
                        title: {
                            display: true,
                            text: key
                        },
                        // locale: 'en-US'
                        tooltips: {
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    return tooltipItem.yLabel.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                                }
                            }
                        }
                    }
                });
            // });
        }); 

    </script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script type="text/javascript">  
        function note_change(c) {
            var note_input = document.getElementById('reason['+c+']');            
            // console.log(note_input);
            note_input.style.color = 'red';
            note_input.style.backgroundColor = '#D2D3D2';
        }

        function key_up(e,c,st,pd,old) 
        {
            // look for window.event in case event isn't passed in
            e = e || window.event;
            if (e.keyCode == 13)
            {
                var el = document.getElementById('reason['+c+']');
                var el_v = el.value;
                var titleElement = document.getElementsByClassName("form-control");  
                let c_s = titleElement.length;
                if(el_v != old){
                    // var note_input = document.getElementById('reason');
                    // console.log(c_s);
                    
                    var str = window.location.href;
                    var str_1 = str.split("/").slice(0, -1).join("/")
                    // var link_url = str_1+"/note/"+st+"/"+pd+"/"+el_v;
                    var link_url = str_1+"/note?st="+st+"&pd="+pd+"&value="+el_v;
                    // console.log(e.key);
                    $.get(link_url,function (data) {
                        // console.log(data);
                        if(data=='1'){
                            alert('Success to save '+st+'->'+pd);
                            el.style.color = "";
                            el.style.backgroundColor = "";    
                        }else if(data==0){
                            alert('Can not save.');
                        }
                    });
                }
                
                if((c+1)<=c_s){
                    document.getElementById('reason['+(c+1)+']').focus();
                }
                
            }
        }            
            
    </script>    
@endsection