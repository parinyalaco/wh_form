@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            <div class="page-heading row">        
                <div class="pull-right col-auto">
                    <a class="btn btn-success" href="{{ route('wh_act_import_export') }}">Back</a>
                </div>
                <h3 class="col">{{ __('งานรับ-จ่ายประจำวัน') }} -> ข้อมูลไม่ถูกต้อง</h3>
            </div>
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>              
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <table class="table app-table-hover mb-0 text-left">
                            <thead>
                                <tr>
                                    <th class="cell"></th>
                                    <th class="cell">ข้อมูล</th>
                                    <th class="cell">บรรทัดที่</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $loop_row = 1;
                                @endphp
                                @if(count($er)>0)
                                    @foreach ($er as $key=>$value)
                                        <tr>                                                                        
                                            <td>{{ $loop_row }}</td>                                       
                                            <td>ไม่รู้จัก code : {{ $key }}</td> 
                                            <td>
                                                @php
                                                    $i=0;
                                                @endphp
                                                @foreach ($value as $krow=>$vrow)
                                                    @if($i<>0)
                                                        {{ ', ' }}
                                                    @endif
                                                    {{ $krow+1 }}
                                                    @php
                                                        $i++;
                                                    @endphp
                                                @endforeach
                                            </td>                                        
                                        </tr>
                                        @php
                                            $loop_row++;
                                        @endphp 
                                    @endforeach   
                                @endif                                
                            </tbody>
                        </table>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
@endsection