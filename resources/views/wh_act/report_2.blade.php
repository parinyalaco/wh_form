@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-3">
        <div class="app-container-xl">
            
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <div class="d-flex flex-row">
                        <span class="nav-item">
                            <a class="btn-sm app-btn-secondary" href="{{ route('wh_act_report') }}">
                                <i class="fas fa-long-arrow-alt-left"></i>
                                Back
                            </a>
                        </span> 
                        <h1 class="app-page-title mb-0">งานรับสินค้าวันที่ {{ $show_date }}</h1>
                    </div>
                </div>                
            </div><!--//row onclick="getURL($key);" -->            
            
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>

            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        @if(!empty($tb_1['รับ']))
                            <table class="table table-bordered border-dark-3" style="width: 80%">
                                <thead>
                                    <tr style="text-align: center; background-color:#F4B084;">
                                        <th class="cell">ลำดับที่ </th>
                                        <th class="cell">แผนก</th>
                                        <th class="cell">รหัสสินค้า</th>
                                        <th class="cell">B (Kg.)</th>
                                        <th class="cell">C (Kg.)</th>
                                        <th class="cell">รวม</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $sum_pd = array();
                                        $sum_act = array();
                                        $c_show = array();
                                        $i = 0;
                                    @endphp
                                    @foreach ($tb_1['รับ'] as $kst=>$vst)
                                        @foreach ($vst as $kpd=>$vpd)
                                            <tr>
                                                <td class="cell">{{ ++$i }}</td>
                                                @if(empty($c_show[$kst]))
                                                    <td class="cell" rowspan="{{ count($vst) }}" style="text-align: center;">{{ $kst }}</td>
                                                    @php
                                                        $c_show[$kst] = 1;
                                                    @endphp
                                                @endif
                                                <td class="cell">{{ $kpd }}</td>
                                                <td class="cell" style="text-align: right; background-color:#FFD966">
                                                    @if(!empty($tb_1['รับ'][$kst][$kpd]['qty']['actual_b']))
                                                        {{ number_format($tb_1['รับ'][$kst][$kpd]['qty']['actual_b'],2) }}
                                                        @php
                                                            $to_save = intval($tb_1['รับ'][$kst][$kpd]['qty']['actual_b']);
                                                            if(!empty($sum_pd[$kpd]))   $sum_pd[$kpd] += $to_save;
                                                            else    $sum_pd[$kpd] = $to_save;
                        
                                                            if(!empty($sum_act['B']))   $sum_act['B'] += $to_save;
                                                            else    $sum_act['B'] = $to_save;
                                                        @endphp
                                                    @endif
                                                </td>
                                                <td class="cell" style="text-align: right; background-color:#A9D08E">
                                                    @if(!empty($tb_1['รับ'][$kst][$kpd]['qty']['actual_c']))
                                                        {{ number_format($tb_1['รับ'][$kst][$kpd]['qty']['actual_c'],2) }}
                                                        @php
                                                            $to_save = intval($tb_1['รับ'][$kst][$kpd]['qty']['actual_c']);
                                                            if(!empty($sum_pd[$kpd]))   $sum_pd[$kpd] += $to_save;
                                                            else    $sum_pd[$kpd] = $to_save;
                        
                                                            if(!empty($sum_act['C']))   $sum_act['C'] += $to_save;
                                                            else    $sum_act['C'] = $to_save;
                                                        @endphp
                                                    @endif
                                                </td>  
                                                <td class="cell" style="text-align: right">
                                                    @if(!empty($sum_pd[$kpd]))
                                                        {{ number_format($sum_pd[$kpd],2) }}
                                                        @php
                                                            if(!empty($sum_act['sum']))   $sum_act['sum'] += $sum_pd[$kpd];
                                                            else    $sum_act['sum'] = $sum_pd[$kpd];
                                                        @endphp
                                                    @endif
                                                </td>                      
                                            </tr>
                                        @endforeach
                                    @endforeach
                                    <tr>
                                        <td class="cell" colspan="3" style="text-align: right">รวม</td>
                                        <td class="cell" style="text-align: right">@if(!empty($sum_act['B'])){{ number_format($sum_act['B'],2) }}@endif</td>
                                        <td class="cell" style="text-align: right">@if(!empty($sum_act['C'])){{ number_format($sum_act['C'],2) }}@endif</td>
                                        <td class="cell" style="text-align: right">@if(!empty($sum_act['sum'])){{ number_format($sum_act['sum'],2) }}@endif</td>
                                    </tr>
                                </tbody>
                            </table>
                        @else
                            <h5 class="col">ไม่พบข้อมูลวันที่ {{ $show_date }}</h5>
                        @endif
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->     
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->   
@endsection