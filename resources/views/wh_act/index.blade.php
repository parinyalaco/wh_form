@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <h1 class="app-page-title mb-0">งานรับ-จ่ายประจำวัน</h1>
                </div>
                <div class="col-auto">
                    <div class="page-utilities">
                        <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                            <div class="col-auto">						    
                                <a class="btn app-btn-secondary" href="{{ route('wh_act_import_export') }}">
                                    <i class="fas fa-file-upload"></i>&nbsp;
                                    Upload Excel
                                </a>                                
                                <a class="btn app-btn-secondary" href="{{ route('wh_act_report') }}">
                                    <i class="fas fa-file-download"></i>&nbsp;
                                    Report
                                </a>
                                <a class="btn app-btn-secondary" href="{{ route('wh_act_mail') }}">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;
                                    Sent Mail
                                </a>
                                {{-- <a class="btn app-btn-secondary" href="{{ route('wh_act.create') }}">
                                    <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
                                    Add
                                </a> --}}
                            </div>
                        </div><!--//row-->
                    </div><!--//table-utilities-->
                </div><!--//col-auto-->
            </div><!--//row onclick="getURL($key);" -->
        
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>  
            
            <div class="col-auto">
                <form method="GET" action="{{ route('wh_act.index') }}" accept-charset="UTF-8"
                class="table-search-form row gx-1 align-items-center" role="search">
                    <div class="form-group row mb-3">
                        <label for="name" class="col-auto form-label">วันที่ </label>
                        <div class="col-auto">
                            <input type="date" class="form-control" id="date_from" name="date_from" @if(!empty($date_from)) value="{{ date("Y-m-d", strtotime($date_from)) }}" @endif>											
                        </div>
                        <label for="name" class="col-auto form-label">ถึงวันที่ </label>
                        <div class="col-auto">
                            <input type="date" class="form-control" id="date_to" name="date_to" @if(!empty($date_to)) value="{{ date("Y-m-d", strtotime($date_to)) }}" @endif>											
                        </div>
                        <label for="dep_id" class="col-auto form-label">แผนก</label>
                        <div class="col-auto">
                            <select name="dep_id" class="form-select">
                                <option value=""@if(empty($dep_id)) selected @endif>ไม่ระบุ</option>
                                <option value="PF" @if($dep_id=='PF') selected @endif>PF</option>
                                <option value="PK" @if($dep_id=='PK') selected @endif>PK</option>
                            </select>											
                        </div>
                        <div class="col-auto">
                            <button type="submit" class="btn btn-success">Search</button>
                        </div>
                    </div>
                </form>                
            </div><!--//col-->
            
            <div class="tab-content" id="orders-table-tab-content">                
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <table class="table app-table-hover mb-0 text-left">
                            <thead>
                                <tr>
                                    <th class="cell" rowspan="2">วันที่</th>
                                    <th class="cell" rowspan="2">แผนก</th>
                                    <th class="cell" colspan="2">สินค้า</th>
                                    <th class="cell" colspan="2">เคลื่อนย้าย</th>
                                    <th class="cell" colspan="2">จ่าย</th>
                                    <th class="cell" colspan="2">รับ</th>
                                    <th class="cell" colspan="2">Total</th>
                                </tr>
                                <tr>
                                    <th class="cell">ชื่อ</th>
                                    <th class="cell">หน่วย</th>
                                    @for($i = 0; $i < 4; $i++)
                                        <th class="cell">Qty.</th>
                                        <th class="cell">row</th>
                                    @endfor                   
                                </tr>
                            </thead>
                            <tbody>
                                {{-- $qty[$key->entry_date][$key->store_loc][$key->mat][$key->name][$key->unit_entry] --}}
                                
                                @foreach ($qty as $kdate=>$vdate)
                                    @php $loop_date = 0; @endphp
                                    @foreach ($vdate as $kst=>$vst)
                                        @php $loop_st = 0; @endphp
                                        @foreach ($vst as $kmat=>$vmat)
                                            @foreach ($vmat as $kunit=>$vunit)                    
                                                <tr>
                                                    @php $total_q = 0; $total_p = 0; @endphp
                                                    @if($loop_date==0) 
                                                        @php 
                                                            $loop_date = 1; 
                                                            if(!empty($qty[$kdate]['PF'])){
                                                                if(!empty($qty[$kdate]['PK'])){
                                                                    $row_span = count($qty[$kdate]['PF'])+count($qty[$kdate]['PK']);
                                                                }else{
                                                                    $row_span = count($qty[$kdate]['PF']);
                                                                }                                                                
                                                            }else{
                                                                $row_span = count($qty[$kdate]['PK']);
                                                            }
                                                        @endphp
                                                        <td class="cell" rowspan="{{ $row_span }}" style="vertical-align: top;">
                                                            <div class="row">
                                                                @if(auth()->user()->user_type_id<=2)
                                                                    <div class="col-4">
                                                                        <a onclick="return confirm(&quot;การลบจะทำให้ข้อมูลแผนและรัย-จ่ายหายไป คุณแน่ใจมั้ยว่าต้องการลบรายการของวันที่ {{ $kdate }} ?&quot;)" title="Delete" href="{{ route('wh_act_del',$kdate) }}">
                                                                            <i class="far fa-trash-alt"></i>
                                                                        </a>
                                                                    </div>
                                                                @endif
                                                                <div class="col-8">{{ $kdate }}</div>
                                                            </div>
                                                        </td>
                                                    @endif
                                                    @if($loop_st==0) 
                                                        @php $loop_st = 1; @endphp
                                                        <td class="cell" rowspan="{{ count($vst) }}" style="vertical-align: top;">
                                                            <div class="row">
                                                                <div class="col-8">{{ $kst }}</div>
                                                            </div>
                                                        </td>
                                                    @endif
                                                    <td class="cell">{{ $kmat }}</td>  
                                                    <td class="cell">{{ $kunit }}</td>                                                      
                                                    <td class="cell" style="text-align: right">
                                                        @if(!empty($qty[$kdate][$kst][$kmat][$kunit]['เคลื่อนย้าย']))
                                                            @php $total_q = $qty[$kdate][$kst][$kmat][$kunit]['เคลื่อนย้าย']; @endphp
                                                            {{ number_format($qty[$kdate][$kst][$kmat][$kunit]['เคลื่อนย้าย'],2) }}
                                                        @endif
                                                    </td>
                                                    <td class="cell" style="text-align: right">
                                                        @if(!empty($c_row[$kdate][$kst][$kmat][$kunit]['เคลื่อนย้าย']))
                                                            @php $total_p = $c_row[$kdate][$kst][$kmat][$kunit]['เคลื่อนย้าย']; @endphp
                                                            {{ number_format($c_row[$kdate][$kst][$kmat][$kunit]['เคลื่อนย้าย'],0) }}
                                                        @endif
                                                    </td>                                                    
                                                    <td class="cell" style="text-align: right">
                                                        @if(!empty($qty[$kdate][$kst][$kmat][$kunit]['จ่าย']))
                                                            @php 
                                                                if(empty($total_q))    $total_q = $qty[$kdate][$kst][$kmat][$kunit]['จ่าย']; 
                                                                else    $total_q += $qty[$kdate][$kst][$kmat][$kunit]['จ่าย'];
                                                            @endphp
                                                            {{ number_format($qty[$kdate][$kst][$kmat][$kunit]['จ่าย'],2) }}
                                                        @endif
                                                    </td>
                                                    <td class="cell" style="text-align: right">
                                                        @if(!empty($c_row[$kdate][$kst][$kmat][$kunit]['จ่าย']))
                                                            @php 
                                                                if(empty($total_p))    $total_p = $c_row[$kdate][$kst][$kmat][$kunit]['จ่าย']; 
                                                                else    $total_p += $c_row[$kdate][$kst][$kmat][$kunit]['จ่าย'];
                                                            @endphp
                                                            {{ number_format($c_row[$kdate][$kst][$kmat][$kunit]['จ่าย'],0) }}
                                                        @endif
                                                    </td>                                                   
                                                    <td class="cell" style="text-align: right">
                                                        @if(!empty($qty[$kdate][$kst][$kmat][$kunit]['รับ']))
                                                            @php 
                                                                if(empty($total_q))    $total_q = $qty[$kdate][$kst][$kmat][$kunit]['รับ']; 
                                                                else    $total_q += $qty[$kdate][$kst][$kmat][$kunit]['รับ'];
                                                            @endphp
                                                            {{ number_format($qty[$kdate][$kst][$kmat][$kunit]['รับ'],2) }}
                                                        @endif
                                                    </td>
                                                    <td class="cell" style="text-align: right">
                                                        @if(!empty($c_row[$kdate][$kst][$kmat][$kunit]['รับ']))
                                                            @php 
                                                                if(empty($total_p))    $total_p = $c_row[$kdate][$kst][$kmat][$kunit]['รับ']; 
                                                                else    $total_p += $c_row[$kdate][$kst][$kmat][$kunit]['รับ'];
                                                            @endphp
                                                            {{ number_format($c_row[$kdate][$kst][$kmat][$kunit]['รับ'],0) }}
                                                        @endif
                                                    </td>
                                                    <td class="cell" style="text-align: right">{{ number_format($total_q,2) }}</td>
                                                    <td class="cell" style="text-align: right">{{ number_format($total_p,0) }}</td>                                                       
                                                </tr>
                                            @endforeach                                                   
                                        @endforeach   
                                    @endforeach    
                                @endforeach
                            </tbody>
                        </table>
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->                  
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
@endsection