@extends('layouts.auth-master')

@section('content')
    <form method="post" action="{{ route('login.perform') }}">
        
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <img class="mb-4" src="{{ asset('pic/download.png') }}" alt="" width="72" height="72">
        
        <h1 class="h3 mb-3 fw-normal">Login</h1>

        @include('layouts.partials.messages')

        <div class="form-group mb-3 row">
            <label for="username" class="col-sm-4 form-label my-2">Email or Username</label>
            <input type="text" class="form-control col" name="username" placeholder="Email or Username" value="{{ old('username') }}"  required="required" autofocus>            
            @if ($errors->has('username'))
                <span class="text-danger text-left">{{ $errors->first('username') }}</span>
            @endif
        </div>
        
        <div class="form-group mb-3 row">
            <label for="password" class="col-sm-3 form-label my-2">Password</label>
            <label for="password" class="col-sm-1 form-label my-2"></label>
            <input type="password" class="form-control col" name="password" placeholder="Password" value="{{ old('password') }}" required="required">            
            @if ($errors->has('password'))
                <span class="text-danger text-left">{{ $errors->first('password') }}</span>
            @endif
        </div>

        <div class="form-group mb-3 row">
            <label for="" class="col-sm-4 form-label my-2"></label>
            <button class="w-25 btn btn-lg btn-primary" type="submit">Login</button>
        </div>

        
        
        {{-- @include('auth.partials.copy') --}}
    </form>
@endsection