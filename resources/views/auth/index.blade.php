@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <h1 class="app-page-title mb-0">User</h1>
                </div>
                <div class="col-auto">
                    <div class="page-utilities">
                        <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                            <div class="col-auto">						    
                                <a class="btn app-btn-secondary" href="{{ route('user.create') }}">
                                    <i class="fas fa-plus"></i>&nbsp;
                                    Add
                                </a>
                            </div>
                        </div><!--//row-->
                    </div><!--//table-utilities-->
                </div><!--//col-auto-->
            </div><!--//row onclick="getURL($key);" -->            
            
            <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
                <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                    aria-controls="orders-all" aria-selected="true"></a>
            </nav>

            <div class="tab-content" >
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <div class="table-responsive">
                            <table class="table app-table-hover mb-0 text-left">
                                <thead>
                                    <tr>
                                        <th class="cell">ลำดับ</th>
                                        <th class="cell">ชื่อ-สกุล</th>
                                        <th class="cell">Username</th>
                                        <th class="cell">E-mail</th>                                                
                                        <th class="cell">สถานะ</th>
                                        <th class="cell"></th>
                                        <th class="cell"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($query as $key)
                                        <tr>
                                            <td class="cell">{{ $loop->iteration }}</td>
                                            <td class="cell">{{ $key->name }}</td>
                                            <td class="cell">{{ $key->username }}</td>
                                            <td class="cell">{{ $key->email }}</td>
                                            <td class="cell">{{ $key->type_name }}</td>                                            
                                            <td class="cell">
                                                <a class="btn btn-outline-secondary" href="{{ route('user.edit',$key->id) }}">
                                                    <i class="far fa-edit"></i>&nbsp;
                                                    Edit
                                                </a></td>
                                            <td class="cell">
                                                <a class="btn btn-outline-danger" href="{{ route('user_delete',$key->id) }}">
                                                    <i class="far fa-trash-alt"></i>&nbsp;
                                                    Delete
                                                </a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div><!--//table-responsive-->
                    
                    </div><!--//app-card-body-->		
                </div><!--//app-card-->   
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
@endsection