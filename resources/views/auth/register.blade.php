@extends('layouts.app-master')

@section('content')
    <div class="app-content p-md-4">
        <div class="app-container-xl">
            <div class="page-heading row">        
                <div class="pull-right col-auto">
                    <a class="btn btn-success" href="{{ route('user.index') }}"><i class="fas fa-long-arrow-alt-left"></i> Back</a>
                </div>
                <h3 class="col">{{ __('User Type') }} -> Create</h3>
            </div>         
        </div> <!-- end of row -->

        <nav class="orders-table-tab app-nav-tabs nav shadow-sm flex-column flex-sm-row mb-4">
            <a class="flex-sm-fill text-sm-center nav-link active" data-bs-toggle="tab" href="#" role="tab"
                aria-controls="orders-all" aria-selected="true"></a>
        </nav> 

        <div class="page-content">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        {{-- <div class="card-header">{{ __('แก้ไข User') }}</div> --}}

                        <div class="card-body">
                            <form action="{{ route('user.store') }}" method="POST">
                                @csrf

                                {{-- @method('PUT') 
                                <h1 class="h3 mb-3 fw-normal">Edit</h1>--}}
                                
                                <div class="form-group my-3 row">
                                    <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('ชื่อ') }}</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                            name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group my-3 row">
                                    <label for="username" class="col-md-3 col-form-label text-md-right">{{ __('Username') }}</label>

                                    <div class="col-md-6">
                                        <input id="username" type="text" class="form-control @error('username') is-invalid @enderror"
                                            name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>

                                        @error('username')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group my-3 row">
                                    <label for="email" class="col-md-3 col-form-label text-md-right">{{ __('Email address') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="text" class="form-control @error('email') is-invalid @enderror"
                                            name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group my-3 row">
                                    <label for="user_type_id" class="col-md-3 col-form-label text-md-right">{{ __('ประเภทผู้ใช้') }}</label>

                                    <div class="col-md-6">
                                        {{-- <input id="user_type_id" type="text" class="form-control @error('user_type_id') is-invalid @enderror"
                                            name="user_type_id" value="{{ $user->email }}" required autocomplete="user_type_id" autofocus> --}}
                                        <select name="user_type_id" class="form-select" required>
                                            <option value="" @empty(old('user_type_id')) selected @endempty >ไม่ระบุ</option>
                                            @foreach ($type as $key)											
                                                <option value="{{ $key->id }}" 
                                                    @if($key->id==old('user_type_id'))
                                                        selected
                                                    @endif
                                                >{{ $key->name }}</option>
                                            @endforeach
                                        </select>

                                        @error('user_type_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group my-3 row">
                                    <label for="password" class="col-md-3 col-form-label text-md-right">{{ __('Password') }}</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                                            name="password" value="" required autocomplete="password" autofocus>

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group my-3 row">
                                    <label for="password_confirmation" class="col-md-3 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                    <div class="col-md-6">
                                        <input id="password_confirmation" type="password" class="form-control @error('password_confirmation') is-invalid @enderror"
                                            name="password_confirmation" value="" required autocomplete="password" autofocus>

                                        @error('password_confirmation')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-3">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Save') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection