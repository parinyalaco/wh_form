<?php

use App\Http\Controllers\TR\Test\Test_TRController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WhDepsController;
use App\Http\Controllers\JobDatasController;
use App\Http\Controllers\WhActsContoller;
// use App\Http\Controllers\HomeController;
use App\Http\Controllers\RegisterController;
// use App\Http\Controllers\LoginController;
// use App\Http\Controllers\LogoutController;
use App\Http\Controllers\ProductInputController;
use App\Http\Controllers\UserTypeController;
use App\Http\Controllers\ForkliftController;
use App\Http\Controllers\FlocationController;
use App\Http\Controllers\FpeopleController;
use App\Http\Controllers\WhActTypeController;
use App\Http\Controllers\FLogController;
use App\Http\Controllers\RecvRmOtherPlansController;
use App\Http\Controllers\RmManualController;
use App\Http\Controllers\StProductController;
use App\Http\Controllers\StWhBeginController;
use App\Http\Controllers\StBrokerController;
use App\Http\Controllers\StReceiveController;
use App\Http\Controllers\StWhithdrawController;


use App\Http\Controllers\FS\ShiftController;
use App\Http\Controllers\FS\ProductController;
use App\Http\Controllers\FS\StoreController;
use App\Http\Controllers\FS\StepController;
use App\Http\Controllers\FS\StepTgController;
use App\Http\Controllers\FS\LoseTypeController;
use App\Http\Controllers\FS\ToReuseController;
use App\Http\Controllers\FS\WashController;
use App\Http\Controllers\FS\DryController;
use App\Http\Controllers\FS\ShineController;
use App\Http\Controllers\FS\ChooseBoxController;
use App\Http\Controllers\FS\StockController;
use App\Http\Controllers\FS\ToUseController;
use App\Http\Controllers\FS\LoseController;
use App\Http\Controllers\FS\TargetController;
use App\Http\Controllers\FS\TargetProductController;
use App\Http\Controllers\FS\ReportController;

//Stock FS WH part
use App\Http\Controllers\Fs\FsWhsController;
use App\Http\Controllers\Fs\FsStocksController;
use App\Http\Controllers\FS\FsReportsController;

use App\Http\Controllers\HandheldImportsController;
use App\Http\Controllers\TR\CalendarController;
//Track Schedule Management
use App\Http\Controllers\TR\TrucksController;
use App\Http\Controllers\TR\ImpDeliveryPlansController;
use App\Http\Controllers\TR\ScheduleTrucksController;
use App\Http\Controllers\TR\TailsController;
use App\Http\Controllers\TR\VesselLinesController;
use App\Http\Controllers\TR\HolidaysController;
use App\Http\Controllers\TR\SumaryController;
use App\Http\Controllers\TR\WeekendsController;

//TEST WHRM
use App\Http\Controllers\WHRM\BeanController;
use App\Http\Controllers\WHRM\TestController;
use App\Http\Controllers\WHRM\BananaController;
use App\Http\Controllers\WHRM\setting\WhRmBrokerAreaController;
use App\Http\Controllers\WHRM\WhRmController;
use App\Http\Controllers\WHRM\WhRmDeleteController;
use App\Http\Controllers\WHRM\WhRmEmailController;
use App\Http\Controllers\WHRM\WhRmImportFileController;
use App\Http\Controllers\WHRM\WhRmReportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::group(['namespace' => 'App\Http\Controllers'], function () {
    /**
     * Home Routes
     */
    Route::get('/', 'HomeController@index')->name('home.index');

    Route::resource('whdeps', 'WhDepsController');
    Route::resource('jobdatas', 'JobDatasController');

    Route::get('/whacts/planvsactrpt/{date}', 'WhActsContoller@planvsactrpt');

    Route::group(['middleware' => ['guest']], function () {
        /**
         * Register Routes
         */
        Route::get('/register', 'RegisterController@show')->name('register.show');
        Route::post('/register', 'RegisterController@register')->name('register.perform');
        /**
         * Login Routes
         */
        Route::get('/login', 'LoginController@show')->name('login.show');
        Route::post('/login', 'LoginController@login')->name('login.perform');
    });

    Route::group(['middleware' => ['auth']], function () {
        /**
         * Logout Routes
         */
        Route::get('/logout', 'LogoutController@perform')->name('logout.perform');
    });
});

Route::get('/user/delete/{id}', [RegisterController::class, 'destroy'])->name('user_delete');
Route::resource('user', RegisterController::class);

Route::get('/pd_input/importExportView', [ProductInputController::class, 'importExportView'])->name('pd_import_export');
Route::post('/pd_input/import/{tbl}', [ProductInputController::class, 'import'])->name('pd_import');
Route::get('/pd_input/reportView', [ProductInputController::class, 'reportView'])->name('pd_report');
Route::get('/pd_input/report_show', [ProductInputController::class, 'report_export'])->name('pd_show');
Route::get('/pd_input/file_delete/{id}', [ProductInputController::class, 'destroy'])->name('pd_delete');    //delete file
Route::get('/pd_input/list_delete/{type}/{id}', [ProductInputController::class, 'list_destroy'])->name('pd_list_delete');    //delete list
Route::get('/pd_input/mail/{tbl}/{date_to}', [ProductInputController::class, 'sentmail'])->name('pd_sentmail');
Route::get('/pd_input/sum_mail/{date_to}', [ProductInputController::class, 'sum_mail']);
Route::get('/pd_input/sum_mail_month', [ProductInputController::class, 'sum_mail_month']);
Route::resource('pd_input', ProductInputController::class);

Route::resource('/setting/user_type', UserTypeController::class);

Route::get('/setting/forklift/importExportView', [ForkliftController::class, 'importExportView'])->name('forklift_import_export');
Route::post('/setting/forklift/import', [ForkliftController::class, 'import'])->name('forklift_import');
Route::resource('/setting/forklift', ForkliftController::class);

Route::resource('/setting/location', FlocationController::class);
Route::resource('/setting/people', FpeopleController::class);

Route::get('/setting/WhAct_type/importExportView', [WhActTypeController::class, 'importExportView'])->name('WhAct_type_import_export');
Route::post('/setting/WhAct_type/import', [WhActTypeController::class, 'import'])->name('WhAct_type_import');
Route::resource('/setting/WhAct_type', WhActTypeController::class);

Route::get('/FL/FL_del/{date}', [FLogController::class, 'destroy_date'])->name('FL_del');    //delete file
Route::get('/FL/importExportView', [FLogController::class, 'importExportView'])->name('FL_import_export');
Route::post('/FL/import', [FLogController::class, 'import'])->name('FL_import');
Route::get('/FL/reportView', [FLogController::class, 'reportView'])->name('FL_report');
Route::get('/FL/report_show', [FLogController::class, 'report_export'])->name('FL_show');
// Route::get('/FL/mail/{date_to}', [FLogController::class, 'sentmail'])->name('FL_sentmail');
Route::get('/FL/mailView', [FLogController::class, 'mailView'])->name('FL_mail');
Route::get('/FL/sent_mail', [FLogController::class, 'sent_mail'])->name('FL_sent_mail');
Route::get('/FL/atc_search', [FLogController::class, 'selectfl'])->name('FL_autocomplete');
Route::get('/FL/cmb_area', [FLogController::class, 'cmb_area'])->name('FL_area');
Route::get('/FL/cmb_person', [FLogController::class, 'cmb_person'])->name('FL_person');
Route::get('/FL/cmb_broken', [FLogController::class, 'cmb_broken'])->name('FL_broken');
Route::resource('FL', FLogController::class);


Route::get('/recvrmotherplans/changeStatus/{id}', [RecvRmOtherPlansController::class, 'changeStatus'])->name('recvrmotherplans.changestatus');
Route::resource('recvrmotherplans', RecvRmOtherPlansController::class);

Route::get('/wh_act/importExportView', [WhActsContoller::class, 'importExportView'])->name('wh_act_import_export');
Route::post('/wh_act/import_plan', [WhActsContoller::class, 'import_plan'])->name('wh_act_import_plan');
Route::post('/wh_act/import_act', [WhActsContoller::class, 'import_act'])->name('wh_act_import_act');
Route::get('/wh_act/reportView', [WhActsContoller::class, 'reportView'])->name('wh_act_report');
Route::get('/wh_act/report_show', [WhActsContoller::class, 'report_export'])->name('wh_act_show');
Route::get('/wh_act/mailView', [WhActsContoller::class, 'mailView'])->name('wh_act_mail');
Route::get('/wh_act/sent_mail', [WhActsContoller::class, 'sent_mail'])->name('wh_act_sent_mail');
Route::get('/wh_act/del/{date}', [WhActsContoller::class, 'destroy_date'])->name('wh_act_del');    //delete file ลบทั้ง plan และ act จากปุ่มถังขยะหน้า index
Route::post('/wh_act/del_import', [WhActsContoller::class, 'destroy_import'])->name('wh_act_del_import');    //ลบโดยเลือกว่าลบจากอะไร plan/act ที่หน้า import
// Route::get('/wh_act/note/{st}/{pd}/{note}', [WhActsContoller::class, 'save_note']);
Route::get('/wh_act/note', [WhActsContoller::class, 'save_note']);
Route::resource('wh_act', WhActsContoller::class);

Route::get('/wh_rm/importExportView', [RmManualController::class, 'importExportView'])->name('wh_rm_import_export');
Route::post('/wh_rm/import_sap', [RmManualController::class, 'import_sap'])->name('wh_rm_import_sap');
Route::get('/wh_rm/export_manual', [RmManualController::class, 'export_manual'])->name('wh_rm_export_manual');
Route::post('/wh_rm/import_manual', [RmManualController::class, 'import_manual'])->name('wh_rm_import_manual');
Route::post('/wh_rm/export_sap', [RmManualController::class, 'import_comment'])->name('wh_rm_import_comment');
Route::get('/wh_rm/reportView', [RmManualController::class, 'reportView'])->name('wh_rm_report');
Route::get('/wh_rm/report_show', [RmManualController::class, 'report_export'])->name('wh_rm_show');
Route::get('/wh_rm/mailView', [RmManualController::class, 'mailView'])->name('wh_rm_mail');
Route::get('/wh_rm/sent_mail/{date}', [RmManualController::class, 'to_manual_mail'])->name('wh_rm_sent_mail');
Route::get('/wh_rm/del/{date}', [RmManualController::class, 'destroy_date'])->name('wh_rm_del');
Route::get('/wh_rm/del_img/{id}', [RmManualController::class, 'destroy_img'])->name('wh_rm_img');
Route::resource('wh_rm', RmManualController::class);

Route::get('/wh_rm/show_img/{path_1}/{path_2}', [RmManualController::class, 'show_img'])->name('show_img');

Route::resource('/st_wh/st_product', StProductController::class);
Route::resource('/st_wh/st_stock', StWhBeginController::class);
Route::resource('/st_wh/st_broker', StBrokerController::class);

Route::get('/st_wh/st_receive/stock', [StReceiveController::class, 'stock'])->name('st_pd_stock');
Route::get('/st_wh/st_receive/index/{type}/{id}', [StReceiveController::class, 'index'])->name('st_receive_index');
Route::get('/st_wh/st_receive/create/{type}/{id}', [StReceiveController::class, 'create'])->name('st_receive_create');
Route::post('/st_wh/st_receive/store/{type}', [StReceiveController::class, 'store'])->name('st_receive_store');
Route::get('/st_wh/st_receive/edit/{type}/{id}', [StReceiveController::class, 'edit'])->name('st_receive_edit');
Route::post('/st_wh/st_receive/update/{type}/{id}', [StReceiveController::class, 'update'])->name('st_receive_update');
Route::get('/st_wh/st_receive/del/{type}/{id}', [StReceiveController::class, 'destroy'])->name('st_receive_del');
Route::get('/st_wh/st_receive/show/{id}', [StReceiveController::class, 'show'])->name('st_receive_show');
// Route::get('/st_wh/st_receive/export/{id}', [StReceiveController::class, 'export'])->name('st_receive_export');
Route::resource('/st_wh/st_receive', StReceiveController::class);

Route::get('/st_wh/st_withdraw/stock', [StWhithdrawController::class, 'stock'])->name('st_withdraw_stock');
Route::get('/st_wh/st_withdraw/index/{type}/{id}', [StWhithdrawController::class, 'index'])->name('st_withdraw_index');
Route::get('/st_wh/st_withdraw/create/{type}/{id}', [StWhithdrawController::class, 'create'])->name('st_withdraw_create');
Route::post('/st_wh/st_withdraw/store/{type}/{id}', [StWhithdrawController::class, 'store'])->name('st_withdraw_store');
Route::get('/st_wh/st_withdraw/edit/{type}/{id}', [StWhithdrawController::class, 'edit'])->name('st_withdraw_edit');
Route::post('/st_wh/st_withdraw/update/{type}/{id}', [StWhithdrawController::class, 'update'])->name('st_withdraw_update');
Route::get('/st_wh/st_withdraw/del/{type}/{id}', [StWhithdrawController::class, 'destroy'])->name('st_withdraw_del');
Route::get('/st_wh/st_withdraw/show/{id}', [StWhithdrawController::class, 'show'])->name('st_withdraw_show');
Route::get('/st_wh/st_withdraw/detail/{bk}/{pd}', [StWhithdrawController::class, 'detail_no_set'])->name('st_withdraw_det');

Route::get('/st_wh/st_report/reportView', [StWhithdrawController::class, 'reportView'])->name('st_report_view');
Route::get('/st_wh/st_report/report_show', [StWhithdrawController::class, 'report_export'])->name('st_report_show');

Route::resource('/st_wh/st_withdraw', StWhithdrawController::class);

//FS
// Route::get('/user/delete/{id}', [RegisterController::class, 'destroy'])->name('user_delete');
// Route::resource('base_data/user', UserController::class);

Route::resource('base_data/shift', ShiftController::class);

Route::get('/base_data/product/set_status', [ProductController::class, 'set_status'])->name('product.set_status');
Route::resource('base_data/product', ProductController::class);

Route::resource('base_data/store', StoreController::class);
Route::resource('base_data/step', StepController::class);
Route::resource('base_data/for_tg', StepTgController::class);   //tg_step
Route::resource('base_data/lose_type', LoseTypeController::class);

// Route::get('/to_reuse/set_toend/{chk}/{date}/{pd_id}/{use_id}', [RegisterController::class, 'set_toend'])->name('set_toend');
// Route::get('/to_reuse/to_create/{page}', [ToReuseController::class, 'create'])->name('to_reuse.to_create');
Route::get('/to_reuse/show_det/{date}/{pd_id}', [ToReuseController::class, 'show'])->name('to_reuse.show_det');
Route::get('/to_reuse/set_toend', [ToReuseController::class, 'set_toend'])->name('set_toend');
Route::get('/to_reuse/detail/{id}', [ToReuseController::class, 'detail'])->name('to_reuse.detail');
// Route::get('/to_reuse/stock', [ToReuseController::class, 'stock'])->name('to_reuse.stock');
Route::resource('to_reuse', ToReuseController::class);

Route::get('/wash_edit/{date}/{shift}/{pd}', [WashController::class, 'edit'])->name('wash.wash_edit');
Route::PUT('/wash_update', [WashController::class, 'update'])->name('wash.wash_update');
Route::get('/wash_del', [WashController::class, 'destroy'])->name('wash.wash_del');
Route::get('/wash_detail', [WashController::class, 'show'])->name('wash.detail');
// Route::get('/wash_new', [WashController::class, 'create'])->name('wash.new');
Route::resource('wash', WashController::class);

Route::get('/dry_edit', [DryController::class, 'edit'])->name('dry.dry_edit');
Route::PUT('/dry_update', [DryController::class, 'update'])->name('dry.dry_update');
Route::get('/dry_del', [DryController::class, 'destroy'])->name('dry.dry_del');
Route::get('/dry_detail', [DryController::class, 'show'])->name('dry.detail');
Route::resource('dry', DryController::class);


Route::get('/shine_edit', [ShineController::class, 'edit'])->name('shine.shine_edit');
Route::PUT('/shine_update', [ShineController::class, 'update'])->name('shine.shine_update');
Route::get('/shine_del', [ShineController::class, 'destroy'])->name('shine.shine_del');
Route::get('/shine_detail', [ShineController::class, 'show'])->name('shine.detail');
Route::resource('shine', ShineController::class);

Route::get('/choose_box_edit', [ChooseBoxController::class, 'edit'])->name('choose_box.choose_box_edit');
Route::PUT('/choose_box_update', [ChooseBoxController::class, 'update'])->name('choose_box.choose_box_update');
Route::get('/choose_box_del', [ChooseBoxController::class, 'destroy'])->name('choose_box.choose_box_del');
Route::get('/choose_box_detail', [ChooseBoxController::class, 'show'])->name('choose_box.detail');
Route::resource('choose_box', ChooseBoxController::class);

Route::get('/stock_edit', [StockController::class, 'edit'])->name('stock.stock_edit');
Route::PUT('/stock_update', [StockController::class, 'update'])->name('stock.stock_update');
Route::get('/stock_del', [StockController::class, 'destroy'])->name('stock.stock_del');
Route::get('/to_process', [StockController::class, 'edit_wait'])->name('stock.to_process');
Route::PUT('/process', [StockController::class, 'update_wait'])->name('stock.process');
Route::get('/process_del', [StockController::class, 'del_wait'])->name('stock.process_del');
Route::get('/stock_detail', [StockController::class, 'show'])->name('stock.detail');
Route::get('/stock_return', [StockController::class, 'return'])->name('stock.stock_return');
Route::resource('stock', StockController::class);

Route::get('/fs_to_report', [ReportController::class, 'to_report'])->name('fs_report.fs_to_report');
Route::resource('fs_report', ReportController::class);

Route::get('/lose_auto/{lt}/{id}', [LoseController::class, 'lose_auto'])->name('lose.lose_auto');
Route::resource('lose', LoseController::class);

Route::get('/target/save_det/{id}', [TargetController::class, 'save_det'])->name('target.save_det');
Route::post('/target/clone_data', [TargetController::class, 'clone_data'])->name('target.clone_data');
Route::get('/target/report/{id}', [TargetController::class, 'report'])->name('target.report');
Route::resource('target', TargetController::class);

//Stock FS WH part
Route::get('/setting/FsWhs/autoGenLoc/{id}', [FsWhsController::class, 'autoGenLoc'])->name('FsWhs.autoGenLoc');
Route::get('/setting/FsWhs/detailList/{fs_wh_id}', [FsWhsController::class, 'detailList'])->name('FsWhs.detailList');
Route::get('/setting/FsWhs/editWhLoc/{fs_wh_loc_id}', [FsWhsController::class, 'editWhLoc'])->name('FsWhs.editWhLoc');
Route::post('/setting/FsWhs/editWhLocAction/{fs_wh_loc_id}', [FsWhsController::class, 'editWhLocAction'])->name('FsWhs.editWhLocAction');


Route::resource('/setting/FsWhs', FsWhsController::class);




Route::get('/FsStocks/detailList/{id}', [FsStocksController::class, 'detailList'])->name('FsStocks.detailList');
Route::post('/FsStocks/addDetailAction/{id}', [FsStocksController::class, 'addDetailAction'])->name('FsStocks.addDetailAction');
Route::post('/FsStocks/searchLoc/{id}', [FsStocksController::class, 'searchLoc'])->name('FsStocks.searchLoc');
Route::get('/FsStocks/deleteDetailAction/{id}', [FsStocksController::class, 'deleteDetailAction'])->name('FsStocks.deleteDetailAction');
Route::get('/FsStocks/mapfswh/{id}', [FsStocksController::class, 'mapfswh'])->name('FsStocks.mapfswh');
Route::get('/FsStocks/viewstock/{id}', [FsStocksController::class, 'viewstock'])->name('FsStocks.viewstock');
Route::get('/FsStocks/addProdToStock/{id}', [FsStocksController::class, 'addProdToStock'])->name('FsStocks.addProdToStock');
Route::post('/FsStocks/addProdToStockAction/{id}', [FsStocksController::class, 'addProdToStockAction'])->name('FsStocks.addProdToStockAction');
Route::get('/FsStocks/exportFromStock/{id}', [FsStocksController::class, 'exportFromStock'])->name('FsStocks.exportFromStock');
Route::post('/FsStocks/exportFromStockAction/{id}', [FsStocksController::class, 'exportFromStockAction'])->name('FsStocks.exportFromStockAction');
Route::get('/FsStocks/recvToStock/{id}', [FsStocksController::class, 'recvToStock'])->name('FsStocks.recvToStock');
Route::post('/FsStocks/recvToStockAction/{id}', [FsStocksController::class, 'recvToStockAction'])->name('FsStocks.recvToStockAction');
Route::get('/FsStocks/deliverFromStock/{id}', [FsStocksController::class, 'deliverFromStock'])->name('FsStocks.deliverFromStock');
Route::post('/FsStocks/deliverFromStockAction/{id}', [FsStocksController::class, 'deliverFromStockAction'])->name('FsStocks.deliverFromStockAction');
Route::get('/FsStocks/changelocation/{id}/{fs_wh_id}', [FsStocksController::class, 'changelocation'])->name('FsStocks.changelocation');
Route::get('/FsStocks/changelocationconfirm/{id}/{fs_wh_id}/{new_loc}', [FsStocksController::class, 'changelocationconfirm'])->name('FsStocks.changelocationconfirm');

Route::resource('/FsStocks', FsStocksController::class);

//Report FS Stock

Route::get('/FsReports/fsstockuseRpt', [FsReportsController::class, 'fsstockuseRpt'])->name('FsReports.fsstockuseRpt');
Route::get('/FsReports/fsstockuseemail', [FsReportsController::class, 'fsstockuseRptEmail'])->name('FsReports.fsstockuseemail');
Route::post('/FsReports/fsstockuseemailaction', [FsReportsController::class, 'fsstockuseRptEmailAction'])->name('FsReports.fsstockuseemailaction');



Route::get('/handhelds/importData', [HandheldImportsController::class, 'importData'])->name('handhelds.importData');
Route::post('/handhelds/importDataAction', [HandheldImportsController::class, 'importDataAction'])->name('handhelds.importDataAction');

Route::get('/handhelds/list', [HandheldImportsController::class, 'index'])->name('handhelds.index');

Route::prefix('TR')->group(function () {
    Route::resource('trucks', TrucksController::class);
    Route::resource('tails', TailsController::class);
    Route::resource('vessel_lines', VesselLinesController::class);

    Route::get('holidays/{vessel_line_id}/', [HolidaysController::class, 'index']);
    Route::get('holidays/{vessel_line_id}/create', [HolidaysController::class, 'create']);
    Route::post('holidays/{vessel_line_id}/', [HolidaysController::class, 'store']);
    Route::get('holidays/{vessel_line_id}/{id}/edit', [HolidaysController::class, 'edit']);
    Route::put('holidays/{vessel_line_id}/{id}/', [HolidaysController::class, 'update']);
    Route::patch('holidays/{vessel_line_id}/{id}/', [HolidaysController::class, 'update']);
    Route::delete('holidays/{vessel_line_id}/{id}/', [HolidaysController::class, 'destroy']);

    Route::get('weekends/{vessel_line_id}/', [WeekendsController::class, 'index']);
    Route::get('weekends/{vessel_line_id}/create', [WeekendsController::class, 'create']);
    Route::post('weekends/{vessel_line_id}/', [WeekendsController::class, 'store']);
    Route::get('weekends/{vessel_line_id}/{id}/edit', [WeekendsController::class, 'edit']);
    Route::put('weekends/{vessel_line_id}/{id}/', [WeekendsController::class, 'update']);
    Route::patch('weekends/{vessel_line_id}/{id}/', [WeekendsController::class, 'update']);
    Route::delete('weekends/{vessel_line_id}/{id}/', [WeekendsController::class, 'destroy']);

    Route::get('importdlps/uploadXls', [ImpDeliveryPlansController::class, 'uploadXls'])->name('importdlps.uploadXls');
    Route::post('importdlps/uploadXlsAction', [ImpDeliveryPlansController::class, 'uploadXlsAction'])->name('importdlps.uploadXlsAction');

    Route::get('importdlps/process/{filename}', [ImpDeliveryPlansController::class, 'process'])->name('importdlps.process');
    Route::get('importdlps/planview/{filename}', [ImpDeliveryPlansController::class, 'planview'])->name('importdlps.planview');

    Route::get('importdlps/caldate/{filename}', [ImpDeliveryPlansController::class, 'caldate'])->name('importdlps.caldate');
    Route::get('importdlps/exportplan/{filename}', [ImpDeliveryPlansController::class, 'exportplan'])->name('importdlps.exportplan');

    Route::resource('importdlps', ImpDeliveryPlansController::class);

    Route::get('scheduletrucks/{filename}/', [ScheduleTrucksController::class, 'index']);
    Route::get('scheduletrucks/{filename}/create', [ScheduleTrucksController::class, 'create']);
    Route::post('scheduletrucks/{filename}/', [ScheduleTrucksController::class, 'store']);
    Route::get('scheduletrucks/{filename}/{id}/edit', [ScheduleTrucksController::class, 'edit']);
    Route::put('scheduletrucks/{filename}/{id}/', [ScheduleTrucksController::class, 'update']);
    Route::patch('scheduletrucks/{filename}/{id}/', [ScheduleTrucksController::class, 'update']);
    Route::delete('scheduletrucks/{filename}/{id}/', [ScheduleTrucksController::class, 'destroy']);

    // Samary
    Route::get('scheduletrucksSumary', [SumaryController::class, 'index'])->name('scheduletrucksSumary');
    Route::get('scheduletrucksSumary/{month}', [SumaryController::class, 'scheduletrucksSumaryMonth'])->name('scheduletrucksSumaryMonth');


    Route::resource('calendar', CalendarController::class);
    Route::get('manual_plan', [CalendarController::class, 'manual_plan'])->name('manual_plan');
    Route::post('manual_plan_store', [CalendarController::class, 'manual_plan_store'])->name('manual_plan_store');

    Route::get('DeliveryPlan/export', [CalendarController::class, 'export'])->name('DeliveryPlan');
});



// ================================================================ WHRM ================================================================
// ================================================================ TEST ================================================================
// ======================================================================================================================================


Route::get('test', [WhRmController::class, 'index'])->name('test');

// ================================================================ WHRM Setting ================================================================
Route::resource('whrmSetting', WhRmBrokerAreaController::class);
Route::post('whrmSettingAddBroker', [WhRmBrokerAreaController::class, 'whrmSettingAddBroker'])->name('whrmSettingAddBroker');
Route::post('whrmSettingAddArea', [WhRmBrokerAreaController::class, 'whrmSettingAddArea'])->name('whrmSettingAddArea');



Route::get('show_issue_img/{fileName}', [WhRmController::class, 'show_issue_img'])->where('fileName', '.*');
Route::delete('delete_issue_img/{id}', [WhRmController::class, 'delete_issue_img'])->name('delete_issue_img');

// Route สำหรับตรวจสอบสถานะ issue_daily
Route::get('check_issue_daily/{issueDate}/{raw_material_id}', [WhRmController::class, 'checkIssueDaily'])->name('check_issue_daily');
Route::post('create_issue_daily/{issueDate}/{raw_material_id}', [WhRmController::class, 'createIssueDaily'])->name('create_issue_daily');
Route::post('upload_issue_daily_img/{issueDaily_id}', [WhRmController::class, 'upload_issue_daily_img'])->name('upload_issue_daily_img');
Route::delete('delete_issue_daily_img/{id}', [WhRmController::class, 'delete_issue_daily_img'])->name('delete_issue_daily_img');
Route::post('edit_issue_daily_detail/{issueDaily_id}', [WhRmController::class, 'edit_issue_daily_detail'])->name('edit_issue_daily_detail');

// ================================================================ WHRM Import File ================================================================
Route::post('test/upload_whrm_sap', [WhRmImportFileController::class, 'import_sap'])->name('upload_whrm_sap');
Route::post('test/upload_whrm_sap_banana', [WhRmImportFileController::class, 'import_sap_banana'])->name('upload_whrm_sap_banana');

Route::post('test/upload_whrm_manual', [WhRmImportFileController::class, 'import_manual'])->name('upload_whrm_manual');



// ================================================================ WHRM Banana TEST ================================================================
Route::get('test/export_manual_banana', [BananaController::class, 'export_manual_banana'])->name('test_export_manual_banana');
Route::post('test/banana_add_new_issue/{sap_no}', [BananaController::class, 'add_new_issue'])->name('banana_add_new_issue');
Route::delete('/banana_delete_issue/{id}', [BananaController::class, 'bananaDeleteIssue'])->name('banana_delete_issue');




Route::post('test/upload_banana_issue_img/{issue_id}', [BananaController::class, 'test_upload_banana_issue_img'])->name('test_upload_banana_issue_img');

// ================================================================ WHRM Bean TEST ================================================================
Route::post('test/upload_bean_sap', [BeanController::class, 'import_sap_bean'])->name('test_upload_bean_sap');
Route::get('test/export_manual_bean', [BeanController::class, 'export_manual_bean'])->name('test_export_manual_bean');
// ------------ step upload Manual ------------


Route::post('test/upload_bean_issue_img/{issue_id}', [BeanController::class, 'upload_issue_img'])->name('test_upload_bean_issue_img');
Route::post('test/add_new_issue/{sap_no}', [BeanController::class, 'add_new_issue'])->name('bean_add_new_issue');
Route::delete('/bean_delete_issue/{id}', [BeanController::class, 'bananaDeleteIssue'])->name('bean_delete_issue');


// ================================================================ WHRM Reports ================================================================
Route::get('test/sumary_report/{rmMat}', [WhRmReportController::class, 'sumaryReport'])->name('sumary_report_rmMat');

// ================================================================ WHRM Email ================================================================
Route::get('example_mail/{rmMat}', [WhRmEmailController::class, 'example_mail'])->name('whRmEmail_example_mail');
Route::get('send_mail/{rmMat}/{date}', [WhRmEmailController::class, 'send_mail'])->name('whRmEmail_send_mail');



// ================================================================ WHRM Delete ================================================================
Route::delete('delete_rm/{manual_date}', [WhRmDeleteController::class, 'delete_rm'])->name('delete_rm');
Route::delete('delete_issue/{manual_date}/{sap_no}', [WhRmDeleteController::class, 'delete_issue'])->name('delete_issue');
Route::delete('delete_daily_issue/{manual_date}/{rmMat}', [WhRmDeleteController::class, 'delete_daily_issue'])->name('delete_daily_issue');



// ================================================================= TR =================================================================
// ================================================================ TEST ================================================================
// ======================================================================================================================================

Route::get('TR/test_tr', [Test_TRController::class, 'index'])->name('test_tr');
Route::post('TR/tr_import_test', [Test_TRController::class, 'uploadXlsAction'])->name('tr_import_test');
