<?php

use App\Http\Controllers\TR\api\CalendarController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('calendar', CalendarController::class);

Route::post('edit', [CalendarController::class, 'edit'])->name('edit');
Route::post('delete', [CalendarController::class, 'delete'])->name('delete');

Route::get('trucks', [CalendarController::class, 'trucks'])->name('trucks');
