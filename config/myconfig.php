<?php

return [
    'directory' => [
        'whactivity' => [
            // 'queues' => 'c:/project/whforms/data/whforms/WhActivites/queues',
            'queues' => [
                'plan' => 'c:/project/whforms/data/whforms/WhActivites/queues/plan',
                'act' =>  'c:/project/whforms/data/whforms/WhActivites/queues/act',
            ],
            // 'completes' => 'c:/project/whforms/data/whforms/WhActivites/completes',
            'completes' => [
                'plan' => 'c:/project/whforms/data/whforms/WhActivites/completes/plan',
                'act' =>  'c:/project/whforms/data/whforms/WhActivites/completes/act',
            ],
            // 'queues' => 'C:/code_program/WH_form/public/storage/WhPlanAct/act/queues',
            // 'completes' => 'C:/code_program/WH_form/public/storage/WhPlanAct/act/completes',
        ],
        'DeliveryPlan' => [
            'TR' => [
                'completes' => [
                    'import' => 'c:/project/whforms/data/whforms/DeliveryPlan/TR/import',
                ],
            ],
        ],

        // 'whplanact' => [
        //     'queues' => 'c:/project/whforms/data/whforms/WhPlanActs/queues',
        //     'completes' => 'c:/project/whforms/data/whforms/WhPlanActs/completes',
        //     // 'queues' => 'C:/code_program/WH_form/public/storage/WhPlanAct/plan/queues',
        //     // 'completes' => 'C:/code_program/WH_form/public/storage/WhPlanAct/plan/completes',
        // ],
        'productinput' => [
            // 'queues' => 'c:/project/whforms/data/whforms/productinput/queues',
            // 'completes' => 'c:/project/whforms/data/whforms/productinput/completes',
            // 'queues' => 'C:/code_program/WH_form/public/storage/queues',
            // 'completes' => 'C:/code_program/WH_form/public/storage/completes',
            'laco' => [
                'queues' => 'c:/project/whforms/data/whforms/productinput/laco/queues',
                'completes' => 'c:/project/whforms/data/whforms/productinput/laco/completes',
            ],
            'en' => [
                'queues' => 'c:/project/whforms/data/whforms/productinput/en/queues',
                'completes' => 'c:/project/whforms/data/whforms/productinput/en/completes',
            ],
            'dc' => [
                'queues' => 'c:/project/whforms/data/whforms/productinput/dc/queues',
                'completes' => 'c:/project/whforms/data/whforms/productinput/dc/completes',
            ],
        ],
        'fl' => [
            'queues' => 'c:/project/whforms/data/whforms/FL/queues',
            'completes' => 'c:/project/whforms/data/whforms/FL/completes',
            'errors' => 'c:/project/whforms/data/whforms/FL/errors',
            'pic' => 'c:/project/whforms/data/whforms/FL/pic/',
            // 'queues' => 'C:/code_program/WH_form/public/storage/FL/queues',
            // 'completes' => 'C:/code_program/WH_form/public/storage/FL/completes',
            // 'errors' => 'C:/code_program/WH_form/public/storage/FL/errors',
        ],
        'whrmrecv' => [
            'queues' => 'c:/project/whforms/data/whforms/whrmrecv/queues',
            'completes' => 'c:/project/whforms/data/whforms/whrmrecv/completes',
        ],
        'whrm' => [
            'queues' => 'c:/project/whforms/data/whforms/WhRm/queues',
            'completes' => [
                'sap' => 'c:/project/whforms/data/whforms/WhRm/completes/sap',
                'manual' =>  'c:/project/whforms/data/whforms/WhRm/completes/manual',
                'img' => 'c:/project/whforms/data/whforms/WhRm/completes/img',
                'test' => 'c:/project/whforms/data/whforms/WhRm/completes/test',

            ],
        ],
        'handheld' => [
            'queues' => 'c:/project/whforms/data/handheld/queues',
            'completes' => 'c:/project/whforms/data/handheld/completes',
        ],
    ],
    'pd_sent_mail' => env('PRODUCT_SENT_MAIL', 'test'),
    'mail' => [
        'test' => [
            // 'to' => 'Parinya.K@lannaagro.com',
            'to' => 'yupa@lannaagro.com',
            'cc' => 'yupa@lannaagro.com',
        ],
        'laco' => [
            'to' => [
                'JJT' => 'Jeerawat@Lannaagro.com',
                'JJY' => 'Janjira@Lannaagro.com',
                'JPW' => 'Jirawan@Lannaagro.com',
                'KU' => 'Kanjana@Lannaagro.com',
                'NN' => 'Narongsak@lannaagro.com',
                'PCH' => 'Pikul@Lannaagro.com',
                'AI' => 'Arkkarapong@Lannaagro.com',
                'PIT' => 'Paison@Lannaagro.com',
                'WH_Staff' => 'WH_Staff@Lannaagro.com',
                'WHDC_Wangnoi_Staff' => 'WHDC_Wangnoi_Staff@Lannaagro.com',
                'WHRM_Staff' => 'WHRM_Staff@Lannaagro.com',
                'WHST_Staff' => 'WHST_Staff@Lannaagro.com',
                'WHTR_Staff' => 'WHTR_Staff@Lannaagro.com',
                // 'WSC' => 'Warunyu@Lannaagro.com',
                'WU' => 'Wanatsanun@Lannaagro.com',
                'WY' => 'Wimonjai@Lannaagro.com',
                'YP' => 'Yaowaluck@Lannaagro.com',
                'JB' => 'chenwit@Lannaagro.com',

                'AH' => 'Anucha@Lannaagro.com',

                'NS' => 'Nichada@Lannaagro.com',
                'NA' => 'nitchanan@lannaagro.com',
                'NT' => 'Nutcha@lannaagro.com',
                'NW' => 'Nantawan@Lannaagro.com',
                'PPN' => 'Piyawan.p@Lannaagro.com',
            ],
            'cc' => [
                'PKP' => 'Parinya.K@lannaagro.com',
                'YS' => 'yupa@lannaagro.com',
            ],
        ],
        'en-dc' => [
            'to' => [
                // 'APT' => 'Aumporn@Lannaagro.com',
                'JB' => 'chenwit@Lannaagro.com',
                'JJT' => 'Jeerawat@Lannaagro.com',
                'JJY' => 'Janjira@Lannaagro.com',
                'JPW' => 'Jirawan@Lannaagro.com',
                'KU' => 'Kanjana@Lannaagro.com',
                'NN' => 'Narongsak@lannaagro.com',
                'PCH' => 'Pikul@Lannaagro.com',
                'AI' => 'Arkkarapong@Lannaagro.com',
                'PIT' => 'Paison@Lannaagro.com',

                'WH_Staff' => 'WH_Staff@Lannaagro.com',
                'WHDC_Wangnoi_Staff' => 'WHDC_Wangnoi_Staff@Lannaagro.com',
                'WHRM_Staff' => 'WHRM_Staff@Lannaagro.com',
                'WHST_Staff' => 'WHST_Staff@Lannaagro.com',
                'WHTR_Staff' => 'WHTR_Staff@Lannaagro.com',
                'WU' => 'Wanatsanun@Lannaagro.com',
                'WY' => 'Wimonjai@Lannaagro.com',
                'YP' => 'Yaowaluck@Lannaagro.com',
                'BN' => 'Benya@Lannaagro.com',
                'JPT' => 'Jittranuch@Lannaagro.com',
                'PC' => 'Pimchanok@lannaagro.com',
                'VW' => 'Varut@lannaagro.com',
                'WT' => 'Wichchan@Lannaagro.com',
                'WW' => 'Wanwalee@Lannaagro.com',
                'NA' => 'nitchanan@lannaagro.com',
                'NS' => 'Nichada@Lannaagro.com',
                'NT' => 'Nutcha@lannaagro.com',
                'NW' => 'Nantawan@Lannaagro.com',
                'PPN' => 'Piyawan.p@Lannaagro.com',
            ],
            'cc' => [
                'PKP' => 'Parinya.K@lannaagro.com',
                'YS' => 'yupa@lannaagro.com',
            ],
        ],
        'sum' => [
            'to' => [
                'JJT' => 'Jeerawat@Lannaagro.com',
                'JJY' => 'Janjira@Lannaagro.com',
                'JPW' => 'Jirawan@Lannaagro.com',
                'KU' => 'Kanjana@Lannaagro.com',
                'NN' => 'Narongsak@lannaagro.com',
                'PCH' => 'Pikul@Lannaagro.com',
                'AI' => 'Arkkarapong@Lannaagro.com',
                'PIT' => 'Paison@Lannaagro.com',
                'WH_Staff' => 'WH_Staff@Lannaagro.com',
                'WHDC_Wangnoi_Staff' => 'WHDC_Wangnoi_Staff@Lannaagro.com',
                'WHRM_Staff' => 'WHRM_Staff@Lannaagro.com',
                'WHST_Staff' => 'WHST_Staff@Lannaagro.com',
                'WHTR_Staff' => 'WHTR_Staff@Lannaagro.com',
                'WU' => 'Wanatsanun@Lannaagro.com',
                'WY' => 'Wimonjai@Lannaagro.com',
                'YP' => 'Yaowaluck@Lannaagro.com',
                'JB' => 'chenwit@Lannaagro.com',

                'AH' => 'Anucha@Lannaagro.com',

                'NS' => 'Nichada@Lannaagro.com',
                'NA' => 'nitchanan@lannaagro.com',
                'NT' => 'Nutcha@lannaagro.com',
                'NW' => 'Nantawan@Lannaagro.com',
                'PPN' => 'Piyawan.p@Lannaagro.com',

                'BN' => 'Benya@Lannaagro.com',
                'JPT' => 'Jittranuch@Lannaagro.com',
                'PC' => 'Pimchanok@lannaagro.com',
                'VW' => 'Varut@lannaagro.com',
                'WT' => 'Wichchan@Lannaagro.com',
                'WW' => 'Wanwalee@Lannaagro.com',
            ],
            'cc' => [
                'PKP' => 'Parinya.K@lannaagro.com',
                'YS' => 'yupa@lannaagro.com',
            ],
        ],
    ],
    'mailFL' => [
        'real' => [
            'to' => [
                'JJT' => 'Jeerawat@Lannaagro.com',
                'JJY' => 'Janjira@Lannaagro.com',
                'JPW' => 'Jirawan@Lannaagro.com',
                'KU' => 'Kanjana@Lannaagro.com',
                'PIT' => 'Paison@Lannaagro.com',
                'WH_Staff' => 'WH_Staff@Lannaagro.com',
                'WHDC_Wangnoi_Staff' => 'WHDC_Wangnoi_Staff@Lannaagro.com',
                'WHRM_Staff' => 'WHRM_Staff@Lannaagro.com',
                'WHST_Staff' => 'WHST_Staff@Lannaagro.com',
                'WHTR_Staff' => 'WHTR_Staff@Lannaagro.com',
                'WSC' => 'Warunyu@Lannaagro.com',
                'WY' => 'Wimonjai@Lannaagro.com',
                'YP' => 'Yaowaluck@Lannaagro.com',
                'JB' => 'chenwit@Lannaagro.com',

                'FS_Staff' => 'FS@lannaagro.com',
                'SKY' => 'Sipawan@Lannaagro.com',
                'TM' => 'Thongpen@Lannaagro.com',
            ],
            'cc' => [
                'NN' => 'Narongsak@Lannaagro.com',
                'TM' => 'Thongpen@Lannaagro.com',
                'WU' => 'Wanatsanun@Lannaagro.com',
                'PCH' => 'Pikul@Lannaagro.com',
                'AI' => 'Arkkarapong@Lannaagro.com',
                'AH' => 'Anucha@Lannaagro.com',
                'SPW' => 'Supapong@Lannaagro.com',

                'PKP' => 'Parinya.K@lannaagro.com',
                'YS' => 'yupa@lannaagro.com',
            ],
        ],
        'test' => [
            'to' => [
                'YS' => 'yupa@lannaagro.com',
            ],
            'cc' => [
                'YS' => 'yupa@lannaagro.com',
            ],
        ],
    ],
    'mailWHRM' => [
        'test' => [
            // 'to' => 'Parinya.K@lannaagro.com',
            'to' => 'yupa@lannaagro.com',
            'cc' => 'yupa@lannaagro.com',
        ],
        'real' => [
            'to' => [
                'KMI' => 'kajohn@lannaagro.com',
                'KPP' => 'Kittisun@Lannaagro.com',
                //ER
                // 'ER' => 'ER@Lannaagro.com',
                'Anupon Aphimahachai ' => 'Anupon@Lannaagro.com',
                'Anuwat Losa ' => 'Anuwat@lannaagro.com',
                'Athiwat Winan ' => 'Athiwat@Lannaagro.com',
                'Banyawat Tiya ' => 'Banyawat@Lannaagro.com',
                'Boontarik Sankom ' => 'Boontarik@lannaagro.com',
                'Boonyatad Eabruethai ' => 'Boonyatad@Lannaagro.com',
                'Komsan Sanchang ' => 'komsan@lannaagro.com',
                'Narakorn Thephan ' => 'Narakorn@Lannaagro.com',
                'Nipon Wichaphaktayukon ' => 'Nipon@Lannaagro.com',
                'Nopparat Srichompu ' => 'Nopparat@Lannaagro.com',
                'Nuttapon Peerachan ' => 'Nuttapon@lannaagro.com',
                'Peeradon Puttharaksa ' => 'Peeradon@Lannaagro.com',
                'Sakon Kaeomala ' => 'Sakon@lannaagro.com',
                'Sakorn Yasamut ' => 'Sakorn@Lannaagro.com',
                'Sanan Phomthi ' => 'Sanan@Lannaagro.com',
                'Satawat Fongrat ' => 'Satawat@Lannaagro.com',
                'Thanachai Payplug ' => 'Thanachai@Lannaagro.com',
                'Vittawat Khaitong ' => 'Vittawat@Lannaagro.com',
                'Vorwoot Poogo ' => 'Vorwoot@Lannaagro.com',
                'Wanida Chaimongkon ' => 'Wanida@Lannaagro.com',
                'Wannachat Boonkhan ' => 'Wannachat@Lannaagro.com',
                'Weerachon Chantasirisit ' => 'Weerachon@Lannaagro.com',
                'AK ' => 'Aek@Lannaagro.com',
                'CPP ' => 'Chalermchai@Lannaagro.com',
                'KPP ' => 'Kittisun@Lannaagro.com',
                'NSK ' => 'Nattapong@Lannaagro.com',
                'PPV ' => 'Piyachai@Lannaagro.com',
                'RL ' => 'Ratchapak@Lannaagro.com',
                'RO ' => 'Romchat@Lannaagro.com',
                'STK ' => 'saithan@lannaagro.com',
                'WPR ' => 'Wuttinun@Lannaagro.com',
                'DR ' => 'David.Rousseau@Lannaagro.com',
                'KMI ' => 'kajohn@lannaagro.com',
                'AKN ' => 'Amnat@lannaagro.com',
                'ASK ' => 'Adipong@Lannaagro.com',
                'PR ' => 'pairat@lannaagro.com',
                'SJT ' => 'Songkran@lannaagro.com',
                'SKM ' => 'Sumitra@lannaagro.com',


                //FT
                // 'FT' => 'FT@Lannaagro.com',
                'FS_Staff ' => 'FS_Staff@Lannaagro.com',
                'SKY ' => 'Sipawan@Lannaagro.com',
                'TM ' => 'Thongpen@Lannaagro.com',
                'FT_Staff ' => 'FT_Staff@Lannaagro.com',
                'APY ' => 'Attawit@Lannaagro.com',
                'MTFT_Staff ' => 'MTFT_Staff@lannaagro.com',
                'MTMF_Staff ' => 'MTMF_Staff@Lannaagro.com',
                'MTPK_Staff ' => 'MTPK_Staff@Lannaagro.com',
                'PA ' => 'Pornpimon@lannaagro.com',
                'PD ' => 'Pariwat@Lannaagro.com',
                'SHK ' => 'Somporn@Lannaagro.com',
                'SKS ' => 'Somsak@lannaagro.com',
                'SU ' => 'suriyan@lannaagro.com',
                'TD ' => 'Tarathon@lannaagro.com',
                'VW ' => 'Varut@lannaagro.com',
                'WK ' => 'Weeranan@Lannaagro.com',
                'WKT ' => 'Woraphan@Lannaagro.com',
                'WLY ' => 'Walarin@Lannaagro.com',
                'AY ' => 'Akkarapon@lannaagro.com',
                'OC ' => 'Oraphan@Lannaagro.com',
                'PfIQF_Staff ' => 'PfIQF_Staff@Lannaagro.com',
                'PI ' => 'Piphop@Lannaagro.com',
                'PSR ' => 'piyawan@lannaagro.com',
                'SJU ' => 'Supattra2@Lannaagro.com',
                'SPT ' => 'Supakchaya@Lannaagro.com',
                'WIY ' => 'Wanlaya@Lannaagro.com',
                'WT ' => 'Wichchan@Lannaagro.com',
                'PK_Staff ' => 'PK_Staff@Lannaagro.com',
                'SAN ' => 'Supaporn.a@Lannaagro.com',
                'SJB ' => 'Sarawut@Lannaagro.com',
                'TWP ' => 'Thanawat@Lannaagro.com',
                'JPT ' => 'Jittranuch@Lannaagro.com',
                'PC ' => 'Pimchanok@lannaagro.com',
                'WW ' => 'Wanwalee@Lannaagro.com',
                'Mattanee Jinopeng ' => 'Mattanee@Lannaagro.com',
                'PST_Staff ' => 'pst_staff@lannaagro.com',
                'JJT ' => 'Jeerawat@Lannaagro.com',
                'JPW ' => 'Jirawan@Lannaagro.com',
                'KU ' => 'Kanjana@Lannaagro.com',
                'NN ' => 'Narongsak@lannaagro.com',
                'PIT ' => 'Paison@Lannaagro.com',
                'WH_Staff ' => 'WH_Staff@Lannaagro.com',
                'WHDC_Wangnoi_Staff ' => 'WHDC_Wangnoi_Staff@Lannaagro.com',
                'WHRM_Staff ' => 'WHRM_Staff@Lannaagro.com',
                'WHST_Staff ' => 'WHST_Staff@Lannaagro.com',
                'WHTR_Staff ' => 'WHTR_Staff@Lannaagro.com',
                'WSC ' => 'Warunyu@Lannaagro.com',
                'YP ' => 'Yaowaluck@Lannaagro.com',

            ],
            'cc' => [
                'CW' => 'chotiroj@Lannaagro.com',
                'GW' => 'gorragot@Lannaagro.com',
                'SPW' => 'Supapong@Lannaagro.com',
                'AH' => 'Anucha@Lannaagro.com',
                'BN' => 'Benya@Lannaagro.com',
                'PCH' => 'Pikul@Lannaagro.com',
                'AI' => 'Arkkarapong@Lannaagro.com',
                'WU' => 'Wanatsanun@lannaagro.com',
                'JB' => 'chenwit@lannaagro.com',
                'WY' => 'Wimonjai@Lannaagro.com',
                'JJY' => 'Janjira@lannaagro.com',

                'CH' => 'Chonlada@Lannaagro.com',

                'PKP' => 'Parinya.K@lannaagro.com',
                'YS' => 'yupa@lannaagro.com',
                'TR' => 'thapthai@lannaagro.com'
            ],
        ],
    ],
    'mailAct' => [
        'real' => [
            'to' => [
                'JJT' => 'Jeerawat@Lannaagro.com',
                'JJY' => 'Janjira@Lannaagro.com',
                'JPW' => 'Jirawan@Lannaagro.com',
                'KU' => 'Kanjana@Lannaagro.com',
                'NN' => 'Narongsak@lannaagro.com',

                'PIT' => 'Paison@Lannaagro.com',
                'WH_Staff' => 'WH_Staff@Lannaagro.com',
                'WHDC_Wangnoi_Staff' => 'WHDC_Wangnoi_Staff@Lannaagro.com',
                'WHRM_Staff' => 'WHRM_Staff@Lannaagro.com',
                'WHST_Staff' => 'WHST_Staff@Lannaagro.com',
                'WHTR_Staff' => 'WHTR_Staff@Lannaagro.com',

                'WY' => 'Wimonjai@Lannaagro.com',
                'YP' => 'Yaowaluck@Lannaagro.com',
                'AY' => 'Akkarapon@lannaagro.com',
                'OC' => 'Oraphan@Lannaagro.com',
                'PfIQF_Staff' => 'PfIQF_Staff@Lannaagro.com',
                'PI' => 'Piphop@Lannaagro.com',
                'PSR' => 'piyawan@lannaagro.com',
                'SJB' => 'Sarawut@Lannaagro.com',
                'SJU' => 'Supattra2@Lannaagro.com',
                'SPT' => 'Supakchaya@Lannaagro.com',
                'WIY' => 'Wanlaya@Lannaagro.com',
                'WT' => 'Wichchan@Lannaagro.com',
                'VW' => 'Varut@lannaagro.com',
                'PK_Staff' => 'PK_Staff@Lannaagro.com',
                'SAN' => 'Supaporn.a@Lannaagro.com',
                'TWP' => 'Thanawat@Lannaagro.com',
                'Mattanee' => 'Mattanee@Lannaagro.com',
                'PST_Staff' => 'pst_staff@lannaagro.com',
                'BN' => 'Benya@Lannaagro.com',
                'JPT' => 'Jittranuch@Lannaagro.com',
                'PC' => 'Pimchanok@lannaagro.com',
                'WW' => 'Wanwalee@Lannaagro.com',
                'AS' => 'Apapisoot@Lannaagro.com',
                'Kantamanee' => 'Kantamanee@Lannaagro.com',
                'OP' => 'Onnicha@Lannaagro.com',
                'Pirunlak' => 'Pirunlak@Lannaagro.com',
                'QcPF_Staff' => 'QcPF_Staff@Lannaagro.com',
                'HS' => 'Hutsarit@lannaagro.com',
                'LU' => 'Lapassaporn@Lannaagro.com',
                'RPT' => 'Rotratree@Lannaagro.com',
                'WC' => 'Wasan@Lannaagro.com',
                'QcPK_Staff' => 'QcPK_Staff@Lannaagro.com',
                'QcPP_Staff' => 'QcPP_Staff@Lannaagro.com',
                'QcPP_Sup' => 'QcPP_Sup@Lannaagro.com',
                'QcRM_Staff' => 'Qcrm_Staff@Lannaagro.com',
                'TWD' => 'Tipsuda@Lannaagro.com',

            ],
            'cc' => [
                'CW' => 'chotiroj@lannaagro.com',
                'GW' => 'gorragot@lannaagro.com',
                'AH' => 'Anucha@Lannaagro.com',
                'AI' => 'Arkkarapong@Lannaagro.com',
                'PCH' => 'Pikul@Lannaagro.com',
                'WU' => 'Wanatsanun@Lannaagro.com',
                'JB' => 'chenwit@Lannaagro.com',

                'PKP' => 'Parinya.K@lannaagro.com',
                'YS' => 'yupa@lannaagro.com',
            ],
        ],
        'mailfs' => [
            'to' => [
                'AI' => 'Arkkarapong@Lannaagro.com',
                'WU' => 'Wanatsanun@Lannaagro.com',
                'WT' => 'Wichchan@Lannaagro.com',
                'WY' => 'Wimonjai@Lannaagro.com',
                'WW' => 'Wanwalee@Lannaagro.com',
                'SJU' => 'Supattra2@Lannaagro.com',
                'OC' => 'Oraphan@Lannaagro.com',
                'SAN' => 'Supaporn.a@Lannaagro.com',
                'TWP' => 'Thanawat@Lannaagro.com',
            ],
            'cc' => [
                'DN' => 'Daraporn@Lannaagro.com',
                'NSY' => 'Namthip@Lannaagro.com',
                'FS_Staff' => 'FS_Staff@Lannaagro.com',
                'PJ' => 'Peerawas@Lannaagro.com',
                'SKY'  => 'Sipawan@Lannaagro.com',
                'PK_Staff' => 'PK_Staff@Lannaagro.com',
                'JJT' => 'Jeerawat@Lannaagro.com',
                'PI' => 'Piphop@Lannaagro.com',
                'PS' => 'Piyaporn@Lannaagro.com',
                'PSR' => 'piyawan@lannaagro.com',
                'Mattanee Jinopeng' => ' Mattanee@Lannaagro.com',
                'PfIQF_Staff' => 'PfIQF_Staff@Lannaagro.com',
                'SPT' => 'Supakchaya@Lannaagro.com',
                'WIY' => 'Wanlaya@Lannaagro.com',
                'VW' => 'Varut@lannaagro.com'
            ]
        ],
        'test' => [
            'to' => [
                'YS' => 'yupa@lannaagro.com',
            ],
            'cc' => [
                'YS' => 'yupa@lannaagro.com',
            ],
        ],
    ],
];
