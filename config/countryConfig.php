<?php

return ['country' => [
    'USA' => [
        'city' => [
            'NEW YORK',
            'JACKSONVILLE',
            'BOSTON',
            'LOS ANGELES',
            'PHILADELPHIA',
            'HONOLULU',
            'HONOLULU,HAWAII',
            'OAKLAND',
            'MIAMI',
            'TACOMA',
            'DALLAS',
            'HONOLULU, HI, USA',
            'HONOLULU',
            'LONG BEACH',
        ]
    ],
    'U.A.E' => [
        'city' => [
            'JEBEL ALI'
        ]
    ],
    'JAPAN' => [
        'city' => [
            'YOKOHAMA',
            'OSAKA',
            'TOKYO',
            'HAKATA',
            'NAGOYA',
            'TOMAKOMAI',
            'HIROSHIMA',
        ]
    ],
    'VIETNAM' => [
        'city' => [
            'CAT LAI',
        ]
    ],
    'SOUTH KOREA' => [
        'city' => [
            'BUSAN'
        ]
    ],


]];
