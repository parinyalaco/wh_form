<?php

namespace App\Console\Commands;

use App\Models\RecvRmOther;
use App\Models\RecvRmOtherImport;
use App\Models\RecvRmOtherPlan;
use App\Models\RecvRmOtherPlanMain;
use SimpleXLSX;
use Illuminate\Console\Command;

class RecvRmOtherImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:recvrmothers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Recv RM Other by place file in folder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $directory = config('myconfig.directory.whrmrecv.queues');
        $completedirectory = config('myconfig.directory.whrmrecv.completes');
        $scanned_directory = array_diff(scandir($directory), array('..', '.'));

        foreach ($scanned_directory as $fileupload) {


            $this->_readdata($fileupload);
            echo "Import File: " . $fileupload;
             if (copy($directory . '/' . $fileupload, $completedirectory . '/' . $fileupload)) {
                 // unlink($directory . '/' . $fileupload);
            echo " Complete!;\n";
              }
        }
        return 0;
    }

    private function _readdata($filename)
    {
        $directory = config('myconfig.directory.whrmrecv.queues');
        $loadFile = $directory . "/" . $filename;
        $recvrmotherplanmain = RecvRmOtherPlanMain::where('status','Active')->first();
        //dd($recvrmotherplanmain);
        if ($xlsx = SimpleXLSX::parse($loadFile)) {
            $importedrow = 0;
            $tmpRecvRmOtherImport = [];
            $tmpRecvRmOtherImport['filename'] = $filename;
            $tmpRecvRmOtherImport['import_num'] = $importedrow;
            $tmpRecvRmOtherImport['status'] = 'Active';
            $tmpRecvRmOtherImport['recv_rm_other_plan_m_id'] = $recvrmotherplanmain->id;
            $recvRmOtherImport = RecvRmOtherImport::create($tmpRecvRmOtherImport);
            foreach ($xlsx->rows(0) as $r => $row) {
                if ($r > 0) {
                    // print_r($row);

                    $tmpRecvRmOther = [];
                    $tmpRecvRmOther['recv_rm_other_import_id'] = $recvRmOtherImport->id;
                    $tmpRecvRmOther['row'] = $r;
                    $tmpRecvRmOther['act_date'] = date('Y-m-d', strtotime($row[0])); 
                    $tmpRecvRmOther['seq'] = $row[1];
                    $tmpRecvRmOther['poduct_code'] = 'BANANA'; 
                    $tmpRecvRmOther['license_no'] = $row[2];
                    $tmpRecvRmOther['location'] = $row[3];
                    $tmpRecvRmOther['car_no'] = $row[4]; 
                    $tmpRecvRmOther['qty'] = $row[5];  
                    $tmpRecvRmOther['fail_qty'] = $row[6];   
                    $tmpRecvRmOther['weight'] = $row[7];
                    $tmpRecvRmOther['recv_rm_other_plan_m_id'] = $recvrmotherplanmain->id;
                    if(!empty($row[8])){
                        $tmpRecvRmOther['left_time'] = date('Y-m-d',strtotime($row[0])).' '. date('H:i', strtotime($row[8]));   
                    }
                    if (!empty($row[9])) {
                        $tmpRecvRmOther['arv_laco_time'] = date('Y-m-d', strtotime($row[0])) . ' ' . date('H:i', strtotime($row[9]));    
                    }
                    if (!empty($row[11])) {
                        $tmpRecvRmOther['start_move_time'] = date('Y-m-d', strtotime($row[0])) . ' ' . date('H:i', strtotime($row[11]));  
                    }
                    if (!empty($row[12])) {
                        $tmpRecvRmOther['end_move_time'] = date('Y-m-d', strtotime($row[0])) . ' ' . date('H:i', strtotime($row[12])); 
                    }
                    $tmpRecvRmOther['staff_no'] = $row[14];
                    $tmpRecvRmOther['car_type'] = $row[15]; 
                    $tmpRecvRmOther['move_type'] = $row[16];
                    $tmpRecvRmOther['note'] = $row[20];

                    //print_r($tmpRecvRmOther);

                    RecvRmOther::create($tmpRecvRmOther);

                    //print_r($tmpRecvRmOther);

                    $importedrow++;
                }
            }
            foreach ($xlsx->rows(1) as $r => $row) {
                if ($r > 1) {
                  //  dd($row);
                    $tmpRecvRmOtherPlan = [];
                    $tmpRecvRmOtherPlan['recv_rm_other_import_id'] = $recvRmOtherImport->id;
                    $tmpRecvRmOtherPlan['row'] = $r;
                    $tmpRecvRmOtherPlan['plan_date'] = date('Y-m-d', strtotime($row[0]));
                    $tmpRecvRmOtherPlan['weight'] = $row[1];
                    $tmpRecvRmOtherPlan['recv_rm_other_plan_m_id'] = $recvrmotherplanmain->id;
                    RecvRmOtherPlan::create($tmpRecvRmOtherPlan);
                }elseif($r == 0){
                    $recvrmotherplanmain->total_plan = $row[1];
                    $recvrmotherplanmain->update();
                }
            }
        }
        return $importedrow;
    }
}
