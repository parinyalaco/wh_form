<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use SimpleXLSX;
use App\Models\WhActivity;
use App\Models\WhActivityDetail;
use App\Models\WhActType;
use App\Models\WhActMaterial;
use App\Models\Unit;
use App\Models\WhMoveType;

class ImportWhActivity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:whactivity';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import File WH Activity ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tmpfolder = date('Ymd');
        $directory = config('myconfig.directory.whactivity.queues.act');
        $completedirectory = config('myconfig.directory.whactivity.completes.act');
        $scanned_directory = array_diff(scandir($directory), array('..', '.'));
        if(count($scanned_directory)>0){
            $files = \File::files($directory);   
            foreach ($files as $fileupload) {
                $chk = $this->_readdata($fileupload);             
                if($chk=="0"){
                    $uploadname = $fileupload->getFilename();
                    $str_pos = substr($uploadname,0,strrpos($uploadname,'.'));
                    // dd($str_pos);
                    if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                        mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
                    }                
                    $name = $str_pos.'-'.date('YmdHis') . '.' . $fileupload->getExtension();
                    if (copy($directory . '/' . $uploadname, $completedirectory . '/' . $tmpfolder . '/' . $name)) {                    
                        unlink($directory . '/' . $uploadname);
                    }
                    echo "Import File: " . $uploadname." Complete!;\n";
                }else{
                    echo $chk;
                }
            }
        }else{
            echo "No File;\n";
        }
        
        return 0;            
    }

    private function _readdata($filename){
        // $directory = config('myconfig.directory.whactivity.queues');
        // $loadFile = $directory . "/" . $filename;
        if ($xlsx = SimpleXLSX::parse($filename)) {            
            $query = WhActMaterial::join('units', 'wh_act_materials.unit_id', '=', 'units.id')
                ->select('wh_act_materials.id', 'wh_act_materials.name', 'wh_act_materials.mat_desc', 'wh_act_materials.unit_id', 'units.name AS unit_name')
                ->get();
            foreach ($query as $key) {
                $mat[$key->name][$key->mat_desc][$key->unit_name] = $key->id;
            }

            $query = Unit::all();
            foreach ($query as $key) {
                $unit[$key->name] = $key->id;
            }

            $query = WhActType::all();
            foreach ($query as $key) {
                $act_type[$key->code] = $key->id;
            }

            $query = WhMoveType::all();
            foreach ($query as $key) {
                $move_type[$key->name] = $key->id;
            }
            
            $data_d = array();
            foreach ($xlsx->rows(0) as $r => $row) {
                if($r>0){
                    if(!empty($row[0])){
                        if(!empty($mat[$row[1]][$row[0]][$row[12]])){
                            $data_d[$r]['wh_act_material_id'] = $mat[$row[1]][$row[0]][$row[12]];
                        }else{
                            if(empty($unit[$row[12]])){
                                $add_unit['name'] = $row[12];
                                $unit_id = Unit::create($add_unit)->id; 
                                $unit[$row[12]] = $unit_id;
                            }else{
                                $unit_id = $unit[$row[12]];
                            }
                            $add_mat['name'] = $row[1];
                            $add_mat['mat_desc'] = $row[0];
                            $add_mat['unit_id'] = $unit_id;
                            $mat_id = WhActMaterial::create($add_mat)->id; 
                            $mat[$row[1]][$row[0]][$row[12]] = $mat_id;

                            $data_d[$r]['wh_act_material_id'] = $mat_id;
                        }

                        if(empty($move_type[$row[2]])){
                            $add_move['name'] = $row[2];
                            $move_id = WhMoveType::create($add_move)->id; 
                            $move_type[$row[2]] = $move_id;
                        }else{
                            $move_id = $move_type[$row[2]];
                        }
                        $data_d[$r]['wh_move_type_id'] = $move_id;

                        $data_d[$r]['posting_date'] = date('Y-m-d',strtotime($row[3]));

                        $data_d[$r]['batch'] = $row[4];
                        $data_d[$r]['entry_date'] = date('Y-m-d H:i:s',strtotime($row[5])); 
                        $data_d[$r]['entry_time'] = date('H:i:s', strtotime($row[6])); 
                        $data_d[$r]['mat_doc'] = $row[7]; 

                        if(empty($act_type[$row[8]])){
                            return "unknow Movement Type on row number ".($r+1)."\n";
                        }else{
                            $data_d[$r]['wh_act_type_id'] = $act_type[$row[8]]; 
                        }

                        $data_d[$r]['store_loc'] = $row[9];
                        $data_d[$r]['mat_doc_item'] = $row[10];
                        $data_d[$r]['qty_entry'] = $row[11];
                        $data_d[$r]['amount_lc'] = $row[13];
                        $data_d[$r]['ref'] = $row[14];
                        $data_d[$r]['order'] = $row[15];
                        $data_d[$r]['username'] = $row[16];
                    }
                }
            }
            
            if(count($data_d)>0){
                $importedrow = 0;
                $uploadname = $filename->getFilename();
                $whActivitytmp = array();
                $whActivitytmp['filename'] = $uploadname;
                $whActivitytmp['import_num'] = $importedrow;
                $whActivitytmp['status'] = 'Active';
                $importMain = WhActivity::create($whActivitytmp);

                foreach ($data_d as $r => $row) {
                    $tmpdetail = array();
                    $tmpdetail['wh_activity_id']=$importMain->id;
                    $tmpdetail['row']= $r;
                    $tmpdetail['wh_act_material_id'] = $data_d[$r]['wh_act_material_id'];
                    $tmpdetail['wh_move_type_id'] = $data_d[$r]['wh_move_type_id'];
                    $tmpdetail['posting_date'] = $data_d[$r]['posting_date'];
                    $tmpdetail['batch'] = $data_d[$r]['batch'];
                    $tmpdetail['entry_date'] = $data_d[$r]['entry_date']; 
                    $tmpdetail['entry_time'] = $data_d[$r]['entry_time']; 
                    $tmpdetail['mat_doc'] = $data_d[$r]['mat_doc']; 
                    $tmpdetail['wh_act_type_id'] = $data_d[$r]['wh_act_type_id']; 
                    $tmpdetail['store_loc'] = $data_d[$r]['store_loc'];
                    $tmpdetail['mat_doc_item'] = $data_d[$r]['mat_doc_item'];
                    $tmpdetail['qty_entry'] = $data_d[$r]['qty_entry'];
                    $tmpdetail['amount_lc'] = $data_d[$r]['amount_lc'];
                    $tmpdetail['ref'] = $data_d[$r]['ref'];
                    $tmpdetail['order'] = $data_d[$r]['order'];
                    $tmpdetail['username'] = $data_d[$r]['username'];
                    WhActivityDetail::create($tmpdetail);
                }
                return "0";
            }else{
                return "No data!\n";
            }
        }else{
            return "Can't read file!\n";
        }
        
    }
}
