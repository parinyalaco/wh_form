<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\FLog;
use App\Models\Forklift;
use App\Models\Fperson;
use App\Models\Flocation;
use Carbon\Carbon;
use SimpleXLSX;

use App\Mail\ReportFL;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;
use DB;

class AutoReportFL extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'WHForm:AutoReportFL{range}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Use to auto send mail report FL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('memory_limit', '2000M');
		ini_set('post_max_size', '64M');
		ini_set('upload_max_filesize', '64M');
		ini_set('max_input_time', 300);
		ini_set('max_execution_time', 1900);

        $range_date = $this->argument('range');

        $ed_date = date ("Y-m-d", strtotime("-1 day"));
        $st_date = date ("Y-m-d", strtotime("-".$range_date." day", strtotime($ed_date)));

        $date_to = $ed_date.':'.$st_date;
        // echo $date_to;
        // return $this -> sentmail($date_to);
        // $controller = new FLogController();
        // $controller->sentmail($date_to);

        // dd($date_to);
        $explode_date = explode(':',$date_to);
        $ed_date = $explode_date[0];
        $st_date = $explode_date[1];

        // $ed_date = '2022-03-21';
        // $st_date = '2022-03-14';
        $start_date = date ("d/m/Y", strtotime($st_date));
        $end_date = date ("d/m/Y", strtotime($ed_date));
        $query = Forklift::orderByRaw('len(name)')->get();
        foreach($query as $key){
            $fl_data[$key->id] = $key->name;
            $status[$key->id] = $key->status;
            $fl = FLog::where('forklift_id',$key->id)->where('log_date','<', $st_date)->whereNotNull('time')->orderBy('log_date','desc')->orderBy('shift','desc')->limit(1)->get();
            if(count($fl)>0 && $fl[0]['updated_at'] > $key->updated_at){
                $fl_time[$key->id] = number_format($fl[0]['time'],1,'.','');
            }else{
                $fl_time[$key->id] = number_format($key->time,1,'.','');
            }
        }
        // dd($fl_data);
        $query = Flocation::orderByRaw('len(name)')->get();
        foreach($query as $key){
            $locate_data[$key->id] = $key->name;
        }

        $fl_log = array();
        $tbl_1 = array();
        $total_val = array();
        $query_1 = new FLog;
        $query_1 = $query_1->whereBetween('log_date', [$st_date, $ed_date]);
        $query_1 = $query_1->orderBy('forklift_id')->orderBy('log_date')->orderBy('shift')->get();

        $datetime1 = strtotime($st_date); // convert to timestamps
        $datetime2 = strtotime($ed_date); // convert to timestamps
        $days = (int)(($datetime2 - $datetime1)/86400);
        if(count($query_1)>0){
            foreach($query_1 as $key){
                if($key->type == 'R'){
                    if(empty($fl_id) || $fl_id <> $key->forklift_id){
                        $to_diff = $fl_time[$key->forklift_id];
                    }
                    $fl_id = $key->forklift_id;
                    $key_time = number_format($key->time,1,'.','');
                    $diff_time = $key_time - $to_diff;
                    if($fl_type[$key->forklift_id] == 'LACO'){
                        $diff_time = $key_time;
                    }
                    $fl_log_locate[$key->f_location_id][$key->forklift_id][$key->log_date][$key->shift][$key->type] = number_format($diff_time,1,'.','');
                    if(!empty($total_val['count'][$key->f_location_id][$key->forklift_id][$key->shift])){
                        $total_val['count'][$key->f_location_id][$key->forklift_id][$key->shift] += 1;
                        $total_val['sum'][$key->f_location_id][$key->forklift_id][$key->shift] += $diff_time;
                    }else{
                        $total_val['count'][$key->f_location_id][$key->forklift_id][$key->shift] = 1;
                        $total_val['sum'][$key->f_location_id][$key->forklift_id][$key->shift] = $diff_time;
                    }
                    $to_diff = number_format($key->time,1,'.','');
                    if($key->log_date == $ed_date){
                        $to_gp3[$key->shift][$key->forklift_id] = $diff_time;
                    }

                }elseif($key->type == 'S'){
                    $fl_log_locate[$key->f_location_id][$key->forklift_id][$key->log_date][$key->shift][$key->type] = 'S';
                }elseif($key->type == 'A'){
                    $fl_log_locate[$key->f_location_id][$key->forklift_id][$key->log_date][$key->shift][$key->type] = 'A';
                }

                if($key->log_date == $ed_date){
                    if(empty($gp_2[$key->type][$key->shift]))   $gp_2[$key->type][$key->shift] = 1;
                    else    $gp_2[$key->type][$key->shift] += 1;
                }
            }
            // dd($fl_log_locate);
            //sort ใหม่
            foreach ($fl_data as $kfl => $vfl) {
                foreach ($locate_data as $kloc => $vloc) {
                    if(!empty($fl_log_locate[$kloc][$kfl])) {
                        foreach($fl_log_locate[$kloc][$kfl] as $kdate => $vdate){
                            foreach($vdate as $kshift => $vshift) {
                                foreach ($vshift as $ktype => $vtype) {
                                    if(!empty($fl_log_locate[$kloc][$kfl][$kdate][$kshift][$ktype])){
                                        $tbl_1[$kloc][$kfl][$kdate][$kshift][$ktype] = $vtype;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // dd($tbl_1);
            $arr_type = array('R','S','A');
            $arr_shift = array('B','C');
            foreach ($arr_type as $ktype => $vtype) {
                foreach($arr_shift as $kshift => $vshift) {
                    if(empty($gp_2[$vtype][$vshift]))   $gp_2[$vtype][$vshift]=0;
                }
            }
        }
        // dd($total_val);

        //แสดง Chart ข้อที่ 2 -------------------
        require_once app_path() . '/jpgraph/jpgraph.php';
        require_once app_path() . '/jpgraph/jpgraph_bar.php';
        require_once app_path() . '/jpgraph/jpgraph_canvas.php';
        require_once app_path() . '/jpgraph/jpgraph_table.php';
        // Display the graph
        $path = public_path() . '/graph/'.date('Y'). "/" . date('m') . '/mail';
        // dd($path);
        if (!File::exists($path)) {
            File::makeDirectory($path,  0777, true, true);
        }
        $date = date('ymdHis');

        if(!empty($gp_2)){
            $datay = array(
                array('กะ B','กะ C'),
                array($gp_2['R']['B'],$gp_2['R']['C']),
                array($gp_2['S']['B'],$gp_2['S']['C']),
                array($gp_2['A']['B'],$gp_2['A']['C']));
            // dd($datay);
            $nbrbar = 2; // number of bars and hence number of columns in table
            $cellwidth = 100;  // width of each column in the table
            $tableypos = 200; // top left Y-coordinate for table
            $tablexpos = 130;  // top left X-coordinate for table
            $tablewidth = $nbrbar*$cellwidth; // overall table width
            $rightmargin = 30;  // right margin in the graph
            $topmargin = 60;  // top margin of graph
            $height = 320;  // a suitable height for the image
            $width = $tablexpos+$tablewidth+$rightmargin; // the width of the image
            $graph = new \Graph($width,$height);
            $graph->img->SetMargin($tablexpos,$rightmargin,$topmargin,$height-$tableypos);

            $graph->SetScale('textlin');
            // $graph->SetMarginColor('white');
            $graph->xaxis->Hide();  //ซ่อน label ใต้กราฟ ต้องอยู่ใต้ SetMarginColor
            $bplot = new \BarPlot($datay[1]);
            $bplot->SetFillGradient('limegreen','limegreen',GRAD_VERT);
            $bplot->SetWeight(0);

            $bplot2 = new \BarPlot($datay[2]);
            $bplot2->SetFillGradient('yellow','yellow',GRAD_VERT);
            $bplot2->SetWeight(0);

            $bplot3 = new \BarPlot($datay[3]);
            $bplot3->SetFillGradient('azure4','azure4',GRAD_VERT);
            $bplot3->SetWeight(0);


            $accbplot = new \AccBarPlot(array($bplot,$bplot2,$bplot3));

            $accbplot->value->Show();
            $accbplot->value->SetFont(FF_ANGSA, FS_NORMAL,14);
            // $accbplot->value->SetAngle(90);
            $accbplot->SetColor('white');
            $accbplot->SetWeight(1);
            $graph->Add($accbplot);

            $table = new \GTextTable();
            // $table->Init(4,3); // Create a 5 rows x 7 columns table
            $data_tbl = array(
                array('','กะ B','กะ C'),
                array('เสียต้องจอด',$gp_2['A']['B'],$gp_2['A']['C']),
                array('จอด',$gp_2['S']['B'],$gp_2['S']['C']),
                array('ใช้งาน',$gp_2['R']['B'],$gp_2['R']['C']));
            $table->Set($data_tbl);
            $table->SetPos(30,$tableypos+1);
            $table->SetFont(FF_ANGSA, FS_BOLD, 12);
            $table->SetAlign('right');
            $table->SetMinColWidth($cellwidth);
            // $table->SetNumberFormat('%0.1f');

            // Format table header row
            // $table->SetRowFillColor(0,'teal@0.7');
            $table->SetRowFont(0,FF_ANGSA, FS_BOLD, 14);
            $table->SetRowAlign(0,'center');

            $table->SetCellFillColor(1,0,'azure4');
            $table->SetCellFillColor(2,0,'yellow');
            $table->SetCellFillColor(3,0,'limegreen');
            // .. and add it to the graph
            $graph->Add($table);
            $graph->title->Set("สัดส่วนการใช้งานรถ FL (รถเช่า) วันที่ ".$end_date);
            $graph->title->SetFont(FF_ANGSA, FS_BOLD, 18);

            $gp['gp1']['link'][0] = "graph/".date('Y'). "/" . date('m') . "/mail/FL_2_" .  $date . ".jpg";
            $gp['gp1']['path'][0] = $path . "/FL_2_" .  $date . ".jpg";
            $graph->Stroke($gp['gp1']['path'][0]);
            echo $gp['gp1']['path'][0]."\r\n";
            // dd($gp['gp1']['path'][0]);
        }
        //แสดง Chart ข้อที่ 2 --------จบ-----------


        //แสดง Chart ข้อที่ 3 -------------------
        // dd($to_gp3['B']);
        natsort($fl_data);
        // dd($fl_data);
        $to_shift = array();
        $to_show = array();
        $i=0;
        foreach($fl_data as $key=>$value){
            if($status[$key]=='Active'){
                if(empty($to_gp3['B'][$key]))  $gp_3['B'][$i]=0;
                else    $gp_3['B'][$i]=$to_gp3['B'][$key];
                if(empty($to_gp3['C'][$key]))  $gp_3['C'][$i]=0;
                else    $gp_3['C'][$i]=$to_gp3['C'][$key];
                array_push($to_shift, 'B', 'C');
                for($l=0;$l<2;$l++){
                    $to_show[] = $value;
                }
                $i++;
            }
        }
        // dd($to_shift);
        $data1y=$gp_3['B'];
        $data2y=$gp_3['C'];


        $nbrbar = count($gp_3['B'])*2; // number of bars and hence number of columns in table
        $cellwidth = 30;  // width of each column in the table
        $tableypos = 332; // top left Y-coordinate for table
        $tablexpos = 130;  // top left X-coordinate for table
        $tablewidth = $nbrbar*$cellwidth; // overall table width
        $rightmargin = 30;  // right margin in the graph
        $topmargin = 60;  // top margin of graph
        $height = 400;  // a suitable height for the image
        $width = $tablexpos+$tablewidth+$rightmargin; // the width of the image
        $graph2 = new \Graph($width,$height);
        $graph2->img->SetMargin($tablexpos,$rightmargin,$topmargin,$height-$tableypos);




        // $graph2 = new \Graph(900, 400);
        $graph2->SetScale("textlin");

        $theme_class = new \UniversalTheme;
        $graph2->SetTheme($theme_class);


        // //$graph->yaxis->SetTickPositions(array(0, 30, 60, 90, 120, 150), array(15, 45, 75, 105, 135));
        $graph2->SetBox(false);

        $graph2->ygrid->SetFill(false);
        // $graph2->xaxis->SetTickLabels($to_show);
        // $graph2->xaxis->SetLabelAlign('center', 'center');
        // $graph2->xaxis->title->SetFont(FF_ANGSA, FS_BOLD, 18);
        // $graph2->xaxis->SetFont(FF_ANGSA, FS_NORMAL, 14);
        $graph2->xaxis->Hide();  //ซ่อน label ใต้กราฟ ต้องอยู่ใต้ SetMarginColor
        $graph2->yaxis->SetColor("#000000");
        $graph2->yaxis->HideLine(true);
        $graph2->yaxis->HideTicks(false, false);
        $graph2->yaxis->SetFont(FF_ANGSA, FS_NORMAL, 14);

        // Create the bar plots
        $b1plot2 = new \BarPlot($data1y);
        $b2plot2 = new \BarPlot($data2y);

        // Create the grouped bar plot
        $gbplot2 = new \GroupBarPlot(array($b1plot2, $b2plot2));
        // ...and add it to the graPH
        $graph2->Add($gbplot2);

        $b1plot2->SetColor("#000000");  //สีเส้น bar
        $b1plot2->SetFillColor("#FFC000");  //สี bar
        // $b1plot2->SetLegend("Sum of list");
        $b1plot2->value->SetFormat('%01.1f');
        $b1plot2->value->SetColor("#000000");
        $b1plot2->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
        $b1plot2->value->Show();
        $b1plot2->value->SetAngle(90);

        $b2plot2->SetColor("#000000");  //สีเส้น bar
        $b2plot2->SetFillColor("#FFC000");  //สี bar
        // $b2plot2->SetLegend("Sum of random result");
        $b2plot2->value->SetFormat('%01.1f');
        $b2plot2->value->SetColor("#000000");
        $b2plot2->value->Show();
        $b2plot2->value->SetFont(FF_ANGSA, FS_NORMAL,14);
        $b2plot2->value->SetAngle(90);

        $graph2->ygrid->Show(false);

        $graph2->legend->SetFrameWeight(1);
        $graph2->legend->SetColumns(2);
        $graph2->legend->SetColor('#FFC000', '#FFC000');
        $graph2->legend->SetPos(0.5, 0.08, 'center', 'top');

        $graph2->title->SetFont(FF_ANGSA, FS_BOLD, 18);
        $graph2->title->Set("กราฟแสดงชั่วโมงการใช้รถประจำวันที่ ".$end_date);

        // $canvasG = new \CanvasGraph(900,400);

        // Setup the basic table and data
        $data = array($to_shift,$to_show);
        $table = new \GTextTable();
        $table->Set($data);
        // $table->SetPos(40,333); //ตำแหน่งเริ่มต้นของตาราง
        $table->SetPos(40,$tableypos+1);/////////////////////////////////////////////
        // $table->SetMinColWidth(30);
        $i=0;
        for($i=0;$i<count($to_show);$i++){
            // echo $i.'--';
            $table->MergeCells(1,$i,1,$i+1);
            $i++;
        }
        $table->SetFont(FF_ANGSA, FS_NORMAL, 12);
        $table->SetAlign('center');
        $table->SetMinColWidth($cellwidth+2.6);////////////////////////////////////////////2.8 2.7
        $graph2->Add($table);

        $gp['gp2']['link'][0] = "graph/".date('Y'). "/" . date('m') . "/mail/FL_3_" .  $date . ".jpg";
        $gp['gp2']['path'][0] = $path . "/FL_3_" .  $date . ".jpg";
        $graph2->Stroke($gp['gp2']['path'][0]);
        echo $gp['gp2']['path'][0]."\r\n";
        // dd($gp['gp2']['path'][0]);
        //แสดง Chart ข้อที่ 3 ----------จบ---------


        //แสดง Chart ข้อที่ 4 -------------------
        $query = Forklift::where('status','Active')->orderByRaw('len(name)')->get();
        foreach($query as $key){
            $fl = FLog::where('forklift_id',$key->id)->where('log_date','<', date('Y-m').'-01')->whereNotNull('time')->orderBy('log_date','desc')->orderBy('shift','desc')->limit(1)->get();
            if(count($fl)>0 && $fl[0]['updated_at'] > $key->updated_at){
                $gp4_time[$key->id] = number_format($fl[0]['time'],1,'.','');
            }else{
                $gp4_time[$key->id] = number_format($key->time,1,'.','');
            }
        }
        // dd($gp4_time);   $fl_data[$key->id]  $locate_data[$key->id]
        $fl_log = array();
        $total_val_gp = array();
        $query_1 = new FLog;
        $query_1 = $query_1->join('flocations', 'f_logs.f_location_id', '=', 'flocations.id');
        $query_1 = $query_1->join('forklifts', 'f_logs.forklift_id', '=', 'forklifts.id');
        $query_1 = $query_1->whereMonth('log_date', '=', date('m'))->whereYear('log_date', '=', date('Y'));
        $query_1 = $query_1->select('f_logs.id', 'f_logs.log_date', 'f_logs.f_location_id', 'f_logs.forklift_id', 'f_logs.fperson_id', 'f_logs.shift', 'f_logs.type', 'f_logs.time', 'f_logs.note');
        $query_1 = $query_1->orderBy('forklifts.name')->orderBy('f_logs.log_date')->orderBy('f_logs.shift')->get();
        // dd($query_1);
        if(count($query_1)>0){
            foreach($query_1 as $key){
                if(empty($locate_data[$key->f_location_id]))    $chk_loc = '';
                else    $chk_loc = $locate_data[$key->f_location_id];
                // echo $chk_loc.':';
                if(empty($fl_data[$key->forklift_id]))    $chk_fl = '';
                else $chk_fl = $fl_data[$key->forklift_id];
                // echo $chk_fl.'--';
                // $gp4_log[$chk_loc][$chk_fl][$key->shift] = $key->type;  //ใช้เรียงลำดับ
                // echo $key->type.'-';
                if($key->type == 'R'){
                    if(empty($fl_id) || $fl_id <> $chk_fl){
                        $to_diff = $gp4_time[$key->forklift_id];
                    }
                    $fl_id = $chk_fl;
                    $key_time = number_format($key->time,1,'.','');
                    $diff_time = $key_time-$to_diff;
                    if(!empty($total_val_gp['count'][$chk_loc][$chk_fl][$key->shift])){
                        $total_val_gp['count'][$chk_loc][$chk_fl][$key->shift] += 1;
                        $total_val_gp['sum'][$chk_loc][$chk_fl][$key->shift] += $diff_time;

                    }else{
                        $total_val_gp['count'][$chk_loc][$chk_fl][$key->shift] = 1;
                        $total_val_gp['sum'][$chk_loc][$chk_fl][$key->shift] = $diff_time;
                    }
                    $to_diff = number_format($key->time,1,'.','');
                    $to_gp5[$chk_fl][$chk_loc][$key->shift][$key->log_date] = $diff_time;
                // }else{
                //     $total_val_gp['count'][$chk_loc][$chk_fl][$key->shift] = 0;
                //     $total_val_gp['sum'][$chk_loc][$chk_fl][$key->shift] = 0;
                }
            }
        }
        // dd($to_gp5);
        // dd($total_val_gp);
        $to_shift = array();
        $to_show = array();
        $to_loc = array();
        $i=0;

        $date_in_month = date("t", mktime(0, 0, 0, 7, 1, 2000));

        // dd($fl_data);
        // dd($locate_data);
        //เรียงลำดับใหม่
        foreach ($fl_data as $kfl=>$vfl) {
            foreach($locate_data as $kloc => $vloc){
                // echo $vfl.'-'.$vloc."\r\n";
                if(isset($total_val_gp['count'][$vloc][$vfl]['B']) || isset($total_val_gp['count'][$vloc][$vfl]['C']))    $to_sort[$vloc][$vfl] = $vfl.'-'.$vloc;
            }
        }
        // dd($to_sort);
        foreach ($to_sort as $kloc => $vloc) {
            foreach($vloc as $kfl=>$vfl){
                if(!empty($total_val_gp['sum'][$kloc][$kfl]['B']))    $gp_4['B'][] = $total_val_gp['sum'][$kloc][$kfl]['B']/$total_val_gp['count'][$kloc][$kfl]['B'];
                else $gp_4['B'][] = 0;
                if(!empty($total_val_gp['sum'][$kloc][$kfl]['C']))    $gp_4['C'][] = $total_val_gp['sum'][$kloc][$kfl]['C']/$total_val_gp['count'][$kloc][$kfl]['C'];
                else $gp_4['C'][] = 0;

                array_push($to_shift, 'B', 'C');
                for($l=0;$l<2;$l++){
                    $to_show[] = $kfl;
                    $to_loc[] = $kloc;
                }

                $to_shift_gp5 = array();
                $to_date_gp5 = array();
                // $date_in_month = 28;
                for($i=1;$i<=$date_in_month;$i++){
                    // if($kfl=='FL No.1' && $kloc =='WH-ST')    echo $i.'--';
                    if(empty($to_gp5[$kfl][$kloc]['B'][date('Y-m-d',strtotime(date('Y-m').'-'.$i))])){
                        $to_show_gp5[$kfl][$kloc]['B'][] = 0;
                    }else{
                        $to_show_gp5[$kfl][$kloc]['B'][] = $to_gp5[$kfl][$kloc]['B'][date('Y-m-d',strtotime(date('Y-m').'-'.$i))];
                    }
                    if(empty($to_gp5[$kfl][$kloc]['C'][date('Y-m-d',strtotime(date('Y-m').'-'.$i))])){
                        $to_show_gp5[$kfl][$kloc]['C'][] = 0;
                    }else{
                        $to_show_gp5[$kfl][$kloc]['C'][] = $to_gp5[$kfl][$kloc]['C'][date('Y-m-d',strtotime(date('Y-m').'-'.$i))];
                    }
                    array_push($to_shift_gp5, 'B', 'C');
                    for($l=0;$l<2;$l++){
                        $to_date_gp5[] = $i;
                    }
                }
            }
        }


        //bar1
        $data1y=$gp_4['B'];
        //bar2
        $data2y=$gp_4['C'];
        // dd(count($to_loc));
        // foreach ($data6y as &$y) { $y -=10; }


        $nbrbar = count($gp_4['B'])*2; // number of bars and hence number of columns in table
        $cellwidth = 30;  // width of each column in the table
        $tableypos = 332; // top left Y-coordinate for table
        $tablexpos = 130;  // top left X-coordinate for table
        $tablewidth = $nbrbar*$cellwidth; // overall table width
        $rightmargin = 30;  // right margin in the graph
        $topmargin = 60;  // top margin of graph
        $height = 400;  // a suitable height for the image
        $width = $tablexpos+$tablewidth+$rightmargin; // the width of the image
        $graph3 = new \Graph($width,$height);
        $graph3->img->SetMargin($tablexpos,$rightmargin,$topmargin,$height-$tableypos);


        // Create the graph. These two calls are always required
        // $graph3 = new \Graph(900,400,'auto');
        $graph3->SetScale("textlin");
        $graph3->SetY2Scale("lin",0,90);
        $graph3->SetY2OrderBack(false);

        $theme_class = new \UniversalTheme;
        $graph3->SetTheme($theme_class);

        // $graph3->SetMargin(40,20,46,80);

        $graph3->SetBox(false);

        $graph3->xaxis->Hide();  //ซ่อน label ใต้กราฟ ต้องอยู่ใต้ SetMarginColor
        $graph3->ygrid->SetFill(false);
        // $graph3->xaxis->SetTickLabels(array('A','B','C','D'));
        $graph3->yaxis->HideLine(false);
        $graph3->yaxis->HideTicks(false,false);

        // Create the bar plots
        $b1plot = new \BarPlot($data1y);
        $b2plot = new \BarPlot($data2y);

        $gbplot = new \GroupBarPlot(array($b1plot,$b2plot));

        // ...and add it to the graPH
        $graph3->Add($gbplot);
        // $graph->AddY2($lplot);

        $b1plot->SetColor("#0000CD");   //สีเส้น bar
        $b1plot->SetFillColor("#0000CD");   //สี bar
        // $b1plot->SetLegend("Cliants");   //สำหรับใส่ป้ายกำกับ
        $b1plot->value->SetFormat('%01.1f');
        $b1plot->value->SetColor("#000000");
        $b1plot->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
        $b1plot->value->SetAngle(90);
        $b1plot->value->Show();

        $b2plot->SetColor("#B0C4DE");   //สีเส้น bar
        $b2plot->SetFillColor("#B0C4DE");   //สี bar
        // $b2plot->SetLegend("Machines");  //สำหรับใส่ป้ายกำกับ
        $b2plot->value->SetFormat('%01.1f');
        $b2plot->value->SetColor("#000000");
        $b2plot->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
        $b2plot->value->SetAngle(90);
        $b2plot->value->Show();

        $graph3->legend->SetFrameWeight(1);
        $graph3->legend->SetColumns(2);
        $graph3->legend->SetColor('#4E4E4E','#00A78A');

        $table = new \GTextTable();
        $table->Init(3,count($to_loc)); // Create a 5 rows x 7 columns table
        $data_tbl = array($to_shift, $to_show, $to_loc);
        $table->Set($data_tbl);
        // $table->SetPos(40,320);//ตำแหน่งเริ่มต้นของตาราง
        $table->SetPos(40,$tableypos+1);/////////////////////////////////////////////
        $i=0;
        $c=0;
        foreach ($to_sort as $kloc => $vloc) {
            foreach($vloc as $kfl=>$vfl){
                // echo $i.':'.($i+1).'--';
                $table->MergeCells(1,$i,1,$i+1);
                $i=$i+2;
            }
            // echo ';'.$c.':'.($i+1).'--';
            $table->MergeCells(2,$c,2,$i-1);
            $c=$i;
        }
        $table->SetFont(FF_ANGSA, FS_NORMAL, 8);////////ขนาดตัวอักษรแถว 2 ลงมา
        $table->SetAlign('center');
        // $table->SetMinColWidth($cellwidth);
        $table->SetMinColWidth($cellwidth+2.7);////////////////////////////////////////////  3
        // $table->SetNumberFormat('%0.1f');

        // Format table header row
        // $table->SetRowFillColor(0,'teal@0.7');
        $table->SetRowFont(0,FF_ANGSA, FS_NORMAL, 10);////////ขนาดตัวอักษรแถวแรก
        $table->SetRowAlign(0,'center');
        $graph3->Add($table);
        $graph3->title->Set("กราฟแสดงชั่วโมงการใช้รถโดยเฉลี่ยในเดือน ".date('m/Y'));
        $graph3->title->SetFont(FF_ANGSA, FS_NORMAL, 18);

        $gp['gp3']['link'][0] = "graph/".date('Y'). "/" . date('m') . "/mail/FL_4_" .  $date . ".jpg";
        $gp['gp3']['path'][0] = $path . "/FL_4_" .  $date . ".jpg";
        $graph3->Stroke($gp['gp3']['path'][0]);
        echo $gp['gp3']['path'][0]."\r\n";
        // dd($gp['gp3']['path'][0]);
        //แสดง Chart ข้อที่ 4 ---------จบ----------

        // dd($to_show_gp5);
        //แสดง Chart ข้อที่ 5 -------------------
        $count_gp = 0;
        foreach ($to_show_gp5 as $kfl => $vfl) {
            foreach ($vfl as $kloc => $vloc) {
                $data1y=$to_show_gp5[$kfl][$kloc]['B'];
                $data2y=$to_show_gp5[$kfl][$kloc]['C'];
                // dd($data2y);

                $count_col = count($to_show_gp5[$kfl][$kloc]['B']);
                $nbrbar = $count_col*2; // number of bars and hence number of columns in table
                $cellwidth = 17;
                $tableypos = 319; // top left Y-coordinate for table
                $tablexpos = 130;  // top left X-coordinate for table
                $tablewidth = $nbrbar*$cellwidth; // overall table width
                $rightmargin = 30;  // right margin in the graph
                $topmargin = 60;  // top margin of graph
                $height = 384;  // a suitable height for the image
                $width = $tablexpos+$tablewidth+$rightmargin; // the width of the image
                $graph4 = new \Graph($width,$height);
                $graph4->img->SetMargin($tablexpos,$rightmargin,$topmargin,$height-$tableypos);


                // $graph4 = new \Graph(1200,400,'auto');
                $graph4->SetScale("textlin");
                $graph4->SetY2Scale("lin",0,90);
                $graph4->SetY2OrderBack(false);

                $theme_class = new \UniversalTheme;
                $graph4->SetTheme($theme_class);

                // $graph4->SetMargin(40,20,46,80);
                $graph4->SetBox(false);

                $graph4->xaxis->Hide();  //ซ่อน label ใต้กราฟ ต้องอยู่ใต้ SetMarginColor
                $graph4->ygrid->SetFill(false);
                // $graph3->xaxis->SetTickLabels(array('A','B','C','D'));
                $graph4->yaxis->HideLine(false);
                $graph4->yaxis->HideTicks(false,false);

                // Create the bar plots
                $b1plot = new \BarPlot($data1y);
                $b2plot = new \BarPlot($data2y);

                $gbplot = new \GroupBarPlot(array($b1plot,$b2plot));

                // ...and add it to the graPH
                $graph4->Add($gbplot);
                // $graph->AddY2($lplot);

                $b1plot->SetColor("#FFFFFF");   //สีเส้น bar
                $b1plot->SetFillColor("#5B9BD5");   //สี bar
                // $b1plot->SetLegend("Cliants");   //สำหรับใส่ป้ายกำกับ
                $b1plot->value->SetFormat('%01.1f');
                $b1plot->value->SetColor("#000000");
                $b1plot->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
                $b1plot->value->SetAngle(90);
                $b1plot->value->Show();

                $b2plot->SetColor("#FFFFFF");   //สีเส้น bar
                $b2plot->SetFillColor("#5B9BD5");   //สี bar
                // $b2plot->SetLegend("Machines");  //สำหรับใส่ป้ายกำกับ
                $b2plot->value->SetFormat('%01.1f');
                $b2plot->value->SetColor("#000000");
                $b2plot->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
                $b2plot->value->SetAngle(90);
                $b2plot->value->Show();

                $graph4->legend->SetFrameWeight(1);
                $graph4->legend->SetColumns(2);
                // $graph4->legend->SetColor('#4E4E4E','#00A78A');

                $table = new \GTextTable();
                $table->Init(2,count($to_date_gp5)); // Create a 5 rows x 7 columns table
                $data_tbl = array($to_shift_gp5, $to_date_gp5);
                $table->Set($data_tbl);
                // $table->SetPos(40,320);//ตำแหน่งเริ่มต้นของตาราง
                $table->SetPos(40,$tableypos+1);/////////////////////////////////////////////
                $i=0;
                for($i=0;$i<count($to_date_gp5);$i++){
                    // echo $i.'--';
                    $table->MergeCells(1,$i,1,$i+1);
                    $i++;
                }
                $table->SetFont(FF_ANGSA, FS_NORMAL, 12);
                $table->SetAlign('center');
                $table->SetMinColWidth($cellwidth+1.6);//////////////////////////////////////////// 1.75
                // $table->SetMinColWidth($cellwidth);
                // $table->SetNumberFormat('%0.1f');

                // Format table header row
                // $table->SetRowFillColor(0,'teal@0.7');
                $table->SetRowFont(0,FF_ANGSA, FS_NORMAL, 12);  //font ของ row แรก
                $table->SetRowAlign(0,'center');

                $graph4->Add($table);
                $graph4->title->Set("$kfl (พื้นที่ปฏิบัติงาน $kloc) ในเดือน ".date('m/Y'));
                $graph4->title->SetFont(FF_ANGSA, FS_NORMAL, 18);

                $gp['gp4']['link'][$count_gp] = "graph/".date('Y'). "/" . date('m') . "/mail/FL_5_".$kfl.'-'.$kloc.'_' .  $date . ".jpg";
                $gp['gp4']['path'][$count_gp] = $path . "/FL_5_".$kfl.'-'.$kloc.'_' .  $date . ".jpg";
                $graph4->Stroke($gp['gp4']['path'][$count_gp]);
                echo $gp['gp4']['path'][$count_gp]."\r\n";
                // dd($gp['gp4']['path'][$count_gp]);
                $count_gp++;
            }
        }
        //แสดง Chart ข้อที่ 5 ---------จบ----------
        // dd('auto FL');
        Mail::send(new ReportFL($tbl_1,$st_date,$ed_date,$fl_data,$locate_data,$total_val,$days,$gp));
    }
}
