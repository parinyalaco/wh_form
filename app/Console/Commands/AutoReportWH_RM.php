<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\RmManualController;

class AutoReportWH_RM extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'WHForm:AutoReportWH_RM';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Use to send mail Auto report WH_RM';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $controller = new RmManualController();
        $controller->to_auto_mail();
    }
}
