<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
// use App\Models\ToImportfile;
// use App\Models\Material;
// use App\Models\Vendor;
// use App\Models\Unit;
// use App\Models\ProductType;
use App\Models\ProductInput;
use App\Models\ProductInputEn;
use App\Models\ProductInputDc;
use App\Http\Controllers\ProductInputController;

use SimpleXLSX;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Arr;

class ImportProductInput extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auto:importproductinput {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import File to WH Form System';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $type = $this->argument('type');
        // dd($type);

        $directory = config('myconfig.directory.productinput.'.$type.'.queues');
        $completedirectory = config('myconfig.directory.productinput.'.$type.'.completes');

        $scanned_directory = array_diff(scandir($directory), array('..', '.'));
        // dd($scanned_directory);
        $files = \File::files($directory);
        // dd($files);

        if(count($files)>0){

            $vender_data = array();
            $unit_data = array();
            $pdtype_data = array();
            $mat_data = array();
            $pd_input = array();
            $controller = new ProductInputController();
            $test = $controller->base_data($type);
            if(!empty($test[0]))    $vender_data = $test[0];
            if(!empty($test[1]))    $unit_data = $test[1];
            if(!empty($test[2]))    $pdtype_data = $test[2];
            if(!empty($test[3]))    $mat_data = $test[3];
            if(!empty($test[4]))    $pd_input = $test[4];

            foreach ($files as $fileupload) {  

                $tmpfolder = date('Ymd');
                // dd($tmpfolder);
                $uploadname = $fileupload->getFilename();
                $name = $type.'-'.md5($fileupload->getFilename() . time()) . '.' . $fileupload->getExtension();
                $destinationPath = public_path($completedirectory . $tmpfolder);
                // $fileupload->move($destinationPath, $name);
                
                $uploadpath = $tmpfolder  . "/" . $name;
                $uploadfile = $destinationPath  . "/" . $name; 
                // $filenewname = md5(time());
                
                
                $to_save = array();       
                $tmpUploadD = array();            
                if ($xlsx = SimpleXLSX::parse($fileupload)) {
                    if($type == 'laco') $c_col = 14;
                    else $c_col = 12;
                    // dd($c_col);
                    if(count($xlsx->rows()[0])==$c_col){ 

                        $to_save = new ProductInputController();
                        $to_save->save_file($xlsx, $type, $uploadname, $uploadpath, $vender_data, $unit_data, $pdtype_data, $mat_data, $pd_input);

                        if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                            mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
                        }                

                        if (copy($directory . '/' . $uploadname, $completedirectory . '/' . $tmpfolder . '/' . $name)) {                    
                            unlink($directory . '/' . $uploadname);
                        }

                        echo "Save file : ".$uploadname." is complete\r\n";
                    }else{
                        echo $uploadname."Number of column is wrong!!\r\n";
                    }
                }     
            }
        }
    }    
}
