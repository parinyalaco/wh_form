<?php

namespace App\Console\Commands;

use App\Models\RecvRmOther;
use App\Models\RecvRmOtherPlanMain;
use App\Models\RecvRmOtherPlan;
use App\Mail\AutoRecvRmOtherMail;
use Illuminate\Console\Command;

use Illuminate\Support\Facades\Mail;

class AutoReportRmOtherWH extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'WHForm:AutoReportRmOther';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Report Summary WHRM Banana RM';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $selectDate = date('Y-m-d', strtotime("-1 days"));     
        $recvrmotherplanmain = RecvRmOtherPlanMain::where('status', 'Active')->first();
        $data = RecvRmOther::where('recv_rm_other_plan_m_id',$recvrmotherplanmain->id)
        ->where('act_date', $selectDate)
        ->orderBy('car_no','ASC')
        ->get();

        $datasum = RecvRmOther::where('recv_rm_other_plan_m_id', $recvrmotherplanmain->id)
        ->where('act_date','<=', $selectDate)
        ->selectRaw('sum(weight) as sumall')
        ->first();

        //dd($datasum);

        $plan = RecvRmOtherPlan::where('recv_rm_other_plan_m_id', $recvrmotherplanmain->id)
        ->where('plan_date', $selectDate)
        ->first();

        $movetxt = [
            '< 30' => '< 30 นาที',
            '< 45' => '< 45 นาที',
            '< 60' => '< 1 ชั่วโมง',
            '< 120' => '< 2 ชั่วโมง',
            '>= 120' => '> 2 ชั่วโมง',
        ];

        $movecase = [
            '>= 120', '< 120', '< 60', '< 45', '< 30'
        ];


        $showdata = [];
        $showdata['date'] = $selectDate;
        $showdata['totalplan'] = $recvrmotherplanmain->total_plan;
        $showdata['totalact'] = $datasum->sumall;
        $showdata['limitsum'] = 0;
        if(isset($plan->weight)){
            $showdata['limitsum'] = $plan->weight;
        }
        $showdata['realsum'] = 0;
        $showdata['car'] = $data->count();
        $showdata['worker'] = 0;
        foreach ($movetxt as $key => $value) {
            $showdata['move'][$key] = 0;
        }
        foreach($data  as $dataObj) {
            $showdata['realsum'] += $dataObj->weight;
            $showdata['worker'] += $dataObj->staff_no;
            if(isset($showdata['firstcar'])){
                if($showdata['firstcar'] >  $dataObj->arv_laco_time){
                    $showdata['firstcar'] = $dataObj->arv_laco_time;
                }
            }else{
                $showdata['firstcar'] = $dataObj->arv_laco_time;
            }
            if (isset($showdata['lastcar'])) {
                if ($showdata['lastcar'] <  $dataObj->arv_laco_time) {
                    $showdata['lastcar'] = $dataObj->arv_laco_time;
                }
            } else {
                $showdata['lastcar'] = $dataObj->arv_laco_time;
            }
            if (isset($showdata['lastmove'])) {
                if ($showdata['lastmove'] <  $dataObj->end_move_time) {
                    $showdata['lastmove'] = $dataObj->end_move_time;
                }
            } else {
                $showdata['lastmove'] = $dataObj->end_move_time;
            }
            
            //chackdate rang
           // echo date('i', strtotime($dataObj->arv_laco_time));
            if(date('i', strtotime($dataObj->arv_laco_time)) == '00'){
                $round =  "Before ".date('H', strtotime($dataObj->arv_laco_time));
                
            }else{

                $round =  "Before " . date('H', strtotime($dataObj->arv_laco_time)+3600);
            }
            if(isset($showdata['round'][$round])){
                $showdata['round'][$round]['car_no'] += 1;
                $showdata['round'][$round]['sumpack'] += $dataObj->qty;
                $showdata['round'][$round]['sumweight'] += $dataObj->weight;
            }else{
                $showdata['round'][$round]['car_no'] = 1;
                $showdata['round'][$round]['sumpack'] = $dataObj->qty;
                $showdata['round'][$round]['sumweight'] = $dataObj->weight;
            }

            foreach ($movetxt as $movecasecond=> $movecasetxt) {
                $difftime = (strtotime($dataObj->end_move_time) - strtotime($dataObj->start_move_time))/60;
                //echo  $difftime;
                $strcond = "return (" . $difftime . $movecasecond . ");";
                if (eval($strcond)) {
                    $showdata['move'][$movecasecond] += 1;
                    break;
                }
            }
        }

        //dd($showdata);

        require_once app_path() . '/jpgraph/jpgraph.php';
        require_once app_path() . '/jpgraph/jpgraph_bar.php';
        
        //graph 1
        $data1y = array($showdata['limitsum']);
        $data2y = array($showdata['realsum']);

        $graph = new \Graph(650, 400, 'auto');
        $graph->SetScale("textlin");

      

        $theme_class = new \UniversalTheme;
        $graph->SetTheme($theme_class);

      
        $graph->SetBox(false);

        $graph->ygrid->SetFill(false);
        $graph->xaxis->SetTickLabels(array($selectDate));
        $graph->xaxis->SetLabelAlign('left', 'center');
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false, false);

        // Create the bar plots
        $b1plot = new \BarPlot($data1y);
        $b2plot = new \BarPlot($data2y);

        // Create the grouped bar plot
        $gbplot = new \GroupBarPlot(array($b1plot, $b2plot));
        // ...and add it to the graPH
        $graph->Add($gbplot);


        $b1plot->SetColor("white");
        $b1plot->SetFillColor("#cc1111");

        $b1plot->SetLegend("Plan");
        $b1plot->value->SetFormat('%d');
        $b1plot->value->Show();

        $b2plot->SetColor("white");
        $b2plot->SetFillColor("#11cccc");

        $b2plot->SetLegend("Actual");
        $b2plot->value->SetFormat('%d');
        $b2plot->value->Show();

        $graph->title->Set("Plan Vs. Actual");

        $graph->legend->SetPos(0.8, 0.05, 'center', 'top');

        // Display the graph
        $showdata['graph'][1]['url'] = "/graph/" . date('Y') . "/" . date('m') . "/mail/bar_rpt_" .  md5(date('YmdHi')) . ".jpg";
        $showdata['graph'][1]['path'] = public_path() . $showdata['graph'][1]['url'];

        $graph->Stroke($showdata['graph'][1]['path']);


        //Graph 2
        $data1y = array($showdata['totalplan']);
        $data2y = array($showdata['totalact']);

        $graph = new \Graph(650, 400, 'auto');
        $graph->SetScale("textlin");

        // dd($showdata);

        $theme_class = new \UniversalTheme;
        $graph->SetTheme($theme_class);


        $graph->SetBox(false);

        $graph->ygrid->SetFill(false);

        $graph->xaxis->SetLabelAlign('left', 'center');
        $graph->xaxis->SetTickLabels(array('Target'));
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false, false);

        // Create the bar plots
        $b1plot = new \BarPlot($data1y);
        $b2plot = new \BarPlot($data2y);

        // Create the grouped bar plot
        $gbplot = new \GroupBarPlot(array($b1plot, $b2plot));
        // ...and add it to the graPH
        $graph->Add($gbplot);


        $b1plot->SetColor("white");
        $b1plot->SetFillColor("#cc1111");

        $b1plot->SetLegend("Total Plan");
        $b1plot->value->SetFormat('%d');
        $b1plot->value->Show();

        $b2plot->SetColor("white");
        $b2plot->SetFillColor("#11cccc");

        $b2plot->SetLegend("Total Actual");
        $b2plot->value->SetFormat('%d');
        $b2plot->value->Show();

        $graph->title->Set("Total Plan Vs. Actual");

        $graph->legend->SetFrameWeight(1);

        $graph->legend->SetPos(0.8, 0.05, 'center', 'top');

        // Display the graph
        $showdata['graph'][2]['url'] = "/graph/" . date('Y') . "/" . date('m') . "/mail/bar_2rpt_" .  md5(date('YmdHi')) . ".jpg";
        $showdata['graph'][2]['path'] = public_path() . $showdata['graph'][2]['url'];

        $graph->Stroke($showdata['graph'][2]['path']);

        //Graph 3
        $labeldata = [];
        $datay = [];
        foreach ($movetxt as $key => $value) {
            $labeldata[] = $value;
            $datay[] = $showdata['move'][$key];
        }

        $graph = new \Graph(650, 400, 'auto');
        $graph->SetScale("textlin");

        //$theme_class="DefaultTheme";
        //$graph->SetTheme(new $theme_class());

        // set major and minor tick positions manually
        $graph->SetBox(false);

        //$graph->ygrid->SetColor('gray');
        $graph->ygrid->SetFill(false);

        $graph->xaxis->title->SetFont(FF_ANGSA, FS_BOLD, 18);
        $graph->xaxis->SetTickLabels($labeldata);

        $graph->xaxis->SetFont(FF_ANGSA, FS_NORMAL, 14);
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false, false);

        // Create the bar plots
        $b1plot = new \BarPlot($datay);

        // ...and add it to the graPH
        $graph->Add($b1plot);


        $b1plot->SetColor("white");
        $b1plot->SetWidth(45);
        $b1plot->value->SetFormat('%d');
        $b1plot->value->Show();
        $graph->title->Set("จำนวครั้งของเวลาการลงวัตถุดิบ");
        $graph->title->SetFont(FF_ANGSA, FS_BOLD, 18);

        // Display the graph
        $showdata['graph'][3]['url'] = "/graph/" . date('Y') . "/" . date('m') . "/mail/bar_3rpt_" .  md5(date('YmdHi')) . ".jpg";
        $showdata['graph'][3]['path'] = public_path() . $showdata['graph'][3]['url'];
        
        $graph->Stroke($showdata['graph'][3]['path']);


        // /dd($showdata);
        $testemail = array(
            'PKP' => 'parinya.k@lannaagro.com',
        );

        Mail::to($testemail)->send(new AutoRecvRmOtherMail($showdata, $movetxt));
        return 0;
    }
}
