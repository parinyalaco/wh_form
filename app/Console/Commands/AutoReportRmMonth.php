<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ProductInputController;

class AutoReportRmMonth extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'WHForm:ReportProductMonth';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Use to send mail Auto report month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $controller = new ProductInputController();
        $controller->sentmail_month();
    }
}
