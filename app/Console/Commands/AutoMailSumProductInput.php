<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ProductInputController;

class AutoMailSumProductInput extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'WHForm:AutoMailSumProductInput';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To send sum mail product input month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $controller = new ProductInputController();
        $controller->sum_mail_month();
    }
}
