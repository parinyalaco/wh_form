<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use SimpleXLSX;
use App\Models\WhPlanAct;
use App\Models\WhPlanActMS;
use App\Models\WhPlanActD;
use App\Models\WhActType;
use App\Models\WhActMaterial;
use App\Models\Unit;

use File;

class ImportWhPlanAct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:whplanact';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import File WH Plan Act ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tmpfolder = date('Ymd');
        $directory = config('myconfig.directory.whactivity.queues.plan');
        $completedirectory = config('myconfig.directory.whactivity.completes.plan');
        $scanned_directory = array_diff(scandir($directory), array('..', '.'));
        if(count($scanned_directory)>0){
            $files = \File::files($directory);   
            foreach ($files as $fileupload) {
                $chk = $this->_readdata($fileupload);   
                // dd($chk);         
                if($chk=='0'){
                    $uploadname = $fileupload->getFilename();
                    $str_pos = substr($uploadname,0,strrpos($uploadname,'.'));
                    // dd($str_pos);
                    if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                        mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
                    }                
                    $name = $str_pos.'-'.date('YmdHis') . '.' . $fileupload->getExtension();
                    if (copy($directory . '/' . $uploadname, $completedirectory . '/' . $tmpfolder . '/' . $name)) {                    
                        unlink($directory . '/' . $uploadname);
                    }
                    echo "Import File: " . $uploadname." Complete!;\n";
                }else{
                    echo $chk;
                }
            }
        }else{
            echo "No File;\n";
        }        
    }

    private function _readdata($filename)
    {
        $importedrow = 0;
        if ($xlsx = SimpleXLSX::parse($filename)) {
            $uploadname = $filename->getFilename();
            $query = WhActMaterial::join('units', 'wh_act_materials.unit_id', '=', 'units.id')
                ->select('wh_act_materials.id', 'wh_act_materials.name', 'wh_act_materials.mat_desc', 'wh_act_materials.unit_id', 'units.name AS unit_name')
                ->get();
            foreach ($query as $key) {
                $mat[$key->name][$key->mat_desc][$key->unit_name] = $key->id;
            }

            $query = Unit::all();
            foreach ($query as $key) {
                $unit[$key->name] = $key->id;
            }

            $query = WhActType::all();
            foreach ($query as $key) {
                $act_type[$key->code] = $key->id;
            }
            $i = 0;
            foreach ($xlsx->rows(0) as $r => $row) {    //ทำงานแต่ใน sheet แรก
                if(!empty($row[0])){
                    if(empty($row[1])){                        
                        $chk_date = strpos($row[0],"ate");
                        if(!empty($chk_date)){                        
                            //date
                            // echo "find Date -".$chk_date.'/'.$row[0];
                            $i++;
                            $to_date_exp = explode(':',$row[0]);
                            $date_exp = explode('.',trim($to_date_exp[1]));
                            $file_date[$i] = $date_exp[2].'-'.$date_exp[1].'-'.$date_exp[0];
                            // dd($file_date);
                        }else{
                            //time
                            // echo "no find Date -".$chk_date.'/'.$row[0];
                            $to_date_exp = explode(':',$row[0]);
                            $h_time = trim($to_date_exp[1]);
                            $file_time[$i] = $h_time.':'.$to_date_exp[2].':'.$to_date_exp[3];
                            // dd($file_time);
                        }
                    }else{
                        $chk_data[0] = strpos($row[0],"Allowed");
                        $chk_data[1] = strpos($row[1],"No");
                        if(empty($chk_data[0]) && empty($chk_data[0])){
                            $data[$i][$r]['mov_allowed'] = $row[0];                            
                            $data[$i][$r]['reservation_no'] = $row[1];
                            $data[$i][$r]['reservation_item'] = $row[2];

                            $date_req = explode('.',trim($row[3]));
                            $requirement_date[$date_req[2].'-'.$date_req[1].'-'.$date_req[0]] = $date_req[2].'-'.$date_req[1].'-'.$date_req[0];

                            if(empty($act_type[$row[4]])){
                                return "Unknow Movement Type on row number ".($r+1)."\n";
                            }else{
                                $data[$i][$r]['wh_act_type_id'] = $act_type[$row[4]]; 
                            }

                            if(!empty($mat[$row[5]][$row[6]][$row[8]])){
                                $data[$i][$r]['wh_act_material_id'] = $mat[$row[5]][$row[6]][$row[8]];
                            }else{
                                if(empty($unit[$row[8]])){
                                    $add_unit['name'] = $row[8];
                                    $unit_id = Unit::create($add_unit)->id; 
                                    $unit[$row[8]] = $unit_id;
                                }else{
                                    $unit_id = $unit[$row[8]];
                                }
                                $add_mat['name'] = $row[5];
                                $add_mat['mat_desc'] = $row[6];
                                $add_mat['unit_id'] = $unit_id;
                                $mat_id = WhActMaterial::create($add_mat)->id; 
                                $mat[$row[5]][$row[6]][$row[8]] = $mat_id;

                                $data[$i][$r]['wh_act_material_id'] = $mat_id;
                            }

                            $data[$i][$r]['entry_qty'] = $row[7];

                            // $unloading = explode('/',trim($row[9]));
                            // $d_unload[0] = substr($unloading[0],0,2);
                            // $d_unload[1] = substr($unloading[0],2,2);
                            // $d_unload[2] = substr($unloading[0],4,2);
                            // $date_unload = date('Y-m-d',strtotime($d_unload[0].'-'.$d_unload[1].'-'.($d_unload[2]+2000)));
                            // $data[$i][$r]['unloading_date'] = $date_unload;

                            // $time_unload = date('H:i:s',strtotime($unloading[1].':00'));
                            // $data[$i][$r]['unloading_time'] = $time_unload;

                            $data[$i][$r]['unloading_date'] = $row[9];
                            $data[$i][$r]['item_text'] = $row[10];
                            $data[$i][$r]['recipient'] = $row[11];
                            $data[$i][$r]['storage_location'] = $row[12];
                            $data[$i][$r]['plant'] = $row[13];
                            $data[$i][$r]['base_qty'] = $row[14];
                            $data[$i][$r]['withdraw_qty'] = $row[15];
                            $data[$i][$r]['remain_qty'] = $row[16];
                            $data[$i][$r]['order'] = $row[18];
                            $data[$i][$r]['order_type'] = $row[19];
                            
                            $date_st = explode('.',$row[20]);
                            $data[$i][$r]['start_date'] = $date_st[2].'-'.$date_st[1].'-'.$date_st[0];
                            $date_ed = explode('.',$row[21]);
                            $data[$i][$r]['finish_date'] = $date_ed[2].'-'.$date_ed[1].'-'.$date_ed[0];
                            
                            $data[$i][$r]['item_text_line1'] = $row[22];
                            $data[$i][$r]['requirement_type'] = $row[23];
                            $data[$i][$r]['batch'] = $row[24];
                            $data[$i][$r]['del'] = $row[25];
                            $data[$i][$r]['fls'] = $row[26];
                            $data[$i][$r]['cost_center'] = $row[27];
                            $data[$i][$r]['receiving_location'] = $row[28];
                        }
                    }
                }
            }
            // asort($requirement_date);
            // dd($data);
            if(count($requirement_date)==1){
                if(count($data)>0){
                    $add_plan = array();
                    $add_plan['filename'] = $uploadname;
                    foreach ($requirement_date as $key => $value) {
                        $add_plan['requirement_date'] = $value;
                    }                    
                    $plan_id = WhPlanAct::create($add_plan)->id;

                    foreach ($file_date as $key => $value) {
                        $add_planM = array();
                        $add_planM['wh_plan_act_id'] = $plan_id;
                        $add_planM['plan_date'] = date('Y-m-d',strtotime($value));
                        $add_planM['plan_time'] = date('H:i:s',strtotime($file_time[$key]));
                        $planM_id = WhPlanActMS::create($add_planM)->id;

                        foreach ($data[$key] as $krow => $vrow) {
                            $add_planD = array();
                            $add_planD['wh_plan_act_m_id'] = $planM_id;
                            $add_planD['mov_allowed'] = $data[$key][$krow]['mov_allowed'];
                            $add_planD['reservation_no'] = $data[$key][$krow]['reservation_no'];
                            $add_planD['reservation_item'] = $data[$key][$krow]['reservation_item'];
                            $add_planD['wh_act_type_id'] = $data[$key][$krow]['wh_act_type_id'];
                            $add_planD['wh_act_material_id'] = $data[$key][$krow]['wh_act_material_id'];
                            $add_planD['entry_qty'] = $data[$key][$krow]['entry_qty'];
                            $add_planD['unloading_date'] = $data[$key][$krow]['unloading_date'];
                            // $add_planD['unloading_time'] = $data[$key][$krow]['unloading_time'];
                            $add_planD['item_text'] = $data[$key][$krow]['item_text'];
                            $add_planD['recipient'] = $data[$key][$krow]['recipient'];
                            $add_planD['storage_location'] = $data[$key][$krow]['storage_location'];
                            $add_planD['plant'] = $data[$key][$krow]['plant'];
                            $add_planD['base_qty'] = $data[$key][$krow]['base_qty'];
                            $add_planD['withdraw_qty'] = $data[$key][$krow]['withdraw_qty'];
                            $add_planD['remain_qty'] = $data[$key][$krow]['remain_qty'];
                            $add_planD['order'] = $data[$key][$krow]['order'];
                            $add_planD['order_type'] = $data[$key][$krow]['order_type'];
                            $add_planD['start_date'] = $data[$key][$krow]['start_date'];
                            $add_planD['finish_date'] = $data[$key][$krow]['finish_date'];
                            $add_planD['item_text_line1'] = $data[$key][$krow]['item_text_line1'];
                            $add_planD['requirement_type'] = $data[$key][$krow]['requirement_type'];
                            $add_planD['batch'] = $data[$key][$krow]['batch'];
                            $add_planD['del'] = $data[$key][$krow]['del'];
                            $add_planD['fls'] = $data[$key][$krow]['fls'];
                            $add_planD['cost_center'] = $data[$key][$krow]['cost_center'];
                            $add_planD['receiving_location'] = $data[$key][$krow]['receiving_location'];
                            // dd($add_planD);
                            WhPlanActD::create($add_planD);
                        }
                    }
                }else{
                    return $uploadname." no data!\n";
                }
            }else{
                return $uploadname." has many date!\n";
            }
            return "0";            
        }else{
            return "Can't read file!\n";
        }
    }
}
