<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
// use App\Models\Material;
// use App\Models\ProductType;

// use App\Models\ProductInput;
use App\Http\Controllers\ProductInputController;

// use SimpleXLSX;
// use Maatwebsite\Excel\Facades\Excel;

// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// // use PhpOffice\PhpSpreadsheet\Worksheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
// use PhpOffice\PhpSpreadsheet\Style\Border;
// use PhpOffice\PhpSpreadsheet\Style\Color;
// use PhpOffice\PhpSpreadsheet\Style\Alignment;
// use Illuminate\Support\Str;
// use Illuminate\Support\Facades\Input;
// use PhpOffice\PhpSpreadsheet\Style\Fill;
// use PhpOffice\PhpSpreadsheet\IOFactory;

// use App\Mail\ReportRM_WH_Mail;
// use Illuminate\Support\Facades\Mail;

class AutoReportRmWH extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'WHForm:AutoReportInput {tbl} {range}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Use to send mail Auto report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $range_date = $this->argument('range');
        $tbl = $this->argument('tbl');

        $ed_date = date ("Y-m-d");
        // $ed_date = date ("Y-m-d", strtotime('2022-08-01'));
        $st_date = date ("Y-m-d", strtotime("-".$range_date." day", strtotime($ed_date)));
        
        $date_to = $ed_date.':'.$st_date;
        // echo $date_to;
        // return $this -> sentmail($date_to);
        $controller = new ProductInputController();
        $controller->sentmail($tbl, $date_to);
    }

}
