<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\WhPlanActD;
use App\Models\WhActivityDetail;
use App\Http\Controllers\WhActsContoller;

class AutoReportWhAct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'WHForm:AutoReportWHAct';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Use to send mail Auto report WHActivity';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $controller = new WhActsContoller();
        $controller->planvsactrpt();
    }
}
