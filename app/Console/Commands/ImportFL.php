<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Forklift;
use App\Models\Flocation;
use App\Models\Fperson;
use App\Models\FLog;

use File;
use SimpleXLSX;
use Maatwebsite\Excel\Facades\Excel;
// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Arr;

class ImportFL extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:importFL';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import File from /storage/app/public/FL/queues to WH Form System';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $directory = config('myconfig.directory.fl.queues');
        $completedirectory = config('myconfig.directory.fl.completes');
        $scanned_directory = array_diff(scandir($directory), array('..', '.'));
        // dd($scanned_directory);
        $files = \File::files($directory);
        // dd($files);

        if(count($files)>0){  
            //ดึงค่าไว้เปรียบเทียบ 
            $query = Forklift::where('status','Active')->get();
            foreach($query as $key){
                $fl_data[$key->name] = $key->id;    
                $date_update[$key->id] = $key->updated_at;   
                $fltime[$key->id] = $key->time;
            }
            // dd(count($fl_data));
            if(empty($fl_data)){
                echo "ImportFL request forklift data.\r\n";
                exit();
            }
            $query = Flocation::where('status','Active')->get();
            foreach($query as $key){
                $locate_data[$key->name] = $key->id;
            }

            $query = Fperson::where('status','Active')->get();
            foreach($query as $key){
                $person_data[$key->fname.' '.$key->lname] = $key->id;
            }

            $query = FLog::get();
            foreach($query as $key){
                $fl_input[$key->log_date][$key->forklift_id][$key->shift] = $key->id;
            }
            $set_active['active'] = 0;
            // dd($fl_data);
            foreach ($files as $fileupload) {  
                $tmpfolder = date('Ymd');
                $uploadname = $fileupload->getFilename();
                $name = md5($fileupload->getFilename() . time()) . '.' . $fileupload->getExtension();
                $destinationPath = public_path($completedirectory . $tmpfolder);
                // $fileupload->move($destinationPath, $name);
                
                $uploadpath = $tmpfolder  . "/" . $name;
                $uploadfile = $destinationPath  . "/" . $name; 
                // $filenewname = md5(time());                
                
                $to_save = array();  
                $chk = array();    
                // $tmpUploadD = array();            
                if ($xlsx = SimpleXLSX::parse($fileupload)) {
                    if(count($xlsx->rows()[4])>7){
                        $chk['col'][][] = 'ข้อมูลในไฟล์ เกินกว่าคอลัมน์ G';
                    }else{                        
                        $tosave = array();
                        foreach ($xlsx->rows() as $r => $row) {
                            if ($r == 3 ){
                                // dd($row[1]);
                                if(!empty($row[1])) {                            
                                    $chk_date = date('Y-m-d',strtotime($row[1]));
                                    $explo_date = explode('-',$chk_date);
                                    if($explo_date[0]>date("Y")+1){
                                        $chk['log_date'][$chk_date][$r] = 'ข้อมูลวันที่ไม่ถูกรูปแบบ';
                                    }else{
                                        //เก็บเลขไมล์ไว้เทียบ
                                        foreach($fl_data as $key=>$value){                       
                                            $fl = FLog::where('forklift_id',$value)->where('log_date','<',$chk_date)->whereNotNull('time')->orderBy('log_date','desc')->orderBy('shift','desc')->limit(1)->get();
                                            if(count($fl)>0 && $fl[0]['updated_at'] > $date_update[$value]){
                                                $fl_time[$key] = number_format($fl[0]['time'],1,'.','');
                                            }else{
                                                $fl_time[$key] = number_format($fltime[$value],1,'.','');
                                            }
                                        }
                                    }
                                }else{
                                    $chk['log_date'][$chk_date][$r] = 'ไม่พบข้อมูลวันที่';
                                }
                            }
                        }   //ปิด loop เพื่่อให้มีการเก็บค้า fl_time ครั้งเดียวไว้เทียบ
                        // dd($fl_time);
                        foreach ($xlsx->rows() as $r => $row) {                           
                            if ($r > 4 && !empty(trim($row[0]))) { 
                                // $to_save = array();        
                                // dd($row[4]);
                                if(empty($fl_data[trim($row[0])])){
                                    $chk['forklift_id'][trim($row[0])][$r] = 'ไม่พบข้อมูลรถ '.$row[0];
                                }
                                if(trim($row[2])<>'B' && trim($row[2])<>'C'){
                                    $chk['shift'][trim($row[2])][$r] = 'กะ '.trim($row[2]).' ไม่ถูกต้อง';
                                }
                                $chk_type = strtoupper(trim($row[3]));  
                                // echo $chk_type.'</br>';                           
                                if($chk_type<>'R' && $chk_type<>'S' && $chk_type<>'A'){
                                    $chk['type'][trim($row[3])][$r] = 'สถานะการใช้งาน '.trim($row[4]).' ไม่ถูกต้อง';
                                }
                                if(!empty($row[6]) && !empty($fl_time[trim($row[0])])){
                                    // $time_chk = $fl_time[trim($row[0])];
                                    if(empty($chk_fl) || $chk_fl<>trim($row[0])){
                                        $chk_fl = trim($row[0]);   
                                        $time_chk = $fl_time[trim($row[0])];
                                    }
                                    if($time_chk > $row[6]){
                                        $chk['time'][trim($row[0])][$r] = 'ชั่วโมงทำงาน '.$row[6].' ไม่ถูกต้อง (เวลาล่าสุด '.$time_chk.')';
                                    }else{
                                        if($row[6]-$time_chk>12){
                                            $chk['time'][trim($row[0]).'('.$time_chk.')'][$r] = $row[6];
                                            if(trim($row[2])=='B'){
                                                $time_chk = $row[6];
                                            }
                                        }
                                    } 
                                    if(trim($row[2])=='B'){
                                        $time_chk = $row[6];
                                    }
                                }                                 
                                if(trim($row[3])=='R' && empty($row[6])){
                                    $chk['chk'][trim($row[0]).' กะ'.trim($row[2]).' สถานะ'.trim($row[3])][$r] = 'สถานะการใช้งาน '.$row[3].' ไม่พบชั่วโมงทำงาน';
                                }

                                $tosave[$r] = $chk_date.'+;'.trim($row[0]).'+;'.trim($row[1]).'+;'.trim($row[2]).'+;'.strtoupper(trim($row[3])).'+;'.trim($row[4]).'+;'.trim($row[5]).'+;'.trim($row[6]);                              
                            }                            
                        }                                            
                    }
                    // dd($chk);
                    if(count($chk)>0){   
                        $destinationPath = config('myconfig.directory.fl.errors');                     
                        // $destinationPath=public_path()."/upload/";
                        if (!is_dir($destinationPath)) {  
                            mkdir($destinationPath,0777,true);  
                        }
                        $strFileName = "Err_".date('Ymd').time().rand().".txt";
                        $objFopen = fopen($destinationPath.'/'.$strFileName, 'w');
                        $strText1 = $uploadname."\r\n";
                        fwrite($objFopen, $strText1);
                        foreach ($chk as $key => $value) {
                            foreach ($value as $key1 => $value1) {
                                foreach ($value1 as $key2 => $value2) {
                                    $strText1 = "บรรทัดที่ ".($key2+1).":".$key." - ".$key1." ".$value2."\r\n";
                                    fwrite($objFopen, $strText1);
                                }
                            }
                        }
                        if($objFopen){
                            echo "Error : ".$uploadname.' ('.$destinationPath.$strFileName.')\r\n';
                        }else{
                            echo "File can not write\r\n";
                        }
                        fclose($objFopen);
                        exit();
                    }else{
                        foreach($tosave as $key=>$value){
                            $to_exp = explode('+;',$value);
                            $to_save = array();
                            $to_save['log_date'] = $to_exp[0];
                            $to_save['forklift_id'] = $fl_data[$to_exp[1]];

                            if(empty($locate_data[$to_exp[2]])){
                                $save_locate['name'] = $to_exp[2];
                                $id_locate = Flocation::create($save_locate)->id;
                                $locate_data[$to_exp[2]] = $id_locate;
                            }
                            $to_save['f_location_id'] = $locate_data[$to_exp[2]];                            
                            
                            $to_save['shift'] = $to_exp[3];
                            $to_save['type'] = $to_exp[4];

                            if(!empty($to_exp[5])){
                                    $re_name = str_replace('   ',' ',str_replace('  ',' ',$to_exp[5]));
                                    if(empty($person_data[$re_name])){                                   
                                    $to_exp_p = explode(' ',$re_name);
                                    // dd($to_exp_p);
                                    $save_p['fname'] = $to_exp_p[0];
                                    if(count($to_exp_p)>2){
                                        for($i=1;$i<count($to_exp_p);$i++)
                                            if($i==1)   $lname = $to_exp_p[$i];
                                            else    $lname .= ' '.$to_exp_p[$i];
                                    }
                                    if(!empty($lname)){
                                        $save_p['lname'] = $lname;
                                    }else{
                                        $save_p['lname'] = '-';
                                    }
                                    // dd($save_p);
                                    $id_p = Fperson::create($save_p)->id;
                                    $person_data[$re_name] = $id_p;
                                }
                                $to_save['fperson_id'] = $person_data[$re_name];
                            }

                            $to_save['note'] = $to_exp[6];
                            if(!empty($to_exp[7])) $to_save['time'] = $to_exp[7];
                            if(!empty($fl_input[$to_exp[0]][$fl_data[$to_exp[1]]][$to_exp[3]])){
                                $to_update = FLog::findOrFail($fl_input[$to_exp[0]][$fl_data[$to_exp[1]]][$to_exp[3]]);
                                $to_update->update($to_save);
                            }else{
                                FLog::create($to_save);
                            }                            
                        }
                        if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                            mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
                        }                

                        if (copy($directory . '/' . $uploadname, $completedirectory . '/' . $tmpfolder . '/' . $name)) {                    
                            unlink($directory . '/' . $uploadname);
                        }

                        echo "Save file : ".$uploadname." is complete\r\n";
                    }
                }    
            }
        }
    }    
}
