<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ProductInputController;

class AutoMailProductInput extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'WHForm:AutoMailProductInput {range}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To send sum mail product input';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $range_date = $this->argument('range');

        $ed_date = date ("Y-m-d");
        // $ed_date = date ("Y-m-d", strtotime('2022-08-30'));
        $st_date = date ("Y-m-d", strtotime("-".$range_date." day", strtotime($ed_date)));
        
        $date_to = $ed_date.':'.$st_date;
        // echo $date_to;
        // return $this -> sentmail($date_to);
        $controller = new ProductInputController();
        $controller->sum_mail($date_to);
    }
}
