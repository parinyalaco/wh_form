<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\StProduct;
use App\Models\StWhBegin;

class StWhBeginController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $st_wh = array();
        $st_pd = array();
        $st_wh = StWhBegin::join('st_products', 'st_wh_begins.st_product_id', '=', 'st_products.id')
            ->where('st_wh_begins.status',1)->where('st_products.status',1)
            ->select('st_wh_begins.id', 'st_products.name', 'st_wh_begins.start_date', 'st_wh_begins.amount')
            ->orderBy('st_products.name')->get();
        $st_pd = StProduct::where('status',1)->orderBy('name')->get();
        // dd($st_pd);
        return view('st_wh.st_stock.index',compact('st_wh', 'st_pd'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $st_pd = array();
        $st_pd = StProduct::where('status',1)->orderBy('name')->get();
        // dd($st_pd);
        return view('st_wh.st_stock.create',compact('st_pd'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $chk_pd = array();
        $chk_pd = StWhBegin::where('status',1)->where('st_product_id',$request->st_product_id)->get();
        if(count($chk_pd)>0){
            $st_status['status'] = 0;
            StWhBegin::where('status',1)->where('st_product_id',$request->st_product_id)->update($st_status);
        }
        $requestData = $request->all();
        StWhBegin::create($requestData);

        return redirect('/st_wh/st_stock')->with('success', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $st_pd = StProduct::where('status',1)->orderBy('name')->get();
        $st_wh = StWhBegin::findOrFail($id);
        return view('st_wh.st_stock.edit', compact('st_wh', 'st_pd'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $st_wh = StWhBegin::findOrFail($id);
        $st_status['status'] = 0;
        $st_wh->update($st_status);

        StWhBegin::create($requestData);

        return redirect('/st_wh/st_stock')->with('success', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $st_wh = StWhBegin::findOrFail($id);
        $st_status['status'] = 0;
        $st_wh->update($st_status);

        return redirect('/st_wh/st_stock')->with('success', ' deleted!');
    }
}
