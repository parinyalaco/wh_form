<?php

namespace App\Http\Controllers;
use SimpleXLS;
use SimpleXLSX;
use Excel;

// use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\WhActMaterial;
use App\Models\Unit;
use App\Models\WhActType;
use App\Models\WhPlanAct;
use App\Models\WhPlanActMS;
use App\Models\WhPlanActD;
use App\Models\WhActivity;
use App\Models\WhActivityDetail;
use App\Models\WhMoveType;
use App\Models\WhActNote;

use App\Imports\WhActImport;

use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;

use App\Mail\ReportWH_Acts;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;

class WhActsContoller extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }

    public function index(Request $request)
    {
        $qty = array();
        $c_row = array();
        $dep_id = '';
        $date_from = '';
        $date_to = '';

        $query_1 = new WhActivityDetail;
        // $query_1 = $query_1->join('wh_act_types', 'wh_activity_details.moving_type', '=', DB::raw('wh_act_types.code collate Latvian_BIN'));
        $query_1 = $query_1->join('wh_act_types', 'wh_activity_details.wh_act_type_id', '=', 'wh_act_types.id');
        $query_1 = $query_1->join('wh_act_materials', 'wh_activity_details.wh_act_material_id', '=', 'wh_act_materials.id');
        $query_1 = $query_1->join('units', 'wh_act_materials.unit_id', '=', 'units.id');
        // $query_1 = $query_1->where(function($q) {
        //                         $q->where('store_loc', 'like', 'PK%')
        //                         ->orWhere('store_loc', 'like', 'PF%');
        //                     });
        if(!empty($request->date_from)){
            $date_from = $request->date_from;
            $date_to = $request->date_to;
            $st_date = $date_from.' 07:30:00';
            $to_date = date('Y-m-d',strtotime($date_to . "+1 days"));
            $ed_date = $to_date.' 07:30:01';
            // $query_1 = $query_1->whereBetween('wh_activity_details.entry_date', [$st_date, $ed_date]);
            $query_1 = $query_1->whereRaw("CONCAT(wh_activity_details.entry_date, ' ', wh_activity_details.entry_time) > '$st_date'");
            $query_1 = $query_1->whereRaw("CONCAT(wh_activity_details.entry_date, ' ', wh_activity_details.entry_time) < '$ed_date'");
        }else{
            //query แบบเรียงตามวันที่
            $query = WhActivityDetail::groupBy('entry_date')->orderByDesc('entry_date')->selectRaw('TOP (5) entry_date')->get();
            if(count($query)>0){
                foreach($query as $key){
                    $to_query[] = $key->entry_date;
                }
                // dd($to_query);
                for($i=4; $i>=0; $i--){
                    if(!empty($to_query[$i])){
                        $date_from = $to_query[$i];
                        break;
                    }
                }
                // $date_from = $to_query[0];
                if($date_from==$to_query[0])    $to_date = $date_from;
                else $to_date = date('Y-m-d',strtotime($to_query[0] . "-1 days"));
                $date_to = $to_date;
                $st_date = $date_from.' 07:30:00';
                $ed_date =$to_query[0].' 07:30:01';

                $query_1 = $query_1->whereRaw("CONCAT(wh_activity_details.entry_date, ' ', wh_activity_details.entry_time) > '$st_date'");
                $query_1 = $query_1->whereRaw("CONCAT(wh_activity_details.entry_date, ' ', wh_activity_details.entry_time) < '$ed_date'");
            }
        }

        if(!empty($request->dep_id)){
            $dep_id = $request->dep_id;
            $query_1 = $query_1->whereRaw('SUBSTRING(wh_activity_details.store_loc,1,2) = ?', [$request->dep_id]);
        }
        // $query_1 = $query_1->selectRaw("wh_activity_details.id, wh_activity_details.mat, wh_activity_details.mat_desc, wh_act_types.name,
        // CASE
        //     WHEN CONVERT(time,wh_activity_details.entry_time) BETWEEN '00:00:00' AND '07:29:59' THEN CONVERT(DATE, DATEADD(day,-1,wh_activity_details.entry_date))
        //     ELSE CONVERT(DATE, wh_activity_details.entry_date) END AS entry_date,
        // CASE WHEN CONVERT(time,wh_activity_details.entry_time) BETWEEN '07:30:00' AND '18:00:00' THEN 'B' ELSE 'C' END AS time_shift,
        // SUBSTRING(wh_activity_details.store_loc,1,2) as store_loc, wh_activity_details.qty_entry,
        //     wh_activity_details.unit_entry");
        $query_1 = $query_1->selectRaw("wh_act_materials.name AS mat, wh_act_types.name, units.name AS unit_entry,
        CASE
            WHEN CONVERT(time,wh_activity_details.entry_time) BETWEEN '00:00:00' AND '07:29:59' THEN CONVERT(DATE, DATEADD(day,-1,wh_activity_details.entry_date))
            ELSE CONVERT(DATE, wh_activity_details.entry_date) END AS entry_date,
        CASE WHEN CONVERT(time,wh_activity_details.entry_time) BETWEEN '07:30:00' AND '18:00:00' THEN 'B' ELSE 'C' END AS time_shift,
        SUBSTRING(wh_activity_details.store_loc,1,2) as store_loc, wh_activity_details.qty_entry");
        $query_1 = $query_1->orderBy('wh_activity_details.entry_date','DESC');
        $query_1 = $query_1->orderBy('wh_activity_details.store_loc','DESC');
        $query_1 = $query_1->orderBy('wh_act_materials.name');
        $query_1 = $query_1->get();
        // dd($query_1);
        foreach ($query_1 as $key) {
            // if(!empty($qty[$key->entry_date][$key->store_loc][$key->mat][$key->unit_entry][$key->name])){
            //     $qty[$key->entry_date][$key->store_loc][$key->mat][$key->unit_entry][$key->name] += $key->qty_entry;
            // }else{
            //     $qty[$key->entry_date][$key->store_loc][$key->mat][$key->unit_entry][$key->name] = $key->qty_entry;
            // }
            // if(!empty($c_row[$key->entry_date][$key->store_loc][$key->mat][$key->unit_entry][$key->name])){
            //     $c_row[$key->entry_date][$key->store_loc][$key->mat][$key->unit_entry][$key->name] += 1;
            // }else{
            //     $c_row[$key->entry_date][$key->store_loc][$key->mat][$key->unit_entry][$key->name] = 1;
            // }
            if($key->store_loc=='PK' || $key->store_loc=='PF'){
                if(!empty($qty[$key->entry_date][$key->store_loc][$key->mat][$key->unit_entry][$key->name])){
                    $qty[$key->entry_date][$key->store_loc][$key->mat][$key->unit_entry][$key->name] += $key->qty_entry;
                }else{
                    $qty[$key->entry_date][$key->store_loc][$key->mat][$key->unit_entry][$key->name] = $key->qty_entry;
                }
            }
            if(!empty($c_row[$key->entry_date][$key->store_loc][$key->mat][$key->unit_entry][$key->name])){
                $c_row[$key->entry_date][$key->store_loc][$key->mat][$key->unit_entry][$key->name] += 1;
            }else{
                $c_row[$key->entry_date][$key->store_loc][$key->mat][$key->unit_entry][$key->name] = 1;
            }
        }
        // dd($qty);
        return view('wh_act.index',compact('qty','c_row','dep_id','date_from','date_to'));
    }

    public function planvsactrpt(){
        $sent_date = date("Y-m-d", strtotime("-1 day"));   //ลบหนึ่งวันจากวันปัจจุบัน
        // $sent_date = '2022-04-20';
        $ed_date = date ("Y-m-d", strtotime("1 day",strtotime($sent_date)));

        $show_date = date("d", strtotime($sent_date)).'/'.date("m", strtotime($sent_date)).'/'.(date("Y", strtotime($sent_date))+543);

        $to_show = array();
        $to_show1 = array();
        $to_move = array();

        $test = $this->mail_1($sent_date,$ed_date);
        if(!empty($test[0]))    $to_show = $test[0];
        if(!empty($test[1]))    $to_show1 = $test[1];
        if(!empty($test[2]))    $to_move = $test[2];
        // $test1 = $this->mail_2($sent_date,$ed_date);
        // if(!empty($test1[0]))    $to_rec = $test1[0];

        $test2 = $this->mail_file($show_date,$sent_date,$to_show,$to_show1,$to_move);
        if(!empty($test2[0])){
            $spreadsheet = $test2[0];
            $writer = new Xlsx($spreadsheet);
            $filename = "exportdata-". $sent_date."-".date('ymd-his').".xlsx";
            $writer->save('storage/app/public/'.$filename);
        }

        if(count($to_show)>0){
            Mail::send(new ReportWH_Acts($show_date,$to_show,$filename));
            echo "Sent mail is complete!";
        }else{
            echo "No data!";
        }

        // $data = array();
        // $plandatarw = DB::table('wh_plan_act_d_s')
        // ->select(DB::raw('store_loc,status,mat,mat_desc,sum(qty_entry) as sumval ,unit_entry'))
        // ->groupBy(DB::raw('store_loc,mat,mat_desc,unit_entry,status'))
        // ->where('posting_date', $date)
        // ->get();
        // foreach ($plandatarw as $plandataObj) {
        //     $data[$plandataObj->status][$plandataObj->store_loc][$plandataObj->mat]['plan'] = $plandataObj;
        // }


        // $actdatarw = DB::table('wh_activity_details')
        // ->select(DB::raw('store_loc,status,mat,mat_desc,sum(qty_entry) as sumval ,unit_entry'))
        // ->groupBy(DB::raw('store_loc,mat,mat_desc,unit_entry,status'))
        // ->where('posting_date', $date)
        // ->get();

        // foreach ($actdatarw as $actdataObj) {
        //     $data[$actdataObj->status][$actdataObj->store_loc][$actdataObj->mat]['act'] = $actdataObj;
        // }

        // $spreadsheet = new Spreadsheet();
        // $sheet = $spreadsheet->getActiveSheet();

        // $sheet->setCellValueByColumnAndRow(1, 1, "รายงานรับ-จ่ายสินค้าประจำวันที่ " . $date);

        // $startrow = 3;

        // if (isset($data['จ่าย'])) {
        //     $sheet->setCellValueByColumnAndRow(1, $startrow, "งานจ่าย");
        //     $startrow++;
        //     $sheet->setCellValueByColumnAndRow(1, $startrow, "Store");
        //     $sheet->setCellValueByColumnAndRow(2, $startrow, "Mat");
        //     $sheet->setCellValueByColumnAndRow(3, $startrow, "Mat Desc");
        //     $sheet->setCellValueByColumnAndRow(4, $startrow, "จำนวนแผน");
        //     $sheet->setCellValueByColumnAndRow(5, $startrow, "จำนวนจริง");
        //     $sheet->setCellValueByColumnAndRow(6, $startrow, "หน่วย");
        //     $sheet->setCellValueByColumnAndRow(7, $startrow, "Diff");
        //     $startrow++;
        //     $start1 = $startrow;
        //     foreach ($data['จ่าย'] as $store => $paidObj1) {
        //         foreach ($paidObj1 as $codemat => $paidObj2) {
        //             $sheet->setCellValueByColumnAndRow(1, $startrow, $store);
        //             $sheet->setCellValueByColumnAndRow(2, $startrow, $codemat);
        //             if(isset($paidObj2['plan'])){
        //                 $sheet->setCellValueByColumnAndRow(3, $startrow, $paidObj2['plan']->mat_desc);
        //                 $sheet->setCellValueByColumnAndRow(4, $startrow, $paidObj2['plan']->sumval);
        //                 $sheet->setCellValueByColumnAndRow(6, $startrow, $paidObj2['plan']->unit_entry);
        //             }
        //             if (isset($paidObj2['act'])) {
        //                 $sheet->setCellValueByColumnAndRow(3, $startrow, $paidObj2['act']->mat_desc);
        //                 $sheet->setCellValueByColumnAndRow(5, $startrow, $paidObj2['act']->sumval);
        //                 $sheet->setCellValueByColumnAndRow(6, $startrow, $paidObj2['act']->unit_entry);
        //             }
        //             $sheet->setCellValueByColumnAndRow(7, $startrow,'=D'. $startrow.'-E'. $startrow);
        //             $startrow++;
        //         }
        //     }
        //     $sheet->setCellValueByColumnAndRow(1, $startrow, "Total");
        //     $sheet->setCellValueByColumnAndRow(2, $startrow, "");
        //     $sheet->setCellValueByColumnAndRow(3, $startrow, "");
        //     $sheet->setCellValueByColumnAndRow(4, $startrow, "=Sum(D" . $start1 . ":D" . ($startrow - 1) . ")");
        //     $sheet->setCellValueByColumnAndRow(5, $startrow, "=Sum(E" . $start1 . ":E" . ($startrow - 1) . ")");
        //     $sheet->setCellValueByColumnAndRow(6, $startrow, "");
        //     $sheet->setCellValueByColumnAndRow(7, $startrow, "=Sum(G" . $start1 . ":G" . ($startrow - 1) . ")");
        // }
        // $startrow++;
        // $startrow++;
        // if (isset($data['รับ'])) {
        //     $sheet->setCellValueByColumnAndRow(1, $startrow, "งานรับ");
        //     $startrow++;
        //     $sheet->setCellValueByColumnAndRow(1, $startrow, "Store");
        //     $sheet->setCellValueByColumnAndRow(2, $startrow, "Mat");
        //     $sheet->setCellValueByColumnAndRow(3, $startrow, "Mat Desc");
        //     $sheet->setCellValueByColumnAndRow(4, $startrow, "จำนวนแผน");
        //     $sheet->setCellValueByColumnAndRow(5, $startrow, "จำนวนจริง");
        //     $sheet->setCellValueByColumnAndRow(6, $startrow, "หน่วย");
        //     $sheet->setCellValueByColumnAndRow(7, $startrow, "Diff");
        //     $startrow++;
        //     $start1 = $startrow;
        //     foreach ($data['รับ'] as $store => $paidObj1) {
        //         foreach ($paidObj1 as $codemat => $paidObj2) {
        //             $sheet->setCellValueByColumnAndRow(1, $startrow, $store);
        //             $sheet->setCellValueByColumnAndRow(2, $startrow, $codemat);
        //             if (isset($paidObj2['plan'])) {
        //                 $sheet->setCellValueByColumnAndRow(3, $startrow, $paidObj2['plan']->mat_desc);
        //                 $sheet->setCellValueByColumnAndRow(4, $startrow, $paidObj2['plan']->sumval);
        //                 $sheet->setCellValueByColumnAndRow(6, $startrow, $paidObj2['plan']->unit_entry);
        //             }
        //             if (isset($paidObj2['act'])) {
        //                 $sheet->setCellValueByColumnAndRow(3, $startrow, $paidObj2['act']->mat_desc);
        //                 $sheet->setCellValueByColumnAndRow(5, $startrow, $paidObj2['act']->sumval);
        //                 $sheet->setCellValueByColumnAndRow(6, $startrow, $paidObj2['act']->unit_entry);
        //             }
        //             $sheet->setCellValueByColumnAndRow(7, $startrow, '=D' . $startrow . '-E' . $startrow);
        //             $startrow++;
        //         }
        //     }
        //     $sheet->setCellValueByColumnAndRow(1, $startrow, "Total");
        //     $sheet->setCellValueByColumnAndRow(2, $startrow, "");
        //     $sheet->setCellValueByColumnAndRow(3, $startrow, "");
        //     $sheet->setCellValueByColumnAndRow(4, $startrow, "=Sum(D". $start1.":D". ($startrow-1).")");
        //     $sheet->setCellValueByColumnAndRow(5, $startrow, "=Sum(E". $start1.":E". ($startrow-1) . ")");
        //     $sheet->setCellValueByColumnAndRow(6, $startrow, "");
        //     $sheet->setCellValueByColumnAndRow(7, $startrow, "=Sum(G" . $start1 . ":G" . ($startrow - 1) . ")");
        //     $startrow++;
        // }

        // $writer = new Xlsx($spreadsheet);

        // $filename = "exportdata-". $date."-" . date('yymmdd-hi') . ".xlsx";

        // $writer->save('storage/' . $filename);

        // return response()->download('storage/' . $filename);

    }

    public function importExportView()
    {
        return view('wh_act.import');
    }

    public function import_plan(Request $request)
    {
        $validatedData = $request->validate([
            'file_plan'  => 'required|mimes:xls,xlsx'
        ]);

        // $completedirectory = 'storage/app/public/WhAct/completes/plan/';
        $completedirectory = config('myconfig.directory.whactivity.completes.plan');
        // dd($completedirectory);
        $tmpfolder = date('Ymd');
        if (!is_dir($completedirectory . '/' . $tmpfolder)) {
            mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
        }

        $zipfile = $request->file('file_plan');
        $uploadname = $zipfile->getClientOriginalName();
        $name = md5($zipfile->getClientOriginalName() . time()) . '-upload.' . $zipfile->getClientOriginalExtension();
        // $destinationPath = public_path($completedirectory . $tmpfolder);
        $destinationPath = $completedirectory . "/" . $tmpfolder;
        $zipfile->move($destinationPath, $name);

        $uploadpath = $completedirectory . "/" . $tmpfolder  . "/" . $name;
        $uploadfile = $destinationPath  . "/" . $name;

        $data = array();
        $er = array();
        $import = new WhActImport;
        Excel::import($import, $uploadfile);


        $er = $import->err;
        $data = $import->sent_data;
        // dd($import);

        if(count($er)>0){
            return view('wh_act.not_match',compact('er'));
        }else{
            if(count($data)>0){
                foreach ($data as $krow => $vrow) {
                    $add_planD = array();
                    // $add_planD['wh_plan_act_m_id'] = $planM_id;
                    $add_planD['mov_allowed'] = $data[$krow]['mov_allowed'];
                    $add_planD['reservation_no'] = $data[$krow]['reservation_no'];
                    $add_planD['reservation_item'] = $data[$krow]['reservation_item'];
                    $add_planD['requirement_date'] = $data[$krow]['requirement_date'];
                    $add_planD['wh_act_type_id'] = $data[$krow]['wh_act_type_id'];
                    $add_planD['wh_act_material_id'] = $data[$krow]['wh_act_material_id'];
                    $add_planD['entry_qty'] = $data[$krow]['entry_qty'];
                    $add_planD['unloading_date'] = $data[$krow]['unloading_date'];
                    // $add_planD['unloading_time'] = $data[$krow]['unloading_time'];
                    $add_planD['item_text'] = $data[$krow]['item_text'];
                    $add_planD['recipient'] = $data[$krow]['recipient'];
                    $add_planD['storage_location'] = $data[$krow]['storage_location'];
                    $add_planD['plant'] = $data[$krow]['plant'];
                    $add_planD['base_qty'] = $data[$krow]['base_qty'];
                    $add_planD['withdraw_qty'] = $data[$krow]['withdraw_qty'];
                    $add_planD['remain_qty'] = $data[$krow]['remain_qty'];
                    $add_planD['order'] = $data[$krow]['order'];
                    $add_planD['order_type'] = $data[$krow]['order_type'];
                    $add_planD['start_date'] = $data[$krow]['start_date'];
                    $add_planD['finish_date'] = $data[$krow]['finish_date'];
                    $add_planD['item_text_line1'] = $data[$krow]['item_text_line1'];
                    $add_planD['requirement_type'] = $data[$krow]['requirement_type'];
                    $add_planD['batch'] = $data[$krow]['batch'];
                    $add_planD['del'] = $data[$krow]['del'];
                    $add_planD['fls'] = $data[$krow]['fls'];
                    $add_planD['cost_center'] = $data[$krow]['cost_center'];
                    $add_planD['receiving_location'] = $data[$krow]['receiving_location'];
                    // dd($add_planD);
                    WhPlanActD::create($add_planD);
                }
            }else{
                return back()->with('error',$uploadname.' no data!');
            }
            return back()->with('success','Upload Plan successfully');
        }
    }


    public function import_act(Request $request)
    {
        $requestData = $request->all();
        // $completedirectory = 'storage/app/public/WhAct/completes/act/';
        $completedirectory = config('myconfig.directory.whactivity.completes.act');

        if ($request->hasFile('file_act')) {
            // $tmpfolder = md5(time());
            $tmpfolder = date('Ymd');
            if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
            }

            $zipfile = $request->file('file_act');
            $uploadname = $zipfile->getClientOriginalName();
            $name = md5($zipfile->getClientOriginalName() . time()) . '-upload.' . $zipfile->getClientOriginalExtension();
            // $destinationPath = public_path($completedirectory . $tmpfolder);
            $destinationPath = $completedirectory . "/" . $tmpfolder;
            $zipfile->move($destinationPath, $name);

            $uploadpath = $completedirectory . "/" . $tmpfolder  . "/" . $name;
            $uploadfile = $destinationPath  . "/" . $name;

            // dd($uploadname);
            // $to_save['name'] = $uploadname;
            // $to_save['file_tosave'] = $uploadpath;       //ข้างหน้าเป็น 'storage/app/public/banks/completes'


            if ($xlsx = SimpleXLSX::parse($uploadfile)) {
                $query = WhActMaterial::join('units', 'wh_act_materials.unit_id', '=', 'units.id')
                    ->select('wh_act_materials.id', 'wh_act_materials.name', 'wh_act_materials.mat_desc', 'wh_act_materials.unit_id', 'units.name AS unit_name')
                    ->get();
                foreach ($query as $key) {
                    $mat[$key->name][$key->mat_desc][$key->unit_name] = $key->id;
                }

                $query = Unit::all();
                foreach ($query as $key) {
                    $unit[$key->name] = $key->id;
                }

                $query = WhActType::all();
                foreach ($query as $key) {
                    $act_type[$key->code] = $key->id;
                }

                $query = WhMoveType::all();
                foreach ($query as $key) {
                    $move_type[$key->name] = $key->id;
                }
                $er = array();
                $data_d = array();
                foreach ($xlsx->rows(0) as $r => $row) {
                    if($r>0){
                        if(!empty($row[0])){
                            if(!empty($mat[$row[1]][$row[0]][$row[12]])){
                                $data_d[$r]['wh_act_material_id'] = $mat[$row[1]][$row[0]][$row[12]];
                            }else{
                                if(empty($unit[$row[12]])){
                                    $add_unit['name'] = $row[12];
                                    $unit_id = Unit::create($add_unit)->id;
                                    $unit[$row[12]] = $unit_id;
                                }else{
                                    $unit_id = $unit[$row[12]];
                                }
                                $add_mat['name'] = $row[1];
                                $add_mat['mat_desc'] = $row[0];
                                $add_mat['unit_id'] = $unit_id;
                                $mat_id = WhActMaterial::create($add_mat)->id;
                                $mat[$row[1]][$row[0]][$row[12]] = $mat_id;

                                $data_d[$r]['wh_act_material_id'] = $mat_id;
                            }

                            if(empty($move_type[$row[2]])){
                                $add_move['name'] = $row[2];
                                $move_id = WhMoveType::create($add_move)->id;
                                $move_type[$row[2]] = $move_id;
                            }else{
                                $move_id = $move_type[$row[2]];
                            }
                            $data_d[$r]['wh_move_type_id'] = $move_id;

                            $data_d[$r]['posting_date'] = date('Y-m-d',strtotime($row[3]));

                            $data_d[$r]['batch'] = $row[4];
                            $data_d[$r]['entry_date'] = date('Y-m-d H:i:s',strtotime($row[5]));
                            $data_d[$r]['entry_time'] = date('H:i:s', strtotime($row[6]));
                            $data_d[$r]['mat_doc'] = $row[7];

                            if(empty($act_type[$row[8]])){
                                // return "unknow Movement Type on row number ".($r+1)."->".$row[8]."\n";
                                $er[$row[8]][$r] = $r;
                            }else{
                                $data_d[$r]['wh_act_type_id'] = $act_type[$row[8]];
                            }

                            $data_d[$r]['store_loc'] = $row[9];
                            $data_d[$r]['mat_doc_item'] = $row[10];
                            $data_d[$r]['qty_entry'] = $row[11];
                            $data_d[$r]['amount_lc'] = $row[13];
                            $data_d[$r]['ref'] = $row[14];
                            $data_d[$r]['order'] = $row[15];
                            $data_d[$r]['username'] = $row[16];
                        }
                    }
                }
                if(count($er)>0){
                    return view('wh_act.not_match',compact('er'));
                }else{
                    if(count($data_d)>0){
                        // $importedrow = 0;
                        // $whActivitytmp = array();
                        // $whActivitytmp['filename'] = $uploadname;
                        // $whActivitytmp['import_num'] = $importedrow;
                        // $whActivitytmp['status'] = 'Active';
                        // $importMain = WhActivity::create($whActivitytmp);

                        foreach ($data_d as $r => $row) {
                            $tmpdetail = array();
                            // $tmpdetail['wh_activity_id']=$importMain->id;
                            $tmpdetail['row']= $r;
                            $tmpdetail['wh_act_material_id'] = $data_d[$r]['wh_act_material_id'];
                            $tmpdetail['wh_move_type_id'] = $data_d[$r]['wh_move_type_id'];
                            $tmpdetail['posting_date'] = $data_d[$r]['posting_date'];
                            $tmpdetail['batch'] = $data_d[$r]['batch'];
                            $tmpdetail['entry_date'] = $data_d[$r]['entry_date'];
                            $tmpdetail['entry_time'] = $data_d[$r]['entry_time'];
                            $tmpdetail['mat_doc'] = $data_d[$r]['mat_doc'];
                            $tmpdetail['wh_act_type_id'] = $data_d[$r]['wh_act_type_id'];
                            $tmpdetail['store_loc'] = $data_d[$r]['store_loc'];
                            $tmpdetail['mat_doc_item'] = $data_d[$r]['mat_doc_item'];
                            $tmpdetail['qty_entry'] = $data_d[$r]['qty_entry'];
                            $tmpdetail['amount_lc'] = $data_d[$r]['amount_lc'];
                            $tmpdetail['ref'] = $data_d[$r]['ref'];
                            $tmpdetail['order'] = $data_d[$r]['order'];
                            $tmpdetail['username'] = $data_d[$r]['username'];
                            WhActivityDetail::create($tmpdetail);
                        }
                        return back()->with('success','Upload Act successfully');
                    }else{
                        return back()->with('error',"No data!");
                    }
                }
            }else{
                return back()->with('error',"Can't read file!");
            }
        }else{
            return back();
        }
    }

    public function reportView()
    {
        return view('wh_act.report');
    }

    public function report_export(Request $request)
    {
        $report_id = $request->report_id;
        $sent_date = date ("Y-m-d", strtotime($request->st_date));
        $ed_date = date ("Y-m-d", strtotime("1 day",strtotime($sent_date)));
        $show_date = date("d", strtotime($sent_date)).'/'.date("m", strtotime($sent_date)).'/'.(date("Y", strtotime($sent_date))+543);

        $tb_1 = array();
        $tb_2 = array();
        $test = $this->mail_1($sent_date,$ed_date);
        if(!empty($test[0]))    $tb_1 = $test[0];
        // dd($tb_1);
        // if($report_id==1){
        //     return view('wh_act.report_1',compact('show_date','tb_1'));
        // }elseif($report_id==2){
        //     return view('wh_act.report_2',compact('show_date','tb_1'));
        // }elseif($report_id==3){
            return view('wh_act.report_3',compact('show_date','tb_1','report_id'));
        // }
    }

    public function mailView()
    {
        $sent_date = date("Y-m-d", strtotime("-1 day"));   //ลบหนึ่งวันจากวันปัจจุบัน
        // $sent_date = '2022-08-22';
        $ed_date = date ("Y-m-d", strtotime("1 day",strtotime($sent_date)));

        $show_date = date("d", strtotime($sent_date)).'/'.date("m", strtotime($sent_date)).'/'.(date("Y", strtotime($sent_date))+543);

        $tb_1 = array();
        $graph_hv = array();

        $test = $this->mail_1($sent_date,$ed_date);
        if(!empty($test[0]))    $tb_1 = $test[0];
        if(!empty($test[3]))    $graph_hv = $test[3];

        // dd($tb_1);
        // $test1 = $this->mail_2($sent_date,$ed_date);
        // if(!empty($test1[0]))   $tb_2 = $test1[0];


        return view('wh_act.mail',compact('show_date','tb_1','graph_hv'));
    }

    public function mail_1($sent_date,$ed_date){
        $query = WhActMaterial::join('units', 'wh_act_materials.unit_id', '=', 'units.id')
            ->select('wh_act_materials.id', 'wh_act_materials.name', 'wh_act_materials.mat_desc', 'wh_act_materials.unit_id', 'units.name AS unit_name')
            ->get();
        foreach ($query as $key) {
            $mat[$key->id]['name'] = $key->name;
            $mat[$key->id]['mat_desc'] = $key->mat_desc;
            $mat[$key->id]['unit_name'] = $key->unit_name;

            $unit_name = strtoupper(trim($key->unit_name));
            $chk_unit = strpos($unit_name,"CAR");
            if(strpos($unit_name,"CAR") === False){ //'==='เทียบ boolean, '=='เทียบค่าทั่วไป
                $mat[$key->id]['cal'] = 1;
            }else{
                $cut_1 = substr($key->name,strlen(str_replace(range(0,9),'',$key->name)));
                if(strlen(str_replace(range(0,9),'',$cut_1))>0){
                    $cut_2 = substr($cut_1,strlen(str_replace(range(0,9),'',$cut_1)));
                }else{
                    $cut_2 = $cut_1;
                }
                $mat[$key->id]['cal'] = (int)$cut_2/1000;
            }
        }

        $to_show = array();
        $to_excel1 = array();
        $status_move = array();
        $to_real = array();
        $to_real1 = array();
        $exp_time = array();
        $plan = WhPlanActD::selectRaw('wh_act_material_id, entry_qty, unloading_date, SUBSTRING(receiving_location, 1, 2) AS store_loc')
            ->whereRaw('CONVERT(DATE, requirement_date) =  ?', [$sent_date])->get();
        // dd($plan);
        foreach($plan as $key){
            $exp = array();
            $exp = explode('/', $key->unloading_date);
            foreach($exp as $kex=>$vx){
                $exp_time[$key->store_loc][$mat[$key->wh_act_material_id]['name']][$vx] = $vx;
            }
            // if(empty($to_show['จ่าย'][$key->store_loc][$mat[$key->wh_act_material_id]['name']]['time']['plan'])){
            //     $to_show['จ่าย'][$key->store_loc][$mat[$key->wh_act_material_id]['name']]['time']['plan'] = $key->unloading_date;
            // }
            if(empty($to_show['จ่าย'][$key->store_loc][$mat[$key->wh_act_material_id]['name']]['qty']['plan'])){
                $to_show['จ่าย'][$key->store_loc][$mat[$key->wh_act_material_id]['name']]['qty']['plan'] = (float)$key->entry_qty * (float)$mat[$key->wh_act_material_id]['cal'];
            }else{
                $to_show['จ่าย'][$key->store_loc][$mat[$key->wh_act_material_id]['name']]['qty']['plan'] += (float)$key->entry_qty * (float)$mat[$key->wh_act_material_id]['cal'];
            }
        }
        foreach ($exp_time as $kloc => $vloc) {
            foreach ($vloc as $kmat => $vmat) {
                foreach ($vmat as $karr => $varr) {
                    if(!empty($karr)){
                        if(empty($to_show['จ่าย'][$kloc][$kmat]['time']['plan'])){
                            $to_show['จ่าย'][$kloc][$kmat]['time']['plan'] = $karr.'/';
                        }else{
                            $to_show['จ่าย'][$kloc][$kmat]['time']['plan'] .= $karr.'/';
                        }
                    }
                }
            }
        }
        // dd($to_show);
        //query วันเดียวมาแยกกะไม่ได้ เพราะมี min_time
        $to_actual = WhActivityDetail::join('wh_act_types', 'wh_activity_details.wh_act_type_id', '=', 'wh_act_types.id')
            ->selectRaw("SUBSTRING(wh_activity_details.store_loc, 1, 2) AS store_loc, wh_activity_details.store_loc AS st_loc, wh_activity_details.wh_act_material_id,  wh_act_types.name,
                wh_activity_details.qty_entry, wh_activity_details.entry_time,
                CASE
                    WHEN CONVERT(time,wh_activity_details.entry_time) BETWEEN '00:00:00' AND '07:29:59' THEN CONVERT(DATE, DATEADD(day,-1,wh_activity_details.entry_date))
                    ELSE CONVERT(DATE, wh_activity_details.entry_date) END AS entry_date,
                CASE WHEN CONVERT(time,wh_activity_details.entry_time) BETWEEN '07:30:01' AND '19:30:00' THEN 'actual_b' ELSE 'actual_c' END AS time_shift")
            ->whereBetween('wh_activity_details.entry_date', [$sent_date, $ed_date])
            ->orderBy('wh_activity_details.entry_date')->orderBy('wh_activity_details.entry_time')->orderBy('wh_activity_details.store_loc')->get();
        // dd($to_actual);
        foreach($to_actual as $key){
            if(($key->entry_date >= $sent_date) && ($key->entry_date <= $ed_date)){
                // if($mat[$key->wh_act_material_id]['name'] == 'IGSBCLSJ')    $chk_chk[$key->name][$key->store_loc][$key->entry_time][] = $key->qty_entry;
                if(empty($to_show[$key->name][$key->store_loc][$mat[$key->wh_act_material_id]['name']]['time'][$key->time_shift])){
                    $to_show[$key->name][$key->store_loc][$mat[$key->wh_act_material_id]['name']]['time'][$key->time_shift] = date("H:i:s", strtotime($key->entry_time));
                }
                $qty_mat = abs((float)$key->qty_entry * (float)$mat[$key->wh_act_material_id]['cal']);
                if(empty($to_show[$key->name][$key->store_loc][$mat[$key->wh_act_material_id]['name']]['qty'][$key->time_shift])){
                    $to_show[$key->name][$key->store_loc][$mat[$key->wh_act_material_id]['name']]['qty'][$key->time_shift] = $qty_mat;
                }else{
                    $to_show[$key->name][$key->store_loc][$mat[$key->wh_act_material_id]['name']]['qty'][$key->time_shift] += $qty_mat;
                }
                if(empty($to_show[$key->name][$key->store_loc][$mat[$key->wh_act_material_id]['name']]['pl'][$key->time_shift])){
                    $to_show[$key->name][$key->store_loc][$mat[$key->wh_act_material_id]['name']]['pl'][$key->time_shift] = 1;
                }else{
                    $to_show[$key->name][$key->store_loc][$mat[$key->wh_act_material_id]['name']]['pl'][$key->time_shift] += 1;
                }

                if(empty($to_excel1[$key->store_loc][$mat[$key->wh_act_material_id]['name']][$key->st_loc][$key->name]['Q'])){
                    $to_excel1[$key->store_loc][$mat[$key->wh_act_material_id]['name']][$key->st_loc][$key->name]['Q'] = (float)$key->qty_entry;
                }else{
                    $to_excel1[$key->store_loc][$mat[$key->wh_act_material_id]['name']][$key->st_loc][$key->name]['Q'] += (float)$key->qty_entry;
                }
                if(empty($to_excel1[$key->store_loc][$mat[$key->wh_act_material_id]['name']][$key->st_loc][$key->name]['P'])){
                    $to_excel1[$key->store_loc][$mat[$key->wh_act_material_id]['name']][$key->st_loc][$key->name]['P'] = 1;
                }else{
                    $to_excel1[$key->store_loc][$mat[$key->wh_act_material_id]['name']][$key->st_loc][$key->name]['P'] += 1;
                }
                $status_move[$key->store_loc][$key->name] = $key->name;
                $status_move['all'][$key->name] = $key->name;
                if(empty($to_excel1['all'][$mat[$key->wh_act_material_id]['name']][$key->name]['Q'])){
                    $to_excel1['all'][$mat[$key->wh_act_material_id]['name']][$key->name]['Q'] = (float)$key->qty_entry;
                }else{
                    $to_excel1['all'][$mat[$key->wh_act_material_id]['name']][$key->name]['Q'] += (float)$key->qty_entry;
                }
                if(empty($to_excel1['all'][$mat[$key->wh_act_material_id]['name']][$key->name]['P'])){
                    $to_excel1['all'][$mat[$key->wh_act_material_id]['name']][$key->name]['P'] = 1;
                }else{
                    $to_excel1['all'][$mat[$key->wh_act_material_id]['name']][$key->name]['P'] += 1;
                }
            }
        }
        //เรียงลำดับ to_show->to_real
        $sort_act = ['เคลื่อนย้าย', 'จ่าย', 'รับ'];
        if(count($to_show)>0){
            $pk_pf = ['PK', 'PF'];
            $sort_pd = array();
            foreach ($sort_act as $kact => $vact) {
                foreach ($pk_pf as $kp => $vp) {
                    if(!empty($to_show[$vact][$vp])){
                        foreach ($to_show[$vact][$vp] as $key => $value) {
                            $sort_pd[$vact][$vp][] = $key;
                        }
                        sort($sort_pd[$vact][$vp]);
                    }
                }
            }
            foreach ($sort_pd as $kact => $vact) {
                foreach ($vact as $kp => $vp) {
                    foreach ($vp as $kmat => $vmat) {
                        $to_real[$kact][$kp][$vmat] = $to_show[$kact][$kp][$vmat];
                    }
                }
            }
        }

        //note
        $to_note = WhActNote::where('entry_date', $sent_date)->get();
        foreach ($to_note as $knote) {
            $to_real['จ่าย'][$knote->store_loc][$mat[$knote->wh_act_material_id]['name']]['note'] = $knote->note;
        }


        //เรียงลำดับ to_excel1->to_real1
        // dd($to_show);
        if(count($to_excel1)>0){
            $sort_pd = array();
            $sort_act1 = array();
            $sort_dep = array();
            foreach ($to_excel1 as $kp => $vp) {
                foreach ($vp as $kpd => $vpd) {
                    $sort_pd[$kp][] = $kpd;
                    if($kp == 'PK' || $kp == 'PF'){
                        foreach ($vpd as $kdep => $vdep) {
                            if(!empty($to_excel1[$kp][$kpd])){
                                $sort_dep[$kp][$kpd][] = $kdep;
                                foreach ($sort_act as $kact => $vact) {
                                    if(!empty($to_excel1[$kp][$kpd][$kdep][$vact])){
                                        $sort_act1[$kp][$kpd][$kdep][$vact] = $to_excel1[$kp][$kpd][$kdep][$vact];
                                    }
                                }
                            }
                        }
                        sort($sort_dep[$kp][$kpd]);
                    }
                }
                sort($sort_pd[$kp]);
            }
            // dd($sort_act1);
            foreach ($sort_pd as $kp => $vp) {
                foreach ($vp as $kpd => $vpd) {
                    if($kp == 'PK' || $kp == 'PF'){
                        foreach ($sort_dep[$kp][$vpd] as $kdep => $vdep) {
                            $to_real1[$kp][$vpd][$vdep] = $sort_act1[$kp][$vpd][$vdep];
                        }
                    }else{
                        // echo $kp.'-'.$kp.'-'.'</br>';
                        foreach ($sort_act as $kact => $vact) {
                            // dd($to_excel1[$kp][$vpd]);
                            if(!empty($to_excel1[$kp][$vpd][$vact])){
                                $to_real1[$kp][$vpd][$vact] = $to_excel1[$kp][$vpd][$vact];
                            }
                        }
                    }
                }
            }
            // dd($to_real1);
        }
        // dd($to_real);

        //เก็บสินค้า ไปทำ col ที่กราฟ
        $graph_hv = array();
        $graph_val = array();
        if(!empty($to_real['จ่าย'])){
            foreach ($to_real['จ่าย'] as $kst => $vst) {
                if($kst == 'PK' || $kst == 'PF'){
                    $i = 0;
                    foreach ($vst as $kpd => $vpd) {
                        $graph_hv[$kst]['head'][$i] =  $kpd;
                        //ไม่มี number_format
                        if(!empty($to_real['จ่าย'][$kst][$kpd]['qty']['plan']))
                            $graph_hv[$kst]['plan'][$i] = $to_real['จ่าย'][$kst][$kpd]['qty']['plan'];
                        else
                            $graph_hv[$kst]['plan'][$i] = 0;
                        $graph_hv[$kst]['actual'][$i] = 0;
                        if(!empty($to_real['จ่าย'][$kst][$kpd]['qty']['actual_b']))
                            $graph_hv[$kst]['actual'][$i] += $to_real['จ่าย'][$kst][$kpd]['qty']['actual_b'];
                        if(!empty($to_real['จ่าย'][$kst][$kpd]['qty']['actual_c']))
                            $graph_hv[$kst]['actual'][$i] += $to_real['จ่าย'][$kst][$kpd]['qty']['actual_c'];
                        $graph_hv[$kst]['diff'][$i] = $graph_hv[$kst]['actual'][$i] - $graph_hv[$kst]['plan'][$i];

                        $i++;
                    }
                }
            }
        }
        // dd($graph_hv);

        return [$to_real, $to_real1, $status_move, $graph_hv] ;
    }

    public function mail_2($sent_date,$ed_date){
        // $to_rec = array();
        // $actual_C = WhActivityDetail::join('wh_act_types', 'wh_activity_details.moving_type', '=', DB::raw('wh_act_types.code collate Latvian_BIN'))
        //     ->selectRaw("wh_activity_details.mat, SUBSTRING(wh_activity_details.store_loc,1,2) as store_loc,
        //     CASE
        //         WHEN wh_activity_details.entry_date BETWEEN '".$sent_date." 07:30:00' AND '".$sent_date." 18:00:00' THEN 'B'
        //         ELSE 'C' END AS time_shift,
        //     sum(wh_activity_details.qty_entry) AS qty_entry, wh_activity_details.unit_entry")
        //     ->where(function($q1) {
        //         $q1->where('wh_activity_details.store_loc', 'like', 'PK%')
        //            ->orWhere('wh_activity_details.store_loc', 'like', 'PF%');
        //          })
        //     ->whereRaw("(wh_activity_details.entry_date >= ? AND wh_activity_details.entry_date < ?)", [$sent_date ." 08:30:00", $ed_date ." 05:00:00"])
        //     ->where('wh_act_types.name', 'like', '%รับ%')
        //     ->groupByRaw("CASE
        //     WHEN wh_activity_details.entry_date BETWEEN '".$sent_date." 07:30:00' AND '".$sent_date." 18:00:00' THEN 'B'
        //     ELSE 'C' END, SUBSTRING(wh_activity_details.store_loc,1,2), wh_activity_details.mat, wh_activity_details.unit_entry")
        //     ->orderByRaw('SUBSTRING(wh_activity_details.store_loc,1,2)')->orderBy('wh_activity_details.mat')->get();
        // // dd($actual_C);
        // foreach($actual_C as $key){
        //     if($key->unit_entry == 'KG'){
        //         $to_cal = 1;
        //     }else{
        //         $cut_1 = substr($key->mat,strlen(str_replace(range(0,9),'',$key->mat)));
        //         if(strlen(str_replace(range(0,9),'',$cut_1))>0){
        //             $cut_2 = substr($cut_1,strlen(str_replace(range(0,9),'',$cut_1)));
        //         }else{
        //             $cut_2 = $cut_1;
        //         }
        //         $to_cal = (int)$cut_2/1000;
        //     }
        //     $to_rec[$key->store_loc][$key->mat][$key->time_shift] = (float)$key->qty_entry * $to_cal;
        // }
        // return [$to_rec] ;

    }

    public function mail_file($show_date,$sent_date,$to_show,$to_show1,$to_move){

        $spreadsheet = new Spreadsheet();
        $count_sheet = 0;
        // dd($to_show);

        //1
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValueByColumnAndRow(1, 1, "รายงานสรุปงานประจำวัน แผนกคลังสินค้าแช่แข็ง วันที่ ".$show_date);
        $sheet->mergeCellsByColumnAndRow(1, 1, 10, 1);
        $sheet->getStyle('A1')->getFont()->setName('Cordia New')->setSize(16);
        $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1')->getFont()->setBold(true);
        $startrow = 3;
        if (isset($to_show['จ่าย'])) {
            $sheet->setCellValueByColumnAndRow(1, $startrow, "1. งานจ่ายสินค้า");
            $sheet->mergeCellsByColumnAndRow(1, $startrow, 3, $startrow);
            $sheet->getStyle('A3')->getFont()->setName('Cordia New')->setSize(14);
            $sheet->getStyle('A3')->getFont()->setBold(true);
            $sheet->getStyle('A3')->getAlignment()->setHorizontal('left');
            $startrow++;
            $startrow++;

            $sheet->setCellValueByColumnAndRow(1, $startrow, "ลำดับที่");
            $sheet->mergeCellsByColumnAndRow(1, $startrow, 1, ($startrow+2));
            $sheet->setCellValueByColumnAndRow(2, $startrow, "แผนก");
            $sheet->mergeCellsByColumnAndRow(2, $startrow, 2, ($startrow+2));
            $sheet->setCellValueByColumnAndRow(3, $startrow, "รหัสสินค้า");
            $sheet->mergeCellsByColumnAndRow(3, $startrow, 3, ($startrow+2));
            $sheet->setCellValueByColumnAndRow(4, $startrow, "เวลา (น.)");
            $sheet->mergeCellsByColumnAndRow(4, $startrow, 6, $startrow);
            $sheet->setCellValueByColumnAndRow(7, $startrow, "จำนวน (Kg.)");
            $sheet->mergeCellsByColumnAndRow(7, $startrow, 13, $startrow);
            $sheet->setCellValueByColumnAndRow(14, $startrow, "หมายเหตุ");
            $sheet->mergeCellsByColumnAndRow(14, $startrow, 14, $startrow+2);

            $startrow++;

            $sheet->setCellValueByColumnAndRow(4, $startrow, "Plan");
            $sheet->mergeCellsByColumnAndRow(4, $startrow, 4, ($startrow+1));
            $sheet->setCellValueByColumnAndRow(5, $startrow, "Actual-B");
            $sheet->mergeCellsByColumnAndRow(5, $startrow, 5, ($startrow+1));
            $sheet->setCellValueByColumnAndRow(6, $startrow, "Actual-C");
            $sheet->mergeCellsByColumnAndRow(6, $startrow, 6, ($startrow+1));
            $sheet->setCellValueByColumnAndRow(7, $startrow, "Plan");
            $sheet->mergeCellsByColumnAndRow(7, $startrow, 7, ($startrow+1));
            $sheet->setCellValueByColumnAndRow(8, $startrow, "Actual-B");
            $sheet->mergeCellsByColumnAndRow(8, $startrow, 9, $startrow);
            $sheet->setCellValueByColumnAndRow(10, $startrow, "Actual-C");
            $sheet->mergeCellsByColumnAndRow(10, $startrow, 11, $startrow);
            $sheet->setCellValueByColumnAndRow(12, $startrow, "รวม");
            $sheet->mergeCellsByColumnAndRow(12, $startrow, 13, $startrow);

            $startrow++;
            $startcol = 8;
            for($i=0; $i<3; $i++){
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, "Q");
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, "P");
                $startcol++;
            }

            $sheet->getStyle('A5:N7')->getAlignment()->setVertical('center');
            $sheet->getStyle('A5:N7')->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A5:N7')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('F4B084');
            $startrow++;

            $start1 = $startrow;
            $num_row = 0;
            $arr_loop = array('plan','actual_b','actual_c');
            foreach ($to_show['จ่าย'] as $kstore => $vstore) {
                // echo $kstore.'--';
                // dd($vstore);
                foreach ($vstore as $kpd => $vpd) {
                    $num_row++;
                    $sheet->setCellValueByColumnAndRow(1, $startrow, $num_row);
                    $sheet->setCellValueByColumnAndRow(2, $startrow, $kstore);
                    $sheet->setCellValueByColumnAndRow(3, $startrow, $kpd);

                    $startcol=4;
                    foreach ($arr_loop as $ktype => $vtype) {
                        if(isset($to_show['จ่าย'][$kstore][$kpd]['time'][$vtype])){
                            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $to_show['จ่าย'][$kstore][$kpd]['time'][$vtype]);
                        }
                        $startcol++;
                    }


                    foreach ($arr_loop as $ktype => $vtype) {
                        if(isset($to_show['จ่าย'][$kstore][$kpd]['qty'][$vtype])){
                            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $to_show['จ่าย'][$kstore][$kpd]['qty'][$vtype]);
                        }
                        $startcol++;

                        if($vtype != 'plan'){
                            if(isset($to_show['จ่าย'][$kstore][$kpd]['pl'][$vtype])){
                                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $to_show['จ่าย'][$kstore][$kpd]['pl'][$vtype]);
                            }
                            $startcol++;
                        }
                    }
                    $sheet->setCellValueByColumnAndRow(12, $startrow,'=H'. $startrow.'+J'. $startrow);
                    $sheet->setCellValueByColumnAndRow(13, $startrow,'=I'. $startrow.'+K'. $startrow);
                    if(!empty($to_show['จ่าย'][$kstore][$kpd]['note']))    $sheet->setCellValueByColumnAndRow(14, $startrow, $to_show['จ่าย'][$kstore][$kpd]['note']);
                    $startrow++;
                }
            }

            $sheet->getStyle('A'.$start1.':A'.($startrow-1))->getAlignment()->setVertical('center');

            $count_row = $start1;
            foreach ($to_show['จ่าย'] as $kstore => $vstore) {
                $sheet->mergeCellsByColumnAndRow(2, $count_row, 2, ($count_row+count($vstore)-1));
                $count_row = $count_row+count($vstore);
            }
            $sheet->getStyle('B'.$start1.':B'.($startrow-1))->getAlignment()->setVertical('center');
            $sheet->getStyle('B'.$start1.':B'.($startrow-1))->getAlignment()->setHorizontal('center');

            $sheet->getStyle('E'.$start1.':E'.($startrow-1))->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('FFD966');
            $sheet->getStyle('F'.$start1.':F'.($startrow-1))->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('A9D08E');

            $sheet->getStyle('H'.$start1.':I'.($startrow-1))->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('FFD966');
            $sheet->getStyle('J'.$start1.':K'.($startrow-1))->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('A9D08E');

            $sheet->setCellValueByColumnAndRow(1, $startrow, "รวม");
            $sheet->mergeCellsByColumnAndRow(1, $startrow, 6, $startrow);
            $sheet->getStyle('A'.$startrow)->getAlignment()->setHorizontal('right');   //getStyle($col_txt.'4:'.$col_ed_wrap.'4')

            $startcol=7;
            for($i=0;$i<7;$i++){
                $col_name = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($startcol);
                $sheet->setCellValueByColumnAndRow($startcol, $startrow,'=SUM('.$col_name.$start1.':'.$col_name.($startrow-1).')');
                $startcol++;
            }

            $sheet->getStyle('G'.$start1.':M'.$startrow)->getNumberFormat()->setFormatCode('#,##0.00');

            $sheet->getStyle('A3:N'.$startrow)->getFont()->setName('Cordia New')->setSize(14);
            $sheet->getStyle('A5:N'.$startrow)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

            $startrow++;
            $startrow++;
            $startrow++;
        }

        if (isset($to_show['รับ'])) {
            $sheet->setCellValueByColumnAndRow(1, $startrow, "2. งานรับสินค้า");
            $sheet->mergeCellsByColumnAndRow(1, $startrow, 3, $startrow);
            $sheet->getStyle('A'.$startrow)->getFont()->setName('Cordia New')->setSize(14);
            $sheet->getStyle('A'.$startrow)->getFont()->setBold(true);
            $sheet->getStyle('A'.$startrow)->getAlignment()->setHorizontal('left');

            $startrow++;
            $startrow++;
            $strow = $startrow;
            $sheet->setCellValueByColumnAndRow(1, $startrow, "ลำดับที่");
            $sheet->mergeCellsByColumnAndRow(1, $startrow, 1, ($startrow+1));
            $sheet->setCellValueByColumnAndRow(2, $startrow, "แผนก");
            $sheet->mergeCellsByColumnAndRow(2, $startrow, 2, ($startrow+1));
            $sheet->setCellValueByColumnAndRow(3, $startrow, "รหัสสินค้า");
            $sheet->mergeCellsByColumnAndRow(3, $startrow, 3, ($startrow+1));
            $sheet->setCellValueByColumnAndRow(4, $startrow, "B (Kg.)");
            $sheet->mergeCellsByColumnAndRow(4, $startrow, 5, $startrow);
            $sheet->setCellValueByColumnAndRow(6, $startrow, "C (Kg.)");
            $sheet->mergeCellsByColumnAndRow(6, $startrow, 7, $startrow);
            $sheet->setCellValueByColumnAndRow(8, $startrow, "รวม (Kg.)");
            $sheet->mergeCellsByColumnAndRow(8, $startrow, 9, $startrow);

            $startrow++;
            $startcol = 4;
            for($i=0; $i<3; $i++){
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, "Q");
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, "P");
                $startcol++;
            }

            $sheet->getStyle('A'.$strow.':I'.$startrow)->getAlignment()->setVertical('center');
            $sheet->getStyle('A'.$strow.':I'.$startrow)->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A'.$strow.':I'.$startrow)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('F4B084');
            $startrow++;

            $start1 = $startrow;
            $num_row = 0;
            foreach ($to_show['รับ'] as $kstore => $vstore) {
                foreach ($vstore as $kpd => $vpd) {
                    $num_row++;
                    $sheet->setCellValueByColumnAndRow(1, $startrow, $num_row);
                    $sheet->setCellValueByColumnAndRow(2, $startrow, $kstore);
                    $sheet->setCellValueByColumnAndRow(3, $startrow, $kpd);
                    if (isset($to_show['รับ'][$kstore][$kpd]['qty']['actual_b'])) {
                        $sheet->setCellValueByColumnAndRow(4, $startrow, $to_show['รับ'][$kstore][$kpd]['qty']['actual_b']);
                    }
                    if (isset($to_show['รับ'][$kstore][$kpd]['pl']['actual_b'])) {
                        $sheet->setCellValueByColumnAndRow(5, $startrow, $to_show['รับ'][$kstore][$kpd]['pl']['actual_b']);
                    }
                    if (isset($to_show['รับ'][$kstore][$kpd]['qty']['actual_c'])) {
                        $sheet->setCellValueByColumnAndRow(6, $startrow, $to_show['รับ'][$kstore][$kpd]['qty']['actual_c']);
                    }
                    if (isset($to_show['รับ'][$kstore][$kpd]['pl']['actual_c'])) {
                        $sheet->setCellValueByColumnAndRow(7, $startrow, $to_show['รับ'][$kstore][$kpd]['pl']['actual_c']);
                    }
                    $sheet->setCellValueByColumnAndRow(8, $startrow, '=D' . $startrow . '+F' . $startrow);
                    $sheet->setCellValueByColumnAndRow(9, $startrow, '=E' . $startrow . '+G' . $startrow);
                    $startrow++;
                }
            }
            $sheet->getStyle('A'.$start1.':A'.($startrow-1))->getAlignment()->setVertical('center');

            $count_row = $start1;
            foreach ($to_show['รับ'] as $kstore => $vstore) {
                $sheet->mergeCellsByColumnAndRow(2, $count_row, 2, ($count_row+count($vstore)-1));
                $count_row = $count_row+count($vstore);
            }
            $sheet->getStyle('B'.$start1.':B'.($startrow-1))->getAlignment()->setVertical('center');
            $sheet->getStyle('B'.$start1.':B'.($startrow-1))->getAlignment()->setHorizontal('center');

            $sheet->getStyle('D'.$start1.':E'.($startrow-1))->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('FFD966');
            $sheet->getStyle('F'.$start1.':G'.($startrow-1))->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('A9D08E');

            $sheet->setCellValueByColumnAndRow(1, $startrow, "รวม");
            $sheet->mergeCellsByColumnAndRow(1, $startrow, 3, $startrow);
            $sheet->getStyle('A'.$startrow)->getAlignment()->setHorizontal('right');   //getStyle($col_txt.'4:'.$col_ed_wrap.'4')

            $startcol=4;
            for($i=0;$i<6;$i++){
                $col_name = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($startcol);
                $sheet->setCellValueByColumnAndRow($startcol, $startrow,'=SUM('.$col_name.$start1.':'.$col_name.($startrow-1).')');
                $startcol++;
            }
            $sheet->getStyle('D'.$strow.':I'.$startrow)->getNumberFormat()->setFormatCode('#,##0.00');

            $sheet->getStyle('A'.$strow.':I'.$startrow)->getFont()->setName('Cordia New')->setSize(14);
            $sheet->getStyle('A'.$strow.':I'.$startrow)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
        }

        for($i=1; $i<=14; $i++) {
            $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($i);
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $spreadsheet->getActiveSheet()->setTitle('1');
        $count_sheet++;
        $spreadsheet->createSheet();

        //2,3
        $pk_pf = array('PK','PF');
        $sort_act = ['เคลื่อนย้าย', 'จ่าย', 'รับ'];
        if(!empty($to_show1)){
            foreach ($pk_pf as $kp => $vp){
                if(!empty($to_show[$vp])){
                    $sheet = $spreadsheet->setActiveSheetIndex($count_sheet);
                    $startrow = 1;

                    $arr_loop = $to_move[$vp];
                    $sheet->setCellValueByColumnAndRow(1, $startrow, "Item automatically created");
                    $sheet->mergeCellsByColumnAndRow(1, $startrow, 1, ($startrow+2));
                    $sheet->setCellValueByColumnAndRow(2, $startrow, "Material");
                    $sheet->mergeCellsByColumnAndRow(2, $startrow, 2, ($startrow+2));
                    $sheet->setCellValueByColumnAndRow(3, $startrow, "Storage Location");
                    $sheet->mergeCellsByColumnAndRow(3, $startrow, 3, ($startrow+2));
                    $sheet->setCellValueByColumnAndRow(4, $startrow, "สถานะ");
                    $sheet->setCellValueByColumnAndRow(5, $startrow, "Values");
                    $startrow++;
                    $startcol = 4;
                    foreach ($sort_act as $kmov => $vmov) {
                        $sheet->setCellValueByColumnAndRow($startcol, $startrow, $vmov);
                        $startcol = $startcol+2;
                    }
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, "Total Q");
                    $startcol++;
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, "Total P");
                    $col_end = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($startcol);
                    $startrow++;
                    $startcol = 3;
                    for($i=0;$i<count($arr_loop);$i++){
                        $startcol++;
                        $sheet->setCellValueByColumnAndRow($startcol, $startrow, "Q");
                        $startcol++;
                        $sheet->setCellValueByColumnAndRow($startcol, $startrow, "P");
                    }

                    $sheet->getStyle('A1:'.$col_end.'3')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9E1F2');
                    $sheet->getStyle('A1:'.$col_end.'3')->getAlignment()->setVertical('center');
                    $sheet->getStyle('A1:'.$col_end.'3')->getAlignment()->setHorizontal('center');
                    $sheet->getStyle('A1:'.$col_end.'3')->getFont()->setBold(true);

                    $startrow = 4;
                    $sheet->setCellValueByColumnAndRow(1, $startrow, "c");
                    foreach ($to_show1[$vp] as $kmat => $vmat) {
                        foreach ($vmat as $kst => $vst) {
                            $startcol = 2;
                            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $kmat);
                            $startcol++;
                            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $kst);
                            $sum_q = '=';
                            $sum_p = '=';
                            foreach ($sort_act as $kloop => $vloop) {
                                // echo  "to_show1[$vp][".$kmat.']['.$kst.']['.$vloop."]['Q']</br>";
                                $startcol++;
                                if(isset($to_show1[$vp][$kmat][$kst][$vloop]['Q']))   $sheet->setCellValueByColumnAndRow($startcol, $startrow, $to_show1[$vp][$kmat][$kst][$vloop]['Q']);
                                $col_q = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($startcol);
                                $sum_q .= '+'.$col_q.$startrow;
                                $startcol++;
                                if(isset($to_show1[$vp][$kmat][$kst][$vloop]['P']))   $sheet->setCellValueByColumnAndRow($startcol, $startrow, $to_show1[$vp][$kmat][$kst][$vloop]['P']);
                                $col_p = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($startcol);
                                $sum_p .= '+'.$col_p.$startrow;
                            }
                            $startcol++;

                            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $sum_q);
                            $startcol++;
                            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $sum_p);
                            $startrow++;
                        }
                    }
                    $sheet->setCellValueByColumnAndRow(1, $startrow, 'Grand Total');
                    $sheet->mergeCellsByColumnAndRow(2, $startrow, 3, $startrow);
                    $startcol = 3;
                    for($i=0;$i<((count($arr_loop)+1)*2);$i++){
                        $startcol++;
                        $col_name = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($startcol);
                        $sheet->setCellValueByColumnAndRow($startcol, $startrow, '=SUM('.$col_name.'4:'.$col_name.($startrow-1).')');
                        if($startcol%2 == 0){
                            $sheet->getStyle($col_name.'4:'.$col_name.$startrow)->getNumberFormat()->setFormatCode('#,##0.00');
                        // }else{
                        //     $sheet->getStyle($col_name.'4:'.$col_name.$startrow)->getNumberFormat()->setFormatCode('#,##0');
                        }
                    }

                    for($i=1; $i<=$startcol; $i++) {
                        $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($i);
                        $sheet->getColumnDimension($columnID)->setAutoSize(true);
                    }


                    $sheet->getStyle('A'.$startrow.':'.$col_end.$startrow)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9E1F2');
                    $sheet->getStyle('A'.$startrow.':'.$col_end.$startrow)->getFont()->setBold(true);
                    $sheet->getStyle('A3:B'.($startrow-1))->getFont()->setBold(true);

                    // $sheet->getStyle('A3:J'.$startrow)->getFont()->setName('Cordia New')->setSize(14);
                    // $sheet->getStyle('A1:K'.$startrow)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                    $spreadsheet->getActiveSheet()->setTitle('สรุป'.$vp);
                    $count_sheet++;
                    $spreadsheet->createSheet();
                }
            }
        }


        //สรุป
        $sheet = $spreadsheet->setActiveSheetIndex($count_sheet);
        $startrow = 1;
        if(!empty($to_show1['all'])){
            // $startrow = $startrow+2;
            // $st_tb = $startrow;
            $arr_loop = $to_move['all'];
            $sheet->setCellValueByColumnAndRow(1, $startrow, "Row Labels");
            $sheet->mergeCellsByColumnAndRow(1, $startrow, 1, ($startrow+2));
            $sheet->setCellValueByColumnAndRow(2, $startrow, "Column Labels");
            $sheet->mergeCellsByColumnAndRow(2, $startrow, (count($to_move['all'])*2), $startrow);
            $startrow++;
            $startcol = 2;
            foreach ($arr_loop as $kmov => $vmov) {
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $kmov);
                $startcol = $startcol+2;
            }
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, "Total Q");
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, "Total P");
            $col_end = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($startcol);
            $startrow++;
            $startcol = 2;
            foreach ($arr_loop as $kmov => $vmov) {
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, 'Q');
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, 'P');
                $startcol++;
            }

            $sheet->getStyle('A1:'.$col_end.'3')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9E1F2');
            $sheet->getStyle('A1:'.$col_end.'3')->getAlignment()->setVertical('center');
            $sheet->getStyle('A1:'.$col_end.'3')->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A1:'.$col_end.'3')->getFont()->setBold(true);


            $startrow = 4;
            foreach ($to_show1['all'] as $kmat => $vmat) {
                $startcol = 1;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $kmat);
                $sum_q = '=';
                $sum_p = '=';
                foreach ($arr_loop as $kmov => $vmov) {
                    $startcol++;
                    if(isset($to_show1['all'][$kmat][$kmov]['Q'])){
                        $sheet->setCellValueByColumnAndRow($startcol, $startrow, $to_show1['all'][$kmat][$kmov]['Q']);
                        $col_q = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($startcol);
                        $sum_q .= '+'.$col_q.$startrow;
                    }
                    $startcol++;
                    if(isset($to_show1['all'][$kmat][$kmov]['P'])){
                        $sheet->setCellValueByColumnAndRow($startcol, $startrow, $to_show1['all'][$kmat][$kmov]['P']);
                        $col_p = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($startcol);
                        $sum_p .= '+'.$col_p.$startrow;
                    }
                }
                $startcol++;
                if($sum_q!='=') $sheet->setCellValueByColumnAndRow($startcol, $startrow, $sum_q);
                $startcol++;
                if($sum_p!='=') $sheet->setCellValueByColumnAndRow($startcol, $startrow, $sum_p);
                $startrow++;
            }
            $sheet->setCellValueByColumnAndRow(1, $startrow, 'Grand Total');
            $startcol = 1;
            for($i=0;$i<((count($arr_loop)+1)*2);$i++){
                $startcol++;
                $col_name = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($startcol);
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, '=SUM('.$col_name.'4:'.$col_name.($startrow-1).')');
                if($startcol%2 == 0){
                    $sheet->getStyle($col_name.'4:'.$col_name.$startrow)->getNumberFormat()->setFormatCode('#,##0.00');
                // }else{
                //     $sheet->getStyle($col_name.'4:'.$col_name.$startrow)->getNumberFormat()->setFormatCode('#,##0');
                }
            }

            for($i=1; $i<=$startcol; $i++) {
                $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($i);
                $sheet->getColumnDimension($columnID)->setAutoSize(true);
            }

            $sheet->getStyle('A'.$startrow.':'.$col_end.$startrow)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9E1F2');
            $sheet->getStyle('A'.$startrow.':'.$col_end.$startrow)->getFont()->setBold(true);

            // $sheet->getStyle('A3:J'.$startrow)->getFont()->setName('Cordia New')->setSize(14);
            // $sheet->getStyle('A1:K'.$startrow)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
        }
        $spreadsheet->getActiveSheet()->setTitle('สรุป');

        $spreadsheet->setActiveSheetIndex(0);
        return [$spreadsheet] ;
    }

    function separator1000($aVal) {
        return number_format($aVal);
    }
    function separator1000_2($aVal) {
        return number_format($aVal,2);
    }

    public function sent_mail()
    {
        $sent_date = date("Y-m-d", strtotime("-1 day"));   //ลบหนึ่งวันจากวันปัจจุบัน
        // $sent_date = '2022-08-22';
        $ed_date = date ("Y-m-d", strtotime("1 day",strtotime($sent_date)));

        $show_date = date("d", strtotime($sent_date)).'/'.date("m", strtotime($sent_date)).'/'.(date("Y", strtotime($sent_date))+543);

        $to_show = array();
        $to_show1 = array();
        $to_move = array();
        $graph_hv = array();
        // $graph_val = array();

        $test = $this->mail_1($sent_date,$ed_date);
        if(!empty($test[0]))    $to_show = $test[0];
        if(!empty($test[1]))    $to_show1 = $test[1];
        if(!empty($test[2]))    $to_move = $test[2];
        if(!empty($test[3]))    $graph_hv = $test[3];
        // dd($to_show);

        require_once app_path() . '/jpgraph/jpgraph.php';
        require_once app_path() . '/jpgraph/jpgraph_bar.php';

        foreach ($graph_hv as $kst => $vst) {
            $data1y=$graph_hv[$kst]['plan'];
            $data2y=$graph_hv[$kst]['actual'];
            $data3y=$graph_hv[$kst]['diff'];
            // dd($data2y);

            // $count_col = count($graph_hv[$kst]['plan']);
            // $nbrbar = $count_col*2; // number of bars and hence number of columns in table
            // $cellwidth = 17;
            // $tableypos = 319; // top left Y-coordinate for table
            // $tablexpos = 130;  // top left X-coordinate for table
            // $tablewidth = $nbrbar*$cellwidth; // overall table width
            // $rightmargin = 30;  // right margin in the graph
            // $topmargin = 60;  // top margin of graph
            // $height = 384;  // a suitable height for the image
            // $width = $tablexpos+$tablewidth+$rightmargin; // the width of the image
            // $graph1 = new \Graph($width,$height);
            // $graph1->img->SetMargin($tablexpos,$rightmargin,$topmargin,$height-$tableypos);


            $graph1 = new \Graph(600,400,'auto');
            $graph1->SetScale("textlin");
            $graph1->SetY2Scale("lin",0,90);
            $graph1->SetY2OrderBack(false);

            $theme_class = new \UniversalTheme;
            $graph1->SetTheme($theme_class);

            $graph1->SetMargin(60,20,100,80);    //ระยะขอบรอบพื้นที่ ($lm,$rm,$tm,$bm)
            $graph1->SetBox(false);

            // $graph->xaxis->Hide();  //ซ่อน label ใต้กราฟ ต้องอยู่ใต้ SetMarginColor
            $graph1->ygrid->SetFill(false);

            $graph1->xaxis->SetPos('min');  //ทำให้ได้ค่าต่ำสุดของกราฟ เพื่อให้ชื่อกลุ่มไปอยู่ล่างสุด

            $graph1->xaxis->SetTickLabels($graph_hv[$kst]['head']);
            $graph1->xaxis->SetLabelAngle(45);
            $graph1->yaxis->SetLabelFormatCallback(array(new WhActsContoller,'separator1000'));
            $graph1->yaxis->HideLine(false);
            $graph1->yaxis->HideTicks(false,false);

            // Create the bar plots
            $b1plot = new \BarPlot($data1y);
            $b2plot = new \BarPlot($data2y);
            $b3plot = new \BarPlot($data3y);

            $gbplot = new \GroupBarPlot(array($b1plot,$b2plot,$b3plot));

            // ...and add it to the graPH
            $graph1->Add($gbplot);
            // $graph->AddY2($lplot);
            $b1plot->SetColor("#FFFFFF");   //สีเส้น bar
            $b1plot->SetFillColor("#1065FA");   //สี bar
            $b1plot->SetLegend("plan");   //สำหรับใส่ป้ายกำกับ
            $b1plot->value->SetFormat('%01.2f');
            $b1plot->value->SetColor("#000000");
            $b1plot->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
            $b1plot->value->SetAngle(90);
            $b1plot->value->SetFormatCallback(array(new WhActsContoller,'separator1000_2'));  //เรียกใช้ function separator1000 เพื่อกำหนด number_format
            // $b1plot->value->SetFormatCallback('separator1000');
            $b1plot->value->Show();

            $b2plot->SetColor("#FFFFFF");   //สีเส้น bar
            $b2plot->SetFillColor("#21A80B");   //สี bar
            $b2plot->SetLegend("actual");  //สำหรับใส่ป้ายกำกับ
            $b2plot->value->SetFormat('%01.2f');
            $b2plot->value->SetColor("#000000");
            $b2plot->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
            $b2plot->value->SetAngle(90);
            $b2plot->value->SetFormatCallback(array(new WhActsContoller,'separator1000_2'));  //เรียกใช้ function separator1000 เพื่อกำหนด number_format
            // $b2plot->value->SetFormatCallback('separator1000');
            $b2plot->value->Show();

            $b3plot->SetColor("#FFFFFF");   //สีเส้น bar
            $b3plot->SetFillColor("#F4150B");   //สี bar
            $b3plot->SetLegend("diff");  //สำหรับใส่ป้ายกำกับ
            $b3plot->value->SetFormat('%01.2f');
            $b3plot->value->SetColor("#000000");
            $b3plot->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
            $b3plot->value->SetAngle(90);
            $b3plot->value->SetFormatCallback(array(new WhActsContoller,'separator1000_2'));  //เรียกใช้ function separator1000 เพื่อกำหนด number_format
            // $b3plot->value->SetFormatCallback('separator1000');
            $b3plot->value->Show();

            $graph1->legend->SetFrameWeight(1);
            $graph1->legend->SetColumns(3);
            // $graph4->legend->SetColor('#4E4E4E','#00A78A');
            $graph1->legend->SetPos(0.5,0.05,'center','top');    //ตำแหน่งของป้ายกำกับ ($aX,$aY,$aHAlign='right',$aVAlign='top') $aX,$aY=0-1

            $graph1->title->Set($kst);
            $graph1->title->SetFont(FF_ANGSA, FS_NORMAL, 18);

            $path = public_path() . '/graph/'.date('Y') . "/" . date('m') . '/mail';
            if (!File::exists($path)) {
                File::makeDirectory($path,  0777, true, true);
            }
            $date = date('ymdHis');

            $gp['link'][$kst] = "graph/".date('Y'). "/" . date('m') . "/mail/".$kst.'_'.$date . ".jpg";
            $gp['path'][$kst] = $path . "/".$kst.'_'.$date . ".jpg";
            $graph1->Stroke($gp['path'][$kst]);
            echo $gp['path'][$kst]."\r\n";
            // dd($gp['gp4']['path'][$count_gp]);
            // $count_gp++;
        }

        // $test1 = $this->mail_2($sent_date,$ed_date);
        // if(!empty($test1[0]))    $to_rec = $test1[0];
        $test2 = $this->mail_file($show_date,$sent_date,$to_show,$to_show1,$to_move);
        if(!empty($test2[0])){
            $spreadsheet = $test2[0];
            $writer = new Xlsx($spreadsheet);
            $filename = "exportdata-". $sent_date."-".date('ymd-his').".xlsx";
            $writer->save('storage/'.$filename);


        }
        if(count($to_show)>0){
            $mail_to = Mail::send(new ReportWH_Acts($show_date,$to_show,$gp,$filename));
            return redirect('wh_act')->with('success', ' Send Mail is complete!');
        }else{
            return redirect('wh_act')->with('error', ' Send Mail is Error!');
        }
    }

    public function destroy_date($date)
    {
        // dd($date);
        $ed_date = date ("Y-m-d", strtotime("1 day",strtotime($date)));
        // dd($ed_date);
        // $act_id = array();
        // $act_1 = array();
        // $plan_1 = array();
        // $plan_2 = array();
        // $act_1 = WhActivityDetail::whereRaw("CONCAT(entry_date, ' ', entry_time) > '$date 07:30:00'")
        //     ->whereRaw("CONCAT(entry_date, ' ', entry_time) < '$ed_date 07:30:01'")->get();
        // foreach($act_1 as $key){
        //     $act_id[$key->wh_activity_id] = $key->wh_activity_id;
        // }
        // WhActivityDetail::whereRaw("CONCAT(entry_date, ' ', entry_time) > '$date 07:30:00'")
        //     ->whereRaw("CONCAT(entry_date, ' ', entry_time) < '$ed_date 07:30:01'")->delete();

        // foreach($act_id as $key=>$value){
        //     $act = WhActivityDetail::where('wh_activity_id',$key)->count();
        //     if($act==0){
        //         $pd_input = WhActivity::where('id',$key)->delete();
        //     }
        // }
        // // dd('delet act');
        // $plan_1 = WhPlanAct::where('requirement_date',$date)->get();
        // foreach($plan_1 as $key){
        //     $plan_2 = WhPlanActMS::where('wh_plan_act_id',$key->id)->get();
        //     foreach($plan_2 as $key1){
        //         WhPlanActD::where('wh_plan_act_m_id',$key1->id)->delete();
        //     }
        //     WhPlanActMS::where('wh_plan_act_id',$key->id)->delete();
        // }
        // WhPlanAct::where('requirement_date',$date)->delete();


        // $plan = WhPlanActD::
        //     ->whereRaw('CONVERT(DATE, requirement_date) =  ?', [$sent_date])->get();

        // $to_actual = WhActivityDetail::join('wh_act_types', 'wh_activity_details.wh_act_type_id', '=', 'wh_act_types.id')
        //     ->selectRaw("SUBSTRING(wh_activity_details.store_loc, 1, 2) AS store_loc, wh_activity_details.store_loc AS st_loc, wh_activity_details.wh_act_material_id,  wh_act_types.name,
        //         wh_activity_details.qty_entry, wh_activity_details.entry_time,
        //         CASE
        //             WHEN CONVERT(time,wh_activity_details.entry_time) BETWEEN '00:00:00' AND '07:29:59' THEN CONVERT(DATE, DATEADD(day,-1,wh_activity_details.entry_date))
        //             ELSE CONVERT(DATE, wh_activity_details.entry_date) END AS entry_date,
        //         CASE WHEN CONVERT(time,wh_activity_details.entry_time) BETWEEN '07:30:01' AND '19:30:00' THEN 'actual_b' ELSE 'actual_c' END AS time_shift")
        //     ->whereBetween('wh_activity_details.entry_date', [$sent_date, $ed_date])
        //     ->orderBy('wh_activity_details.entry_date')->orderBy('wh_activity_details.entry_time')->orderBy('wh_activity_details.store_loc')->get();

        // $to_note = WhActNote::where('entry_date', $sent_date)->get();



        WhActivityDetail::whereRaw("CONCAT(entry_date, ' ', entry_time) > '$date 07:30:00'")
            ->whereRaw("CONCAT(entry_date, ' ', entry_time) < '$ed_date 07:30:01'")->delete();
        WhPlanActD::whereRaw('CONVERT(DATE, requirement_date) =  ?', [$date])->delete();
        WhActNote::where('entry_date', $date)->delete();

        return redirect('wh_act')->with('success', ' deleted!');
    }

    // public function save_note($st, $pd, $val){
    public function save_note(Request $request){
        $requestData = $request->all();
        // dd(!empty($requestData['value']));
        $to_return = '';
        $sent_date = date("Y-m-d", strtotime("-1 day"));
        $query = WhActMaterial::where("name", '<>', '')->get();
        foreach ($query as $key) {
            $mat[$key->name] = $key->id;
        }
        $to_save = array();
        $chk = WhActNote::where("entry_date", $sent_date)->where("store_loc", $requestData['st'])
            ->where("wh_act_material_id", $mat[$requestData['pd']])->get();
        // dd($chk[0]['note']);
        if(count($chk)==0){
            $to_save['entry_date'] = $sent_date;
            $to_save['store_loc'] = $requestData['st'];
            $to_save['wh_act_material_id'] = $mat[$requestData['pd']];
            $to_save['note'] = $requestData['value'];
            $to_return = WhActNote::create($to_save);
        }else{
            $query = WhActNote::where("entry_date", $sent_date)->where("store_loc", $requestData['st'])
                ->where("wh_act_material_id", $mat[$requestData['pd']]);
            if($chk[0]['note']!=trim($requestData['value'])){
                if(!empty($requestData['value'])){
                    $to_save['note'] = $requestData['value'];
                    $to_return = $query->update($to_save);
                }else{
                    // dd('delete');
                    $to_return = $query->delete();
                }
            }
        }
        // dd($to_return);
        if($to_return){
            return '1';
        }else{
            return '0';
        }
    }

    public function destroy_import(Request $request)
    {
        $requestData = $request->all();
        // dd($requestData);
        if($requestData['type'] === 'plan'){
            WhPlanActD::whereRaw('CONVERT(DATE, requirement_date) =  ?', [$requestData['date']])->delete();
        }elseif($requestData['type'] === 'act'){
            $ed_date = date ("Y-m-d", strtotime("1 day",strtotime($requestData['date'])));
            WhActivityDetail::whereRaw("CONCAT(entry_date, ' ', entry_time) > '".$requestData['date']." 07:30:00'")
            ->whereRaw("CONCAT(entry_date, ' ', entry_time) < '$ed_date 07:30:01'")->delete();
            WhActNote::where('entry_date', $requestData['date'])->delete();
        }
        return redirect()->back()->with('success', ' deleted!');

    }
}
