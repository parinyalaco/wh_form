<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\WhActType;
use SimpleXLSX;

class WhActTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $WhAct_type = WhActType::where('status','Active')->get();
        return view('setting.WhAct_type.index',compact('WhAct_type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('setting.WhAct_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        WhActType::create($requestData);

        return redirect('setting/WhAct_type')->with('success', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $WhAct_type = WhActType::findOrFail($id);
        return view('setting.WhAct_type.view', compact('WhAct_type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $WhAct_type = WhActType::findOrFail($id);
        return view('setting.WhAct_type.edit', compact('WhAct_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();
        $WhAct_type = WhActType::findOrFail($id);
        // dd($WhAct_type->code);
        if($requestData['code']==$WhAct_type->code && $requestData['name']==$WhAct_type->name && $requestData['desc']==$WhAct_type->desc){
            return redirect('setting/WhAct_type');
        }else{
            $set_status['status'] = 'Inactive';        
            $WhAct_type->update($set_status);
            
            WhActType::create($requestData); 
            return redirect('setting/WhAct_type')->with('success', ' updated!');
        }   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $set_status['status'] = 'Inactive';
        $WhAct_type = WhActType::findOrFail($id);
        $WhAct_type->update($set_status);
        return redirect('setting/WhAct_type')->with('success', ' deleted!');
    }

    public function importExportView()
    {
        return view('setting.WhAct_type.import');
    }
   
    public function import(Request $request) 
    {
        $requestData = $request->all();
        $completedirectory = 'storage/app/public/';  

        if ($request->hasFile('file_upload')) {
            // $tmpfolder = md5(time());
            $tmpfolder = date('Ymd');
            if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
            }  

            $zipfile = $request->file('file_upload');
            $uploadname = $zipfile->getClientOriginalName();
            $name = md5($zipfile->getClientOriginalName() . time()) . '-upload.' . $zipfile->getClientOriginalExtension();
            $destinationPath = public_path($completedirectory . $tmpfolder);
            $zipfile->move($destinationPath, $name);

            $uploadpath = $completedirectory . "/" . $tmpfolder  . "/" . $name;
            $uploadfile = $destinationPath  . "/" . $name;              
            
            $chk_file = array();
            $chk_row = array();
            if ($xlsx = SimpleXLSX::parse($uploadfile)) {
                // dd(count($xlsx->rows()[0]));
                if(count($xlsx->rows()[0])==3){                  
                    $set_status['status'] = 'Inactive';
                    foreach ($xlsx->rows() as $r => $row) {                                                                         
                        if ($r > 0) {  
                            if(trim($row[0])=='' || trim($row[1])=='') {
                                if(trim($row[0])==''){
                                    // return back()->with('error','บรรทัดที่ '.$r.' คอลัมน์ code เป็นค่าว่าง');
                                    $chk_file['code'][$r] = 'คอลัมน์ code เป็นค่าว่าง';                                
                                }
                                if(trim($row[1])==''){
                                    // return back()->with('error','บรรทัดที่ '.$r.' คอลัมน์ name เป็นค่าว่าง');   
                                    $chk_file['name'][$r] = 'คอลัมน์ name เป็นค่าว่าง';  
                                }
                            }else{
                                if(!empty($chk_row[trim($row[0])])) $chk_file['duplicate'][$r] = 'คอลัมน์ code มีค่าซ้ำ';  
                                else    $chk_row[trim($row[0])] = trim($row[1]).'++'.trim($row[2]);    
                            } 
                        }
                    }
                }else{
                    $chk_file['all'][0] = 'จำนวนคอลัมน์เกินกว่าที่กำหนด';
                }  
                // dd($chk_file);
                if(!empty($chk_file)){
                    return view('setting.WhAct_type.not_match',compact('chk_file'));                    
                }else{
                    foreach ($chk_row as $key => $value) {
                        $exp = explode('++',$value);
                        $to_save = array();
                        $to_save['code'] = $key;
                        $to_save['name'] = $exp[0]; 
                        $to_save['desc'] = $exp[1];
                        // print_r($to_save);
                        // echo '</br>';
                        $chk_wh = WhActType::where('status', 'Active')->where('code', 'like', $to_save['code'])->count();
                        // echo '---->>'.$chk_wh.'</br>';
                        // dd($chk_wh);
                        if($chk_wh==0){
                            WhActType::create($to_save);
                        }else{
                            WhActType::where('code','like', $to_save['code'])->update($to_save);
                        }
                    }                    
                }
                // dd('');
                return back()->with('success','Updated successfully');  
            } else {
                echo SimpleXLSX::parseError();
            }   
        } 
        
    }
}
