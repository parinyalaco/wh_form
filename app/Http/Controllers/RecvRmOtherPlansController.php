<?php

namespace App\Http\Controllers;

use App\Models\RecvRmOtherPlanMain;
use Illuminate\Http\Request;

class RecvRmOtherPlansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recvrmotherplans = RecvRmOtherPlanMain::paginate(25);
        return view('recvrmotherplans.index', compact('recvrmotherplans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('recvrmotherplans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        RecvRmOtherPlanMain::create($requestData);


        return redirect('recvrmotherplans');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $recvrmotherplan = RecvRmOtherPlanMain::findOrFail($id);

        return view('recvrmotherplans.show', compact('recvrmotherplan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $recvrmotherplan = RecvRmOtherPlanMain::findOrFail($id);

        return view('recvrmotherplans.edit', compact('recvrmotherplan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $recvrmotherplan = RecvRmOtherPlanMain::findOrFail($id);
        $recvrmotherplan->update($requestData);

        return redirect('recvrmotherplans');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changeActive($id)
    {
        $models = RecvRmOtherPlanMain::all();
        $models->update(['status' => 'Inactive']);

        $recvrmotherplan = RecvRmOtherPlanMain::findOrFail($id);
        $recvrmotherplan->status = 'Active';
        $recvrmotherplan->update();
        return redirect('recvrmotherplans');
    }
}
