<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\StProduct;
use App\Models\StWhBegin;
use App\Models\RmBroker;
use App\Models\StBroker;
use App\Models\StReceive;
use App\Models\StWithdraw;
use App\Models\StReturn;
use App\Models\StChange;

class StReceiveController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    
    public function stock()
    {
        $st_wh = array();
        $sum_wh = array();        
        $st_wh = StWhBegin::join('st_products', 'st_wh_begins.st_product_id', '=', 'st_products.id')
            ->where('st_wh_begins.status',1)
            ->select('st_wh_begins.id', 'st_wh_begins.st_product_id', 'st_products.name', 'st_wh_begins.amount', 'st_wh_begins.start_date')
            ->orderBy('st_products.name')->get();
        if(count($st_wh)>0){
            foreach ($st_wh as $key) {
                $sum_wh[$key->name]['pd'] = $key->st_product_id;
                $sum_wh[$key->name]['st'] = $key->amount;
                $sum_wh[$key->name]['rc'] = StReceive::where('st_product_id', $key->st_product_id)->where('status', 1)->where('receive_date', '>=', $key->start_date)->sum('amount');
                $sum_wh[$key->name]['wd'] = StWithdraw::where('st_product_id', $key->st_product_id)->where('status', 1)->where('withdraw_date', '>=', $key->start_date)->sum('amount');
                $sum_wh[$key->name]['rt'] = StReturn::where('st_product_id', $key->st_product_id)->where('status', 1)->where('return_date', '>=', $key->start_date)->sum('amount');
                $sum_wh[$key->name]['ch'] = StChange::where('st_product_id', $key->st_product_id)->where('status', 1)->where('change_date', '>=', $key->start_date)->sum('amount');
            }
            foreach ($sum_wh as $key=>$value) {
                // echo 'tt = st->'.$sum_wh[$key]['st'] .' + = rc->'. $sum_wh[$key]['rc'] .' + = rt->'. $sum_wh[$key]['rt'] .' - = wd->'. $sum_wh[$key]['wd'] .' - = ch->'. $sum_wh[$key]['ch'].'</br>';
                $sum_wh[$key]['tt'] = $sum_wh[$key]['st'] + $sum_wh[$key]['rc'] + $sum_wh[$key]['rt'] - $sum_wh[$key]['wd'] - $sum_wh[$key]['ch'];
            }
        }
        
        return view('st_wh.main.stock',compact('sum_wh'));
    }

    public function show_pd($id)
    {
        $st_wh = StWhBegin::join('st_products', 'st_wh_begins.st_product_id', '=', 'st_products.id')
            ->where('st_wh_begins.st_product_id', $id)->where('st_wh_begins.status',1)
            ->select('st_wh_begins.id', 'st_wh_begins.st_product_id', 'st_products.name', 'st_wh_begins.start_date', 'st_wh_begins.amount')
            ->get();
        return [$st_wh];
    }

    public function index($type, $id)
    {
        $tran['type'] = $type;
        $show_pd = $this->show_pd($id);
        $st_wh  = $show_pd[0];
        // dd($st_wh[0]['st_product_id']);
        // $st_wh->st_product_id;

        $st_receive = array();
        if($type == 'receive'){
            $tran['type_show'] = 'รับเข้า';
            $st_receive = StReceive::where('st_product_id', $st_wh[0]['st_product_id'])->where('receive_date', '>=', $st_wh[0]['start_date'])->where('status', 1)
            ->select('id', 'no_id', 'st_product_id', 'receive_date AS tran_date', 'amount')->orderBy('receive_date')->get();
            
        }elseif($type == 'change'){
            $tran['type_show'] = 'ชำรุด';
            $st_receive = StChange::where('st_product_id', $st_wh[0]['st_product_id'])->where('change_date', '>=', $st_wh[0]['start_date'])->where('status', 1)
            ->select('id', 'no_id', 'st_product_id', 'change_date AS tran_date', 'amount')->orderBy('change_date')->get();

        }
        
        return view('st_wh.st_receive.index',compact('tran','st_receive','st_wh'));
    }

    public function show($id)
    {   
        $show_pd = $this->show_pd($id);
        $st_wh  = $show_pd[0];
        // dd($st_wh);
        $to_show = array();
        $to_show_set = array();
        $bk_rc = StReceive::where('st_product_id', $st_wh[0]['st_product_id'])
            ->where('status',1)->where('receive_date', '>=', $st_wh[0]['start_date'])
            ->selectRaw('receive_date as tran_date, sum(amount) as amount')
            ->groupBy('receive_date')
            // ->orderBy('receive_date', 'DESC')
            ->get(); 
        foreach ($bk_rc as $key) {
            $to_show_set[$key->tran_date]['rc'] = $key->amount;
        }
        $bk_ch = StChange::where('st_product_id', $st_wh[0]['st_product_id'])
            ->where('status',1)->where('change_date', '>=', $st_wh[0]['start_date'])
            ->selectRaw('change_date as tran_date, sum(amount) as amount')
            ->groupBy('change_date')
            // ->orderBy('change_date', 'DESC')
            ->get(); 
        foreach ($bk_ch as $key) {
            $to_show_set[$key->tran_date]['ch'] = $key->amount;
        }
        $bk_wd = StWithdraw::where('st_product_id', $st_wh[0]['st_product_id'])
            ->where('status',1)->where('withdraw_date', '>=', $st_wh[0]['start_date'])
            ->selectRaw('withdraw_date as tran_date, sum(amount) as amount')
            ->groupBy('withdraw_date')
            // ->orderBy('withdraw_date', 'DESC')
            ->get(); 
        foreach ($bk_wd as $key) {
            $to_show_set[$key->tran_date]['wd'] = $key->amount;
        }
        $bk_rt = StReturn::where('st_product_id', $st_wh[0]['st_product_id'])
            ->where('status',1)->where('return_date', '>=', $st_wh[0]['start_date'])
            ->selectRaw('return_date as tran_date, sum(amount) as amount')
            ->groupBy('return_date')
            // ->orderBy('return_date', 'DESC')
            ->get(); 
        foreach ($bk_rt as $key) {
            $to_show_set[$key->tran_date]['rt'] = $key->amount;
        }
        // dd($to_show_set);
        if(count($to_show_set)>0){
            $max_array = strtotime( max(array_keys($to_show_set)));
            $min_array = strtotime( min(array_keys($to_show_set)));

            for ( $i = $min_array; $i <= $max_array; $i = $i + 86400 ) {
                $thisDate = date( 'Y-m-d', $i ); 
                if(!empty($to_show_set[$thisDate]['rc']))  $to_show[$thisDate]['rc'] = $to_show_set[$thisDate]['rc'];
                if(!empty($to_show_set[$thisDate]['ch']))  $to_show[$thisDate]['ch'] = $to_show_set[$thisDate]['ch'];
                if(!empty($to_show_set[$thisDate]['wd']))  $to_show[$thisDate]['wd'] = $to_show_set[$thisDate]['wd'];
                if(!empty($to_show_set[$thisDate]['rt']))  $to_show[$thisDate]['rt'] = $to_show_set[$thisDate]['rt'];
            }
        }
        return view('st_wh.st_receive.show',compact('st_wh','to_show'));
        
        // dd($to_show);

        
    }

    public function create($type, $id)
    {
        $tran['type'] = $type;
        $show_pd = $this->show_pd($id);
        $st_wh  = $show_pd[0];

        if($type == 'receive'){
            $tran['type_show'] = 'รับเข้า';
            
        }elseif($type == 'change'){
            $tran['type_show'] = 'ชำรุด';

        }

        return view('st_wh.st_receive.create',compact('tran','st_wh'));
    }

    public function store(Request $request, $type)
    {
        $tran['type'] = $type;
        $requestData = $request->all();
        $show_pd = $this->show_pd($requestData['st_product_id']);
        $st_wh  = $show_pd[0];

        if($type == 'receive'){
            $tran['type_show'] = 'รับเข้า';
            $requestData['receive_date'] = $requestData['tran_date'];
            // dd($requestData);
            StReceive::create($requestData);
            
        }elseif($type == 'change'){
            $tran['type_show'] = 'ชำรุด';    
            $requestData['change_date'] = $requestData['tran_date'];
            // dd($requestData);
            StChange::create($requestData);

        }        
    
        return redirect('/st_wh/st_receive/index/'.$tran['type'].'/'.$requestData['st_product_id'])->with('success', ' added!');
    }

    public function edit($type, $id)
    {
        $tran['type'] = $type;
        if($type == 'receive'){
            $tran['type_show'] = 'รับเข้า';
            $st_rc = StReceive::findOrFail($id);
            $st_rc->tran_date =  $st_rc->receive_date;
            
        }elseif($type == 'change'){
            $tran['type_show'] = 'ชำรุด';
            $st_rc = StChange::findOrFail($id);
            $st_rc->tran_date =  $st_rc->change_date;

        }

        $show_pd = $this->show_pd($st_rc->st_product_id);
        $st_wh  = $show_pd[0];
        
        return view('st_wh.st_receive.edit',compact('tran', 'st_wh', 'st_rc'));
    }

    public function update(Request $request, $type, $id)
    {        
        $requestData = $request->all();
        $tran['type'] = $type;
        if($type == 'receive'){
            $tran['type_show'] = 'รับเข้า';
            $st_rc = StReceive::findOrFail($id);
            $requestData['receive_date'] = $requestData['tran_date'];
            
        }elseif($type == 'change'){
            $tran['type_show'] = 'ชำรุด';
            $st_rc = StChange::findOrFail($id);
            $requestData['change_date'] = $requestData['tran_date'];

        }

        $st_rc->update($requestData);

        return redirect('/st_wh/st_receive/index/'.$tran['type'].'/'.$requestData['st_product_id'])->with('success', ' updated!');
    }
    
    public function destroy($type, $id)
    {
        $st_status['status'] = 0;
        $tran['type'] = $type;
        if($type == 'receive'){
            $tran['type_show'] = 'รับเข้า';
            $st_rc = StReceive::findOrFail($id);
            
        }elseif($type == 'change'){
            $tran['type_show'] = 'ชำรุด';
            $st_rc = StChange::findOrFail($id);

        }

        $st_rc->update($st_status);

        return redirect('/st_wh/st_receive/index/'.$tran['type'].'/'.$st_rc->st_product_id)->with('success', ' deleted!');
    }

}
