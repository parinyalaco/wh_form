<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserType;
use App\Http\Requests\RegisterRequest;

class RegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    
    public function index()
    {
        $query = array();
        $main_import = array();
        $material = array();
        $vendor = array();
        $query = User::join('user_types', 'users.user_type_id', '=', 'user_types.id')
            ->select('users.id', 'users.name', 'users.username', 'users.email', 'user_types.name AS type_name')->get();        

        return view('auth.index',compact('query'));
    }

    public function create()
    {
        $type = UserType::get();
        return view('auth.register',compact('type'));
    }

    public function store(Request $request)
    {
        $requestData = $request->all();
        // dd($requestData);
        $request->validate([
            'name'=>'required|max:255',
            'username'=>'required|max:255|unique:users',
            'email'=>'required|email|unique:users,email',    
            'password' => 'required|same:password_confirmation|min:4', 
            'password_confirmation' => 'required',     
        ],
        [
            'name.required'=>"กรุณาระบุชื่อพนักงานด้วยค่ะ",
            'name.max'=>"ระบุข้อมูลเกิน 255 ตัวอักษร",
            'username.required'=>"กรุณาระบุ Username ด้วยค่ะ",
            'username.max'=>"ระบุข้อมูลเกิน 255 ตัวอักษร",            
            'username.unique'=>"Username นี้ มีในระบบแล้ว",
            'email.required'=>"กรุณาระบุ email ด้วยค่ะ",
            'email.email'=>"ข้อมูลไม่เป็นรูปแบบ email",   
            'email.unique'=>"email นี้ มีในระบบแล้ว",          
            'password.required'=>"กรุณาระบุรหัสผ่านใหม่",
            'password.same'=>"รหัสผ่านใหม่ และ ยืนยันรหัสผ่าน ไม่ตรงกัน",        
            'password_confirmation.required'=>"กรุณายืนยันรหัสผ่านใหม่",
        ]);
        
        // dd($requestData);
        $update_to['_token'] = $requestData['_token'];
        $update_to['name'] = $requestData['name'];        
        $update_to['username'] = $requestData['username'];
        $update_to['email'] = $requestData['email'];        
        $update_to['user_type_id'] = $requestData['user_type_id'];
        $update_to['password'] = $requestData['password'];
        // dd($update_to);
        $user = User::create($update_to);

        return redirect()->route('user.index')->with('success', 'User updated!');
    }

    public function register(RegisterRequest $request) //store
    {
        $user = User::create($request->validated());

        auth()->login($user);

        return redirect()->route('user.index')->with('success', "Account successfully registered.");
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $type = UserType::get();
        return view('auth.edit',compact('user','type'));
    }

    public function update(Request $request, $id)
    {
        $requestData = $request->all();
        // dd($requestData);
        $request->validate([
            'name'=>'required|max:255',
            'username'=>'required|max:255',
            'email'=>'required|email',    
            'password' => 'required|same:password_confirmation|min:4', 
            'password_confirmation' => 'required',     
        ],
        [
            'name.required'=>"กรุณาระบุชื่อพนักงานด้วยค่ะ",
            'name.max'=>"ระบุข้อมูลเกิน 255 ตัวอักษร",
            'username.required'=>"กรุณาระบุ Username ด้วยค่ะ",
            'username.max'=>"ระบุข้อมูลเกิน 255 ตัวอักษร",
            'email.required'=>"กรุณาระบุ email ด้วยค่ะ",
            'email.email'=>"ข้อมูลไม่เป็นรูปแบบ email",            
            'password.required'=>"กรุณาระบุรหัสผ่านใหม่",
            'password.same'=>"รหัสผ่านใหม่ และ ยืนยันรหัสผ่าน ไม่ตรงกัน",        
            'password_confirmation.required'=>"กรุณายืนยันรหัสผ่านใหม่",
        ]);
        // dd($requestData);
        $update_to['_token'] = $requestData['_token'];
        $update_to['_method'] = $requestData['_method'];
        $update_to['name'] = $requestData['name'];
        $update_to['username'] = $requestData['username'];
        $update_to['email'] = $requestData['email'];        
        $update_to['user_type_id'] = $requestData['user_type_id'];
        $update_to['password'] = $requestData['password'];
        // dd($update_to);
        $user = User::findOrFail($id);
        $user->update($update_to);

        return redirect()->route('user.index')->with('success', 'User updated!');
    }
    
    public function destroy($id)
    {
        $user = User::findOrFail($id);        
        $user->delete();
        return redirect()->route('user.index')->with('success', 'User delete!');
    }

    public function show()
    {
        return view('auth.index');
    }

}
