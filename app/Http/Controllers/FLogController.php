<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FLog;
use App\Models\Forklift;
use App\Models\Fperson;
use App\Models\Flocation;
use App\Models\FlArea;
use App\Models\FlBroken;
use Carbon\Carbon;
use SimpleXLSX;

use App\Mail\MannualMailFl;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;
use DB;
use Auth;

class FLogController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fl = new FLog;
        if(!empty($request->get('date_from'))){
            $date_from = $request->get('date_from');
            if(!empty($request->get('date_to'))){                
                $date_to = $request->get('date_to'); 
            }else{
                $date_to = Carbon::now(); 
            }          
        }else{
            $chk = FLog::orderBy('log_date','desc')->limit(1)->select('log_date')->get();
            if(count($chk)==0){
                $date_from = Carbon::now();
                $date_to = Carbon::now();
            }else{
                $date_to = $chk[0]['log_date'];
                $date_to_c = Carbon::createFromFormat('Y-m-d', $date_to);
                $date_from_c = $date_to_c->addDays(-2);
                $date_from = date("Y-m-d", strtotime($date_from_c));
            }            
        }
        $fl = $fl->whereBetween('log_date', [$date_from, $date_to]);
        if(!empty($request->get('fl_id'))){
            $fl_id =$request->get('fl_id');
            $fl = $fl->where('forklift_id', $fl_id);
        }else{
            $fl_id = "";
        }
        $fl = $fl->orderBy('log_date','desc')->get();

        $val_save = $this->set_save();
        $to_fl = $val_save[0];
        $to_p = $val_save[1];
        $to_a = $val_save[2];
        $to_b = $val_save[3];
        $to_locate = $val_save[4];
        $log_type = $val_save[5];


        $fl_list = array();
        $count_row = array();
        $set_fl = array();
        foreach($fl as $key){ 
            if(empty($set_fl) || $set_fl <> $key->forklift_id){
                $set_fl = $key->forklift_id;
                $time_before =0;
                $test = DB::select("EXEC GetTime_FL_2 ".$key->forklift_id.",'".$key->log_date."'");
                // dd($test[0]->time);
                if(!empty($test[0]->time)){  
                    $fl_list[$key->log_date][$key->f_location_id][$key->forklift_id][$key->shift]['time_before'][] = $test[0]->time;
                }else{
                    $fl_list[$key->log_date][$key->f_location_id][$key->forklift_id][$key->shift]['time_before'][] = 0;
                }
            }else{
                $fl_list[$key->log_date][$key->f_location_id][$key->forklift_id][$key->shift]['time_before'][] = $time_before;
            }
            if(!empty($count_row['date'][$key->log_date])){
                $count_row['date'][$key->log_date] += 1;          
            }else{
                $count_row['date'][$key->log_date] = 1;
            }
            if(!empty($count_row['locate'][$key->log_date][$key->f_location_id])){
                $count_row['locate'][$key->log_date][$key->f_location_id] += 1;          
            }else{
                $count_row['locate'][$key->log_date][$key->f_location_id] = 1;
            }
            if(!empty($count_row['fl'][$key->log_date][$key->f_location_id][$key->forklift_id])){
                $count_row['fl'][$key->log_date][$key->f_location_id][$key->forklift_id] += 1;          
            }else{
                $count_row['fl'][$key->log_date][$key->f_location_id][$key->forklift_id] = 1;
            }
            $fl_list[$key->log_date][$key->f_location_id][$key->forklift_id][$key->shift]['id'][] = $key->id; 
            // $fl_list[$key->log_date][$key->shift][$key->forklift_id]['f_location_id'] = $key->f_location_id; 
            if(!empty($key->fperson_id))   
                $fl_list[$key->log_date][$key->f_location_id][$key->forklift_id][$key->shift]['fperson_id'][] = $to_p['id'][$key->fperson_id]; 
            else
                $fl_list[$key->log_date][$key->f_location_id][$key->forklift_id][$key->shift]['fperson_id'][] = ''; 
            // $fl_list[$key->log_date][$key->f_location_id][$key->forklift_id][$key->shift]['fperson_id'][] = $to_p['id'][$key->fperson_id]; 
            $fl_list[$key->log_date][$key->f_location_id][$key->forklift_id][$key->shift]['type'][] =  $log_type[strtoupper($key->type)]; 
            $fl_list[$key->log_date][$key->f_location_id][$key->forklift_id][$key->shift]['time'][] = $key->time;
            if(empty($key->time))   $time_before = $test[0]->time;
            else    $time_before = $key->time;
            if(!empty($key->fl_area_id))   
                $fl_list[$key->log_date][$key->f_location_id][$key->forklift_id][$key->shift]['area'][] = $to_a['id'][$key->fl_area_id]; 
            else
                $fl_list[$key->log_date][$key->f_location_id][$key->forklift_id][$key->shift]['area'][] = '';
            if(!empty($key->fl_broken_id))   
                $fl_list[$key->log_date][$key->f_location_id][$key->forklift_id][$key->shift]['broken'][] = $to_b['id'][$key->fl_broken_id];
            else
                $fl_list[$key->log_date][$key->f_location_id][$key->forklift_id][$key->shift]['broken'][] = ''; 
            $fl_list[$key->log_date][$key->f_location_id][$key->forklift_id][$key->shift]['note'][] = $key->note; 
        }  
        // dd($count_row['fl']); 
        return view('FL.index',compact('fl_list','date_from','date_to','fl_id','to_locate','to_fl','count_row'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {      
        $f_create = array();  
        $to_time = $this->get_time($f_create);
        $get_log = $to_time[0];
        $locate = $to_time[1];
        $fkl = $to_time[2];

        return view('FL.create',compact('locate','fkl','get_log'));
    }

    public function selectfl(Request $request)
    {
        $query = $request->get('q');
        $result = Forklift::where('name', 'LIKE', '%'. $query. '%')->where('status', 'Active')->get();
        return response()->json($result);
    }

    public function cmb_area(Request $request)
    {
          $query = $request->get('query');
          $filterResult = FlArea::where('name', 'LIKE', '%'. $query. '%')->get();
          return response()->json($filterResult);
    }

    public function cmb_person(Request $request)
    {
        $ps = $request->get('query');
        $filterResult = Fperson::selectRaw("id, concat(fname,' ', lname) as name")->where(function ($query) use ($ps)  {
                            $query->where('fname', 'LIKE', '%'. $ps. '%')
                                    ->orWhere('lname', 'LIKE', '%'. $ps. '%');
                        })->where('status', 'Active')->get();
        return response()->json($filterResult);
    }

    public function cmb_broken(Request $request)
    {
        $query = $request->get('query');
        $filterResult = FlBroken::where('name', 'LIKE', '%'. $query. '%')->get();
        return response()->json($filterResult);
    }


    public function get_time($fl_log)
    {
        $fkl = Forklift::where('status','Active')->get();
        foreach ($fkl as $key) {
            $fl[] = $key->id;
            $update_fl['id'][$key->id] = $key->updated_at;
            $update_fl['time'][$key->id] = $key->time;            
        }

        if(empty($fl_log)){
            $loc_id = FLog::selectRaw('forklift_id, f_location_id, time, updated_at')->whereIn('forklift_id', $fl)->orderBy('id','desc')->get();
        }else{
            $loc_id = FLog::selectRaw('forklift_id, f_location_id, time, updated_at')->whereIn('forklift_id', $fl);
            if($fl_log->shift == 'B')
                $loc_id = $loc_id->where('log_date', '<', [$fl_log->log_date]);
            else
                $loc_id = $loc_id->where('log_date', '<=', [$fl_log->log_date])->whereNotIn('id', [$fl_log->id]);
            $loc_id = $loc_id->orderBy('id','desc')->get();
        }
        foreach ($loc_id as $key) {
            if(empty($get_log['locate'][$key->forklift_id]))
                $get_log['locate'][$key->forklift_id] = $key->f_location_id;

            if(empty($time_log['time'][$key->forklift_id]) && !empty($key->time)){
                $time_log['time'][$key->forklift_id] = $key->time;
                $time_log['up'][$key->forklift_id] = $key->updated_at;
            }
        }

        foreach ($get_log['locate'] as $key=>$value) {
            if(empty($time_log['time'][$key]) || $update_fl['id'][$key] > $time_log['up'][$key])   
                $get_log['time'][$key] = $update_fl['time'][$key];
            else
                $get_log['time'][$key] = $time_log['time'][$key];
        }
        
        $locate = Flocation::where('status','Active')->get();
        $fkl = Forklift::where('status','Active')->get();
        return [$get_log, $locate, $fkl] ; 
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {     
        $val_save = $this->set_save();
        $to_fl = $val_save[0];
        $to_p = $val_save[1];
        $to_a = $val_save[2];
        $to_b = $val_save[3];

        $requestData = $request->all();
        // dd($requestData);
        $log_chk = FLog::where('log_date',$requestData['log_date'])->where('forklift_id',$requestData['forklift_id'])->where('shift',$requestData['shift'])->count();
        if($log_chk==0){
            if($requestData['type']=='R'){  //ใช้งาน
                $requestData['fl_broken_id'] = null;

                $req_area = trim($requestData['fl_area_name']);
                if(empty($to_a['name'][$req_area])){
                    $save_det['name'] = $req_area;
                    $area_id = FlArea::create($save_det)->id; 
                    $requestData['fl_area_id'] = $area_id;
                }else{
                    $requestData['fl_area_id'] = $to_a['name'][$req_area];
                } 
                $req_person = trim($requestData['fperson_name']);
                if(empty($to_p['name'][$req_person])){
                    $re_name = explode(' ',$req_person);
                    $save_p['fname'] = $re_name[0];
                    if(!empty($re_name[1])){
                        $save_p['lname'] = $re_name[1];
                    }else{
                        $save_p['lname'] = '-';
                    }
                    $id_p = Fperson::create($save_p)->id;
                    $requestData['fperson_id'] = $id_p;
                }else{
                    $requestData['fperson_id'] = $to_p['name'][$req_person];
                } 
                $req_broken = trim($requestData['fl_broken_name']);
                if(empty($to_b['name'][$req_broken])){
                    $save_det['name'] = $req_broken;
                    $broken_id = FlBroken::create($save_det)->id; 
                    $requestData['fl_broken_id'] = $broken_id;
                }else{
                    $requestData['fl_broken_id'] = $to_b['name'][$req_broken];
                } 
            }elseif ($requestData['type']=='S') {   //ไม่ใช้งาน                    
                $requestData['fl_area_id'] = null;   
                $requestData['fperson_id'] = null;
                $requestData['time'] = null;
                $requestData['fl_broken_id'] = null;
            }else { //รถเสีย
                $requestData['fl_area_id'] = null;    
                $requestData['fperson_id'] = null;
                $requestData['time'] = null;

                $req_broken = trim($requestData['fl_broken_name']);
                if(empty($to_b['name'][$req_broken])){
                    $save_det['name'] = $req_broken;
                    $broken_id = FlBroken::create($save_det)->id; 
                    $requestData['fl_broken_id'] = $broken_id;
                }else{
                    $requestData['fl_broken_id'] = $to_b['name'][$req_broken];
                } 
            }        
            $requestData['user_id'] = Auth::user()->id;  
            // dd($requestData); 
            $get_id = FLog::create($requestData)->id;  

            $fl_pic = FLog::findOrFail($get_id); 
            $completedirectory = config('myconfig.directory.fl.pic').$get_id.'/'; 
            if (!is_dir($completedirectory)) {
                mkdir($completedirectory, 0777, true);
            } 
            // $pic_name = array('รูปด้านหน้ารถ', 'รูปด้านข้าง(ซ้าย)', 'รูปด้านข้าง(ขวา)', 'รูปด้านหลัง', 'รูปแบตเตอรี่', 'รูปหน้าจอ');
            $pic_field = array('head_img', 'left_img', 'right_img', 'back_img', 'batt_img', 'monitor_img');
            $pic_path = array('head_img_path', 'left_img_path', 'right_img_path', 'back_img_path', 'batt_img_path', 'monitor_img_path');
            $save_img = array();

            for($i=0;$i<6;$i++){
                if ($request->hasFile('show_'.$i)) {
                    $image = $request->file('show_'.$i);
                    $img_type = $image->extension();
                    $fileName = uniqid() . '.'.$img_type;                        

                    $width=500; //*** Fix Width & Heigh (Autu caculate) ***//
                    $size=GetimageSize($image);
                    $height=round($width*$size[1]/$size[0]);
                    if($img_type == 'jpg')
                        $images_orig = ImageCreateFromJPEG($image);
                    elseif($img_type == 'png')
                        $images_orig = ImageCreateFromPNG($image);
                    $photoX = ImagesX($images_orig);
                    $photoY = ImagesY($images_orig);
                    $images_fin = ImageCreateTrueColor($width, $height);
                    ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
                    ImageJPEG($images_fin,$completedirectory.$fileName);
                    ImageDestroy($images_orig);
                    ImageDestroy($images_fin);

                    $save_img[$pic_field[$i]] = $fileName;
                    $save_img[$pic_path[$i]] = $completedirectory . $fileName;
                }
            }
            $fl_pic->update($save_img);
            return redirect('FL')->with('success', ' added!');
        }else{
            return redirect()->back()->with('error', 'ในวันที่ '.$requestData['log_date'].' รถโฟล์คลิฟท์ '.$to_fl[$requestData['forklift_id']].' กะ '.$requestData['shift'].' มีการใช้งานแล้ว !');
        }
        
    }

    public function set_save()
    {
        $to_fl = array();
        $to_p = array();
        $to_a = array();
        $to_b = array();
        $to_locate = array();

        $fkl = Forklift::all();
        if(count($fkl)>0){
            foreach ($fkl as $key) {
                $to_fl[$key->id] = $key->name;
            }
        }

        $fkl = Fperson::all();
        if(count($fkl)>0){
            foreach ($fkl as $key) {
                $to_p['id'][$key->id] = $key->fname.' '.$key->lname;
                $to_p['name'][$key->fname.' '.$key->lname] = $key->id;
            }
        }

        $fkl = FlArea::all();
        if(count($fkl)>0){
            foreach ($fkl as $key) {
                $to_a['id'][$key->id] = $key->name;
                $to_a['name'][$key->name] = $key->id;
            }
        }

        $fkl = FlBroken::all();
        if(count($fkl)>0){
            foreach ($fkl as $key) {
                $to_b['id'][$key->id] = $key->name;
                $to_b['name'][$key->name] = $key->id;
            }
        }

        $locate = Flocation::all();
        if(count($locate)>0){
            foreach ($locate as $key) {
                $to_locate[$key->id] = $key->name;
            }
        }        
        $log_type = array ("R"=>"ใช้งาน", "S"=>"ไม่ใช้งาน", "A"=>"รถเสีย" );
        return [$to_fl,$to_p,$to_a,$to_b,$to_locate,$log_type];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fl_log = FLog::findOrFail($id);
        $fkl_show = Forklift::where('id',$fl_log->forklift_id)->get();
        $pp = Fperson::selectRaw("concat(fname,' ', lname) as name")->where('id',$fl_log->fperson_id)->get();
        $fa = FlArea::select('name')->where('id',$fl_log->fl_area_id)->get();
        $fb = FlBroken::select('name')->where('id',$fl_log->fl_broken_id)->get();

        $to_time = $this->get_time($fl_log);
        $get_log = $to_time[0];
        $locate = $to_time[1];
        $fkl = $to_time[2];
        // dd($get_log);
        return view('FL.edit',compact('fl_log','locate','fkl','get_log','pp','fa','fb','fkl_show'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $val_save = $this->set_save();
        $to_fl = $val_save[0];
        $to_p = $val_save[1];
        $to_a = $val_save[2];
        $to_b = $val_save[3];
        // dd($to_b);

        $requestData = $request->all();
        $log_chk = FLog::where('log_date',$requestData['log_date'])->where('forklift_id',$requestData['forklift_id'])->where('shift',$requestData['shift'])->whereNotIn('id', [$id])->count();
        if($log_chk==0){
            $fl_log = FLog::findOrFail($id); 
            if($requestData['type']=='R'){  //ใช้งาน
                // $requestData['fl_broken_id'] = null;
                $req_area = trim($requestData['fl_area_name']);
                if(empty($to_a['name'][$req_area])){
                    $save_det['name'] = $req_area;
                    $area_id = FlArea::create($save_det)->id; 
                    $requestData['fl_area_id'] = $area_id;
                }else{
                    $requestData['fl_area_id'] = $to_a['name'][$req_area];
                } 

                $req_p = trim($requestData['fperson_name']);  
                $re_name = explode(' ',$req_p);
                if(empty($re_name[1])){
                    $re_name[1] = '-';
                }                
                $req_person = $re_name[0].' '.$re_name[1];             
                if(empty($to_p['name'][$req_person])){
                    $re_name = explode(' ',$req_person);
                    $save_p['fname'] = $re_name[0];
                    if(!empty($re_name[1])){
                        $save_p['lname'] = $re_name[1];
                    }else{
                        $save_p['lname'] = '-';
                    }
                    $id_p = Fperson::create($save_p)->id;
                    $requestData['fperson_id'] = $id_p;
                }else{
                    $requestData['fperson_id'] = $to_p['name'][$req_person];
                } 
                
                $req_broker = trim($requestData['fl_broken_name']);
                if(!empty($req_broker)){ 
                    if(empty($to_b['name'][$req_broker])){
                        $save_det['name'] = $req_broker;
                        $area_id = FlBroken::create($save_det)->id; 
                        $requestData['fl_broken_id'] = $area_id;    
                    }else{
                        $requestData['fl_broken_id'] = $to_b['name'][$req_broker];
                    }                
                }else{
                    $requestData['fl_broken_id'] = null;
                } 
            }elseif ($requestData['type']=='S') {   //ไม่ใช้งาน                    
                $requestData['fl_area_id'] = null;   
                $requestData['fperson_id'] = null;
                $requestData['time'] = null;
                $requestData['fl_broken_id'] = null;
            }else { //รถเสีย
                $requestData['fl_area_id'] = null;    
                $requestData['fperson_id'] = null;
                $requestData['time'] = null;
                
                $req_broker = trim($requestData['fl_broken_name']);
                if(!empty($req_broker)){ 
                    if(empty($to_b['name'][$req_broker])){
                        $save_det['name'] = $req_broker;
                        $area_id = FlBroken::create($save_det)->id; 
                        $requestData['fl_broken_id'] = $area_id;    
                    }else{
                        $requestData['fl_broken_id'] = $to_b['name'][$req_broker];
                    }                
                }else{
                    $requestData['fl_broken_id'] = null;
                }             
            }                
                
            $requestData['user_id'] = Auth::user()->id;  

            $pic_name = array('รูปด้านหน้ารถ', 'รูปด้านข้าง(ซ้าย)', 'รูปด้านข้าง(ขวา)', 'รูปด้านหลัง', 'รูปแบตเตอรี่', 'รูปหน้าจอ');
            $pic_field = array('head_img', 'left_img', 'right_img', 'back_img', 'batt_img', 'monitor_img');
            $pic_path = array('head_img_path', 'left_img_path', 'right_img_path', 'back_img_path', 'batt_img_path', 'monitor_img_path');

            $completedirectory = config('myconfig.directory.fl.pic').$id.'/'; 

            for($i=0;$i<6;$i++){
                $pic_val = $pic_field[$i];
                $path_val = $pic_path[$i];
                echo $requestData['img_'.$i].' != '.$fl_log->$pic_val.'</br>';
                if ($requestData['img_'.$i] != $fl_log->$pic_val) {
                    if(!empty($fl_log->$pic_val)){
                        if (file_exists($fl_log->$path_val)) {
                            unlink($fl_log->$path_val);
                        }
                    }
                    if(!empty($requestData['show_'.$i])){
                        $image = $request->file('show_'.$i);
                        $img_type = $image->extension();
                        $fileName = uniqid() . '.'.$img_type;                    

                        $width=500; //*** Fix Width & Heigh (Autu caculate) ***//
                        $size=GetimageSize($image);
                        $height=round($width*$size[1]/$size[0]);
                        if($img_type == 'jpg')
                            $images_orig = ImageCreateFromJPEG($image);
                        elseif($img_type == 'png')
                            $images_orig = ImageCreateFromPNG($image);
                        $photoX = ImagesX($images_orig);
                        $photoY = ImagesY($images_orig);
                        $images_fin = ImageCreateTrueColor($width, $height);
                        ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
                        ImageJPEG($images_fin,$completedirectory.$fileName);
                        ImageDestroy($images_orig);
                        ImageDestroy($images_fin);
                        echo '---'.$fileName.'</br>';
                        $requestData[$pic_val] = $fileName;
                        $requestData[$path_val] = $completedirectory . $fileName;
                    }                
                }
            }
            // dd($requestData);            
            $fl_log->update($requestData);
            return redirect('FL')->with('success', ' updated!');
        }else{
            return redirect()->back()->with('error', 'ในวันที่ '.$requestData['log_date'].' รถโฟล์คลิฟท์ '.$to_fl[$requestData['forklift_id']].' กะ '.$requestData['shift'].' มีการใช้งานแล้ว !');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd('ลบ');
        $dirname = config('myconfig.directory.fl.pic').$id;
        $this->deleteDirectory($dirname);

        FLog::destroy($id);
        return redirect('FL')->with('success', ' deleted!');
    }

    function deleteDirectory($path){
        if(is_dir($path)){
            $files = array_diff(scandir($path), array('.','..')); 
            foreach($files as $file){
                if(is_dir($path.'/'.$file)){
                    $this->deleteDirectory("$path/$file");
                }
                if (file_exists("$path/$file")) {unlink("$path/$file");}
            }
            return rmdir($path);
        }
    } 

    public function destroy_date($date)
    {
        $fl_del = FLog::where('log_date',$date)->get();
        foreach ($fl_del as $key) {
            $dirname = config('myconfig.directory.fl.pic').$key->id;
            $this->deleteDirectory($dirname);
        }
        $pd_input = FLog::where('log_date',$date)->delete();
        return redirect('FL')->with('success', ' deleted!');
    }

    public function importExportView()
    {
        return view('FL.import');
    }
   
    public function import(Request $request) 
    {
        $requestData = $request->all();
        // $completedirectory = 'storage/app/public/banks/FL/'; 
        $completedirectory = config('myconfig.directory.fl.completes'); 

        if ($request->hasFile('file_upload')) {
            // $tmpfolder = md5(time());
            $tmpfolder = date('Ymd');
            if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
            }  

            $zipfile = $request->file('file_upload');
            $uploadname = $zipfile->getClientOriginalName();
            $name = md5($zipfile->getClientOriginalName() . time()) . '-upload.' . $zipfile->getClientOriginalExtension();
            // $destinationPath = public_path($completedirectory . $tmpfolder);
            $destinationPath = $completedirectory . "/" . $tmpfolder;
            $zipfile->move($destinationPath, $name);

            $uploadpath = $completedirectory . "/" . $tmpfolder  . "/" . $name;
            $uploadfile = $destinationPath  . "/" . $name;

            // dd($uploadpath);
            // dd($uploadname);
            // $to_save['name'] = $uploadname;
            // $to_save['file_tosave'] = $uploadpath;       //ข้างหน้าเป็น 'storage/app/public/banks/completes'
              
          
            if ($xlsx = SimpleXLSX::parse($uploadfile)) {
                // dd(count($xlsx->rows()[4]));
                if(count($xlsx->rows()[4])==7){
                    // $file_id = ToImportfile::create($to_save)->id;
                    $query = Forklift::where('status','Active')->get();
                    foreach($query as $key){
                        $fl_data[$key->name] = $key->id;    
                        $date_update[$key->id] = $key->updated_at;   
                        $fltime[$key->id] = $key->time;                 
                        // $fl = FLog::where('forklift_id',$key->id)->where('log_date','<',$key->id)->whereNotNull('time')->orderBy('log_date','desc')->limit(1)->get();
                        // if(count($fl)>0 && $fl[0]['updated_at'] > $key->updated_at){
                        //     $fl_time[$key->name] = number_format($fl[0]['time'],1,'.','');
                        // }else{
                        //     $fl_time[$key->name] = number_format($key->time,1,'.','');
                        // }
                    }
                    // dd($fl_time);
                    $query = Flocation::where('status','Active')->get();
                    foreach($query as $key){
                        $locate_data[$key->name] = $key->id;
                    }

                    $query = Fperson::where('status','Active')->get();
                    foreach($query as $key){
                        $person_data[$key->fname.' '.$key->lname] = $key->id;
                    }
                    // dd($person_data);

                    $query = FLog::get();
                    foreach($query as $key){
                        $fl_input[$key->log_date][$key->forklift_id][$key->shift] = $key->id;
                    }
                    // $set_active['active'] = 0;
                    // dd($fl_input);
                    $chk = array();
                    $tosave = array();
                    $fl_excle = array();
                    //ตรวจเช็คไฟล์---------------------
                    foreach ($xlsx->rows() as $r => $row) {       
                        if ($r == 3 ){
                            // dd($row[1]);
                            if(!empty($row[1])) {                            
                                $chk_date = date('Y-m-d',strtotime($row[1]));
                                $explo_date = explode('-',$chk_date);
                                if($explo_date[0]>date("Y")+1){
                                    $chk['log_date'][$chk_date][$r] = 'ข้อมูลวันที่ไม่ถูกรูปแบบ';
                                }else{
                                    //เก็บเลขไมล์ไว้เทียบ
                                    foreach($fl_data as $key=>$value){                       
                                        $fl = FLog::where('forklift_id',$value)->where('log_date','<',$chk_date)->whereNotNull('time')->orderBy('log_date','desc')->orderBy('shift','desc')->limit(1)->get();
                                        if(count($fl)>0 && $fl[0]['updated_at'] > $date_update[$value]){
                                            $fl_time[$key] = number_format($fl[0]['time'],1,'.','');
                                        }else{
                                            $fl_time[$key] = number_format($fltime[$value],1,'.','');
                                        }
                                    }
                                }
                            }else{
                                $chk['log_date'][$chk_date][$r] = 'ไม่พบข้อมูลวันที่';
                            }
                        }
                    }//ปิด loop เพื่่อให้มีการเก็บค้า fl_time ครั้งเดียวไว้เทียบ
                    // dd($fl_time);
                    foreach ($xlsx->rows() as $r => $row) {                             
                        if ($r > 4 && !empty(trim($row[0]))) { 
                            // if(empty($chk_fl) || $chk_fl<>trim($row[0])){
                            //     $chk_fl = trim($row[0]);
                            // }
                            if(empty($fl_excle[$chk_date][trim($row[0])][trim($row[2])])){
                                $fl_excle[trim($row[0])][trim($row[2])] = 1;                                 
                            }else{
                                $fl_excle[trim($row[0])][trim($row[2])] += 1;
                            }
                            $chk_row[trim($row[0])][trim($row[2])][$r] = $r;   
                            
                            // echo $r.'</br>';
                            $tosave[$r] = $chk_date.'+;'.trim($row[0]).'+;'.trim($row[1]).'+;'.trim($row[2]).'+;'.strtoupper(trim($row[3])).'+;'.trim($row[4]).'+;'.trim($row[5]).'+;'.trim($row[6]);

                            // dd($row[4]);
                            if(empty($fl_data[trim($row[0])])){
                                $chk['forklift_id'][trim($row[0])][$r] = $row[0];
                            }                            
                            if(trim($row[2])<>'B' && trim($row[2])<>'C'){
                                $chk['shift'][trim($row[2])][$r] = trim($row[2]);
                            }
                            $chk_type = strtoupper(trim($row[3]));
                            if($chk_type<>'R' && $chk_type<>'S' && $chk_type<>'A'){
                                $chk['type'][trim($row[3])][$r] = trim($row[4]);
                            }
                            
                            // if(!empty($fl_input[$chk_date][trim($row[0])][trim($row[2])])){
                            //     $chk['id'][$chk_date.' รถ'.trim($row[0]).' กะ'.trim($row[2])][$r] = trim($row[0]);
                            // }
                            // dd($row[6].'--'.number_format($fl_time[trim($row[0])],1,'.',''));
                            if(!empty($row[6]) && !empty($fl_time[trim($row[0])])){
                                if(empty($chk_fl) || $chk_fl<>trim($row[0])){
                                    $chk_fl = trim($row[0]);   
                                    $time_chk = $fl_time[trim($row[0])];
                                }
                                // echo $row[0].'--'.$row[2].'--'.$row[6].' ('.$time_chk.')*****';
                                if($time_chk > $row[6]){
                                    // dd('T');
                                    $chk['time'][trim($row[0]).'('.$time_chk.')'][$r] = $row[6];
                                }else{
                                    // dd('F');
                                    if($row[6]-$time_chk>12){
                                        $chk['time'][trim($row[0]).'('.$time_chk.')'][$r] = $row[6];
                                        if(trim($row[2])=='B'){
                                            $time_chk = $row[6];
                                        }
                                    }                                    
                                } 
                                if(trim($row[2])=='B'){
                                    $time_chk = $row[6];
                                }
                            } 
                            
                            if(trim($row[3])=='R' && empty($row[6])){
                                $chk['chk'][trim($row[0]).' กะ'.trim($row[2]).' สถานะ'.trim($row[3])][$r] = $row[3];
                            }
                            // else{
                            //     if(!empty($fl_data[trim($row[0])])){
                            //         $test = DB::select("EXEC GetTime_FL_2 ".$fl_data[trim($row[0])].",'".$key->log_date."'");
                            //         if(floatval($row[6])>12){
                            //             echo 'time '.trim($row[0]).'('.$time_chk.')'.$r.'=>'.$row[6];
                            //             $chk['time'][trim($row[0]).'('.$time_chk.')'][$r] = $row[6];
                            //         }
                            //     }                                
                            // }
                        }
                    }
                    foreach ($fl_excle as $key1 => $value1) {
                        foreach ($value1 as $key2 => $value2) {
                            if($value2 > 1){
                                foreach ($chk_row[$key1][$key2] as $key => $value) {
                                    $chk['excel'][$key1.' กะ'.$key2][$key] = $key1;
                                }
                            }   
                        }
                    } 
                    // dd($chk['time']);
                    //ตรวจเช็คไฟล์---------------------
                    
                    if(!empty($chk)){
                        return view('FL.not_match',compact('chk')); 
                    }else{
                        foreach($tosave as $key=>$value){
                            $to_exp = explode('+;',$value);
                            $to_save = array();
                            $to_save['log_date'] = $to_exp[0];
                            $to_save['forklift_id'] = $fl_data[$to_exp[1]];

                            if(empty($locate_data[$to_exp[2]])){
                                $save_locate['name'] = $to_exp[2];
                                $id_locate = Flocation::create($save_locate)->id;
                                $locate_data[$to_exp[2]] = $id_locate;
                            }
                            $to_save['f_location_id'] = $locate_data[$to_exp[2]];                            
                            
                            $to_save['shift'] = $to_exp[3];
                            $to_save['type'] = $to_exp[4];

                            if(!empty($to_exp[5])){
                                 $re_name = str_replace('   ',' ',str_replace('  ',' ',$to_exp[5]));
                                 if(empty($person_data[$re_name])){                                   
                                    $to_exp_p = explode(' ',$re_name);
                                    // dd($to_exp_p);
                                    $save_p['fname'] = $to_exp_p[0];
                                    if(!empty($to_exp_p[1])){
                                        $save_p['lname'] = $to_exp_p[1];
                                    }else{
                                        $save_p['lname'] = '-';
                                    }
                                    // dd($save_p);
                                    $id_p = Fperson::create($save_p)->id;
                                    $person_data[$re_name] = $id_p;
                                }
                                $to_save['fperson_id'] = $person_data[$re_name];
                            }

                            $to_save['note'] = $to_exp[6];
                            if(!empty($to_exp[7])) $to_save['time'] = $to_exp[7];
                            else $to_save['time'] = null;
                            if(!empty($fl_input[$to_exp[0]][$fl_data[$to_exp[1]]][$to_exp[3]])){
                                $to_update = FLog::findOrFail($fl_input[$to_exp[0]][$fl_data[$to_exp[1]]][$to_exp[3]]);
                                $to_update->update($to_save);
                            }else{
                                FLog::create($to_save);
                            }                            
                        }
                    }
                }else{
                    $chk['col'][][] = 'ข้อมูลในไฟล์ เกินกว่าคอลัมน์ G';
                    return view('FL.not_match',compact('chk')); 
                }                        

            } else {
                echo SimpleXLSX::parseError();
            }
            return back()->with('success','Updated successfully');
        }else{
            return back();
        }
        
    }

    public function reportView()
    {
        return view('FL.report');
    }

    public function get_time_show($st_date)
    {
        $fkl = Forklift::all();
        foreach ($fkl as $key) {
            // $fl[] = $key->id;
            $update_fl['id'][$key->id] = $key->created_at;
            $update_fl['time'][$key->id] = $key->time;            
        }

        if(!empty($st_date)){
            $loc_id = FLog::selectRaw('forklift_id, f_location_id, time, created_at')->where('log_date', '<', $st_date)->orderBy('id','desc')->get();
        }
        foreach ($loc_id as $key) {
            if(empty($get_log['locate'][$key->forklift_id]))
                $get_log['locate'][$key->forklift_id] = $key->f_location_id;

            if(empty($time_log['time'][$key->forklift_id]) && !empty($key->time)){
                $time_log['time'][$key->forklift_id] = $key->time;
                $time_log['up'][$key->forklift_id] = $key->created_at;
            }
        }

        foreach ($update_fl['time'] as $key=>$value) { //ต้องใช้ $get_log['locate'] เพราะเผื่อรถบางคันไม่มีเวลา
            if(empty($time_log['time'][$key]) || $update_fl['id'][$key] > $time_log['up'][$key])   
                $get_log['time'][$key] = $update_fl['time'][$key];
            else
                $get_log['time'][$key] = $time_log['time'][$key];    
        }
        
        return $get_log; 
    }

    public function report_export(Request $request)
    {            
        $fl_log_locate = array();
        $fl_data = array();
        $locate_data = array();
        $gp_2 = array();
        $to_show = array();
        $gp_3 = array();
        $total_val = array();
        $fl_mile = array();

        $report_id = $request->report_id;    
        $st_date = date ("Y-m-d", strtotime($request->st_date));
        $ed_date = date ("Y-m-d", strtotime($request->ed_date));
        $month_date = date ("m", strtotime($request->month_date));
        $year_date = date ("Y", strtotime($request->month_date));

        if($report_id==1){
            $datetime1 = strtotime($st_date); // convert to timestamps
            $datetime2 = strtotime($ed_date); // convert to timestamps
            $days = (int)(($datetime2 - $datetime1)/86400);

            $test = $this->mail_tbl_1($st_date,$ed_date);
            if(!empty($test[0]))    $fl_log_locate = $test[0];
            if(!empty($test[1]))    $fl_data = $test[1];
            if(!empty($test[2]))    $locate_data = $test[2];
            // if(!empty($test[3]))    $gp_2 = $test[3];
            if(!empty($test[4]))    $to_show = $test[4];
            // if(!empty($test[5]))    $gp_3 = $test[5];
            if(!empty($test[6]))    $total_val = $test[6];  
            if(!empty($test[7]))    $fl_mile = $test[7];

            return view('FL.report_1',compact('fl_log_locate','st_date','ed_date','fl_data','locate_data','total_val','days', 'fl_mile'));

        }elseif($request->report_id==2){
            $test = $this->mail_tbl_1($st_date,$st_date); //ต้องใส่ข้อมูล st_date เพราะเป็นค่าที่ได้รับมาจากการค้น
            if(!empty($test[3]))    $gp_2 = $test[3]; 
            // dd($gp_2);
            return view('FL.report_2',compact('st_date','gp_2'));

        }elseif($request->report_id==3){

            $test = $this->mail_tbl_1($st_date,$st_date);            
            if(!empty($test[1]))    $fl_data = $test[1];
            // if(!empty($test[4]))    $to_show = $test[4];     //ใช้ได้กับ jpgraph เท่านั้น เพราะมี 38 ตัว แต่ js chart ใช้แค่ 19 ตัว   
            if(!empty($test[5]))    $gp_3 = $test[5]; 
            $fl_data = array_unique($fl_data);
            foreach ($fl_data as $key => $value) {
                $to_show[] = $value;
            }             
            // dd($fl_data);      
            return view('FL.report_3', compact('st_date','to_show','gp_3'));

        }elseif($request->report_id==4){
            $st_date = date ("Y-m-d", strtotime($year_date.'-'.$month_date.'-01'));
            $last_day = cal_days_in_month(CAL_GREGORIAN, $month_date, $year_date);
            // dd($last_day);
            $ed_date = date ("Y-m-d", strtotime($year_date.'-'.$month_date.'-'.$last_day));

            $test = $this->mail_tbl_1($st_date,$ed_date);            
            if(!empty($test[0]))    $fl_log_locate = $test[0];
            if(!empty($test[1]))    $fl_data = $test[1];
            if(!empty($test[2]))    $locate_data = $test[2];
            if(!empty($test[6]))    $total_val = $test[6];  

            $test1 = $this->mail_gp_4($locate_data,$fl_data,$month_date,$year_date,$fl_log_locate);
            $to_show = $test1[0];
            $gp_4 = $test1[1];
            // dd($to_show);
            return view('FL.report_4',compact('to_show','gp_4','month_date','year_date','fl_log_locate','fl_data','locate_data','total_val'));
            
        }elseif($request->report_id==5){
            $st_date = date ("Y-m-d", strtotime($year_date.'-'.$month_date.'-01'));
            $last_day = cal_days_in_month(CAL_GREGORIAN, $month_date, $year_date);
            // dd($last_day);
            $ed_date = date ("Y-m-d", strtotime($year_date.'-'.$month_date.'-'.$last_day));
            $test = $this->mail_tbl_1($st_date,$ed_date);     
            if(!empty($test[0]))    $fl_log_locate = $test[0];
            if(!empty($test[1]))    $fl_data = $test[1];
            if(!empty($test[2]))    $locate_data = $test[2];  
            // dd($fl_log_locate);       

            $test1 = $this->mail_gp_4($locate_data,$fl_data,$month_date,$year_date,$fl_log_locate);
            $date_in_month = $test1[3];
            $to_show_gp5 = $test1[4]; 
            // dd($date_in_month);

            return view('FL.report_5',compact('date_in_month','to_show_gp5','month_date','year_date'));
        } 
    }

    public function mailView()
    {        
        $range_date = 4;
        $ed_date = date ("Y-m-d", strtotime("-1 day"));
        // $ed_date = '2022-12-23'; 
        $st_date = date ("Y-m-d", strtotime("-".$range_date." day", strtotime($ed_date)));   
        
        $days = (int)((strtotime($ed_date) - strtotime($st_date))/86400);
        $cell_w = 55/(($days+1)*2);
        // dd($st_date.'-'.$ed_date);
        $fl_log_locate = array();
        $fl_data = array();
        $locate_data = array();
        $gp_2 = array();
        $to_show = array();
        $gp_3 = array();
        $total_val = array();
        $fl_mile = array();

        $test = $this->mail_tbl_1($st_date,$ed_date);
        if(!empty($test[0]))    $fl_log_locate = $test[0];
        if(!empty($test[1]))    $fl_data = $test[1];
        if(!empty($test[2]))    $locate_data = $test[2];
        if(!empty($test[3]))    $gp_2 = $test[3];
        // if(!empty($test[4]))    $to_show = $test[4];     //ใช้ได้กับ jpgraph เท่านั้น เพราะมี 38 ตัว แต่ js chart ใช้แค่ 19 ตัว
        $to_show_dup = array_unique($fl_data);
        foreach ($to_show_dup as $key => $value) {
            $to_show[] = $value;
        }
        if(!empty($test[5]))    $gp_3 = $test[5];
        if(!empty($test[6]))    $total_val = $test[6]; 
        
        if(!empty($test[7]))    $fl_mile = $test[7]; 

        // dd($gp_2);
        $test1 = $this->mail_gp_4($locate_data,$fl_data,date ("m",strtotime($ed_date)),date ("Y",strtotime($ed_date)),$fl_log_locate);
        $to_show4 = $test1[0];
        $gp_4 = $test1[1];
        $to_gp4 = $test1[2];
        $date_in_month = $test1[3];
        $to_show_gp5 = $test1[4];
        $to_sort = $test1[5];
        $togp4 = $test1[6];
        // dd($togp4);
        return view('FL.mail',compact('st_date','ed_date','cell_w','fl_log_locate','fl_data','locate_data','gp_2','to_show','gp_3','total_val','to_show4','gp_4','to_gp4','date_in_month','to_show_gp5','togp4','fl_mile'));
    }

    //ข้อมูลข้อ 1-3
    public function mail_tbl_1($st_date,$ed_date)
    {
        $query = Forklift::orderByRaw('len(name)')->get();
        foreach($query as $key){
            $fl_data[$key->id] = $key->name;  
            // $fl_data_id[$key->name] = $key->id; 
            $status[$key->id] = $key->status; 
            // $fl_type[$key->id] = $key->type; 
        }

        $fix_show_mile = '20';
        
        $query = Flocation::orderByRaw('len(name)')->get();
        foreach($query as $key){
            $locate_data[$key->id] = $key->name;
        }

        $get_log = $this->get_time_show($st_date); //เรียกเลขไมล์ของวันก่อนหน้า
        
        $fl_log = array();
        $query_1 = new FLog;
        $query_1 = $query_1->join('forklifts', 'f_logs.forklift_id', '=', 'forklifts.id');
        $query_1 = $query_1->select('f_logs.*');
        $query_1 = $query_1->whereBetween('f_logs.log_date', [$st_date, $ed_date]);
        $query_1 = $query_1->orderByRaw('LEN(forklifts.name), forklifts.name, f_logs.log_date, f_logs.shift')->get();
        // $query_1 = $query_1->orderBy('forklifts.name')->orderBy('f_logs.log_date')->orderBy('f_logs.shift')->get(); 
        
        $datetime1 = strtotime($st_date); // convert to timestamps
        $datetime2 = strtotime($ed_date); // convert to timestamps
        $days = (int)(($datetime2 - $datetime1)/86400);
        if(count($query_1)>0){
            foreach($query_1 as $key){
                
                if(!empty($key->time)){  
                    if(empty($fl_id) || $fl_id <> $key->forklift_id){ 
                        $to_diff = $get_log['time'][$key->forklift_id];
                    }                     
                    $fl_id = $key->forklift_id;
                    $key_time = number_format($key->time,1,'.','');                                           
                    $diff_time = $key_time-$to_diff;
                    
                    $fl_log_locate[$key->f_location_id][$key->forklift_id][$key->log_date][$key->shift][$key->type] = number_format($diff_time,1,'.','');
                    if(!empty($total_val['count'][$key->f_location_id][$key->forklift_id][$key->shift])){
                        $total_val['count'][$key->f_location_id][$key->forklift_id][$key->shift] += 1;
                        $total_val['sum'][$key->f_location_id][$key->forklift_id][$key->shift] += $diff_time;
                    }else{
                        $total_val['count'][$key->f_location_id][$key->forklift_id][$key->shift] = 1;
                        $total_val['sum'][$key->f_location_id][$key->forklift_id][$key->shift] = $diff_time;
                    }
                    if($key->log_date == $ed_date){
                        $to_gp3[$key->shift][$key->forklift_id] = $diff_time;
                    }
                    $to_diff = $key->time;


                    if (strpos($fl_data[$key->forklift_id], $fix_show_mile) !== FALSE) {
                        $fl_mile[$key->f_location_id][$fl_data[$key->forklift_id]][$key->log_date][$key->shift][$key->type] = round($key->time,1);
                    }
                        
                }else{
                    $fl_log_locate[$key->f_location_id][$key->forklift_id][$key->log_date][$key->shift][$key->type] = $key->type;
                    if (strpos($fl_data[$key->forklift_id], $fix_show_mile) !== FALSE) {
                        $fl_mile[$key->f_location_id][$fl_data[$key->forklift_id]][$key->log_date][$key->shift][$key->type] = $key->type;
                    }
                }
                
                if($key->log_date == $ed_date){
                    if(empty($gp_2[$key->type][$key->shift]))   $gp_2[$key->type][$key->shift] = 1;
                    else    $gp_2[$key->type][$key->shift] += 1;
                }    
            }
            
            foreach ($fl_log_locate as $kloc => $vloc) {
                foreach($vloc as $kfl => $vfl) {
                    if(!empty($total_val['count'][$kloc][$kfl])){
                        foreach($total_val['count'][$kloc][$kfl] as $ktt_sh => $vtt_sh) {
                            if(!empty($total_val_name['count'][$kloc][$fl_data[$kfl]][$ktt_sh]))  
                                $total_val_name['count'][$kloc][$fl_data[$kfl]][$ktt_sh] += $vtt_sh;
                            else
                                $total_val_name['count'][$kloc][$fl_data[$kfl]][$ktt_sh] = $vtt_sh;
                            if(!empty($total_val_name['sum'][$kloc][$fl_data[$kfl]][$ktt_sh]))  
                                $total_val_name['sum'][$kloc][$fl_data[$kfl]][$ktt_sh] += $total_val['sum'][$kloc][$kfl][$ktt_sh];
                            else
                                $total_val_name['sum'][$kloc][$fl_data[$kfl]][$ktt_sh] = $total_val['sum'][$kloc][$kfl][$ktt_sh];
                        }
                    }                    
                    foreach ($vfl as $kdate => $vdate) {
                        foreach($vdate as $ksh => $vsh) {
                            foreach($vsh as $kt => $vt) {
                                $fl_log_name[$kloc][$fl_data[$kfl]][$kdate][$ksh][$kt] = $vt;                                
                            }
                        }
                    }
                }
            }
            //ข้อมูลข้อ 2
            $arr_type = array('R','S','A');
            $arr_shift = array('B','C');
            foreach ($arr_type as $ktype => $vtype) {
                foreach($arr_shift as $kshift => $vshift) {
                    if(empty($gp_2[$vtype][$vshift]))   $gp_2[$vtype][$vshift]=0;
                }
            }
            // dd($gp_2);

            //ข้อมูลข้อ 3
            natsort($fl_data);
            $to_shift = array();
            $to_show = array();
            $i=0;
            foreach($fl_data as $key=>$value){   
                if($status[$key]=='Active'){             
                    if(empty($to_gp3['B'][$key]))  $gp_3['B'][$i]=0;
                    else    $gp_3['B'][$i]=number_format($to_gp3['B'][$key],1);
                    if(empty($to_gp3['C'][$key]))  $gp_3['C'][$i]=0;
                    else    $gp_3['C'][$i]=number_format($to_gp3['C'][$key],1);
                    array_push($to_shift, 'B', 'C');
                    for($l=0;$l<2;$l++){
                        $to_show[] = $value;
                    }            
                    $i++;
                }
            }

            return [$fl_log_name, $fl_data, $locate_data, $gp_2, $to_show, $gp_3, $total_val_name, $fl_mile] ;
        }
    }

    //ข้อมูลข้อ 4-5
    public function mail_gp_4($locate_data,$fl_data,$month,$year,$fl_log_locate)
    {
        // dd($fl_log_locate);
        $total_gp4 = array();
        $to_show_gp5 = array();

        $total_val_gp = array();
        $query_1 = new FLog;
        $query_1 = $query_1->join('flocations', 'f_logs.f_location_id', '=', 'flocations.id');
        $query_1 = $query_1->join('forklifts', 'f_logs.forklift_id', '=', 'forklifts.id');
        $query_1 = $query_1->whereMonth('log_date', '=', $month)->whereYear('log_date', '=', $year);
        $query_1 = $query_1->select('f_logs.id', 'f_logs.log_date', 'f_logs.f_location_id', 'f_logs.forklift_id', 'f_logs.fperson_id', 'f_logs.shift', 'f_logs.type', 'f_logs.time', 'f_logs.note');
        $query_1 = $query_1->orderBy('forklifts.name')->orderBy('f_logs.log_date')->orderBy('f_logs.shift')->get(); 
        // dd($query_1);
        if(count($query_1)>0){
            foreach($query_1 as $key){
                if(empty($locate_data[$key->f_location_id]))    $chk_loc = '';
                else    $chk_loc = $locate_data[$key->f_location_id];
                // echo $chk_loc.':';
                if(empty($fl_data[$key->forklift_id]))    $chk_fl = '';
                else $chk_fl = $fl_data[$key->forklift_id];

                if(!empty($key->time)){

                    if(empty($set_fl) || $set_fl <> $key->forklift_id){
                        $set_fl = $key->forklift_id;
                        $time_before =0;
                        $test = DB::select("EXEC GetTime_FL_2 ".$key->forklift_id.",'".$key->log_date."'");
                        // dd($test[0]->time);
                        if(!empty($test[0]->time)){  
                            $to_diff = $test[0]->time;
                        }else{
                            $to_diff = 0;
                        }
                    }else{
                        $to_diff = $time_before;
                    }                    

                    $key_time = number_format($key->time,1,'.','');                        
                    $diff_time = (float)$key_time-(float)$to_diff;        
                    // echo $fl_data[$key->forklift_id].'('.$key->forklift_id.' กะ '.$key->shift.') ->'.$key->log_date.':'.$key->time.'-'.$to_diff.'='.$diff_time.'</br>';      
                    if(!empty($total_val_gp['count'][$chk_loc][$chk_fl][$key->shift])){     //ได้ fl, loc เป็น name
                        $total_val_gp['count'][$chk_loc][$chk_fl][$key->shift] += 1;
                        $total_val_gp['sum'][$chk_loc][$chk_fl][$key->shift] += $diff_time;                                               
                    }else{
                        $total_val_gp['count'][$chk_loc][$chk_fl][$key->shift] = 1;
                        $total_val_gp['sum'][$chk_loc][$chk_fl][$key->shift] = $diff_time;
                    }

                    if(!empty($total_gp4['count'][$key->f_location_id][$key->forklift_id][$key->shift])){   //ได้ fl, loc เป็น id ใช้แสดงตาราง foreach กับ fl_log_locate
                        $total_gp4['count'][$key->f_location_id][$key->forklift_id][$key->shift] += 1;  
                        $total_gp4['sum'][$key->f_location_id][$key->forklift_id][$key->shift] += $diff_time;                        
                    }else{
                        $total_gp4['count'][$key->f_location_id][$key->forklift_id][$key->shift] = 1;
                        $total_gp4['sum'][$key->f_location_id][$key->forklift_id][$key->shift] = $diff_time;
                    }
                    $to_diff = number_format($key->time,1,'.',''); 
                    $to_gp5[$chk_fl][$chk_loc][$key->shift][$key->log_date] = $diff_time; 
                    if(empty($key->time))   $time_before = $test[0]->time;
                    else    $time_before = $key->time;                 
                } 
            }
        }

        // dd($total_val_gp);
        
        $to_shift = array();
        $change_fl4 = array();
        $change_gp4 = array();
        $gp_show4 = array();
        $to_sort = array();
        $i=0;
        // $date_in_month = date("t", mktime(0, 0, 0, 7, 1, 2000));
        $date_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        // dd($fl_log_locate);

        if(count($fl_log_locate)>0){
            foreach ($fl_log_locate as $key => $value) {
                foreach ($value as $kfl => $vfl) {
                    // $change_fl4[] = $fl_data[$kfl];
                    // $to_sort[$locate_data[$key]][$fl_data[$kfl]] = $fl_data[$kfl].'-'.$locate_data[$key];
                    $change_fl4[] = $kfl;
                    $to_sort[$locate_data[$key]][$kfl] = $kfl.'-'.$locate_data[$key];
                }
            }
        }
        // dd($to_sort);
        if(count($to_sort)>0){
            foreach ($to_sort as $kloc => $vloc) {
                foreach($vloc as $kfl=>$vfl){                
                    if(!empty($total_val_gp['sum'][$kloc][$kfl]['B'])){
                        $gp_show4['B'][$kfl] = number_format($total_val_gp['sum'][$kloc][$kfl]['B']/$total_val_gp['count'][$kloc][$kfl]['B'],1);
                    }else{
                        $gp_show4['B'][$kfl] = 0;
                    } 
                    if(!empty($total_val_gp['sum'][$kloc][$kfl]['C'])){
                        $gp_show4['C'][$kfl] = number_format($total_val_gp['sum'][$kloc][$kfl]['C']/$total_val_gp['count'][$kloc][$kfl]['C'],1);
                    }else{
                        $gp_show4['C'][$kfl] = 0;
                    } 

                    array_push($to_shift, 'B', 'C');
                    $to_shift_gp5 = array();
                    $to_date_gp5 = array();
                    for($i=1;$i<=$date_in_month;$i++){
                        // if($kfl=='FL No.1' && $kloc =='WH-ST')    echo $i.'--';
                        if(empty($to_gp5[$kfl][$kloc]['B'][date('Y-m-d',strtotime($year.'-'.$month.'-'.$i))])){
                            $to_show_gp5[$kfl][$kloc]['B'][] = 0;
                        }else{
                            $to_show_gp5[$kfl][$kloc]['B'][] = floatval(number_format($to_gp5[$kfl][$kloc]['B'][date('Y-m-d',strtotime($year.'-'.$month.'-'.$i))],1));
                        }
                        if(empty($to_gp5[$kfl][$kloc]['C'][date('Y-m-d',strtotime($year.'-'.$month.'-'.$i))])){
                            $to_show_gp5[$kfl][$kloc]['C'][] = 0;
                        }else{
                            $to_show_gp5[$kfl][$kloc]['C'][] = floatval(number_format($to_gp5[$kfl][$kloc]['C'][date('Y-m-d',strtotime($year.'-'.$month.'-'.$i))],1));
                        }
                        array_push($to_shift_gp5, 'B', 'C');
                        for($l=0;$l<2;$l++){
                            $to_date_gp5[] = $i;
                        }
                    }
                }
            } 
        }
        // dd($gp_show4);
        if(count($fl_log_locate)>0){
            foreach ($fl_log_locate as $key => $value) {
                foreach ($value as $kfl => $vfl) {
                    // if(!empty($gp_show4['B'][$fl_data[$kfl]]))   $change_gp4['B'][] = $gp_show4['B'][$fl_data[$kfl]];
                    if(!empty($gp_show4['B'][$kfl]))   $change_gp4['B'][] = $gp_show4['B'][$kfl];
                    else    $change_gp4['B'][] =0;
                    // if(!empty($gp_show4['C'][$fl_data[$kfl]]))   $change_gp4['C'][] = $gp_show4['C'][$fl_data[$kfl]];
                    if(!empty($gp_show4['C'][$kfl]))   $change_gp4['C'][] = $gp_show4['C'][$kfl];
                    else    $change_gp4['C'][] =0;
                }
            }
        }
        // dd($date_in_month);
        return [$change_fl4, $change_gp4, $total_gp4, $date_in_month, $to_show_gp5, $to_sort, $gp_show4] ; 
    }
    
    public function sent_mail(){
        // dd($request->mail);        
        ini_set('memory_limit', '2000M');
		ini_set('post_max_size', '64M');
		ini_set('upload_max_filesize', '64M');
		ini_set('max_input_time', 300);
		ini_set('max_execution_time', 1900);
        
        // $range_date = $this->argument('range');
        $range_date = 4;

        $ed_date = date ("Y-m-d", strtotime("-1 day"));
        // $ed_date = '2022-12-23';
        $st_date = date ("Y-m-d", strtotime("-".$range_date." day", strtotime($ed_date)));
        
        $fl_log_locate = array();
        $fl_data = array();
        $locate_data = array();
        $gp_2 = array();
        $to_show = array();
        $gp_3 = array();
        $total_val = array();
        $fl_mile = array();

        $test = $this->mail_tbl_1($st_date,$ed_date);
        if(!empty($test[0]))    $fl_log_locate = $test[0];
        if(!empty($test[1]))    $fl_data = $test[1];
        if(!empty($test[2]))    $locate_data = $test[2];
        if(!empty($test[3]))    $gp_2 = $test[3];
        if(!empty($test[4]))    $to_show = $test[4];
        if(!empty($test[5]))    $gp_3 = $test[5];
        if(!empty($test[6]))    $total_val = $test[6]; 
        if(!empty($test[7]))    $fl_mile = $test[7]; 
        // dd($gp_3);

        $test1 = $this->mail_gp_4($locate_data,$fl_data,date ("m",strtotime($ed_date)),date ("Y",strtotime($ed_date)),$fl_log_locate);
        $to_show4 = $test1[0];
        $gp_4 = $test1[1];
        $to_gp4 = $test1[2];
        $date_in_month = $test1[3];
        $to_show_gp5 = $test1[4];
        $to_sort = $test1[5]; 
        $togp4 = $test1[6];       

        //แสดง Chart ข้อที่ 2 -------------------
        require_once app_path() . '/jpgraph/jpgraph.php';
        require_once app_path() . '/jpgraph/jpgraph_bar.php';
        require_once app_path() . '/jpgraph/jpgraph_canvas.php';
        require_once app_path() . '/jpgraph/jpgraph_table.php';

        // Display the graph
        $path = public_path() . '/graph/'.date('Y'). "/" . date('m') . '/mail';
        // dd($path);
        if (!File::exists($path)) {
            File::makeDirectory($path,  0777, true, true);
        }
        $date = date('ymdHis');

        if(!empty($gp_2)){
            $datay = array(
                array('กะ B','กะ C'),
                array($gp_2['R']['B'],$gp_2['R']['C']),
                array($gp_2['S']['B'],$gp_2['S']['C']),
                array($gp_2['A']['B'],$gp_2['A']['C']));
            // dd($datay);
            $nbrbar = 2; // number of bars and hence number of columns in table
            $cellwidth = 100;  // width of each column in the table
            $tableypos = 200; // top left Y-coordinate for table
            $tablexpos = 130;  // top left X-coordinate for table
            $tablewidth = $nbrbar*$cellwidth; // overall table width
            $rightmargin = 30;  // right margin in the graph
            $topmargin = 60;  // top margin of graph
            $height = 320;  // a suitable height for the image
            $width = $tablexpos+$tablewidth+$rightmargin; // the width of the image
            $graph = new \Graph($width,$height);	
            $graph->img->SetMargin($tablexpos,$rightmargin,$topmargin,$height-$tableypos);
            
            $graph->SetScale('textlin');
            // $graph->SetMarginColor('white');
            $graph->xaxis->Hide();  //ซ่อน label ใต้กราฟ ต้องอยู่ใต้ SetMarginColor
            $bplot = new \BarPlot($datay[1]);     
            $bplot->SetFillGradient('limegreen','limegreen',GRAD_VERT);
            $bplot->SetWeight(0);

            $bplot2 = new \BarPlot($datay[2]);
            $bplot2->SetFillGradient('yellow','yellow',GRAD_VERT);
            $bplot2->SetWeight(0);

            $bplot3 = new \BarPlot($datay[3]);
            $bplot3->SetFillGradient('azure4','azure4',GRAD_VERT);
            $bplot3->SetWeight(0);


            $accbplot = new \AccBarPlot(array($bplot,$bplot2,$bplot3));

            $accbplot->value->Show();
            $accbplot->value->SetFont(FF_ANGSA, FS_NORMAL,14);
            // $accbplot->value->SetAngle(90);
            $accbplot->SetColor('white');        
            $accbplot->SetWeight(1);
            $graph->Add($accbplot);

            $table = new \GTextTable();
            // $table->Init(4,3); // Create a 5 rows x 7 columns table
            $data_tbl = array(
                array('','กะ B','กะ C'),
                array('เสียต้องจอด',$gp_2['A']['B'],$gp_2['A']['C']),
                array('จอด',$gp_2['S']['B'],$gp_2['S']['C']),
                array('ใช้งาน',$gp_2['R']['B'],$gp_2['R']['C']));
            $table->Set($data_tbl);
            $table->SetPos(30,$tableypos+1);
            $table->SetFont(FF_ANGSA, FS_BOLD, 12);
            $table->SetAlign('right');
            $table->SetMinColWidth($cellwidth);
            // $table->SetNumberFormat('%0.1f');

            // Format table header row
            // $table->SetRowFillColor(0,'teal@0.7');
            $table->SetRowFont(0,FF_ANGSA, FS_BOLD, 14);
            $table->SetRowAlign(0,'center');

            $table->SetCellFillColor(1,0,'azure4');
            $table->SetCellFillColor(2,0,'yellow');
            $table->SetCellFillColor(3,0,'limegreen');
            // .. and add it to the graph
            $graph->Add($table);
            $graph->title->Set("สัดส่วนการใช้งานรถ FL (รถเช่า) วันที่ ".date ("d/m/Y", strtotime($ed_date)));
            $graph->title->SetFont(FF_ANGSA, FS_BOLD, 18);
            
            $gp['gp1']['link'][0] = "graph/".date('Y'). "/" . date('m') . "/mail/FL_2_" .  $date . ".jpg";
            $gp['gp1']['path'][0] = $path . "/FL_2_" .  $date . ".jpg";        
            $graph->Stroke($gp['gp1']['path'][0]);
            echo $gp['gp1']['path'][0]."\r\n";  
            // dd($gp['gp1']['path'][0]);  
        }
        //แสดง Chart ข้อที่ 2 --------จบ-----------


        //แสดง Chart ข้อที่ 3 -------------------
        $data1y=$gp_3['B'];
        $data2y=$gp_3['C'];

        $nbrbar = count($gp_3['B'])*2; // number of bars and hence number of columns in table
        $cellwidth = 30;  // width of each column in the table
        $tableypos = 332; // top left Y-coordinate for table
        $tablexpos = 130;  // top left X-coordinate for table
        $tablewidth = $nbrbar*$cellwidth; // overall table width
        $rightmargin = 30;  // right margin in the graph
        $topmargin = 60;  // top margin of graph
        $height = 400;  // a suitable height for the image
        $width = $tablexpos+$tablewidth+$rightmargin; // the width of the image
        $graph2 = new \Graph($width,$height);	
        $graph2->img->SetMargin($tablexpos,$rightmargin,$topmargin,$height-$tableypos);




        // $graph2 = new \Graph(900, 400);
        $graph2->SetScale("textlin");

        $theme_class = new \UniversalTheme;
        $graph2->SetTheme($theme_class);
        

        // //$graph->yaxis->SetTickPositions(array(0, 30, 60, 90, 120, 150), array(15, 45, 75, 105, 135));
        $graph2->SetBox(false);

        $graph2->ygrid->SetFill(false);
        // $graph2->xaxis->SetTickLabels($to_show);
        // $graph2->xaxis->SetLabelAlign('center', 'center');
        // $graph2->xaxis->title->SetFont(FF_ANGSA, FS_BOLD, 18);
        // $graph2->xaxis->SetFont(FF_ANGSA, FS_NORMAL, 14);
        $graph2->xaxis->Hide();  //ซ่อน label ใต้กราฟ ต้องอยู่ใต้ SetMarginColor
        $graph2->yaxis->SetColor("#000000");
        $graph2->yaxis->HideLine(true);
        $graph2->yaxis->HideTicks(false, false);
        $graph2->yaxis->SetFont(FF_ANGSA, FS_NORMAL, 14);

        // Create the bar plots
        $b1plot2 = new \BarPlot($data1y);
        $b2plot2 = new \BarPlot($data2y);

        // Create the grouped bar plot
        $gbplot2 = new \GroupBarPlot(array($b1plot2, $b2plot2));
        // ...and add it to the graPH
        $graph2->Add($gbplot2);

        $b1plot2->SetColor("#000000");  //สีเส้น bar
        $b1plot2->SetFillColor("#FFC000");  //สี bar
        // $b1plot2->SetLegend("Sum of list");
        $b1plot2->value->SetFormat('%01.1f');
        $b1plot2->value->SetColor("#000000");
        $b1plot2->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
        $b1plot2->value->Show();        
        $b1plot2->value->SetAngle(90);

        $b2plot2->SetColor("#000000");  //สีเส้น bar
        $b2plot2->SetFillColor("#FFC000");  //สี bar
        // $b2plot2->SetLegend("Sum of random result");
        $b2plot2->value->SetFormat('%01.1f');
        $b2plot2->value->SetColor("#000000");   
        $b2plot2->value->Show();        
        $b2plot2->value->SetFont(FF_ANGSA, FS_NORMAL,14);
        $b2plot2->value->SetAngle(90);

        $graph2->ygrid->Show(false);

        $graph2->legend->SetFrameWeight(1);
        $graph2->legend->SetColumns(2);
        $graph2->legend->SetColor('#FFC000', '#FFC000');
        $graph2->legend->SetPos(0.5, 0.08, 'center', 'top');

        $graph2->title->SetFont(FF_ANGSA, FS_BOLD, 18);
        $graph2->title->Set("กราฟแสดงชั่วโมงการใช้รถประจำวันที่ ".date ("d/m/Y", strtotime($ed_date)));

        // $canvasG = new \CanvasGraph(900,400);

        // dd($fl_data);
        $fl_shift_dup = array_unique($fl_data);
        $to_shift = array();
        foreach($fl_shift_dup as $key=>$value){
            array_push($to_shift, 'B', 'C');
            // for($l=0;$l<2;$l++){
            //     $to_show[] = $value;
            // }
        }
        // dd($to_show4);
        // Setup the basic table and data
        $data = array($to_shift,$to_show);
        // dd($to_show);
        $table = new \GTextTable();
        $table->Set($data);            
        // $table->SetPos(40,333); //ตำแหน่งเริ่มต้นของตาราง
        $table->SetPos(40,$tableypos+1);/////////////////////////////////////////////
        // $table->SetMinColWidth(30);
        $i=0;
        for($i=0;$i<count($to_show);$i++){
            // echo $i.'--';
            $table->MergeCells(1,$i,1,$i+1);
            $i++;
        }
        $table->SetFont(FF_ANGSA, FS_NORMAL, 12);
        $table->SetAlign('center');
        $table->SetMinColWidth($cellwidth+2.6);////////////////////////////////////////////2.8 2.7
        $graph2->Add($table);

        $gp['gp2']['link'][0] = "graph/".date('Y'). "/" . date('m') . "/mail/FL_3_" .  $date . ".jpg";
        $gp['gp2']['path'][0] = $path . "/FL_3_" .  $date . ".jpg";        
        $graph2->Stroke($gp['gp2']['path'][0]);
        echo $gp['gp2']['path'][0]."\r\n";
        // dd($gp['gp2']['path'][0]);
        //แสดง Chart ข้อที่ 3 ----------จบ---------


        //แสดง Chart ข้อที่ 4 -------------------   
        $to_shift_gp5 = array();
        $to_date_gp5 = array();
        for($i=1;$i<=$date_in_month;$i++){
            array_push($to_shift_gp5, 'B', 'C');
            for($l=0;$l<2;$l++){
                $to_date_gp5[] = $i;
            }
        }
        foreach ($to_sort as $kloc => $vloc) {  
            foreach($vloc as $kfl=>$vfl){
                for($l=0;$l<2;$l++){
                    $to_loc[] = $kloc;
                    $to_show_gp4[] = $kfl;
                }
            }
        }
        // dd($to_show_gp4);
        $data1y=$gp_4['B'];
        //bar2
        $data2y=$gp_4['C'];
        // dd($to_show4);

        $nbrbar = count($gp_4['B'])*2; // number of bars and hence number of columns in table
        $cellwidth = 30;  // width of each column in the table
        $tableypos = 332; // top left Y-coordinate for table
        $tablexpos = 130;  // top left X-coordinate for table
        $tablewidth = $nbrbar*$cellwidth; // overall table width
        $rightmargin = 30;  // right margin in the graph
        $topmargin = 60;  // top margin of graph
        $height = 400;  // a suitable height for the image
        $width = $tablexpos+$tablewidth+$rightmargin; // the width of the image
        $graph3 = new \Graph($width,$height);	
        $graph3->img->SetMargin($tablexpos,$rightmargin,$topmargin,$height-$tableypos);


        // Create the graph. These two calls are always required
        // $graph3 = new \Graph(900,400,'auto');
        $graph3->SetScale("textlin");
        $graph3->SetY2Scale("lin",0,90);
        $graph3->SetY2OrderBack(false);

        $theme_class = new \UniversalTheme;
        $graph3->SetTheme($theme_class);

        // $graph3->SetMargin(40,20,46,80);

        $graph3->SetBox(false);

        $graph3->xaxis->Hide();  //ซ่อน label ใต้กราฟ ต้องอยู่ใต้ SetMarginColor
        $graph3->ygrid->SetFill(false);
        // $graph3->xaxis->SetTickLabels(array('A','B','C','D'));
        $graph3->yaxis->HideLine(false);
        $graph3->yaxis->HideTicks(false,false);

        // Create the bar plots
        $b1plot = new \BarPlot($data1y);
        $b2plot = new \BarPlot($data2y);

        $gbplot = new \GroupBarPlot(array($b1plot,$b2plot));

        // ...and add it to the graPH
        $graph3->Add($gbplot);
        // $graph->AddY2($lplot);

        $b1plot->SetColor("#0000CD");   //สีเส้น bar
        $b1plot->SetFillColor("#0000CD");   //สี bar
        // $b1plot->SetLegend("Cliants");   //สำหรับใส่ป้ายกำกับ
        $b1plot->value->SetFormat('%01.1f');
        $b1plot->value->SetColor("#000000");
        $b1plot->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
        $b1plot->value->SetAngle(90);
        $b1plot->value->Show();

        $b2plot->SetColor("#B0C4DE");   //สีเส้น bar
        $b2plot->SetFillColor("#B0C4DE");   //สี bar
        // $b2plot->SetLegend("Machines");  //สำหรับใส่ป้ายกำกับ
        $b2plot->value->SetFormat('%01.1f');
        $b2plot->value->SetColor("#000000");
        $b2plot->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
        $b2plot->value->SetAngle(90);
        $b2plot->value->Show();
        
        $graph3->legend->SetFrameWeight(1);
        $graph3->legend->SetColumns(2);
        $graph3->legend->SetColor('#4E4E4E','#00A78A');

        $table = new \GTextTable();        
        $table->Init(3,count($to_loc)); // Create a 5 rows x 7 columns table
        $data_tbl = array($to_shift, $to_show_gp4, $to_loc);
        $table->Set($data_tbl);
        // $table->SetPos(40,320);//ตำแหน่งเริ่มต้นของตาราง
        $table->SetPos(40,$tableypos+1);/////////////////////////////////////////////
        $i=0;
        $c=0;
        foreach ($to_sort as $kloc => $vloc) {  
            foreach($vloc as $kfl=>$vfl){
                // echo $i.':'.($i+1).'--';
                $table->MergeCells(1,$i,1,$i+1);
                $i=$i+2;                 
            }
            // echo ';'.$c.':'.($i+1).'--';
            $table->MergeCells(2,$c,2,$i-1);
            $c=$i;
        }  
        $table->SetFont(FF_ANGSA, FS_NORMAL, 8);////////ขนาดตัวอักษรแถว 2 ลงมา
        $table->SetAlign('center');
        // $table->SetMinColWidth($cellwidth);
        $table->SetMinColWidth($cellwidth+2.7);////////////////////////////////////////////  3
        // $table->SetNumberFormat('%0.1f');

        // Format table header row
        // $table->SetRowFillColor(0,'teal@0.7');
        $table->SetRowFont(0,FF_ANGSA, FS_NORMAL, 10);////////ขนาดตัวอักษรแถวแรก
        $table->SetRowAlign(0,'center');            
        $graph3->Add($table);
        $graph3->title->Set("กราฟแสดงชั่วโมงการใช้รถโดยเฉลี่ยในเดือน ".date('m/Y',strtotime($ed_date)));
        $graph3->title->SetFont(FF_ANGSA, FS_NORMAL, 18);

        $gp['gp3']['link'][0] = "graph/".date('Y'). "/" . date('m') . "/mail/FL_4_" .  $date . ".jpg";
        $gp['gp3']['path'][0] = $path . "/FL_4_" .  $date . ".jpg";        
        $graph3->Stroke($gp['gp3']['path'][0]);
        echo $gp['gp3']['path'][0]."\r\n";
        // dd($gp['gp3']['path'][0]);
        //แสดง Chart ข้อที่ 4 ---------จบ----------

        // dd($to_show_gp5);
        //แสดง Chart ข้อที่ 5 -------------------        
        $count_gp = 0;
        foreach ($to_show_gp5 as $kfl => $vfl) {
            foreach ($vfl as $kloc => $vloc) {
                $data1y=$to_show_gp5[$kfl][$kloc]['B'];                
                $data2y=$to_show_gp5[$kfl][$kloc]['C'];
                // dd($data2y);

                $count_col = count($to_show_gp5[$kfl][$kloc]['B']);
                $nbrbar = $count_col*2; // number of bars and hence number of columns in table
                $cellwidth = 17;
                $tableypos = 319; // top left Y-coordinate for table
                $tablexpos = 130;  // top left X-coordinate for table
                $tablewidth = $nbrbar*$cellwidth; // overall table width
                $rightmargin = 30;  // right margin in the graph
                $topmargin = 60;  // top margin of graph
                $height = 384;  // a suitable height for the image
                $width = $tablexpos+$tablewidth+$rightmargin; // the width of the image
                $graph4 = new \Graph($width,$height);	
                $graph4->img->SetMargin($tablexpos,$rightmargin,$topmargin,$height-$tableypos);


                // $graph4 = new \Graph(1200,400,'auto');
                $graph4->SetScale("textlin");
                $graph4->SetY2Scale("lin",0,90);
                $graph4->SetY2OrderBack(false);

                $theme_class = new \UniversalTheme;
                $graph4->SetTheme($theme_class);

                // $graph4->SetMargin(40,20,46,80);
                $graph4->SetBox(false);

                $graph4->xaxis->Hide();  //ซ่อน label ใต้กราฟ ต้องอยู่ใต้ SetMarginColor
                $graph4->ygrid->SetFill(false);
                // $graph3->xaxis->SetTickLabels(array('A','B','C','D'));
                $graph4->yaxis->HideLine(false);
                $graph4->yaxis->HideTicks(false,false);

                // Create the bar plots
                $b1plot = new \BarPlot($data1y);
                $b2plot = new \BarPlot($data2y);

                $gbplot = new \GroupBarPlot(array($b1plot,$b2plot));

                // ...and add it to the graPH
                $graph4->Add($gbplot);
                // $graph->AddY2($lplot);

                $b1plot->SetColor("#FFFFFF");   //สีเส้น bar
                $b1plot->SetFillColor("#5B9BD5");   //สี bar
                // $b1plot->SetLegend("Cliants");   //สำหรับใส่ป้ายกำกับ
                $b1plot->value->SetFormat('%01.1f');
                $b1plot->value->SetColor("#000000");
                $b1plot->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
                $b1plot->value->SetAngle(90);
                $b1plot->value->Show();

                $b2plot->SetColor("#FFFFFF");   //สีเส้น bar
                $b2plot->SetFillColor("#5B9BD5");   //สี bar
                // $b2plot->SetLegend("Machines");  //สำหรับใส่ป้ายกำกับ
                $b2plot->value->SetFormat('%01.1f');
                $b2plot->value->SetColor("#000000");
                $b2plot->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
                $b2plot->value->SetAngle(90);
                $b2plot->value->Show();
                
                $graph4->legend->SetFrameWeight(1);
                $graph4->legend->SetColumns(2);
                // $graph4->legend->SetColor('#4E4E4E','#00A78A');

                $table = new \GTextTable();
                $table->Init(2,count($to_date_gp5)); // Create a 5 rows x 7 columns table
                $data_tbl = array($to_shift_gp5, $to_date_gp5);
                $table->Set($data_tbl);
                // $table->SetPos(40,320);//ตำแหน่งเริ่มต้นของตาราง
                $table->SetPos(40,$tableypos+1);/////////////////////////////////////////////
                $i=0;
                for($i=0;$i<count($to_date_gp5);$i++){
                    // echo $i.'--';
                    $table->MergeCells(1,$i,1,$i+1);
                    $i++;
                }
                $table->SetFont(FF_ANGSA, FS_NORMAL, 12);
                $table->SetAlign('center');
                $table->SetMinColWidth($cellwidth+1.6);//////////////////////////////////////////// 1.75
                // $table->SetMinColWidth($cellwidth);
                // $table->SetNumberFormat('%0.1f');

                // Format table header row
                // $table->SetRowFillColor(0,'teal@0.7');
                $table->SetRowFont(0,FF_ANGSA, FS_NORMAL, 12);  //font ของ row แรก
                $table->SetRowAlign(0,'center');
                    
                $graph4->Add($table);
                $graph4->title->Set("$kfl (พื้นที่ปฏิบัติงาน $kloc) ในเดือน ".date('m/Y',strtotime($ed_date)));
                $graph4->title->SetFont(FF_ANGSA, FS_NORMAL, 18);

                $gp['gp4']['link'][$count_gp] = "graph/".date('Y'). "/" . date('m') . "/mail/FL_5_".$kfl.'-'.$kloc.'_' .  $date . ".jpg";
                $gp['gp4']['path'][$count_gp] = $path . "/FL_5_".$kfl.'-'.$kloc.'_' .  $date . ".jpg";        
                $graph4->Stroke($gp['gp4']['path'][$count_gp]);
                echo $gp['gp4']['path'][$count_gp]."\r\n";
                // dd($gp['gp4']['path'][$count_gp]);
                $count_gp++;
            }
        }
        //แสดง Chart ข้อที่ 5 ---------จบ----------
        // dd('auto FL');

        if(count($fl_log_locate)>0){ 
            $mail_to = Mail::send(new MannualMailFl($fl_log_locate,$st_date,$ed_date,$fl_data,$locate_data,$total_val,$range_date,$gp,$to_gp4,$togp4,$fl_mile));
            return redirect('FL')->with('success', ' Send Mail is complete!');
        }else{
            return redirect('FL')->with('error', ' Send Mail is Error!');
        }
    }
}
