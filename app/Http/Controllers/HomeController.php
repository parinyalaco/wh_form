<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    
    public function index() 
    {
        return view('home.index');
        // return redirect()->route('login.perform');
    }
}
