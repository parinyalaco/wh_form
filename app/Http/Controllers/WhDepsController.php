<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\WhDep;
use Session;

class WhDepsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $whdeps = WhDep::paginate(25);
        return view('whdeps.index',compact('whdeps'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('whdeps.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        WhDep::create($requestData);

        Session::flash('flash_message', ' added!');

        return redirect('whdeps');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $whdep = WhDep::findOrFail($id);

        return view('whdeps.show', compact('whdep'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $whdep = WhDep::findOrFail($id);

        return view('whdeps.edit', compact('whdep'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $whdep = WhDep::findOrFail($id);
        $whdep->update($requestData);

        Session::flash('flash_message', ' updated!');

        return redirect('whdeps');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
