<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Unit;
use App\Models\RawMaterial;
use App\Models\RmCar;
use App\Models\RmTypeReceive;
use App\Models\RmManual;
use App\Models\RmSap;
use App\Models\FileExcel;
use App\Models\RmArea;
use App\Models\RmBroker;
use App\Models\RmFarmer;
use App\Models\RmTypeCar;
use App\Models\RmMailImage;
use App\Models\Crop;
use App\Models\InputItem;
use App\Models\HarvestPlan;
use App\Models\ImportWeightSap;
use App\Models\Planning;
use App\Models\StReturn;
use SimpleXLSX;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;
use App\Mail\ReportWHRM;

class RmManualController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!empty($request->date_from)) {
            $date_from = $request->date_from;
            $date_to = $request->date_to;
        } else {
            $date_sap = RmSap::select('sap_date')->orderBy('sap_date', 'DESC')->limit(1)->get();
            if (count($date_sap)) {
                $date_to = $date_sap[0]['sap_date'];
                $date_from = date("Y-m-d", strtotime("-5 day", strtotime($date_to)));
            } else {
                $date_to = date("Y-m-d");
                $date_from = date("Y-m-d");
            }
        }

        $mn_show = array();
        $sap_show = array();
        $test = $this->show_sap($date_from, $date_to);
        if (!empty($test[0]))    $sap_show = $test[0];

        $test = $this->show_mn($date_from, $date_to);
        if (!empty($test[0]))    $mn_show = $test[0];

        return view('WHRM.index', compact('mn_show', 'sap_show', 'date_from', 'date_to'));
    }

    public function show_sap($date_from, $date_to)
    {
        $query = Unit::all();
        foreach ($query as $key) {
            $unit[$key->id] = $key->name;
        }

        $query = RmFarmer::all();
        foreach ($query as $key) {
            $far[$key->id] = $key->name;
            $far_bk[$key->id] = $key->rm_broker_id;
        }

        $query = RmBroker::all();
        foreach ($query as $key) {
            $broker[$key->id] = $key->name;
        }

        $query = RawMaterial::all();
        foreach ($query as $key) {
            $rm[$key->id] = $key->name;
        }

        $query = RmTypeCar::all();
        foreach ($query as $key) {
            $car[$key->id] = $key->car_detail;
        }

        $date_to2 = date("Y-m-d", strtotime("1 day", strtotime($date_to)));  //เพิ่มไป 1 วัน เพราะเผื่อวัตถุดิบเข้าในอีกวัน
        $sap_set = array();
        $sap_show = array();
        $problem = array();
        $mail_problem = array();
        $sap_set_broker = array();
        $sap_show_broker = array();

        $query = new RmSap;
        $query = $query->selectRaw("sap_date, sap_no, car_name, rm_farmer_id, unit_id, unit_num, time_in, rm_weight, rm_farmer_id,
            rm_weight_agv, raw_material_id, rm_type_car_id");
        $query = $query->whereBetween('sap_date', [$date_from, $date_to2]);
        $query = $query->orderBy('sap_date')->orderBy('sap_no')->orderBy('time_in');
        $query = $query->get();

        foreach ($query as $key) {
            $to_no = intval(substr($key->sap_no, 7, 9));  //no.
            $sap_set[$key->sap_date][$to_no]['car'] = $key->car_name;
            $sap_set[$key->sap_date][$to_no]['car_type'] = $car[$key->rm_type_car_id];
            if (empty($sap_set[$key->sap_date][$to_no]['time_in'])) {
                $sap_set[$key->sap_date][$to_no]['time_in'] = $key->time_in;
            } else {
                if (strtotime($sap_set[$key->sap_date][$to_no]['time_in']) < strtotime('06:00:00')) {
                    if (strtotime($key->time_in) < strtotime('06:00:00')) {
                        if (strtotime($sap_set[$key->sap_date][$to_no]['time_in']) > strtotime($key->time_in)) {
                            $sap_set[$key->sap_date][$to_no]['time_in'] = $key->time_in;
                        }
                    } else {
                        $sap_set[$key->sap_date][$to_no]['time_in'] = $key->time_in;
                    }
                } else {
                    if (strtotime($sap_set[$key->sap_date][$to_no]['time_in']) > strtotime($key->time_in)) {
                        $sap_set[$key->sap_date][$to_no]['time_in'] = $key->time_in;
                    }
                }
            }

            $sap_set[$key->sap_date][$to_no]['unit'] = $unit[$key->unit_id];
            $sap_set[$key->sap_date][$to_no]['rm'] = $rm[$key->raw_material_id];
            $sap_set[$key->sap_date][$to_no]['sap_no'] = $key->sap_no;

            if (empty($sap_set[$key->sap_date][$to_no]['unit_num']))    $sap_set[$key->sap_date][$to_no]['unit_num'] = $key->unit_num;
            else    $sap_set[$key->sap_date][$to_no]['unit_num'] += $key->unit_num;

            if (empty($sap_set[$key->sap_date][$to_no]['rm_weight']))    $sap_set[$key->sap_date][$to_no]['rm_weight'] = round($key->rm_weight, 2);
            else    $sap_set[$key->sap_date][$to_no]['rm_weight'] += round($key->rm_weight, 2);
            if (empty($sap_set[$key->sap_date][$to_no]['num_far']))    $sap_set[$key->sap_date][$to_no]['num_far'] = 1;
            else    $sap_set[$key->sap_date][$to_no]['num_far'] += 1;

            if (empty($sap_set_broker[$key->sap_date][$to_no][$broker[$far_bk[$key->rm_farmer_id]]]))    $sap_set_broker[$key->sap_date][$to_no][$broker[$far_bk[$key->rm_farmer_id]]] = $key->unit_num;
            else    $sap_set_broker[$key->sap_date][$to_no][$broker[$far_bk[$key->rm_farmer_id]]] += $key->unit_num;

            if (round($key->rm_weight_agv, 2) > 18) {
                $problem[$key->sap_date][$to_no][] = $rm[$key->raw_material_id] . '@;' . $broker[$far_bk[$key->rm_farmer_id]] . '@;' . $far[$key->rm_farmer_id] . '@;' . $key->unit_num . ' ' . $unit[$key->unit_id] . '@;' . $key->rm_weight . '@;' . $key->rm_weight_agv;
            }
        }

        $run_no = 0;
        $check_date = '';
        foreach ($sap_set as $kdate => $vdate) {
            foreach ($vdate as $kno => $vno) {
                if ((strtotime($sap_set[$kdate][$kno]['time_in']) > strtotime('06:00:00') && date("Y-m-d", strtotime($kdate)) == $date_from) ||
                    (strtotime($sap_set[$kdate][$kno]['time_in']) < strtotime('06:00:00') && date("Y-m-d", strtotime($kdate)) == $date_to2) ||
                    (date("Y-m-d", strtotime($kdate)) > $date_from && date("Y-m-d", strtotime($kdate)) < $date_to2)
                ) {

                    if (strtotime($sap_set[$kdate][$kno]['time_in']) < strtotime('06:00:00') && date("Y-m-d", strtotime($kdate)) != $date_from) {
                        $set_date = date("Y-m-d", strtotime("-1 day", strtotime($kdate)));
                        if ($check_date == '' || $check_date <> $set_date) {
                            $check_date = $set_date;
                        }
                    } else {
                        if ($check_date == '' || $check_date <> $kdate) {
                            $check_date = $kdate;
                        }
                    }
                    if (empty($sap_show[$check_date])) {
                        $run_no = 1;
                    } else {
                        $run_no = count($sap_show[$check_date]) + 1;
                    }
                    $sap_show[$check_date][$run_no]['car'] = $sap_set[$kdate][$kno]['car'];
                    $sap_show[$check_date][$run_no]['car_type'] = $sap_set[$kdate][$kno]['car_type'];
                    $sap_show[$check_date][$run_no]['time_in'] = $sap_set[$kdate][$kno]['time_in'];
                    $sap_show[$check_date][$run_no]['unit'] = $sap_set[$kdate][$kno]['unit'];
                    $sap_show[$check_date][$run_no]['rm'] = $sap_set[$kdate][$kno]['rm'];
                    $sap_show[$check_date][$run_no]['sap_no'] = $sap_set[$kdate][$kno]['sap_no'];
                    $sap_show[$check_date][$run_no]['unit_num'] = $sap_set[$kdate][$kno]['unit_num'];
                    $sap_show[$check_date][$run_no]['rm_weight'] = $sap_set[$kdate][$kno]['rm_weight'];
                    $sap_show[$check_date][$run_no]['num_far'] = $sap_set[$kdate][$kno]['num_far'];

                    foreach ($sap_set_broker[$kdate][$kno] as $kbk => $vvk) {
                        $sap_show_broker[$check_date][$run_no][$kbk] = $vvk;
                    }
                    if (!empty($problem[$kdate][$kno])) {
                        foreach ($problem[$kdate][$kno] as $kpb => $vpb) {
                            $mail_problem[] = $vpb;
                        }
                    }
                }
            }
        }

        return [$sap_show, $sap_show_broker, $mail_problem];
    }

    public function show_mn($date_from, $date_to)
    {
        $query = RmTypeReceive::all();
        foreach ($query as $key) {
            $type_receive[$key->id] = $key->name;
        }
        $query = RmArea::all();
        foreach ($query as $key) {
            $area[$key->id] = $key->name;
        }
        $mn_show = array();
        $crop = '';
        $query = new RmManual;
        $query = $query->join('file_excels', 'rm_manuals.file_excel_id', '=', 'file_excels.id');
        $query = $query->select(
            'file_excels.crop',
            'rm_manuals.manual_date',
            'rm_manuals.manual_no',
            'rm_manuals.car_name',
            'rm_manuals.rm_car_id',
            'rm_manuals.time_to_laco',
            'rm_manuals.rm_area_code',
            'rm_manuals.rm_area_id',
            'rm_manuals.time_out_area',
            'rm_manuals.time_start_work',
            'rm_manuals.staff_use',
            'rm_manuals.time_use',
            'rm_manuals.rm_type_receive_id',
            'rm_manuals.time_end_work',
            'rm_manuals.note'
        );
        $query = $query->whereBetween('manual_date', [$date_from, $date_to]);
        $query = $query->get();
        foreach ($query as $key) {
            $crop = $key->crop;
            $mn_show[$key->manual_date][$key->manual_no]['zone'] = $key->rm_area_code;
            $mn_show[$key->manual_date][$key->manual_no]['area'] = $area[$key->rm_area_id];
            $mn_show[$key->manual_date][$key->manual_no]['time_out'] = $key->time_out_area;
            $mn_show[$key->manual_date][$key->manual_no]['time_start'] = $key->time_start_work;
            $mn_show[$key->manual_date][$key->manual_no]['staff_use'] = $key->staff_use;

            $mn_show[$key->manual_date][$key->manual_no]['car'] = $key->car_name;
            $mn_show[$key->manual_date][$key->manual_no]['rm_type'] = $type_receive[$key->rm_type_receive_id];
            $mn_show[$key->manual_date][$key->manual_no]['time_end'] = $key->time_end_work;
            $mn_show[$key->manual_date][$key->manual_no]['time_use'] = $key->time_use;
            $mn_show[$key->manual_date][$key->manual_no]['time_in'] = $key->time_to_laco;
            $mn_show[$key->manual_date][$key->manual_no]['note'] = $key->note;
        }

        $chk_img = array();
        $show_img = array();

        $chk_img = RmMailImage::whereBetween('img_date', [$date_from, $date_to])->orderBy('img_date')->orderBy('seq')->get();
        if (count($chk_img) > 0) {
            foreach ($chk_img as $key) {
                $show_img[date('d-m-Y', strtotime($key->img_date))][$key->id] = date('Ymd', strtotime($key->img_date)) . '/' . $key->image;
            }
        }

        return [$mn_show, $crop, $show_img];
    }

    public function importExportView()
    {
        $date_to = date("Y-m-d", strtotime("-1 day"));
        $mail_img = array();
        $mail_img = RmMailImage::where('img_date', $date_to)->orderBy('seq')->get();
        $img_date = date("Y-m-d", strtotime("-1 day"));
        $tmpfolder = date('Ymd', strtotime($img_date));
        $completedirectory = 'storage/app/public/WhRM/completes/img/' . $tmpfolder . '/';
        return view('WHRM.import', compact('mail_img', 'completedirectory'));
    }

    public function import_sap(Request $request)
    {
        $requestData = $request->all();
        $completedirectory = config('myconfig.directory.whrm.completes.sap');

        if ($request->hasFile('file_sap')) {
            $tmpfolder = date('Ymd');
            if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
            }

            $zipfile = $request->file('file_sap');
            $uploadname = $zipfile->getClientOriginalName();
            $name = md5($zipfile->getClientOriginalName() . time()) . '-upload.' . $zipfile->getClientOriginalExtension();
            $destinationPath = $completedirectory . "/" . $tmpfolder;
            $zipfile->move($destinationPath, $name);

            $uploadpath = $completedirectory . "/" . $tmpfolder  . "/" . $name;
            $uploadfile = $destinationPath  . "/" . $name;

            if ($xlsx = SimpleXLSX::parse($uploadfile)) {
                $query = RawMaterial::all();
                foreach ($query as $key) {
                    $rm[$key->name] = $key->id;
                }
                $query = RmFarmer::join('rm_brokers', 'rm_farmers.rm_broker_id', '=', 'rm_brokers.id')
                    ->select('rm_farmers.id', 'rm_farmers.name', 'rm_farmers.rm_broker_id', 'rm_brokers.name AS broker_name')->get();
                foreach ($query as $key) {
                    $far[$key->name][$key->broker_name] = $key->id;
                    $broker[$key->broker_name] = $key->rm_broker_id;
                }

                $query = Unit::all();
                foreach ($query as $key) {
                    $unit[$key->name] = $key->id;
                }

                $query = RmTypeCar::all();
                foreach ($query as $key) {
                    $type_car[$key->car_no][$key->car_detail] = $key->id;
                }

                $add_excel = array();
                $add_excel['name'] = $uploadname;
                $file_id = FileExcel::create($add_excel)->id;

                $i = 0;
                $add_rt = array();
                foreach ($xlsx->rows(0) as $r => $row) {    //ทำงานแต่ใน sheet แรก
                    if ($r > 0 && !empty(trim($row[2]))) {
                        $add_sap = array();
                        $add_sap['file_excel_id'] = $file_id;

                        if (empty($far[$row[5]][$row[4]])) {
                            if (empty($broker[$row[4]])) {
                                $add_broker['name'] = $row[4];
                                $broker_id = RmBroker::create($add_broker)->id;
                                $broker[$row[4]] = $broker_id;
                            } else {
                                $broker_id = $broker[$row[4]];
                            }
                            $add_far['name'] = $row[5];
                            $add_far['rm_broker_id'] = $broker_id;
                            $far_id = RmFarmer::create($add_far)->id;
                            $far[$row[5]][$row[4]] = $far_id;
                        } else {
                            $far_id = $far[$row[5]][$row[4]];
                        }
                        $add_sap['rm_farmer_id'] = $far_id;

                        $chk_sap = RmSap::where('sap_date', date('Y-m-d', strtotime($row[0])))
                            ->where('sap_no', $row[2])
                            ->where('rm_farmer_id', $far_id)
                            ->where('time_in', date('H:i:s', strtotime($row[12])))
                            ->where('change_time', date('H:i:s', strtotime($row[25])))
                            ->count();
                        if ($chk_sap == 0) {
                            $add_sap['sap_date'] = date('Y-m-d', strtotime($row[0]));
                            $add_sap['sap_no'] = $row[2];
                        }

                        //   echo $chk_sap ;

                        if (empty($rm[$row[1]])) {
                            $add_rm['name'] = $row[1];
                            $rm_id = RawMaterial::create($add_rm)->id;
                            $rm[$row[1]] = $rm_id;
                        } else {
                            $rm_id = $rm[$row[1]];
                        }
                        $add_sap['raw_material_id'] = $rm_id;

                        $add_sap['car_name'] = $row[3];

                        if (empty($add_rt[$row[2]][date('Y-m-d', strtotime($row[0]))][$broker[$row[4]]])) {
                            $add_rt[$row[2]][date('Y-m-d', strtotime($row[0]))][$broker[$row[4]]] = $row[6];
                        } else {
                            $add_rt[$row[2]][date('Y-m-d', strtotime($row[0]))][$broker[$row[4]]] += $row[6];
                        }

                        if (empty($unit[$row[7]])) {
                            $add_unit['name'] = $row[7];
                            $unit_id = Unit::create($add_unit)->id;
                            $unit[$row[7]] = $unit_id;
                        } else {
                            $unit_id = $unit[$row[7]];
                        }
                        $add_sap['unit_id'] = $unit_id;

                        $add_sap['unit_num'] = $row[6];
                        $add_sap['rm_weight'] = $row[8];
                        $add_sap['rm_weight_agv'] = $row[9];
                        $add_sap['rm_price'] = $row[10];

                        if (empty($type_car[$row[11]][$row[14]])) {
                            $add_tc['car_no'] = $row[11];
                            $add_tc['car_detail'] = $row[14];
                            $type_car_id = RmTypeCar::create($add_tc)->id;
                            $type_car[$row[11]][$row[14]] = $type_car_id;
                        } else {
                            $type_car_id = $type_car[$row[11]][$row[14]];
                        }
                        $add_sap['rm_type_car_id'] = $type_car_id;

                        $add_sap['time_in'] = date('H:i:s', strtotime($row[12]));
                        $add_sap['rm_weight_sum'] = $row[13];
                        $add_sap['remark'] = $row[15];
                        $add_sap['customer'] = $row[16];
                        $add_sap['rm_weight_first'] = $row[17];
                        $add_sap['rm_weight_last'] = $row[18];
                        $add_sap['weight_type'] = $row[19];
                        $add_sap['weight_time_in'] = date('H:i:s', strtotime($row[20]));
                        $add_sap['weight_time_out'] = date('H:i:s', strtotime($row[21]));
                        $add_sap['requester'] = $row[22];
                        $add_sap['change_by'] = $row[23];
                        $add_sap['change_date'] = date('Y-m-d', strtotime($row[24]));
                        $add_sap['change_time'] = date('H:i:s', strtotime($row[25]));
                        // dd($add_sap);
                        if ($chk_sap > 0) {
                            RmSap::where('sap_date', date('Y-m-d', strtotime($row[0])))->where('sap_no', $row[2])
                                ->where('time_in', date('H:i:s', strtotime($row[12])))->update($add_sap);
                        } else {
                            RmSap::create($add_sap);
                        }
                    }
                }

                // dd($add_rt);
                // $add_rt[$row[2]][date('Y-m-d',strtotime($row[0]))][$broker[$row[4]]]
                // foreach ($add_rt as $kno => $vno) {
                //     foreach ($vno as $kdate => $vdate) {
                //         foreach ($vdate as $kbk => $vbk) {
                //             //add st_return
                //             $add_st_return = array();
                //             $add_st_return['no_id'] = $kno;
                //             $add_st_return['rm_broker_id'] = $kbk;
                //             $add_st_return['st_product_id'] = 1;
                //             $add_st_return['return_date'] = date('Y-m-d',strtotime($kdate));
                //             $add_st_return['amount'] = $vbk;
                //             $st_return = StReturn::where('no_id', $kno)->where('return_date', date('Y-m-d',strtotime($kdate)))->get();
                //             if(count($st_return)>0){
                //                 $st_return = StReturn::where('no_id', $kno)->where('return_date', date('Y-m-d',strtotime($kdate)))->update($add_st_return);
                //             }else{
                //                 StReturn::create($add_st_return);
                //             }
                //         }
                //     }
                // }


                return back()->with('success', 'Upload Sap File successfully');
            } else {
                return back()->with('error', "Can't read file!");
            }
        } else {
            return back();
        }
    }

    public function export_manual(Request $request)
    {
        $date_from = date("Y-m-d", strtotime($request->date_from));
        $date_to = date("Y-m-d", strtotime($request->date_to));

        $sap_show = array();
        $test = $this->show_sap($date_from, $date_to);
        if (!empty($test[0]))    $sap_show = $test[0];

        if (count($sap_show) > 0) {
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            $sheet->setCellValueByColumnAndRow(1, 1, "กรุณาระบุชื่อ crop ในช่องสี เหลือง");
            $sheet->mergeCellsByColumnAndRow(1, 1, 3, 1);
            $sheet->mergeCellsByColumnAndRow(4, 1, 6, 1);
            $sheet->getStyle('D1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF33');

            $sheet->setCellValueByColumnAndRow(1, 2, "วันที่");
            $sheet->setCellValueByColumnAndRow(2, 2, "ลำดับที่");
            $sheet->setCellValueByColumnAndRow(3, 2, "รถทะเบียน");
            $sheet->setCellValueByColumnAndRow(4, 2, "ขนาดรถ");
            $sheet->setCellValueByColumnAndRow(5, 2, "จำนวนกระสอบ");
            $sheet->setCellValueByColumnAndRow(6, 2, "น้ำหนัก");
            $sheet->setCellValueByColumnAndRow(7, 2, "เวลารถถึง LACO");
            $sheet->setCellValueByColumnAndRow(8, 2, "เขต");
            $sheet->setCellValueByColumnAndRow(9, 2, "พื้นที่เขต");
            $sheet->setCellValueByColumnAndRow(10, 2, "เวลารถออกจากพื้นที่");
            $sheet->setCellValueByColumnAndRow(11, 2, "เวลาลงวัตถุดิบ");
            $sheet->setCellValueByColumnAndRow(12, 2, "ลงเสร็จ");
            $sheet->setCellValueByColumnAndRow(13, 2, "จำนวนชั่วโมง");
            $sheet->setCellValueByColumnAndRow(14, 2, "จำนวนพนง.");
            $sheet->setCellValueByColumnAndRow(15, 2, "ประเภทการลง");
            $sheet->setCellValueByColumnAndRow(16, 2, "หมายเหตุ");

            $sheet->getStyle('A2:P2')->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A2:P2')->getAlignment()->setVertical('center');
            $sheet->getStyle('A2:P2')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');

            $startrow = 3;
            foreach ($sap_show as $kdate => $vdate) {
                foreach ($vdate as $kno => $vno) {
                    $startcol = 1;
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, $kdate);
                    $startcol++;
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, $kno);
                    $startcol++;
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, $sap_show[$kdate][$kno]['car']);
                    $startcol++;
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, $sap_show[$kdate][$kno]['car_type']);
                    $startcol++;
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, round($sap_show[$kdate][$kno]['unit_num']));
                    $startcol++;
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, round($sap_show[$kdate][$kno]['rm_weight'], 2));
                    $startcol++;
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, date('H:i:s', strtotime($sap_show[$kdate][$kno]['time_in'])));

                    $startrow++;
                }
            }

            $sheet->getStyle('A2:P' . ($startrow - 1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

            $sheet->getStyle('A3:G' . ($startrow - 1))->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('DCE6F1');

            for ($i = 1; $i <= 17; $i++) {
                $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($i);
                $sheet->getColumnDimension($columnID)->setAutoSize(true);
            }

            $writer = new Xlsx($spreadsheet);
            $filename = "rm_manual-" . date('yymmdd-hi') . ".xlsx";
            $writer->save('storage/' . $filename);
            return response()->download('storage/' . $filename);
        } else {
            return back()->with('error', 'No data!');
        }
    }

    public function import_manual(Request $request)
    {
        $requestData = $request->all();
        $completedirectory = config('myconfig.directory.whrm.completes.manual');

        if ($request->hasFile('file_manual')) {
            $tmpfolder = date('Ymd');
            if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
            }

            $zipfile = $request->file('file_manual');
            $uploadname = $zipfile->getClientOriginalName();
            $name = md5($zipfile->getClientOriginalName() . time()) . '-upload.' . $zipfile->getClientOriginalExtension();
            $destinationPath = $completedirectory . "/" . $tmpfolder;
            $zipfile->move($destinationPath, $name);

            $uploadpath = $completedirectory . "/" . $tmpfolder  . "/" . $name;
            $uploadfile = $destinationPath  . "/" . $name;

            if ($xlsx = SimpleXLSX::parse($uploadfile)) {
                $count_col = 0;
                foreach ($xlsx->rows()[1] as $key => $value) {
                    if (!empty($value)) $count_col++;
                    else    break;
                };

                if ($count_col == 16) {
                    $er = array();
                    $s_date = array();
                    $chk_data = array();
                    $chk_date = '';
                    $i = 0;
                    foreach ($xlsx->rows(0) as $r => $row) {    //ทำงานแต่ใน sheet แรก
                        if ($r == 0) $crop_name = $row[3];
                        if ($r > 1 && !empty(trim($row[0]))) {
                            if (empty($row[1]) || empty($row[2]) || empty($row[4]) || empty($row[5]) || empty($row[6]) || empty($row[12]) || empty($row[13]) || empty($row[14])) {
                                $er['no-data'][$r] = $r;
                            } else {
                                if (date('Y', strtotime($row[0])) > 2500) {
                                    $y = date('Y', strtotime($row[0])) - 543;
                                    $chk_date = date('Y-m-d', strtotime($y . '-' . date('m-d', strtotime($row[0]))));
                                } else {
                                    $chk_date = date('Y-m-d', strtotime($row[0]));
                                }
                                if (empty($s_date['min']) || strtotime($s_date['min']) > strtotime($chk_date)) {
                                    $s_date['min'] = $chk_date;
                                }
                                if (empty($s_date['max']) || strtotime($s_date['max']) < strtotime($chk_date)) {
                                    $s_date['max'] = $chk_date;
                                }

                                $col_4 = intval(str_replace(',', '', $row[4]));
                                $col_5 = floatval(str_replace(',', '', $row[5]));
                                $col_6 = date('H:i:s', strtotime($row[6]));
                                $col_9 = date('H:i:s', strtotime($row[9]));
                                $col_10 = date('H:i:s', strtotime($row[10]));
                                $col_11 = date('H:i:s', strtotime($row[11]));
                                $col_12 = date('H:i:s', strtotime($row[12]));
                                $chk_data[$r] = $chk_date . '^&' . $row[1] . '^&' . $row[2] . '^&' . $col_4 . '^&' . $col_5 . '^&' . $col_6 . '^&' . $row[7] . '^&' . $row[8] . '^&' . $col_9 . '^&' . $col_10 . '^&' . $col_11 . '^&' . $col_12 . '^&' . $row[13] . '^&' . $row[14] . '^&' . $row[15];
                            }
                        }
                    }
                    //check จำนวนบรรทัดว่าเท่ากับ SAP มั้ย
                    $sap_show = array();
                    $test = $this->show_sap($s_date['min'], $s_date['max']);
                    if (!empty($test[0]))    $sap_show = $test[0];
                    for ($i = strtotime($s_date['min']); $i <= strtotime($s_date['max']); $i = $i + 86400) {
                        $thisDate = date('Y-m-d', $i);
                        $input = preg_quote($thisDate, '~'); // don't forget to quote input string!
                        $result = preg_grep('~' . $input . '~', $chk_data);
                        if (count($sap_show[$thisDate]) != count($result)) {
                            $er['no-row'][$thisDate][0] = count($sap_show[$thisDate]);
                            $er['no-row'][$thisDate][1] = count($result);
                        }
                    }

                    if (count($chk_data) > 0 && count($sap_show) > 0) {
                        foreach ($chk_data as $key => $value) {
                            $ex_data = explode('^&', $value);
                            $exp_data[$ex_data[0]][$ex_data[1]][$ex_data[2]][$ex_data[3]][$ex_data[4]][$ex_data[5]] = $key;
                        }
                        foreach ($exp_data as $kdate => $vdate) {
                            foreach ($vdate as $kno => $vno) {
                                foreach ($vno as $kcar => $vcar) {
                                    foreach ($vcar as $knum => $vnum) {
                                        foreach ($vnum as $kw => $vw) {
                                            foreach ($vw as $ktime => $vtime) {
                                                $chk_car = $sap_show[$kdate][$kno]['car'];
                                                $chk_num = $sap_show[$kdate][$kno]['unit_num'];
                                                $chk_weight = $sap_show[$kdate][$kno]['rm_weight'];
                                                $chk_time = date('H:i:s', strtotime($sap_show[$kdate][$kno]['time_in']));
                                                if ($kcar != $chk_car || $knum != $chk_num || $kw != round($chk_weight, 2) || $ktime != $chk_time) {
                                                    $er['no-match'][$vtime] = $vtime;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (count($er) > 0) {
                        return view('WHRM.not_match', compact('er'));
                    } else {
                        if (count($chk_data) > 0) {

                            $query = RmCar::all();
                            foreach ($query as $key) {
                                $car_size[$key->name] = $key->id;
                            }
                            $query = RmArea::all();
                            foreach ($query as $key) {
                                $area[$key->name] = $key->id;
                            }

                            $query = RmTypeReceive::all();
                            foreach ($query as $key) {
                                $tr[$key->name] = $key->id;
                            }

                            $add_excel = array();
                            $add_excel['name'] = $uploadname;
                            $add_excel['crop'] = $crop_name;
                            $file_id = FileExcel::create($add_excel)->id;
                            foreach ($chk_data as $key => $value) {
                                $add_manual = array();
                                $add_manual['file_excel_id'] = $file_id;

                                $ex_data = explode('^&', $value);
                                $chk_mn = RmManual::where('manual_date', $ex_data[0])->where('manual_no', $ex_data[1])->count();
                                if ($chk_mn == 0) {
                                    $add_manual['manual_date'] = $ex_data[0];
                                    $add_manual['manual_no'] = $ex_data[1];
                                }

                                $add_manual['car_name'] = $ex_data[2];
                                $add_manual['rm_area_code'] = $ex_data[6];

                                if (empty($area[$ex_data[7]])) {
                                    $add_area['name'] = $ex_data[7];
                                    $area_id = RmArea::create($add_area)->id;
                                    $area[$ex_data[7]] = $area_id;
                                } else {
                                    $area_id = $area[$ex_data[7]];
                                }
                                $add_manual['rm_area_id'] = $area_id;

                                $add_manual['manual_num'] = $ex_data[3];
                                $add_manual['manual_weight'] = str_replace(',', '', $ex_data[4]);
                                $add_manual['time_out_area'] = date('H:i:s', strtotime($ex_data[8]));
                                $add_manual['time_to_laco'] = date('H:i:s', strtotime($ex_data[5]));
                                $add_manual['time_start_work'] = date('H:i:s', strtotime($ex_data[9]));
                                $add_manual['time_end_work'] = date('H:i:s', strtotime($ex_data[10]));
                                $add_manual['time_use'] = date('H:i:s', strtotime($ex_data[11]));
                                $add_manual['staff_use'] = $ex_data[12];

                                if (empty($tr[$ex_data[13]])) {
                                    $add_tr['name'] = $ex_data[13];
                                    $tr_id = RmTypeReceive::create($add_tr)->id;
                                    $tr[$ex_data[13]] = $tr_id;
                                } else {
                                    $tr_id = $tr[$ex_data[13]];
                                }
                                $add_manual['rm_type_receive_id'] = $tr_id;
                                if (!empty($ex_data[14]))    $add_manual['note'] = $ex_data[14];
                                if ($chk_mn == 0) {
                                    RmManual::create($add_manual);
                                } else {
                                    RmManual::where('manual_date', $ex_data[0])->where('manual_no', $ex_data[1])->update($add_manual);
                                }
                            }
                            return back()->with('success', 'Upload Manual File successfully');
                        } else {
                            return back()->with('error', "No data!");
                        }
                    }
                } else {
                    return back()->with('error', "Invalid column!");
                }
            } else {
                return back()->with('error', "Can't read file!");
            }
        } else {
            return back();
        }
    }

    public function import_comment(Request $request)
    {
        $requestData = $request->all();
        $completedirectory = config('myconfig.directory.whrm.completes.img');
        if ($request->hasFile('file_comment')) {
            $zipfile = $request->file('file_comment');
            $uploadname = $zipfile->getClientOriginalName();
            $file_type = strrchr($uploadname, ".");
            if ($file_type == '.jpg' || $file_type == '.png') {
                $name_file = strrpos($uploadname, '.'); //หาจำนวนตัวอักษรก่อน . สุดท้าย
                $name_file2 = substr($uploadname, 0, $name_file);   //ตัดเอาเฉพาะชื่อไฟล์
                $exp_name = explode('-', $name_file2);  //วัน-เดือน-ปี ค.ศ.-ลำดับรูป
                if (count($exp_name) == 4) {
                    $img_date = $exp_name[2] . '-' . sprintf('%02d', $exp_name[1]) . '-' . sprintf('%02d', $exp_name[0]);
                    $chk_num = $exp_name[3];
                } else {
                    $img_date = date("Y-m-d", strtotime("-1 day"));
                    $chk_num = $name_file2;
                }
                $chk_img = RmMailImage::where('img_date', $img_date)->where('image', $uploadname)->get();
                if (count($chk_img) > 0) {
                    return back()->with('error', 'ชื่อไฟล์ซ้ำกับที่มีอยู่!!');
                } else {
                    if (is_numeric($chk_num) && $chk_num < 2147483647) {
                        $tmpfolder = date('Ymd', strtotime($img_date));
                        if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                            mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
                        }
                        $destinationPath = $completedirectory . '/' . $tmpfolder . '/';
                        $zipfile->move($destinationPath, $uploadname);

                        $add_img = array();
                        $add_img['img_date'] = $img_date;
                        $add_img['seq'] = $chk_num;
                        $add_img['image'] = $uploadname;
                        $mail_img = RmMailImage::create($add_img);
                        return back()->with('success', 'Upload Image Successfully');
                    } else {
                        return back()->with('error', 'รูปแบบชื่อของไฟล์ต้องเป็น วัน-เดือน-ปี ค.ศ.-ลำดับรูป หรือ ตัวเลข เท่านั้น!!');
                    }
                }
            } else {
                return back()->with('error', 'ไฟล์ต้องเป็นนามสกุล .jpg หรือ .png เท่านั้น!!');
            }
        } else {
            return back();
        }
    }

    public function reportView()
    {
        $crop = Crop::orderBy('id', 'DESC')->get();
        return view('WHRM.report', compact('crop'));
    }

    public function report_export(Request $request)
    {
        $report_id = $request->report_id;
        $sent_date = date("Y-m-d", strtotime($request->st_date));

        if ($report_id == 1) {
            $path_back = 'wh_rm_report';

            $sap_show = array();
            $sap_show_broker = array();
            $mail_problem = array();
            $mn_show = array();
            $crop = "";
            $show_img = array();

            $test = $this->show_sap($sent_date, $sent_date);
            $sap_show = $test[0];
            if (!empty($test[1]))    $sap_show_broker = $test[1];
            $mail_problem = $test[2];


            $test = $this->show_mn($sent_date, $sent_date);
            $mn_show = $test[0];
            $crop = $test[1];
            $show_img = $test[2];
            // dd($show_img);

            $test = $this->cal_tomail($sap_show, $sap_show_broker, $mn_show);
            $to_report = $test[0];
            $tb_receipt = $test[1];
            $tb_receipt_gp = $test[2];

            $format_setdate = date("d-m-Y", strtotime($sent_date));
            return view('WHRM.mail', compact('format_setdate', 'to_report', 'tb_receipt', 'tb_receipt_gp', 'mail_problem', 'crop', 'path_back', 'show_img'));
        } else {
            $ed_date = date("Y-m-d", strtotime($request->ed_date));
            if ($report_id == 2) {
                $date_from = date("Y-m-d", strtotime($sent_date));
                $date_to = date("Y-m-d", strtotime($ed_date));

                $sap_show = array();
                $test = $this->show_sap($date_from, $date_to);
                if (!empty($test[0]))    $sap_show = $test[0];

                $mn_show = array();
                $test = $this->show_mn($date_from, $date_to);
                if (!empty($test[0]))    $mn_show = $test[0];

                if (count($sap_show) > 0 && count($mn_show) > 0) {
                    $test = $this->report_2($sent_date, $ed_date, $sap_show, $mn_show);
                    $rp_2 = $test[0];
                    return response()->download($rp_2);
                } else {
                    return back()->with('error', 'No data!');
                }
            } elseif ($report_id == 3) {
                $crop = $request->crop_id;
                $test = $this->report_3($sent_date, $ed_date, $crop);
                $sap_show = $test[0];
                return response()->download($sap_show);
            }
        }
    }

    public function report_2($sent_date, $ed_date, $sap_show, $mn_show)
    {
        $date_from = date("Y-m-d", strtotime($sent_date));
        $date_to = date("Y-m-d", strtotime($ed_date));

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValueByColumnAndRow(1, 1, "การลงวัตถุดิบถั่วแระ ระหว่างวันที่ " . date("d-m-Y", strtotime($date_from)) . " ถึงวันที่ " . date("d-m-Y", strtotime($date_to)));
        $sheet->mergeCellsByColumnAndRow(1, 1, 17, 1);

        $sheet->setCellValueByColumnAndRow(1, 2, "วันที่");
        $sheet->setCellValueByColumnAndRow(2, 2, "ลำดับที่");
        $sheet->setCellValueByColumnAndRow(3, 2, "ชื่อวัตถุดิบ");
        $sheet->setCellValueByColumnAndRow(4, 2, "รถทะเบียน");
        $sheet->setCellValueByColumnAndRow(5, 2, "ขนาดรถ");
        $sheet->setCellValueByColumnAndRow(6, 2, "จำนวนกระสอบ");
        $sheet->setCellValueByColumnAndRow(7, 2, "น้ำหนัก");
        $sheet->setCellValueByColumnAndRow(8, 2, "เวลารถถึง LACO");
        $sheet->setCellValueByColumnAndRow(9, 2, "เขต");
        $sheet->setCellValueByColumnAndRow(10, 2, "พื้นที่เขต");
        $sheet->setCellValueByColumnAndRow(11, 2, "เวลารถออกจากพื้นที่");
        $sheet->setCellValueByColumnAndRow(12, 2, "เวลาลงวัตถุดิบ");
        $sheet->setCellValueByColumnAndRow(13, 2, "ลงเสร็จ");
        $sheet->setCellValueByColumnAndRow(14, 2, "จำนวนชั่วโมง");
        $sheet->setCellValueByColumnAndRow(15, 2, "จำนวนพนง.");
        $sheet->setCellValueByColumnAndRow(16, 2, "ประเภทการลง");
        $sheet->setCellValueByColumnAndRow(17, 2, "หมายเหตุ");

        $sheet->getStyle('A2:Q2')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A2:Q2')->getAlignment()->setVertical('center');
        $sheet->getStyle('A2:Q2')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');

        $startrow = 3;
        foreach ($sap_show as $kdate => $vdate) {
            foreach ($vdate as $kno => $vno) {
                $startcol = 1;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $kdate);
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $kno);
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $sap_show[$kdate][$kno]['rm']);
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $sap_show[$kdate][$kno]['car']);
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $sap_show[$kdate][$kno]['car_type']);
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, number_format($sap_show[$kdate][$kno]['unit_num']));
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, round($sap_show[$kdate][$kno]['rm_weight'], 2));
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, date('H:i:s', strtotime($sap_show[$kdate][$kno]['time_in'])));


                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $mn_show[$kdate][$kno]['zone']);
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $mn_show[$kdate][$kno]['area']);
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, date('H:i:s', strtotime($mn_show[$kdate][$kno]['time_out'])));
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, date('H:i:s', strtotime($mn_show[$kdate][$kno]['time_start'])));
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, date('H:i:s', strtotime($mn_show[$kdate][$kno]['time_end'])));
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, date('H:i:s', strtotime($mn_show[$kdate][$kno]['time_use'])));
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $mn_show[$kdate][$kno]['staff_use']);
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $mn_show[$kdate][$kno]['rm_type']);
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $mn_show[$kdate][$kno]['note']);

                $startrow++;
            }
        }

        $sheet->getStyle('A2:Q' . ($startrow - 1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

        for ($i = 1; $i <= 19; $i++) {
            $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($i);
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $writer = new Xlsx($spreadsheet);
        $filename = "rm_manual-" . date('yymmdd-hi') . ".xlsx";
        $completedirectory = 'storage/app/public/WhRM/completes/export_mn/';
        $tmpfolder = date('Ymd');
        if (!is_dir($completedirectory . '/' . $tmpfolder)) {
            mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
        }
        $writer->save($completedirectory . '/' . $tmpfolder . '/' . $filename);
        $path_file = $completedirectory . '/' . $tmpfolder . '/' . $filename;
        return [$path_file];
    }

    public function report_3($sent_date, $ed_date, $crop)
    {
        $date_from = date("Y-m-d", strtotime($sent_date));
        $date_to = date("Y-m-d", strtotime($ed_date));

        $query = Crop::all();
        foreach ($query as $key) {
            $crop_name[$key->id] = $key->name;
        }

        $to_show = array();
        $havest = array();
        //plan havest
        $havest = Planning::join('input_items', 'plannings.item_input_id', '=', 'input_items.id')
            ->where('crop_id', $crop)
            ->selectRaw('input_items.name, input_items.tradename, SUM(plannings.plan_value) AS plan_value')
            ->groupBy('input_items.name', 'input_items.tradename')->get();
        if (count($havest) > 0) {
            foreach ($havest as $key) {
                $to_show[$key->name . ' ' . $key->tradename]['plan'] = $key->plan_value;
                // $to_show[$key->name.' '.$key->tradename]['plan_sap'] = $key->net_wg;
            }
        }

        $sap_show = array();
        $sap_show_broker = array();
        $test = $this->show_sap($date_from, $date_to);
        if (!empty($test[0])) {
            $sap_show = $test[0];
            // dd($sap_show);
            if (!empty($test[1]))    $sap_show_broker = $test[1];
        }

        // คำนวนจำนวนรวมใหม่ fn show_sap ไม่ได้ทำแยก rm ไว้
        $query = RawMaterial::all();
        foreach ($query as $key) {
            $rm[$key->id] = $key->name;
        }

        $sap_set_r = array();
        $query = new RmSap;
        $query = $query->selectRaw("sap_date, sap_no, time_in, rm_weight, raw_material_id");
        $query = $query->whereBetween('sap_date', [$date_from, $date_to]);
        $query = $query->orderBy('sap_date')->orderBy('sap_no')->orderBy('time_in');
        $query = $query->get();

        //no.
        $to_no = 0;
        foreach ($query as $key) {

            if (empty($to_show[$rm[$key->raw_material_id]]['receive'])) {
                $to_show[$rm[$key->raw_material_id]]['receive'] = round($key->rm_weight, 2);
            } else {
                $to_show[$rm[$key->raw_material_id]]['receive'] += round($key->rm_weight, 2);
            }
        }

        $mn_show = array();
        $test = $this->show_mn($date_from, $date_to);
        if (!empty($test[0])) {
            $mn_show = $test[0];
        }

        if (count($to_show) > 0) {
            $to_report = array();
            $tb_receipt = array();
            $test = $this->cal_tomail($sap_show, $sap_show_broker, $mn_show);
            $to_report = $test[0];
            $tb_receipt = $test[1];
            $area_time = $test[3];

            $spreadsheet = new Spreadsheet();
            $count_sheet = 0;


            $sheet = $spreadsheet->getActiveSheet();

            //ปริมาณรับ
            $sheet->setCellValueByColumnAndRow(1, 1, "ปริมาณการรับวัตถุดิบถั่วแระ " . $crop_name[$crop]);
            $sheet->mergeCellsByColumnAndRow(1, 1, 3, 1);

            $sheet->setCellValueByColumnAndRow(1, 2, "พันธุ์");
            $sheet->setCellValueByColumnAndRow(2, 2, "เป้าผลผลิต");
            $sheet->setCellValueByColumnAndRow(3, 2, "รับจริง");

            $sheet->getStyle('A2:C2')->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A2:C2')->getAlignment()->setVertical('center');
            $sheet->getStyle('A2:C2')->getFont()->setBold(true);
            $sheet->getStyle('A2:C2')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');

            $startrow = 3;
            foreach ($to_show as $krm => $vrm) {
                $startcol = 1;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $krm);
                $startcol++;
                if (!empty($to_show[$krm]['plan']))  $sheet->setCellValueByColumnAndRow($startcol, $startrow, round($to_show[$krm]['plan'], 2));
                $startcol++;
                if (!empty($to_show[$krm]['receive'])) $sheet->setCellValueByColumnAndRow($startcol, $startrow, round($to_show[$krm]['receive'], 2));

                $startrow++;
            }

            $startcol = 1;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, 'รวม');
            for ($i = 2; $i < 4; $i++) {
                $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($i);
                $sheet->setCellValueByColumnAndRow($i, $startrow, '=SUM(' . $columnID . '3:' . $columnID . ($startrow - 1) . ')');
            }

            $sheet->getStyle('A' . $startrow)->getAlignment()->setHorizontal('right');
            $sheet->getStyle('A' . $startrow . ':C' . $startrow)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');

            $sheet->getStyle('A2:C' . $startrow)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
            $sheet->getStyle('B3:C' . $startrow)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

            for ($i = 1; $i < 4; $i++) {
                $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($i);
                $sheet->getColumnDimension($columnID)->setAutoSize(true);
            }

            $startrow++;
            $sheet->setCellValueByColumnAndRow(1, $startrow, "** ข้อมูล 'เป้าผลผลิต' ดึงจาก planning ของส่งเสริมฯ ** ");
            $sheet->mergeCellsByColumnAndRow(1, $startrow, 3, $startrow);
            $sheet->getStyle('A' . $startrow)->getFont()->getColor()->setRGB('FF0000');

            $spreadsheet->getActiveSheet()->setTitle('ปริมาณรับเข้า');

            $count_sheet++;
            $spreadsheet->createSheet();
            $sheet = $spreadsheet->setActiveSheetIndex($count_sheet);
            $startrow = 1;

            //เครื่อง+คน
            $rec_time = ['< 10 นาที', '< 15 นาที', '< 20 นาที', '> 20 นาที'];
            $sheet->setCellValueByColumnAndRow(1, 1, "เปรียบเทียบการรับวัตถุดิบ ระหว่างเครื่องเรียงกระสอบกับพนักงานยก");
            $sheet->mergeCellsByColumnAndRow(1, 1, 6, 1);
            $startrow++;

            $st_2 = $startrow;
            $sheet->setCellValueByColumnAndRow(1, $startrow, "การรับวัตถุดิบ");
            $startcol = 2;
            foreach ($rec_time as $key => $value) {
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $value);
                $startcol++;
            }
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, "รวม");

            $sheet->getStyle('A' . $st_2 . ':F' . $st_2)->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A' . $st_2 . ':F' . $st_2)->getAlignment()->setVertical('center');
            $sheet->getStyle('A' . $st_2 . ':F' . $st_2)->getFont()->setBold(true);
            $sheet->getStyle('A' . $st_2 . ':F' . $st_2)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');
            $startrow++;

            foreach ($tb_receipt as $krec => $vrec) {
                $startcol = 1;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $krec);
                foreach ($vrec as $ktime => $vtime) {
                    $startcol++;
                    if (!empty($tb_receipt[$krec][$ktime]))  $sheet->setCellValueByColumnAndRow($startcol, $startrow, $tb_receipt[$krec][$ktime]);
                }
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, '=SUM(B' . $startrow . ':E' . $startrow . ')');
                $startrow++;
            }

            $sheet->getStyle('A' . ($startrow - 1))->getAlignment()->setHorizontal('right');
            $sheet->getStyle('A' . ($startrow - 1) . ':F' . ($startrow - 1))->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');

            for ($i = 1; $i < 7; $i++) {
                $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($i);
                $sheet->getColumnDimension($columnID)->setAutoSize(true);
            }

            $sheet->getStyle('A' . $st_2 . ':F' . ($startrow - 1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);


            $spreadsheet->getActiveSheet()->setTitle('เปรียบเทียบเครื่อง+คน');


            $count_sheet++;
            $spreadsheet->createSheet();
            $sheet = $spreadsheet->setActiveSheetIndex($count_sheet);
            $startrow = 1;

            //ช่วงเวลาเข้า   $to_report['range']
            // $rec_time = ['< 10 นาที', '< 15 นาที', '< 20 นาที', '> 20 นาที'];
            $sheet->setCellValueByColumnAndRow(1, 1, "ช่วงเวลารถเข้า " . $crop_name[$crop]);
            $sheet->mergeCellsByColumnAndRow(1, 1, 3, 1);
            $startrow++;

            $st_3 = $startrow;
            $sheet->setCellValueByColumnAndRow(1, $startrow, "ช่วงเวลา");
            $sheet->setCellValueByColumnAndRow(2, $startrow, "จำนวน(คัน)");
            $sheet->setCellValueByColumnAndRow(3, $startrow, "ปริมาณ(KG.)");

            $sheet->getStyle('A' . $st_2 . ':C' . $st_2)->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A' . $st_2 . ':C' . $st_2)->getAlignment()->setVertical('center');
            $sheet->getStyle('A' . $st_2 . ':C' . $st_2)->getFont()->setBold(true);
            $sheet->getStyle('A' . $st_2 . ':C' . $st_2)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');
            $startrow++;

            foreach ($to_report['range'] as $key => $value) {
                $sheet->setCellValueByColumnAndRow(1, $startrow, $key);
                $sheet->setCellValueByColumnAndRow(2, $startrow, $value);
                $sheet->setCellValueByColumnAndRow(3, $startrow, $to_report['range_weight'][$key]);
                $startrow++;
            }

            $sheet->setCellValueByColumnAndRow(1, $startrow, 'รวม');
            $sheet->setCellValueByColumnAndRow(2, $startrow, '=SUM(B' . ($st_3 + 1) . ':B' . ($startrow - 1) . ')');
            $sheet->setCellValueByColumnAndRow(3, $startrow, '=SUM(C' . ($st_3 + 1) . ':C' . ($startrow - 1) . ')');

            $sheet->getStyle('A' . $startrow)->getAlignment()->setHorizontal('right');
            $sheet->getStyle('A' . $startrow . ':C' . $startrow)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');

            $sheet->getStyle('A' . $st_3 . ':C' . $startrow)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
            $sheet->getStyle('C' . $st_3 . ':C' . $startrow)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

            for ($i = 1; $i < 4; $i++) {
                $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($i);
                $sheet->getColumnDimension($columnID)->setAutoSize(true);
            }

            $spreadsheet->getActiveSheet()->setTitle('ช่วงเวลา');


            $count_sheet++;
            $spreadsheet->createSheet();
            $sheet = $spreadsheet->setActiveSheetIndex($count_sheet);
            $startrow = 1;


            //เวลาเข้าตามเขตพื้นที่   $area_time
            foreach ($area_time['count'] as $karea => $varea) {
                foreach ($varea as $ktime => $vtime) {
                    $set_val['area'][$karea] = $karea;
                    $set_val['time'][$ktime] = $ktime;
                }
            }

            $sheet->setCellValueByColumnAndRow(1, 1, "ช่วงเวลารถเข้าตามเขตพื้นที่ " . $crop_name[$crop]);
            $sheet->mergeCellsByColumnAndRow(1, 1, 3, 1);
            $startrow++;

            $st_4 = $startrow;
            $startcol = 2;
            foreach ($set_val['area'] as $key => $value) {
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $key);
                $startcol++;
            }
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, 'รวม');

            $end_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($startcol);
            $sheet->getStyle('A' . $st_4 . ':' . $end_col . $st_4)->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A' . $st_4 . ':' . $end_col . $st_4)->getAlignment()->setVertical('center');
            $sheet->getStyle('A' . $st_4 . ':' . $end_col . $st_4)->getFont()->setBold(true);
            $sheet->getStyle('A' . $st_4 . ':' . $end_col . $st_4)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');
            $startrow++;

            foreach ($set_val['time'] as $ktime => $vtime) {
                $startcol = 1;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $ktime);
                foreach ($set_val['area'] as $karea => $varea) {
                    $startcol++;
                    if (!empty($area_time['count'][$karea][$ktime]))   $sheet->setCellValueByColumnAndRow($startcol, $startrow, $area_time['count'][$karea][$ktime]);
                }
                $startcol++;
                $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($startcol - 1);
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, '=SUM(A' . $startrow . ':' . $columnID . $startrow . ')');
                $startrow++;
            }

            $sheet->setCellValueByColumnAndRow(1, $startrow, 'รวม');
            $startcol = 1;
            foreach ($set_val['area'] as $key => $value) {
                $startcol++;
                $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($startcol);
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, '=SUM(' . $columnID . ($st_4 + 1) . ':' . $columnID . ($startrow - 1) . ')');
            }
            $startcol++;
            $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($startcol);
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, '=SUM(' . $columnID . ($st_4 + 1) . ':' . $columnID . ($startrow - 1) . ')');

            $sheet->getStyle('A' . $startrow)->getAlignment()->setHorizontal('right');
            $sheet->getStyle('A' . $startrow . ':' . $end_col . $startrow)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');
            $sheet->getStyle('A' . $st_3 . ':' . $end_col . $startrow)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

            for ($i = 1; $i <= $startrow; $i++) {
                $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($i);
                $sheet->getColumnDimension($columnID)->setAutoSize(true);
            }

            $spreadsheet->getActiveSheet()->setTitle('เขตพื้นที่');

            $spreadsheet->setActiveSheetIndex(0);


            $writer = new Xlsx($spreadsheet);
            $filename = "report_3-" . date('yymmdd-hi') . ".xlsx";
            $completedirectory = 'storage/app/public/WhRM/completes/report/';
            $tmpfolder = date('Ymd');
            if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
            }
            $writer->save($completedirectory . '/' . $tmpfolder . '/' . $filename);
            $path_file = $completedirectory . '/' . $tmpfolder . '/' . $filename;
            return [$path_file];
        } else {
            return back()->with('error', 'No data!');
        }
    }

    public function mailView(Request $request)
    {
        $path_back = 'wh_rm.index';
        if (!empty($request->date_mail)) $sent_date = date("Y-m-d", strtotime($request->date_mail));
        else    $sent_date = date("Y-m-d");   //ลบหนึ่งวันจากวันปัจจุบัน
        // $sent_date = '2022-07-22';
        // dd(date("Y-m-d H:i:s"));
        $sap_show = array();
        $sap_show_broker = array();
        $mail_problem = array();
        $mn_show = array();
        $crop = "";
        $show_img = array();

        $test = $this->show_sap($sent_date, $sent_date);
        $sap_show = $test[0];
        if (!empty($test[1]))    $sap_show_broker = $test[1];
        $mail_problem = $test[2];

        $test = $this->show_mn($sent_date, $sent_date);
        $mn_show = $test[0];
        $crop = $test[1];
        $show_img = $test[2];
        // dd(count($sap_show[$sent_date]).' = '.count($mn_show[$sent_date]));
        // if(count($sap_show[$sent_date]) != count($mn_show[$sent_date])){
        //     return redirect()->back()->with('error', 'ข้อมูล SAP และ Mannual ไม่เท่ากัน กรุณาตรวจเช็คอีกครั้ง!');
        // }else{
        $test = $this->cal_tomail($sap_show, $sap_show_broker, $mn_show);
        $to_report = $test[0];
        $tb_receipt = $test[1];
        $tb_receipt_gp = $test[2];

        $format_setdate = date("d-m-Y", strtotime($sent_date));
        // return view('WHRM.mail',compact('sent_date','to_report','tb_receipt','tb_receipt_gp','to_bag_gp','sum_bag','sum_p','col_broker','mail_problem'));
        return view('WHRM.mail', compact('format_setdate', 'to_report', 'tb_receipt', 'tb_receipt_gp', 'mail_problem', 'crop', 'path_back', 'show_img', 'sent_date'));
        // }

    }

    public function cal_tomail($sap_show, $sap_show_broker, $mn_show)
    {
        $tr_data = ['พนักงานยก', 'เครื่องเรียง', 'total'];
        $col_rang = ['< 10 นาที', '< 15 นาที', '< 20 นาที', '> 20 นาที'];
        foreach ($tr_data as $ktr => $vtr) {
            foreach ($col_rang as $krang => $vrang) {
                $tb_receipt[$vtr][$vrang] = 0;
            }
        }

        $col_bag = ['good', 'bad'];
        foreach ($col_bag as $kbag => $vbag) {
            $sum_bag[$vbag] = 0;
        }

        $to_report = array();
        $tb_receipt_gp = array();
        $area_time = array();
        if (count($sap_show) > 0 && count($mn_show) > 0) {
            foreach ($sap_show as $kdate => $vdate) {
                foreach ($vdate as $kno => $vno) {
                    if (empty($to_report['weight'][1]))  $to_report['weight'][1] = $sap_show[$kdate][$kno]['rm_weight'];
                    else    $to_report['weight'][1] += $sap_show[$kdate][$kno]['rm_weight'];
                    $to_report['car'][] = $sap_show[$kdate][$kno]['car'];
                    if (empty($to_report['time']['min'])) {
                        $to_report['time']['min'] = $sap_show[$kdate][$kno]['time_in'];
                    } else {
                        if (strtotime($to_report['time']['min']) < strtotime('06:00:00')) {
                            if (strtotime($sap_show[$kdate][$kno]['time_in']) > strtotime('06:00:00')) {
                                $to_report['time']['min'] = $sap_show[$kdate][$kno]['time_in'];
                            } else {
                                if (strtotime($to_report['time']['min']) < strtotime($sap_show[$kdate][$kno]['time_in'])) {
                                    $to_report['time']['min'] = $sap_show[$kdate][$kno]['time_in'];
                                }
                            }
                        } else {
                            if (strtotime($sap_show[$kdate][$kno]['time_in']) < strtotime('06:00:00')) {
                                if ($to_report['time']['min'] < $sap_show[$kdate][$kno]['time_in']) {
                                    $to_report['time']['min'] = $sap_show[$kdate][$kno]['time_in'];
                                }
                            }
                        }
                    }
                    if (empty($to_report['time']['max'])) {
                        $to_report['time']['max'] = $sap_show[$kdate][$kno]['time_in'];
                    } else {
                        if (strtotime($to_report['time']['max']) < strtotime('06:00:00')) {
                            if (strtotime($sap_show[$kdate][$kno]['time_in']) < strtotime('06:00:00')) {
                                if (strtotime($to_report['time']['max']) < strtotime($sap_show[$kdate][$kno]['time_in'])) {
                                    $to_report['time']['max'] = $sap_show[$kdate][$kno]['time_in'];
                                }
                            }
                        } else {
                            if (strtotime($sap_show[$kdate][$kno]['time_in']) < strtotime('06:00:00')) {
                                $to_report['time']['max'] = $sap_show[$kdate][$kno]['time_in'];
                            } else {
                                if (strtotime($to_report['time']['max']) < strtotime($sap_show[$kdate][$kno]['time_in'])) {
                                    $to_report['time']['max'] = $sap_show[$kdate][$kno]['time_in'];
                                }
                            }
                        }
                    }

                    if (empty($to_report['time']['end_work'])) {
                        $to_report['time']['end_work'] = $mn_show[$kdate][$kno]['time_end'];
                    } else {
                        // echo $mn_show[$kdate][$kno]['end_work'].'</br>';
                        if (strtotime($to_report['time']['end_work']) < strtotime('06:00:00')) {
                            if (strtotime($mn_show[$kdate][$kno]['time_end']) < strtotime('06:00:00')) {
                                if (strtotime($to_report['time']['end_work']) < strtotime($mn_show[$kdate][$kno]['time_end'])) {
                                    $to_report['time']['end_work'] = $mn_show[$kdate][$kno]['time_end'];
                                }
                            }
                        } else {
                            if (strtotime($mn_show[$kdate][$kno]['time_end']) < strtotime('06:00:00')) {
                                $to_report['time']['end_work'] = $mn_show[$kdate][$kno]['time_end'];
                            } else {
                                if (strtotime($to_report['time']['end_work']) < strtotime($mn_show[$kdate][$kno]['time_end'])) {
                                    $to_report['time']['end_work'] = $mn_show[$kdate][$kno]['time_end'];
                                }
                            }
                        }
                    }

                    $h = date('H', strtotime('1 hour', strtotime($sap_show[$kdate][$kno]['time_in'])));
                    if (empty($to_report['range']['ก่อน ' . $h . ':00'])) {
                        $to_report['range']['ก่อน ' . $h . ':00'] = 1;
                    } else {
                        $to_report['range']['ก่อน ' . $h . ':00'] += 1;
                    }

                    if (empty($to_report['range_weight']['ก่อน ' . $h . ':00'])) {
                        $to_report['range_weight']['ก่อน ' . $h . ':00'] = $sap_show[$kdate][$kno]['rm_weight'];
                    } else {
                        $to_report['range_weight']['ก่อน ' . $h . ':00'] += $sap_show[$kdate][$kno]['rm_weight'];
                    }

                    if (empty($area_time['count'][$mn_show[$kdate][$kno]['area']]['ก่อน ' . $h . ':00']))  $area_time['count'][$mn_show[$kdate][$kno]['area']]['ก่อน ' . $h . ':00'] = 1;
                    else    $area_time['count'][$mn_show[$kdate][$kno]['area']]['ก่อน ' . $h . ':00'] += 1;
                    if (empty($area_time['weight'][$mn_show[$kdate][$kno]['area']]['ก่อน ' . $h . ':00']))  $area_time['weight'][$mn_show[$kdate][$kno]['area']]['ก่อน ' . $h . ':00'] = $sap_show[$kdate][$kno]['rm_weight'];
                    else    $area_time['weight'][$mn_show[$kdate][$kno]['area']]['ก่อน ' . $h . ':00'] += $sap_show[$kdate][$kno]['rm_weight'];

                    if (!empty($mn_show[$kdate][$kno]['rm_type'])) {
                        $h = date('H:i', strtotime($mn_show[$kdate][$kno]['time_use']));
                        $m = explode(':', $h);
                        $minite = ($m[0] * 60) + $m[1];
                        if ($minite < 20) {
                            if ($minite < 10) {
                                $tb_receipt[$mn_show[$kdate][$kno]['rm_type']]['< 10 นาที'] += 1;
                                $tb_receipt['total']['< 10 นาที'] += 1;
                            } else if ($minite < 15) {
                                $tb_receipt[$mn_show[$kdate][$kno]['rm_type']]['< 15 นาที'] += 1;
                                $tb_receipt['total']['< 15 นาที'] += 1;
                            } else {
                                $tb_receipt[$mn_show[$kdate][$kno]['rm_type']]['< 20 นาที'] += 1;
                                $tb_receipt['total']['< 20 นาที'] += 1;
                            }
                        } else {
                            $tb_receipt[$mn_show[$kdate][$kno]['rm_type']]['> 20 นาที'] += 1;
                            $tb_receipt['total']['> 20 นาที'] += 1;
                        }
                    }

                    foreach ($sap_show_broker[$kdate][$kno] as $kbk => $vbk) {
                        if (empty($to_bag['good'][$kbk]))    $to_bag['good'][$kbk] = $vbk;
                        else    $to_bag['good'][$kbk] += $vbk;
                        $sum_bag['good'] += $vbk;
                    }
                }
            }

            foreach ($tr_data as $ktr => $vtr) {
                foreach ($col_rang as $krang => $vrang) {
                    $tb_receipt_gp[$vtr][] = $tb_receipt[$vtr][$vrang];
                }
            }
        }

        return [$to_report, $tb_receipt, $tb_receipt_gp, $area_time];
    }

    public function sent_mail($date)
    {
        $sent_date = date("Y-m-d", strtotime($date));
        $sap_show = array();
        $sap_show_broker = array();
        $mail_problem = array();

        $mn_show = array();
        $crop = "";
        $show_img = array();

        $to_report = array();
        $tb_receipt = array();
        $tb_receipt_gp = array();

        $test = $this->show_sap($sent_date, $sent_date);
        $sap_show = $test[0];
        $sap_show_broker = $test[1];
        $mail_problem = $test[2];

        $test = $this->show_mn($sent_date, $sent_date);
        $mn_show = $test[0];
        $crop = $test[1];
        $show_img = $test[2];

        $test = $this->cal_tomail($sap_show, $sap_show_broker, $mn_show);
        $to_report = $test[0];
        $tb_receipt = $test[1];
        $tb_receipt_gp = $test[2];

        $gp = array();
        require_once app_path() . '/jpgraph/jpgraph.php';
        require_once app_path() . '/jpgraph/jpgraph_bar.php';
        require_once app_path() . '/jpgraph/jpgraph_pie.php';
        $date = date('ymdHis');
        $col_rang = ['< 10 นาที', '< 15 นาที', '< 20 นาที', '> 20 นาที'];
        $path = public_path() . '/graph/' . date('Y') . "/" . date('m') . '/mail';
        // dd($path);
        if (!File::exists($path)) {
            File::makeDirectory($path,  0777, true, true);
        }

        //แสดง Chart ข้อที่ 1 -------------------
        if (count($tb_receipt_gp) > 0) {
            $data1y = $tb_receipt_gp['พนักงานยก'];
            $data2y = $tb_receipt_gp['เครื่องเรียง'];
            $data3y = $tb_receipt_gp['total'];

            $graph2 = new \Graph(700, 350);
            $graph2->SetScale("textlin");
            $theme_class = new \UniversalTheme;
            $graph2->SetTheme($theme_class);
            // //$graph->yaxis->SetTickPositions(array(0, 30, 60, 90, 120, 150), array(15, 45, 75, 105, 135));
            $graph2->SetBox(false);

            $graph2->ygrid->SetFill(false);
            $graph2->xaxis->SetTickLabels($col_rang);    //
            $graph2->xaxis->SetLabelAlign('center', 'center');
            $graph2->xaxis->title->SetFont(FF_ANGSA, FS_BOLD, 18);
            $graph2->xaxis->SetFont(FF_ANGSA, FS_NORMAL, 14);
            // $graph2->xaxis->Hide();  //ซ่อน label ใต้กราฟ ต้องอยู่ใต้ SetMarginColor
            $graph2->yaxis->SetColor("#000000");
            $graph2->yaxis->HideLine(false);    //เส้นแกน x ของกราฟ false->show, ture->hide
            $graph2->yaxis->HideTicks(false, false);
            $graph2->yaxis->SetFont(FF_ANGSA, FS_NORMAL, 14);


            // Create the bar plots
            $b1plot2 = new \BarPlot($data1y);
            $b2plot2 = new \BarPlot($data2y);
            $b3plot3 = new \BarPlot($data3y);

            // Create the grouped bar plot
            $gbplot2 = new \GroupBarPlot(array($b1plot2, $b2plot2, $b3plot3));
            // ...and add it to the graPH
            $graph2->Add($gbplot2);

            $b1plot2->SetColor("#000000");  //สีเส้น bar
            $b1plot2->SetFillColor("#B06126");  //สี bar
            $b1plot2->SetLegend("พนักงานยก");
            // $b1plot2->value->SetFormat('%01.1f');
            $b1plot2->value->SetFormat('%d');
            $b1plot2->value->SetColor("#000000");
            $b1plot2->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
            $b1plot2->value->Show();
            // $b1plot2->value->SetAngle(90);

            $b2plot2->SetColor("#000000");  //สีเส้น bar
            $b2plot2->SetFillColor("#5D84CB");  //สี bar
            $b2plot2->SetLegend("เครื่องเรียงกระสอบ");
            // $b2plot2->value->SetFormat('%01.1f');
            $b2plot2->value->SetFormat('%d');
            $b2plot2->value->SetColor("#000000");
            $b2plot2->value->Show();
            $b2plot2->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
            // $b2plot2->value->SetAngle(90);

            $b3plot3->SetColor("#000000");  //สีเส้น bar
            $b3plot3->SetFillColor("#86B723");  //สี bar
            $b3plot3->SetLegend("total");
            // $b3plot3->value->SetFormat('%01.1f');
            $b3plot3->value->SetFormat('%d');
            $b3plot3->value->SetColor("#000000");
            $b3plot3->value->Show();
            $b3plot3->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
            // $b3plot3->value->SetAngle(90);

            $graph2->ygrid->Show(false);

            $graph2->legend->SetFrameWeight(2);
            $graph2->legend->SetColumns(3);
            $graph2->legend->SetColor('#000000');
            $graph2->legend->SetFont(FF_ANGSA, FS_NORMAL, 14);
            $graph2->legend->SetPos(0.5, 0.97, 'center', 'bottom');

            $gp['bar1']['link'] = "graph/" . date('Y') . "/" . date('m') . "/mail/wh_rm_bar1_" .  $date . ".jpg";
            $gp['bar1']['path'] = $path . "/wh_rm_bar1_" .  $date . ".jpg";
            $graph2->Stroke($gp['bar1']['path']);
            echo $gp['bar1']['path'] . "\r\n";
            // dd($gp['bar1']['path']);
        }
        //แสดง Chart ข้อที่ 1 ----------จบ---------

        $find_file = date('d-m-Y', strtotime($sent_date));
        return [$find_file, $to_report, $tb_receipt, $gp, $mail_problem, $show_img, $crop];
    }

    public function to_manual_mail($date)
    {
        // dd($date);
        $test = $this->sent_mail($date);
        $find_file = $test[0];
        $to_report = $test[1];
        $tb_receipt = $test[2];
        $gp = $test[3];
        $mail_problem = $test[4];
        $att_file = $test[5];
        $crop = $test[6];

        if (count($to_report) > 0) {
            $mail_to = Mail::send(new ReportWHRM($find_file, $to_report, $tb_receipt, $gp, $mail_problem, $att_file, $crop));
            return redirect()->route('wh_rm_mail')->with('success', ' Send Mail is complete!');
        } else {
            return redirect()->route('wh_rm_mail')->with('error', ' Send Mail is Error!');
        }
    }

    public function to_auto_mail()
    {
        $test = $this->sent_mail();
        $find_file = $test[0];
        $to_report = $test[1];
        $tb_receipt = $test[2];
        $gp = $test[3];
        $mail_problem = $test[4];
        $att_file = $test[5];
        $crop = $test[6];

        if (count($to_report) > 0) {
            Mail::send(new ReportWHRM($find_file, $to_report, $tb_receipt, $gp, $mail_problem, $att_file, $crop));
        } else {
            echo 'No Data !';
        }
    }

    public function destroy_date($date)
    {
        $sap_show = array();
        $test = $this->show_sap($date, $date);
        $sap_show = $test[0];
        // dd($sap_show);
        if (!empty($sap_show)) {
            foreach ($sap_show as $kdate => $vdate) {
                foreach ($vdate as $kno => $vno) {
                    // StReturn::where('no_id', $sap_show[$kdate][$kno]['sap_no'])->delete();
                    RmSap::where('sap_no', $sap_show[$kdate][$kno]['sap_no'])->delete();
                }
            }
        }

        $del_mn = RmManual::where('manual_date', $date)->delete();

        //ลบรูป
        $tmpfolder = date('Ymd', strtotime($date));
        $completedirectory = 'storage/app/public/WhRM/completes/img/' . $tmpfolder;
        if (is_dir($completedirectory)) {
            array_map('unlink', glob("$completedirectory/*.*"));
            if (rmdir($completedirectory)) {
                $mail_img = RmMailImage::where('img_date', $date)->delete();
            }
        }

        return redirect('wh_rm')->with('success', ' deleted!');
    }

    public function destroy_img($id)
    {
        $mail_img = RmMailImage::findOrFail($id);
        $tmpfolder = date('Ymd', strtotime($mail_img->img_date));
        $completedirectory = config('myconfig.directory.whrm.completes.img') . '/' . $tmpfolder . '/';

        if (unlink($completedirectory . $mail_img->image)) {
            $del_img = RmMailImage::where('id', $id)->delete();
            if ($del_img)    return redirect()->back()->with('success', ' deleted!');
        } else {
            return redirect()->back()->with('error', 'No file!');
        }
    }

    private $image_cache_expires = "Sat, 01 Jan 2050 00:00:00 GMT";
    public function show_img($path_1, $path_2)
    {
        $completedirectory = config('myconfig.directory.whrm.completes.img');
        $path = $completedirectory . '/' . $path_1 . '/';
        $exp = explode('.', $path_2);
        $photo_path = $path . $path_2; // eg: "file_name"
        $photo_mime_type = $exp[1]; // eg: "image/jpeg"
        $response = response()->make(File::get($photo_path));
        $response->header("Content-Type", $photo_mime_type);
        $response->header("Expires", $this->image_cache_expires);
        return $response;
    }
}
