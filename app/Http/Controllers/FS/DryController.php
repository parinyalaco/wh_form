<?php

namespace App\Http\Controllers\FS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\FS\Dry;
use App\Models\FS\Product;
use App\Models\FS\Wash;
use App\Models\FS\Lose;
use App\Models\FS\Stock;

class DryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $set_pd = $request->get('set_pd');
        if($request->get('st_search') != ""){
            $st_search = $request->get('st_search');
            if(empty($request->get('ed_search')))    $ed_search = date('Y-m-d');
            else    $ed_search = $request->get('ed_search');
        }else{
            $to_query = array();
            $query = Dry::groupBy('to_date')->orderByDesc('to_date')->selectRaw('TOP (5) to_date')->get();
            if(count($query)>0){
                foreach($query as $key){
                    $to_query[] = $key->to_date;               
                }                
                for($i=4; $i>=0; $i--){
                    if(!empty($to_query[$i])){         
                        $st_search = $to_query[$i];
                        break;
                    }
                }              
                // $st_search = $to_query[4];
                $ed_search =$to_query[0];
                
                $request->replace(['st_search' => $st_search, 'ed_search' => $ed_search]);
            }
        }

        $case = '';
        if (!empty($set_pd)) {     
            $case .= "(to_reuses.product_id = ".$set_pd.")";
            if (!empty($st_search)) $case .= " AND ";
        }
        if (!empty($st_search)) {
            $case .= "(dries.to_date Between '".$st_search."' AND '".$ed_search."')";
        } 
        $query1 = $this->main_query($case);
        // $pd = $main_query[0];
        // $query1 = $main_query[1];
        // dd($query1);
        $to_show = array();
        if(!empty($query1)){          
            $arr1 = array('quantity', 'stock_qty', 'lose_qty');
            foreach ($query1 as $key) {
                foreach ($arr1 as $karr=>$varr) {
                    if(empty($to_show[$key->to_date][$key->product_id][$key->shift][$key->dry_no][$key->round_no][$varr]))
                        $to_show[$key->to_date][$key->product_id][$key->shift][$key->dry_no][$key->round_no][$varr] = $key->$varr;
                    else
                        $to_show[$key->to_date][$key->product_id][$key->shift][$key->dry_no][$key->round_no][$varr] += $key->$varr;
                }
            }  
        }
        // dd($to_show);
        $set_num = $this->set_num();
        $pd = $set_num[0];
        $to_dry = $set_num[1];
        // dd($to_dry);
        $to_dry = array_map('array_filter', $to_dry);
        foreach ($to_dry as $kpd => $vpd) {
            $to_dry[$kpd] = array_map('array_filter', $to_dry[$kpd]);
            $to_dry[$kpd] = array_filter($to_dry[$kpd]);
        }
        $to_dry = array_filter($to_dry);

        return view('step.dry.index', compact('to_show', 'pd', 'to_dry'));
    }

    public function main_query($where) //main->tbl dry ใช้กับ index ข้อมูลการปั่น
    {
        $query = new Dry;
        $query = $query->join('washes', 'dries.wash_id', '=', 'washes.id')
            ->join('to_reuses', 'washes.to_reuse_id', '=', 'to_reuses.id');        
        $query = $query->selectRaw('dries.id, dries.to_date, dries.shift, to_reuses.product_id, 
            dries.dry_no, dries.round_no, dries.note, sum(dries.quantity) AS quantity,
            (SELECT sum(stocks.quantity)
                FROM   stocks
                WHERE stocks.dry_id = dries.id AND stocks.status = 1) AS stock_qty,
            (SELECT SUM(loses.quantity)
                FROM   loses
                WHERE loses.refer_id = dries.id AND loses.step_id = 3) AS lose_qty');
        $query = $query->where('dries.status', '1');  
        if(!empty($where))  $query = $query->whereRaw($where);    
        $query = $query->groupBy('dries.id', 'dries.to_date', 'dries.shift', 'to_reuses.product_id', 'dries.dry_no', 'dries.round_no', 'dries.note')
            ->orderBy('dries.to_date', 'DESC')->orderBy('dries.shift')
            ->get();
       
        return $query;
    }

    public function main_query2($where) //main->tbl wash ใช้กับ set_num, to_save จำนวนตั้งต้นก่อนซัก
    {
        $query = Product::where('status_wash', '1')->get();
        $pd = array();
        foreach ($query as $key) {
            $pd[$key->id] = $key->name;
            // $pd['wash'][$key->id] = $key->status_wash;
            // $pd['chk'][$key->id] = $key->status_check;
        } 
        // dd($where);
        $query = new Wash;
        $query = $query->join('to_reuses', 'washes.to_reuse_id', '=', 'to_reuses.id');        
        $query = $query->selectRaw('washes.id, washes.to_date, washes.shift, to_reuses.product_id, washes.note, 
            sum(washes.quantity) AS quantity,
            (SELECT sum(dries.quantity)
                FROM   dries
                WHERE dries.wash_id = washes.id  AND dries.status = 1) AS dry_qty,
            (SELECT SUM(shines.quantity)
                FROM   shines
                WHERE shines.wash_id = washes.id AND shines.status = 1) AS shine_qty, 
            (SELECT SUM(loses.quantity)
                FROM   loses
                WHERE loses.refer_id = washes.id AND loses.step_id = 2) AS lose_qty');
        $query = $query->where('washes.status', '1');  
        if(!empty($where))  $query = $query->whereRaw($where);    
        $query = $query->groupBy('washes.id', 'washes.to_date', 'washes.shift', 'to_reuses.product_id', 'washes.note')
            ->orderBy('washes.to_date', 'DESC')->orderBy('washes.shift')
            ->get();
       
        return [$pd, $query];
    }

    public function set_num()
    {
        $case = '';
        $main_query2 = $this->main_query2($case);
        $query1 = $main_query2[1];
        $pd = $main_query2[0];

        // dd($pd);
        $to_show = array();
        foreach ($query1 as $key) {
            if(!empty($to_show[$key->product_id][$key->to_date][$key->shift]))
                $to_show[$key->product_id][$key->to_date][$key->shift] += $key->quantity - $key->dry_qty - $key->shine_qty - $key->lose_qty; 
            else
                $to_show[$key->product_id][$key->to_date][$key->shift] = $key->quantity - $key->dry_qty  - $key->shine_qty- $key->lose_qty;   
        }
        return [$pd, $to_show];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $requestData = $request->all();

        $set_num = $this->set_num();
        $pd = $set_num[0];
        $to_show = $set_num[1];
        $to_show = array_map('array_filter', $to_show);
        foreach ($to_show as $kpd => $vpd) {
            $to_show[$kpd] = array_map('array_filter', $to_show[$kpd]);
            $to_show[$kpd] = array_filter($to_show[$kpd]);
        }
        $to_show = array_filter($to_show);
        // dd($to_show);
        if(empty($to_show)){
            return redirect('dry')->with('error', 'ไม่มีสินค้าสำหรับการปั่น!');
        }else{
            return view('step.dry.create',compact('pd', 'to_show', 'requestData'));
        }        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'product_id' => 'required',
        ]);
    
        if ($v->fails()){
            return redirect()->back()->withInput()->with('error', 'กรุณาเลือกสินค้าด้วยค่ะ');
        }

        $requestData = $request->all();
        $to_save = $this->to_save($requestData);
        // dd($to_save);
        if($to_save[0]=='<'){
            return redirect()->back()->withInput($request->input())->with('error','จำนวนที่ซักจนถึงวันที่เลือก มีน้อยกว่าจำนวนที่จะนำไปปั่น!');
        }elseif ($to_save[0]=='again') {
            return redirect()->back()->withInput($request->input())->with('error','รายการสินค้าในวันที่และกะนี้เคยมีการบันทึกแล้ว กรุณาใช้การแก้ไขแทนค่ะ!');
        }elseif ($to_save[0]=='not') {
            return redirect()->back()->withInput($request->input())->with('error','ไม่พบรายการซักที่สามารถปั่นได้!');
        }else{
            return redirect('dry')->with('success', ' Added!');
        }
    }

    public function to_save($requestData){
        $to_return = '';
        //ดูว่ามีค่าที่เป็นวันที่ กะ สินค้า ถังที่ รอบที่ ซ้ำมั้ย
        $check = Dry::join('washes', 'dries.wash_id', '=', 'washes.id')
            ->join('to_reuses', 'washes.to_reuse_id', '=', 'to_reuses.id')
            ->where('dries.to_date', $requestData['to_date'])
            ->where('dries.shift', $requestData['shift'])
            ->where('to_reuses.product_id', $requestData['product_id'])
            ->where('dries.dry_no', $requestData['dry_no'])
            ->where('dries.round_no', $requestData['round_no'])
            ->where('dries.status', '1')
            ->count();
        if($check==0){
            //ไม่ซ้ำ ทำการหาจำนวนที่นำมาได้
            $reuse = array();  

            $case = "(washes.to_date <= '".$requestData['to_date']."') 
                AND (to_reuses.product_id = ".$requestData['product_id'].")";
            if($requestData['shift']=='B'){
                $case .= " AND (washes.shift = 'B')";
            }
            $main_query2 = $this->main_query2($case);
            $query1 = $main_query2[1];
            // dd($query1);
            if(!empty($query1)){          
                foreach ($query1 as $key) {
                    $reuse['quantity'][$key->id] = $key->quantity - $key->dry_qty - $key->shine_qty - $key->lose_qty;                    
                }  
            }
            
            //ดูว่าจำนวนมีเพียงพอต่อมั้ย
            if(!empty($reuse)){
                if(array_sum($reuse['quantity']) < $requestData['quantity']){
                    // return back()->with('error','จำนวนที่รับเข้าเหลือน้อยกว่าจำนวนที่จะนำไปซัก!');
                    $to_return = '<';
                }else{
                    $to_wash = $requestData['quantity'];                    
                    foreach ($reuse['quantity'] as $key => $value) {
                        // echo 'to_wash->'.$to_wash.'</br>';
                        if($to_wash>0){
                            if($reuse['quantity'][$key]>0){
                                $to_save = array();
                                $to_save['to_date'] = $requestData['to_date'];
                                $to_save['shift'] = $requestData['shift'];
                                $to_save['wash_id'] = $key;
                                $to_save['dry_no'] = $requestData['dry_no'];
                                $to_save['round_no'] = $requestData['round_no'];
                                // echo $to_wash.' > '.$reuse['quantity'][$key].'</br>';
                                if($to_wash > $reuse['quantity'][$key]){
                                    $to_save['quantity'] = $reuse['quantity'][$key];                                
                                }else{
                                    $to_save['quantity'] = $to_wash;
                                }                    
                                $to_save['note'] = $requestData['note'];
                                $to_save['user_id'] = auth()->user()->id;
                                Dry::create($to_save);
                                $to_wash -= $to_save['quantity'];
                            }                            
                        }                    
                    } 
                    // return redirect('wash')->with('success', ' Added!');
                    // dd('chk');
                    $to_return = 'success';           
                }                
            }else{
                // return back()->with('error','ไม่พบรายการรับเข้าที่สามารถตัดได้!');
                $to_return = 'not';
            }
        }else{
            // return back()->with('error','รายการสินค้าในวันที่และกะนี้เคยมีการบันทึกแล้ว กรุณาใช้การแก้ไขแทนค่ะ!');
            $to_return = 'again';
        } 
        return [$to_return];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $requestData = $request->all();
        $to_show = array();
        //dry
            $query[1] = Dry::join('washes', 'dries.wash_id', '=', 'washes.id')
                ->join('to_reuses', 'washes.to_reuse_id', '=', 'to_reuses.id')
                ->join('products', 'to_reuses.product_id', '=', 'products.id')
                // ->select('washes.*', 'products.name')
                ->where('dries.to_date', $requestData['date'])
                ->where('dries.shift', $requestData['shift'])
                ->where('to_reuses.product_id', $requestData['pd'])
                ->where('dries.dry_no', $requestData['dry_no'])
                ->where('dries.round_no', $requestData['round_no'])
                ->selectRaw("dries.to_date, dries.shift, to_reuses.product_id, products.name, dries.dry_no, dries.round_no, 
                    sum(dries.quantity) AS quantity, dries.note, string_agg(dries.id, ',') AS sum_id")
                ->groupBy('dries.to_date', 'dries.shift', 'to_reuses.product_id', 'products.name', 'dries.dry_no', 'dries.round_no', 'dries.note')
                ->get();

            $exp = explode(',',$query[1][0]['sum_id']);
            // dd($query[1]);
        //stock
            $query[2] = Stock::whereIn('dry_id', $exp)            
                ->selectRaw("id, to_date, shift, pallet_no, quantity, note,
                    (SELECT sum(pass + not_pass)
                        FROM   stock_processes
                        WHERE stock_processes.stock_id = stocks.id) AS stock_process, 
                    (SELECT sum(fs_wh_tran_ds.volumn)
                        FROM   fs_wh_tran_ds
                        WHERE fs_wh_tran_ds.stock_id = stocks.id) AS to_use_qty,                         
                    (SELECT SUM(loses.quantity)
                        FROM   loses
                        WHERE loses.refer_id = stocks.id AND loses.step_id = 4) AS lose_qty")
                // ->groupBy('to_date', 'shift', 'pallet_no', 'note')
                ->get();
            if(count($query[2])>0){
                $arr = array('quantity', 'stock_process', 'to_use_qty', 'lose_qty');
                $arr2 = array('id', 'note');
                foreach ($query[2] as $key) {
                    foreach ($arr as $karr => $varr) {                    
                        if(empty($to_show['stock'][$key->to_date][$key->shift][$key->pallet_no][$varr]))
                            $to_show['stock'][$key->to_date][$key->shift][$key->pallet_no][$varr] = $key->$varr;
                        else
                            $to_show['stock'][$key->to_date][$key->shift][$key->pallet_no][$varr] += $key->$varr;  
                    }
                    foreach ($arr2 as $karr => $varr) {
                        if(empty($to_show['stock'][$key->to_date][$key->shift][$key->pallet_no][$varr]))
                            $to_show['stock'][$key->to_date][$key->shift][$key->pallet_no][$varr] = $key->$varr;
                        else
                            $to_show['stock'][$key->to_date][$key->shift][$key->pallet_no][$varr] .= ','.$key->$varr;
                    }
                }                
            }

        //lose        
            $query[3] = Lose::join('lose_types', 'loses.lose_type_id', '=', 'lose_types.id')
                ->select('loses.*','lose_types.name')
                ->whereIn('refer_id',$exp)->where('step_id', 3)->get();        
        // dd($exp);

        return view('step.dry.detail', compact('query', 'to_show'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $requestData = $request->all();
        $pd_name = Product::where('status_wash', 1)->orderBy('name')->get();
        $query = Dry::join('washes', 'dries.wash_id', '=', 'washes.id')
            ->join('to_reuses', 'washes.to_reuse_id', '=', 'to_reuses.id')
            ->where('dries.to_date', $requestData['date'])
            ->where('dries.shift', $requestData['shift'])
            ->where('to_reuses.product_id', $requestData['pd'])
            ->where('dries.dry_no', $requestData['dry_no'])
            ->where('dries.round_no', $requestData['round_no'])
            ->selectRaw("dries.to_date,dries.shift,to_reuses.product_id,dries.dry_no,dries.round_no,
                sum(dries.quantity) AS quantity, dries.note, STRING_AGG(dries.id, ',') AS to_id")
            ->groupBy('dries.to_date','dries.shift','to_reuses.product_id','dries.dry_no','dries.round_no','dries.note')
            ->get();

        $set_num = $this->set_num();
        $to_show = $set_num[1];
        $set_max = 0;

        foreach ($to_show[$requestData['pd']] as $key => $value) {            
            foreach ($value as $kshf => $vshf) {
                if($key < $requestData['date']){
                    $set_max += $vshf;
                }elseif($key == $requestData['date']){
                    if(($requestData['shift']=='B' && $kshf=='B') || $requestData['shift']=='C'){
                        $set_max += $vshf;
                    }
                }       
            }            
        }
        $set_max += $query[0]['quantity'];

        return view('step.dry.edit',compact('pd_name', 'query', 'set_max'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //เก็บข้อมูลเก่า
            $requestData = $request->all();
            $exp = explode(',', $requestData['to_id']);
            $query = array();
            $to_status['status'] = '0';
            $query = Dry::whereIn('id', $exp)
                ->update($to_status);

        //เก็บข้อมูลใหม่ ถ้าไม่ใช่ success ต้อง save ข้อมูลเก่าคืน
            $to_save = $this->to_save($requestData);
            $query = Dry::whereIn('id', $exp);
            if($to_save[0]=='success'){
                $query->delete();
                return redirect('dry')->with('success', ' Update!');
            }else{
                $to_status['status'] = '1';
                $query->update($to_status);
                if($to_save[0]=='<'){
                    return redirect()->back()->withInput($request->input())->with('error','จำนวนที่รับเข้าเหลือน้อยกว่าจำนวนที่จะนำไปซัก!');
                }elseif ($to_save[0]=='again') {
                    return redirect()->back()->withInput($request->input())->with('error','รายการสินค้าในวันที่และกะนี้เคยมีการบันทึกแล้ว กรุณาใช้การแก้ไขแทนค่ะ!');
                }elseif ($to_save[0]=='not') {
                    return redirect()->back()->withInput($request->input())->with('error','ไม่พบรายการรับเข้าที่สามารถตัดได้!');
                }
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $requestData = $request->all();
        if($requestData['page']=='dry'){
            $query = Dry::join('washes', 'dries.wash_id', '=', 'washes.id')
            ->join('to_reuses', 'washes.to_reuse_id', '=', 'to_reuses.id')
            ->where('dries.to_date', $requestData['date'])
            ->where('dries.shift', $requestData['shift'])
            ->where('to_reuses.product_id', $requestData['pd'])
            ->where('dries.dry_no', $requestData['dry_no'])
            ->where('dries.round_no', $requestData['round_no'])->delete();
        }elseif($requestData['page']=='wash'){     
            $exp = explode(',',$requestData['id']);        
            $query = Dry::whereIn('id',$exp)->delete();            
        }
        if($query){
            return back()->with('success','Deleted!');
        }else{
            return back()->with('error',"Can't delete!");
        }
    }
}
