<?php

namespace App\Http\Controllers\FS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\FS\FsHwMain;
use App\Models\FS\FsHwLoc;
use App\Models\FS\FsWhStock;
use App\Models\FS\FsWhTranDs;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Facades\File;

class ReportController extends Controller
{
    public function index()
    {
        $fs = array();
        $loc = array();
        $fs = FsHwMain::all();

        $query = FsHwLoc::all();
        foreach ($query as $key) {
            $exp = explode('-', $key->name);
            $loc[$key->fs_wh_id][$key->row] = $exp[0];
        }

        return view('step.report.index', compact('fs', 'loc'));
    }

    public function to_report(Request $request)
    {
        $fs = array();
        $query = FsHwMain::all();
        foreach ($query as $key) {
            $fs_name[$key->id] = $key->name;
            if(empty($request->fs_id)){
                $fs[] = $key->id;
            }
        }
        if(!empty($request->fs_id)){
            $fs[] = $request->fs_id;
        }

        if($request->report_id==1){
            if(!empty($fs)){
                $loc = array();
                $query = FsWhStock::join('fs_hw_locs', 'fs_wh_stocks.fs_wh_loc_id', '=', 'fs_hw_locs.id')
                    ->join('products', 'fs_wh_stocks.product_id', '=', 'products.id');
                if(!empty($request->loc_id)) $query = $query->where('fs_hw_locs.row', $request->loc_id);
                $query = $query->select('fs_hw_locs.fs_wh_id', 'fs_hw_locs.name AS loc_name', 'products.name AS pd_name', 'fs_wh_stocks.volumn')
                    ->whereIn('fs_hw_locs.fs_wh_id', $fs)->get();
                foreach ($query as $key) {
                    $loc[$key->fs_wh_id][$key->loc_name][$key->pd_name] = $key->volumn;
                }
                if(!empty($loc)){
                    $spreadsheet = new Spreadsheet();
                    $count_sheet = 0;

                    $sheet = $spreadsheet->getActiveSheet();
                    $i = 0;
                    foreach ($loc as $kfs => $vfs) {
                        if($i>0){
                            $count_sheet++;
                            $spreadsheet->createSheet();
                            $sheet = $spreadsheet->setActiveSheetIndex($count_sheet);
                        }
                        // $sheet->setCellValueByColumnAndRow(1, 1, "คลัง");
                        $sheet->setCellValueByColumnAndRow(1, 1, "Pallet");
                        $sheet->setCellValueByColumnAndRow(2, 1, "Product");
                        $sheet->setCellValueByColumnAndRow(3, 1, "จำนวน");

                        $sheet->getStyle('A1:C1')->getAlignment()->setHorizontal('center');
                        $sheet->getStyle('A1:C1')->getAlignment()->setVertical('center');
                        $sheet->getStyle('A1:C1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');

                        $startrow = 2;
                        foreach ($vfs as $kloc => $vloc) {
                            foreach ($vloc as $kpd => $vpd) {
                                $startcol = 1;
                                // $sheet->setCellValueByColumnAndRow($startcol, $startrow, $fs_name[$kfs]);
                                // $startcol++;
                                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $kloc);
                                $startcol++;
                                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $kpd);
                                $startcol++;
                                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $vpd);
                                $startrow++;
                            }
                        }
                        $sheet->getStyle('A1:C'.($startrow-1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
                        for($i=1; $i<=3; $i++) {
                            $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($i);
                            $sheet->getColumnDimension($columnID)->setAutoSize(true);
                        }
                        $spreadsheet->getActiveSheet()->getStyle('C:C')->getNumberFormat()->setFormatCode('#,###');
                        $spreadsheet->getActiveSheet()->setTitle($fs_name[$kfs]);
                        $i++;
                    }
                    $writer = new Xlsx($spreadsheet);
                    $date = date('ymdHis');
                    $filename = "/FS-" . $date . ".xlsx";
                    $path_2 = '/storage/FS/'.date('Y'). "/" . date('m');
                    $path = public_path() . $path_2 ;
                    if (!File::exists($path)) {
                        File::makeDirectory($path,  0777, true, true);
                    }
                    $location = $path . $filename;
                    $writer->save($location);

                    return response()->download($location);
                }else{
                    return redirect()->back()->with('error', 'No Data!!');
                }
            }else{
                return redirect()->back()->with('error', 'No Data!!');
            }
        }elseif ($request->report_id==2) {
            $loc = array();
            $query = FsWhStock::join('fs_hw_locs', 'fs_wh_stocks.fs_wh_loc_id', '=', 'fs_hw_locs.id')
                ->join('products', 'fs_wh_stocks.product_id', '=', 'products.id');
            if(!empty($request->loc_id)) $query = $query->where('fs_hw_locs.row', $request->loc_id);
            $query = $query->select('fs_hw_locs.fs_wh_id', 'fs_hw_locs.name AS loc_name', 'products.name AS pd_name', 'fs_wh_stocks.volumn')
                ->whereIn('fs_hw_locs.fs_wh_id', $fs)->get();
            foreach ($query as $key) {
                $loc[$key->fs_wh_id][$key->loc_name][$key->pd_name] = $key->volumn;
            }
        }
    }
}
