<?php

namespace App\Http\Controllers\FS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\FS\Product;
use App\Models\FS\ToUse;
use App\Models\FS\Lose;
use App\Models\FS\Step;
use App\Models\FS\Stock;

class ToUseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = 10;        
        $set_pd = $request->get('set_pd');
        if($request->get('st_search') != ""){
            $st_search = $request->get('st_search');
            if(empty($request->get('ed_search')))    $ed_search = date('Y-m-d');
            else    $ed_search = $request->get('ed_search');
        }

        $query = Product::all();
        foreach ($query as $key) {
            $pd[$key->id] = $key->name;
        }
        $query = array();

        //to_use
            $query = new ToUse;
            $query = $query->join('stocks', 'to_uses.stock_id', '=', 'stocks.id');
            $query = $query->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id');
            $query = $query->selectRaw('to_uses.to_date, to_reuses.product_id, to_uses.pallet_no, SUM(to_uses.quantity) AS quantity');
            if (!empty($set_pd)) {     //ต้องมาใส่เงื่อนไขใหม่เพราะไม่ดูที่สถานะแล้ว
                $query = $query->where('to_reuses.product_id', $set_pd);
            }
            if (!empty($st_search)) {
                $query = $query->whereBetween('to_uses.to_date', [$st_search, $ed_search]);
            }
            $query = $query->groupBy('to_uses.to_date', 'to_reuses.product_id', 'to_uses.pallet_no');
            $query = $query->orderBy('to_uses.to_date', 'DESC')->paginate($perPage);


        //lose  
            $to_show = array();      
            $query3 = Lose::join('to_uses', 'loses.refer_id', '=', 'to_uses.id')
                ->join('stocks', 'to_uses.stock_id', '=', 'stocks.id')
                ->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
                ->selectRaw('to_uses.to_date, to_reuses.product_id, to_uses.pallet_no, sum(loses.quantity) AS lose_qty')
                ->where('loses.step_id', 5)
                ->groupBy('to_uses.to_date', 'to_reuses.product_id', 'to_uses.pallet_no')
                ->get();
            if(!empty($query3)){
                foreach ($query3 as $key) {
                    $to_show[$key->to_date][$key->product_id][$key->pallet_no]['lose_qty'] = $key->lose_qty;
                }
            } 
            // dd($query);
        return view('step.use.index', compact('pd','query','to_show'));
    }

    public function set_num()
    {
        $query1 = ToUse::join('stocks', 'to_uses.stock_id', '=', 'stocks.id')
            ->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
            ->selectRaw('stocks.to_date, to_reuses.product_id, SUM(to_uses.quantity) AS quantity')
            ->groupBy('stocks.to_date', 'to_reuses.product_id')
            ->get();
        foreach ($query1 as $key) {
            $qty['to_use'][$key->product_id][$key->to_date] = $key->quantity;
        }

        $query3 = Lose::join('stocks', 'loses.refer_id', '=', 'stocks.id')        
            ->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
            ->selectRaw('stocks.to_date, to_reuses.product_id, SUM(loses.quantity) AS quantity')
            ->where('loses.step_id', 4)
            ->groupBy('stocks.to_date', 'to_reuses.product_id')
            ->get();            
        foreach ($query3 as $key) {
            $qty['lose'][$key->product_id][$key->to_date] = $key->quantity;
        }
        // dd($qty);

        $arr = array('to_use');     //, 'lose' pass เป็นการหักลบสูญเสียแล้ว
        $query = Stock::join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id') 
            ->selectRaw("stocks.to_date, to_reuses.product_id, SUM(stocks.pass) AS quantity, STRING_AGG(stocks.id, ',') AS concat_id")
            ->groupBy('stocks.to_date', 'to_reuses.product_id')
            ->orderBy('stocks.to_date')
            ->get();
        // dd($query);
        foreach ($query as $key) {
            $to_diff = 0;
            foreach($arr  as $karr=>$varr){
                if(!empty($qty[$varr][$key->product_id][$key->to_date]))
                    $to_diff += $qty[$varr][$key->product_id][$key->to_date];
            }
            // echo $key->to_date.'->'.$key->quantity.' - '.$to_diff.'</br>';
            $to_show[$key->product_id][$key->to_date] = $key->quantity - $to_diff;
        }   
        // dd($to_show);
        $to_show = array_map('array_filter', $to_show);
        $to_show = array_filter($to_show);
        // dd($to_show);
        $query_pd = Product::orderBy('name')->get();
        foreach($query_pd  as $key){
            $pd[$key->id] = $key->name;
        }
        return [$pd, $to_show];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $set_num = $this->set_num();
        $pd = $set_num[0];
        $to_show = $set_num[1];
        // dd($to_show);
        if(empty($to_show)){
            return redirect('to_use')->with('error', 'ไม่มีสินค้าสำหรับการเบิก!');
        }else{
            return view('step.use.create',compact('pd', 'to_show'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $to_save = $this->to_save($requestData);
        // dd($to_save);
        if($to_save[0]=='<'){
            return redirect()->back()->withInput($request->input())->with('error','จำนวนที่มีจนถึงวันที่เลือก มีน้อยกว่าจำนวนที่ต้องการใช้!');
        }elseif ($to_save[0]=='again') {
            return redirect()->back()->withInput($request->input())->with('error','รายการสินค้าในวันที่และกะนี้เคยมีการบันทึกแล้ว กรุณาใช้การแก้ไขแทนค่ะ!');
        }elseif ($to_save[0]=='not') {
            return redirect()->back()->withInput($request->input())->with('error','ไม่พบรายการที่สามารถใช้ได้!');
        }else{
            return redirect('to_use')->with('success', ' Added!');
        }
    }

    public function to_save($requestData){
        $to_return = '';
        //ดูว่ามีค่าที่เป็นวันที่ กะ สินค้า pallet ซ้ำมั้ย
        $check = ToUse::join('stocks', 'to_uses.stock_id', '=', 'stocks.id')
            ->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
            ->where('to_uses.to_date', $requestData['to_date'])
            // ->where('to_uses.shift', $requestData['shift'])
            ->where('to_reuses.product_id', $requestData['product_id'])
            ->where('to_uses.pallet_no', $requestData['pallet_no'])
            ->where('to_uses.status', 1)
            ->count(); 
               
        if($check==0){
            //ไม่ซ้ำ ทำการหาจำนวนที่นำมาได้
            $query = array();
            $reuse = array();

            $query1 = ToUse::join('stocks', 'to_uses.stock_id', '=', 'stocks.id')
                ->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
                ->selectRaw('stocks.id, SUM(to_uses.quantity) AS quantity')
                ->where('to_uses.status', 1)
                ->groupBy('stocks.id')
                ->get();
            foreach ($query1 as $key) {
                $qty['to_use'][$key->id] = $key->quantity;
            }

            // $query3 = Lose::join('stocks', 'loses.refer_id', '=', 'stocks.id')        
            //     ->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
            //     ->selectRaw('stocks.id, SUM(loses.quantity) AS quantity')
            //     ->where('loses.step_id', 4)
            //     ->groupBy('stocks.id')
            //     ->get();            
            // foreach ($query3 as $key) {
            //     $qty['lose'][$key->id] = $key->quantity;
            // }
            // dd($qty);

            $arr = array('to_use');     //, 'lose' pass เป็นการหักลบสูญเสียแล้ว
            $query = Stock::join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id') 
                ->selectRaw("stocks.id, stocks.to_date, SUM(stocks.pass) AS quantity")
                ->where('stocks.to_date', '<=', $requestData['to_date'])
                ->where('to_reuses.product_id', $requestData['product_id'])
                ->groupBy('stocks.id', 'stocks.to_date')
                ->orderBy('stocks.to_date')
                ->get();
            // dd($query);
            foreach ($query as $key) {
                $to_diff = 0;
                foreach($arr  as $karr=>$varr){
                    if(!empty($qty[$varr][$key->id]))
                        $to_diff += $qty[$varr][$key->id];
                }
                // echo $key->to_date.'->'.$key->quantity.' - '.$to_diff.'</br>';
                // $to_show[$key->product_id][$key->to_date] = $key->quantity - $to_diff;
                $reuse['quantity'][$key->id] = $key->quantity - $to_diff;
            }   




            // $query = new Stock;
            // $query = $query->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
            //     // ->join('dries', 'washes.id', '=', 'dries.wash_id', 'left outer')
            //     ->leftJoin('to_uses', function($join)
            //     {
            //         $join->on('stocks.id', '=', 'to_uses.stock_id')
            //             ->where('to_uses.status', 1);
            //     })
            //     ->leftJoin('loses', function($join)
            //     {
            //         $join->on('stocks.id', '=', 'loses.refer_id')
            //             ->where('loses.step_id', 4);
            //     })                
            //     ->where('stocks.to_date', '<=', $requestData['to_date'])
            //     ->where('to_reuses.product_id', $requestData['product_id']);
            // // if($requestData['shift']=='B'){
            // //     $query = $query->where('stocks.shift', $requestData['shift']);
            // // }
            // $query = $query->selectRaw('stocks.id, stocks.to_date, to_reuses.product_id, stocks.pass, 
            //         SUM(loses.quantity) AS lose_qty, SUM(to_uses.quantity) AS to_use_qty')
            //     ->groupBy('stocks.id', 'to_reuses.id', 'stocks.to_date', 'to_reuses.product_id',  'stocks.pass')
            //     ->orderBy('stocks.to_date')->get();
            // // dd($query);
            // foreach ($query as $key) {
            //     if($key->pass > $key->lose_qty+$key->to_use_qty){
            //         // $reuse['quantity'][$key->id] = $key->pass - $key->lose_qty - $key->to_use_qty;
            //         $reuse['quantity'][$key->id] = $key->pass - $key->to_use_qty;
            //     }
            // }  
            // dd($reuse);
            
            
            // dd(array_sum($reuse['quantity']));
            
            // dd($reuse);
            //ดูว่าจำนวนมีเพียงพอต่อมั้ย
            if(!empty($reuse)){
                if(array_sum($reuse['quantity']) < $requestData['quantity']){
                    // return back()->with('error','จำนวนที่รับเข้าเหลือน้อยกว่าจำนวนที่จะนำไปซัก!');
                    $to_return = '<';
                }else{
                    $to_wash = $requestData['quantity'];

                    $to_save = array();
                    $to_save['to_date'] = $requestData['to_date'];
                    // $to_save['shift'] = $requestData['shift'];
                    $to_save['pallet_no'] = $requestData['pallet_no'];        
                    $to_save['note'] = $requestData['note'];
                    foreach ($reuse['quantity'] as $key => $value) {
                        if($value>0){   
                            if($reuse['quantity'][$key]>0){                                                   
                                $to_save['stock_id'] = $key;
                                if($to_wash>0){  
                                    if($to_wash > $value){
                                        $to_save['quantity'] = $value;                              
                                    }else{
                                        $to_save['quantity'] = $to_wash;
                                    }            
                                    ToUse::create($to_save);
                                    $to_wash -= $to_save['quantity'];
                                }
                            }
                        }                    
                    } 
                    // return redirect('wash')->with('success', ' Added!');
                    $to_return = 'success';           
                }                
            }else{
                // return back()->with('error','ไม่พบรายการรับเข้าที่สามารถตัดได้!');
                $to_return = 'not';
            }
        }else{
            // return back()->with('error','รายการสินค้าในวันที่และกะนี้เคยมีการบันทึกแล้ว กรุณาใช้การแก้ไขแทนค่ะ!');
            $to_return = 'again';
        } 
        return [$to_return];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $requestData = $request->all();
        //to_use
            $query[1] = ToUse::join('stocks', 'to_uses.stock_id', '=', 'stocks.id')
                ->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
                ->join('products', 'to_reuses.product_id', '=', 'products.id')
                ->where('to_uses.to_date', $requestData['date'])
                // ->where('to_uses.shift', $requestData['shift'])
                ->where('to_reuses.product_id', $requestData['pd'])
                ->where('to_uses.pallet_no', $requestData['pallet_no'])
                ->selectRaw("to_uses.to_date, to_reuses.product_id, products.name, to_uses.pallet_no, 
                    sum(to_uses.quantity) AS quantity, to_uses.note, string_agg(to_uses.id, ',') AS sum_id")
                ->groupBy('to_uses.to_date', 'to_reuses.product_id', 'products.name', 'to_uses.pallet_no', 'to_uses.note')
                ->get();

            $exp = explode(',',$query[1][0]['sum_id']);
            // dd($exp);
        //to_use
            // $query[2] = ToUse::whereIn('stock_id', [$query[1][0]['id']])
            //     // join('products', 'to_reuses.product_id', '=', 'products.id')
            //     // ->select('to_reuses.to_date', 'products.name', 'to_reuses.quantity', 'to_reuses.pallet_no', 'to_reuses.note')
            //     // ->select('to_reuses.*', 'products.name')
            //     // ->where('to_reuses.id', $id)
            //     ->get();

        // //StockProcess
        //     $query[2] = ToUse::whereIn('wash_id', [$query[1][0]['id']])
        //         // join('products', 'to_reuses.product_id', '=', 'products.id')
        //         // ->select('to_reuses.to_date', 'products.name', 'to_reuses.quantity', 'to_reuses.pallet_no', 'to_reuses.note')
        //         // ->select('to_reuses.*', 'products.name')
        //         // ->where('to_reuses.id', $id)
        //         ->get();

        //lose        
            $query[3] = Lose::join('lose_types', 'loses.lose_type_id', '=', 'lose_types.id')
                ->select('loses.*','lose_types.name')
                ->whereIn('refer_id',$exp)->where('step_id', 5)->get();
        
        // dd($query);
        return view('step.use.detail', compact('query'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $requestData = $request->all();
        $pd_name = Product::orderBy('name')->get();
        $query = ToUse::join('stocks', 'to_uses.stock_id', '=', 'stocks.id')
            ->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
            ->where('to_uses.to_date', $requestData['date'])
            // ->where('stocks.shift', $requestData['shift'])
            ->where('to_reuses.product_id', $requestData['pd'])
            ->where('to_uses.pallet_no', $requestData['pallet_no'])
            ->selectRaw('to_uses.to_date,to_reuses.product_id,to_uses.pallet_no,sum(to_uses.quantity) AS quantity,to_uses.note')
            ->groupBy('to_uses.to_date','to_reuses.product_id','to_uses.pallet_no','to_uses.note')
            ->get();

        $set_num = $this->set_num();
        // $pd = $set_num[0];
        $to_show = $set_num[1];
        // dd($to_show);
        $set_max = 0;
        if(!empty($to_show[$requestData['pd']])){
            foreach ($to_show[$requestData['pd']] as $key => $value) {         
                if($key <= $requestData['date']){
                    $set_max += $value;
                }      
            }
        }
        $set_max += $query[0]['quantity'];

        return view('step.use.edit',compact('pd_name', 'query', 'set_max'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //เก็บข้อมูลเก่า
        $requestData = $request->all();
        $query = array();
        $to_status['status'] = '0';
        $query = ToUse::join('stocks', 'to_uses.stock_id', '=', 'stocks.id')
            ->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
            ->where('to_uses.to_date', $requestData['to_date'])
            // ->where('stocks.shift', $requestData['shift'])
            ->where('to_reuses.product_id', $requestData['product_id'])                
            ->where('to_uses.pallet_no', $requestData['pallet_no'])
            ->update($to_status);
    
        //เก็บข้อมูลใหม่ ถ้าไม่ใช่ success ต้อง save ข้อมูลเก่าคืน
        $to_save = $this->to_save($requestData);
        $query = ToUse::join('stocks', 'to_uses.stock_id', '=', 'stocks.id')
                // ->join('washes', 'dries.wash_id', '=', 'washes.id')
                ->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
                ->where('to_uses.to_date', $requestData['to_date'])
                // ->where('stocks.shift', $requestData['shift'])
                ->where('to_reuses.product_id', $requestData['product_id'])          
                ->where('to_uses.pallet_no', $requestData['pallet_no'])
                ->where('to_uses.status', '0');
        if($to_save[0]=='success'){
            $query->delete();
            return redirect('to_use')->with('success', ' Update!');
        }else{
            $to_status['status'] = '1';
            $query->update($to_status);
            if($to_save[0]=='<'){
                return redirect()->back()->withInput($request->input())->with('error','จำนวนที่มีจนถึงวันที่เลือก มีน้อยกว่าจำนวนที่จะใช้!');
            }elseif ($to_save[0]=='again') {
                return redirect()->back()->withInput($request->input())->with('error','รายการสินค้าในวันที่และกะนี้เคยมีการบันทึกแล้ว กรุณาใช้การแก้ไขแทนค่ะ!');
            }elseif ($to_save[0]=='not') {
                return redirect()->back()->withInput($request->input())->with('error','ไม่พบรายการรับเข้าที่สามารถตัดได้!');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $requestData = $request->all();
        $query = ToUse::join('stocks', 'to_uses.stock_id', '=', 'stocks.id')
            // ->join('washes', 'dries.wash_id', '=', 'washes.id')
            ->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
            ->where('to_uses.to_date', $requestData['date'])
            // ->where('to_uses.shift', $requestData['shift'])
            ->where('to_reuses.product_id', $requestData['pd'])
            ->where('to_uses.pallet_no', $requestData['pallet_no'])
            ->delete();
        if($query){
            return back()->with('success','Deleted!');
        }else{
            return back()->with('error',"Can't delete!");
        }
    }
}
