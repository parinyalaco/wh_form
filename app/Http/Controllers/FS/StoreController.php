<?php

namespace App\Http\Controllers\FS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\FS\Store;
use App\Models\FS\TargetManpower;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Store::orderBy('name')->get();
        $chk_ed = array();
        $query1 = TargetManpower::select('store_id')->groupBy('store_id')->get();
        foreach ($query1 as $key) {
            $chk_ed[$key->store_id] = $key->store_id;
        }
        return view('base_data.store.index', compact('query', 'chk_ed'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('base_data.store.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required|unique:stores|max:255',
        ]);
    
        if ($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }

        $requestData = $request->all();
        Store::create($requestData);

        return redirect('base_data/store')->with('success', ' Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query = Store::findOrFail($id);
        return view('base_data.store.edit', compact('query'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:stores,name,'.$id,
        ]);

        $requestData = $request->all();
        $query = Store::findOrFail($id);
        $query->update($requestData);

        return redirect('base_data/store')->with('success', 'Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = Store::where('id',$id)->delete();
        if($query){
            return back()->with('success','Deleted!');
        }else{
            return back()->with('error',"Can't delete!");
        }
    }
}
