<?php

namespace App\Http\Controllers\FS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\FS\Target;
use App\Models\FS\TargetManpower;
use App\Models\FS\TargetProduct;
use App\Models\FS\Shift;
use App\Models\FS\Product;
use App\Models\FS\Step;
use App\Models\FS\Store;
use App\Models\FS\StepTg;
// use Illuminate\Support\Facades\DB;
use App\Models\FS\Wash;
use App\Models\FS\Dry;
use App\Models\FS\Shine;
use App\Models\FS\ChooseBox;
use App\Models\FS\Stock;
use DB;

class TargetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sh = array();
        $query = Shift::orderBy('name')->get();
        foreach ($query as $key) {
            $sh[$key->id] = 'กะ '.$key->name.' ('.date('H:i', strtotime($key->start_time)).'-'.date('H:i', strtotime($key->end_time)).') ';  
            $m_sh[$key->id] = $key->name;
        }        
        $set_sh = $request->get('set_sh');

        $perPage = 10;        
        // $set_end = $request->get('set_end');
        if($request->get('st_search') != ""){
            $st_search = $request->get('st_search');
            if(empty($request->get('ed_search')))    $ed_search = date('Y-m-d');
            else    $ed_search = $request->get('ed_search');
        }else{
            $to_query = array();
            $query = Target::orderByDesc('start_time')->selectRaw("start_time")->get();
            if(count($query)>0){
                foreach($query as $key){
                    $ex = explode(' ',$key->start_time);
                    $ch = array_search($ex[0],$to_query);
                    if(empty($ch)){
                        $to_query[] = $ex[0];
                    }                    
                }  
                for($i=5; $i>=0; $i--){
                    if(!empty($to_query[$i])){         
                        $st_search = $to_query[$i];
                        break;
                    }
                }              
                // $st_search = $to_query[5];
                $ed_search =$to_query[0]; 
            }
        }
        $query = array();
        $to_target = array();
        // dd($m_sh[$set_sh]);
        //target            
            $to_date = date('Y-m-d',strtotime($ed_search . "+1 days"));
            $query = new Target;
            $query = $query->select('id', 'start_time', 'end_time', 'shift', 'note');
            if (!empty($st_search)) {
                $query = $query->whereBetween('targets.start_time', [$st_search, $to_date]);
            }            
            if (!empty($set_sh)) {     
                $query = $query->where('targets.shift', $m_sh[$set_sh]);
            }
            $query = $query->orderBy('start_time', 'DESC')->get();
            // ->paginate($perPage);
            // dd($query);

        //target_pd
            $query1 = TargetProduct::join('target_manpowers', 'target_products.id', '=', 'target_manpowers.target_product_id')
                ->selectRaw('target_products.target_id, target_products.id, SUM(target_manpowers.quantity) AS tg_man');
            $query1 = $query1->groupBy('target_products.target_id', 'target_products.id')->get();
            foreach ($query1 as $key) {
                $to_target[$key->target_id][$key->id] = $key->tg_man;
            }        

        return view('step.target_n.index', compact('query', 'to_target', 'sh', 'st_search', 'ed_search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $query = Shift::orderBy('name')->get();
        foreach ($query as $key) {
            $shift[$key->name]['st'] = $key->start_time;
            $shift[$key->name]['ed'] = $key->end_time;
        }
        return view('step.target_n.create', compact('shift'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        $st_date = date('Y-m-d H:i', strtotime($requestData['start_time']));
        $ed_date = date('Y-m-d H:i', strtotime($requestData['end_time']));
        // dd(date('Y-m-d H:i', strtotime($requestData['end_time'])));
        $check = Target::where('start_time', $st_date)
            ->where('end_time', $ed_date)
            ->count(); 
               
        if($check==0){
            //ไม่ซ้ำ ทำการหาจำนวนที่นำมาได้

            $to_save = array();
            $to_save['shift'] = $requestData['shift'];
            $to_save['start_time'] = $st_date; 
            $to_save['end_time'] = $ed_date;        
            $to_save['note'] = $requestData['note'];
            $to_save['user_id'] = auth()->user()->id;
            Target::create($to_save);

            return redirect('target')->with('success', ' Added!');  
        }else{
            return redirect()->back()->withInput($request->input())->with('error','วันเวลาที่ระบุ เคยมีการบันทึกแล้ว!');
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query[0] = Store::all();
        foreach ($query[0] as $key) {
            $data['store'][$key->id] = $key->name;
        }
        $query[1] = StepTg::all();
        foreach ($query[1] as $key) {
            $data['step'][$key->id] = $key->name;
        }
        $query[2] = Target::findOrFail($id);

        $query[3] = TargetProduct::join('target_manpowers', 'target_products.id', '=', 'target_manpowers.target_product_id')
            ->where('target_products.target_id', $id)
            ->select('target_products.target_id', 'target_products.step_tg_id', 'target_products.quantity AS pd_qty', 
                'target_manpowers.store_id', 'target_manpowers.quantity AS man_qty')
            ->orderBy('target_products.step_tg_id')->get();
        // $step = array('wash'=>'การซัก', 'dry'=>'การปั่น, การพับ', 'screen'=>'การคัด', 'shine'=>'การตาก');
        // $query[2] = TargetProduct::where('target_products.target_id', $id)->get();
        foreach ($query[3] as $key) {
            // if(empty($tg_pd) || $tg_pd <> $key->step_tg_id){
            //     $tg_pd = $key->step_tg_id;
                $data['tg_pd'][$key->step_tg_id] = $key->pd_qty;
            // }
            $data['tg_man'][$key->step_tg_id][$key->store_id] = $key->man_qty;
        }
        // dd($data);
        return view('step.target_n.detail',compact('query','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query = Target::findOrFail($id);
        $to_query = Shift::orderBy('name')->get();
        foreach ($to_query as $key) {
            $shift[$key->name]['st'] = $key->start_time;
            $shift[$key->name]['ed'] = $key->end_time;
        }
        // dd($query->id);
        return view('step.target_n.edit',compact('query', 'shift'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $st_date = date('Y-m-d H:i', strtotime($requestData['start_time']));
        $ed_date = date('Y-m-d H:i', strtotime($requestData['end_time']));

        $check = Target::where('start_time', $st_date)
            ->where('end_time', $ed_date)
            ->where('id', '<>', $id)
            ->count(); 
               
        if($check==0){
            //ไม่ซ้ำ ทำการหาจำนวนที่นำมาได้
            $query = Target::findOrfail($id);
            $to_save = array();
            $to_save['shift'] = $requestData['shift'];
            $to_save['start_time'] = $st_date; 
            $to_save['end_time'] = $ed_date;        
            $to_save['note'] = $requestData['note'];
            $to_save['user_id'] = auth()->user()->id;
            $query->update($to_save);

            return redirect('target')->with('success', ' Edited!');  
        }else{
            return redirect()->back()->withInput($request->input())->with('error','วันเวลาที่ระบุ เคยมีการบันทึกแล้ว!');
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            //  target_products ON target_manpowers.target_product_id = target_products.id
        $query1 = TargetManpower::join('target_products', 'target_manpowers.target_product_id', '=', 'target_products.id')
            ->where('target_products.target_id',$id)->delete();
        $query2 = TargetProduct::where('target_id',$id)->delete();        
        $query3 = Target::where('id',$id)->delete();
        // if($query1 && $query2 && $query3){
            return back()->with('success','Deleted!');
        // }else{
        //     return back()->with('error',"Can't delete!");
        // }
    }

    public function save_det(Request $request, $id)
    {
        // dd($id);
        $requestData = $request->all();
        // dd($requestData);
        //clear old
        $chk_man = 't';
        // $chk_pd = 't';
        $query = TargetProduct::where('target_id', $id)->get();
        foreach ($query as $key) {
            $chk = TargetManpower::where('target_product_id', $key->id)->delete();
            // dd($chk);
            if(!$chk)  $chk_man = 'f'; 
        }
        // if($chk_man == 't'){
            $chk_pd = TargetProduct::where('target_id', $id)->delete();
        //     dd($chk_pd);
        // }else{
        //     return redirect()->back()->withInput($request->input())->with('error','ลบข้อมูลแรงงานไม่ได้!');
        // }
        // if($chk_pd){
            foreach ($requestData['step_id'] as $key => $value) {
                if(!empty($value)){
                    $to_step = array();
                    $to_step['target_id'] = $id; 
                    $to_step['step_tg_id'] = $key;
                    $to_step['quantity'] = $value;  
                    $to_step['user_id'] = auth()->user()->id;
                    $pd_id = TargetProduct::create($to_step)->id;
                    foreach ($requestData['store_id'][$key] as $kman => $vman) {
                        if(!empty($vman)){
                            $to_store = array();
                            $to_store['target_product_id'] = $pd_id; 
                            $to_store['store_id'] = $kman;
                            $to_store['quantity'] = $vman;
                            $to_store['user_id'] = auth()->user()->id;
                            TargetManpower::create($to_store); 
                        }
                        
                    }
                }    
            }
            return redirect('target')->with('success', 'Added!');
        // }else{
        //     return redirect()->back()->withInput($request->input())->with('error','ลบข้อมูลงานไม่ได้!');
        // }        
    }

    public function clone_data(Request $request)
    {
        $requestData = $request->all();
        $ev = array();
        // dd($requestData);
        $query = Target::join('target_products', 'targets.id', '=', 'target_products.target_id')
            ->join('target_manpowers', 'target_products.id', '=', 'target_manpowers.target_product_id')
            ->select('target_products.step_tg_id', 'target_products.quantity AS pd_qty', 
                'target_manpowers.store_id', 'target_manpowers.quantity AS man_qty')
            ->whereRaw("datediff(day, targets.start_time, '".$requestData['date']."') = 0")
            ->where('targets.shift', $requestData['shift'])->get();
        // dd($query);
        if(count($query)>0){
            foreach ($query as $key) {
                $tg['pd'][$key->step_tg_id] = $key->pd_qty;
                $tg['man'][$key->step_tg_id][$key->store_id] = $key->man_qty;
            }

            foreach ($tg['pd'] as $key => $value) {
                $to_step = array();
                $to_step['target_id'] = $requestData['tg_id']; 
                $to_step['step_tg_id'] = $key;
                $to_step['quantity'] = $value;  
                $to_step['user_id'] = auth()->user()->id;
                $pd_id = TargetProduct::create($to_step)->id;
                foreach ($tg['man'][$key] as $kman => $vman) {
                    $to_store = array();
                    $to_store['target_product_id'] = $pd_id; 
                    $to_store['store_id'] = $kman;
                    $to_store['quantity'] = $vman;
                    $to_store['user_id'] = auth()->user()->id;
                    TargetManpower::create($to_store);                 
                }   
            }
            $ev[0] = 'success';
            $ev[1] = 'Added!';
        }else{
            $ev[0] = 'error';
            $ev[1] = 'ไม่พบข้อมูล!';
        }
        return redirect()->back()->with($ev[0],$ev[1]);
    }

    public function report($id)
    {
        $month_name = array("","มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        $query = Target::findOrFail($id);
        $m_date = date ("m", strtotime($query->start_time));
        $m_name = $month_name[intval($m_date)];
        // dd($m_name);
        $y_date = date ("Y", strtotime($query->start_time));
        $y_th = intval($y_date) + 543;
        // dd($y_th);
        $th_date = date ("d", strtotime($query->start_time)).' '.$m_name.' '.$y_th.'( กะ '.$query->shift.' '.date('H:i', strtotime($query->start_time)).'-'.date('H:i', strtotime($query->end_time)).')';
        // dd($th_date);
        $data = array();
        $row_span = array();
        $query_tg = TargetProduct::join('target_manpowers', 'target_products.id', '=', 'target_manpowers.target_product_id')
            ->select('target_products.step_tg_id', 'target_products.quantity AS pd_qty', 'target_manpowers.store_id', 'target_manpowers.quantity AS man_qty')
            ->where('target_products.target_id', $id)->get(); 
        foreach ($query_tg as $key) {
            $data['step_tg'][$key->step_tg_id] = $key->pd_qty;
            $data['store'][$key->step_tg_id][$key->store_id] = $key->man_qty;
        } 
        // dd($data);
        $query_st = Step::all();
        foreach ($query_st as $key) {
            $step[$key->id] = $key->name;
        }  
        $store = array();
        $query_st = Store::all();
        foreach ($query_st as $key) {
            $store[$key->id] = $key->name;
        }
        $query_st = StepTg::all();
        foreach ($query_st as $key) {
            $step_tg['name'][$key->id] = $key->name;
            $exp = explode(',', $key->refer_step);
            foreach ($exp as $kexp => $vexp) {
                $step_tg['ref_to'][$key->id][$vexp] = $step[$vexp];
            }
        }

        $to_query = Product::all();
        foreach ($to_query as $key) {
            $pd[$key->id] = $key->name;
        }
        $set_date = date ("Y-m-d", strtotime($query->start_time));
        // $tbl_step = array('to_reuses', 'washes', 'dries', 'shines', 'choose_boxes', 'stocks');
        // $tbl_step = array('washes', 'dries', 'shines', 'choose_boxes', 'stocks');
        // foreach ($tbl_step as $key => $value) {
        //     $query_qty[$value] = DB::table($value)->selectRaw('sum(quantity) AS quantity')->where('to_date', $set_date)->where('shift', $query->shift)->where('status', '1')->get();
        // }
        $query_qty = array();
        // 1 => "Reuse", 2 => "Wash", 3 => "Dry", 4 => "Stock", 5 => "Use", 6 => "Choose_box", 7 => "Shine"
        $to_query = Wash::join('to_reuses', 'washes.to_reuse_id','=', 'to_reuses.id')
            ->selectRaw('to_reuses.product_id, sum(washes.quantity) AS quantity')
            ->where('washes.to_date', $set_date)->where('washes.shift', $query->shift)->where('washes.status', '1')
            ->groupBy('to_reuses.product_id')->get();
        foreach ($to_query as $key) {
            $query_qty[2][$key->product_id] = $key->quantity;
        }

        $to_query = Dry::join('washes', 'dries.wash_id','=', 'washes.id')
            ->join('to_reuses', 'washes.to_reuse_id','=', 'to_reuses.id')
            ->selectRaw('to_reuses.product_id, sum(dries.quantity) AS quantity')
            ->where('dries.to_date', $set_date)->where('dries.shift', $query->shift)->where('dries.status', '1')
            ->groupBy('to_reuses.product_id')->get();
        foreach ($to_query as $key) {
            $query_qty[3][$key->product_id] = $key->quantity;
        }
        $to_query = Shine::join('washes', 'shines.wash_id','=', 'washes.id')
            ->join('to_reuses', 'washes.to_reuse_id','=', 'to_reuses.id')
            ->selectRaw('to_reuses.product_id, sum(shines.quantity) AS quantity')
            ->where('shines.to_date', $set_date)->where('shines.shift', $query->shift)->where('shines.status', '1')
            ->groupBy('to_reuses.product_id')->get();
        foreach ($to_query as $key) {
            $query_qty[7][$key->product_id] = $key->quantity;
        }
        $to_query = ChooseBox::join('to_reuses', 'choose_boxes.to_reuse_id','=', 'to_reuses.id')
            ->selectRaw('to_reuses.product_id, sum(choose_boxes.quantity) AS quantity')
            ->where('choose_boxes.to_date', $set_date)->where('choose_boxes.shift', $query->shift)->where('choose_boxes.status', '1')
            ->groupBy('to_reuses.product_id')->get();
        foreach ($to_query as $key) {
            $query_qty[6][$key->product_id] = $key->quantity;
        }        
        $to_query = Stock::join('to_reuses', 'stocks.to_reuse_id','=', 'to_reuses.id')
            ->selectRaw('to_reuses.product_id, sum(stocks.quantity) AS quantity')
            ->where('stocks.to_date', $set_date)->where('stocks.shift', $query->shift)->where('stocks.status', '1')
            ->groupBy('to_reuses.product_id')->get();
        foreach ($to_query as $key) {
            $query_qty[4][$key->product_id] = $key->quantity;
        }
        // dd($data);
        $to_pd = array();
        if(!empty($data['step_tg'])){
            foreach ($data['step_tg'] as $key => $value) {  //1,2,3
                $row_span[$key] = 0;
                // echo 'data->'.$key.'</br>';
                foreach ($step_tg['ref_to'][$key] as $kstep => $vstep) {                
                    // echo 'ref_to->'.$kstep.'</br>';  
                    if(!empty($query_qty[$kstep])){
                        foreach ($query_qty[$kstep] as $kpd => $vpd) {            
                            // echo 'query_qty->'.$kpd.'</br>';  
                            $row_span[$key] += 1;
                            if(empty($to_pd[$key][$kpd]))   $to_pd[$key][$kpd] = $vpd;
                            else    $to_pd[$key][$kpd] += $vpd;
                        }
                    }  
                }
            }

            foreach ($row_span as $key => $value) {
                if(empty($value)){
                    $row_span[$key] = 1;
                }else{
                    $row_span[$key] = count($to_pd[$key]);
                }
            }
        }
        // dd($query_qty);
        return view('step.target_n.report',compact('query','th_date', 'data', 'store', 'step_tg', 'pd', 'query_qty', 'row_span', 'to_pd'));
    }
}
