<?php

namespace App\Http\Controllers\FS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FS\FsStock;
use App\Models\FS\FsStockTrans;
use App\Models\FS\FsHwMain;
use App\Mail\FSStockUseMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;

class FsReportsController extends Controller
{
    public function fsstockuseRpt()
    {
        $datas = FsHwMain::all();

        $data = [];
        $queries = [];
        require_once app_path() . '/jpgraph/jpgraph.php';
        require_once app_path() . '/jpgraph/jpgraph_bar.php';

        foreach ($datas as $subdata) {
            # code...
            $data[$subdata->id] = $subdata;

            $query = FsStock::join('fs_hw_locs', 'fs_stocks.fs_wh_loc_id', '=', 'fs_hw_locs.id')
                ->join('products', 'fs_stocks.product_id', '=', 'products.id')
                ->where('fs_hw_locs.fs_wh_id', $subdata->id)
                ->selectRaw('products.name, SUM(fs_stocks.volumn) AS volumn')
                ->groupBy('products.name')
                ->get();



            // var_dump($query);

            if ($query->count() > 0) {
                $queries[$subdata->id] = $query;
                $dataX = array();
                $dataY = array();
                foreach ($query as $item) {
                    $dataX[] = $item->name;
                    $dataY[] = $item->volumn;
                }

                //แสดง Chart ข้อที่ 3 -------------------

                // Some data
                $graph1 = new \Graph(700, 500);
                $graph1->SetScale("textlin");
                $graph1->SetMargin(50, 10, 100, 150);
                $theme_class = new \UniversalTheme;
                //$graph1->SetTheme($theme_class);
                // //$graph->yaxis->SetTickPositions(array(0, 30, 60, 90, 120, 150), array(15, 45, 75, 105, 135));
                $graph1->SetBox(false);

                $graph1->ygrid->SetFill(false);
                $graph1->xaxis->SetTickLabels($dataX);    //
                $graph1->xaxis->SetLabelAlign('center', 'top');
                $graph1->xaxis->SetLabelAngle(50);
                $graph1->xaxis->title->SetFont(FF_ANGSA, FS_BOLD, 18);
                $graph1->xaxis->SetFont(FF_ANGSA, FS_NORMAL, 14);
                // $graph2->xaxis->Hide();  //ซ่อน label ใต้กราฟ ต้องอยู่ใต้ SetMarginColor
                $graph1->yaxis->SetColor("#000000");
                $graph1->yaxis->HideLine(false);    //เส้นแกน x ของกราฟ false->show, ture->hide
                $graph1->yaxis->HideTicks(false, false);
                $graph1->yaxis->SetFont(FF_ANGSA, FS_NORMAL, 14);


                // Create the bar plots
                $b1plot1 = new \BarPlot($dataY);

                $graph1->Add($b1plot1);

                $graph1->legend->SetPos(0.5, 0.1, 'center', 'top');

                $b1plot1->SetColor("#000000");  //สีเส้น bar
                $b1plot1->SetFillColor("#B06126");  //สี bar
                $b1plot1->SetLegend("Stock Use");


                // $b1plot2->value->SetFormat('%01.1f');
                $b1plot1->value->SetFormat('%d');
                $b1plot1->value->SetColor("#000000");
                $b1plot1->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
                $b1plot1->value->Show();
                $path = public_path() . '/graph/' . date('Y') . "/" . date('m') . '/mail';
                if (!File::exists($path)) {
                    File::makeDirectory($path,  0777, true, true);
                }

                $date = date('ymdHis');

                $chartname[$subdata->id]['bar']['link'] = "graph/" . date('Y') . "/" . date('m') . "/mail/bar_rpt_" .  $date . ".jpg";

                $chartname[$subdata->id]['bar']['path'] = public_path() . "/graph/" . date('Y') . "/" . date('m') . "/mail/bar_rpt_" .  $date . ".jpg";
                $graph1->Stroke($chartname[$subdata->id]['bar']['path']);
            }
        }

        return view('FS.Reports.index', compact('data', 'queries', 'chartname'));
    }

    public function fsstockuseRptEmail()
    {

        $datas = FsHwMain::all();

        $data = [];
        $queries = [];
        require_once app_path() . '/jpgraph/jpgraph.php';
        require_once app_path() . '/jpgraph/jpgraph_bar.php';

        foreach ($datas as $subdata) {
            # code...
            $data[$subdata->id] = $subdata;

            $query = FsStock::join('fs_hw_locs', 'fs_stocks.fs_wh_loc_id', '=', 'fs_hw_locs.id')
            ->join('products', 'fs_stocks.product_id', '=', 'products.id')
            ->where('fs_hw_locs.fs_wh_id', $subdata->id)
                ->selectRaw('products.name, SUM(fs_stocks.volumn) AS volumn')
                ->groupBy('products.name')
                ->get();



            // var_dump($query);

            if ($query->count() > 0) {
                $queries[$subdata->id] = $query;
                $dataX = array();
                $dataY = array();
                foreach ($query as $item) {
                    $dataX[] = $item->name;
                    $dataY[] = $item->volumn;
                }

                //แสดง Chart ข้อที่ 3 -------------------

                // Some data
                $graph1 = new \Graph(700, 500);
                $graph1->SetScale("textlin");
                $graph1->SetMargin(50, 10, 100, 150);
                $theme_class = new \UniversalTheme;
                //$graph1->SetTheme($theme_class);
                // //$graph->yaxis->SetTickPositions(array(0, 30, 60, 90, 120, 150), array(15, 45, 75, 105, 135));
                $graph1->SetBox(false);

                $graph1->ygrid->SetFill(false);
                $graph1->xaxis->SetTickLabels($dataX);    //
                $graph1->xaxis->SetLabelAlign('center', 'top');
                $graph1->xaxis->SetLabelAngle(50);
                $graph1->xaxis->title->SetFont(FF_ANGSA, FS_BOLD, 18);
                $graph1->xaxis->SetFont(FF_ANGSA, FS_NORMAL, 14);
                // $graph2->xaxis->Hide();  //ซ่อน label ใต้กราฟ ต้องอยู่ใต้ SetMarginColor
                $graph1->yaxis->SetColor("#000000");
                $graph1->yaxis->HideLine(false);    //เส้นแกน x ของกราฟ false->show, ture->hide
                $graph1->yaxis->HideTicks(false, false);
                $graph1->yaxis->SetFont(FF_ANGSA, FS_NORMAL, 14);


                // Create the bar plots
                $b1plot1 = new \BarPlot($dataY);

                $graph1->Add($b1plot1);

                $graph1->legend->SetPos(0.5, 0.1, 'center', 'top');

                $b1plot1->SetColor("#000000");  //สีเส้น bar
                $b1plot1->SetFillColor("#B06126");  //สี bar
                $b1plot1->SetLegend("Stock Use");


                // $b1plot2->value->SetFormat('%01.1f');
                $b1plot1->value->SetFormat('%d');
                $b1plot1->value->SetColor("#000000");
                $b1plot1->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
                $b1plot1->value->Show();
                $path = public_path() . '/graph/' . date('Y') . "/" . date('m') . '/mail';
                if (!File::exists($path)) {
                    File::makeDirectory($path,  0777, true, true);
                }

                $date = date('ymdHis');

                $chartname[$subdata->id]['bar']['link'] = "graph/" . date('Y') . "/" . date('m') . "/mail/bar_rpt_" .  $date . ".jpg";

                $chartname[$subdata->id]['bar']['path'] = public_path() . "/graph/" . date('Y') . "/" . date('m') . "/mail/bar_rpt_" .  $date . ".jpg";
                $graph1->Stroke($chartname[$subdata->id]['bar']['path']);
            }
        }

        return view('FS.mails.index', compact('data', 'queries', 'chartname'));
    }

    public function fsstockuseRptEmailAction(Request $request)
    {
        $requestData = $request->all();
        $note = $requestData['note'];
        $datas = FsHwMain::all();

        $data = [];
        $queries = [];
        require_once app_path() . '/jpgraph/jpgraph.php';
        require_once app_path() . '/jpgraph/jpgraph_bar.php';

        foreach ($datas as $subdata) {
            # code...
            $data[$subdata->id] = $subdata;

            $query = FsStock::join('fs_hw_locs', 'fs_stocks.fs_wh_loc_id', '=', 'fs_hw_locs.id')
            ->join('products', 'fs_stocks.product_id', '=', 'products.id')
            ->where('fs_hw_locs.fs_wh_id', $subdata->id)
                ->selectRaw('products.name, SUM(fs_stocks.volumn) AS volumn')
                ->groupBy('products.name')
                ->get();



            // var_dump($query);

            if ($query->count() > 0) {
                $queries[$subdata->id] = $query;
                $dataX = array();
                $dataY = array();
                foreach ($query as $item) {
                    $dataX[] = $item->name;
                    $dataY[] = $item->volumn;
                }

                //แสดง Chart ข้อที่ 3 -------------------

                // Some data
                $graph1 = new \Graph(700, 500);
                $graph1->SetScale("textlin");
                $graph1->SetMargin(50, 10, 100, 150);
                $theme_class = new \UniversalTheme;
                //$graph1->SetTheme($theme_class);
                // //$graph->yaxis->SetTickPositions(array(0, 30, 60, 90, 120, 150), array(15, 45, 75, 105, 135));
                $graph1->SetBox(false);

                $graph1->ygrid->SetFill(false);
                $graph1->xaxis->SetTickLabels($dataX);    //
                $graph1->xaxis->SetLabelAlign('center', 'top');
                $graph1->xaxis->SetLabelAngle(50);
                $graph1->xaxis->title->SetFont(FF_ANGSA, FS_BOLD, 18);
                $graph1->xaxis->SetFont(FF_ANGSA, FS_NORMAL, 14);
                // $graph2->xaxis->Hide();  //ซ่อน label ใต้กราฟ ต้องอยู่ใต้ SetMarginColor
                $graph1->yaxis->SetColor("#000000");
                $graph1->yaxis->HideLine(false);    //เส้นแกน x ของกราฟ false->show, ture->hide
                $graph1->yaxis->HideTicks(false, false);
                $graph1->yaxis->SetFont(FF_ANGSA, FS_NORMAL, 14);


                // Create the bar plots
                $b1plot1 = new \BarPlot($dataY);

                $graph1->Add($b1plot1);

                $graph1->legend->SetPos(0.5, 0.1, 'center', 'top');

                $b1plot1->SetColor("#000000");  //สีเส้น bar
                $b1plot1->SetFillColor("#B06126");  //สี bar
                $b1plot1->SetLegend("Stock Use");


                // $b1plot2->value->SetFormat('%01.1f');
                $b1plot1->value->SetFormat('%d');
                $b1plot1->value->SetColor("#000000");
                $b1plot1->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
                $b1plot1->value->Show();
                $path = public_path() . '/graph/' . date('Y') . "/" . date('m') . '/mail';
                if (!File::exists($path)) {
                    File::makeDirectory($path,  0777, true, true);
                }

                $date = date('ymdHis');

                $chartname[$subdata->id]['bar']['link'] = "graph/" . date('Y') . "/" . date('m') . "/mail/bar_rpt_" .  $date . ".jpg";

                $chartname[$subdata->id]['bar']['path'] = public_path() . "/graph/" . date('Y') . "/" . date('m') . "/mail/bar_rpt_" .  $date . ".jpg";
                $graph1->Stroke($chartname[$subdata->id]['bar']['path']);
            }
        }

        $this->_fsstockuseRptSendEmail($data, $queries, $chartname,$note);
        return redirect('/setting/FsWhs' )->with('success', 'Send Email '.date('d/m/Y').' Complete!');
    }

    public function fsstockuseRptExport($date)
    {
    }

    private function _fsstockuseRptExcelExport($data)
    {
    }

    private function _fsstockuseRptSendEmail($data, $queries, $chartname, $note)
    {
        $mail_to = Mail::send(new FSStockUseMail($data, $queries, $chartname, $note));
    }
}
