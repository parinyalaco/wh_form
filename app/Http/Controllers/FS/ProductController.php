<?php

namespace App\Http\Controllers\FS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\FS\Product;
use App\Models\FS\ToReuse;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Product::orderBy('name')->get();
        $reuse = array();
        $query1 = ToReuse::select('product_id')->groupBy('product_id')->get();
        foreach ($query1 as $key) {
            $reuse[$key->product_id] = $key->product_id;
        }
        // dd($reuse);
        return view('base_data.product.index', compact('query', 'reuse'));
    }

    public function set_status(Request $request)
    {
        // dd($request['chk']);
        if($request['set']=='wash'){
            $set = 'status_wash';
            $tex1 = 'ต้องซัก';
        }else{
            $set = 'status_check';
            $tex1 = 'ต้องรอผลตรวจ';
        }
        if($request['chk'] == 'check'){
            $set_status[$set] = '1';
            $tex = "ปรับเป็น '".$tex1."' แล้ว";
        }else{
            $set_status[$set] = '0';
            $tex = "ปรับเป็น 'ไม่".$tex1."' แล้ว";
        }    

        $query = Product::where('id',$request['id'])
            ->update($set_status);

        return $tex;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('base_data.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $v = Validator::make($request->all(), [
            'name' => 'required|unique:products|max:255',
        ]);
    
        if ($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }

        $requestData = $request->all();
        Product::create($requestData);

        return redirect('base_data/product')->with('success', ' Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query = Product::findOrFail($id);
        return view('base_data.product.edit', compact('query'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $request->validate([
            'name' => 'required|unique:products,name,'.$id,
        ]);

        $requestData = $request->all();
        if(empty($requestData['status_wash']))  $requestData['status_wash'] = '0';
        $query = Product::findOrFail($id);
        $query->update($requestData);

        return redirect('base_data/product')->with('success', 'Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = Product::where('id',$id)->delete();
        if($query){
            return back()->with('success','Deleted!');
        }else{
            return back()->with('error',"Can't delete!");
        }
    }
}
