<?php

namespace App\Http\Controllers\FS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\FS\Stock;
use App\Models\FS\ToReuse;
// use App\Models\FS\Wash;
use App\Models\FS\Dry;
use App\Models\FS\Shine;
use App\Models\FS\ChooseBox;
use App\Models\FS\Product;
use App\Models\FS\Step;
use App\Models\FS\Lose;
use App\Models\FS\StockProcess;
use App\Models\FS\FsWhTranDs;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {      
        $set_pd = $request->get('set_pd');
        if($request->get('st_search') != ""){
            $st_search = $request->get('st_search');
            if(empty($request->get('ed_search')))    $ed_search = date('Y-m-d');
            else    $ed_search = $request->get('ed_search');
        }else{
            $to_query = array();
            $query = Stock::groupBy('to_date')->orderByDesc('to_date')->selectRaw('TOP (5) to_date')->get();
            if(count($query)>0){
                foreach($query as $key){
                    $to_query[] = $key->to_date;               
                }                
                for($i=4; $i>=0; $i--){
                    if(!empty($to_query[$i])){         
                        $st_search = $to_query[$i];
                        break;
                    }
                }              
                // $st_search = $to_query[4];
                $ed_search =$to_query[0];
                
                $request->replace(['st_search' => $st_search, 'ed_search' => $ed_search]);
            }
        }

        // $query = Product::all();
        // foreach ($query as $key) {
        //     $pd[$key->id] = $key->name;
        // }

        $to_show = array();
        $case = '';
        if (!empty($set_pd)) {     
            $case .= "(to_reuses.product_id = ".$set_pd.")";
            if (!empty($st_search)) $case .= " AND ";
        }
        if (!empty($st_search)) {
            $case .= "(stocks.to_date Between '".$st_search."' AND '".$ed_search."')";
        } 
        $query1 = $this->main_query($case);
        // $query1 = $this->main_query($set_pd, $st_search, $ed_search,'','');
        // // $query1 = $main_query[0];
        // // $to_fs = $main_query[1];

        if(!empty($query1)){          
            $arr1 = array('quantity', 'pass', 'wait', 'not_pass','to_fs', 'to_stock_process', 'lose_qty');
            $arr2 = array('id', 'note');
            foreach ($query1 as $key) {
                foreach ($arr1 as $karr=>$varr) {
                    if(empty($to_show[$key->to_date][$key->product_id][$key->shift][$key->pallet_no][$varr]))
                        $to_show[$key->to_date][$key->product_id][$key->shift][$key->pallet_no][$varr] = $key->$varr;
                    else
                        $to_show[$key->to_date][$key->product_id][$key->shift][$key->pallet_no][$varr] += $key->$varr;
                }
                foreach ($arr2 as $karr=>$varr) {
                    if(empty($to_show[$key->to_date][$key->product_id][$key->shift][$key->pallet_no][$varr]))
                        $to_show[$key->to_date][$key->product_id][$key->shift][$key->pallet_no][$varr] = $key->$varr;
                    else
                        $to_show[$key->to_date][$key->product_id][$key->shift][$key->pallet_no][$varr] .= ','.$key->$varr;
                }
            }  
        }
        // dd($to_show);
        $set_num = $this->set_num();
        $pd = $set_num[0];
        $to_stock = $set_num[1];
        $to_stock = array_map('array_filter', $to_stock);
        foreach ($to_stock as $kpd => $vpd) {
            $to_stock[$kpd] = array_map('array_filter', $to_stock[$kpd]);
            $to_stock[$kpd] = array_filter($to_stock[$kpd]);
        }
        $to_stock = array_filter($to_stock);

        return view('step.stock.index', compact('pd', 'to_show', 'to_stock'));
    }

    public function main_query($where) //main->tbl wash ใช้กับ index ข้อมูลการซัก
    {

        $query = new Stock;
        $query = $query->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id');        
        $query = $query->selectRaw('stocks.id, stocks.to_date, stocks.shift, stocks.pallet_no, to_reuses.product_id, stocks.note, 
            stocks.quantity, stocks.pass, stocks.wait, stocks.not_pass, 
            (SELECT sum(fs_wh_tran_ds.volumn)
                FROM   fs_wh_tran_ds
                WHERE fs_wh_tran_ds.stock_id = stocks.id) AS to_fs,
            (SELECT count(stock_processes.id)
                FROM   stock_processes
                WHERE stock_processes.stock_id = stocks.id) AS to_stock_process, 
            (SELECT SUM(loses.quantity)
                FROM   loses
                WHERE loses.refer_id = stocks.id AND loses.step_id = 4) AS lose_qty');
        $query = $query->where('stocks.status', '1');  
        if(!empty($where))  $query = $query->whereRaw($where);    
        
        $query = $query->orderBy('stocks.to_date', 'DESC')->orderBy('stocks.shift')
            ->get();
        
        return $query;
    }

    public function main_query2($where) //main->tbl reuse ใช้กับ set_num, to_save จำนวนตั้งต้นก่อนซัก
    {
        $pd = array();
        $pd_set_num = array();
        $query = Product::orderBy('name')->get();
        foreach ($query as $key) {
            $pd[$key->id] = $key->name;            
            // $pd_set_num[] = $key->id;
        }
        $query = array();
        //dry
            $query[0] = new Dry;
            $query[0] = $query[0]->join('washes', 'dries.wash_id', '=', 'washes.id')
                ->join('to_reuses', 'washes.to_reuse_id', '=', 'to_reuses.id');
            $query[0] = $query[0]->selectRaw('dries.id, dries.to_date, dries.shift, to_reuses.product_id, dries.quantity, to_reuses.id as reuse_id, 
                (SELECT SUM(stocks.quantity)
                    FROM   stocks
                    WHERE (stocks.dry_id = dries.id) AND (stocks.status = 1)) AS stocks_qty, 
                (SELECT SUM(loses.quantity)
                    FROM   loses
                    WHERE loses.refer_id = dries.id AND loses.step_id = 3) AS lose_qty'); 
            if(!empty($where[0]))  $query[0] = $query[0]->whereRaw($where[0]); 
            // $query[0] = $query[0]->whereIn('to_reuses.product_id', $pd_set_num);
            // $query[0] = $query[0]->groupBy('dries.id', 'dries.to_date', 'dries.shift', 'to_reuses.product_id')
            $query[0] = $query[0]->orderBy('dries.to_date')->orderBy('dries.shift')
                ->get();

        //shine
            $query[1] = new Shine;
            $query[1] = $query[1]->join('washes', 'shines.wash_id', '=', 'washes.id')
                ->join('to_reuses', 'washes.to_reuse_id', '=', 'to_reuses.id');
            $query[1] = $query[1]->selectRaw('shines.id, shines.to_date, shines.shift, to_reuses.product_id, shines.quantity, to_reuses.id as reuse_id, 
                (SELECT SUM(stocks.quantity)
                    FROM   stocks
                    WHERE (stocks.shine_id = shines.id) AND (stocks.status = 1)) AS stocks_qty, 
                (SELECT SUM(loses.quantity)
                    FROM   loses
                    WHERE loses.refer_id = shines.id AND loses.step_id = 7) AS lose_qty'); 
            if(!empty($where[1]))  $query[1] = $query[1]->whereRaw($where[1]); 
            // $query[1] = $query[1]->whereIn('to_reuses.product_id', $pd_set_num);
            // $query[1] = $query[1]->groupBy('shines.id', 'shines.to_date', 'shines.shift', 'to_reuses.product_id')
            $query[1] = $query[1]->orderBy('shines.to_date')->orderBy('shines.shift')
                ->get();
            // dd($query[1]);
        //choose_box
            $query[2] = new ChooseBox;
            $query[2] = $query[2]->join('to_reuses', 'choose_boxes.to_reuse_id', '=', 'to_reuses.id');
            $query[2] = $query[2]->selectRaw('choose_boxes.id, choose_boxes.to_date, choose_boxes.shift, to_reuses.product_id, 
                choose_boxes.quantity, to_reuses.id as reuse_id, 
                (SELECT SUM(stocks.quantity)
                    FROM   stocks
                    WHERE (stocks.choose_box_id = choose_boxes.id) AND (stocks.status = 1)) AS stocks_qty, 
                (SELECT SUM(loses.quantity)
                    FROM   loses
                    WHERE loses.refer_id = choose_boxes.id AND loses.step_id = 6) AS lose_qty'); 
            if(!empty($where[2]))  $query[2] = $query[2]->whereRaw($where[2]); 
            // $query[1] = $query[1]->whereIn('to_reuses.product_id', $pd_set_num);
            // $query[2] = $query[2]->groupBy('choose_boxes.id', 'choose_boxes.to_date', 'choose_boxes.shift', 'to_reuses.product_id')
            $query[2] = $query[2]->orderBy('choose_boxes.to_date')->orderBy('choose_boxes.shift')
                ->get();

        // dd($query);
        return [$query, $pd];
    }

    public function set_num()
    {
        $case = '';
        $main_query2 = $this->main_query2($case);
        $query1 = $main_query2[0];
        $pd = $main_query2[1];        
        // dd($query1);
        $to_show = array();
        foreach ($query1 as $kid=>$vid) {
            foreach ($vid as $key) {
                if(!empty($to_show[$key->product_id][$key->to_date][$key->shift]))
                    $to_show[$key->product_id][$key->to_date][$key->shift] += $key->quantity - $key->stocks_qty - $key->lose_qty; 
                else
                    $to_show[$key->product_id][$key->to_date][$key->shift] = $key->quantity - $key->stocks_qty - $key->lose_qty;  
            }       
        }

        return [$pd, $to_show];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $requestData = $request->all();
        $set_num = $this->set_num();
        $pd = $set_num[0];
        $to_show = $set_num[1];
        $to_show = array_map('array_filter', $to_show);
        foreach ($to_show as $kpd => $vpd) {
            $to_show[$kpd] = array_map('array_filter', $to_show[$kpd]);
            $to_show[$kpd] = array_filter($to_show[$kpd]);
        }
        $to_show = array_filter($to_show);
        // dd($to_show);
        if(empty($to_show)){
            return redirect('stock')->with('error', 'ไม่มีสินค้าสำหรับการพับ!');
        }else{
            return view('step.stock.create',compact('pd', 'to_show', 'requestData'));
        }         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'product_id' => 'required',
        ]);
    
        if ($v->fails()){
            return redirect()->back()->withInput()->with('error', 'กรุณาเลือกสินค้าด้วยค่ะ');
        }

        $requestData = $request->all();
        $to_save = $this->to_save($requestData);
        // dd($to_save);
        if($to_save[0]=='<'){
            return redirect()->back()->withInput($request->input())->with('error','จำนวนที่มีจนถึงวันที่เลือก มีน้อยกว่าจำนวนที่จะใช้!');
        }elseif ($to_save[0]=='again') {
            return redirect()->back()->withInput($request->input())->with('error','รายการสินค้าในวันที่และกะนี้เคยมีการบันทึกแล้ว กรุณาใช้การแก้ไขแทนค่ะ!');
        }elseif ($to_save[0]=='not') {
            return redirect()->back()->withInput($request->input())->with('error','ไม่พบรายการซักที่สามารถปั่นได้!');
        }else{
            return redirect('stock')->with('success', ' Added!');
        }
    }

    public function to_save($requestData){
        $to_return = '';
        //ดูว่ามีค่าที่เป็นวันที่ กะ สินค้า pallet ซ้ำมั้ย
        $check = Stock::join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
            ->where('stocks.to_date', $requestData['to_date'])
            ->where('stocks.shift', $requestData['shift'])
            ->where('to_reuses.product_id', $requestData['product_id'])
            ->where('stocks.pallet_no', $requestData['pallet_no'])
            ->where('stocks.status', 1)
            ->count(); 
        // dd($check);       
        if($check==0){
            //ไม่ซ้ำ ทำการหาจำนวนที่นำมาได้
            $reuse = array();            
            
                $query[0] = new Dry;
                $query[0] = $query[0]->join('washes', 'dries.wash_id', '=', 'washes.id')
                    ->join('to_reuses', 'washes.to_reuse_id', '=', 'to_reuses.id');
                $query[0] = $query[0]->selectRaw("dries.id, dries.to_date, dries.shift, to_reuses.product_id, dries.quantity, 'dry' as type, to_reuses.id as reuse_id, 
                    (SELECT SUM(stocks.quantity)
                        FROM   stocks
                        WHERE (stocks.dry_id = dries.id) AND (stocks.status = 1)) AS stocks_qty, 
                    (SELECT SUM(loses.quantity)
                        FROM   loses
                        WHERE loses.refer_id = dries.id AND loses.step_id = 3) AS lose_qty"); 
                $query[0] = $query[0]->where('dries.to_date', '<=', $requestData['to_date']);
                $query[0] = $query[0]->where('to_reuses.product_id', $requestData['product_id']);
                $query[0] = $query[0]->orderBy('dries.to_date')->orderBy('dries.shift')
                    ->get();

                $query[1] = new Shine;
                $query[1] = $query[1]->join('washes', 'shines.wash_id', '=', 'washes.id')
                    ->join('to_reuses', 'washes.to_reuse_id', '=', 'to_reuses.id');
                $query[1] = $query[1]->selectRaw("shines.id, shines.to_date, shines.shift, to_reuses.product_id, shines.quantity, 'shine' as type, to_reuses.id as reuse_id, 
                    (SELECT SUM(stocks.quantity)
                        FROM   stocks
                        WHERE (stocks.shine_id = shines.id) AND (stocks.status = 1)) AS stocks_qty, 
                    (SELECT SUM(loses.quantity)
                        FROM   loses
                        WHERE loses.refer_id = shines.id AND loses.step_id = 7) AS lose_qty"); 
                $query[1] = $query[1]->where('shines.to_date', '<=', $requestData['to_date']);
                $query[1] = $query[1]->where('to_reuses.product_id', $requestData['product_id']);
                $query[1] = $query[1]->orderBy('shines.to_date')->orderBy('shines.shift')
                    ->get();

                $query[2] = new ChooseBox;
                $query[2] = $query[2]->join('to_reuses', 'choose_boxes.to_reuse_id', '=', 'to_reuses.id');
                $query[2] = $query[2]->selectRaw("choose_boxes.id, choose_boxes.to_date, choose_boxes.shift, to_reuses.product_id, 
                    choose_boxes.quantity, 'choose_box' as type, to_reuses.id as reuse_id, 
                    (SELECT SUM(stocks.quantity)
                        FROM   stocks
                        WHERE (stocks.choose_box_id = choose_boxes.id) AND (stocks.status = 1)) AS stocks_qty, 
                    (SELECT SUM(loses.quantity)
                        FROM   loses
                        WHERE loses.refer_id = choose_boxes.id AND loses.step_id = 6) AS lose_qty"); 
                $query[2] = $query[2]->where('choose_boxes.to_date', '<=', $requestData['to_date']);
                $query[2] = $query[2]->where('to_reuses.product_id', $requestData['product_id']);
                $query[2] = $query[2]->orderBy('choose_boxes.to_date')->orderBy('choose_boxes.shift')
                    ->get();

                $allItems = new \Illuminate\Database\Eloquent\Collection; //Create empty collection which we know has the merge() method
                $allItems = $allItems->merge($query[0]);
                $allItems = $allItems->merge($query[1]);
                $allItems = $allItems->merge($query[2]);
                $allItems = $allItems->sortBy('to_date')->sortBy('shift');
                
            if(!empty($allItems)){ 
                foreach ($allItems as $key) {    //0=dries, 1=shines, 2=choose_boxes                               
                    $reuse['quantity'][$key->to_date][$key->shift][$key->type][$key->id] = $key->quantity - $key->stocks_qty - $key->lose_qty; 
                    $reuse['reuse_id'][$key->to_date][$key->shift][$key->type][$key->id] = $key->reuse_id;     
                }
            }

            $pd = Product::findOrFail($requestData['product_id']);
            
            if(!empty($reuse)){
                $to_wash = $requestData['quantity'];

                $arr = array('dry'=>'dry_id', 'shine'=>'shine_id', 'choose_box'=>'choose_box_id');
                $to_save = array();
                $to_save['to_date'] = $requestData['to_date'];
                $to_save['shift'] = $requestData['shift'];
                $to_save['pallet_no'] = $requestData['pallet_no'];        
                $to_save['note'] = $requestData['note'];
                $to_save['user_id'] = auth()->user()->id;
                
                foreach ($reuse['quantity'] as $kdate => $vdate) {
                    foreach ($vdate as $ksh => $vsh) {
                        foreach ($vsh as $ktype => $vtype) {
                            foreach ($vtype as $kid => $vid) {
                                foreach ($arr as $karr => $varr) {
                                    if(!empty($reuse['quantity'][$kdate][$ksh][$karr][$kid]))   $to_save[$varr] = $kid;
                                    else $to_save[$varr] = null;
                                } 
                                $to_save['to_reuse_id'] = $reuse['reuse_id'][$kdate][$ksh][$ktype][$kid];
                                if($to_wash>0){ 
                                    if($vid>0){ 
                                        if($pd->status_wash==1){  
                                            if($to_wash > $vid){
                                                $quantity = $vid;                                 
                                            }else{
                                                $quantity = $to_wash;
                                            }           
                                            $to_save['quantity'] = $quantity;
                                            if($pd->status_check==1){
                                                $to_save['wait'] = $quantity; 
                                            }else{
                                                $to_save['wait'] = 0; 
                                                $to_save['pass'] = $quantity; 
                                            } 
                                            Stock::create($to_save);
                                            $to_wash -= $to_save['quantity'];
                                        }else{ 
                                            if($to_wash > $vid){
                                                $quantity = $vid;  
                                            }else{
                                                $quantity = $to_wash;
                                            }                                       
                                            $to_save['quantity'] = $quantity;
                                            $to_save['wait'] = 0; 
                                            $to_save['pass'] = $quantity;        
                                            Stock::create($to_save);
                                            $to_wash -= $to_save['quantity'];
                                        }
                                    }   
                                } 
                            }                          
                        } 
                    }                   
                } 
                // return redirect('wash')->with('success', ' Added!');
                $to_return = 'success';                     
            }else{
                // return back()->with('error','ไม่พบรายการรับเข้าที่สามารถตัดได้!');
                $to_return = 'not';
            }
        }else{
            // return back()->with('error','รายการสินค้าในวันที่และกะนี้เคยมีการบันทึกแล้ว กรุณาใช้การแก้ไขแทนค่ะ!');
            $to_return = 'again';
        } 
        return [$to_return];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $requestData = $request->all();
        $to_show = array();
        //stock
            $query[1] = Stock::join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
                ->join('products', 'to_reuses.product_id', '=', 'products.id')
                ->where('stocks.to_date', $requestData['date'])
                ->where('stocks.shift', $requestData['shift'])
                ->where('to_reuses.product_id', $requestData['pd'])
                ->where('stocks.pallet_no', $requestData['pallet_no'])
                ->selectRaw("stocks.to_date, stocks.shift, stocks.pallet_no, to_reuses.product_id, products.name, 
                    sum(stocks.quantity) AS quantity, stocks.note, string_agg(stocks.id, ',') AS sum_id")
                ->groupBy('stocks.to_date', 'stocks.shift', 'stocks.pallet_no', 'to_reuses.product_id', 'products.name', 'stocks.note')
                ->get();

            $exp = explode(',',$query[1][0]['sum_id']);
            // dd($exp);

        //to_use
            $query[2] = FsWhTranDs::whereIn('stock_id', $exp)
                ->selectRaw('fs_wh_tran_m_id, to_date, pallet, sum(volumn) AS volumn')
                ->groupBy('fs_wh_tran_m_id', 'to_date', 'pallet')
                ->get();
            // dd($query[2]);

        //to_reuse
            $query[3] = ToReuse::selectRaw("id, to_date, quantity, note, 
                (SELECT sum(washes.quantity)
                    FROM   washes
                    WHERE washes.to_reuse_id = to_reuses.id AND washes.status = 1) AS wash_qty,
                (SELECT SUM(loses.quantity)
                    FROM   loses
                    WHERE loses.refer_id = to_reuses.id AND loses.step_id = 1) AS lose_qty")                    
                ->whereIn('stock_id', $exp)
                // ->groupBy('dries.to_date', 'dries.shift', 'dries.dry_no', 'dries.round_no', 'dries.note')
                ->get();
            if(count($query[3])>0){
                $arr = array('quantity', 'wash_qty', 'lose_qty'); 
                $arr2 = array('id', 'note'); 
                foreach ($query[3] as $key) {              
                    foreach ($arr as $karr => $varr) {
                        if(empty($to_show['reuse'][$key->to_date][$varr]))
                            $to_show['reuse'][$key->to_date][$varr] = $key->$varr;
                        else
                            $to_show['reuse'][$key->to_date][$varr] += $key->$varr;  
                    }
                    foreach ($arr2 as $karr => $varr) {
                        if(empty($to_show['reuse'][$key->to_date][$varr]))
                            $to_show['reuse'][$key->to_date][$varr] = $key->$varr;
                        else
                            $to_show['reuse'][$key->to_date][$varr] .= ','.$key->$varr;
                    }
                }                
            } 

        //stock_process
            $query[5] = StockProcess::join('stocks', 'stock_processes.stock_id', '=', 'stocks.id')
                ->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
                ->selectRaw("stock_processes.id, stock_processes.to_date, stock_processes.pass, stock_processes.not_pass, stock_processes.note, 
                    (SELECT washes.quantity
                        FROM   to_reuses INNER JOIN
                            washes ON to_reuses.id = washes.to_reuse_id
                        WHERE (to_reuses.stock_id = stock_processes.stock_id) AND (washes.status = 1)) AS wash_qty,
                    (SELECT sum(fs_wh_tran_ds.volumn)
                        FROM   fs_wh_tran_ds
                        WHERE fs_wh_tran_ds.stock_id = stock_processes.stock_id) AS to_use_qty, 
                    (SELECT loses.quantity
                        FROM   to_reuses INNER JOIN
                            loses ON to_reuses.id = loses.refer_id
                        WHERE (to_reuses.stock_id = stock_processes.stock_id) AND (loses.step_id = 1)) AS lose_qty")                    
                ->whereIn('stock_processes.stock_id',$exp)->get();
            // dd($query[5]);
            if(count($query[5])>0){
                $arr = array('pass', 'not_pass', 'wash_qty', 'to_use_qty', 'lose_qty'); 
                $arr2 = array('id', 'note'); 
                foreach ($query[5] as $key) {              
                    foreach ($arr as $karr => $varr) {
                        if(empty($to_show['stp'][$key->to_date][$varr]))
                            $to_show['stp'][$key->to_date][$varr] = $key->$varr;
                        else
                            $to_show['stp'][$key->to_date][$varr] += $key->$varr;  
                    }
                    foreach ($arr2 as $karr => $varr) {
                        if(empty($to_show['stp'][$key->to_date][$varr]))
                            $to_show['stp'][$key->to_date][$varr] = $key->$varr;
                        else
                            $to_show['stp'][$key->to_date][$varr] .= ','.$key->$varr;
                    }
                }                
            }
            // dd($to_show);

            // // $exp = explode(',',$query[1][0]['id']);
            // $query[4] = new ToReuse;
            // $query[4] = $query[4]->where('stock_id', $exp);
            // $query[4] = $query[4]->get();

        //lose        
            $query[4] = Lose::join('lose_types', 'loses.lose_type_id', '=', 'lose_types.id')
                ->select('loses.*','lose_types.name')
                ->whereIn('refer_id',$exp)->where('step_id', 4)->get();
        
        // dd($query);

        // //can't del
        //     $to_del = array();
        //     $query6 = Wash::join('to_reuses', 'washes.to_reuse_id', '=', 'to_reuses.id')
        //         // ->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
        //         ->whereNotNull('to_reuses.stock_id')
        //         ->select('to_reuses.id')
        //         ->get();
        //     if(!empty($query6)){
        //         foreach ($query6 as $key) {
        //             $to_del[$key->id] = 'del';
        //         }
        //     }
        //     // dd($to_del);

        return view('step.stock.detail', compact('query', 'to_show'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $requestData = $request->all();
        $pd_name = Product::orderBy('name')->get();
        $query = Stock::join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
            ->where('stocks.to_date', $requestData['date'])
            ->where('stocks.shift', $requestData['shift'])
            ->where('to_reuses.product_id', $requestData['pd'])
            ->where('stocks.pallet_no', $requestData['pallet_no'])
            ->selectRaw("stocks.to_date,stocks.shift,to_reuses.product_id,stocks.pallet_no,sum(stocks.quantity) AS quantity, 
                stocks.note, STRING_AGG(stocks.id, ',') AS to_id")
            ->groupBy('stocks.to_date','stocks.shift','to_reuses.product_id','stocks.pallet_no','stocks.note')
            ->get();

        $set_num = $this->set_num();
        // $pd = $set_num[0];
        $to_show = $set_num[1];
        // dd($to_show);
        $set_max = 0;
        foreach ($to_show[$requestData['pd']] as $key => $value) {            
            foreach ($to_show[$requestData['pd']][$key] as $kshf => $vshf) {
                if($requestData['shift']=='B'){
                    if($key < $requestData['date']){
                        $set_max += $vshf;
                    }
                }else{
                    if($key <= $requestData['date']){
                        $set_max += $vshf;
                    }
                }       
            }            
        }
        $set_max += $query[0]['quantity'];

        return view('step.stock.edit',compact('pd_name', 'query', 'set_max'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //เก็บข้อมูลเก่า
        $requestData = $request->all();
        $exp = explode(',', $requestData['to_id']);
        $query = array();
        $to_status['status'] = '0';
        $query = Stock::whereIn('id', $exp)
            ->update($to_status);
    
        //เก็บข้อมูลใหม่ ถ้าไม่ใช่ success ต้อง save ข้อมูลเก่าคืน
            $to_save = $this->to_save($requestData);
            $query = Stock::whereIn('id', $exp);
            if($to_save[0]=='success'){
                $query->delete();
                return redirect('stock')->with('success', ' Update!');
            }else{
                $to_status['status'] = '1';
                $query->update($to_status);
                if($to_save[0]=='<'){
                    return redirect()->back()->withInput($request->input())->with('error','จำนวนที่มีจนถึงวันที่เลือก มีน้อยกว่าจำนวนที่จะใช้!');
                }elseif ($to_save[0]=='again') {
                    return redirect()->back()->withInput($request->input())->with('error','รายการสินค้าในวันที่และกะนี้เคยมีการบันทึกแล้ว กรุณาใช้การแก้ไขแทนค่ะ!');
                }elseif ($to_save[0]=='not') {
                    return redirect()->back()->withInput($request->input())->with('error','ไม่พบรายการรับเข้าที่สามารถตัดได้!');
                }
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $requestData = $request->all();

        if($requestData['page']=='stock'){
            $query = Stock::join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
            ->where('stocks.to_date', $requestData['date'])
            ->where('stocks.shift', $requestData['shift'])
            ->where('to_reuses.product_id', $requestData['pd'])
            ->where('stocks.pallet_no', $requestData['pallet_no'])->delete();
        // }elseif($requestData['page']=='dry'){ shine, choose_box
        }else{
            $exp = explode(',',$requestData['id']);           
            $query = Stock::whereIn('id',$exp)->delete();            
        }
        if($query){
            return back()->with('success','Deleted!');
        }else{
            return back()->with('error',"Can't delete!");
        }
    }
    
    public function edit_wait(Request $request)
    {
        $query = $request->all();
        // $bal_wait = $query['bal_wait'];
        $pd_name = Product::where('id', $query['pd'])->get();
        // dd($pd_name[0]['name']);
        // $query = Stock::join('dries', 'stocks.dry_id', '=', 'dries.id')
        //     ->join('washes', 'dries.wash_id', '=', 'washes.id')
        //     ->join('to_reuses', 'washes.to_reuse_id', '=', 'to_reuses.id')
        //     ->where('stocks.to_date', $requestData['date'])
        //     ->where('stocks.shift', $requestData['shift'])
        //     ->where('to_reuses.product_id', $requestData['pd'])
        //     ->where('stocks.pallet_no', $requestData['pallet_no'])
        //     ->selectRaw('stocks.to_date,stocks.shift,to_reuses.product_id,stocks.pallet_no,sum(stocks.quantity) AS quantity,stocks.note')
        //     ->groupBy('stocks.to_date','stocks.shift','to_reuses.product_id','stocks.pallet_no','stocks.note')
        //     ->get();
        return view('step.stock.process',compact('pd_name', 'query'));
    }

    public function update_wait(Request $request)
    {
        $requestData = $request->all();
        //เก็บค่าผลตรวจ เฝื่อต้องออกรายงาน ไม่เช็ค เผื่อวันนั้นจะคีย์มากกว่า 1 ครั้ง
        // $check = StockProcess::join('stocks', 'stock_processes.stock_id', '=', 'stocks.id')
        //     ->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
        //     ->where('stock_processes.to_date', $requestData['to_date'])
        //     ->where('to_reuses.product_id', $requestData['product_id'])
        //     ->count();        
        // if($check==0){
        if(empty($requestData['pass'])) $pass = 0; else $pass = $requestData['pass'];
        if(empty($requestData['not_pass'])) $not_pass = 0; else $not_pass = $requestData['not_pass'];
        $sum_process = $pass + $not_pass;

        if ($sum_process > $requestData['bal_wait']){
            return redirect()->back()->withInput()->with('error', 'ผลตรวจมากเกินกว่าจำนวนที่มี ('.$requestData['bal_wait'].')');
        }else{
            // dd($requestData);

        // if($requestData['bal_wait'] >= $sum_process){
            //ไม่ซ้ำ ทำการหาจำนวนที่นำมาได้
            $reuse = array();
            $case = "(stocks.to_date <= '".$requestData['to_date']."') AND (to_reuses.product_id = ".$requestData['pd'].") 
                AND (stocks.shift = '".$requestData['shift']."') AND (stocks.pallet_no = '".$requestData['pallet_no']."')";
            $query1 = $this->main_query($case);
            // $main_query = $this->main_query($requestData['pd'], $requestData['to_date'], $requestData['to_date'], $requestData['shift'], $requestData['pallet_no']);
            // $query1 = $main_query[0];
            // $to_fs = $main_query[1];
            // dd($query1);
            if(!empty($query1)){          
                // $arr1 = array('quantity', 'pass', 'wait', 'not_pass', 'to_use_qty', 'to_reuse_qty', 'lose_qty');
                // dd($query1);
                foreach ($query1 as $key) {
                    // dd($key->id);
                    // if($key->wait > $key->lose_qty){
                        $reuse['quantity'][$key->id] = $key->wait;                        
                        // $reuse['quantity'][$key->id] = $key->pass - $key->lose_qty - $key->to_use_qty;
                        // $reuse['to_date'][$key->id] = $key->to_date;
                    // }
                }
                if(!empty($reuse)){
                    $to_save = array();
                    $to_save['to_date'] = $requestData['to_date'];
                    // $to_save['pass'] = $requestData['pass'];
                    // $to_save['not_pass'] = $requestData['not_pass'];        
                    $to_save['note'] = $requestData['note'];
                    $to_save['user_id'] = auth()->user()->id;
                    foreach ($reuse['quantity'] as $key => $value) {                        
                        if($value>0){                                                      
                            $to_save['stock_id'] = $key;
                            if($sum_process>0){  
                                //quantity = wait ในรอบนั้น
                                if($sum_process > $value){
                                    $quantity = $value; 
                                }else{
                                    $quantity = $sum_process;
                                } 
                                //ลบ not_pass ให้หมดก่อน  
                                if($not_pass >= $quantity){
                                    $to_pass = 0; 
                                    $to_not_pass = $quantity;  
                                    $not_pass -= $quantity;
                                }else{
                                    // if($pass >= $quantity-$not_pass){
                                        $to_pass = $quantity-$not_pass; 
                                        $to_not_pass = $not_pass;
                                    // }
                                    $not_pass -= $not_pass;
                                    $pass -= $quantity-$not_pass;
                                }                             
                                // $to_save['quantity'] = $quantity;
                                $to_save['pass'] = $to_pass; 
                                $to_save['not_pass'] = $to_not_pass; 
                                StockProcess::create($to_save);   
                                
                                //เก็บค่าลง reuse auto แต่มีเรื่องของ pallet ที่ต้องบันทึก เพื่อให้สินค้าไปรอซักได้เลย
                                if($to_not_pass>0){
                                    $save_reuse = array();
                                    $save_reuse['to_date'] = $requestData['to_date'];
                                    $save_reuse['product_id'] = $requestData['pd'];
                                    $save_reuse['quantity'] = $to_not_pass;
                                    $save_reuse['pallet_no'] = $requestData['pallet_no'];
                                    $save_reuse['note'] = 'ส่งซักซ้ำ';
                                    $save_reuse['user_id'] = auth()->user()->id;
                                    $save_reuse['stock_id'] = $key;
                                    ToReuse::create($save_reuse); 
                                }

                                $to_update = array();
                                $to_query = Stock::findOrFail($key);
                                $to_update['wait'] = $to_query->wait - $quantity;
                                $to_update['pass'] = $to_query->pass + $to_pass;
                                $to_update['not_pass'] = $to_query->not_pass + $to_not_pass;
                                $to_update['user_id'] = auth()->user()->id;
                                $to_query->update($to_update);
                                $sum_process -= $quantity;
                            }
                        }                    
                    } 
                    return redirect('stock')->with('success', ' Added!');
                    // $to_return = 'success';           
                    // }                
                }else{
                    return redirect()->back()
                        ->withInput($request->input())
                        ->with('error','ไม่พบรายการรับเข้าที่สามารถตัดได้!');
                    // $to_return = 'not';
                }
            }
        }
    }

    public function del_wait(Request $request)
    {     
        $requestData = $request->all();   
        // dd($requestData);
        $exp = explode(',',$requestData['id']);
        $query = StockProcess::whereIn('id', $exp)->get();
        foreach ($query as $key) {
            $reuse = ToReuse::where('stock_id', $key->stock_id)->delete(); 

            $stock = Stock::findOrFail($key->stock_id);
            $to_update = array();
            $to_update['wait'] = $stock->wait + $key->pass + $key->not_pass;
            $to_update['pass'] = $stock->pass - $key->pass;
            $to_update['not_pass'] = $stock->not_pass - $key->not_pass;
            $stock->update($to_update);
            
        }                
        $to_del = StockProcess::whereIn('id', $exp)->delete();    
            
        if($to_del){
            return back()->with('success','Deleted!');
        }else{
            return back()->with('error',"Can't delete!");
        }
    }

    public function return(Request $request)    //ไม่ใช้แล้วเพราะถ้าผลเป็นไม่ผ่านให้ไปที่ to_reuse auto
    {
        $requestData = $request->all();
        // $st_id = '';
        $bal_wait = $requestData['bal_wait'];
        $query = Stock::join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
            ->where('stocks.to_date', $requestData['date'])
            ->where('stocks.shift', $requestData['shift'])
            ->where('to_reuses.product_id', $requestData['pd'])
            ->where('stocks.pallet_no', $requestData['pallet_no'])
            ->select('stocks.id', 'stocks.wait')->get();
        foreach ($query as $key) { 
            // $st_id .=  $key->id.',';           
            if($bal_wait>0 && $key->wait>0){
                $to_save = array();
                $to_save['wait'] = 0;
                $to_save['user_id'] = auth()->user()->id;
                Stock::where('id', $key->id)->update($to_save);
                $bal_wait -= $key->wait;

                $to_save = array();
                $to_save['to_date'] = date('Y-m-d'); 
                $to_save['stock_id'] = $key->id; 
                $to_save['not_pass'] = $key->wait; 
                $to_save['note'] = 'ส่งซักใหม่'; 
                $to_save['user_id'] = auth()->user()->id;
                StockProcess::create($to_save);   
                
                $to_save = array();
                $to_save['to_date'] = date('Y-m-d'); 
                $to_save['product_id'] = $requestData['pd']; 
                $to_save['quantity'] = $key->wait; 
                $to_save['pallet_no'] = $requestData['pallet_no'];
                $to_save['stock_id'] = $key->id;
                $to_save['note'] = 'ส่งซักใหม่';
                $to_save['user_id'] = auth()->user()->id;
                ToReuse::create($to_save);             
            }            
        }               

        return back()->with('success','ส่งซักใหม่!');
    }
}
