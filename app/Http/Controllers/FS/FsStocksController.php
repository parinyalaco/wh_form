<?php

namespace App\Http\Controllers\FS;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;

use App\Models\FS\FsWhTranMs;
use App\Models\FS\FsWhTranDs;
use App\Models\FS\FsHwLoc;
use App\Models\FS\FsHwMain;
use App\Models\FS\Product;
use App\Models\FS\FsWhStock;
use App\Models\FS\Stock;
use App\Models\FS\FsStock;
use App\Models\FS\FsStockTrans;

class FsStocksController extends Controller
{
    public function index(Request $request)
    {
        $perPage = 20;
        $query = FsWhTranMs::orderBy('tran_dt','desc')->paginate($perPage);;
        return view('trans_fs.index', compact('query'));
    }

    public function create()
    {
        $transList = array(
            'รับเข้า' => 'รับเข้า',
            'จ่ายออก' => 'จ่ายออก',
        );
        return view('trans_fs.create',compact('transList'));
    }

    public function store(Request $request)
    {
        // dd($request);
        $v = Validator::make($request->all(), [
            'tran_dt' => 'required',
            'tran_type' => 'required',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $requestData = $request->all();
        $requestData['tran_dt'] = date('Y-m-d H:i:s', strtotime($requestData['tran_dt']));
        $requestData['user_id'] = auth()->user()->id;
        FsWhTranMs::create($requestData);

        return redirect('/FsStocks')->with('success', ' Added!');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $transList = array(
            'รับเข้า' => 'รับเข้า',
            'จ่ายออก' => 'จ่ายออก',
        );
        $query = FsWhTranMs::findOrFail($id);
        return view('trans_fs.edit', compact('query', 'transList'));
    }

    public function update(Request $request, $id)
    {
        // dd($request);
        $request->validate([
            'tran_dt' => 'required',
            'tran_type' => 'required',
        ]);

        $requestData = $request->all();
        $requestData['tran_dt'] = date('Y-m-d H:i:s', strtotime($requestData['tran_dt']));
        $requestData['user_id'] = auth()->user()->id;
        $query = FsWhTranMs::findOrFail($id);
        $query->update($requestData);

        return redirect('/FsStocks')->with('success', 'Updated!');
    }

    public function destroy($id)
    {
        $query = FsWhTranMs::where('id', $id)->delete();
        if ($query) {
            return back()->with('success', 'Deleted!');
        } else {
            return back()->with('error', "Can't delete!");
        }
    }

    public function detailList($fs_wh_tran_m_id){
        $fswhList = FsHwMain::pluck('name', 'id');
        $productList = Product::pluck('name', 'id');
        $query = FsWhTranMs::findOrFail($fs_wh_tran_m_id);
        return view('trans_fs.view', compact('query', 'fswhList', 'productList'));
    }

    public function addDetailAction(Request $request, $fs_wh_tran_m_id){
        $requestData = $request->all();

        $query = FsWhTranMs::findOrFail($fs_wh_tran_m_id);
        for ($i=0; $i < $requestData['locnum']; $i++) {
            if(isset($requestData['location'. $i])
                && isset($requestData['locationid' . $i])
                && isset($requestData['productid' . $i])
                && isset($requestData['pallet' . $i])
                && isset($requestData['volumn' . $i])
                && !empty($requestData['location' . $i])
                && !empty($requestData['locationid' . $i])
                && !empty($requestData['productid' . $i])
                && !empty($requestData['pallet' . $i])
                && !empty($requestData['volumn' . $i])
            ){
                $tmpDetail = [];
                $tmpDetail['fs_wh_tran_m_id'] = $fs_wh_tran_m_id;
                $tmpDetail['fs_wh_loc_id'] = $requestData['locationid' . $i];
                $tmpDetail['product_id'] = $requestData['productid' . $i];
                $tmpDetail['pallet'] = $requestData['pallet' . $i];
                if($query->tran_type == 'รับเข้า'){
                    if( $requestData['volumn' . $i] >= 0){
                        $tmpDetail['volumn'] = $requestData['volumn' . $i];
                    }else{
                        $tmpDetail['volumn'] = $requestData['volumn' . $i]*-1;
                    }
                }elseif($query->tran_type == 'จ่ายออก'){
                    if ($requestData['volumn' . $i] <= 0) {
                        $tmpDetail['volumn'] = $requestData['volumn' . $i];
                    } else {
                        $tmpDetail['volumn'] = $requestData['volumn' . $i] * -1;
                    }
                }
                $tmpDetail['desc'] = '';
                $tmpDetail['status'] = 'Active';
                $tmpDetail['user_id'] = auth()->user()->id;

                FsWhTranDs::create($tmpDetail);
            }
        }
        return redirect('/FsStocks')->with('success', ' Detail Added! ');
    }

    public function deleteDetailAction($id)
    {
        //ถ้าใช้ต้องมาดูการลบอีกครั้ง แต่ตอนนี้ไม่ให้ใช้เพราะการเบิกออกต้องไม่มีการยกเลิก
        $query = FsWhTranDs::where('id', $id)->delete();

        if ($query) {
            return back()->with('success', 'Deleted!');
        } else {
            return back()->with('error', "Can't delete!");
        }
    }

    public function searchLoc(Request $request,$id){
        $requestData = $request->all();

        $data = FsHwLoc::where('fs_wh_id', $id)
        ->where('name','like','%'. $requestData['term'].'%')
        ->limit(10)
        ->get();
        $map = [];
        foreach($data as  $obj){
            $map[] = array(
                'name' => $obj->name,
                'id' => $obj->id
            );
        }
        return response()->json(array('data' => $map));
    }

    public function mapfswh($id){
        $data = FsHwMain::findOrFail($id);

        $pd = array();
        $to_show = array();
        $query = Product::orderBy('name')->get();
        foreach ($query as $key) {
            $pd[$key->id] = $key->name;
            $to_show['sum'][$key->id] = 0;
        }

        $query = fsWhStock::join('fs_hw_locs', 'fs_wh_stocks.fs_wh_loc_id', '=', 'fs_hw_locs.id')
            ->where('fs_hw_locs.fs_wh_id', $id)
            ->selectRaw('fs_wh_stocks.fs_wh_loc_id, fs_wh_stocks.product_id, fs_wh_stocks.pallet, SUM(fs_wh_stocks.volumn) AS volumn, fs_wh_stocks.to_date')
            ->groupBy('fs_wh_stocks.fs_wh_loc_id', 'fs_wh_stocks.product_id', 'fs_wh_stocks.pallet', 'fs_wh_stocks.to_date')
            ->orderBy('fs_wh_stocks.to_date', 'DESC')
            ->get();
        foreach ($query as $key) {
            $to_show[$key->fs_wh_loc_id][$key->product_id][$key->pallet][$key->to_date] = $key->volumn;
            $to_show['sum'][$key->product_id] += $key->volumn;
        }
        if(!empty($to_show['sum'])) $to_show['sum'] = array_filter($to_show['sum']);

        return view('trans_fs.mapfswh', compact('data', 'to_show', 'pd'));
    }

    public function pd_pl_now($wh_loc_id){
        $pd = array();
        $query = FsWhStock::where('fs_wh_loc_id', $wh_loc_id)
            ->selectRaw('product_id, pallet, SUM(volumn) AS volumn')
            ->groupBy('product_id', 'pallet')
            ->get();
        if(!empty($query)){
            foreach ($query as $kvol) {
                if(empty($pd[$kvol->product_id][$kvol->pallet]))
                    $pd[$kvol->product_id][$kvol->pallet] = $kvol->volumn;
                else
                    $pd[$kvol->product_id][$kvol->pallet] += $kvol->volumn;
            }
        }

        $old_pl = '';
        $old_pd = '';
        $volumn = 0;
        if (!empty($pd)){
            foreach ($pd as $kpd => $vpd) {
                foreach ($vpd as $kpl => $vpl) {
                    $old_pd = $kpd;
                    $old_pl = $kpl;
                    $volumn = $vpl;
                }
            }
        }

        $query = Product::orderBy('name')->get();
        foreach ($query as $key) {
            $pd[$key->id] = $key->name;
        }

        return [$old_pl, $old_pd, $volumn, $pd];
    }

    public function pd_pl_now_new($wh_loc_id)
    {
        $pd = array();
        $query = FsStock::where('fs_wh_loc_id', $wh_loc_id)
            ->selectRaw('product_id, pallet, SUM(volumn) AS volumn')
            ->groupBy('product_id', 'pallet')
            ->get();
        if (!empty($query)) {
            foreach ($query as $kvol) {
                if (empty($pd[$kvol->product_id][$kvol->pallet]))
                    $pd[$kvol->product_id][$kvol->pallet] = $kvol->volumn;
                else
                    $pd[$kvol->product_id][$kvol->pallet] += $kvol->volumn;
            }
        }

        $old_pl = '';
        $old_pd = '';
        $volumn = 0;
        if (!empty($pd)) {
            foreach ($pd as $kpd => $vpd) {
                foreach ($vpd as $kpl => $vpl) {
                    $old_pd = $kpd;
                    $old_pl = $kpl;
                    $volumn = $vpl;
                }
            }
        }

        $query = Product::orderBy('name')->get();
        foreach ($query as $key) {
            $pd[$key->id] = $key->name;
        }

        return [$old_pl, $old_pd, $volumn, $pd];
    }

    public function addProdToStock($wh_loc_id){

        $data = FsHwLoc::findOrFail($wh_loc_id);

        $pd_pl = $this->pd_pl_now($wh_loc_id);

        $old_pl = $pd_pl[0];
        $old_pd = $pd_pl[1];
        $pd = $pd_pl[3];

        $productList = Product::pluck('name', 'id');

        $to_show = array();
        $query = new Stock;
        $query = $query->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id');
        $query = $query->selectRaw("stocks.id, to_reuses.product_id, stocks.pass,
            (SELECT SUM(fs_wh_tran_ds.volumn)
                FROM   fs_wh_tran_ds
                WHERE (fs_wh_tran_ds.stock_id = stocks.id)) AS to_use_qty");
        $query = $query->where('stocks.status', '1')->get();
        // $query = $query->groupBy('to_reuses.product_id')->get();
        // dd($query);
        if(!empty($query)){
            foreach ($query as $key) {
                if(empty($to_show[$key->product_id]))
                    $to_show[$key->product_id] = $key->pass - $key->to_use_qty;
                else
                    $to_show[$key->product_id] += $key->pass - $key->to_use_qty;
            }
        }

        $to_show = array_filter($to_show);

        $query = FsWhTranDs::join('fs_wh_tran_ms', 'fs_wh_tran_ds.fs_wh_tran_m_id', '=', 'fs_wh_tran_ms.id')
                ->where('fs_wh_tran_ds.fs_wh_loc_id', $wh_loc_id)
                ->where('fs_wh_tran_ms.tran_type', 'รับเข้า')
                ->selectRaw('fs_wh_tran_ms.id, fs_wh_tran_ds.product_id, sum(fs_wh_tran_ds.volumn) AS volumn, fs_wh_tran_ds.pallet, fs_wh_tran_ds.to_date')
                ->groupBy('fs_wh_tran_ms.id', 'fs_wh_tran_ds.product_id', 'fs_wh_tran_ds.pallet', 'fs_wh_tran_ds.to_date')
                ->orderBy('fs_wh_tran_ds.to_date', 'DESC')
                ->get();

        return view('trans_fs.addstock', compact('data', 'productList', 'pd', 'to_show', 'old_pl', 'old_pd', 'query'));
    }

    public function addProdToStockAction(Request $request,$wh_loc_id)
    {
        $fsHwLoc = FsHwLoc::findOrFail($wh_loc_id);
        $requestData = $request->all();
        // dd($requestData);
        if(empty($requestData['productid'])){
            $data = FsHwLoc::findOrFail($wh_loc_id);
            if ($data->fswhtrands()->count() > 0){
                foreach ($data->fswhtrands()->get() as $item){
                    $requestData['pallet'] = $item->pallet;
                    $requestData['productid'] = $item->product_id;
                }
            }
        }
        $tmp = [];
        $tmp['tran_dt'] = date('Y-m-d H:i:s');
        $tmp['tran_type'] = 'รับเข้า';
        // $tmp['desc'] = '';
        // $tmp['user'] = 1;
        $tmp['status'] = 'Active';
        $tmp['user_id'] = auth()->user()->id;
        $FsWhTranMsid = FsWhTranMs::create($tmp)->id;

        $tmpDetail = [];
        $tmpDetail['fs_wh_tran_m_id'] = $FsWhTranMsid;
        $tmpDetail['fs_wh_loc_id'] = $wh_loc_id;
        $tmpDetail['product_id'] = $requestData['productid'];
        $tmpDetail['pallet'] = $requestData['pallet'];
        $tmpDetail['status'] = 'Active';
        $tmpDetail['user_id'] = auth()->user()->id;
        $tmpDetail['to_date'] = $requestData['to_date'];

        // FsWhTranDs = เก็บประวัติรับเข้า, จ่ายออก  FsWhStock = เป็น stock คงเหลือ
        if($requestData['desc']=='tran'){
            $query = new Stock;
            $query = $query->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id');
            $query = $query->selectRaw("stocks.id, to_reuses.product_id, stocks.pass,
                (SELECT SUM(fs_wh_tran_ds.volumn)
                    FROM   fs_wh_tran_ds
                    WHERE (fs_wh_tran_ds.stock_id = stocks.id)) AS fs_qty");
            $query = $query->where('stocks.to_date', '<=', date('Y-m-d'));
            $query = $query->where('to_reuses.product_id', $requestData['productid']);
            $query = $query->orderBy('stocks.to_date')->orderBy('stocks.shift')
                ->get();
            // dd($query);
            if(!empty($query)){
                foreach ($query as $key) {
                    $reuse['quantity'][$key->id] = $key->pass - $key->fs_qty;
                }
                // dd($reuse);
                $to_fs = $requestData['volumn'];
                foreach ($reuse['quantity'] as $key => $value) {
                    // echo 'to_wash->'.$to_wash.'</br>';
                    if($to_fs>0){
                        if($reuse['quantity'][$key]>0){
                            $tmpDetail['stock_id'] = $key;
                            // echo $to_wash.' > '.$reuse['quantity'][$key].'</br>';
                            if($to_fs > $reuse['quantity'][$key]){
                                $tmpDetail['volumn'] = $reuse['quantity'][$key];
                            }else{
                                $tmpDetail['volumn'] = $to_fs;
                            }
                            FsWhTranDs::create($tmpDetail);
                            FsWhStock::create($tmpDetail);
                            $to_fs -= $tmpDetail['volumn'];
                        }
                    }
                }
            }
        }else{
            $tmpDetail['volumn'] = $requestData['volumn'];
            FsWhTranDs::create($tmpDetail);
            FsWhStock::create($tmpDetail);
        }

        // return redirect('/FsStocks/mapfswh/'. $fsHwLoc->fs_wh_id)->with('success', ' Detail Added! ');
        return back()->with('success', 'Added! ');
    }

    public function exportFromStock($id){

        // $data = FsWhStock::findOrFail($id);
        // $fsHwLoc = FsHwLoc::findOrFail($data->fs_wh_loc_id);
        // return view('trans_fs.exportstock', compact('data', 'fsHwLoc'));

        // SELECT fs_wh_tran_ds.product_id, fs_wh_tran_ds.volumn, fs_wh_tran_ds.pallet, fs_wh_tran_ds.to_date, fs_wh_tran_ds.updated_at
        // FROM   fs_wh_tran_ds INNER JOIN
        //      fs_wh_tran_ms ON fs_wh_tran_ds.fs_wh_tran_m_id = fs_wh_tran_ms.id
        // WHERE (fs_wh_tran_ds.fs_wh_loc_id = 2598) AND (fs_wh_tran_ms.tran_type = N'รับเข้า')
        // ORDER BY fs_wh_tran_ds.to_date, fs_wh_tran_ds.updated_at DESC

        // SELECT fs_wh_tran_ds.product_id, SUM(fs_wh_tran_ds.volumn) AS volumn, fs_wh_tran_ds.pallet
        // FROM   fs_wh_tran_ds INNER JOIN
        //      fs_wh_tran_ms ON fs_wh_tran_ds.fs_wh_tran_m_id = fs_wh_tran_ms.id
        // WHERE (fs_wh_tran_ds.fs_wh_loc_id = 2598) AND (fs_wh_tran_ms.tran_type = N'รับเข้า')
        // GROUP BY fs_wh_tran_ds.product_id, fs_wh_tran_ds.pallet

        $data = FsHwLoc::findOrFail($id);
        $pd_pl = $this->pd_pl_now($id);

        $query = FsWhTranDs::join('fs_wh_tran_ms', 'fs_wh_tran_ds.fs_wh_tran_m_id', '=', 'fs_wh_tran_ms.id')
                ->where('fs_wh_tran_ds.fs_wh_loc_id', $id)
                ->where('fs_wh_tran_ms.tran_type', 'จ่ายออก')
                ->selectRaw('fs_wh_tran_ms.id, fs_wh_tran_ds.product_id, sum(fs_wh_tran_ds.volumn) AS volumn, fs_wh_tran_ds.pallet, fs_wh_tran_ds.to_date')
                ->groupBy('fs_wh_tran_ms.id', 'fs_wh_tran_ds.product_id', 'fs_wh_tran_ds.pallet', 'fs_wh_tran_ds.to_date', 'fs_wh_tran_ds.to_date')
                ->orderBy('fs_wh_tran_ds.to_date', 'DESC')
                ->get();
        // $old_pl = $pd_pl[0];
        // $old_pd = $pd_pl[1];
        // $volumn = $pd_pl[2];
        // $pd = $pd_pl[3];


        return view('trans_fs.exportstock', compact('data', 'pd_pl', 'query'));
    }

    public function exportFromStockAction(Request $request,$id){
        $requestData = $request->all();

        //dd($requestData);

        $fsloc = FsHwLoc::findOrFail($id);

        $tmp = [];
        $tmp['tran_dt'] = date('Y-m-d H:i:s');
        $tmp['tran_type'] = 'จ่ายออก';
        $tmp['desc'] = $requestData['desc'];
        $tmp['status'] = 'Active';
        $FsWhTranMsid = FsWhTranMs::create($tmp)->id;

        $tmpDetail = array();
        $tmpDetail['fs_wh_tran_m_id'] = $FsWhTranMsid;
        $tmpDetail['fs_wh_loc_id'] = $id;
        $tmpDetail['product_id'] = $requestData['old_pd'];
        $tmpDetail['pallet'] = $requestData['old_pl'];
        $tmpDetail['volumn'] = $requestData['volumn'];
        $tmpDetail['status'] = 'Active';
        $tmpDetail['user_id'] = auth()->user()->id;
        $tmpDetail['to_date'] = $requestData['to_date'];
        FsWhTranDs::create($tmpDetail);

        $st = $requestData['volumn'];
        $query = fsWhStock::where('fs_wh_loc_id', $id)
            ->select('id', 'volumn')
            ->orderBy('to_date')->orderBy('updated_at')
            ->get();
        foreach ($query as $key) {
            if($st>0){
                if($key->volumn > $st){
                    $to_update = array();
                    $to_update['volumn'] = $key->volumn - $st;
                    fsWhStock::where('id', $key->id)->update($to_update);
                    $st = 0;
                }else{
                    fsWhStock::where('id', $key->id)->delete();
                    $st -= $key->volumn;
                }
            }

        }

        $count_st = fsWhStock::where('fs_wh_loc_id', $id)->count();
        // dd($count_st);
        if($count_st>0){
            return back()->with('success', 'Updated! ');
        }else{
            return redirect('/FsStocks/mapfswh/' . $fsloc->fs_wh_id)->with('success', ' Detail Updated! ');
        }
    }

    public function viewstock($id)
    {
        $data = FsHwMain::findOrFail($id);

        $query = FsStock::join('fs_hw_locs', 'fs_stocks.fs_wh_loc_id', '=', 'fs_hw_locs.id')
            ->join('products', 'fs_stocks.product_id', '=', 'products.id')
            ->where('fs_hw_locs.fs_wh_id', $id)
            ->selectRaw('products.name, SUM(fs_stocks.volumn) AS volumn')
            ->groupBy('products.name')
            ->get();

        return view('trans_fs.viewstock', compact('data', 'query'));
    }

    public function recvToStock($wh_loc_id)
    {

        $data = FsHwLoc::findOrFail($wh_loc_id);

        $pd_pl = $this->pd_pl_now_new($wh_loc_id);

        $old_pl = $pd_pl[0];
        $old_pd = $pd_pl[1];
        $pd = $pd_pl[3];

        $productList = Product::pluck('name', 'id');

        $to_show = array();
        $query = new Stock;
        $query = $query->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id');
        $query = $query->selectRaw("stocks.id, to_reuses.product_id, stocks.pass,
            (SELECT SUM(fs_wh_tran_ds.volumn)
                FROM   fs_wh_tran_ds
                WHERE (fs_wh_tran_ds.stock_id = stocks.id)) AS to_use_qty");
        $query = $query->where('stocks.status', '1')->get();
        // $query = $query->groupBy('to_reuses.product_id')->get();
        // dd($query);
        if (!empty($query)) {
            foreach ($query as $key) {
                if (empty($to_show[$key->product_id]))
                    $to_show[$key->product_id] = $key->pass - $key->to_use_qty;
                else
                    $to_show[$key->product_id] += $key->pass - $key->to_use_qty;
            }
        }

        $to_show = array_filter($to_show);

        $query = [];

        return view('trans_fs.recvstock', compact('data', 'productList', 'pd', 'to_show', 'old_pl', 'old_pd', 'query'));
    }

    public function recvToStockAction(Request $request, $wh_loc_id)
    {
        $data = FsHwLoc::findOrFail($wh_loc_id);
        $requestData = $request->all();
        //dd($requestData);
        //บันทีก transaction
        $tmpFsStockTrans = [];
        $tmpFsStockTrans['fs_wh_loc_id'] = $wh_loc_id;
        if(empty($requestData['productid'])){
            $tmpFsStockTrans['product_id'] = $requestData['old_pd'];
        }else{
            $tmpFsStockTrans['product_id'] = $requestData['productid'];
        }
        if (empty($requestData['pallet'])) {
            $tmpFsStockTrans['pallet'] = $requestData['old_pl'];
        } else {
            $tmpFsStockTrans['pallet'] = $requestData['pallet'];
        }
        $tmpFsStockTrans['volumn'] = $requestData['volumn'];
        $tmpFsStockTrans['desc'] = $requestData['desc'];
        $tmpFsStockTrans['user_id'] = auth()->user()->id;
        $tmpFsStockTrans['to_date'] = $requestData['to_date'];

        $tmpFsStockTrans = FsStockTrans::create($tmpFsStockTrans);
        //คำนวน current stock
        $currentFsStocks = FsStock::where('fs_wh_loc_id', $wh_loc_id)->get();
        if($currentFsStocks->count() > 0){
            foreach ($currentFsStocks as $currentFsStock) {
                if($currentFsStock->product_id == $tmpFsStockTrans['product_id']){
                    $currentFsStock->volumn = $currentFsStock->volumn + $requestData['volumn'];
                    $currentFsStock->user_id = auth()->user()->id;
                    $currentFsStock->update();
                }
            }
        }else{
            $tmpFsStock = [];
            $tmpFsStock['fs_wh_loc_id'] = $wh_loc_id;
            $tmpFsStock['product_id'] = $tmpFsStockTrans['product_id'];
            $tmpFsStock['pallet'] = $requestData['pallet'];
            $tmpFsStock['volumn'] = $requestData['volumn'];
            $tmpFsStock['desc'] = $requestData['desc'];
            $tmpFsStock['user_id'] = auth()->user()->id;
            $tmpFsStock['to_date'] = $requestData['to_date'];

            $tmpFsStock = FsStock::create($tmpFsStock);
        }


        return redirect('/FsStocks/viewstock/'. $data->fs_wh_id)->with('success', ' Detail Added! ');
        //return back()->with('success', 'Added! ');
    }

    public function deliverFromStock($id)
    {

        $data = FsHwLoc::findOrFail($id);
        $pd_pl = $this->pd_pl_now_new($id);

        $query = FsStockTrans::where('fs_wh_loc_id', $id)->get();

        return view('trans_fs.deliverstock', compact('data', 'pd_pl', 'query'));
    }

    public function deliverFromStockAction(Request $request, $id)
    {

        $fsStock = FsStock::where('fs_wh_loc_id', $id)->get();
        //dd($fsStock);
        $wh_loc_id = $id;

        $data = FsHwLoc::findOrFail($wh_loc_id);
        $fsid = $data->fs_wh_id;
        $requestData = $request->all();
        //บันทีก transaction
        $tmpFsStockTrans = [];
        $tmpFsStockTrans['fs_wh_loc_id'] = $wh_loc_id;
        if (empty($requestData['productid'])) {
            $tmpFsStockTrans['product_id'] = $requestData['old_pd'];
        } else {
            $tmpFsStockTrans['product_id'] = $requestData['productid'];
        }
        if (empty($requestData['pallet'])) {
            $tmpFsStockTrans['pallet'] = $requestData['old_pl'];
        } else {
            $tmpFsStockTrans['pallet'] = $requestData['pallet'];
        }
        $tmpFsStockTrans['volumn'] = -1*$requestData['volumn'];
        $tmpFsStockTrans['desc'] = $requestData['desc'];
        $tmpFsStockTrans['user_id'] = auth()->user()->id;
        $tmpFsStockTrans['to_date'] = $requestData['to_date'];

        $tmpFsStockTrans = FsStockTrans::create($tmpFsStockTrans);
        //คำนวน current stock
        $currentFsStocks = FsStock::where('fs_wh_loc_id', $wh_loc_id)->get();
        if ($currentFsStocks->count() > 0) {
            foreach ($currentFsStocks as $currentFsStock) {
                if ($currentFsStock->product_id == $tmpFsStockTrans['product_id']) {

                    $currentFsStock->volumn = $currentFsStock->volumn + $tmpFsStockTrans['volumn'];
                    $currentFsStock->user_id = auth()->user()->id;

                    if($currentFsStock->volumn == 0){
                        $currentFsStock->delete();
                    }else{
                        $currentFsStock->update();
                    }
                }
            }
        }


        $count_st = fsStock::where('fs_wh_loc_id', $id)->count();
        // dd($count_st);
        if ($count_st > 0) {
            return back()->with('success', 'Updated! ');
        } else {
            return redirect('/FsStocks/viewstock/' . $fsid)->with('success', ' Detail Updated! ');
        }
    }

    public function changelocation($id,$fs_wh_id)
    {

        $dataLoc = FsHwLoc::findOrFail($id);
        $pd_pl = $this->pd_pl_now_new($id);

       // return view('trans_fs.deliverstock', compact('data', 'pd_pl', 'query'));


        $data = FsHwMain::findOrFail($fs_wh_id);

        $query = FsStock::join('fs_hw_locs', 'fs_stocks.fs_wh_loc_id', '=', 'fs_hw_locs.id')
        ->join('products', 'fs_stocks.product_id', '=', 'products.id')
        ->where('fs_hw_locs.fs_wh_id', $fs_wh_id)
        ->selectRaw('products.name, SUM(fs_stocks.volumn) AS volumn')
        ->groupBy('products.name')
        ->get();

        return view('trans_fs.viewchange', compact('data', 'query', 'dataLoc', 'pd_pl'));
    }

    public function changelocationconfirm($id,$fs_wh_id,$new_loc)
    {
        $dataLocFrom = FsHwLoc::findOrFail($id);
        $dataLocTo = FsHwLoc::findOrFail($new_loc);

        foreach($dataLocFrom->fsstocks as $fromcurrent){
            $tmpFromFsStockTrans = [];
            $tmpFromFsStockTrans['fs_wh_loc_id'] = $fromcurrent->fs_wh_loc_id;
            $tmpFromFsStockTrans['product_id'] = $fromcurrent->product_id;
            $tmpFromFsStockTrans['pallet'] = $fromcurrent->pallet;
            $tmpFromFsStockTrans['volumn'] = -1 * $fromcurrent->volumn;
            $tmpFromFsStockTrans['desc'] = "Move from ". $dataLocFrom->name . " TO ". $dataLocTo->name;
            $tmpFromFsStockTrans['user_id'] = auth()->user()->id;
            $tmpFromFsStockTrans['to_date'] = date("Y-m-d");

            $tmpFromFsStockTrans = FsStockTrans::create($tmpFromFsStockTrans);

            $tmpToFsStockTrans = [];
            $tmpToFsStockTrans['fs_wh_loc_id'] = $new_loc;
            $tmpToFsStockTrans['product_id'] = $fromcurrent->product_id;
            $tmpToFsStockTrans['pallet'] = $fromcurrent->pallet;
            $tmpToFsStockTrans['volumn'] = $fromcurrent->volumn;
            $tmpToFsStockTrans['desc'] = "Move from " . $dataLocFrom->name . " TO " . $dataLocTo->name;
            $tmpToFsStockTrans['user_id'] = auth()->user()->id;
            $tmpToFsStockTrans['to_date'] = date("Y-m-d");

            $tmpToFsStockTrans = FsStockTrans::create($tmpToFsStockTrans);

            $tmpFsStock = [];
            $tmpFsStock['fs_wh_loc_id'] = $new_loc;
            $tmpFsStock['product_id'] = $fromcurrent->product_id;
            $tmpFsStock['pallet'] = $fromcurrent->pallet;
            $tmpFsStock['volumn'] = $fromcurrent->volumn;
            $tmpFsStock['desc'] = "Move from " . $dataLocFrom->name . " TO " . $dataLocTo->name;
            $tmpFsStock['user_id'] = auth()->user()->id;
            $tmpFsStock['to_date'] =  date("Y-m-d");

            $tmpFsStock = FsStock::create($tmpFsStock);

            $fromcurrent->delete();

        }

        return redirect('/FsStocks/viewstock/' . $fs_wh_id)->with('success', ' Detail Updated! ');
    }
}
