<?php

namespace App\Http\Controllers\FS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\FS\ToReuse;
use App\Models\FS\Product;
use App\Models\FS\Step;
use App\Models\FS\LoseType;
use App\Models\FS\Lose;
use App\Models\FS\Wash;
use App\Models\FS\ChooseBox;
use App\Models\FS\Stock;
use App\Models\FS\StockProcess;
use App\Models\FS\Dry;
use App\Models\FS\ToUse;

class ToReuseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
        $set_pd = $request->get('set_pd');
        if($request->get('st_search') != ""){
            $st_search = $request->get('st_search');
            if(empty($request->get('ed_search')))    $ed_search = date('Y-m-d');
            else    $ed_search = $request->get('ed_search');
        }else{
            $to_query = array();
            $query = ToReuse::groupBy('to_date')->orderByDesc('to_date')->selectRaw('TOP (5) to_date')->get();
            if(count($query)>0){
                foreach($query as $key){
                    $to_query[] = $key->to_date;               
                }                
                for($i=4; $i>=0; $i--){
                    if(!empty($to_query[$i])){         
                        $st_search = $to_query[$i];
                        break;
                    }
                }              
                // $st_search = $to_query[4];
                $ed_search =$to_query[0];
                
                $request->replace(['st_search' => $st_search, 'ed_search' => $ed_search]);
            }
        }
        $pd = array();
        $query = Product::all();
        foreach ($query as $key) {
            $pd['name'][$key->id] = $key->name;
            $pd['wash'][$key->id] = $key->status_wash;
            $pd['chk'][$key->id] = $key->status_check;
        }

        $case = '';
        if (!empty($set_pd)) {     
            $case .= "(to_reuses.product_id = ".$set_pd.")";
            if (!empty($st_search)) $case .= " AND ";
        }
        if (!empty($st_search)) {
            $case .= "(to_reuses.to_date Between '".$st_search."' AND '".$ed_search."')";
        } 
        $query1 = $this->main_query($case);
        // dd($query1);
        $to_show = array();
        if(!empty($query1)){
            $arr = array('quantity', 'wash_qty', 'choose_box_qty', 'lose_qty', 'enable_chk');
            foreach ($query1 as $key) {
                foreach ($arr as $karr=>$varr) {
                    if(empty($to_show[$key->to_date][$key->product_id][$varr]))
                        $to_show[$key->to_date][$key->product_id][$varr] = $key->$varr;
                    else
                        $to_show[$key->to_date][$key->product_id][$varr] += $key->$varr;
                }
            }  
        }

        // $to_stock = $this->set_num();
        // $pd = $set_num[0];
        // $to_stock = $set_num[1];
        // dd($to_stock);
        
        return view('step.reuse.index', compact('pd','to_show'));
    }

    public function main_query($where)
    {
        // dd($where);
        $query1 = new ToReuse;
        $query1 = $query1->selectRaw('to_reuses.id, to_reuses.to_date, to_reuses.product_id, to_reuses.pallet_no, to_reuses.note, to_reuses.stock_id, 
            SUM(to_reuses.quantity) AS quantity,
            (SELECT SUM(washes.quantity)
                FROM   washes
                WHERE washes.to_reuse_id = to_reuses.id) AS wash_qty, 
            (SELECT SUM(choose_boxes.quantity)
                FROM   choose_boxes
                WHERE (choose_boxes.to_reuse_id = to_reuses.id) AND (choose_boxes.status = 1)) AS choose_box_qty,
            (SELECT SUM(loses.quantity)
                FROM   loses
                WHERE loses.refer_id = to_reuses.id AND loses.step_id = 1) AS lose_qty, 
            (SELECT SUM(loses.quantity)
                FROM   loses
                WHERE loses.refer_id = to_reuses.id AND loses.step_id = 1 AND loses.lose_type_id = 8) AS enable_chk');    
        if(!empty($where))  $query1 = $query1->whereRaw($where);
        $query1 = $query1->groupBy('to_reuses.id', 'to_reuses.to_date', 'to_reuses.product_id', 'to_reuses.pallet_no', 'to_reuses.note', 'to_reuses.stock_id')
            ->orderBy('to_reuses.to_date', 'DESC')
            ->get();
        // dd($query1);

        return $query1;
    }

    public function main_query2($where) //main->tbl stock ใช้กับ set_num จำนวนตั้งต้น
    {
        // $pd = array();
        // // $pd_set_num = array();
        // $query = Product::orderBy('name')->get();
        // foreach ($query as $key) {
        //     $pd[$key->id] = $key->name;            
        //     // $pd_set_num[] = $key->id;
        // }

        $query = new Stock;
        $query = $query->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id');
        $query = $query->selectRaw('stocks.id, stocks.to_date, stocks.shift, to_reuses.product_id, stocks.pallet_no, stocks.note, 
            stocks.not_pass,
            (SELECT SUM(to_reuses.quantity)
                FROM   to_reuses
                WHERE (to_reuses.stock_id = stocks.id)) AS stock_qty'); 
        // if(!empty($where))  $query = $query->whereRaw($where);         
        $query = $query->where('stocks.not_pass', '>', 0);
        // $query = $query->groupBy('to_reuses.id', 'to_reuses.to_date', 'to_reuses.product_id', 'to_reuses.pallet_no', 'to_reuses.note')
        $query = $query->orderBy('stocks.to_date')->get();

        return $query;
    }

    public function set_num()
    {
        $case = '';
        $query1 = $this->main_query2($case);
        // $query1 = $main_query2[0];
        // $pd = $main_query2[1];
        // dd($query1);
        foreach ($query1 as $key) {
            if(!empty($to_show[$key->product_id][$key->to_date][$key->shift]))
                $to_show[$key->product_id][$key->to_date][$key->shift] += $key->not_pass - $key->stock_qty; 
            else
                $to_show[$key->product_id][$key->to_date][$key->shift] = $key->not_pass - $key->stock_qty;     
        }
        // dd($to_show);
        $to_show = array_map('array_filter', $to_show);
        $to_show = array_filter($to_show);
        // dd($to_show);
        // $query_pd = Product::where('status_wash', 1)->orderBy('name')->get();
        // foreach($query_pd  as $key){
        //     $pd[$key->id] = $key->name;
        // }
        // dd($pd);
        return $to_show;
    }

    public function set_toend(Request $request)
    {
        $requestData = $request->all();
        // dd($requestData);  
        $query = Step::where('name','Reuse')->get();
        $step_id = $query[0]['id'];

        $query = LoseType::where('name','auto')->get();
        $loset_id = $query[0]['id']; 

        if($request['chk'] == 'check'){
            $to_save = array();
            $to_save['to_date'] = date('Y-m-d');
            $to_save['step_id'] = $step_id;
            $to_save['lose_type_id'] = $loset_id;
            $to_save['note'] = 'ปรับเป็นใช้หมดแล้ว';     
               

            $case = '';
            if($request['page']=='index'){         
                $case = "(to_reuses.to_date <= '".$requestData['date']."') AND (to_reuses.product_id = ".$requestData['pd_id'].")";                
            }else{
                $case = "(to_reuses.id = ".$requestData['id'].")";
            } 
            $query = $this->main_query($case);
            // dd($query);
            foreach ($query as $key) {
                $arr = array('lose_qty','wash_qty','stock_qty');
                foreach($arr as $karr=>$varr){
                    if(!empty($key->$varr))  $set[$varr] = $key->$varr;
                    else    $set[$varr] = 0;
                }
                $check = $key->quantity - $set['lose_qty'] - $set['wash_qty'] - $set['stock_qty'];
                if($check > 0){
                    $to_save['refer_id'] = $key->id;
                    $to_save['quantity'] = $check;
                    Lose::create($to_save); 
                }           
            }
            $tex = "ปรับเป็น 'ใช้หมด' แล้ว";
        }else{
            $query = new Lose;
            $query = $query->leftJoin('to_reuses', 'loses.refer_id', '=', 'to_reuses.id');
            $query = $query->where('step_id',$step_id)->where('lose_type_id', $loset_id);
            if($request['page']=='index'){         
                $query = $query->where('to_reuses.to_date', '<=', $requestData['date'])
                    ->where('to_reuses.product_id', $requestData['pd_id']);               
            }else{
                $query = $query->where('refer_id', $requestData['id']);
            } 
            $query = $query->delete();
            $tex = "ปรับเป็น 'คงเหลือ' แล้ว";
        } 

        return $tex;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // dd($request);
        $query = $request->all();
        $pd = Product::orderBy('name')->get();
        // dd($query);
        return view('step.reuse.create',compact('query','pd'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $requestData = $request->all();  
        $requestData['user_id'] = auth()->user()->id;
        // dd($requestData);
        $chk = array();
        ToReuse::create($requestData);
        if($requestData['page']=='index')
            return redirect('to_reuse')->with('success', ' Added!');
        else
            return redirect()->route('to_reuse.show_det',['date'=>$requestData['to_date'],'pd_id'=>$requestData['product_id']])->with('success', ' Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($date,$pd_id)
    {
        $sum_lot = 0;
        $pd = Product::findOrFail($pd_id);  

        $to_show = array();
        $case = "(to_reuses.to_date = '".$date."') AND (to_reuses.product_id = ".$pd_id.")";  
        $query = $this->main_query($case);
        $arr = array('to_date', 'product_id', 'pallet_no', 'note', 'quantity','wash_qty', 'choose_box_qty', 'lose_qty', 'enable_chk', 'stock_id');
        foreach ($query as $key) {
            foreach ($arr as $karr => $varr) {
                $to_show[$key->id][$varr] = $key->$varr;                 
            }
            $sum_lot += $key->quantity;            
        }

        return view('step.reuse.show', compact('to_show', 'date', 'pd', 'sum_lot')); 
    }

    public function detail($id)
    {
        $to_show = array();
        //reuse
            $query[1] = ToReuse::join('products', 'to_reuses.product_id', '=', 'products.id')
                // ->select('to_reuses.to_date', 'products.name', 'to_reuses.quantity', 'to_reuses.pallet_no', 'to_reuses.note')
                ->select('to_reuses.*', 'products.name')
                ->where('to_reuses.id', $id)->get();

        //wash
            $query[2] = Wash::selectRaw("id, to_date, shift, quantity, note,                 
                (SELECT sum(dries.quantity)
                    FROM   dries
                    WHERE dries.wash_id = washes.id AND dries.status = 1) AS dry_qty,      
                (SELECT sum(shines.quantity)
                        FROM   shines
                        WHERE shines.wash_id = washes.id AND shines.status = 1) AS shine_qty,
                (SELECT SUM(loses.quantity)
                    FROM   loses
                    WHERE loses.refer_id  = washes.id AND loses.step_id = 2) AS lose_qty")
                // ->groupBy('id', 'to_date', 'shift', 'note')
                ->where('to_reuse_id',$id)->get();  
            // dd($query[2]);
            if(count($query[2])>0){
                $arr = array('quantity', 'dry_qty', 'shine_qty', 'lose_qty');
                $arr2 = array('id', 'note');
                foreach ($query[2] as $key) {
                    foreach ($arr as $karr => $varr) {                    
                        if(empty($to_show['wash'][$key->to_date][$key->shift][$varr]))
                            $to_show['wash'][$key->to_date][$key->shift][$varr] = $key->$varr;
                        else
                            $to_show['wash'][$key->to_date][$key->shift][$varr] += $key->$varr;  
                    }
                    foreach ($arr2 as $karr => $varr) {
                        if(empty($to_show['wash'][$key->to_date][$key->shift][$varr]))
                            $to_show['wash'][$key->to_date][$key->shift][$varr] = $key->$varr;
                        else
                            $to_show['wash'][$key->to_date][$key->shift][$varr] .= ','.$key->$varr;
                    }
                }                
            }

        //lose        
            $query[3] = Lose::join('lose_types', 'loses.lose_type_id', '=', 'lose_types.id')
                ->select('loses.*','lose_types.name')
                ->where('refer_id',$id)->where('step_id', 1)->get();

        //choose_Box
            $query[4] = ChooseBox::selectRaw("id, to_date, shift, quantity, note, 
                (SELECT sum(stocks.quantity)
                    FROM   stocks
                    WHERE stocks.choose_box_id = choose_boxes.id AND stocks.status = 1) AS stock_qty,
                (SELECT SUM(loses.quantity)
                    FROM   loses
                    WHERE loses.refer_id = choose_boxes.id AND loses.step_id = 6) AS lose_qty")
                // ->groupBy('id','to_date', 'shift', 'note')
                ->where('to_reuse_id',$id)->get();
            if(count($query[4])>0){
                $arr = array('quantity', 'stock_qty', 'lose_qty');                
                $arr2 = array('id', 'note');
                foreach ($query[4] as $key) {
                    foreach ($arr as $karr => $varr) {
                        if(empty($to_show['choose_box'][$key->to_date][$key->shift][$varr]))
                            $to_show['choose_box'][$key->to_date][$key->shift][$varr] = $key->$varr;
                        else
                            $to_show['choose_box'][$key->to_date][$key->shift][$varr] += $key->$varr;   
                    }
                    foreach ($arr2 as $karr => $varr) {     
                        if(empty($to_show['choose_box'][$key->to_date][$key->shift][$varr]))
                            $to_show['choose_box'][$key->to_date][$key->shift][$varr] = $key->$varr;
                        else
                            $to_show['choose_box'][$key->to_date][$key->shift][$varr] .= ','.$key->$varr;               
                    }
                }  
            }

        // dd($query[2]);
        return view('step.reuse.detail', compact('query','to_show'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query = ToReuse::join('stocks', 'to_reuses.id', '=', 'stocks.to_reuse_id', 'left outer')
            ->selectRaw('to_reuses.id, to_reuses.to_date, to_reuses.product_id,  
                to_reuses.quantity, to_reuses.pallet_no, to_reuses.is_end, to_reuses.note') 
                // ,to_uses.to_date AS use_date
            ->where('to_reuses.id',$id)->get();
        $pd = Product::orderBy('name')->get();
        // dd($query['0']['id']);
        return view('step.reuse.edit', compact('query', 'pd'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $query = ToReuse::findOrFail($id);
        // dd($query);
        $requestData = $request->all();        
        $requestData['user_id'] = auth()->user()->id;
        if(empty($requestData['is_end']))  $requestData['is_end'] = '0';
        // dd($requestData);
        $query->update($requestData);

        return redirect()->route('to_reuse.show_det',['date'=>$requestData['to_date'],'pd_id'=>$requestData['product_id']])->with('success', 'Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $qtu = ToReuse::findOrFail($id);
        // dd($qtu);
        // $get_url = url()->previous();   
        // $exp = explode('stock',$get_url);        
        // if(count($exp)>1){                        
        //     $qst = Stock::findOrFail($qtu->stock_id);

        //     //คืนค่า
        //     $to_save = array();
        //     $to_save['wait'] = $qst->wait + $qtu->quantity;
        //     $qst->update($to_save);

        //     StockProcess::where('to_date', $qtu->to_date)->where('stock_id', $qtu->stock_id)->where('not_pass', $qtu->quantity)->where('note', 'ส่งซักใหม่')->delete();         
        // }
        $qtu->delete();
        if($qtu){
            return back()->with('success','Deleted!');
        }else{
            return back()->with('error',"Can't delete!");
        }  
    }

}
