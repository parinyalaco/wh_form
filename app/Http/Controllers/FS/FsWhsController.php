<?php

namespace App\Http\Controllers\FS;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Fs\FsHwMain;
use App\Models\Fs\FsHwLoc;
use App\Models\Fs\Product;

class FsWhsController extends Controller
{
    public function index(){
        $query = FsHwMain::orderBy('name')->get();
        $count_tran = array();
        $query1 = FsHwLoc::join('fs_wh_tran_ds', 'fs_hw_locs.id', '=', 'fs_wh_tran_ds.fs_wh_loc_id')
            ->selectRaw('fs_hw_locs.fs_wh_id, COUNT(fs_wh_tran_ds.id) AS count_tran')
            ->groupBy('fs_hw_locs.fs_wh_id')->get(); 
        // dd($query1); 
        foreach ($query1 as $key) {
            $count_tran[$key->fs_wh_id] = $key->count_tran;
        }    
        // dd($count_tran); 
        return view('base_data.fswh.index', compact('query', 'count_tran'));
    }

    public function create(){
        return view('base_data.fswh.create');
    }

    public function store(Request $request){
        // dd($request);
        $v = Validator::make($request->all(), [
            'name' => 'required|unique:fs_hw_mains|max:255',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $requestData = $request->all();
        $requestData['user_id'] = auth()->user()->id;
        FsHwMain::create($requestData);

        return redirect('setting/FsWhs')->with('success', ' Added!');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $query = FsHwMain::findOrFail($id);
        return view('base_data.fswh.edit', compact('query'));
    }

    public function update(Request $request, $id)
    {
        // dd($request);
        $request->validate([
            'name' => 'required|unique:fs_hw_mains,name,' . $id,
        ]);

        $requestData = $request->all();
        $requestData['user_id'] = auth()->user()->id;
        $query = FsHwMain::findOrFail($id);
        $query->update($requestData);

        return redirect('setting/FsWhs')->with('success', 'Updated!');
    }

    public function destroy($id)
    {
        $query1 = FsHwLoc::where('fs_wh_id', $id)->delete();
        $query = FsHwMain::where('id', $id)->delete();
        if ($query && $query1) {
            return back()->with('success', 'Deleted!');
        } else {
            return back()->with('error', "Can't delete!");
        }
    }

    private function _num2alpha($n)
    {
        $r = '';
        for ($i = 1; $n >= 0 && $i < 10; $i++) {
            $r = chr(0x41 + ($n % pow(26, $i) / pow(26, $i - 1))) . $r;
            $n -= pow(26, $i);
        }
        return $r;
    }

    public function autoGenLoc($fs_wh_id){
        $query = FsHwMain::findOrFail($fs_wh_id);

        for($rowloop = 1;$rowloop <= $query->row;$rowloop++){
            $dataAutogen = [];
            for ($colloop = 1; $colloop <= $query->col; $colloop++) {
                for ($levelloop = 1; $levelloop <= $query->level; $levelloop++) {
                    $tmpLoc = [];
                    $tmpLoc['fs_wh_id'] = $fs_wh_id;
                    $tmpLoc['name'] = $this->_num2alpha($rowloop-1)."-".$colloop."-".  $levelloop;
                    $tmpLoc['desc'] = "";
                    $tmpLoc['row'] = $rowloop;
                    $tmpLoc['col'] = $colloop;
                    $tmpLoc['level'] = $levelloop;
                    $tmpLoc['product_id'] = 0;
                    $tmpLoc['status'] = "Active";

                    $dataAutogen[] = $tmpLoc;

                }
            }
            FsHwLoc::insert($dataAutogen);
        }
        // /dd($dataAutogen);

        return redirect('setting/FsWhs')->with('success', 'Auto Generate Complete!');
    }

    public function detailList($fs_wh_id, Request $request){
        $perPage = 20;
        $dataFsWh = FsHwMain::findOrFail($fs_wh_id);
        $keyword = $request->get('search');
        if(!empty($keyword)){
            $dataFsWhLocs = FsHwLoc::where('fs_wh_id', $fs_wh_id)->where('name','like','%'.$keyword.'%')->paginate($perPage);
        }else{
            $dataFsWhLocs = FsHwLoc::where('fs_wh_id', $fs_wh_id)->paginate($perPage);
        }
        return view('base_data.fswh.detail_list',compact('dataFsWh','dataFsWhLocs'));
    }

    public function editWhLoc($fs_wh_loc_id){
        $query = FsHwLoc::findOrFail($fs_wh_loc_id);
        $productList = Product::pluck('name', 'id');
        return view('base_data.fswh.edit_detail', compact('query', 'productList'));
    }

    public function editWhLocAction($fs_wh_loc_id, Request $request)
    {
        $request->validate([
            'name' => 'required|unique:fs_hw_locs,name,' . $fs_wh_loc_id,
        ]);

        $requestData = $request->all();
        $requestData['user_id'] = auth()->user()->id;
        
        $query = FsHwLoc::findOrFail($fs_wh_loc_id);
        $query->update($requestData);

        return redirect('setting/FsWhs/detailList/'.$query->fs_wh_id)->with('success', 'Updated!');
    }
}
