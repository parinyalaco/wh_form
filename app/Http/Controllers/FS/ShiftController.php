<?php

namespace App\Http\Controllers\FS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\FS\Shift;

class ShiftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Shift::orderBy('name')->get();
        return view('base_data.shift.index', compact('query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('base_data.shift.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $v = Validator::make($request->all(), [
            'name' => 'required|unique:shifts|max:255',
        ]);
    
        if ($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }

        $requestData = $request->all();
        Shift::create($requestData);

        return redirect('base_data/shift')->with('success', ' Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query = Shift::findOrFail($id);
        return view('base_data.shift.edit', compact('query'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:shifts,name,'.$id,
        ]);

        $requestData = $request->all();
        $query = Shift::findOrFail($id);
        $query->update($requestData);

        return redirect('base_data/shift')->with('success', 'Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = Shift::where('id',$id)->delete();
        if($query){
            return back()->with('success','Deleted!');
        }else{
            return back()->with('error',"Can't delete!");
        }
    }
}
