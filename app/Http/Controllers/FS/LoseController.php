<?php

namespace App\Http\Controllers\FS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\FS\Lose;
use App\Models\FS\Step;
use App\Models\FS\LoseType;
use App\Models\FS\ToReuse;
use App\Models\FS\Wash;
use App\Models\FS\Stock;
use App\Models\FS\Dry;
use App\Models\FS\Shine;
use App\Models\FS\ChooseBox;
use App\Models\FS\ToUse;

use App\Http\Controllers\Controller\ToReuseController;

class LoseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($step, $id)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $query = $request->all();
        // dd($query['page']);
        $lt = LoseType::orderBy('name')->where('id', '<>', 8)->get();
        return view('step.lose.create', compact('query','lt'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        // dd($requestData);
        if($requestData['quantity'] > $requestData['balance']){
            return redirect()->back()->withInput($request->input())->with('error','จำนวนที่ระบุมากเกินกว่าจำนวนที่ตัดได้!');
        }
        if($requestData['page']=='index' || $requestData['page']=='show'){
            $step_name = 'Reuse';
        }else{
            $step_name = $requestData['page'];
        }
        
        $query = Step::where('name',$step_name)->get();
        // dd($query[0]['id']);
        $step_id = $query[0]['id'];
        // dd($step_id);
        $to_save = array();
        $to_save['to_date'] = date('Y-m-d');
        $to_save['step_id'] = $step_id;
        $to_save['lose_type_id'] = $requestData['lose_type_id'];
        $to_save['note'] = $requestData['note']; 
        $to_save['user_id'] = auth()->user()->id;
        
        if($step_name=='Reuse'){
            //reuse
                $query = new ToReuse;
                $query = $query->selectRaw('id, quantity,
                    (SELECT SUM(washes.quantity)
                        FROM   washes
                        WHERE washes.to_reuse_id = to_reuses.id) AS wash_qty, 
                    (SELECT SUM(choose_boxes.quantity)
                        FROM   choose_boxes
                        WHERE (choose_boxes.to_reuse_id = to_reuses.id) AND (choose_boxes.status = 1)) AS choose_box_qty,
                    (SELECT SUM(loses.quantity)
                        FROM   loses
                        WHERE loses.refer_id = to_reuses.id AND loses.step_id = 1) AS lose_qty');    
                if($requestData['page']=='index'){         
                    $query = $query->where('to_date', $requestData['date'])
                        ->where('product_id', $requestData['pd_id']);                
                }else{
                    $query = $query->where('id', $requestData['id']);
                } 
                // $query = $query->groupBy('id')
                $query = $query->orderBy('to_date')
                    ->get();

                // if($requestData['page']=='index'){          
                //     $case = "(to_date = ".$requestData['date'].") AND (product_id = ".$requestData['pd_id'].")";             
                // }else{
                //     $case = "(id = ".$requestData['id'].")";
                // }  
                // $query = new ToReuseController();
                // $query->main_query($case);


            //     $query = new ToReuse;
            //     $query = $query->selectRaw('id, quantity');
            //     if($requestData['page']=='index'){         
            //         $query = $query->where('to_date', $requestData['date'])
            //             ->where('product_id', $requestData['pd_id']);                
            //     }else{
            //         $query = $query->where('id', $requestData['id']);
            //     } 
            //     $query = $query->orderBy('to_date')->get();

            // //wash
            //     $to_show = array();
            //     $query2 = Wash::selectRaw('to_reuse_id, sum(quantity) AS wash_qty')
            //         ->groupBy('to_reuse_id')
            //         ->get();
            //     if(!empty($query2)){
            //         foreach ($query2 as $key) {
            //             $to_show[$key->to_reuse_id]['wash_qty'] = $key->wash_qty;
            //         }
            //     }            

            // //stock
            //     $query4 = Stock::selectRaw('to_reuse_id, sum(quantity) AS stock_qty')
            //         ->whereNull('dry_id')
            //         ->groupBy('to_reuse_id')
            //         ->get();
            //     if(!empty($query4)){
            //         foreach ($query4 as $key) {
            //             $to_show[$key->to_reuse_id]['stock_qty'] = $key->stock_qty;
            //         } 
            //     } 
        }else if($step_name=='wash'){

            $query = new Wash;
            $query = $query->join('to_reuses', 'washes.to_reuse_id', '=', 'to_reuses.id');        
            $query = $query->selectRaw('washes.id, washes.quantity,
                (SELECT sum(dries.quantity)
                    FROM   dries
                    WHERE dries.wash_id = washes.id AND dries.status = 1) AS dry_qty,
                (SELECT SUM(shines.quantity)
                    FROM   shines
                    WHERE shines.wash_id = washes.id) AS shine_qty, 
                (SELECT SUM(loses.quantity)
                    FROM   loses
                    WHERE loses.refer_id = washes.id AND loses.step_id = 2) AS lose_qty');
            $query = $query->where('washes.status', '1')  
                ->where('washes.to_date', $requestData['date'])
                ->where('to_reuses.product_id', $requestData['pd_id'])
                ->where('washes.shift', $requestData['shift']);    
            // $query = $query->groupBy('washes.id')
            $query = $query->orderBy('washes.to_date')->orderBy('washes.shift')
                ->get();

            // $case = "(washes.to_date <= ".$requestData['date'].") AND (to_reuses.product_id = ".$requestData['pd_id'].") 
            //     AND (washes.shift = ".$requestData['shift'].")";
            // $query = new ToReuseController();
            // $query->main_query($case);

            // //wash
            //     $query = new Wash;
            //     $query = $query->join('to_reuses', 'washes.to_reuse_id', '=', 'to_reuses.id');
            //     $query = $query->selectRaw('washes.id, washes.quantity');        
            //     $query = $query->where('washes.to_date', '<=', $requestData['date'])
            //         ->where('to_reuses.product_id', $requestData['pd_id'])
            //         ->where('washes.shift', $requestData['shift']);
            //     $query = $query->orderBy('washes.to_date')->get();

            // //dry
            //     $to_show = array();
            //     $query2 = Dry::selectRaw('wash_id, sum(quantity) AS dry_qty')
            //         ->groupBy('wash_id')->get();
            //     if(!empty($query2)){
            //         foreach ($query2 as $key) {
            //             $to_show[$key->wash_id]['dry_qty'] = $key->dry_qty;
            //         }
            //     }            

        }else if($step_name=='dry'){
            
            $query = new Dry;
            $query = $query->join('washes', 'dries.wash_id', '=', 'washes.id')
                ->join('to_reuses', 'washes.to_reuse_id', '=', 'to_reuses.id');        
            $query = $query->selectRaw('dries.id, dries.quantity,
                (SELECT sum(stocks.quantity)
                    FROM   stocks
                    WHERE stocks.dry_id = dries.id AND stocks.status = 1) AS stock_qty,
                (SELECT SUM(loses.quantity)
                    FROM   loses
                    WHERE loses.refer_id = dries.id AND loses.step_id = 3) AS lose_qty');
            $query = $query->where('dries.status', '1');  
            $query = $query->where('dries.to_date', $requestData['date'])
                ->where('to_reuses.product_id', $requestData['pd_id'])
                ->where('dries.shift', $requestData['shift'])
                ->where('dries.dry_no', $requestData['dry_no'])
                ->where('dries.round_no', $requestData['round_no']);  
            // $query = $query->groupBy('dries.id')
            $query = $query->orderBy('dries.to_date')->orderBy('dries.shift')
                ->get();


            // //dry
            //     $query = new Dry;
            //     $query = $query->join('washes', 'dries.wash_id', '=', 'washes.id');
            //     $query = $query->join('to_reuses', 'washes.to_reuse_id', '=', 'to_reuses.id');
            //     $query = $query->selectRaw('dries.id, dries.quantity');        
            //     $query = $query->where('dries.to_date', '<=', $requestData['date'])
            //         ->where('to_reuses.product_id', $requestData['pd_id'])
            //         ->where('dries.shift', $requestData['shift'])
            //         ->where('dries.dry_no', $requestData['dry_no'])
            //         ->where('dries.round_no', $requestData['round_no']);
            //     $query = $query->orderBy('dries.to_date')->get();         

            // //stock
            //     $query4 = Stock::selectRaw('dry_id, sum(quantity) AS stock_qty')
            //         ->groupBy('dry_id')
            //         ->get();
            //     if(!empty($query4)){
            //         foreach ($query4 as $key) {
            //             $to_show[$key->dry_id]['stock_qty'] = $key->stock_qty;
            //         } 
            //     } 
        }else if($step_name=='stock'){
            $query = new Stock;
            $query = $query->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id');        
            $query = $query->selectRaw('stocks.id, (stocks.wait + stocks.pass) AS quantity, stocks.wait, stocks.pass, 
                (SELECT sum(to_uses.quantity)
                    FROM   to_uses
                    WHERE to_uses.stock_id = stocks.id AND to_uses.status = 1) AS to_use_qty,
                (SELECT sum(to_reuses.quantity)
                    FROM   to_reuses
                    WHERE to_reuses.stock_id = stocks.id) AS to_reuse_qty,
                (SELECT SUM(loses.quantity)
                    FROM   loses
                    WHERE loses.refer_id = stocks.id AND loses.step_id = 4) AS lose_qty');
            $query = $query->where('stocks.status', '1');  
            $query = $query->where('stocks.to_date', $requestData['date'])
                ->where('to_reuses.product_id', $requestData['pd_id'])
                ->where('stocks.shift', $requestData['shift'])
                ->where('stocks.pallet_no', $requestData['pallet_no']); 
            // $query = $query->groupBy('stocks.id')
            $query = $query->orderBy('stocks.to_date')->orderBy('stocks.shift')
                ->get();

            // // dd($query);

        }else if($step_name=='use'){
            //to_use
                $query = new ToUse;
                $query = $query->join('stocks', 'to_uses.stock_id', '=', 'stocks.id');
                $query = $query->join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id');
                $query = $query->selectRaw('to_uses.id, to_uses.quantity,
                (SELECT SUM(loses.quantity)
                    FROM   loses
                    WHERE loses.refer_id = to_uses.id AND loses.step_id = 5) AS lose_qty');        
                $query = $query->where('to_uses.to_date', $requestData['date'])
                    ->where('to_reuses.product_id', $requestData['pd_id'])
                    ->where('to_uses.pallet_no', $requestData['pallet_no']);
                $query = $query->orderBy('to_uses.to_date')->get();

        }else if($step_name=='choose_box'){

            $query = new ChooseBox;
            $query = $query->join('to_reuses', 'choose_boxes.to_reuse_id', '=', 'to_reuses.id');        
            $query = $query->selectRaw('choose_boxes.id, choose_boxes.quantity,
                (SELECT sum(stocks.quantity)
                    FROM   stocks
                    WHERE stocks.choose_box_id = choose_boxes.id AND stocks.status = 1) AS stock_qty,
                (SELECT SUM(loses.quantity)
                    FROM   loses
                    WHERE loses.refer_id = choose_boxes.id AND loses.step_id = 6) AS lose_qty');
            $query = $query->where('choose_boxes.status', '1') 
                ->where('choose_boxes.to_date', $requestData['date'])
                ->where('to_reuses.product_id', $requestData['pd_id'])
                ->where('choose_boxes.shift', $requestData['shift']);    
            // $query = $query->groupBy('choose_boxes.id')
            $query = $query->orderBy('choose_boxes.to_date')->orderBy('choose_boxes.shift')
                ->get();

       
        }else if($step_name=='shine'){

            $query = new Shine;
            $query = $query::join('washes', 'shines.wash_id', '=', 'washes.id')
                ->join('to_reuses', 'washes.to_reuse_id', '=', 'to_reuses.id');        
            $query = $query->selectRaw('shines.id, shines.quantity,            
                (SELECT SUM(stocks.quantity)
                    FROM   stocks
                    WHERE stocks.shine_id = shines.id) AS stock_qty, 
                (SELECT SUM(loses.quantity)
                    FROM   loses
                    WHERE loses.refer_id = shines.id AND loses.step_id = 7) AS lose_qty');
            $query = $query->where('shines.status', '1')  
                ->where('shines.to_date', $requestData['date'])
                ->where('to_reuses.product_id', $requestData['pd_id'])
                ->where('shines.shift', $requestData['shift']);     
            // $query = $query->groupBy('shines.id')
            $query = $query->orderBy('shines.to_date')->orderBy('shines.shift')
                ->get();
           
        }

        //lose        
            $query3 = Lose::selectRaw('refer_id, sum(quantity) AS lose_qty')
                ->where('loses.step_id', $step_id)
                ->groupBy('refer_id')
                ->get();
            if(!empty($query3)){
                foreach ($query3 as $key) {
                    $to_show[$key->refer_id]['lose_qty'] = $key->lose_qty;
                }
            }
        // dd($to_show);

        $loop_qty = $requestData['quantity'];
        // dd($query);
        foreach ($query as $key) {
            if($loop_qty>0){
                $arr = array('lose_qty','wash_qty','stock_qty','dry_qty','to_use_qty','shine_qty','choose_box_qty');
                foreach($arr as $karr=>$varr){
                    // if(!empty($to_show[$key->id][$varr]))  $set[$varr] = $to_show[$key->id][$varr];
                    if(!empty($key->$varr))  $set[$varr] = $key->$varr;
                    else    $set[$varr] = 0;
                }
                // echo 'quan->'.$key->quantity.', lose_qty->'.$set['lose_qty'].', wash_qty->'.$set['wash_qty'].', stock_qty->'.$set['stock_qty'].', dry_qty->'.$set['dry_qty'].', to_use_qty->'.$set['to_use_qty'].'</br>';
                $check = $key->quantity - $set['lose_qty'] - $set['wash_qty'] - $set['stock_qty'] - $set['dry_qty'] - $set['to_use_qty'] - $set['shine_qty'] - $set['choose_box_qty'];            
                // echo 'check->'.$check.'</br>';
                // dd($check);
                if($check > 0){
                    $to_save['refer_id'] = $key->id;
                    if($loop_qty > $check){
                        $lose = $check;                               
                    }else{
                        $lose = $loop_qty;
                    }
                    
                    
                    if($step_name=='stock'){
                        $to_save['lose_stock'] = $requestData['type'];
                        // $stock = Stock::findOrFail($key->id);
                        $to_update = array();
                        // dd($key->$requestData['type']);
                        if($requestData['type']=='wait'){
                            if($key->wait > $lose){
                                $diff_type = $lose;
                            }else{
                                $diff_type = $key->wait;
                            }
                            $to_update['wait'] = $key->wait - $diff_type;
                        }else{
                            if($key->pass > $lose){
                                $diff_type = $lose;
                            }else{
                                $diff_type = $key->pass;
                            }
                            $to_update['pass'] = $key->pass - $diff_type;
                        }
                        // if($key->pass > $lose)   $to_update['pass'] = $key->pass - $lose;
                        $to_update['user_id'] = auth()->user()->id;
                        Stock::where('id',$key->id)->update($to_update);
                    }

                    
                    $to_save['quantity'] = $lose; 
                    // dd($to_save);
                    Lose::create($to_save); 


                    $loop_qty -= $to_save['quantity'];
                }  
            // print_r($to_save); 
            }        
        }
        // dd('chk');
        if($step_name=='Reuse'){
            if($requestData['page']=='index')
                return redirect('to_reuse')->with('success', ' Added!');
            else
                return redirect()->route('to_reuse.show_det',['date'=>$requestData['date'],'pd_id'=>$requestData['pd_id']])->with('success', ' Added!');
        // }elseif($step_name=='wash'){
        //     return redirect('wash')->with('success', ' Added!');
        // }elseif($step_name=='dry'){
        //     return redirect('dry')->with('success', ' Added!');
        // }elseif($step_name=='stock'){
        //     return redirect('stock')->with('success', ' Added!');
        }elseif($step_name=='use'){
            return redirect('to_use')->with('success', ' Added!');
        // }elseif($step_name=='chose_box'){
        //     return redirect('chose_box')->with('success', ' Added!');
        // }elseif($step_name=='shine'){
        //     return redirect('shine')->with('success', ' Added!');
        }else{
            return redirect($step_name)->with('success', ' Added!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = Lose::findOrFail($id);
        $get_url = url()->previous();   
        $exp = explode('stock',$get_url);
        if(count($exp)>1){
            // $stock = Stock::findOrFail($query->refer_id);
            // SELECT stocks.wait, stocks.pass, products.status_wash
            // FROM   stocks INNER JOIN
            //  to_reuses ON stocks.to_reuse_id = to_reuses.id INNER JOIN
            //  products ON to_reuses.product_id = products.id
            // WHERE (stocks.id = 1)
            $stock = Stock::join('to_reuses', 'stocks.to_reuse_id', '=', 'to_reuses.id')
                ->join('products', 'to_reuses.product_id', '=', 'products.id')
                ->select('stocks.wait', 'stocks.pass', 'products.status_wash')
                ->where('stocks.id', $query->refer_id)->get();
            if($query->lose_stock=='wait'){
                $to_save['wait'] = $stock[0]['wait'] + $query->quantity;
                Stock::where('id', $query->refer_id)->update($to_save);
            }elseif($query->lose_stock=='pass'){
                $to_save['pass'] = $stock[0]['pass'] + $query->quantity;
                Stock::where('id', $query->refer_id)->update($to_save);
            }
                
        }
        $query->delete();
        if($query){
            return back()->with('success','Deleted!');
        }else{
            return back()->with('error',"Can't delete!");
        }
    }
}
