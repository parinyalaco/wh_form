<?php

namespace App\Http\Controllers\FS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\FS\LoseType;
use App\Models\FS\Lose;

class LoseTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = LoseType::orderBy('name')->get();
        $chk_ed = array();
        $query1 = Lose::select('lose_type_id')->groupBy('lose_type_id')->get();
        foreach ($query1 as $key) {
            $chk_ed[$key->lose_type_id] = $key->lose_type_id;
        }
        return view('base_data.lose_type.index', compact('query', 'chk_ed'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('base_data.lose_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required|unique:lose_types|max:255',
        ]);
    
        if ($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }

        $requestData = $request->all();
        LoseType::create($requestData);

        return redirect('base_data/lose_type')->with('success', ' Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query = LoseType::findOrFail($id);
        return view('base_data.lose_type.edit', compact('query'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:lose_types,name,'.$id,
        ]);

        $requestData = $request->all();
        $query = LoseType::findOrFail($id);
        $query->update($requestData);

        return redirect('base_data/lose_type')->with('success', 'Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = LoseType::where('id',$id)->delete();
        if($query){
            return back()->with('success','Deleted!');
        }else{
            return back()->with('error',"Can't delete!");
        }
    }
}
