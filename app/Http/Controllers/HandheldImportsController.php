<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use SimpleXLSX;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;
use App\Models\HandheldImportData;
use DB;
use Auth;

class HandheldImportsController extends Controller
{
    public function importData()
    {
        return view('handhelds.import');
    }

    public function importDataAction(Request $request)
    {
        $requestData = $request->all();
        $completedirectory = config('myconfig.directory.handheld.completes');

        if ($request->hasFile('file_sap')) {
            $tmpfolder = date('Ymd');
            if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
            }

            $zipfile = $request->file('file_sap');
            $uploadname = $zipfile->getClientOriginalName();
            $name = md5($zipfile->getClientOriginalName() . time()) . '-upload.' . $zipfile->getClientOriginalExtension();
            $destinationPath = $completedirectory . "/" . $tmpfolder;
            $zipfile->move($destinationPath, $name);

            $uploadpath = $completedirectory . "/" . $tmpfolder  . "/" . $name;
            $uploadfile = $destinationPath  . "/" . $name;

            if ($xlsx = SimpleXLSX::parse($uploadfile)) {
                foreach ($xlsx->rows() as $r => $row) {
                    if ($r > 1) {
                        $tmpImportHHData = [];

                        $tmpImportHHData['mat_desc'] = $row[0];
                        $tmpImportHHData['mat'] = $row[1];
                        $tmpImportHHData['movement_type_txt'] = $row[2];
                        $tmpImportHHData['posting_date'] = date('Y-m-d',strtotime($row[3]));
                        $tmpImportHHData['batch'] = $row[4];
                        $tmpImportHHData['entry_date'] = date('Y-m-d H:i:s', strtotime(substr($row[5], 0, 9)." ". substr($row[6],11,8)));
                        $tmpImportHHData['mat_doc'] = $row[7];
                        $tmpImportHHData['movement_type'] = $row[8];
                        $tmpImportHHData['storage_loc'] = $row[9];
                        $tmpImportHHData['mat_doc_item'] = $row[10];
                        $tmpImportHHData['qty_in_unit'] = $row[11];
                        $tmpImportHHData['unit_entry'] = $row[12];
                        $tmpImportHHData['amount_in_lc'] = $row[13];
                        $tmpImportHHData['ref'] = $row[14];
                        $tmpImportHHData['order'] = $row[15];
                        $tmpImportHHData['username'] = $row[16];
                        $tmpImportHHData['item_auto'] = $row[17];
                        $tmpImportHHData['process_type'] = $row[18];
                        $tmpImportHHData['process_status'] = $row[19];
                        $tmpImportHHData['filename'] = $uploadname;
                        $tmpImportHHData['status'] = "New";

                        HandheldImportData::create($tmpImportHHData);
                    }
                }
            }
        }
    }

    private function processData(){

    }

    public function index(){

        $report = DB::table('handheld_import_data')
            ->selectRaw('posting_date')
            ->groupBy('posting_date')
            ->orderBy('posting_date','DESC')
            ->get();

        return view('handhelds.index',compact('report'));

    }

    public function view($id)
    {
        $report = HandheldImportData::where('id',$id)->get();
    }

    public function viewByDate($date)
    {
        $report = HandheldImportData::where('posting_date', $date)->get();
    }

    public function viewByDateBetween($fdate, $tdate)
    {
        $report = HandheldImportData::where('posting_date','>=', $fdate)->where('posting_date', '<=', $tdate)->get();
    }
}
