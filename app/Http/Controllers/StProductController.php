<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\StProduct;
use App\Models\StWhBegin;
use App\Models\StReceive;

class StProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    
    public function index()
    {
        $st_pd = StProduct::all();
        return view('st_wh.st_product.index',compact('st_pd'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('st_wh.st_product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|unique:st_products|max:255',
        ],
        [
            'name.required'=>"กรุณาระบุข้อมูลสินค้าด้วยค่ะ",
            'name.unique'=>"ชื่อสินค้าไม่สามารถซ้ำได้",
            'name.max'=>"ระบุข้อมูลเกิน 255 ตัวอักษร",
        ]);

        $requestData = $request->all();
        StProduct::create($requestData);

        return redirect('/st_wh/st_product')->with('success', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $st_pd = StProduct::findOrFail($id);
        return view('st_wh.st_product.edit', compact('st_pd'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $chk_pd = StProduct::where('name',$request->name)->whereNotIn('id',[$id])->count();
        // dd($chk_pd);
        if($chk_pd>0){
            return redirect()->back()->with('error', 'ไม่สามารถระบุสินค้าซ้ำกับที่มีอยู่ได้!');
        }else{
            $requestData = $request->all();

            $st_pd = StProduct::findOrFail($id);
            $st_stock = StWhBegin::where('st_product_id',$id)->count();
            if($st_stock>0){
                $st_status['status'] = 0;
                $st_pd->update($st_status);

                StProduct::create($requestData);
            }else{
                $st_pd->update($requestData);
            }

            return redirect('/st_wh/st_product')->with('success', ' updated!');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $st_pd = StProduct::findOrFail($id);
        $st_stock = StWhBegin::where('st_product_id',$id)->count();
        if($st_stock>0){
            $st_status['status'] = 0;
            $st_pd->update($st_status);
        }else{
            $st_pd->delete();
        }
        // StProduct::destroy($id);

        return redirect('/st_wh/st_product')->with('success', ' deleted!');
    }

    // public function receive($id)
    // {
    //     $st_rc = array();
    //     $sum_rc = array();        
    //     $st_rc = StReceive::where('st_product_id', $id)->orderBy('receive_date', 'DESC')->get();
    //     if(count($st_rc)>0){
    //         foreach ($st_rc as $key) {
    //             $sum_wh[$key->name]['pd'] = $key->st_product_id;
    //             $sum_wh[$key->name]['st'] = $key->amount;
    //             $sum_wh[$key->name]['rc'] = StReceive::where('st_product_id', $key->st_product_id)->where('status', 1)->where('receive_date', '>=', $key->start_date)->sum('amount');
    //             $sum_wh[$key->name]['wd'] = StWithdraw::where('st_product_id', $key->st_product_id)->where('status', 1)->where('withdraw_date', '>=', $key->start_date)->sum('amount');
    //             $sum_wh[$key->name]['rt'] = StReturn::where('st_product_id', $key->st_product_id)->where('status', 1)->where('return_date', '>=', $key->start_date)->sum('amount');
    //             $sum_wh[$key->name]['ch'] = StChange::where('st_product_id', $key->st_product_id)->where('status', 1)->where('change_date', '>=', $key->start_date)->sum('amount');
    //         }
    //         foreach ($sum_wh as $key=>$value) {
    //             $sum_wh[$key]['tt'] = $sum_wh[$key]['st'] + $sum_wh[$key]['rc'] + $sum_wh[$key]['rt'] - $sum_wh[$key]['wd'] - $sum_wh[$key]['ch'];
    //         }
    //     }
        
    //     return view('st_wh.main.index',compact('sum_wh','sum_bk'));
    // }
}
