<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\StProduct;
use App\Models\StWhBegin;
use App\Models\StReceive;
use App\Models\StChange;
use App\Models\RmBroker;
use App\Models\StBroker;
use App\Models\StWithdraw;
use App\Models\StReturn;
use App\Models\Crop;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use DateTime;
// use Illuminate\Support\Facades\Mail;
// use Illuminate\Support\Facades\File;

class StWhithdrawController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    
    public function stock(Request $request)
    {     
        $requestData = $request->all();

        $bk_id = array();
        $pd_id = array();           
        $st_bk = array();
        $sum_bk = array();

        $st_bk = new StBroker;
        $st_bk = $st_bk->join('rm_brokers', 'st_brokers.rm_broker_id', '=', 'rm_brokers.id');
        $st_bk = $st_bk->join('st_products', 'st_brokers.st_product_id', '=', 'st_products.id');            
        $st_bk = $st_bk->where('st_brokers.status',1);
        if(!empty($requestData['bk_id'])){
            $bk_id = $requestData['bk_id'];
            $st_bk = $st_bk->where('st_brokers.rm_broker_id',$bk_id);
        }
        if(!empty($requestData['pd_id'])){
            $pd_id = $requestData['pd_id'];
            $st_bk = $st_bk->where('st_brokers.st_product_id',$pd_id);
        }
        $st_bk = $st_bk->select('st_brokers.id', 'st_brokers.rm_broker_id', 'rm_brokers.name as bk_name', 'st_brokers.st_product_id', 'st_products.name as pd_name', 'st_brokers.amount', 'st_brokers.start_date');
        $st_bk = $st_bk->orderBy('rm_brokers.name')->orderBy('st_products.name')->get();

        if(count($st_bk)>0){
            foreach ($st_bk as $key) {
                $sum_bk[$key->bk_name][$key->pd_name]['id'] = $key->id;
                $sum_bk[$key->bk_name][$key->pd_name]['bk'] = $key->rm_broker_id;
                $sum_bk[$key->bk_name][$key->pd_name]['pd'] = $key->st_product_id;
                $sum_bk[$key->bk_name][$key->pd_name]['wd'] = StWithdraw::where('rm_broker_id', $key->rm_broker_id)->where('st_product_id', $key->st_product_id)->where('status', 1)->where('withdraw_date', '>=', $key->start_date)->sum('amount');
                $sum_bk[$key->bk_name][$key->pd_name]['rt'] = StReturn::where('rm_broker_id', $key->rm_broker_id)->where('st_product_id', $key->st_product_id)->where('status', 1)->where('return_date', '>=', $key->start_date)->sum('amount');
                
                $sum_bk[$key->bk_name][$key->pd_name]['st'] = $key->amount;

            }
            foreach ($sum_bk as $kbk=>$vbk) {
                foreach ($vbk as $kpd=>$vpd) {
                    $sum_bk[$kbk][$kpd]['tt'] = $sum_bk[$kbk][$kpd]['st'] + $sum_bk[$kbk][$kpd]['wd'] - $sum_bk[$kbk][$kpd]['rt'];
                }
            }
        }
        $st_bk_rt = array();
        $sum_rt = array();
        $st_bk_rt = new StReturn;        
        $st_bk_rt = $st_bk_rt->join('rm_brokers', 'st_returns.rm_broker_id', '=', 'rm_brokers.id');
        $st_bk_rt = $st_bk_rt->join('st_products', 'st_returns.st_product_id', '=', 'st_products.id');    
        $st_bk_rt = $st_bk_rt->where('st_returns.status', 1);
        if(!empty($bk_id))  $st_bk_rt = $st_bk_rt->where('st_returns.rm_broker_id',$bk_id);
        if(!empty($pd_id))  $st_bk_rt = $st_bk_rt->where('st_returns.st_product_id',$pd_id);
        $st_bk_rt = $st_bk_rt->selectRaw('st_returns.rm_broker_id, rm_brokers.name as bk_name, st_returns.st_product_id, st_products.name as pd_name, sum(amount) as amount');
        $st_bk_rt = $st_bk_rt->groupBy('st_returns.rm_broker_id', 'rm_brokers.name', 'st_returns.st_product_id', 'st_products.name')->get();
        foreach ($st_bk_rt as $key) {
            if(empty($sum_bk[$key->bk_name][$key->pd_name]['rt'])){
                $sum_rt[$key->bk_name][$key->pd_name]['rt'] = $key->amount;
                $sum_rt[$key->bk_name][$key->pd_name]['bk'] = $key->rm_broker_id;
                $sum_rt[$key->bk_name][$key->pd_name]['pd'] = $key->st_product_id;
            } 

        }
        
        $bk = StBroker::join('rm_brokers', 'st_brokers.rm_broker_id', '=', 'rm_brokers.id')
            ->where('st_brokers.status',1)
            ->select('st_brokers.rm_broker_id AS id', 'rm_brokers.name')
            ->orderBy('rm_brokers.name')
            ->groupBy('st_brokers.rm_broker_id', 'rm_brokers.name')
            ->get();            
        $pd = StBroker::join('st_products', 'st_brokers.st_product_id', '=', 'st_products.id')
            ->where('st_brokers.status',1)
            ->select('st_brokers.st_product_id AS id', 'st_products.name')
            ->orderBy('st_products.name')
            ->groupBy('st_brokers.st_product_id', 'st_products.name')
            ->get();          
        
        $crop = Crop::orderBy('id','DESC')->get();

        return view('st_wh.main.broker',compact('sum_bk','bk','pd','bk_id','pd_id','sum_rt','crop'));
    }
    
    public function show_bk($id)
    {
        $st_bk = StBroker::join('st_products', 'st_brokers.st_product_id', '=', 'st_products.id')
            ->join('rm_brokers', 'st_brokers.rm_broker_id', '=', 'rm_brokers.id')
            ->where('st_brokers.id', $id)->where('st_brokers.status',1)
            ->select('st_brokers.id', 'st_brokers.rm_broker_id', 'rm_brokers.name AS bk_name', 'st_brokers.st_product_id', 'st_products.name AS pd_name', 'st_brokers.start_date', 'st_brokers.amount')
            ->get();
        return [$st_bk];
    }

    public function bk_pd($type, $id)
    {        
        if($type == 'withdraw'){
            $tran = StWithdraw::findOrFail($id);

        }else if($type == 'return'){
            $tran = StReturn::findOrFail($id);

        // }else if($type == 'change'){
        //     $tran = StChange::findOrFail($id);
        }  

        $st_bk = StBroker::join('st_products', 'st_brokers.st_product_id', '=', 'st_products.id')
            ->join('rm_brokers', 'st_brokers.rm_broker_id', '=', 'rm_brokers.id')
            ->where('st_brokers.rm_broker_id', $tran->rm_broker_id)
            ->where('st_brokers.st_product_id', $tran->st_product_id)
            ->where('st_brokers.status',1)
            ->select('st_brokers.id', 'st_brokers.rm_broker_id', 'rm_brokers.name AS bk_name', 'st_brokers.st_product_id', 'st_products.name AS pd_name', 'st_brokers.start_date')
            ->get();
        return [$st_bk];
    }

    public function show($id)
    {   
        $tran['type'] = 'show';
        $show_pd = $this->show_bk($id);
        $st_bk  = $show_pd[0];        

        $to_show_set = array();
        $to_show = array();
        $bk_wd = StWithdraw::where('rm_broker_id', $st_bk[0]['rm_broker_id'])
            ->where('st_product_id', $st_bk[0]['st_product_id'])
            ->where('status',1)->where('withdraw_date', '>=', $st_bk[0]['start_date'])
            ->selectRaw('withdraw_date as tran_date, sum(amount) as amount')
            ->groupBy('withdraw_date')
            // ->orderBy('withdraw_date', 'DESC')
            ->get(); 
        foreach ($bk_wd as $key) {
            $to_show_set[$key->tran_date]['wd'] = $key->amount;
        }
        $bk_rt = StReturn::where('rm_broker_id', $st_bk[0]['rm_broker_id'])
            ->where('st_product_id', $st_bk[0]['st_product_id'])
            ->where('status',1)->where('return_date', '>=', $st_bk[0]['start_date'])
            ->selectRaw('return_date as tran_date, sum(amount) as amount')
            ->groupBy('return_date')
            // ->orderBy('return_date', 'DESC')
            ->get(); 
        foreach ($bk_rt as $key) {
            $to_show_set[$key->tran_date]['rt'] = $key->amount;
        }
        // dd($to_show);
        if(count($to_show_set)>0){
            $max_array = strtotime( max(array_keys($to_show_set)));
            $min_array = strtotime( min(array_keys($to_show_set)));

            for ( $i = $min_array; $i <= $max_array; $i = $i + 86400 ) {
                $thisDate = date( 'Y-m-d', $i ); 
                if(!empty($to_show_set[$thisDate]['wd']))  $to_show[$thisDate]['wd'] = $to_show_set[$thisDate]['wd'];
                if(!empty($to_show_set[$thisDate]['rt']))  $to_show[$thisDate]['rt'] = $to_show_set[$thisDate]['rt'];
            }
        }

        return view('st_wh.st_withdraw.show',compact('tran','st_bk','to_show'));
    }

    public function index(Request $request, $type, $id)
    {   
        $tran['type'] = $type;        
        $show_pd = $this->show_bk($id);
        $st_bk  = $show_pd[0];
        
        $query = Crop::orderBy('id','DESC')->get();
        foreach ($query as $key) {
            $crop[$key->id] = $key->details;
        }
        // dd($crop);
        $st_search = '';
        if(!empty($request->crop_id)){
            $st_search .= ' (crop_id = '.$request->crop_id.') ';
        }
        if(!empty($request->date_from)){
            if(empty($request->date_to))   $request->date_to = date('Y-m-d'); 
            if(!empty($request->crop_id))   $st_search .= ' AND ';
        }

        $st_tran = array();
        if($type == 'withdraw'){
            $tran['type_show'] = 'เบิก';
            if(!empty($request->date_from)){
                $st_search .= " (withdraw_date Between '".$request->date_from."' AND '".$request->date_to."') ";
            } 
            $st_tran = StWithdraw::where('rm_broker_id', $st_bk[0]['rm_broker_id'])->where('st_product_id', $st_bk[0]['st_product_id'])
                ->where('withdraw_date', '>=', $st_bk[0]['start_date'])->where('status', 1);           
            if(!empty($st_search)) $st_tran = $st_tran->whereRaw($st_search);
            $st_tran = $st_tran->select('id', 'crop_id', 'no_id', 'rm_broker_id', 'st_product_id', 'withdraw_date AS tran_date', 'amount')
                ->orderBy('withdraw_date')->orderBy('no_id')->get();

        }else if($type == 'return'){
            $tran['type_show'] = 'คืน';
            if(!empty($request->date_from)){
                $st_search .= " (return_date Between '".$request->date_from."' AND '".$request->date_to."') ";
            }
            $st_tran = StReturn::where('rm_broker_id', $st_bk[0]['rm_broker_id'])->where('st_product_id', $st_bk[0]['st_product_id'])
            ->where('return_date', '>=', $st_bk[0]['start_date'])->where('status', 1);
            if(!empty($st_search))  $st_tran = $st_tran->whereRaw($st_search);
            $st_tran = $st_tran->select('id', 'crop_id', 'no_id', 'rm_broker_id', 'st_product_id', 'return_date AS tran_date', 'amount')
            ->orderBy('return_date')->orderBy('no_id')->get();        

        // }else if($type == 'change'){
        //     $tran['type_show'] = 'เปลี่ยน';
        //     $st_tran = StChange::where('rm_broker_id', $st_bk[0]['rm_broker_id'])->where('st_product_id', $st_bk[0]['st_product_id'])
        //     ->where('change_date', '>=', $st_bk[0]['start_date'])->where('status', 1)
        //     ->select('id', 'no_id', 'rm_broker_id', 'st_product_id', 'change_date AS tran_date', 'amount')
        //     ->orderBy('change_date', 'DESC')->get();            
        }
        
        return view('st_wh.st_withdraw.index',compact('tran','st_bk','st_tran','crop'));
    }

    public function create($type, $id)
    {
        $show_pd = $this->show_bk($id);
        $st_bk  = $show_pd[0];

        $crop = Crop::orderBy('id','DESC')->get();
        
        $tran['type'] = $type;
        // $st_tran = array();
        if($type == 'withdraw'){
            $tran['type_show'] = 'เบิก';

        }else if($type == 'return'){
            $tran['type_show'] = 'คืน';

        }        

        return view('st_wh.st_withdraw.create',compact('tran','st_bk','crop'));
    }

    public function store(Request $request, $type, $id)
    {
        $requestData = $request->all();
        
        $tran['type'] = $type;
        if($type == 'withdraw'){
            $tran['type_show'] = 'เบิก';
            $requestData['withdraw_date'] = $requestData['tran_date'];
            StWithdraw::create($requestData);

        }else if($type == 'return'){
            $tran['type_show'] = 'คืน';
            $requestData['return_date'] = $requestData['tran_date'];
            StReturn::create($requestData);

        // }else if($type == 'change'){
        //     $tran['type_show'] = 'เปลี่ยน';
        //     $requestData['change_date'] = $requestData['tran_date'];
        //     StChange::create($requestData);
        } 

        return redirect('/st_wh/st_withdraw/index/'.$tran['type'].'/'.$id)->with('success', ' added!');
    }

    public function edit($type, $id)
    {
        
        $show_pd = $this->bk_pd($type, $id);
        $st_bk  = $show_pd[0];
        
        $crop = Crop::orderBy('id','DESC')->get();

        $tran['type'] = $type;
        $st_tran = array();
        if($type == 'withdraw'){
            $tran['type_show'] = 'เบิก';
            $st_tran = StWithdraw::findOrFail($id);
            // dd($st_tran->withdraw_date);
            $st_tran->tran_date =  $st_tran->withdraw_date;

        }else if($type == 'return'){
            $tran['type_show'] = 'คืน';
            $st_tran = StReturn::findOrFail($id);
            $st_tran->tran_date =  $st_tran->return_date;

        // }else if($type == 'change'){
        //     $tran['type_show'] = 'เปลี่ยน';
        //     $st_tran = StChange::findOrFail($id);
        //     $st_tran->tran_date =  $st_tran->change_date;
        }     

        return view('st_wh.st_withdraw.edit',compact('tran','st_bk','st_tran','crop'));
    }

    public function update(Request $request, $type, $id)
    {   
        $requestData = $request->all();     
        $show_pd = $this->bk_pd($type, $id);
        $st_bk  = $show_pd[0];
        
        $tran['type'] = $type;
        if($type == 'withdraw'){
            $tran['type_show'] = 'เบิก';
            $query_tran = StWithdraw::findOrFail($id);
            $requestData['withdraw_date'] = $requestData['tran_date'];

        }else if($type == 'return'){
            $tran['type_show'] = 'คืน';            
            $query_tran = StReturn::findOrFail($id);
            $requestData['return_date'] = $requestData['tran_date'];

        // }else if($type == 'change'){
        //     $tran['type_show'] = 'เปลี่ยน';
        //     $query_tran = StChange::findOrFail($id);
        //     $requestData['change_date'] = $requestData['tran_date'];
        } 
        
        $query_tran->update($requestData);

        return redirect('/st_wh/st_withdraw/index/'.$tran['type'].'/'.$st_bk[0]['id'])->with('success', ' updated!');
    }
    
    public function destroy($type, $id)
    {
        $show_pd = $this->bk_pd($type, $id);
        $st_bk  = $show_pd[0];
        
        $tran['type'] = $type;
        $st_tran = array();
        if($type == 'withdraw'){
            $tran['type_show'] = 'เบิก';
            $st_tran = StWithdraw::findOrFail($id);

        }else if($type == 'return'){
            $tran['type_show'] = 'คืน';
            $st_tran = StReturn::findOrFail($id);

        // }else if($type == 'change'){
        //     $tran['type_show'] = 'เปลี่ยน';
        //     $st_tran = StChange::findOrFail($id);
        }  
        $set_status['status'] = 0;   
        $st_tran->update($set_status);

        return redirect('/st_wh/st_withdraw/index/'.$tran['type'].'/'.$st_bk[0]['id'])->with('success', ' deleted!');
    }

    public function detail_no_set($bk, $pd)
    {
        //สำหรับข้อมูลที่ไม่สามารถจับกับ Broker ได้/วันที่ก่อนหน้าการ set Broker
        $tran['type'] = 'detail';

        //มี bk กับ pd ให้ ไปตามหาวันเริมของ st_brokers เป็นเงิ่อนไขในการดึงข้อมูลที่แสดงใน detail (ที่ไม่อยู่ในช่วงเวลา)
        $query_st_bk = StReturn::join('rm_brokers', 'st_returns.rm_broker_id', '=', 'rm_brokers.id')
            ->join('st_products', 'st_returns.st_product_id', '=', 'st_products.id')
            ->where('st_returns.rm_broker_id', $bk)->where('st_returns.st_product_id', $pd)
            ->where('st_returns.status', 1)
            ->selectRaw('rm_brokers.name AS bk_name, st_products.name AS pd_name, st_returns.return_date, sum(st_returns.amount) as amount')
            ->groupBy('rm_brokers.name', 'st_products.name', 'st_returns.return_date')
            ->orderBy('st_returns.return_date')->get();
        foreach ($query_st_bk as $key) {
            $st_bk[0]['bk_name'] = $key->bk_name;
            $st_bk[0]['pd_name'] = $key->pd_name;
            $to_show_set[$key->return_date]['rt'] = $key->amount;
        }

        //หาวันเริ่มของ st_broker ถ้ามี ค่อยเอาไปเป็นเงื่อนไขใน query ด้านล่าง
        $st_rt  = StBroker::where('rm_broker_id', $bk)->where('st_product_id', $pd)
            ->where('status', 1)
            ->select('start_date')->get();
        // dd($st_rt[0]['start_date']);

        if(!empty($st_rt[0]['start_date'])) $max_array = strtotime("-1 day",strtotime($st_rt[0]['start_date']));
        else    $max_array = strtotime(max(array_keys($to_show_set)));
        $min_array = strtotime(min(array_keys($to_show_set)));

        for ( $i = $min_array; $i <= $max_array; $i = $i + 86400 ) {
            $thisDate = date( 'Y-m-d', $i ); 
            if(!empty($to_show_set[$thisDate]['rt']))  $to_show[$thisDate]['rt'] = $to_show_set[$thisDate]['rt'];
        }

        return view('st_wh.st_withdraw.show',compact('tran','to_show','st_bk'));
    }

    public function reportView()
    {
        $bk = StBroker::join('rm_brokers', 'st_brokers.rm_broker_id', '=', 'rm_brokers.id')
            ->where('st_brokers.status',1)
            ->select('st_brokers.rm_broker_id AS id', 'rm_brokers.name')
            ->orderBy('rm_brokers.name')
            ->groupBy('st_brokers.rm_broker_id', 'rm_brokers.name')
            ->get();            
        $pd = StBroker::join('st_products', 'st_brokers.st_product_id', '=', 'st_products.id')
            ->where('st_brokers.status',1)
            ->select('st_brokers.st_product_id AS id', 'st_products.name')
            ->orderBy('st_products.name')
            ->groupBy('st_brokers.st_product_id', 'st_products.name')
            ->get();
        // dd($pd);
        return view('st_wh.report.report', compact('bk', 'pd'));
    }

    public function report_export(Request $request)
    {
        $bk = RmBroker::all(); 
        foreach($bk as $key){
            $bk_id[$key->id] = $key->name;
        }           
        $pd = StProduct::all();
        foreach($pd as $key){
            $pd_id[$key->id] = $key->name;
        } 
        
        $report_id = $request->report_id;
        $st_date = date ("Y-m-d", strtotime($request->st_date));
        $ed_date = date ("Y-m-d", strtotime($request->ed_date));

        if($report_id == 1){
            //แบ่งการหาข้อมูลเป็น 2 ช่วง คือ ก่อนวันที่ค้น(ถ้ามี), ระหว่างวันที่ค้น(ถ้ามี)
            //ก่อนวันที่ค้น จะต้องสรุปเป็นข้อมูลตั้งต้น รับ เปลี่ยน
            //ตั้งต้น
            $bf_st = array();
            $to_show = array();
            $to_detail = array();
            $to_sum = array();
            $bf_st = StWhBegin::where('status',1)->where('start_date', '<', $st_date)->get(); 
            if(count($bf_st)>0){
                foreach ($bf_st as $key) {
                    $to_show[$pd_id[$key->st_product_id]]['st'] = $key->amount;
                }
            }
            //รับ
            $bf_st = array();
            $bf_st = StReceive::where('status',1)->where('receive_date', '<', $st_date)->selectRaw('st_product_id, SUM(amount) as amount')->groupBy('st_product_id')->get(); 
            if(count($bf_st)>0){
                foreach ($bf_st as $key) {
                    $to_show[$pd_id[$key->st_product_id]]['rc'] = $key->amount;
                }
            }
            //เปลี่ยน
            $bf_st = array();
            $bf_st = StChange::where('status',1)->where('change_date', '<', $st_date)->selectRaw('st_product_id, SUM(amount) as amount')->groupBy('st_product_id')->get();
            if(count($bf_st)>0){
                foreach ($bf_st as $key) {
                    $to_show[$pd_id[$key->st_product_id]]['ch'] = $key->amount;
                }
            }
            //เบิก
            $bf_st = array();
            $bf_st = StWithdraw::where('status',1)->where('withdraw_date', '<', $st_date)->selectRaw('st_product_id, SUM(amount) as amount')->groupBy('st_product_id')->get(); 
            if(count($bf_st)>0){
                foreach ($bf_st as $key) {
                    $to_show[$pd_id[$key->st_product_id]]['wd'] = $key->amount;
                }
            }
            //คืน
            $bf_st = array();
            $bf_st = StReturn::where('status',1)->where('return_date', '<', $st_date)->selectRaw('st_product_id, SUM(amount) as amount')->groupBy('st_product_id')->get();
            if(count($bf_st)>0){
                foreach ($bf_st as $key) {
                    $to_show[$pd_id[$key->st_product_id]]['rt'] = $key->amount;
                }
            }
            

            //ระหว่างวันที่ค้น
            //ตั้งต้น
            $in_time = array();
            $in_time = StWhBegin::where('status',1)->whereBetween('start_date', [$st_date, $ed_date])
            ->select('st_product_id', 'amount')->get();
            if(count($in_time)>0){
                foreach ($in_time as $key) {
                    $to_show[$pd_id[$key->st_product_id]]['st'] = $key->amount;
                }
            } 
            //รับ
            $in_time = array();
            $in_time = StReceive::where('status',1)->whereBetween('receive_date', [$st_date, $ed_date])
            ->select('no_id', 'st_product_id', 'amount', 'receive_date')->orderBy('receive_date')->get(); 
            if(count($in_time)>0){
                foreach ($in_time as $key) {
                    $to_detail[$pd_id[$key->st_product_id]][$key->receive_date][$key->no_id]['rc'] = $key->amount;
                }
            } 
            //เปลี่ยน
            $in_time = array();
            $in_time = StChange::where('status',1)->whereBetween('change_date', [$st_date, $ed_date])
            ->select('no_id', 'st_product_id', 'amount', 'change_date')->orderBy('change_date')->get();
            if(count($in_time)>0){
                foreach ($in_time as $key) {
                    $to_detail[$pd_id[$key->st_product_id]][$key->change_date][$key->no_id]['ch'] = $key->amount;
                }
            } 
            //เบิก
            $in_time = array();
            $in_time = StWithdraw::where('status',1)->whereBetween('withdraw_date', [$st_date, $ed_date])
            ->selectRaw('st_product_id, SUM(amount) AS amount, withdraw_date')->groupBy('st_product_id', 'withdraw_date')->orderBy('withdraw_date')->get(); 
            if(count($in_time)>0){
                foreach ($in_time as $key) {
                    $to_sum[$pd_id[$key->st_product_id]][$key->withdraw_date]['wd'] = $key->amount;
                }
            } 
            //คืน
            $in_time = array();
            $in_time = StReturn::where('status',1)->whereBetween('return_date', [$st_date, $ed_date])
            ->selectRaw('st_product_id, SUM(amount) AS amount, return_date')->groupBy('st_product_id', 'return_date')->orderBy('return_date')->get();
            if(count($in_time)>0){
                foreach ($in_time as $key) {
                    $to_sum[$pd_id[$key->st_product_id]][$key->return_date]['rt'] = $key->amount;
                }
            } 

            foreach ($to_sum as $kpd => $vpd) {
                foreach ($vpd as $kdate => $vdate) {
                    if(empty($to_detail[$kpd][$kdate])){
                        $to_detail[$kpd][$kdate]['']['rc'] = 0;
                    }
                }
            }
            // dd($to_detail);

            //เรียงลำดับวันที่
            $to_dt = array();
            $startTime = strtotime($st_date);
            $endTime = strtotime($ed_date);
            foreach ($to_detail as $kpd => $vpd) {
                for ( $i = $startTime; $i <= $endTime; $i = $i + 86400 ) {
                    if(!empty($to_detail[$kpd][date( 'Y-m-d', $i )])){
                        foreach ($to_detail[$kpd][date( 'Y-m-d', $i )] as $kno => $vno) {
                            if(empty($to_detail[$kpd][date( 'Y-m-d', $i )][$kno]['rc'])) $to_dt[$kpd][date( 'Y-m-d', $i )][$kno]['rc'] = 0;
                            else    $to_dt[$kpd][date( 'Y-m-d', $i )][$kno]['rc'] = $to_detail[$kpd][date( 'Y-m-d', $i )][$kno]['rc'];
                            if(empty($to_detail[$kpd][date( 'Y-m-d', $i )][$kno]['ch'])) $to_dt[$kpd][date( 'Y-m-d', $i )][$kno]['ch'] = 0;
                            else    $to_dt[$kpd][date( 'Y-m-d', $i )][$kno]['ch'] = $to_detail[$kpd][date( 'Y-m-d', $i )][$kno]['ch'];
                        }
                    }
                    
                }
            }
            // dd($to_dt);

            foreach ($to_dt as $kpd => $vpd) {
                if(empty($to_show[$kpd]['st'])) $to_show[$kpd]['st'] = 0;
            }


            if(count($to_show)>0){
                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();

                $sheet->setCellValueByColumnAndRow(1, 1, "รายการเคลื่อนไหว Stock ระหว่างวันที่ ".date ("d-m-Y", strtotime($st_date))." ถึงวันที่ ".date ("d-m-Y", strtotime($ed_date)));
                $sheet->mergeCellsByColumnAndRow(1, 1, 14, 1);

                $sheet->setCellValueByColumnAndRow(1, 2, "Stock");
                $sheet->setCellValueByColumnAndRow(2, 2, "ตั้งต้น");
                $sheet->setCellValueByColumnAndRow(3, 2, "รับ");
                $sheet->setCellValueByColumnAndRow(4, 2, "เปลี่ยน");  
                $sheet->setCellValueByColumnAndRow(5, 2, "เบิก");
                $sheet->setCellValueByColumnAndRow(6, 2, "คืน");  
                $sheet->setCellValueByColumnAndRow(7, 2, "ยกมา");
                $sheet->setCellValueByColumnAndRow(8, 2, "วันที่");
                $sheet->setCellValueByColumnAndRow(9, 2, "เลขที่");  
                $sheet->setCellValueByColumnAndRow(10, 2, "รับ");
                $sheet->setCellValueByColumnAndRow(11, 2, "เปลี่ยน");  
                $sheet->setCellValueByColumnAndRow(12, 2, "เบิก");
                $sheet->setCellValueByColumnAndRow(13, 2, "คืน");                 
                $sheet->setCellValueByColumnAndRow(14, 2, "คงเหลือ");

                $sheet->getStyle('A2:N2')->getAlignment()->setHorizontal('center');         
                $sheet->getStyle('A2:N2')->getAlignment()->setVertical('center');
                $sheet->getStyle('A2:G2')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');
                $sheet->getStyle('H2:N2')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('92CDDC');


                $startrow = 3;
                foreach ($to_show as $key => $value) {
                    $sheet->setCellValueByColumnAndRow(1, $startrow, $key);
                    if(!empty($to_show[$key]['st']))  $sheet->setCellValueByColumnAndRow(2, $startrow, $to_show[$key]['st']);
                    if(!empty($to_show[$key]['rc']))  $sheet->setCellValueByColumnAndRow(3, $startrow, $to_show[$key]['rc']);
                    if(!empty($to_show[$key]['ch']))  $sheet->setCellValueByColumnAndRow(4, $startrow, $to_show[$key]['ch']);
                    if(!empty($to_show[$key]['wd']))  $sheet->setCellValueByColumnAndRow(5, $startrow, $to_show[$key]['wd']);
                    if(!empty($to_show[$key]['rt']))  $sheet->setCellValueByColumnAndRow(6, $startrow, $to_show[$key]['rt']);
                    $sheet->setCellValueByColumnAndRow(7, $startrow, '=B'.$startrow.'+C'.$startrow.'-D'.$startrow.'-E'.$startrow.'+F'.$startrow);
                    $i = 0;
                    if(!empty($to_dt[$key])){
                        foreach ($to_dt[$key] as $kdate => $vdate) {
                            foreach ($vdate as $kno => $vno) {
                                $sheet->setCellValueByColumnAndRow(8, $startrow, date('d/m/Y',strtotime($kdate)));
                                $sheet->setCellValueByColumnAndRow(9, $startrow, $kno);
                                if(!empty($to_dt[$key][$kdate][$kno]['rc']))  $sheet->setCellValueByColumnAndRow(10, $startrow, $to_dt[$key][$kdate][$kno]['rc']);
                                if(!empty($to_dt[$key][$kdate][$kno]['ch']))  $sheet->setCellValueByColumnAndRow(11, $startrow, $to_dt[$key][$kdate][$kno]['ch']);
                                if(!empty($to_sum[$key][$kdate]['wd']))  $sheet->setCellValueByColumnAndRow(12, $startrow, $to_sum[$key][$kdate]['wd']);
                                if(!empty($to_sum[$key][$kdate]['rt']))  $sheet->setCellValueByColumnAndRow(13, $startrow, $to_sum[$key][$kdate]['rt']);
                                if($i==0)   $sheet->setCellValueByColumnAndRow(14, $startrow, '=G'.$startrow.'+J'.$startrow.'-K'.$startrow.'-L'.$startrow.'+M'.$startrow);
                                else    $sheet->setCellValueByColumnAndRow(14, $startrow, '=N'.($startrow-1).'+J'.$startrow.'-K'.$startrow.'-L'.$startrow.'+M'.$startrow);
                                $i++;
                                $startrow++;
                            }
                        }
                    }else{
                        $startrow++;
                    }
                }
                
            }

            $sheet->getStyle('A2:N'.($startrow-1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

            for($i=1; $i<=14; $i++) {
                $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($i);
                $sheet->getColumnDimension($columnID)->setAutoSize(true);
            }

            $writer = new Xlsx($spreadsheet);
            $filename = "st_report_1-" . date('yymmdd-hi') . ".xlsx";            
            $completedirectory = 'storage/app/public/WhRM/completes/report/';
            $tmpfolder = date('Ymd');
            if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
            }
            $writer->save($completedirectory . '/' . $tmpfolder . '/' . $filename);
            $path_file = $completedirectory . '/' . $tmpfolder . '/' . $filename;

            return response()->download($path_file);

        }elseif($report_id == 2){
            //แบ่งการหาข้อมูลเป็น 2 ช่วง คือ ก่อนวันที่ค้น(ถ้ามี), ระหว่างวันที่ค้น(ถ้ามี)
            //ก่อนวันที่ค้น จะต้องสรุปเป็นข้อมูลตั้งต้น รับ เปลี่ยน
            //ตั้งต้น
            $bf_st = array();
            $to_show = array();
            $to_detail = array();
            $bf_st = StBroker::where('status',1)->where('start_date', '<', $st_date)->get(); 
            if(count($bf_st)>0){
                foreach ($bf_st as $key) {
                    $to_show[$pd_id[$key->st_product_id]][$bk_id[$key->rm_broker_id]]['st'] = $key->amount;
                }
            }
            //เบิก
            $bf_st = array();
            $bf_st = StWithdraw::where('status',1)->where('withdraw_date', '<', $st_date)->selectRaw('st_product_id, rm_broker_id, SUM(amount) as amount')->groupBy('st_product_id', 'rm_broker_id')->get(); 
            if(count($bf_st)>0){
                foreach ($bf_st as $key) {
                    $to_show[$pd_id[$key->st_product_id]][$bk_id[$key->rm_broker_id]]['wd'] = $key->amount;
                }
            }
            //คืน
            $bf_st = array();
            $bf_st = StReturn::where('status',1)->where('return_date', '<', $st_date)->selectRaw('st_product_id, rm_broker_id, SUM(amount) as amount')->groupBy('st_product_id', 'rm_broker_id')->get();
            // dd($bf_st);
            if(count($bf_st)>0){
                foreach ($bf_st as $key) {
                    $to_show[$pd_id[$key->st_product_id]][$bk_id[$key->rm_broker_id]]['rt'] = $key->amount;
                }
            }
            

            //ระหว่างวันที่ค้น
            //ตั้งต้น
            $in_time = array();
            $in_time = StBroker::where('status',1)->whereBetween('start_date', [$st_date, $ed_date])
            ->select('st_product_id', 'rm_broker_id', 'amount')->get();
            if(count($in_time)>0){
                foreach ($in_time as $key) {
                    $to_show[$pd_id[$key->st_product_id]][$bk_id[$key->rm_broker_id]]['st'] = $key->amount;
                }
            } 
            // dd($to_show);
            //เบิก
            $in_time = array();
            $in_time = StWithdraw::where('status',1)->whereBetween('withdraw_date', [$st_date, $ed_date])
                ->select('no_id', 'st_product_id', 'rm_broker_id', 'amount', 'withdraw_date')
                ->orderBy('st_product_id')->orderBy('withdraw_date')->orderBy('no_id')->get(); 
            if(count($in_time)>0){
                foreach ($in_time as $key) {
                    $to_detail[$pd_id[$key->st_product_id]][$bk_id[$key->rm_broker_id]][$key->withdraw_date][$key->no_id]['wd'] = $key->amount;
                }
            } 
            //คืน
            $in_time = array();
            $in_time = StReturn::where('status',1)->whereBetween('return_date', [$st_date, $ed_date])
                ->select('no_id', 'st_product_id', 'rm_broker_id', 'amount', 'return_date')
                ->orderBy('st_product_id')->orderBy('return_date')->orderBy('no_id')->get();
            if(count($in_time)>0){
                foreach ($in_time as $key) {
                    $to_detail[$pd_id[$key->st_product_id]][$bk_id[$key->rm_broker_id]][$key->return_date][$key->no_id]['rt'] = $key->amount;
                }
            } 
            // dd($to_detail);
            //เรียงลำดับ
            if(count($to_detail)>0){
                $to_dt = array();
                $startTime = strtotime($st_date);
                $endTime = strtotime($ed_date);
                foreach ($to_detail as $kpd => $vpd) {
                    $pd_index[] = $kpd;
                }
                sort($pd_index);
                foreach ($pd_index as $kpd => $vpd) {
                    foreach ($to_detail[$vpd] as $kbk => $vbk) {
                        $bk_index[$vpd][] = $kbk;
                    }
                    sort($bk_index[$vpd]);
                }
                foreach ($pd_index as $kpd => $vpd) {
                    foreach ($bk_index[$vpd] as $kbk => $vbk) {                    
                        foreach ($to_detail[$vpd][$vbk] as $kdate => $vdate) {
                            $date_index[$vpd][$vbk][] = $kdate;
                        }
                        sort($date_index[$vpd][$vbk]);
                    }
                }
                // dd($pd_index);
                foreach ($pd_index as $kpd => $vpd) {
                    foreach ($bk_index[$vpd] as $kbk => $vbk) {                    
                        foreach ($date_index[$vpd][$vbk] as $kdate => $vdate) {                        
                            foreach ($to_detail[$vpd][$vbk][$vdate] as $kno => $vno) {
                                $no_index[$vpd][$vbk][$vdate][] = $kno;
                            }
                            sort($no_index[$vpd][$vbk][$vdate]);
                        }
                    }
                }
                // dd($no_index);
            }
            

            // dd($to_detail);
            if(!empty($no_index)){
                foreach ($no_index as $kpd => $vpd) {
                    foreach ($vpd as $kbk => $vbk) {
                        if(empty($to_show[$kpd][$kbk]['st'])) $to_show[$kpd][$kbk]['st'] = 0;
                    }
                }
            }
            


            if(count($to_show)>0){
                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();

                $sheet->setCellValueByColumnAndRow(1, 1, "รายการเคลื่อนไหว Stock ระหว่างวันที่ ".date ("d-m-Y", strtotime($st_date))." ถึงวันที่ ".date ("d-m-Y", strtotime($ed_date)));
                $sheet->mergeCellsByColumnAndRow(1, 1, 8, 1);

                $sheet->setCellValueByColumnAndRow(1, 2, "Stock");
                $sheet->setCellValueByColumnAndRow(2, 2, "Broker");
                $sheet->setCellValueByColumnAndRow(3, 2, "ตั้งต้น");
                $sheet->setCellValueByColumnAndRow(4, 2, "เบิก");
                $sheet->setCellValueByColumnAndRow(5, 2, "คืน");                
                $sheet->setCellValueByColumnAndRow(6, 2, "ยกมา");
                $sheet->setCellValueByColumnAndRow(7, 2, "วันที่");
                $sheet->setCellValueByColumnAndRow(8, 2, "เลขที่");  
                $sheet->setCellValueByColumnAndRow(9, 2, "เบิก");
                $sheet->setCellValueByColumnAndRow(10, 2, "คืน");                
                $sheet->setCellValueByColumnAndRow(11, 2, "คงเหลือ");

                $sheet->getStyle('A2:J2')->getAlignment()->setHorizontal('center');         
                $sheet->getStyle('A2:J2')->getAlignment()->setVertical('center');
                $sheet->getStyle('A2:J2')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');

                $startrow = 3;
                if(!empty($no_index)){
                    foreach ($no_index as $key => $value) {
                        $sheet->setCellValueByColumnAndRow(1, $startrow, $key);
                        foreach ($value as $kbk => $vbk) {
                            $sheet->setCellValueByColumnAndRow(2, $startrow, $kbk);
                            if(!empty($to_show[$key][$kbk]['st']))  $sheet->setCellValueByColumnAndRow(3, $startrow, $to_show[$key][$kbk]['st']);
                            if(!empty($to_show[$key][$kbk]['wd']))  $sheet->setCellValueByColumnAndRow(4, $startrow, $to_show[$key][$kbk]['wd']);
                            if(!empty($to_show[$key][$kbk]['rt']))  $sheet->setCellValueByColumnAndRow(5, $startrow, $to_show[$key][$kbk]['rt']);
                            $sheet->setCellValueByColumnAndRow(6, $startrow, '=C'.$startrow.'+D'.$startrow.'-E'.$startrow);
                            $i = 0;
                            if(!empty($to_detail[$key][$kbk])){
                                foreach ($no_index[$key][$kbk] as $kdate => $vdate) {
                                    foreach ($vdate as $kno => $vno) {
                                        $sheet->setCellValueByColumnAndRow(7, $startrow, date('d/m/Y',strtotime($kdate)));
                                        $sheet->setCellValueByColumnAndRow(8, $startrow, $vno);
                                        if(!empty($to_detail[$key][$kbk][$kdate][$vno]['wd']))  $sheet->setCellValueByColumnAndRow(9, $startrow, $to_detail[$key][$kbk][$kdate][$vno]['wd']);
                                        if(!empty($to_detail[$key][$kbk][$kdate][$vno]['rt']))  $sheet->setCellValueByColumnAndRow(10, $startrow, $to_detail[$key][$kbk][$kdate][$vno]['rt']);
                                        if($i==0)   $sheet->setCellValueByColumnAndRow(11, $startrow, '=F'.$startrow.'+I'.$startrow.'-J'.$startrow);
                                        else    $sheet->setCellValueByColumnAndRow(11, $startrow, '=K'.($startrow-1).'+I'.$startrow.'-J'.$startrow);
                                        $i++;
                                        $startrow++;
                                    }
                                }
                            }else{
                                $startrow++;
                            }
                        }
                    }
                }
                
            }

            $sheet->getStyle('A2:K'.($startrow-1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

            for($i=1; $i<=11; $i++) {
                $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($i);
                $sheet->getColumnDimension($columnID)->setAutoSize(true);
            }

            $writer = new Xlsx($spreadsheet);
            $filename = "st_report_2-" . date('yymmdd-hi') . ".xlsx";            
            $completedirectory = 'storage/app/public/WhRM/completes/report/';
            $tmpfolder = date('Ymd');
            if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
            }
            $writer->save($completedirectory . '/' . $tmpfolder . '/' . $filename);
            $path_file = $completedirectory . '/' . $tmpfolder . '/' . $filename;

            return response()->download($path_file);

        }elseif($report_id == 3){
            $pd_report = $request->pd_id;
            $pd_name = $pd_id[$pd_report];
            $before_st = date ("Y-m-d", strtotime('-1 day', strtotime($st_date)));
            
            $month_name = array("","มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
            $m_date = date ("m", strtotime($st_date));
            $y_date = date ("Y", strtotime($st_date));
            $y_th = intval($y_date) + 543;
            
            $th_date = date ("d", strtotime($st_date)).' '.$month_name[$m_date].' '.$y_th;
            
            $tbl_1 = array();
            $query = StWhBegin::where('st_wh_begins.start_date', '<=', $st_date)
                ->where('st_wh_begins.st_product_id', $pd_report)
                ->where('st_wh_begins.status', 1)
                ->selectRaw("TOP (1) st_wh_begins.start_date, st_wh_begins.amount, 
                    (SELECT SUM(amount) FROM st_withdraws
                        WHERE (st_withdraws.st_product_id = st_wh_begins.st_product_id) 
                            AND (st_withdraws.withdraw_date BETWEEN st_wh_begins.start_date AND '".$before_st."')
                    ) AS wh_bf, 
                    (SELECT SUM(amount) FROM st_returns
                        WHERE (st_returns.st_product_id = st_wh_begins.st_product_id) 
                            AND (st_returns.return_date BETWEEN st_wh_begins.start_date AND '".$before_st."')
                    ) AS rt_bf, 
                    (SELECT SUM(amount) FROM st_withdraws
                        WHERE (st_withdraws.st_product_id = st_wh_begins.st_product_id) AND (st_withdraws.withdraw_date = '".$st_date."')
                    ) AS wh_td, 
                    (SELECT SUM(amount) FROM st_returns
                        WHERE (st_returns.st_product_id = st_wh_begins.st_product_id) AND (st_returns.return_date = '".$st_date."')
                    ) AS rt_td")
                ->orderBy('st_wh_begins.start_date', 'DESC')->orderBy('st_wh_begins.updated_at', 'DESC')->get();
            
            foreach ($query as $key) {
                $arr = array('amount', 'wh_bf', 'rt_bf', 'wh_td', 'rt_td');
                foreach ($arr as $karr => $varr) {
                    if(empty($key->$varr))  $tbl_1[$varr] = 0;
                }
                $tbl_1['start_date'] = $key->start_date;
                $tbl_1['bg_amount'] = $key->amount - $key->wh_bf + $key->rt_bf;
                if(!empty($key->wh_td))  $tbl_1['wh_td'] = $key->wh_td;
                if(!empty($key->rt_td))  $tbl_1['rt_td'] = $key->rt_td;
                $tbl_1['total'] = $tbl_1['bg_amount'] - $tbl_1['wh_td'] + $tbl_1['rt_td'];
            }
            $val_rang = array();
            
            $col_rang = array('ยอดกระสอบ', 'จำนวนเบิก/BK', 'จำนวนคืน/BK', 'คงเหลื่อ LACO');
            if(!empty($tbl_1))  $val_rang = array(intval($tbl_1['bg_amount']), intval($tbl_1['wh_td']), intval($tbl_1['rt_td']), intval($tbl_1['total']));
            
            $query = RmBroker::selectRaw("id, name, 
                        (SELECT TOP (1) id
                            FROM   st_brokers
                            WHERE (rm_broker_id = rm_brokers.id) AND (start_date <= '".$st_date."') AND (st_product_id = ".$pd_report.") AND (status = 1)
                            ORDER BY start_date DESC, updated_at DESC
                        ) AS st_id")
                    ->orderBy('name')->get();  
            $arr = array('st', 'wd', 'rt');          
            foreach ($query as $key) {
                if(!empty($key->name)){
                    $set_bk[$key->id] = $key->name;
                    $first_date[$key->id][$key->st_id] = $key->name;
                    foreach ($arr as $ktype => $vtype) {
                        $set_amount[$key->id][$vtype] = 0;
                    }
                }
            }        
            
            $query = StBroker::where('start_date', '<=', $st_date)->where('st_product_id', $pd_report)->get();
            foreach ($query as $key) {
                $set_bk_amount[$key->id][$key->start_date] = $key->amount;
            }
            
            $query = StWithdraw::select('rm_broker_id', 'withdraw_date', 'amount')
                ->where('st_product_id', $pd_report)->where('withdraw_date', '<=', $before_st)->where('status', 1)
                ->orderBy('rm_broker_id')->orderBy('withdraw_date')
                ->get();
            foreach ($query as $key) {
                $wd_bf_bk[$key->rm_broker_id][$key->withdraw_date] = $key->amount;
            }

            $query = StReturn::select('rm_broker_id', 'return_date', 'amount')
                ->where('st_product_id', $pd_report)->where('return_date', '<=', $before_st)->where('status', 1)
                ->orderBy('rm_broker_id')->orderBy('return_date')
                ->get();
            foreach ($query as $key) {
                $rt_bf_bk[$key->rm_broker_id][$key->return_date] = $key->amount;
            }
            $wd_bk = array();
            $query = StWithdraw::selectRaw('rm_broker_id, sum(amount) AS amount')
                ->where('st_product_id', $pd_report)->where('withdraw_date', $st_date)->where('status', 1)
                ->groupBy('rm_broker_id')
                ->get();
            foreach ($query as $key) {
                $wd_bk[$key->rm_broker_id] = $key->amount;
            }
            $rt_bk = array();
            $query = StReturn::selectRaw('rm_broker_id, sum(amount) AS amount')
                ->where('st_product_id', $pd_report)->where('return_date', $st_date)->where('status', 1)
                ->groupBy('rm_broker_id')
                ->get();
            foreach ($query as $key) {
                $rt_bk[$key->rm_broker_id] = $key->amount;
            }


            $end = new DateTime($st_date);
            foreach ($first_date as $kbk=>$vbk) {
                foreach ($vbk as $kst=>$vst) {
                    if(!empty($set_bk_amount[$kst])){
                        foreach ($set_bk_amount[$kst] as $kdate=>$vdate) {
                            $set_amount[$kbk]['st'] += $vdate;
                            $begin = new DateTime($kdate);
                        }
                        for($i = $begin; $i <= $end; $i->modify('+1 day')){
                            if(!empty($wd_bf_bk[$kbk][$i])) $set_amount[$kbk]['wd'] += $wd_bf_bk[$kbk][$i];
                            if(!empty($rt_bf_bk[$kbk][$i])) $set_amount[$kbk]['rt'] += $rt_bf_bk[$kbk][$i];
                        }
                    }else{
                        if(!empty($wd_bf_bk[$kbk])){
                            foreach ($wd_bf_bk[$kbk] as $kdate=>$vdate) {
                                $set_amount[$kbk]['wd'] += $vdate;
                            }
                        }
                        if(!empty($rt_bf_bk[$kbk])){
                            foreach ($rt_bf_bk[$kbk] as $kdate=>$vdate) {
                                $set_amount[$kbk]['rt'] += $vdate;
                            }
                        }
                    }
                        
                }
            }

            return view('st_wh.report.report_3',compact('th_date', 'pd_name', 'col_rang', 'val_rang', 'set_bk', 'tbl_1', 'set_amount', 'wd_bk', 'rt_bk'));

        }
    }
}
