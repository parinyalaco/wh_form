<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\JobData;
use App\Models\WhDep;

class JobDatasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobdatas = JobData::paginate(25);
        return view('jobdatas.index', compact('jobdatas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $whdeplist = WhDep::where('status','Active')->pluck('name','id');
        return view('jobdatas.create',compact('whdeplist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        JobData::create($requestData);

        Session::flash('flash_message', ' added!');

        return redirect('jobdatas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jobdata = JobData::findOrFail($id);

        return view('jobdatas.show', compact('jobdata'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $whdeplist = WhDep::where('status', 'Active')->pluck('name', 'id');
        $jobdata = JobData::findOrFail($id);

        return view('jobdatas.show', compact('jobdata', 'whdeplist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $jobdata = JobData::findOrFail($id);
        $jobdata->update($requestData);

        Session::flash('flash_message', ' updated!');

        return redirect('jobdatas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
