<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\StProduct;
use App\Models\StWhBegin;
use App\Models\RmBroker;
use App\Models\StBroker;

class StBrokerController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $st_wh = array();
        $st_pd = array();
        // SELECT st_products.name AS pd_name, rm_brokers.name AS bk_name, st_brokers.amount, st_brokers.start_date
        // FROM   st_brokers INNER JOIN
        //      st_products ON st_brokers.st_product_id = st_products.id INNER JOIN
        //      rm_brokers ON st_brokers.rm_broker_id = rm_brokers.id
        // ORDER BY bk_name, pd_name
        $st_bk = StBroker::join('rm_brokers', 'st_brokers.rm_broker_id', '=', 'rm_brokers.id')
            ->join('st_products', 'st_brokers.st_product_id', '=', 'st_products.id')
            ->where('st_brokers.status',1)->where('st_products.status',1)
            ->select('st_brokers.id', 'st_products.name AS pd_name', 'rm_brokers.name AS bk_name', 'st_brokers.amount', 'st_brokers.start_date')
            ->orderBy('rm_brokers.name')->orderBy('st_products.name')->get();
        // $st_pd = StProduct::where('status',1)->orderBy('name')->get();
        // dd($st_bk);
        return view('st_wh.st_broker.index',compact('st_bk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rm_bk = RmBroker::orderBy('name')->get();
        $st_pd = StProduct::where('status',1)->orderBy('name')->get();
        return view('st_wh.st_broker.create',compact('rm_bk', 'st_pd'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $chk_bk = array();
        $chk_bk = StBroker::where('status',1)->where('rm_broker_id',$request->rm_broker_id)->where('st_product_id',$request->st_product_id)->get();
        if(count($chk_bk)>0){
            $st_status['status'] = 0;
            StBroker::where('status',1)->where('rm_broker_id',$request->rm_broker_id)->where('st_product_id',$request->st_product_id)->update($st_status);
        }
        $requestData = $request->all();
        // dd($requestData);
        $st_bk = StBroker::create($requestData);
        if($st_bk){
            return redirect('/st_wh/st_broker')->with('success', ' added!'); 
        }else{
            return redirect('/st_wh/st_broker')->with('error', 'Not added!'); 
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        $rm_bk = RmBroker::orderBy('name')->get();
        $st_pd = StProduct::where('status',1)->orderBy('name')->get();
        $st_bk = StBroker::findOrFail($id);
        return view('st_wh.st_broker.edit',compact('rm_bk', 'st_pd', 'st_bk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $st_wh = StBroker::findOrFail($id);
        $st_status['status'] = 0;
        $st_wh->update($st_status);

        StBroker::create($requestData);

        return redirect('/st_wh/st_broker')->with('success', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $st_wh = StBroker::findOrFail($id);
        $st_status['status'] = 0;
        $st_wh->update($st_status);

        return redirect('/st_wh/st_broker')->with('success', ' deleted!');
    }

    public function st_main()
    {
        $st_bk = array();
        $sum_bk = array();
        $st_bk = StBroker::join('rm_brokers', 'st_brokers.rm_broker_id', '=', 'rm_brokers.id')
            ->join('st_products', 'st_brokers.st_product_id', '=', 'st_products.id')            
            ->where('st_brokers.status',1)
            ->select('st_brokers.id', 'st_brokers.rm_broker_id', 'rm_brokers.name as bk_name', 'st_brokers.st_product_id', 'st_products.name as pd_name', 'st_brokers.amount', 'st_brokers.start_date')
            ->orderBy('rm_brokers.name')->orderBy('st_products.name')->get();
        if(count($st_bk)>0){
            foreach ($st_bk as $key) {
                $sum_wh[$key->bk_name][$key->pd_name]['bk'] = $key->rm_broker_id;
                $sum_wh[$key->bk_name][$key->pd_name]['pd'] = $key->st_product_id;
                $sum_bk[$key->bk_name][$key->pd_name]['wd'] = StWithdraw::where('rm_broker_id', $key->rm_broker_id)->where('st_product_id', $key->st_product_id)->where('status', 1)->where('withdraw_date', '>=', $key->start_date)->sum('amount');
                $sum_bk[$key->bk_name][$key->pd_name]['rt'] = StReturn::where('rm_broker_id', $key->rm_broker_id)->where('st_product_id', $key->st_product_id)->where('status', 1)->where('return_date', '>=', $key->start_date)->sum('amount');
                $sum_bk[$key->bk_name][$key->pd_name]['ch'] = StChange::where('rm_broker_id', $key->rm_broker_id)->where('st_product_id', $key->st_product_id)->where('status', 1)->where('change_date', '>=', $key->start_date)->sum('amount');
                $sum_bk[$key->bk_name][$key->pd_name]['st'] = $key->amount;
            }
            foreach ($sum_bk as $kbk=>$vbk) {
                foreach ($vbk as $kpd=>$vpd) {
                    $sum_bk[$kbk][$kpd]['tt'] =$sum_bk[$kbk][$kpd]['st'] + $sum_bk[$kbk][$kpd]['wd'] - $sum_bk[$kbk][$kpd]['rt'];
                }
            }
        }
        
        return view('st_wh.main.index',compact('sum_wh','sum_bk'));
    }
}
