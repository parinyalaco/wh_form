<?php

namespace App\Http\Controllers\WHRM\setting;

use App\Http\Controllers\Controller;
use App\Models\RmArea;
use App\Models\RmBroker;
use App\Models\RmBrokerAears;
use Illuminate\Http\Request;

class WhRmBrokerAreaController extends Controller
{
    public function index()
    {
        $perpage = 20;
        $rm_broker_areas = RmBrokerAears::orderBy('created_at', 'DESC')
            ->paginate($perpage);

        $rmBrokers = RmBroker::whereDoesntHave('rmBrokerAreas')->pluck('name', 'id');
        $rmAears = RmArea::pluck('name', 'id');

        $brokers = RmBroker::pluck('name', 'id');
        $areas = RmArea::pluck('name', 'id');

        return view('TEST.setting.index', compact(
            'rm_broker_areas',
            'rmBrokers',
            'rmAears',
            'brokers',
            'areas',
        ));
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        RmBrokerAears::create([
            'rm_broker_id' => $request->broker,
            'rm_area_id' => $request->area,
        ]);

        return redirect()->back()->with('success', 'บันทึกสำเร็จ ');
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
        RmBrokerAears::findOrFail($id)->update([
            'rm_broker_id' => $request->broker,
            'rm_area_id' => $request->area,
        ]);
        return redirect()->back()->with('success', 'แก้ไขสำเร็จ ');
    }

    public function destroy($id)
    {
        //
    }


    public function whrmSettingAddBroker(Request $request)
    {
        RmBroker::create(['name' => $request->name]);
        return redirect()->back()->with('success', 'เพิ่ม Broker สำเร็จ ');
    }


    public function whrmSettingAddArea(Request $request)
    {
        RmArea::create(['name' => $request->name]);
        return redirect()->back()->with('success', 'เพิ่ม Area สำเร็จ ');
    }
}
