<?php

namespace App\Http\Controllers\WHRM;

use App\Http\Controllers\Controller;
use App\Models\RmIssues;
use App\Models\RmIssuesDaily;
use App\Models\RmIssuesDailyImgs;
use App\Models\RmIssuesImgs;
use App\Models\RmManual;
use App\Models\RmSap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;


class WhRmDeleteController extends Controller
{
    public function delete_rm($manual_date, Request $request)
    {
        $rm_manuals = RmManual::where('manual_date', $manual_date)->get();
        if (count($rm_manuals) > 0) {

            foreach ($rm_manuals as $rm_manual) {

                $rmSaps = RmSap::where('sap_no', $rm_manual->sap_no)->get();

                if (count($rmSaps) > 0) {
                    foreach ($rmSaps as $rmSap) {
                        $rmSap->delete();
                    }
                }
                if (count($rm_manual->rm_issues) > 0) {
                    foreach ($rm_manual->rm_issues as $rm_issue) {
                        $rm_issue->delete();
                    }
                }
                $rm_manual->delete();
            }

            return redirect()->back()->with('success', 'ลบ สำเร็จ');
        } else {
            return redirect()->back()->with('error', 'ไม่พบข้อมูลที่ต้องการลบ');
        }
    }


    public function delete_issue($manual_date, $sap_no, Request $request)
    {
        $rmManuals = RmManual::where('manual_date', $manual_date)
            ->where('sap_no', $sap_no)->get();

        foreach ($rmManuals as $rmManual) {
            $checkIssues = RmIssues::where('sap_no', $rmManual->sap_no)->get();

            if (count($checkIssues) > 0) {
                foreach ($checkIssues as $checkIssue) {
                    $this->delete_issue_img($checkIssue->id);
                    $checkIssue->delete();
                }
            }
        }
        return redirect()->back()->with('success', 'ลบ สำเร็จ');
    }

    public function delete_issue_img($rmIssuesId)
    {
        $rmIssuesImgs = RmIssuesImgs::where('rm_issue_id', $rmIssuesId)->get();

        if (count($rmIssuesImgs) > 0) {
            foreach ($rmIssuesImgs as $rmIssuesImg) {
                if (File::exists($rmIssuesImg->path)) {
                    File::delete($rmIssuesImg->path);
                    $rmIssuesImg->delete();
                }
            }
        }
    }

    public function delete_daily_issue($manual_date, $rmMat, Request $request)
    {
        $issue_Daily = RmIssuesDaily::where('issue_date', $manual_date)->where('raw_material_id', $rmMat)->first();

        if ($issue_Daily) {
            // ลบรูปภาพที่เกี่ยวข้อง
            $this->delete_daily_issue_img($issue_Daily->id);

            // ลบข้อมูลของ issue_Daily
            $issue_Daily->delete();

            return redirect()->back()->with('success', 'ลบสำเร็จ');
        } else {
            return redirect()->back()->with('error', 'ไม่พบข้อมูลที่ต้องการลบ');
        }
    }
    public function delete_daily_issue_img($rmIssueDaily_id)
    {
        $rmIssuesDailyImgs = RmIssuesDailyImgs::where('rm_issue_daily_id', $rmIssueDaily_id)->get();

        if (count($rmIssuesDailyImgs) > 0) {
            foreach ($rmIssuesDailyImgs as $rmIssuesDailyImg) {
                if (File::exists($rmIssuesDailyImg->path)) {
                    File::delete($rmIssuesDailyImg->path);
                    $rmIssuesDailyImg->delete();
                }
            }
        }
    }
}
