<?php

namespace App\Http\Controllers\WHRM;

use App\Http\Controllers\Controller;
use App\Http\Controllers\RmManualController;
use App\Models\Crop;
use App\Models\RmIssuesDaily;
use App\Models\RmIssuesDailyImgs;
use App\Models\RmIssuesImgs;
use App\Models\RmManual;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Image;

class WhRmController extends Controller
{
    protected $rmManualController;

    public function __construct(RmManualController $rmManualController)
    {
        $this->rmManualController = $rmManualController;
    }

    public function index(Request $request)
    {
        $perpage = 50;

        if (!empty($request->date_from)) {
            $date_from = $request->date_from;
            $date_to = $request->date_to;
        } else {

            $date_manual = RmManual::with('rm_sap')
                ->whereDoesntHave('rm_sap.raw_material', function ($query) {
                    $query->where('name', 'LIKE', '%BANANA%');
                })
                ->orderBy('manual_date', 'DESC')->orderBy('manual_no', 'ASC')
                ->limit(1)
                ->value('manual_date');


            if (!empty($date_manual)) {
                $date_to = date('Y-m-d', strtotime($date_manual));
                $date_from = date("Y-m-d", strtotime("-5 day", strtotime($date_to)));
            } else {
                $date_to = date("Y-m-d");
                $date_from = date("Y-m-d");
            }
        }

        $rm_manuals = RmManual::with('rm_sap')
            ->whereBetween('manual_date', [$date_from, $date_to])
            ->whereDoesntHave('rm_sap.raw_material', function ($query) {
                $query->where('name', 'Like', '%BANANA%');
            })
            ->orderBy('manual_date', 'ASC')->orderBy('sap_no', 'ASC')
            ->paginate($perpage, ['*'], 'rm_manual_bean_page');

        $bean_issue_dailys = RmIssuesDaily::whereDoesntHave('raw_material', function ($query) {
            $query->where('name', 'LIKE', '%BANANA%');
        })
            ->whereBetween('issue_date', [$date_from, $date_to])->get();
        // +======================================= BANANA =======================================+

        if (!empty($request->banana_date_from)) {
            $banana_date_from = $request->banana_date_from;
            $banana_date_to = $request->banana_date_to;
        } else {
            $banana_date_manual = RmManual::with('rm_sap')
                ->whereHas('rm_sap.raw_material', function ($query) {
                    $query->where('name', 'LIKE', '%BANANA%');
                })
                ->orderBy('manual_date', 'DESC')->orderBy('sap_no', 'ASC')
                ->limit(1)
                ->value('manual_date');

            if (!empty($banana_date_manual)) {
                $banana_date_to =  date("Y-m-d", strtotime($banana_date_manual));
                $banana_date_from = date("Y-m-d", strtotime("-5 day", strtotime($banana_date_to)));
            } else {
                $banana_date_to = date("Y-m-d");
                $banana_date_from = date("Y-m-d");
            }
        }

        $rm_manual_banananas = RmManual::with('rm_sap')
            ->whereBetween('manual_date', [$banana_date_from, $banana_date_to])
            ->whereHas('rm_sap.raw_material', function ($query) use ($banana_date_from, $banana_date_to) {
                $query->where('name', 'LIKE', '%BANANA%');
            })
            ->orderBy('manual_date', 'ASC')->orderBy('sap_no', 'ASC')
            ->paginate($perpage, ['*'], 'rm_manual_banana_page');

        $banana_issue_dailys = RmIssuesDaily::whereHas('raw_material', function ($query) {
            $query->where('name', 'LIKE', '%BANANA%');
        })
            ->whereBetween('issue_date', [$banana_date_from, $banana_date_to])->get();

        $crops = Crop::get();


        return view('TEST.index', compact(
            'date_from',
            'date_to',
            'rm_manuals',
            'rm_manual_banananas',
            'banana_date_from',
            'banana_date_to',
            'crops',
            'bean_issue_dailys',
            'banana_issue_dailys'
        ));
    }

    public function show_issue_img($fileName)
    {
        if (!File::exists($fileName)) {
            abort(404);
        }

        $file = File::get($fileName);
        $type = File::mimeType($fileName);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }

    public function delete_issue_img($rmIssuesImgId)
    {
        $rmIssuesImg = RmIssuesImgs::findOrFail($rmIssuesImgId);

        if (File::exists($rmIssuesImg->path)) {
            File::delete($rmIssuesImg->path);

            $rmIssuesImg->delete();

            return redirect()->back()->with('success', 'ลบรูปภาพเรียบร้อยแล้ว');
        } else {
            return redirect()->back()->with('error', 'ไม่สามารถลบรูปภาพได้');
        }
    }


    public function createIssueDaily($manual_date, $rawMat, Request $request)
    {

        RmIssuesDaily::create([
            'issue_date' => $manual_date,
            'raw_material_id' => $rawMat,
            'detail' => $request->detail,
        ]);

        return redirect()->back()->with('success', 'บันทึกสำเร็จ');
    }

    public function checkIssueDaily($issueDate, $raw_material_id)
    {
        $exists = RmIssuesDaily::where('issue_date', $issueDate)->where('raw_material_id', $raw_material_id)->exists();

        return response()->json(['exists' => !$exists]);
    }


    public function upload_issue_daily_img($issueDailyId, Request $request)
    {
        $storage_path = config('myconfig.directory.whrm.completes.img');
        $fileName = 'issue_img';

        if ($request->hasFile($fileName)) {
            $tmpfolder = date('Ymd');
            if (!is_dir($storage_path . '/' . $tmpfolder)) {
                mkdir($storage_path . '/' . $tmpfolder, 0777, true);
            }

            foreach ($request->file($fileName) as $key => $image) {
                $newImageName = time() . $key . '.' . $image->getClientOriginalExtension();

                $resizedImage = Image::make($image)->resize(null, 300, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $destinationPath = $storage_path . "/" . $tmpfolder;
                $path = $destinationPath . "/" . $newImageName;
                $resizedImage->save($path);
                RmIssuesDailyImgs::create([
                    'rm_issue_daily_id' => $issueDailyId,
                    'path' => $path,
                ]);
            }
        }
        return redirect()->back()->with('success', 'บันทึกรูปสำเร็จ');
    }

    public function delete_issue_daily_img($rmIssuesImgId)
    {
        $rmIssuesImg = RmIssuesDailyImgs::findOrFail($rmIssuesImgId);

        if (File::exists($rmIssuesImg->path)) {
            File::delete($rmIssuesImg->path);

            $rmIssuesImg->delete();

            return redirect()->back()->with('success', 'ลบรูปภาพเรียบร้อยแล้ว');
        } else {
            return redirect()->back()->with('error', 'ไม่สามารถลบรูปภาพได้');
        }
    }

    public function edit_issue_daily_detail($issueDaily_id, Request $request)
    {
        $rmIssueDaily = RmIssuesDaily::findOrFail($issueDaily_id);

        $rmIssueDaily->update(['detail' => $request->detail]);

        return redirect()->back()->with('success', 'แก้ไข สำเร็จ');
    }
}
