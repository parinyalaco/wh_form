<?php

namespace App\Http\Controllers\WHRM;

use App\Http\Controllers\Controller;
use App\Http\Controllers\RmManualController;
use App\Models\RmIssues;
use App\Models\RmIssuesDaily;
use App\Models\RmManual;
use App\Models\RmSap;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;

class WhRmReportController extends Controller
{

    public function sumaryReport($rmMat, Request $request)
    {

        $rmManualController = new RmManualController;
        $report_id = $request->report_id;
        $sent_date = date("Y-m-d", strtotime($request->st_date));
        if ($report_id == 1) {
            $reports = $this->reportsData($rmMat, $sent_date);
            if ($reports) {

                // dd($reports);

                return view('TEST.Reports.sumary_report.index', compact(
                    'reports',
                    'sent_date',
                    'rmMat',
                ));
            } else {
                return redirect()->back()->with('error', 'ไม่มีข้อมูล');
            }
        }
        if ($report_id == 2) {
            $requestData = $request->all();
            return $this->export_manual($rmMat, $requestData);
        }
        if ($report_id == 3) {
            return $rmManualController->report_export($request);
        }
    }
    public function reportsData($rmMat, $sent_date)
    {
        $reports = [];
        $rmSaps = RmSap::whereDate('sap_date', $sent_date)->count();

        if ($rmSaps > 0) {
            if ($rmMat == 'banana') {
                $BananaRmSaps = RmSap::whereDate('sap_date', $sent_date)
                    ->whereHas('raw_material', function ($query) {
                        $query->where('name', 'LIKE', '%BANANA%');
                    })
                    ->get();

                if (count($BananaRmSaps) > 0) {
                    $banana_report = $this->generateReport($rmMat, $BananaRmSaps);
                    $reports['banana'] = $banana_report;
                } else {
                    return redirect()->back()->with('error', 'ไม่มีข้อมูล');
                }
            }
            if ($rmMat == 'bean') {
                $BeanRmSaps = RmSap::whereDate('sap_date', $sent_date)
                    ->whereDoesntHave('raw_material', function ($query) {
                        $query->where('name', 'LIKE', '%BANANA%');
                    });
                // ->get();

                if ($BeanRmSaps->count() > 0) {
                    $bean_report = $this->generateReport($rmMat, $BeanRmSaps);
                    $reports['bean'] = $bean_report;
                } else {
                    return redirect()->back()->with('error', 'ไม่มีข้อมูล');
                }
            }
            return $reports;
        }
    }
    public function generateReport($rmMat, $rmSaps)
    {
        $filteredSaps = $rmSaps->get();
        $report = [];
        switch ($rmMat) {
            case 'banana':
                $report['type'] = 'กล้วย';
                break;
            case 'bean':
                $report['type'] = 'ถั่ว';
                break;
        }

        $report['totelWeight'] =   $filteredSaps->sum('rm_weight');
        $report['car_in'] = count($filteredSaps->groupBy('car_name'));

        foreach ($filteredSaps->groupBy('sap_no') as $sap_no => $filteredSaps) {
            $rmManual = RmManual::where('sap_no', $sap_no)->first();
            $time = date('H:i:s', strtotime($rmManual->time_to_laco));
            $finishTime = date('H:i:s', strtotime($rmManual->time_end_work));

            if ($time > '06:00:00') {
                if (!isset($report['first']) || $time < $report['first']) {
                    $report['first'] = $time;
                }
                if (!isset($report['last']) || $time > $report['last']) {
                    $report['last'] = $time;
                }
            }

            if ($finishTime > '06:00:00') {
                if (!isset($finishBeforeMid) || $finishTime > $finishBeforeMid) {
                    $finishBeforeMid = $finishTime;
                }
            }

            if ($finishTime < '06:00:00') {
                if (!isset($finishAfterMid) || $finishTime > $finishAfterMid) {
                    $finishAfterMid = $finishTime;
                }
            }
        }

        if (!empty($finishAfterMid)) {
            $report['finish'] = $finishAfterMid;
        } else {
            $report['finish'] = $finishBeforeMid;
        }


        $rmManual = RmManual::where('sap_no', $sap_no)->first();

        if ($rmMat == 'banana') {
            $rmManual = RmManual::where('sap_no', $sap_no)
                ->whereHas('rm_sap.raw_material', function ($query) {
                    $query->where('name', 'LIKE', '%BANANA%');
                })->first();
        }
        if ($rmMat == 'bean') {
            $rmManual = RmManual::where('sap_no', $sap_no)
                ->whereDoesntHave('rm_sap.raw_material', function ($query) {
                    $query->where('name', 'LIKE', '%BANANA%');
                })->first();
        }



        $entryTime = $this->calculateEntryTime($rmSaps->get());
        $typeReceive = $this->calculateTypeReceive($rmSaps->get());
        $issue = $this->calculateIssue($rmSaps->get());
        $dauilyIssue = $this->calculateDailyIssue($rmManual, $rmMat);

        $report['entryTime'] = $entryTime;
        $report['typeReceive'] = $typeReceive;
        $report['issue'] = $issue;
        $report['dailyIssue'] = $dauilyIssue;

        return $report;
    }
    public function calculateEntryTime($rmSaps)
    {

        $rawData = [];
        foreach ($rmSaps->groupBy('sap_no') as $sap_no => $rmSapsGroup) {
            $rmManual = RmManual::where('sap_no', $sap_no)->first();

            $tmp = [];
            $time = date('H:i:s', strtotime(substr($rmManual->time_to_laco, 0, 8)));
            $nextHour = date('H:00', strtotime($time . ' +1 hour'));

            if ($nextHour < '06:00') {
                $tmp['time'] = 'หลัง 00:00';
                $tmp['weight'] = $rmManual->manual_weight;
            } else {
                $tmp['time'] = 'ก่อน' . $nextHour;
                $tmp['weight'] = $rmManual->manual_weight;
            }

            $rawData[] = $tmp;
        }
        $collection = collect($rawData);

        $grouped = $collection->groupBy('time')->map(function ($group) {
            return [
                'count' => $group->count(),
                'weight' => $group->sum('weight'),
            ];
        })->sortKeys();

        $totalCount = $collection->count();
        $totalWeight = $collection->sum('weight');

        $result = $grouped->toArray();
        $result['Grand Total']['count'] = $totalCount;
        $result['Grand Total']['weight'] =  $totalWeight;


        return $result;
    }
    public function calculateTypeReceive($rmSaps)
    {
        $typeReceive = [
            'พนักงานยก' => ['<10' => 0, '<15' => 0, '<20' => 0, '>20' => 0],
            'เครื่องเรียงกระสอบ' => ['<10' => 0, '<15' => 0, '<20' => 0, '>20' => 0],
            'total' => ['<10' => 0, '<15' => 0, '<20' => 0, '>20' => 0],
        ];
        foreach ($rmSaps->groupBy('sap_no') as $sap_no => $rmSaps) {
            $rmManual = RmManual::where('sap_no', $sap_no)->first();

            $time_use = date('H:i:s', strtotime(substr($rmManual->time_use, 0, 8)));

            if ($rmManual->rm_type_receive) {
                if ($rmManual->rm_type_receive->name == 'พนักงานยก') {
                    if ($time_use < '00:10:00') {
                        $typeReceive['พนักงานยก']['<10'] += 1;
                    } elseif ($time_use >= '00:10:00' && $time_use < '00:15:00') {
                        $typeReceive['พนักงานยก']['<15'] += 1;
                    } elseif ($time_use >= '00:15:00' && $time_use < '00:20:00') {
                        $typeReceive['พนักงานยก']['<20'] += 1;
                    } elseif ($time_use >= '00:20:00') {
                        $typeReceive['พนักงานยก']['>20'] += 1;
                    }
                }
                if ($rmManual->rm_type_receive->name == 'เครื่องเรียง') {
                    if ($time_use < '00:10:00') {
                        $typeReceive['เครื่องเรียงกระสอบ']['<10'] += 1;
                    } elseif ($time_use >= '00:10:00' && $time_use < '00:15:00') {
                        $typeReceive['เครื่องเรียงกระสอบ']['<15'] += 1;
                    } elseif ($time_use >= '00:15:00' && $time_use < '00:20:00') {
                        $typeReceive['เครื่องเรียงกระสอบ']['<20'] += 1;
                    } elseif ($time_use >= '00:20:00') {
                        $typeReceive['เครื่องเรียงกระสอบ']['>20'] += 1;
                    }
                }
                if ($time_use < '00:10:00') {
                    $typeReceive['total']['<10'] += 1;
                } elseif ($time_use >= '00:10:00' && $time_use < '00:15:00') {
                    $typeReceive['total']['<15'] += 1;
                } elseif ($time_use >= '00:15:00' && $time_use < '00:20:00') {
                    $typeReceive['total']['<20'] += 1;
                } elseif ($time_use >= '00:20:00') {
                    $typeReceive['total']['>20'] += 1;
                }
            }
        }

        return $typeReceive;
    }
    public function calculateIssue($rmSaps)
    {
        $issue = [];
        foreach ($rmSaps->groupBy('sap_no') as $sap_no => $rmSaps) {
            $rmManual = RmManual::where('sap_no', $sap_no)->first();
            $rmIssues = RmIssues::where('sap_no', $rmManual->sap_no)->get();
            if (count($rmIssues) > 0) {
                $tmpIssues = [];
                foreach ($rmIssues as $rm_issue) {
                    $tmpIssue = [];
                    $tmpIssue['detail'] = $rm_issue->detail;
                    foreach ($rm_issue->rm_issues_imgs as $key => $rm_issues_img) {
                        $tmpIssue['imgs'][$key] = $rm_issues_img->path;
                    }

                    $tmpIssues[] = $tmpIssue;
                }
                $issue[] = [
                    'sap_no' => $rmManual->sap_no,
                    'car_name' => $rmManual->car_name,
                    'sub_issues' => $tmpIssues,
                ];
            }
        }


        return $issue;
    }
    public function calculateDailyIssue($rmManual, $rmMat)
    {
        $dailyissue = [];
        if ($rmMat == 'banana') {
            $rm_daily_issue = RmIssuesDaily::where('issue_date', $rmManual->manual_date)
                ->whereHas('raw_material', function ($query) {
                    $query->where('name', 'LIKE', '%BANANA%');
                })
                ->first();
        } else if ($rmMat == 'bean') {
            $rm_daily_issue = RmIssuesDaily::where('issue_date', $rmManual->manual_date)
                ->whereDoesntHave('raw_material', function ($query) {
                    $query->where('name', 'LIKE', '%BANANA%');
                })
                ->first();
        }
        if ($rm_daily_issue) {
            $dailyissue['detail'] = $rm_daily_issue->detail;
            foreach ($rm_daily_issue->rm_issues_daily_imgs as $key => $rm_issues_daily_img) {
                $dailyissue['imgs'][$key] = $rm_issues_daily_img->path;
            }
            $dailyissue['issue_date'] =  $rmManual->manual_date;
        }
        return $dailyissue;
    }
    public function export_manual($rmMat, $requestData)
    {
        $date_from = date("Y-m-d", strtotime($requestData['st_date']));
        $date_to = date("Y-m-d", strtotime($requestData['ed_date']));
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValueByColumnAndRow(1, 1, "กรุณาระบุชื่อ crop ในช่องสี เหลือง");
        $sheet->mergeCellsByColumnAndRow(1, 1, 3, 1);
        $sheet->mergeCellsByColumnAndRow(4, 1, 6, 1);
        $sheet->getStyle('D1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF33');
        $sheet->setCellValueByColumnAndRow(1, 2, "วันที่");
        $sheet->setCellValueByColumnAndRow(2, 2, "ลำดับที่");
        $sheet->setCellValueByColumnAndRow(3, 2, "sap_no");
        $sheet->setCellValueByColumnAndRow(4, 2, "raw_material");
        $sheet->setCellValueByColumnAndRow(5, 2, "รถทะเบียน");
        $sheet->setCellValueByColumnAndRow(6, 2, "จำนวน");
        $sheet->setCellValueByColumnAndRow(7, 2, "น้ำหนัก");
        $sheet->setCellValueByColumnAndRow(8, 2, "เวลารถถึง LACO");
        $sheet->setCellValueByColumnAndRow(9, 2, "เขต");
        $sheet->setCellValueByColumnAndRow(10, 2, "พื้นที่เขต");
        $sheet->setCellValueByColumnAndRow(11, 2, "เวลารถออกจากพื้นที่");
        $sheet->setCellValueByColumnAndRow(12, 2, "เวลาลงวัตถุดิบ");
        $sheet->setCellValueByColumnAndRow(13, 2, "ลงเสร็จ");
        $sheet->setCellValueByColumnAndRow(14, 2, "จำนวนชั่วโมง");
        $sheet->setCellValueByColumnAndRow(15, 2, "จำนวนพนง.");
        $sheet->setCellValueByColumnAndRow(16, 2, "ประเภทการลง");
        $sheet->setCellValueByColumnAndRow(17, 2, "หมายเหตุ");
        $sheet->setCellValueByColumnAndRow(18, 2, "ปัญหา");
        $sheet->getStyle('A2:R2')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A2:R2')->getAlignment()->setVertical('center');
        $sheet->getStyle('A2:R2')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');
        if ($rmMat == 'banana') {
            $rmManuals = RmManual::with('rm_sap')
                ->whereBetween('manual_date', [$date_from, $date_to])
                ->whereHas('rm_sap.raw_material', function ($query) {
                    $query->where('name', 'LIKE', '%BANANA%');
                })
                ->orderBy('manual_date', 'ASC')
                ->orderBy('time_to_laco', 'ASC')
                ->get();
        } else if ($rmMat == 'bean') {
            $rmManuals = RmManual::with('rm_sap')
                ->whereBetween('manual_date', [$date_from, $date_to])
                ->whereDoesntHave('rm_sap.raw_material', function ($query) {
                    $query->where('name', 'LIKE', '%BANANA%');
                })
                ->orderBy('manual_date', 'ASC')
                ->orderBy('time_to_laco', 'ASC')
                ->get();
        } else {
            $rmManuals = RmManual::with('rm_sap')
                ->whereBetween('manual_date', [$date_from, $date_to])
                ->whereHas('rm_sap', function ($query) use ($date_from, $date_to) {
                })
                ->orderBy('manual_date', 'ASC')
                ->orderBy('time_to_laco', 'ASC')
                ->get();
        }

        $currentDate = null;
        $i = 1;
        $startrow = 3;

        foreach ($rmManuals as $rmManual) {
            if ($rmManual->manual_date != $currentDate && is_null($rmManual->manual_no)) {
                $currentDate = $rmManual->manual_date;
                $i = 1;
            }
            $manual_no = $i++;
            $startcol = 1;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->manual_date);
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $manual_no);
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->sap_no ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->rm_sap->raw_material->name ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->car_name);
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->manual_num);
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->manual_weight);
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, date('H:i:s', strtotime($rmManual->time_to_laco)));
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->rm_area_code ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->rm_area->name ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, !empty($rmManual->time_out_area) ? date('H:i:s', strtotime($rmManual->time_out_area)) : '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, !empty($rmManual->time_start_work) ? date('H:i:s', strtotime($rmManual->time_start_work)) : '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, !empty($rmManual->time_end_work) ? date('H:i:s', strtotime($rmManual->time_end_work)) : '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, !empty($rmManual->time_use) ? date('H:i:s', strtotime($rmManual->time_use)) : '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->staff_use ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->rm_type_receive->name ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->note ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->rm_issue->detail ?? '');
            $startrow++;
        }
        $sheet->getStyle('A2:R' . ($startrow - 1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
        $sheet->getStyle('A3:H' . ($startrow - 1))->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('DCE6F1');
        for ($i = 1; $i <= 18; $i++) {
            $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($i);
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        $writer = new Xlsx($spreadsheet);
        $filename = "rm_manual-" . date('yymmdd-hi') . ".xlsx";
        $writer->save('storage/' . $filename);
        return response()->download('storage/' . $filename)->deleteFileAfterSend(true);
    }
}
