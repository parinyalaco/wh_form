<?php

namespace App\Http\Controllers\WHRM;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\WHRM\WhrmReportMail;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class WhRmEmailController extends Controller
{

    protected $whRmReportController;

    public function __construct(WhRmReportController $whRmReportController)
    {
        $this->whRmReportController = $whRmReportController;
    }

    public function example_mail($rmMat, Request $request)
    {

        $sent_date = date("Y-m-d", strtotime($request->sent_date));

        $reports = $this->whRmReportController->reportsData($rmMat, $sent_date);
        if ($reports) {
            return view('TEST.Reports.email.index', compact(
                'reports',
                'sent_date',
                'rmMat',
            ));
        } else {
            return redirect()->back()->with('error', 'ไม่มีข้อมูล');
        }
    }

    public function send_mail($rmMat, $sent_date)
    {
        $reports = [];
        $chartPath = null;

        if ($rmMat == 'banana') {
            $reports = $this->whRmReportController->reportsData($rmMat, $sent_date);
            $chartBananaData = [
                'พนักงานยก' => array_values($reports['banana']['typeReceive']['พนักงานยก']),
                'เครื่องเรียงกระสอบ' => array_values($reports['banana']['typeReceive']['เครื่องเรียงกระสอบ']),
                'total' => array_values($reports['banana']['typeReceive']['total']),
            ];

            $bananaLabels = array_keys($reports['banana']['typeReceive']['พนักงานยก']);
            $chartPath = $this->generateChartImage($chartBananaData, $bananaLabels);
            if (!empty($reports['banana']['issue'])) {
                $reports['banana']['issue'] = $this->updateIssue($reports['banana']['issue']);
            }

            if (!empty($reports['banana']['dailyIssue'])) {
                $reports['banana']['dailyIssue'] = $this->updateIssueDaily($reports['banana']['dailyIssue']);
            }
        } else if ('bean') {
            $reports = $this->whRmReportController->reportsData($rmMat, $sent_date);
            $chartBeanData = [
                'พนักงานยก' => array_values($reports['bean']['typeReceive']['พนักงานยก']),
                'เครื่องเรียงกระสอบ' => array_values($reports['bean']['typeReceive']['เครื่องเรียงกระสอบ']),
                'total' => array_values($reports['bean']['typeReceive']['total']),
            ];
            $beanLabels = array_keys($reports['bean']['typeReceive']['พนักงานยก']);
            $chartPath = $this->generateChartImage($chartBeanData, $beanLabels);

            if (!empty($reports['bean']['issue'])) {
                $reports['bean']['issue'] = $this->updateIssue($reports['bean']['issue']);
            }

            if (!empty($reports['bean']['dailyIssue'])) {
                $reports['bean']['dailyIssue'] = $this->updateIssueDaily($reports['bean']['dailyIssue']);
            }
        }


        Mail::send(new WhrmReportMail($reports, $sent_date, $chartPath));

        return redirect()->back()->with('success', 'ส่ง E-Mail สำเร็จ');
    }

    function generateChartImage($chartData, $labels)
    {
        $maxYValue = max(
            max($chartData['พนักงานยก']),
            max($chartData['เครื่องเรียงกระสอบ']),
            max($chartData['total'])
        ) + 2;

        $options = [
            'responsive' => true,
            'plugins' => [
                'datalabels' => [
                    'anchor' => 'end',
                    'align' => 'top',
                    'formatter' => '(value, context) => value',
                    'font' => [
                        'weight' => 'bold'
                    ]
                ],
            ],
            'scales' => [
                'y' => [
                    'beginAtZero' => true,
                    'suggestedMax' => $maxYValue
                ]
            ],
            'legend' => [
                'display' => true,
                'position' => 'bottom'
            ],
        ];

        $chartConfig = [
            'type' => 'bar',
            'data' => [
                'labels' => $labels,
                'datasets' => [
                    [
                        'label' => 'พนักงานยก',
                        'data' => $chartData['พนักงานยก'],
                        'backgroundColor' => '#F6BC96',
                        'borderColor' => '#000000',
                    ],
                    [
                        'label' => 'เครื่องเรียงกระสอบ',
                        'data' => $chartData['เครื่องเรียงกระสอบ'],
                        'backgroundColor' => '#B1D3F5',
                        'borderColor' => '#000000',
                    ],
                    [
                        'label' => 'total',
                        'data' => $chartData['total'],
                        'backgroundColor' => '#eed6fc',
                        'borderColor' => '#000000',
                    ]
                ]
            ],
            'options' => $options,
            'plugins' => ['ChartDataLabels'],
        ];

        $response = Http::get('https://quickchart.io/chart', [
            'c' => json_encode($chartConfig),
            'width' => 400,
            'height' => 250,
        ]);

        if (!Storage::exists('public/chart')) {
            Storage::makeDirectory('public/chart');
        }
        $time = date('his');
        $imagePath = storage_path('app/public/chart/' . $time . 'chart.png');
        $path = 'app/public/chart/' . $time . 'chart.png';

        file_put_contents($imagePath, $response->body());

        return $path;
    }


    public function updateIssue($issues)
    {

        $updateData = [];
        foreach ($issues as $issue) {
            $tmp = [];
            $tmp['sap_no'] =  $issue['sap_no'];
            $tmp['car_name'] =  $issue['car_name'];

            foreach ($issue['sub_issues'] as $sub_issue) {
                $tmp_sub = [];

                $tmp_sub['detail'] =  $sub_issue['detail'];

                foreach ($sub_issue['imgs'] as  $img) {

                    if (!File::exists($img)) {
                        $tmp_sub['imgs'][] = null;

                        continue; // ข้ามการประมวลผลของรูปภาพนี้และไปยังรูปภาพถัดไป
                    } else {

                        $file = File::get($img);
                        $time = uniqid();


                        if (!Storage::exists('public/tmpImg')) {
                            Storage::makeDirectory('public/tmpImg');
                        }

                        $imagePath = storage_path('app/public/tmpImg/' . $time . 'tmpimg.png');
                        $path = 'app/public/tmpImg/' . $time . 'tmpimg.png';

                        $resizedImage = Image::make($file)->resize(400, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });


                        $resizedImage->save($imagePath);

                        $tmp_sub['imgs'][] = $path;
                    }
                }
                $tmp['sub_issues'][] = $tmp_sub;
            }


            $updateData[] = $tmp;
        }


        return $updateData;
    }

    public function updateIssueDaily($dailyIssue)
    {


        $updateData = [];
        $updateData['detail'] = $dailyIssue['detail'];
        $updateData['issue_date'] = $dailyIssue['issue_date'];
        $imgData = [];
        foreach ($dailyIssue['imgs'] as $key => $img) {

            if (!File::exists($img)) {
                $updateData['imgs'][] = null;

                continue; // ข้ามการประมวลผลของรูปภาพนี้และไปยังรูปภาพถัดไป
            } else {
                $file = File::get($img);
                $time = uniqid();
                if (!Storage::exists('public/tmpImg')) {
                    Storage::makeDirectory('public/tmpImg');
                }

                $imagePath = storage_path('app/public/tmpImg/' . $time . 'tmpimg.png');
                $path = 'app/public/tmpImg/' . $time . 'tmpimg.png';

                $resizedImage = Image::make($file)->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                });


                $resizedImage->save($imagePath);

                $imgData[] = $path;
            }
        }

        $updateData['imgs'] = $imgData;

        return $updateData;
    }
}
