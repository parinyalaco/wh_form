<?php

namespace App\Http\Controllers\WHRM;

use App\Http\Controllers\Controller;
use App\Models\RawMaterial;
use App\Models\RmFarmer;
use App\Models\Unit;
use App\Models\RmTypeReceive;
use App\Models\RmManual;
use App\Models\RmSap;
use App\Models\FileExcel;
use App\Models\RmArea;
use App\Models\RmBroker;
use App\Models\RmBrokerAears;
use App\Models\RmIssues;
use App\Models\RmTypeCar;

use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use SimpleXLSX;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;


class WhRmImportFileController extends Controller
{
    public function import_sap(Request $request)
    {
        $completedirectory = config('myconfig.directory.whrm.completes.sap');

        if ($request->hasFile('file_sap')) {
            $tmpfolder = date('Ymd');
            if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
            }

            $zipfile = $request->file('file_sap');
            $uploadname = $zipfile->getClientOriginalName();
            $name = md5($zipfile->getClientOriginalName() . time()) . '-upload.' . $zipfile->getClientOriginalExtension();
            $destinationPath = $completedirectory . "/" . $tmpfolder;
            $zipfile->move($destinationPath, $name);

            $uploadpath = $completedirectory . "/" . $tmpfolder  . "/" . $name;
            $uploadfile = $destinationPath  . "/" . $name;

            if ($xlsx = SimpleXLSX::parse($uploadfile)) {

                $query = RawMaterial::all();
                foreach ($query as $key) {
                    $rm[$key->name] = $key->id;
                }

                $query = RmFarmer::join('rm_brokers', 'rm_farmers.rm_broker_id', '=', 'rm_brokers.id')
                    ->select('rm_farmers.id', 'rm_farmers.name', 'rm_farmers.rm_broker_id', 'rm_brokers.name AS broker_name')
                    ->get();

                foreach ($query as $key) {
                    $far[$key->name][$key->broker_name] = $key->id;
                    $broker[$key->broker_name] = $key->rm_broker_id;
                }

                $query = Unit::all();
                foreach ($query as $key) {
                    $unit[$key->name] = $key->id;
                }

                $query = RmTypeCar::all();
                foreach ($query as $key) {
                    $type_car[$key->car_no][$key->car_detail] = $key->id;
                }

                // ================== Check Header of excel ==================
                $check = false;

                $expected_headers = [
                    'วันที่รับ', 'ชื่อวัตถุดิบ', 'เลขที่ใบรับ', 'ทะเบียนรถ', 'Broker Name', 'Farmer name', 'จำนวน',
                    'ภาชนะ', 'นน.สุทธิ', 'Weight Package Net', 'Price total', 'Car no', 'เวลาเข้า', 'นน.รวม',
                    'Car Detail', 'Remark', 'Customer', 'Weight First', 'Weight Last', 'Weight Type',
                    'Weight Tiem In', 'Weight Time Out', 'Requester', 'Change by', 'Change date',
                    'Change time'
                ];

                $xlsx_headers = $xlsx->rows(0)[0];
                $xlsx_headers_filtered = array_filter($xlsx_headers);

                if (empty(array_diff($expected_headers, $xlsx_headers_filtered)) && empty(array_diff($xlsx_headers_filtered, $expected_headers))) {
                    $check = true;
                } else {
                    return back()->with('error', "Header Not Being Correct!");
                }

                $add_excel = [];
                $add_manual = [];
                $add_excel['name'] = $uploadname;
                $file_id = FileExcel::create($add_excel)->id;

                $add_manual = [];

                if ($check) {
                    $manualDate = null;
                    foreach ($xlsx->rows(0) as $r => $row) {
                        if ($r == 1) {
                            $manualDate = date('Y-m-d', strtotime($row[0]));
                        }

                        if ($r > 0 && $row[0]) {
                            $isNotPhetchabun = true; // ตรวจสอบว่ามาจาก เพชรบูรณ์ หรือไม่ ,true = 'ไม่เป็น' false = 'เป็น'
                            $checkBroker = RmBroker::where('name', 'LIKE', '%' . $row[4] . '%')->first();
                            if ($checkBroker) {

                                $checkBrokerArea = RmBrokerAears::where('rm_broker_id', $checkBroker->id)->first();

                                if ($checkBrokerArea) {
                                    if ($checkBrokerArea->rm_area->name == 'เพชรบูรณ์' || $checkBrokerArea->rm_area->name == 'อุทัยธานี') {
                                        $isNotPhetchabun = false;
                                    }
                                }
                            }
                            if ($isNotPhetchabun) {
                                $add_sap = [];
                                $add_sap['file_excel_id'] = $file_id;

                                // ================== RAW MATERIAL ==================
                                if (empty($rm[$row[1]])) {
                                    $add_rm['name'] = $row[1];
                                    $rm_id = RawMaterial::create($add_rm)->id;

                                    $rm[$row[1]] = $rm_id;
                                } else {
                                    $rm_id = $rm[$row[1]];
                                }
                                $add_sap['raw_material_id'] = $rm_id;

                                $add_sap['car_name'] = $row[3];

                                // ================== FARMER ==================
                                if (empty($far[$row[5]][$row[4]])) {

                                    if (empty($broker[$row[4]])) {
                                        $add_broker['name'] = $row[4];
                                        $broker_id = RmBroker::create($add_broker)->id;
                                        $broker[$row[4]] = $broker_id;
                                    } else {
                                        $broker_id = $broker[$row[4]];
                                    }

                                    $add_far['name'] = $row[5];
                                    $add_far['rm_broker_id'] = $broker_id;
                                    $far_id = RmFarmer::create($add_far)->id;
                                    $far[$row[5]][$row[4]] = $far_id;
                                } else {
                                    $far_id = $far[$row[5]][$row[4]];
                                }
                                $add_sap['rm_farmer_id'] = $far_id;

                                // ================== UNIT ==================
                                if (empty($unit[$row[7]])) {
                                    $add_unit['name'] = $row[7];
                                    $unit_id = Unit::create($add_unit)->id;
                                    $unit[$row[7]] = $unit_id;
                                } else {
                                    $unit_id = $unit[$row[7]];
                                }
                                $add_sap['unit_id'] = $unit_id;

                                $add_sap['unit_num'] = $row[6];
                                $add_sap['rm_weight'] = $row[8];
                                $add_sap['rm_weight_agv'] = $row[9];
                                $add_sap['rm_price'] = $row[10];

                                // ================== TYPE CAR ==================
                                if (empty($type_car[$row[11]][$row[14]])) {
                                    $add_tc['car_no'] = $row[11];
                                    $add_tc['car_detail'] = $row[14];
                                    $type_car_id = RmTypeCar::create($add_tc)->id;
                                    $type_car[$row[11]][$row[14]] = $type_car_id;
                                } else {
                                    $type_car_id = $type_car[$row[11]][$row[14]];
                                }
                                $add_sap['rm_type_car_id'] = $type_car_id;

                                $add_sap['time_in'] = date('H:i:s', strtotime($row[12]));
                                $add_sap['rm_weight_sum'] = $row[13];
                                $add_sap['remark'] = $row[15];
                                $add_sap['customer'] = $row[16];
                                $add_sap['rm_weight_first'] = $row[17];
                                $add_sap['rm_weight_last'] = $row[18];
                                $add_sap['weight_type'] = $row[19];
                                $add_sap['weight_time_in'] = date('H:i:s', strtotime($row[20]));
                                $add_sap['weight_time_out'] = date('H:i:s', strtotime($row[21]));
                                $add_sap['requester'] = $row[22];
                                $add_sap['change_by'] = $row[23];
                                $add_sap['change_date'] = date('Y-m-d', strtotime($row[24]));
                                $add_sap['change_time'] = date('H:i:s', strtotime($row[25]));

                                $changeDate = date('Y-m-d', strtotime($row[0]));
                                $timeIn = date('H:i:s', strtotime($row[12]));
                                $changeTime = date('H:i:s', strtotime($row[25]));
                                $sapDate = date('Y-m-d', strtotime($changeDate));
                                $sapNo = $row[2];

                                $chk_sap = RmSap::where('sap_no', $sapNo)
                                    ->where('sap_date', $sapDate)
                                    ->where('rm_farmer_id', $far_id)
                                    ->where('time_in', $timeIn)
                                    ->where('change_time', $changeTime)
                                    ->count();

                                if ($chk_sap == 0) {
                                    $rmSap = RmSap::where('sap_no', $sapNo)->latest()->first();
                                    if ($rmSap) {
                                        if ($rmSap->sap_no == $row[2]) {
                                            if ($rmSap->sap_date != $changeDate) {
                                                if (date('H:i:s', strtotime($row[12])) > '00:00:00' && date('H:i:s', strtotime($row[12])) < '06:00:00') {
                                                    $changeDate = date('Y-m-d', strtotime($row[0] . ' -1 day'));
                                                }
                                            }
                                        }
                                    } else {
                                        if (date('H:i:s', strtotime($row[12])) > '00:00:00' && date('H:i:s', strtotime($row[12])) < '06:00:00') {
                                            $changeDate = date('Y-m-d', strtotime($row[0] . ' -1 day'));
                                        }
                                    }

                                    $add_sap['sap_date'] = date('Y-m-d', strtotime($changeDate));
                                    $add_sap['sap_no'] = $row[2];

                                    if ($row[9] > 18) {
                                        RmIssues::create([
                                            'sap_no' => $row[2],
                                            'detail' => 'น้ำหนักเกิน',
                                        ]);
                                    }

                                    RmSap::create($add_sap);
                                }

                                if ($chk_sap > 0) {

                                    $add_sap['sap_no'] = $row[2];
                                    $add_sap['sap_date'] = date('Y-m-d', strtotime($changeDate));

                                    RmSap::where('sap_date', date('Y-m-d', strtotime($row[0])))->where('sap_no', $row[2])
                                        ->where('time_in', date('H:i:s', strtotime($row[12])))->update($add_sap);
                                }

                                if (empty($add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))])) {
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['manual_date'] =  date('Y-m-d', strtotime($changeDate));
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['sap_no'] = $row[2];
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['manual_num'] = $row[6];
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['manual_weight'] = $row[8];
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['car_name'] = $row[3];
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['time_to_laco'] =   date('H:i:s', strtotime($row[12]));
                                } else {
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['manual_date'] =  date('Y-m-d', strtotime($changeDate));
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['sap_no'] = $row[2];
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['manual_num'] += $row[6];
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['manual_weight'] += $row[8];
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['car_name'] = $row[3];

                                    if (
                                        date('H:i:s', strtotime($row[12])) > '06:00:00'
                                        &&  $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['time_to_laco'] > '06:00:00'
                                    ) {
                                        if (date('H:i:s', strtotime($row[12])) < $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['time_to_laco']) {
                                            $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['time_to_laco'] =   date('H:i:s', strtotime($row[12]));
                                        }
                                    } else if (
                                        date('H:i:s', strtotime($row[12])) < '06:00:00'
                                        &&  $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['time_to_laco'] < '06:00:00'
                                    ) {

                                        if (date('H:i:s', strtotime($row[12])) < $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['time_to_laco']) {
                                            $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['time_to_laco'] =   date('H:i:s', strtotime($row[12]));
                                        }
                                    }
                                }

                                $add_excel[] = $add_sap;
                            }
                        }
                    }

                    ksort($add_manual);

                    $this->store_manual($add_manual);

                    return $this->manual_export_by_import_sap($manualDate);
                } else {
                    return back()->with('error', "Header Not Being Correct!");
                }
            } else {
                return back()->with('error', "Can't read file!");
            }
        } else {
            return back()->with('error', 'ไม่ได้เพิ่ม file');
        }
    }


    public function import_sap_banana(Request $request)
    {

        $completedirectory = config('myconfig.directory.whrm.completes.sap');

        if ($request->hasFile('file_sap')) {
            $tmpfolder = date('Ymd');
            if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
            }

            $zipfile = $request->file('file_sap');
            $uploadname = $zipfile->getClientOriginalName();
            $name = md5($zipfile->getClientOriginalName() . time()) . '-upload.' . $zipfile->getClientOriginalExtension();
            $destinationPath = $completedirectory . "/" . $tmpfolder;
            $zipfile->move($destinationPath, $name);

            $uploadpath = $completedirectory . "/" . $tmpfolder  . "/" . $name;
            $uploadfile = $destinationPath  . "/" . $name;

            if ($xlsx = SimpleXLSX::parse($uploadfile)) {

                $query = RawMaterial::all();
                foreach ($query as $key) {
                    $rm[$key->name] = $key->id;
                }

                $query = RmFarmer::join('rm_brokers', 'rm_farmers.rm_broker_id', '=', 'rm_brokers.id')
                    ->select('rm_farmers.id', 'rm_farmers.name', 'rm_farmers.rm_broker_id', 'rm_brokers.name AS broker_name')
                    ->get();

                foreach ($query as $key) {
                    $far[$key->name][$key->broker_name] = $key->id;
                    $broker[$key->broker_name] = $key->rm_broker_id;
                }

                $query = Unit::all();
                foreach ($query as $key) {
                    $unit[$key->name] = $key->id;
                }

                $query = RmTypeCar::all();
                foreach ($query as $key) {
                    $type_car[$key->car_no][$key->car_detail] = $key->id;
                }

                // ================== Check Header of excel ==================
                $check = false;

                $expected_headers = [
                    'วันที่รับ', 'ชื่อวัตถุดิบ', 'เลขที่ใบรับ', 'ทะเบียนรถ', 'Broker Name', 'Farmer name', 'จำนวน',
                    'ภาชนะ', 'นน.สุทธิ', 'Weight Package Net', 'Price total', 'Car no', 'เวลาเข้า', 'นน.รวม',
                    'Car Detail', 'Remark', 'Customer', 'Weight First', 'Weight Last', 'Weight Type',
                    'Weight Tiem In', 'Weight Time Out', 'Requester', 'Change by', 'Change date',
                    'Change time'
                ];

                $xlsx_headers = $xlsx->rows(0)[0];
                $xlsx_headers_filtered = array_filter($xlsx_headers);

                if (empty(array_diff($expected_headers, $xlsx_headers_filtered)) && empty(array_diff($xlsx_headers_filtered, $expected_headers))) {
                    $check = true;
                } else {
                    return back()->with('error', "Header Not Being Correct!");
                }

                $add_excel = [];
                $add_manual = [];
                $add_excel['name'] = $uploadname;
                $file_id = FileExcel::create($add_excel)->id;

                $add_manual = [];

                if ($check) {
                    $manualDate = null;
                    foreach ($xlsx->rows(0) as $r => $row) {
                        if ($r == 1) {
                            $manualDate = date('Y-m-d', strtotime($row[0]));
                        }

                        if ($r > 0 && $row[0]) {
                            $isNotPhetchabun = true; // ตรวจสอบว่ามาจาก เพชรบูรณ์ หรือไม่ ,true = 'ไม่เป็น' false = 'เป็น'
                            $checkBroker = RmBroker::where('name', 'LIKE', '%' . $row[4] . '%')->first();
                            if ($checkBroker) {
                                $checkBrokerArea = RmBrokerAears::where('rm_broker_id', $checkBroker->id)->first();
                                if ($checkBrokerArea) {
                                    if ($checkBrokerArea->rm_area->name == 'เพชรบูรณ์' || $checkBrokerArea->rm_area->name == 'อุทัยธานี') {
                                        $isNotPhetchabun = false;
                                    }
                                }
                            }
                            if ($isNotPhetchabun) {
                                $add_sap = [];
                                $add_sap['file_excel_id'] = $file_id;

                                // ================== RAW MATERIAL ==================
                                if (empty($rm[$row[1]])) {
                                    $add_rm['name'] = $row[1];
                                    $rm_id = RawMaterial::create($add_rm)->id;

                                    $rm[$row[1]] = $rm_id;
                                } else {
                                    $rm_id = $rm[$row[1]];
                                }
                                $add_sap['raw_material_id'] = $rm_id;

                                $add_sap['car_name'] = $row[3];

                                // ================== FARMER ==================
                                if (empty($far[$row[5]][$row[4]])) {

                                    if (empty($broker[$row[4]])) {
                                        $add_broker['name'] = $row[4];
                                        $broker_id = RmBroker::create($add_broker)->id;
                                        $broker[$row[4]] = $broker_id;
                                    } else {
                                        $broker_id = $broker[$row[4]];
                                    }

                                    $add_far['name'] = $row[5];
                                    $add_far['rm_broker_id'] = $broker_id;
                                    $far_id = RmFarmer::create($add_far)->id;
                                    $far[$row[5]][$row[4]] = $far_id;
                                } else {
                                    $far_id = $far[$row[5]][$row[4]];
                                }
                                $add_sap['rm_farmer_id'] = $far_id;

                                // ================== UNIT ==================
                                if (empty($unit[$row[7]])) {
                                    $add_unit['name'] = $row[7];
                                    $unit_id = Unit::create($add_unit)->id;
                                    $unit[$row[7]] = $unit_id;
                                } else {
                                    $unit_id = $unit[$row[7]];
                                }
                                $add_sap['unit_id'] = $unit_id;

                                $add_sap['unit_num'] = $row[6];
                                $add_sap['rm_weight'] = $row[8];
                                $add_sap['rm_weight_agv'] = $row[9];
                                $add_sap['rm_price'] = $row[10];

                                // ================== TYPE CAR ==================
                                if (empty($type_car[$row[11]][$row[14]])) {
                                    $add_tc['car_no'] = $row[11];
                                    $add_tc['car_detail'] = $row[14];
                                    $type_car_id = RmTypeCar::create($add_tc)->id;
                                    $type_car[$row[11]][$row[14]] = $type_car_id;
                                } else {
                                    $type_car_id = $type_car[$row[11]][$row[14]];
                                }
                                $add_sap['rm_type_car_id'] = $type_car_id;

                                $add_sap['time_in'] = date('H:i:s', strtotime($row[12]));
                                $add_sap['rm_weight_sum'] = $row[13];
                                $add_sap['remark'] = $row[15];
                                $add_sap['customer'] = $row[16];
                                $add_sap['rm_weight_first'] = $row[17];
                                $add_sap['rm_weight_last'] = $row[18];
                                $add_sap['weight_type'] = $row[19];
                                $add_sap['weight_time_in'] = date('H:i:s', strtotime($row[20]));
                                $add_sap['weight_time_out'] = date('H:i:s', strtotime($row[21]));
                                $add_sap['requester'] = $row[22];
                                $add_sap['change_by'] = $row[23];
                                $add_sap['change_date'] = date('Y-m-d', strtotime($row[24]));
                                $add_sap['change_time'] = date('H:i:s', strtotime($row[25]));

                                $changeDate = date('Y-m-d', strtotime($row[0]));
                                $timeIn = date('H:i:s', strtotime($row[12]));
                                $changeTime = date('H:i:s', strtotime($row[25]));
                                $sapDate = date('Y-m-d', strtotime($changeDate));
                                $sapNo = $row[2];

                                $chk_sap = RmSap::where('sap_no', $sapNo)
                                    ->where('sap_date', $sapDate)
                                    ->where('rm_farmer_id', $far_id)
                                    ->where('time_in', $timeIn)
                                    ->where('change_time', $changeTime)
                                    ->count();

                                if ($chk_sap == 0) {
                                    $rmSap = RmSap::where('sap_no', $sapNo)->latest()->first();

                                    // if ($rmSap) {
                                    //     if ($rmSap->sap_no == $row[2]) {
                                    //         if ($rmSap->sap_date != $changeDate) {
                                    //             if (date('H:i:s', strtotime($row[12])) > '00:00:00' && date('H:i:s', strtotime($row[12])) < '06:00:00') {
                                    //                 $changeDate = date('Y-m-d', strtotime($row[0] . ' -1 day'));
                                    //             }
                                    //         }
                                    //     }
                                    // } else {
                                    //     if (date('H:i:s', strtotime($row[12])) > '00:00:00' && date('H:i:s', strtotime($row[12])) < '06:00:00') {
                                    //         $changeDate = date('Y-m-d', strtotime($row[0] . ' -1 day'));
                                    //     }
                                    // }

                                    $add_sap['sap_date'] = date('Y-m-d', strtotime($changeDate));
                                    $add_sap['sap_no'] = $row[2];

                                    if ($row[9] > 18) {
                                        RmIssues::create([
                                            'sap_no' => $row[2],
                                            'detail' => 'น้ำหนักเกิน',
                                        ]);
                                    }

                                    RmSap::create($add_sap);
                                }

                                if ($chk_sap > 0) {

                                    $add_sap['sap_no'] = $row[2];
                                    $add_sap['sap_date'] = date('Y-m-d', strtotime($changeDate));

                                    RmSap::where('sap_date', date('Y-m-d', strtotime($row[0])))->where('sap_no', $row[2])
                                        ->where('time_in', date('H:i:s', strtotime($row[12])))->update($add_sap);
                                }

                                if (empty($add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))])) {
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['manual_date'] =  date('Y-m-d', strtotime($changeDate));
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['sap_no'] = $row[2];
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['manual_num'] = $row[6];
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['manual_weight'] = $row[8];
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['car_name'] = $row[3];
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['time_to_laco'] =   date('H:i:s', strtotime($row[12]));
                                } else {
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['manual_date'] =  date('Y-m-d', strtotime($changeDate));
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['sap_no'] = $row[2];
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['manual_num'] += $row[6];
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['manual_weight'] += $row[8];
                                    $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['car_name'] = $row[3];


                                    // if (
                                    //     date('H:i:s', strtotime($row[12])) > '06:00:00'
                                    //     &&  $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['time_to_laco'] > '06:00:00'
                                    // ) {
                                    //     if (date('H:i:s', strtotime($row[12])) < $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['time_to_laco']) {
                                    //         $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['time_to_laco'] =   date('H:i:s', strtotime($row[12]));
                                    //     }
                                    // } else if (
                                    //     date('H:i:s', strtotime($row[12])) < '06:00:00'
                                    //     &&  $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['time_to_laco'] < '06:00:00'
                                    // ) {

                                    //     if (date('H:i:s', strtotime($row[12])) < $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['time_to_laco']) {
                                    //         $add_manual[$row[2]][date('Y-m-d', strtotime($changeDate))]['time_to_laco'] =   date('H:i:s', strtotime($row[12]));
                                    //     }
                                    // }
                                }

                                $add_excel[] = $add_sap;
                            }
                        }
                    }

                    ksort($add_manual);

                    $this->store_manual($add_manual);

                    return $this->manual_export_by_import_sap($manualDate);
                } else {
                    return back()->with('error', "Header Not Being Correct!");
                }
            } else {
                return back()->with('error', "Can't read file!");
            }
        } else {
            return back()->with('error', 'ไม่ได้เพิ่ม file');
        }
    }



    public function store_manual($add_manual)
    {
        $checkData = [];
        foreach ($add_manual as $sap_no => $array1) {
            foreach ($array1 as $sap_date => $value) {
                $tmp = [
                    'car_name' => $value['car_name'],
                    'manual_date' => date('Y-m-d', strtotime($sap_date)),
                    'sap_no' => $sap_no,
                    'manual_num' => intval($value['manual_num']),
                    'manual_weight' => floatval($value['manual_weight']),
                    'time_to_laco' => date('H:i:s', strtotime($value['time_to_laco'])),
                ];

                $existingRecord = RmManual::where('manual_date', $tmp['manual_date'])
                    ->where('sap_no', $tmp['sap_no'])
                    ->where('car_name', $tmp['car_name'])
                    ->orderBy('sap_no', 'asc')
                    ->first();

                if ($existingRecord) {
                    $existingRecord->update($tmp);
                } else {
                    RmManual::create($tmp);
                }
                $checkData[] = $tmp;
            }
        }
    }

    public function manual_export_by_import_sap($manualDate)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValueByColumnAndRow(1, 1, "กรุณาระบุชื่อ crop ในช่องสี เหลือง");
        $sheet->mergeCellsByColumnAndRow(1, 1, 3, 1);
        $sheet->mergeCellsByColumnAndRow(4, 1, 6, 1);
        $sheet->getStyle('D1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF33');

        $sheet->setCellValueByColumnAndRow(1, 2, "วันที่");
        $sheet->setCellValueByColumnAndRow(2, 2, "ลำดับที่");
        $sheet->setCellValueByColumnAndRow(3, 2, "sap_no");
        $sheet->setCellValueByColumnAndRow(4, 2, "raw_material");
        $sheet->setCellValueByColumnAndRow(5, 2, "รถทะเบียน");
        $sheet->setCellValueByColumnAndRow(6, 2, "จำนวน");
        $sheet->setCellValueByColumnAndRow(7, 2, "น้ำหนัก");
        $sheet->setCellValueByColumnAndRow(8, 2, "เวลารถถึง LACO");
        $sheet->setCellValueByColumnAndRow(9, 2, "เขต");
        $sheet->setCellValueByColumnAndRow(10, 2, "พื้นที่เขต");
        $sheet->setCellValueByColumnAndRow(11, 2, "เวลารถออกจากพื้นที่");
        $sheet->setCellValueByColumnAndRow(12, 2, "เวลาลงวัตถุดิบ");
        $sheet->setCellValueByColumnAndRow(13, 2, "ลงเสร็จ");
        $sheet->setCellValueByColumnAndRow(14, 2, "จำนวนชั่วโมง");
        $sheet->setCellValueByColumnAndRow(15, 2, "จำนวนพนง.");
        $sheet->setCellValueByColumnAndRow(16, 2, "ประเภทการลง");
        $sheet->setCellValueByColumnAndRow(17, 2, "หมายเหตุ");
        $sheet->setCellValueByColumnAndRow(18, 2, "ปัญหา");

        $sheet->getStyle('A2:Q2')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A2:Q2')->getAlignment()->setVertical('center');
        $sheet->getStyle('A2:Q2')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');

        $rmManuals = RmManual::where('manual_date', $manualDate)
            ->orderBy('sap_no', 'asc')
            ->orderBy('time_to_laco', 'ASC')
            ->get();

        $i = 1;
        $currentDate = null;
        $startrow = 3;
        $checkData = [];
        foreach ($rmManuals as $rmManual) {
            if ($rmManual->manual_date != $currentDate && is_null($rmManual->manual_no)) {
                $currentDate = $rmManual->manual_date;
                $i = 1;
            }
            $manual_no = $i++;

            $startcol = 1;

            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->manual_date);
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $manual_no);
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->sap_no ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->rm_sap->raw_material->name ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->car_name);
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->manual_num);
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->manual_weight);
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, date('H:i:s', strtotime($rmManual->time_to_laco)));

            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->rm_area_code ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->rm_area->name ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, !empty($rmManual->time_out_area) ? date('H:i:s', strtotime($rmManual->time_out_area)) : '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, !empty($rmManual->time_start_work) ? date('H:i:s', strtotime($rmManual->time_start_work)) : '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, !empty($rmManual->time_end_work) ? date('H:i:s', strtotime($rmManual->time_end_work)) : '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, !empty($rmManual->time_use) ? date('H:i:s', strtotime($rmManual->time_use)) : '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->staff_use ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->rm_type_receive->name ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->note ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->rm_issue->detail ?? '');
            $startrow++;
        }

        $sheet->getStyle('A2:R' . ($startrow - 1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

        $sheet->getStyle('A3:H' . ($startrow - 1))->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('DCE6F1');

        for ($i = 1; $i <= 18; $i++) {
            $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($i);
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $writer = new Xlsx($spreadsheet);
        $filename = "rm_manual-" . date('yymmdd-hi') . ".xlsx";
        $writer->save('storage/' . $filename);
        return response()->download('storage/' . $filename);
    }

    public function import_manual(Request $request)
    {
        $completedirectory = config('myconfig.directory.whrm.completes.manual');
        $fileName = 'file_sap';

        if ($request->hasFile($fileName)) {
            $tmpfolder = date('Ymd');
            if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
            }

            $zipfile = $request->file($fileName);
            $uploadname = $zipfile->getClientOriginalName();
            $name = md5($zipfile->getClientOriginalName() . time()) . '-upload.' . $zipfile->getClientOriginalExtension();
            $destinationPath = $completedirectory . "/" . $tmpfolder;
            $zipfile->move($destinationPath, $name);

            $uploadpath = $completedirectory . "/" . $tmpfolder  . "/" . $name;
            $uploadfile = $destinationPath  . "/" . $name;

            if ($xlsx = SimpleXLSX::parse($uploadfile)) {

                $query = RmArea::pluck('name', 'id');
                foreach ($query as $id => $name) {
                    $rmArea[$name] = $id;
                }

                $query = RmTypeReceive::pluck('name', 'id');
                foreach ($query as $id => $name) {
                    $rmTypeReceive[$name] = $id;
                }

                // ================== Check Header of excel ==================
                $check = false;
                $expected_headers = [
                    "วันที่", "ลำดับที่", "sap_no", "raw_material",
                    "รถทะเบียน", "จำนวน", "น้ำหนัก",
                    "เวลารถถึง LACO", "เขต", "พื้นที่เขต",
                    "เวลารถออกจากพื้นที่", "เวลาลงวัตถุดิบ",
                    "ลงเสร็จ", "จำนวนชั่วโมง", "จำนวนพนง.",
                    "ประเภทการลง", "หมายเหตุ", "ปัญหา"
                ];

                $xlsx_headers = $xlsx->rows(0)[1];
                $xlsx_headers_filtered = array_filter($xlsx_headers);

                if (empty(array_diff($expected_headers, $xlsx_headers_filtered)) && empty(array_diff($xlsx_headers_filtered, $expected_headers))) {
                    $check = true;
                } else {

                    return back()->with('error', "Header Not Being Correct!");
                }

                $add_excel['name'] = $uploadname;

                $check_fileExcel = FileExcel::where('name', $uploadname)
                    ->first();
                if ($check_fileExcel) {
                    $check_fileExcel->update();
                } else {
                    $check_fileExcel = FileExcel::create($add_excel);
                }


                if ($check) {
                    $checkData = [];
                    foreach ($xlsx->rows(0) as $r => $row) {

                        if ($r == 0) {
                            $crop_name = $row[3];
                            $check_fileExcel->update([
                                'crop' => $crop_name
                            ]);
                        }

                        if ($r > 1 && $row[0]) {
                            $add_manual = [];

                            $add_manual['file_excel_id'] = $check_fileExcel->id;
                            $add_manual['manual_date'] =  date('Y-m-d', strtotime($row[0]));
                            $add_manual['manual_no'] = intval($row[1]);
                            $add_manual['sap_no'] = strval($row[2]);
                            $add_manual['car_name'] = strval($row[4]);
                            $add_manual['rm_area_code'] = $row[8];

                            // ================== RmArea ==================
                            if (empty($rmArea[$row[9]])) {
                                $rmArea['name'] = $row[9];
                                $rmArea_id = RmArea::create($rmArea)->id;
                                $rmArea[$row[9]] = $rmArea_id;
                            } else {
                                $rmArea_id = $rmArea[$row[9]];
                            }
                            $add_manual['rm_area_id'] = intval($rmArea_id);

                            $add_manual['manual_num'] = $row[5];
                            $add_manual['manual_weight'] = floatval($row[6]);
                            $add_manual['time_out_area'] = !empty($row[10]) ? date('H:i:s', strtotime($row[10])) : '';
                            $add_manual['time_to_laco'] = !empty($row[7]) ? date('H:i:s', strtotime($row[7])) : '';
                            $add_manual['time_start_work'] = !empty($row[11]) ? date('H:i:s', strtotime($row[11])) : '';
                            $add_manual['time_end_work'] = !empty($row[12]) ? date('H:i:s', strtotime($row[12])) : '';
                            $add_manual['time_use'] = !empty($row[13]) ? date('H:i:s', strtotime($row[13])) : '';
                            $add_manual['staff_use'] = intval($row[14]);


                            // ================== rmTypeReceive ==================
                            if (empty($rmTypeReceive[$row[15]])) {
                                $rmTypeReceive['name'] = $row[9];
                                $rmTypeReceive_id = RmTypeReceive::create($rmTypeReceive)->id;
                                $rmTypeReceive[$row[15]] = $rmTypeReceive_id;
                            } else {
                                $rmTypeReceive_id = intval($rmTypeReceive[$row[15]]);
                            }

                            $add_manual['rm_type_receive_id'] = intval($rmTypeReceive_id);

                            $add_manual['note'] = strval($row[16]);
                            $add_manual['staff_use'] = $row[14];

                            // ===================== Check RmManual =====================
                            $rmManual = RmManual::where('manual_date', $row[0])
                                ->where('sap_no', $row[2])
                                ->first();


                            if ($rmManual) {
                                $rmManual->update($add_manual);
                            } else {
                                RmManual::create($add_manual);
                            }

                            if (!empty($row[17])) {

                                $check_issue = RmIssues::where('rm_manual_id', $rmManual->id)->first();

                                if ($check_issue) {
                                    $check_issue->update([
                                        'sap_no' => $row[2],
                                        'detail' => $row[17]
                                    ]);
                                } else {
                                    $add_manual['issue'] =  RmIssues::create([
                                        'sap_no' => $row[2],
                                        'rm_manual_id' => $rmManual->id,
                                        'detail' => $row[17]
                                    ])->id;
                                }
                            }
                        }
                    }

                    return redirect()->back()->with('success', 'เพิ่ม Manual สำเร็จ');
                } else {
                    return back()->with('error', "Header Not Being Correct!");
                }
            } else {
                return back()->with('error', "Can't read file!");
            }
        } else {
            return back()->with('ไม่ได้เพิ่มไฟล์!');
        }
    }
}
