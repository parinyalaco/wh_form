<?php

namespace App\Http\Controllers\WHRM;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RmIssues;
use App\Models\RmIssuesImgs;
use App\Models\RmManual;
use Image;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;


class BananaController extends Controller
{
    public function export_manual_banana(Request $request)
    {
        $date_from = date("Y-m-d", strtotime($request->banana_date_from));
        $date_to = date("Y-m-d", strtotime($request->banana_date_to));

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValueByColumnAndRow(1, 1, "กรุณาระบุชื่อ crop ในช่องสี เหลือง");
        $sheet->mergeCellsByColumnAndRow(1, 1, 3, 1);
        $sheet->mergeCellsByColumnAndRow(4, 1, 6, 1);
        $sheet->getStyle('D1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF33');

        $sheet->setCellValueByColumnAndRow(1, 2, "วันที่");
        $sheet->setCellValueByColumnAndRow(2, 2, "ลำดับที่");
        $sheet->setCellValueByColumnAndRow(3, 2, "sap_no");
        $sheet->setCellValueByColumnAndRow(4, 2, "raw_material");
        $sheet->setCellValueByColumnAndRow(5, 2, "รถทะเบียน");
        $sheet->setCellValueByColumnAndRow(6, 2, "จำนวน");
        $sheet->setCellValueByColumnAndRow(7, 2, "น้ำหนัก");
        $sheet->setCellValueByColumnAndRow(8, 2, "เวลารถถึง LACO");
        $sheet->setCellValueByColumnAndRow(9, 2, "เขต");
        $sheet->setCellValueByColumnAndRow(10, 2, "พื้นที่เขต");
        $sheet->setCellValueByColumnAndRow(11, 2, "เวลารถออกจากพื้นที่");
        $sheet->setCellValueByColumnAndRow(12, 2, "เวลาลงวัตถุดิบ");
        $sheet->setCellValueByColumnAndRow(13, 2, "ลงเสร็จ");
        $sheet->setCellValueByColumnAndRow(14, 2, "จำนวนชั่วโมง");
        $sheet->setCellValueByColumnAndRow(15, 2, "จำนวนพนง.");
        $sheet->setCellValueByColumnAndRow(16, 2, "ประเภทการลง");
        $sheet->setCellValueByColumnAndRow(17, 2, "หมายเหตุ");
        $sheet->setCellValueByColumnAndRow(18, 2, "ปัญหา");

        $sheet->getStyle('A2:R2')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A2:R2')->getAlignment()->setVertical('center');
        $sheet->getStyle('A2:R2')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');

        $rmManuals = RmManual::with('rm_sap')
            ->whereBetween('manual_date', [$date_from, $date_to])
            ->whereHas('rm_sap.raw_material', function ($query) {
                $query->where('name', 'LIKE', '%BANANA%');
            })
            ->orderBy('sap_no', 'ASC')
            ->orderBy('manual_date', 'ASC')
            ->orderBy('time_to_laco', 'ASC')
            ->get();

        $currentDate = null;
        $i = 1;
        $startrow = 3;
        $checkData = [];
        foreach ($rmManuals as $rmManual) {
            if ($rmManual->manual_date != $currentDate && is_null($rmManual->manual_no)) {
                $currentDate = $rmManual->manual_date;
                $i = 1;
            }
            $manual_no = $i++;
            $startcol = 1;

            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->manual_date);
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $manual_no);
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->sap_no ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->rm_sap->raw_material->name ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->car_name);
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->manual_num);
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->manual_weight);
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, date('H:i:s', strtotime($rmManual->time_to_laco)));

            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->rm_area_code ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->rm_area->name ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, !empty($rmManual->time_out_area) ? date('H:i:s', strtotime($rmManual->time_out_area)) : '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, !empty($rmManual->time_start_work) ? date('H:i:s', strtotime($rmManual->time_start_work)) : '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, !empty($rmManual->time_end_work) ? date('H:i:s', strtotime($rmManual->time_end_work)) : '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, !empty($rmManual->time_use) ? date('H:i:s', strtotime($rmManual->time_use)) : '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->staff_use ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->rm_type_receive->name ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->note ?? '');
            $startcol++;
            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $rmManual->rm_issue->detail ?? '');
            $startrow++;
        }

        $sheet->getStyle('A2:R' . ($startrow - 1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

        $sheet->getStyle('A3:H' . ($startrow - 1))->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('DCE6F1');

        for ($i = 1; $i <= 18; $i++) {
            $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($i);
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $writer = new Xlsx($spreadsheet);
        $filename = "rm_manual-" . date('yymmdd-hi') . ".xlsx";
        $writer->save('storage/' . $filename);
        return response()->download('storage/' . $filename);
    }

    public function test_upload_banana_issue_img($issue_id, Request $request)
    {
        $storage_path = config('myconfig.directory.whrm.completes.img');
        $fileName = 'issue_img';

        if ($request->hasFile($fileName)) {
            $tmpfolder = date('Ymd');
            if (!is_dir($storage_path . '/' . $tmpfolder)) {
                mkdir($storage_path . '/' . $tmpfolder, 0777, true);
            }

            foreach ($request->file($fileName) as $key => $image) {
                $newImageName = time() . $key . '.' . $image->getClientOriginalExtension();

                $resizedImage = Image::make($image)->resize(null, 300, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $destinationPath = $storage_path . "/" . $tmpfolder;
                $path = $destinationPath . "/" . $newImageName;
                $resizedImage->save($path);
                RmIssuesImgs::create([
                    'rm_issue_id' => $issue_id,
                    'path' => $path,
                ]);
            }
        }
        return redirect()->back()->with('success', 'บันทึกรูปสำเร็จ');
    }
    public function show_issue_img($fileName)
    {
        if (!File::exists($fileName)) {
            abort(404);
        }

        $file = File::get($fileName);
        $type = File::mimeType($fileName);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }

    public function add_new_issue($sap_no, Request $request)
    {

        $issue = RmIssues::create([
            'sap_no' => $sap_no,
            'detail' => $request->issue,
        ]);
        if ($request->hasFile('issue_banana_img')) {
            $storage_path = config('myconfig.directory.whrm.completes.img');
            $tmpfolder = date('Ymd');

            if (!is_dir($storage_path . '/' . $tmpfolder)) {
                mkdir($storage_path . '/' . $tmpfolder, 0777, true);
            }

            $image = $request->file('issue_banana_img');
            $newImageName = time() . '.' . $image->getClientOriginalExtension();

            // Resize image
            $resizedImage = Image::make($image)->resize(null, 300, function ($constraint) {
                $constraint->aspectRatio();
            });

            $destinationPath = $storage_path . "/" . $tmpfolder;
            $path = $destinationPath . "/" . $newImageName;
            $resizedImage->save($path);

            // Save image path to the database
            RmIssuesImgs::create([
                'rm_issue_id' => $issue->id,
                'path' => $path, // save relative path
            ]);
        }

        return redirect()->back()->with('success', 'เพิ่มสำเร็จ');
    }

    public function bananaDeleteIssue($rmIssue_id)
    {
        $issue = RmIssues::findOrFail($rmIssue_id);

        $issueImages = $issue->rm_issues_imgs;

        foreach ($issueImages as $issueImage) {

            if (File::exists($issueImage->path)) {
                File::delete($issueImage->path);
            }
            $issueImage->delete();
        }

        $issue->delete();


        return redirect()->back()->with('success', 'ลบปัญหาเรียบร้อยแล้ว');
    }
}
