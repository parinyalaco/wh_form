<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Forklift;
use SimpleXLSX;

class ForkliftController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forklift = Forklift::where('status','Active')->get();

        return view('setting.forklift.index',compact('forklift'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('setting.forklift.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        Forklift::create($requestData);

        return redirect('setting/forklift')->with('success', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $forklift = Forklift::findOrFail($id);
        // return view('setting.forklift.view', compact('forklift'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $forklift = Forklift::findOrFail($id);
        return view('setting.forklift.edit', compact('forklift'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $requestData = $request->all();
        // $forklift = Forklift::findOrFail($id);
        // $requestData['time'] = str_replace(',','',$requestData['time']);
        // $forklift->update($requestData);

        $set_status['status'] = 'Inactive';
        $forklift = Forklift::findOrFail($id);
        $forklift->update($set_status);
        
        $requestData = $request->all();
        $requestData['time'] = str_replace(',','',$requestData['time']);
        Forklift::create($requestData);

        return redirect('setting/forklift')->with('success', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $set_status['status'] = 'Inactive';
        $forklift = Forklift::findOrFail($id);
        $forklift->update($set_status);
        // Forklift::destroy($id);
        return redirect('setting/forklift')->with('success', ' deleted!');
    }

    public function importExportView()
    {
        return view('setting.forklift.import');
    }
   
    public function import(Request $request) 
    {
        $requestData = $request->all();
        $completedirectory = 'storage/app/public/banks/FL/';  

        if ($request->hasFile('file_upload')) {
            // $tmpfolder = md5(time());
            $tmpfolder = date('Ymd');
            if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
            }  

            $zipfile = $request->file('file_upload');
            $uploadname = $zipfile->getClientOriginalName();
            $name = md5($zipfile->getClientOriginalName() . time()) . '-upload.' . $zipfile->getClientOriginalExtension();
            $destinationPath = public_path($completedirectory . $tmpfolder);
            $zipfile->move($destinationPath, $name);

            $uploadpath = $completedirectory . "/" . $tmpfolder  . "/" . $name;
            $uploadfile = $destinationPath  . "/" . $name;              
            
            if ($xlsx = SimpleXLSX::parse($uploadfile)) {
                if(count($xlsx->rows()[0])==5){                  
                    $set_status['status'] = 'Inactive';
                    foreach ($xlsx->rows() as $r => $row) {                                                 
                        if ($r > 0 && !empty(trim($row[1]))) {                
                            if(trim($row[3])==''){
                                return back()->with('error',trim($row[1]).' ไม่มีข้อมูลชั่วโมงเริ่มต้น');
                            }else{
                                $to_save = array();
                                $to_save['name'] = trim($row[1]);
                                $to_save['type'] = trim($row[2]);  
                                
                                $to_save['time'] = trim($row[3]);   
                                $to_save['desc'] = trim($row[4]);  
                                $chk_fl = Forklift::where('status','Active')->where('name',trim($row[1]))->get();
                                // dd(count($chk_fl));
                                if(count($chk_fl)>0){
                                    Forklift::where('status','Active')->where('name',trim($row[1]))->update($set_status);
                                }
                                // dd($to_save);
                                Forklift::create($to_save); 
                            }                       
                        }
                    }
                    return back()->with('success','Updated successfully');  
                } else{
                    return back()->with('error','Can not Updated');
                } 
            } else {
                echo SimpleXLSX::parseError();
            }   
        }else{
            return back()->with('error','No file');
        } 
        
    }
}
