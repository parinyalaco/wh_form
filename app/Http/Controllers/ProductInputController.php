<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ToImportfile;
use App\Models\Material;
use App\Models\Vendor;
use App\Models\Unit;
use App\Models\ProductType;
use App\Models\ProductInput;
use App\Models\ProductInputEn;
use App\Models\ProductInputDc;

use DateTime;
use DatePeriod;
use DateInterval;
// use Carbon\Carbon;
// use DB;
// use Excel;
use SimpleXLSX;
use Maatwebsite\Excel\Facades\Excel;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\IOFactory;

use App\Mail\ReportRM_WH_Mail;
use App\Mail\ReportRM_Month_Mail;
use App\Mail\ReportSum_ProductInput_Mail;
use App\Mail\ReportSumMonth_ProductInput_Mail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;

class ProductInputController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd($request);
        $to_query = array();
        $main_import = array();
        $to_show = array();
        $material = array();
        $vendor = array();
        $max_id = "";
        $date_from = "";
        $date_to ="";

        if(!empty($request->get('date_from'))){
            $date_from = $request->get('date_from');
            $date_to =$request->get('date_to');

            $query = ProductInput::where('active', 1)->whereBetween('input_date', [$date_from, $date_to])->orderByDesc('input_date')->get();
        }else{
            //query แบบเรียงตามวันที่
            $query = ProductInput::where('active', 1)->groupBy('input_date')->orderByDesc('input_date')->selectRaw('TOP (5) input_date')->get();
            if(count($query)>0){
                foreach($query as $key){
                    $to_query[] = $key->input_date;
                }
                // dd($to_query);
                $date_from = $to_query[4];
                $date_to =$to_query[0]; 

                $query = ProductInput::where('active', 1)->whereIn('input_date', $to_query)->orderByDesc('input_date')->get();
            }
        }
        // if(count($to_query)>0){
            // $query = ProductInput::where('active', 1)->whereIn('input_date', $to_query)->orderByDesc('input_date')->get();
        if(count($query)>0){
            foreach ($query as $key) {
                $to_show[$key->input_date][$key->id]['material_id'] = $key->material_id;
                $to_show[$key->input_date][$key->id]['vendor_id'] = $key->vendor_id;
                $to_show[$key->input_date][$key->id]['po_quantity'] = $key->po_quantity;
                $to_show[$key->input_date][$key->id]['input_real'] = $key->input_real;
                $to_show[$key->input_date][$key->id]['input_list'] = $key->input_list;
                $to_show[$key->input_date][$key->id]['input_random'] = $key->input_random;
                $to_show[$key->input_date][$key->id]['note'] = $key->note;
            }
        }
        $query_1 = Material::join('units', 'materials.unit_id', '=', 'units.id')
            ->join('product_types', 'materials.product_type_id', '=', 'product_types.id')
            ->select('materials.id', 'materials.name', 'materials.po_shot_text', 'materials.unit_id', 'units.name AS un_name', 'materials.product_type_id', 'product_types.name AS pt_name')
            ->get();
        foreach ($query_1 as $key) {
            $material[$key->id]['name'] = $key->name;
            $material[$key->id]['po_shot_text'] = $key->po_shot_text;
            $material[$key->id]['unit'] = $key->un_name;
            // $unit[$key->unit_id] = $key->un_name;
            $material[$key->id]['pd_type'] = $key->pt_name;
            // $pd_type[$key->product_type_id] = $key->pt_name;
        }

        $query_2 = Vendor::get();
        foreach ($query_2 as $key) {
            $vendor[$key->id] = $key->name;
        }

        // dd($to_show);

        return view('product.index',compact('to_query','main_import','to_show','material','vendor','max_id','date_from','date_to'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)    //delete file
    {
        $pd_input = ProductInput::where('to_importfile_id',$id)->delete();
        if($pd_input){
            $to_import = ToImportfile::where('id',$id)->delete();
            if($to_import){
                return back()->with('success','Delete successfully');
            }else{
                return back()->with('error','Delete not successfully');
            }
        }
    }

    public function list_destroy($type, $id)   //delete list
    {
        // dd($type);
        $set_active['active'] = 0;
        if($type=='date'){
            $pd_input = ProductInput::where('input_date',$id)->update($set_active);
        }elseif($type=='row'){            
            $pd_input = ProductInput::where('id',$id)->update($set_active);            
        }  
        if($pd_input){
            return back()->with('success','Delete successfully');
        }else{
            return back()->with('error','Delete not successfully');
        }      
    }

    public function importExportView()
    {
        return view('product.import');
    }
   
    public function import(Request $request, $rec_tbl) 
    {
        // dd($rec_tbl);
        $requestData = $request->all();
        // $completedirectory = 'storage/app/public/banks/completes/';  
        $completedirectory = config('myconfig.directory.productinput.'.$rec_tbl.'.completes');

        if ($request->hasFile('file_upload')) {
            $tmpfolder = date('Ymd');
            if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
            }  

            $zipfile = $request->file('file_upload');
            $uploadname = $zipfile->getClientOriginalName();
            $name = $rec_tbl.'-'.md5($uploadname . time()) . '-upload.' . $zipfile->getClientOriginalExtension();
            $destinationPath = $completedirectory . "/" . $tmpfolder;
            $zipfile->move($destinationPath, $name);
            
            $uploadpath = $tmpfolder  . "/" . $name;
            $uploadfile = $destinationPath  . "/" . $name;             

            $vender_data = array();
            $unit_data = array();
            $pdtype_data = array();
            $mat_data = array();
            $pd_input = array();
            $test = $this->base_data($rec_tbl);
            if(!empty($test[0]))    $vender_data = $test[0];
            if(!empty($test[1]))    $unit_data = $test[1];
            if(!empty($test[2]))    $pdtype_data = $test[2];
            if(!empty($test[3]))    $mat_data = $test[3];
            if(!empty($test[4]))    $pd_input = $test[4];

            if ($xlsx = SimpleXLSX::parse($uploadfile)) {

                if($rec_tbl == 'laco') $c_col = 14;
                else $c_col = 12;
                // dd($c_col);
                if(count($xlsx->rows()[0])==$c_col){ 

                    // $to_save['name'] = $uploadname;
                    // $to_save['file_tosave'] = $uploadpath;       
                    // $file_id = ToImportfile::create($to_save)->id;
                    
                    $this->save_file($xlsx, $rec_tbl, $uploadname, $uploadpath, $vender_data, $unit_data, $pdtype_data, $mat_data, $pd_input);
                    // dd($pd_input);
                }else{
                    return back()->with('error','จำนวนคอลัมน์ไม่ถูกต้อง');
                }                         

            } else {
                echo SimpleXLSX::parseError();
            }            
            return back()->with('success','Updated successfully');
        }
        
    }

    public function base_data($type)
    {
        $rec_tbl = $type;
        $vender_data = array();
        $unit_data = array();
        $pdtype_data = array();
        $mat_data = array();
        $pd_input = array();

        $query = Vendor::get();
        foreach($query as $key){
            $vender_data[$key->name] = $key->id;
        }

        $query = Unit::get();
        foreach($query as $key){
            $unit_data[$key->name] = $key->id;
        }

        $query = ProductType::get();
        foreach($query as $key){
            $pdtype_data[$key->name] = $key->id;
        }

        $query = Material::join('units', 'materials.unit_id', '=', 'units.id')
            ->join('product_types', 'materials.product_type_id', '=', 'product_types.id')
            ->select('materials.id', 'materials.name', 'materials.po_shot_text', 'units.name AS unit_name', 'product_types.name AS pt_name')
            ->get();
        foreach($query as $key){
            $mat_data[$key->name][$key->po_shot_text][$key->unit_name][$key->pt_name] = $key->id;
        }

        if($rec_tbl == 'laco'){
            $query = ProductInput::join('materials', 'product_inputs.material_id', '=', 'materials.id')
                ->where('product_inputs.active',1)
                ->select('product_inputs.id', 'product_inputs.input_date', 'product_inputs.po_no', 'product_inputs.material_id', 'product_inputs.vendor_id', 
                    'product_inputs.po_quantity', 'product_inputs.input_real', 'product_inputs.input_list', 'product_inputs.input_random')
                ->get();
            foreach($query as $key){
                $pd_input[$key->input_date][$key->po_no][$key->material_id][$key->vendor_id][number_format($key->po_quantity,4,'.','')][number_format($key->input_real,4,'.','')][$key->input_list][$key->input_random] = $key->id;
            }
        }else{
            if($rec_tbl == 'en'){
                $query = ProductInputEn::join('materials', 'product_input_ens.material_id', '=', 'materials.id')
                ->where('product_input_ens.active',1)
                ->select('product_input_ens.id', 'product_input_ens.input_date', 'product_input_ens.po_no', 'product_input_ens.material_id', 
                    'product_input_ens.vendor_id', 'product_input_ens.po_quantity', 'product_input_ens.input_real', 
                    'product_input_ens.input_list')
                ->get();                            
            }else{
                // echo 'dc';
                $query = ProductInputDc::join('materials', 'product_input_dcs.material_id', '=', 'materials.id')
                ->where('product_input_dcs.active',1)
                ->select('product_input_dcs.id', 'product_input_dcs.input_date', 'product_input_dcs.po_no', 'product_input_dcs.material_id', 
                    'product_input_dcs.vendor_id', 'product_input_dcs.po_quantity', 'product_input_dcs.input_real', 
                    'product_input_dcs.input_list')
                ->get();
            }
            foreach($query as $key){
                $pd_input[$key->input_date][$key->po_no][$key->material_id][$key->vendor_id][number_format($key->po_quantity,4,'.','')][number_format($key->input_real,4,'.','')][$key->input_list] = $key->id;
            }                        
        }
        return [$vender_data, $unit_data, $pdtype_data, $mat_data, $pd_input] ; 
    }

    public function save_file($xlsx, $rec_tbl, $uploadname, $uploadpath, $vender_data, $unit_data, $pdtype_data, $mat_data, $pd_input)
    {    
        $to_save['name'] = $uploadname;
        $to_save['file_tosave'] = $uploadpath;       
        $file_id = ToImportfile::create($to_save)->id;
        
        foreach ($xlsx->rows() as $r => $row) {   
            if ($r > 1) {
                $to_save = array();        
                // dd($row[4]);
                if(empty($vender_data[trim($row[5])])){
                    $to_save['name'] = trim($row[5]);
                    $vender_id = Vendor::create($to_save)->id; 

                    $vender_data[trim($row[5])] = $vender_id;

                }else{
                    $vender_id = $vender_data[trim($row[5])];
                }
                
                if($rec_tbl == 'laco'){
                    // $ptype = trim($row[11]);
                    if( !preg_replace("/[^ก-๙A-Za-z0-9 _-]+/",'',trim($row[12])) ){
                        // echo 'ไม่ไทย-'.trim($row[12]).'</br>';
                        $to_type = ucwords(strtolower(trim($row[12])));
                    } else {
                        // echo 'ไทยแท้ๆ-'.trim($row[12]).'</br>';
                        $to_type = trim($row[12]);
                    }                    
                }else{
                    $to_type = '';
                }

                if(empty($mat_data[trim($row[3])][trim($row[4])][trim($row[7])][$to_type])){             
                    if(empty($unit_data[trim($row[7])])){
                        $to_save = array();  
                        $to_save['name'] = trim($row[7]);
                        $unit_id = Unit::create($to_save)->id;
                        
                        $unit_data[trim($row[7])] = $unit_id;

                    }else{
                        $unit_id = $unit_data[trim($row[7])];
                    }
                    
                    if(empty($pdtype_data[$to_type])){
                        $to_save = array();  
                        $to_save['name'] = $to_type;
                        $pdtype_id = ProductType::create($to_save)->id;

                        $pdtype_data[$to_type] = $pdtype_id;

                    }else{
                        $pdtype_id = $pdtype_data[$to_type];
                    }
                    $to_save = array();  
                    $to_save['name'] = trim($row[3]);
                    $to_save['po_shot_text'] = trim($row[4]);
                    $to_save['unit_id'] = $unit_id;
                    $to_save['product_type_id'] = $pdtype_id;
                    $mat_id = Material::create($to_save)->id;

                    $mat_data[trim($row[3])][trim($row[4])][trim($row[7])][$to_type] = $mat_id;

                }else{
                    $mat_id = $mat_data[trim($row[3])][trim($row[4])][trim($row[7])][$to_type];
                }

                if(trim($row[6]) == '') $col_6 = 0;
                else $col_6 = trim($row[6]);
                if(trim($row[8]) == '') $col_8 = 0;
                else $col_8 = trim($row[8]);                
                
                $chk_del = 0;
                if($rec_tbl == 'laco'){
                    if(!empty($pd_input[date("Y-m-d", strtotime($row[1]))][trim($row[2])][$mat_id][$vender_id][number_format($col_6,4,'.','')][number_format($col_8,4,'.','')][trim($row[10])][trim($row[11])])){
                        $chk_del = 1;
                        $del_pd = new ProductInput;
                    }
                }else{
                    if(!empty($pd_input[date("Y-m-d", strtotime($row[1]))][trim($row[2])][$mat_id][$vender_id][number_format($col_6,4,'.','')][number_format($col_8,4,'.','')][trim($row[10])])){
                        $chk_del = 1;
                        if($rec_tbl == 'en'){
                            $del_pd = new ProductInputEn;                            
                        }else{
                            $del_pd = new ProductInputDc;
                        }
                    }
                }
                
                // if($chk_del == 1){
                    // $del_pd = $del_pd->where('input_date',date("Y-m-d", strtotime($row[1])));
                    // $del_pd = $del_pd->where('po_no',trim($row[2]));
                    // $del_pd = $del_pd->where('material_id',$mat_id)->where('vendor_id',$vender_id);
                    // $del_pd = $del_pd->where('po_quantity',$col_6);
                    // $del_pd = $del_pd->where('input_real',$col_8);
                    // if(trim($row[9])==''){
                    //     $del_pd = $del_pd->whereNull('input_list');
                    // }else{
                    //     $del_pd = $del_pd->where('input_list',trim($row[9]));
                    // }
                    // if($rec_tbl == 'laco'){
                    //     if(trim($row[10])==''){
                    //         $del_pd = $del_pd->whereNull('input_random');
                    //     }else{
                    //         $del_pd = $del_pd->where('input_random',trim($row[10]));
                    //     }
                    // }
                    // $del_pd = $del_pd->delete();
                // }else{
                if($chk_del == 0){
                    $to_save = array(); 
                    $to_save['to_importfile_id'] = $file_id;
                    $to_save['input_date'] = date("Y-m-d", strtotime($row[1]));
                    $to_save['po_no'] = trim($row[2]);
                    $to_save['material_id'] = $mat_id;
                    $to_save['vendor_id'] = $vender_id;
                    $to_save['po_quantity'] = $col_6;
                    $to_save['input_real'] = $col_8;
                    if(trim($row[10])<>'')    $to_save['input_list'] = trim($row[10]);
                    if($rec_tbl == 'laco'){
                        if(trim($row[11])<>'')    $to_save['input_random'] = trim($row[11]); 
                        $to_save['note'] = $row[13];
                        $laco_id = ProductInput::create($to_save)->id; 
                        $pd_input[$to_save['input_date']][trim($row[2])][$mat_id][$vender_id][number_format($col_6,4,'.','')][number_format($col_8,4,'.','')][trim($row[10])][trim($row[11])] = $laco_id;
                    }elseif($rec_tbl == 'en'){
                        $to_save['note'] = $row[11];
                        $en_id = ProductInputEn::create($to_save)->id;   
                        $pd_input[$to_save['input_date']][trim($row[2])][$mat_id][$vender_id][number_format($col_6,4,'.','')][number_format($col_8,4,'.','')][trim($row[10])] = $en_id;                         
                    }else{
                        $to_save['note'] = $row[11];
                        $dc_id = ProductInputDc::create($to_save)->id;
                        $pd_input[$to_save['input_date']][trim($row[2])][$mat_id][$vender_id][number_format($col_6,4,'.','')][number_format($col_8,4,'.','')][trim($row[10])] = $dc_id;
                    }
                    // dd($to_save);
                }   
            }                        
        }
    }

    public function reportView()
    {
        return view('report.index');
    }

    public function report_export(Request $request)
    {
        $ed_date = date ("Y-m-d", strtotime($request->st_date));
        $st_date = date ("Y-m-d", strtotime("-".$request->rang_date." day", strtotime($ed_date)));
        
        $mat = array();
        $query_1 = Material::select('id','po_shot_text')->get(); 
        if(!empty($query_1)){
            foreach($query_1 as $key){
                $mat[$key->id] = $key->po_shot_text;
            }
        }
        
        $to_show = array();
        $to_show_mat = array();

        if($request->report_id==1){
            $query = ProductInput::whereBetween('input_date', [$st_date, $ed_date])->where('active',1)
                ->selectRaw('input_date, input_real - po_quantity AS plan_input')->get();
            // dd($query);
            foreach ($query as $key) {
                if(!empty($to_show[$key->input_date]['all']))   $to_show[$key->input_date]['all'] += 1;
                else    $to_show[$key->input_date]['all'] = 1;
                if($key->plan_input==0){
                    if(!empty($to_show[$key->input_date]['in_plan']))   $to_show[$key->input_date]['in_plan'] += 1;
                    else    $to_show[$key->input_date]['in_plan'] = 1;
                }else{
                    if(!empty($to_show[$key->input_date]['out_plan']))   $to_show[$key->input_date]['out_plan'] += 1;
                    else    $to_show[$key->input_date]['out_plan'] = 1;
                }
            }
            // dd($to_show);                        

            return view('report.report_1',compact('to_show'));

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            //mergeCellsByColumnAndRow(1, 1, 13, 1) = (1, 1):(13, 1) = A1:M1
            // $sheet->setCellValueByColumnAndRow(1, 1, "รายชื่อพนักงานสำหรับการประเมิน");
            // $sheet->mergeCellsByColumnAndRow(1, 1, 13, 1);
            
            $sheet->setCellValueByColumnAndRow(1, 1, "วันที่");
            $sheet->mergeCellsByColumnAndRow(1, 1, 1, 2); 
            $sheet->getColumnDimension('A')->setWidth(11);  

            $sheet->setCellValueByColumnAndRow(2, 1, "รับทั้งหมด");            
            $sheet->getColumnDimension('B')->setWidth(9); 

            $sheet->setCellValueByColumnAndRow(3, 1, "รับจริง");
            $sheet->mergeCellsByColumnAndRow(3, 1, 4, 1);
            $sheet->setCellValueByColumnAndRow(5, 1, "% (เปอร์เซ็นต์)");
            $sheet->mergeCellsByColumnAndRow(5, 1, 6, 1);
            
            $sheet->setCellValueByColumnAndRow(2, 2, "จำนวน");
            $sheet->setCellValueByColumnAndRow(3, 2, "ตรงแผน");             
            $sheet->getColumnDimension('C')->setWidth(10);  

            $sheet->setCellValueByColumnAndRow(4, 2, "ไม่ตรงแผน");
            $sheet->getColumnDimension('D')->setWidth(10); 

            $sheet->setCellValueByColumnAndRow(5, 2, "ตรงแผน");
            $sheet->getColumnDimension('E')->setWidth(10); 

            $sheet->setCellValueByColumnAndRow(6, 2, "ไม่ตรงแผน");   
            $sheet->getColumnDimension('F')->setWidth(10); 

            
            $sheet->getStyle('A1:F2')->getAlignment()->setHorizontal('center');         
            $sheet->getStyle('A1:F2')->getAlignment()->setVertical('center');
            $sheet->getStyle('A1:F2')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('FCE4D6');

            $startrow = 3;
            $head_id = "";
            // $startcol = 1;

            foreach ($to_show as $key=>$value) {
                
                $startcol = 1;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $key);
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $to_show[$key]['all']);
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $to_show[$key]['in_plan']);
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $to_show[$key]['out_plan']);
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, number_format($to_show[$key]['in_plan']/$to_show[$key]['all']*100,2));
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, number_format($to_show[$key]['out_plan']/$to_show[$key]['all']*100,2));

                $startrow++;
            }

            $sheet->getStyle('A1:F'.($startrow-1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

            $writer = new Xlsx($spreadsheet);

            $filename = "report_1-" . date('yymmdd-hi') . ".xlsx";

            $writer->save('storage/' . $filename);

            return response()->download('storage/' . $filename);

        }elseif($request->report_id==2){
            $query = ProductInput::whereBetween('input_date', [$st_date, $ed_date])->where('active',1)->get();
            // dd($query);
            foreach ($query as $key) {
                if($key->po_quantity>0){
                    if(!empty($to_show[$key->input_date][$key->po_no]['all']))   $to_show[$key->input_date][$key->po_no]['all'] += 1;
                    else    $to_show[$key->input_date][$key->po_no]['all'] = 1;
                }
                if($key->input_real>0){
                    if(!empty($to_show[$key->input_date][$key->po_no]['in_real']))   $to_show[$key->input_date][$key->po_no]['in_real'] += 1;
                    else    $to_show[$key->input_date][$key->po_no]['in_real'] = 1;
                }
                if($key->input_real-$key->po_quantity != 0){
                    $diff = $key->input_real - $key->po_quantity;
                    if(empty($key->note)){
                        if($key->po_quantity>0){
                            if($key->plan_input>0){
                                $to_show_mat[$key->input_date][$key->po_no][$key->material_id] = 'สินค้าเข้ามากกว่าแผน';
                            }else{
                                $to_show_mat[$key->input_date][$key->po_no][$key->material_id] = 'สินค้าเข้าน้อยกว่าแผน'; 
                            }
                        }else{                        
                            $to_show_mat[$key->input_date][$key->po_no][$key->material_id] = 'สินค้าไม่เข้าตามแผน';    
                        }
                    }else{
                        $to_show_mat[$key->input_date][$key->po_no][$key->material_id] = $key->note;    
                    }
                }
            }
            // dd($to_show_mat);      

            return view('report.report_2',compact('to_show','to_show_mat','mat'));

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            //mergeCellsByColumnAndRow(1, 1, 13, 1) = (1, 1):(13, 1) = A1:M1
            // $sheet->setCellValueByColumnAndRow(1, 1, "รายชื่อพนักงานสำหรับการประเมิน");
            // $sheet->mergeCellsByColumnAndRow(1, 1, 13, 1);

            $sheet->setCellValueByColumnAndRow(1, 1, "วันที่");
            $sheet->mergeCellsByColumnAndRow(1, 1, 1, 2); 
            $sheet->getColumnDimension('A')->setWidth(11); 
            
            $sheet->setCellValueByColumnAndRow(2, 1, "แผนรับเข้า");
            $sheet->getColumnDimension('B')->setWidth(10);  
            $sheet->setCellValueByColumnAndRow(3, 1, "รับจริง");
            $sheet->getColumnDimension('C')->setWidth(10); 

            $sheet->setCellValueByColumnAndRow(4, 1, "PO No.");
            $sheet->mergeCellsByColumnAndRow(4, 1, 4, 2); 
            $sheet->getColumnDimension('D')->setWidth(11); 

            $sheet->setCellValueByColumnAndRow(5, 1, "ไม่ตรงตามแผน"); 
            $sheet->mergeCellsByColumnAndRow(5, 1, 5, 2); 
            $sheet->getColumnDimension('E')->setWidth(50);  

            $sheet->setCellValueByColumnAndRow(6, 1, "หมายเหตุ"); 
            $sheet->mergeCellsByColumnAndRow(6, 1, 6, 2); 
            $sheet->getColumnDimension('F')->setWidth(20);                       

            $sheet->setCellValueByColumnAndRow(2, 2, "(รายการ)");
            $sheet->setCellValueByColumnAndRow(3, 2, "(รายการ)");           
            
            $sheet->getStyle('A1:F2')->getAlignment()->setHorizontal('center');         
            $sheet->getStyle('A1:F2')->getAlignment()->setVertical('center');
            $sheet->getStyle('A1:F2')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF00');

            $startrow = 3;
            $head_id = "";
            // $startcol = 1;

            foreach ($to_show as $key=>$value) {
                foreach ($value as $kpo=>$vpo) {                
                    $startcol = 1;
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, $key);                
                    $sheet->mergeCellsByColumnAndRow($startcol, $startrow, $startcol, ($startrow+count($to_show_mat[$key][$kpo])-1));
                    $startcol++;
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, $to_show[$key][$kpo]['all']);
                    $sheet->mergeCellsByColumnAndRow($startcol, $startrow, $startcol, ($startrow+count($to_show_mat[$key][$kpo])-1));
                    $startcol++;
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, $to_show[$key][$kpo]['in_real']);                
                    $sheet->mergeCellsByColumnAndRow($startcol, $startrow, $startcol, ($startrow+count($to_show_mat[$key][$kpo])-1));
                    $startcol++;
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, $kpo);  
                    $startcol++;
                    foreach($to_show_mat[$key][$kpo] as $kmat=>$vmat){
                        $sheet->setCellValueByColumnAndRow($startcol, $startrow, $mat[$kmat]);
                        $sheet->setCellValueByColumnAndRow(($startcol+1), $startrow, $vmat);
                        $startrow++;
                    }
                }
            }

            $sheet->getStyle('A1:C'.($startrow-1))->getAlignment()->setVertical('top'); 
            $sheet->getStyle('A1:F'.($startrow-1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

            $writer = new Xlsx($spreadsheet);

            $filename = "report_2-" . date('yymmdd-hi') . ".xlsx";

            $writer->save('storage/' . $filename);

            return response()->download('storage/' . $filename);
        
        }elseif($request->report_id==3){
            $query = ProductInput::where('input_date', $ed_date)->where('active',1)
                ->selectRaw('input_date, input_real - po_quantity AS plan_input')->get();
            // dd($query);
            foreach ($query as $key) {
                if($key->plan_input==0){                    
                    if(!empty($to_show['in_plan']))   $to_show['in_plan'] += 1;
                    else    $to_show['in_plan'] = 1;
                }else{
                    if(!empty($to_show['out_plan']))   $to_show['out_plan'] += 1;
                    else    $to_show['out_plan'] = 1;
                }
            }
            // dd($to_show);

            $graph_date = date ("d/m/Y", strtotime($ed_date));

            if(!empty($to_show['in_plan'])){
                $in_plan = $to_show['in_plan']/($to_show['in_plan']+$to_show['out_plan'])*100;
                $out_plan = $to_show['out_plan']/($to_show['in_plan']+$to_show['out_plan'])*100;
            }else{
                $in_plan = 0;
                $out_plan = 0;
            }

            return view('report.report_3', compact('in_plan','out_plan','graph_date'));
        }elseif($request->report_id==4){
            $query = ProductInput::whereBetween('input_date', [$st_date, $ed_date])->whereNotNull('input_random')->where('active',1)->get();
            // dd($query);
            foreach ($query as $key) {
                if(!empty($to_show[$key->input_date]['all']))   $to_show[$key->input_date]['all'] += 1;
                else    $to_show[$key->input_date]['all'] = 1;
                if($key->input_random==1){
                    if(!empty($to_show[$key->input_date]['in_pass']))   $to_show[$key->input_date]['in_pass'] += 1;
                    else    $to_show[$key->input_date]['in_pass'] = 1;
                }
                if($key->input_random==0){
                    if(!empty($to_show[$key->input_date]['not_pass']))   $to_show[$key->input_date]['not_pass'] += 1;
                    else    $to_show[$key->input_date]['not_pass'] = 1;
                }
                if($key->input_random==0){
                    $to_show_mat[$key->input_date][$key->material_id] = $mat[$key->material_id];
                }
            }
            // dd($to_show);      

            return view('report.report_4',compact('to_show','to_show_mat','mat'));
            
        }elseif($request->report_id==5){
            $pd_type = "";
            $val_all = "";
            $val_rd = "";
            $query = ProductInput::join('materials', 'product_inputs.material_id', '=', 'materials.id')
                ->join('product_types', 'materials.product_type_id', '=', 'product_types.id')
                ->where('product_inputs.input_date', $ed_date)->where('product_types.name','<>', '')->where('product_inputs.active',1)
                ->select('product_inputs.input_date', 'product_inputs.id', 'materials.name', 'materials.product_type_id', 'product_types.name AS pt_name', 'product_inputs.input_random')
                ->get();
            // dd($query);
            foreach ($query as $key) {
                $pt_name[$key->product_type_id] = $key->pt_name;
                if(!empty($to_show['all'][$key->pt_name]))   $to_show['all'][$key->pt_name] += 1;
                else    $to_show['all'][$key->pt_name] = 1;

                if(!empty($to_show['all']['Total']))   $to_show['all']['Total'] += 1;
                else    $to_show['all']['Total'] = 1;

                if($key->input_random==1){
                    if(!empty($to_show['random'][$key->pt_name]))   $to_show['random'][$key->pt_name] += 1;
                    else    $to_show['random'][$key->pt_name] = 1;

                    if(!empty($to_show['random']['Total']))   $to_show['random']['Total'] += 1;
                    else    $to_show['random']['Total'] = 1;
                }                                   
            }
            if(count($query)>0){
                $pt_name[] = 'Total';

                $pt_nam = "[";
                foreach($pt_name as $key=>$value){
                    $pt_nam .= "'".$value."',"; 
                }
                $pd_type = substr($pt_nam, 0, -1);
                $pd_type .= "]";
                // dd($pd_type); 
                
                foreach($to_show as $key=>$value){
                    foreach($value as $kpt=>$vpt){
                        if(empty($to_show[$key][$kpt]))    $to_show[$key][$kpt] = 0;  
                    }
                } 
                
                $val_all = "[";
                $val_rd = "[";
                foreach($pt_name as $kpt=>$vpt){
                    $val_all .= $to_show['all'][$vpt].',';
                    $val_rd .= $to_show['random'][$vpt].',';
                }
                $val_all = substr($val_all, 0, -1);
                $val_all .= "]";

                $val_rd = substr($val_rd, 0, -1);
                $val_rd .= "]";
                // dd($val_all);                 
            }
            $graph_date = date ("d/m/Y", strtotime($ed_date));
            return view('report.report_5', compact('pd_type','val_all','val_rd','graph_date'));
        } 
    }

    public function sentmail($rec_tbl, $date_to){
        // dd($date_to);
        $explode_date = explode(':',$date_to);
        $ed_date = $explode_date[0];
        $st_date = $explode_date[1]; 
        
        // $ed_date = '2022-08-12';
        // $st_date = '2022-08-10';
        // echo  $st_date.' to '.$ed_date;
        $start_date = date ("d/m/Y", strtotime($st_date));
        $end_date = date ("d/m/Y", strtotime($ed_date));

        $mat = array();
        $query_1 = Material::select('id','po_shot_text')->get(); 
        if(!empty($query_1)){
            foreach($query_1 as $key){
                $mat[$key->id] = $key->po_shot_text;
            }
        }

        $to_show = array();
        $show_4 =array();
        $to_show_mat = array();

        if($rec_tbl == 'laco'){
            $query = ProductInput::whereBetween('input_date', [$st_date, $ed_date])->where('active',1)
                ->selectRaw('id, input_date, po_no, input_real, po_quantity, material_id, input_random, input_real - po_quantity AS plan_input, note')
                ->orderBy('input_date')->get();
        }else{
            if($rec_tbl == 'en'){
                $query = new ProductInputEn;                            
            }else{
                $query = new ProductInputDc;
            }
            $query = $query->whereBetween('input_date', [$st_date, $ed_date])->where('active',1)
                ->selectRaw('id, input_date, po_no, input_real, po_quantity, material_id, input_real - po_quantity AS plan_input, note')
                ->orderBy('input_date')->get();
        }
        $in_to_show = array('in_plan','out_plan','in_real','in_po');
        foreach ($query as $key) {
            if(!empty($to_show[$key->input_date]['all']))   $to_show[$key->input_date]['all'] += 1;
            else    $to_show[$key->input_date]['all'] = 1;
            if($key->plan_input==0){
                if(!empty($to_show[$key->input_date]['in_plan']))   $to_show[$key->input_date]['in_plan'] += 1;
                else    $to_show[$key->input_date]['in_plan'] = 1;

                if(!empty($to_show_all['in_plan']))   $to_show_all['in_plan'] += 1;
                else    $to_show_all['in_plan'] = 1;            
            }else{
                if(!empty($to_show[$key->input_date]['out_plan']))   $to_show[$key->input_date]['out_plan'] += 1;
                else    $to_show[$key->input_date]['out_plan'] = 1;

                if(!empty($to_show_all['out_plan']))   $to_show_all['out_plan'] += 1;
                else    $to_show_all['out_plan'] = 1;
            }            
            if($key->input_real>0){
                if(!empty($to_show[$key->input_date]['in_real']))   $to_show[$key->input_date]['in_real'] += 1;
                else    $to_show[$key->input_date]['in_real'] = 1;
            }
            if($key->po_quantity>0){
                if(!empty($to_show[$key->input_date]['in_po']))   $to_show[$key->input_date]['in_po'] += 1;
                else    $to_show[$key->input_date]['in_po'] = 1;
            }

            foreach ($in_to_show as $kin => $vin) {
                if(empty($to_show[$key->input_date][$vin]))    $to_show[$key->input_date][$vin] = 0;
            }

            if($key->plan_input != 0){                
                // if(empty($key->note)){
                //     // dd("empty");
                //     $to_show_mat[2][$key->input_date][$key->material_id] = 'สินค้าไม่เข้าตามแผน';
                // }else{
                //     // dd($key->note);
                //     $to_show_mat[2][$key->input_date][$key->material_id] = $key->note;
                // } 
                
                
                // if($key->input_real>0){
                //     if($key->plan_input>0){
                //         $to_show_mat[2][$key->input_date][$key->material_id] = 'สินค้าเข้ามากกว่าแผน';
                //     }else{
                //         $to_show_mat[2][$key->input_date][$key->material_id] = 'สินค้าเข้าน้อยกว่าแผน'; 
                //     }
                // }else{
                //     if(empty($key->note)){
                //         $to_show_mat[2][$key->input_date][$key->material_id] = 'สินค้าไม่เข้าตามแผน';
                //     }else{
                //         $to_show_mat[2][$key->input_date][$key->material_id] = $key->note;
                //     }
                // }

                if(empty($key->note)){
                    if($key->po_quantity>0){
                        if($key->plan_input>0){
                            $to_show_mat[2][$key->input_date][$key->po_no][$key->material_id] = 'สินค้าเข้ามากกว่าแผน';
                        }else{
                            $to_show_mat[2][$key->input_date][$key->po_no][$key->material_id] = 'สินค้าเข้าน้อยกว่าแผน'; 
                        }
                    }else{
                        $to_show_mat[2][$key->input_date][$key->po_no][$key->material_id] = 'สินค้าไม่เข้าตามแผน';
                    }
                }else{
                    $to_show_mat[2][$key->input_date][$key->po_no][$key->material_id] = $key->note;
                }
            }

            if($rec_tbl == 'laco'){
                if(isset($key->input_random)){
                    if(!empty($show_4[$key->input_date]['all']))   $show_4[$key->input_date]['all'] += 1;
                    else    $show_4[$key->input_date]['all'] = 1;
                    if($key->input_random==1){
                        if(!empty($show_4[$key->input_date]['in_pass']))   $show_4[$key->input_date]['in_pass'] += 1;
                        else    $show_4[$key->input_date]['in_pass'] = 1;
                    }
                    if($key->input_random==0){
                        if(!empty($show_4[$key->input_date]['not_pass']))   $show_4[$key->input_date]['not_pass'] += 1;
                        else    $show_4[$key->input_date]['not_pass'] = 1;
                    }
                    if($key->input_random==0){
                        $to_show_mat[4][$key->input_date][$key->material_id] = $mat[$key->material_id];
                    }
                }
            }
        }
        // dd($to_show_mat);
        //แสดง Chart ข้อที่ 3        
        if(!empty($to_show_all)){                
            if(empty($to_show_all['in_plan']))   $to_show_all['in_plan'] = 0;
            if(empty($to_show_all['out_plan']))   $to_show_all['out_plan'] = 0;
            // dd($to_show_all);
            if(!empty($to_show_all['in_plan'])){
                $in_plan = $to_show_all['in_plan']/($to_show_all['in_plan']+$to_show_all['out_plan'])*100;
            }else{
                $in_plan = 0;
            }
            if(!empty($to_show_all['out_plan'])){
                $out_plan = $to_show_all['out_plan']/($to_show_all['in_plan']+$to_show_all['out_plan'])*100;
            }else{
                $out_plan = 0;
            }
            

            //แสดง Chart ข้อที่ 3 -------------------
            require_once app_path() . '/jpgraph/jpgraph.php';
            require_once app_path() . '/jpgraph/jpgraph_bar.php';
            require_once app_path() . '/jpgraph/jpgraph_pie.php';
            // Some data
            $data = array(round($in_plan, 2), round($out_plan, 2));

            // Create the Pie Graph. 
            $graph = new \PieGraph(900, 400);

            $theme_class="DefaultTheme";
            //$graph->SetTheme(new $theme_class());

            // Set A title for the plot
            $graph->title->SetFont(FF_ANGSA, FS_BOLD, 18);
            $graph->title->Set("รายการรับ วันที่ ".$start_date." ถึงวันที่ ".$end_date);
            $graph->SetBox(true);

            // Create
            $p1 = new \PiePlot($data);
            $graph->Add($p1);

            $p1->ShowBorder();
            $p1->SetColor('black');
            $p1->SetSliceColors(array('#A2FF33','#FF5733'));
            // $p1->title->SetFont(FF_ANGSA, FS_BOLD, 18);
            $p1->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
            $p1->SetLabels(array('ตรงแผน '.round($in_plan, 2).'%', 'ไม่ตรงแผน '.round($out_plan, 2).'%'));
            
            $path = public_path() . '/graph/'.date('Y'). "/" . date('m') . '/mail';
            if (!File::exists($path)) {
                File::makeDirectory($path,  0777, true, true);
            }

            $date = date('ymdHis');

            $chartname['pie']['link'] = "graph/".date('Y'). "/" . date('m') . "/mail/pie_rpt_" .  $date . ".jpg";

            $chartname['pie']['path'] = public_path() . "/graph/" . date('Y') . "/" . date('m') . "/mail/pie_rpt_" .  $date . ".jpg";
            $graph->Stroke($chartname['pie']['path']);
            //แสดง Chart ข้อที่ 3------------จบ------
            // dd($chartname['pie']['path']);
        }

        if($rec_tbl == 'laco'){
            //แสดง Chart ข้อที่ 5 -------------------        
            $query_2 = ProductInput::join('materials', 'product_inputs.material_id', '=', 'materials.id')
                ->join('product_types', 'materials.product_type_id', '=', 'product_types.id')
                ->whereBetween('input_date', [$st_date, $ed_date])->where('product_types.name','<>', '')->where('product_inputs.active',1)
                ->select('product_inputs.input_date', 'product_inputs.id', 'materials.name', 'materials.product_type_id', 'product_types.name AS pt_name', 'product_inputs.input_random')
                ->orderBy('input_date')
                ->get();
            // dd(count($query_2));
            if(count($query_2)>0){
                foreach ($query_2 as $key) {
                    if(preg_replace("/[^a-zA-Z]+/", "", $key->pt_name))// กำหนด ก-เ
                            $to_type = ucwords(strtolower($key->pt_name));
                    else    $to_type = $key->pt_name;
                    $pt_name[$to_type] = 1;
                    if(!empty($toshow['all'][$to_type]))   $toshow['all'][$to_type] += 1;
                    else    $toshow['all'][$to_type] = 1;

                    if(!empty($toshow['all']['Total']))   $toshow['all']['Total'] += 1;
                    else    $toshow['all']['Total'] = 1;

                    if($key->input_random==1){
                        if(!empty($toshow['random'][$to_type]))   $toshow['random'][$to_type] += 1;
                        else    $toshow['random'][$to_type] = 1;

                        if(!empty($toshow['random']['Total']))   $toshow['random']['Total'] += 1;
                        else    $toshow['random']['Total'] = 1;
                    }                   
                }
                $pt_name['Total'] = 1;

                foreach($pt_name as $key=>$value){ 
                    $pd_name[] = $key;
                }
                // dd($pd_name); 
                
                $arr_type = array('all','random');
                foreach($toshow['all'] as $key=>$value){
                    foreach($arr_type as $kpt=>$vpt){
                        if(empty($toshow[$vpt][$key]))    $toshow[$vpt][$key] = 0;  
                    }
                } 
                // dd($toshow);
                foreach($pt_name as $kpt=>$vpt){
                    // echo $kpt.'=>'.ucwords($kpt);
                    $val_1[] = $toshow['all'][$kpt];
                    $val_2[] = $toshow['random'][$kpt];
                }

                $data1y=$val_1;
                $data2y=$val_2;

                $graph2 = new \Graph(900, 400);
                $graph2->SetScale("textlin");

                $theme_class = new \UniversalTheme;
                $graph2->SetTheme($theme_class);
                

                // //$graph->yaxis->SetTickPositions(array(0, 30, 60, 90, 120, 150), array(15, 45, 75, 105, 135));
                $graph2->SetBox(false);

                $graph2->ygrid->SetFill(false);
                $graph2->xaxis->SetTickLabels($pd_name);
                $graph2->xaxis->SetLabelAlign('center', 'center');
                $graph2->xaxis->title->SetFont(FF_ANGSA, FS_BOLD, 18);
                $graph2->xaxis->SetFont(FF_ANGSA, FS_NORMAL, 14);
                $graph2->yaxis->SetColor("#000000");
                $graph2->yaxis->HideLine(true);
                $graph2->yaxis->HideTicks(false, false);


                // Create the bar plots
                $b1plot2 = new \BarPlot($data1y);
                $b2plot2 = new \BarPlot($data2y);

                // Create the grouped bar plot
                $gbplot2 = new \GroupBarPlot(array($b1plot2, $b2plot2));
                // ...and add it to the graPH
                $graph2->Add($gbplot2);

                $b1plot2->SetColor("white");
                $b1plot2->SetFillColor("#4169E1");
                $b1plot2->SetLegend("Sum of list");
                $b1plot2->value->SetFormat('%d');
                $b1plot2->value->SetColor("#000000");
                $b1plot2->value->Show();

                $b2plot2->SetColor("white");
                $b2plot2->SetFillColor("#ff6347");
                $b2plot2->SetLegend("Sum of random result");
                $b2plot2->value->SetFormat('%d');
                $b2plot2->value->SetColor("#000000");
                $b2plot2->value->Show();

                $graph2->ygrid->Show(false);

                $graph2->legend->SetFrameWeight(1);
                $graph2->legend->SetColumns(2);
                $graph2->legend->SetColor('#4E4E4E', '#00A78A');
                $graph2->legend->SetPos(0.5, 0.08, 'center', 'top');

                $graph2->title->SetFont(FF_ANGSA, FS_BOLD, 18);
                $graph2->title->Set("แสดงข้อมูลตามประเภทสินค้า วันที่ ".$start_date." ถึงวันที่ ".$end_date);
                $graph2->SetMargin(30, 10, 40, 20);        

                $date = date('ymdHis');

                $chartname['bar']['link'] = "graph/".date('Y'). "/" . date('m') . "/mail/bar_rpt_" .  $date . ".jpg";

                $chartname['bar']['path'] = public_path() . "/graph/" . date('Y') . "/" . date('m') . "/mail/bar_rpt_" .  $date . ".jpg";
                $graph2->Stroke($chartname['bar']['path']);
            }
            //แสดง Chart ข้อที่ 5 ----------จบ---------
            // dd('bar chart');
        }
        // dd($to_show);
        //excel
        if(count($to_show)>0){
            //excel-----------------
            $spreadsheet = new Spreadsheet();
            $count_sheet = 0;

            //1
                $sheet = $spreadsheet->getActiveSheet();
                
                $sheet->setCellValueByColumnAndRow(1, 1, "วันที่");
                $sheet->mergeCellsByColumnAndRow(1, 1, 1, 2); 
                $sheet->getColumnDimension('A')->setWidth(11);  

                $sheet->setCellValueByColumnAndRow(2, 1, "รับทั้งหมด");            
                $sheet->getColumnDimension('B')->setWidth(9); 

                $sheet->setCellValueByColumnAndRow(3, 1, "รับจริง");
                $sheet->mergeCellsByColumnAndRow(3, 1, 4, 1);
                $sheet->setCellValueByColumnAndRow(5, 1, "% (เปอร์เซ็นต์)");
                $sheet->mergeCellsByColumnAndRow(5, 1, 6, 1);
                
                $sheet->setCellValueByColumnAndRow(2, 2, "จำนวน");
                $sheet->setCellValueByColumnAndRow(3, 2, "ตรงแผน");             
                $sheet->getColumnDimension('C')->setWidth(10);  

                $sheet->setCellValueByColumnAndRow(4, 2, "ไม่ตรงแผน");
                $sheet->getColumnDimension('D')->setWidth(10); 

                $sheet->setCellValueByColumnAndRow(5, 2, "ตรงแผน");
                $sheet->getColumnDimension('E')->setWidth(10); 

                $sheet->setCellValueByColumnAndRow(6, 2, "ไม่ตรงแผน");   
                $sheet->getColumnDimension('F')->setWidth(10); 

                
                $sheet->getStyle('A1:F2')->getAlignment()->setHorizontal('center');         
                $sheet->getStyle('A1:F2')->getAlignment()->setVertical('center');
                $sheet->getStyle('A1:F2')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('FCE4D6');

                $startrow = 3;
                $head_id = "";
                // $startcol = 1;

                foreach ($to_show as $key=>$value) {
                    
                    $startcol = 1;
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, $key);
                    $startcol++;
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, $to_show[$key]['all']);
                    $startcol++;
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, $to_show[$key]['in_plan']);
                    $startcol++;
                    if(!empty($to_show[$key]['out_plan'])){
                        $sheet->setCellValueByColumnAndRow($startcol, $startrow, $to_show[$key]['out_plan']);
                    }
                    $startcol++;
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, number_format($to_show[$key]['in_plan']/$to_show[$key]['all']*100,2));
                    $startcol++;
                    if(!empty($to_show[$key]['out_plan'])){
                        $sheet->setCellValueByColumnAndRow($startcol, $startrow, number_format($to_show[$key]['out_plan']/$to_show[$key]['all']*100,2));
                    }
                    $startrow++;
                }

                $sheet->getStyle('A1:F'.($startrow-1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                $spreadsheet->getActiveSheet()->setTitle('1');
                


            //2
                $count_sheet++;
                $spreadsheet->createSheet();
                $sheet = $spreadsheet->setActiveSheetIndex($count_sheet);

                $sheet->setCellValueByColumnAndRow(1, 1, "วันที่");
                $sheet->mergeCellsByColumnAndRow(1, 1, 1, 2); 
                $sheet->getColumnDimension('A')->setWidth(11); 
                
                $sheet->setCellValueByColumnAndRow(2, 1, "แผนรับเข้า");
                $sheet->getColumnDimension('B')->setWidth(10);  
                $sheet->setCellValueByColumnAndRow(3, 1, "รับจริง");
                $sheet->getColumnDimension('C')->setWidth(10); 

                $sheet->setCellValueByColumnAndRow(4, 1, "ไม่ตรงตามแผน"); 
                $sheet->mergeCellsByColumnAndRow(4, 1, 4, 2); 
                $sheet->getColumnDimension('D')->setWidth(50);  

                $sheet->setCellValueByColumnAndRow(5, 1, "หมายเหตุ"); 
                $sheet->mergeCellsByColumnAndRow(5, 1, 5, 2); 
                $sheet->getColumnDimension('E')->setWidth(20);                       

                $sheet->setCellValueByColumnAndRow(2, 2, "(รายการ)");
                $sheet->setCellValueByColumnAndRow(3, 2, "(รายการ)");           
                
                $sheet->getStyle('A1:E2')->getAlignment()->setHorizontal('center');         
                $sheet->getStyle('A1:E2')->getAlignment()->setVertical('center');
                $sheet->getStyle('A1:E2')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF00');

                $startrow = 3;
                $head_id = "";
                // $startcol = 1;

                foreach ($to_show as $key=>$value) {
                    if(!empty($to_show_mat[2][$key])){
                        $startcol = 1;
                        $sheet->setCellValueByColumnAndRow($startcol, $startrow, $key);                
                        $sheet->mergeCellsByColumnAndRow($startcol, $startrow, $startcol, ($startrow+count($to_show_mat[2][$key])-1));
                        $startcol++;
                        $sheet->setCellValueByColumnAndRow($startcol, $startrow, $to_show[$key]['all']);
                        $sheet->mergeCellsByColumnAndRow($startcol, $startrow, $startcol, ($startrow+count($to_show_mat[2][$key])-1));
                        $startcol++;
                        $sheet->setCellValueByColumnAndRow($startcol, $startrow, $to_show[$key]['in_real']);                
                        $sheet->mergeCellsByColumnAndRow($startcol, $startrow, $startcol, ($startrow+count($to_show_mat[2][$key])-1));
                        $startcol++;
                        foreach($to_show_mat[2][$key] as $kmat=>$vmat){
                            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $mat[$kmat]);
                            $sheet->setCellValueByColumnAndRow(($startcol+1), $startrow, $vmat);
                            $startrow++;
                        }
                    }
                }

                $sheet->getStyle('A1:C'.($startrow-1))->getAlignment()->setVertical('top'); 
                $sheet->getStyle('A1:E'.($startrow-1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
                $spreadsheet->getActiveSheet()->setTitle('2');

            if($rec_tbl == 'laco'){
                $count_sheet++;
                $spreadsheet->createSheet();

                //3
                $sheet = $spreadsheet->setActiveSheetIndex($count_sheet);

                $sheet->setCellValueByColumnAndRow(1, 1, "วันที่");
                $sheet->mergeCellsByColumnAndRow(1, 1, 1, 2); 
                $sheet->getColumnDimension('A')->setWidth(11); 
                
                $sheet->setCellValueByColumnAndRow(2, 1, "ทั้งหมดรับเข้า");
                $sheet->getColumnDimension('B')->setWidth(12);  

                $sheet->setCellValueByColumnAndRow(3, 1, "ค้างสุ่ม");
                $sheet->getColumnDimension('C')->setWidth(10); 
                
                $sheet->setCellValueByColumnAndRow(4, 1, "% ค้างสุ่ม");
                $sheet->mergeCellsByColumnAndRow(4, 1, 4, 2); 
                $sheet->getColumnDimension('D')->setWidth(10); 
                
                $sheet->setCellValueByColumnAndRow(5, 1, "Remark");
                $sheet->mergeCellsByColumnAndRow(5, 1, 5, 2); 
                $sheet->getColumnDimension('E')->setWidth(30); 
                
                $sheet->setCellValueByColumnAndRow(2, 2, "(รายการ)"); 
                $sheet->setCellValueByColumnAndRow(3, 2, "(รายการ)"); 
                $sheet->setCellValueByColumnAndRow(4, 2, "(รายการ)");          
                
                $sheet->getStyle('A1:E2')->getAlignment()->setHorizontal('center');         
                $sheet->getStyle('A1:E2')->getAlignment()->setVertical('center');
                $sheet->getStyle('A1:E2')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF00');
                
                $startrow = 3;
                $head_id = "";
                // $startcol = 1;
                // dd($show_4);
                // dd($to_show_mat[4]);
                foreach ($show_4 as $key=>$value) {
                    if(empty($to_show_mat[4][$key]))
                        $add_row = 1;
                    else
                        $add_row = count($to_show_mat[4][$key]);
                    $startcol = 1;
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, $key);                
                    $sheet->mergeCellsByColumnAndRow($startcol, $startrow, $startcol, ($startrow+$add_row-1));
                    $startcol++;
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, $show_4[$key]['all']);
                    $sheet->mergeCellsByColumnAndRow($startcol, $startrow, $startcol, ($startrow+$add_row-1));
                    $startcol++;
                    if(!empty($show_4[$key]['not_pass'])){
                        $sheet->setCellValueByColumnAndRow($startcol, $startrow, $show_4[$key]['not_pass']);
                    }                
                    $sheet->mergeCellsByColumnAndRow($startcol, $startrow, $startcol, ($startrow+$add_row-1));
                    $startcol++;  
                    if(!empty($show_4[$key]['not_pass'])){
                        $show_not_pass = $show_4[$key]['not_pass'];
                        $show_percent = number_format($show_4[$key]['not_pass']/$show_4[$key]['all']*100,2);
                    }else{
                        $show_not_pass = "";
                        $show_percent = "";
                    }          
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, $show_not_pass);                
                    $sheet->mergeCellsByColumnAndRow($startcol, $startrow, $startcol, ($startrow+$add_row-1)); 
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, $show_percent);                
                    $sheet->mergeCellsByColumnAndRow($startcol, $startrow, $startcol, ($startrow+$add_row-1));
                    $startcol++;

                    if(!empty($show_4[$key]['not_pass'])){
                        foreach($to_show_mat[4][$key] as $kmat=>$vmat){
                            $sheet->setCellValueByColumnAndRow($startcol, $startrow, $vmat);
                            $startrow++;
                        }
                    }else{
                        $sheet->setCellValueByColumnAndRow($startcol, $startrow, "");
                        $startrow++;
                    }
                }
                $sheet->getStyle('A3:D'.($startrow-1))->getAlignment()->setVertical('top'); 
                $sheet->getStyle('A1:E'.($startrow-1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);  
                $spreadsheet->getActiveSheet()->setTitle('3');  
            }

            $spreadsheet->setActiveSheetIndex(0);
                        
            $writer = new Xlsx($spreadsheet);
            $filename = "report_WH-" . date('yymmdd-hi') . ".xlsx";
            $writer->save('storage/app/public/' . $filename);
            // $test_test =  response()->download('storage/' . $filename);
            // dd($test_test);

            // Mail::send(new ReportRM_WH_Mail($to_show,$start_date,$end_date,$mat,$to_show_mat,$show_4,$imageUrl,$imageUrl_5,$filename));

            Mail::send(new ReportRM_WH_Mail($to_show,$start_date,$end_date,$mat,$to_show_mat,$show_4,$filename,$chartname,$rec_tbl));
        }else{
            echo "No data!";
        }
        
    }

    public function sentmail_month(){
        // $to_day = date('Y-m-d',strtotime('2022-09-01'));
        $to_day = date('Y-m-d');
        $yesterday = date('Y-m-d',strtotime($to_day."-1 days"));        
        // dd(date('m',strtotime($yesterday)));
        $thaimonth=array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
        $show_month = $thaimonth[(intval(date('m',strtotime($yesterday))))]." ".(date('Y',strtotime($yesterday))+543);
        
        $tbl_1 = array();
        $tbl_2 = array();
        $row_lable = array();
        $row_list = array();
        $row_rd = array();
        //แสดงเฉพาะรายการที่มีหมวดสินค้า column product_types.name mail ข้อ 3-4
        // $query_1 = ProductInput::join('materials', 'product_inputs.material_id', '=', 'materials.id')
        //     ->join('product_types', 'materials.product_type_id', '=', 'product_types.id')
        //     ->where('product_inputs.active', 1)->where('product_types.name', '<>', "")
        //     ->whereMonth('product_inputs.input_date', date('m',strtotime($yesterday)))->whereYear('product_inputs.input_date', date('Y',strtotime($yesterday)))
        //     ->selectRaw('materials.product_type_id, product_types.name, SUM(product_inputs.input_list) AS input_list, SUM(product_inputs.input_random) AS input_random')
        //     ->groupBy('materials.product_type_id', 'product_types.name')->orderBy('product_types.name')->get(); 

        // SELECT LOWER(product_types.name) AS name, SUM(product_inputs.input_list) AS input_list, SUM(product_inputs.input_random) AS Expr1
        // FROM   product_inputs INNER JOIN
        //                 materials ON product_inputs.material_id = materials.id INNER JOIN
        //                 product_types ON materials.product_type_id = product_types.id
        // WHERE (MONTH(product_inputs.input_date) = 6) AND (YEAR(product_inputs.input_date) = 2022)
        // GROUP BY LOWER(product_types.name)
        $query_1 = ProductInput::join('materials', 'product_inputs.material_id', '=', 'materials.id')
            ->join('product_types', 'materials.product_type_id', '=', 'product_types.id')
            ->where('product_inputs.active', 1)->where('product_types.name', '<>', "")
            ->whereMonth('product_inputs.input_date', date('m',strtotime($yesterday)))->whereYear('product_inputs.input_date', date('Y',strtotime($yesterday)))
            ->selectRaw('LOWER(product_types.name) AS name, SUM(product_inputs.input_list) AS input_list, SUM(product_inputs.input_random) AS input_random')
            ->groupByRaw('LOWER(product_types.name)')->orderByRaw('LOWER(product_types.name)')->get(); 
        if(count($query_1)>0){
            // dd($query_1);
            foreach($query_1 as $key){
                if(preg_replace("/[^a-zA-Z]+/", "", $key->name))// กำหนด ก-เ
                        $to_type = ucwords($key->name);
                else    $to_type = $key->name;

                $tbl_1['name'][$to_type] = $to_type;
                $tbl_1['list'][$to_type] = $key->input_list;
                $tbl_1['random'][$to_type] = $key->input_random;

                // $tbl_1['name'][$key->product_type_id] = $key->name;
                // $tbl_1['list'][$key->product_type_id] = $key->input_list;
                // $tbl_1['random'][$key->product_type_id] = $key->input_random;
            }
            foreach ($tbl_1['name'] as $key => $value) {
                $row_lable[] = $value;
                $row_list[] = $tbl_1['list'][$key];
                $row_rd[] = $tbl_1['random'][$key];
            }
            $row_lable[] = 'Total';
            $row_list[] = array_sum($row_list);
            $row_rd[] = array_sum($row_rd);
            // dd($row_lable);
        }
         
        $excel_1 = array();
        //แสดงเฉพาะรายการที่มีข้อมูลสุ่มใน column input_random mail ข้อ 5
        $query_2 = ProductInput::join('materials', 'product_inputs.material_id', '=', 'materials.id')
            ->join('product_types', 'materials.product_type_id', '=', 'product_types.id')
            ->join('vendors', 'product_inputs.vendor_id', '=', 'vendors.id')
            ->join('units', 'materials.unit_id', '=', 'units.id')
            ->where('product_inputs.active', 1)->whereMonth('product_inputs.input_date', (intval(date('m',strtotime($yesterday)))))
            ->whereYear('product_inputs.input_date', date('Y',strtotime($yesterday)))->whereNotNull('input_random')
            ->selectRaw('product_inputs.input_date, product_inputs.material_id, materials.name AS mat_name, materials.po_shot_text, product_inputs.vendor_id, vendors.name AS vendor_name, product_inputs.po_quantity, 
                units.name AS unit_name, product_inputs.input_real, product_inputs.input_list, product_inputs.input_random, product_types.name AS type_name')
            ->orderBy('product_inputs.input_date')->get(); 
        if(!empty($query_2)){
            foreach($query_2 as $key){
                if(empty($tbl_2['all'][0]))   $tbl_2['all'][0] = 1;
                else $tbl_2['all'][0] += 1;
                if($key->input_random == 1){
                    if(empty($tbl_2['rd_pass'][0]))   $tbl_2['rd_pass'][0] = 1;
                    else $tbl_2['rd_pass'][0] += 1;
                }else{
                    if(empty($tbl_2['rd_not'][0]))   $tbl_2['rd_not'][0] = 1;
                    else $tbl_2['rd_not'][0] += 1;
                    $tbl_2['pd'][] = $key->po_shot_text;
                }  
                $mat[$key->material_id]['name'] = $key->mat_name;
                $mat[$key->material_id]['po_shot'] = $key->po_shot_text;
                $mat[$key->material_id]['unit'] = $key->unit_name;
                $mat[$key->material_id]['type'] = $key->type_name;
                $vendor[$key->vendor_id] = $key->vendor_name;

                $excel_1[$key->input_date][] = $key->material_id.'+-*/'.$key->vendor_id.'+-*/'.$key->po_quantity.'+-*/'.$key->input_real.'+-*/'.$key->input_list.'+-*/'.$key->input_random;                
            }
        }

        //แสดงข้อมูลทั้งหมด mail ข้อ 1-2
        $tbl_add1 = array();
        $query = new ProductInput;
        $query = $query->where('active', 1)->whereMonth('input_date', (intval(date('m',strtotime($yesterday)))))->whereYear('input_date', date('Y',strtotime($yesterday)));
        $tbl_add1['all'] = $query->count();
        $tbl_add1['real'] = $query->where('input_real', '>', 0)->count();
        $tbl_add1['plan'] = $query->whereRaw('po_quantity-input_real=0')->count();
        if(!empty($tbl_add1['plan']))   $tbl_add1['p_inplan'] = $tbl_add1['plan']/$tbl_add1['all']*100;
        else    $tbl_add1['p_inplan'] = 0;
        if(!empty($tbl_add1['all']) && !empty($tbl_add1['plan']))   $tbl_add1['p_outplan'] = ($tbl_add1['all']-$tbl_add1['plan'])/$tbl_add1['all']*100;
        else    $tbl_add1['p_outplan'] = 0;
        // dd($tbl_add1);  


        //แสดง Chart ข้อที่ 2 -------------------
        require_once app_path() . '/jpgraph/jpgraph.php';
        require_once app_path() . '/jpgraph/jpgraph_bar.php';
        require_once app_path() . '/jpgraph/jpgraph_pie.php';
        
        $gp = array();
        if($tbl_add1['p_inplan']!=0 && $tbl_add1['p_outplan']=!0){
            // Some data
            $data = array(round($tbl_add1['p_inplan'], 2), round($tbl_add1['p_outplan'], 2));

            // Create the Pie Graph. 
            $graph = new \PieGraph(900, 400);

            $theme_class="DefaultTheme";
            //$graph->SetTheme(new $theme_class());

            // Set A title for the plot
            $graph->title->SetFont(FF_ANGSA, FS_BOLD, 18);
            $graph->title->Set("สรุปสินค้ารับเข้าสิ้นเดือน ".date('m/Y',strtotime($yesterday)));
            $graph->SetBox(true);

            // Create
            $p1 = new \PiePlot($data);
            $graph->Add($p1);

            $p1->ShowBorder();
            $p1->SetColor('black');
            $p1->SetSliceColors(array('#A2FF33','#FF5733'));
            // $p1->title->SetFont(FF_ANGSA, FS_BOLD, 18);
            $p1->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
            $p1->SetLabels(array('ตรงแผน '.round($tbl_add1['p_inplan'], 2).'%', 'ไม่ตรงแผน '.round($tbl_add1['p_outplan'], 2).'%'));
            
            $path = public_path() . '/graph/'.date('Y'). "/" . date('m') . '/mail';
            if (!File::exists($path)) {
                File::makeDirectory($path,  0777, true, true);
            }

            $date = date('ymdHis');
            $gp['pie']['link'] = "graph/".date('Y'). "/" . date('m') . "/mail/pie_month_" .  $date . ".jpg";  
            $gp['pie']['path'] = $path . "/pie_month_" .  $date . ".jpg"; 
            $graph->Stroke($gp['pie']['path']);
            echo $gp['pie']['path']."\r\n";
        }
        //แสดง Chart ข้อที่ 2------------จบ------


        
        //แสดง Chart ข้อที่ 4-------------------  
        if(count($row_list)>0){
            $data1y=$row_list;
            $data2y=$row_rd;

            $graph = new \Graph(900, 400);
            $graph->SetScale("textlin");

            $graph->SetY2Scale("lin",0,90);
            $graph->SetY2OrderBack(false);

            $theme_class = new \UniversalTheme;
            $graph->SetTheme($theme_class);        

            $graph->SetBox(false);

            $graph->ygrid->SetFill(false);
            $graph->xaxis->SetTickLabels($row_lable);
            $graph->xaxis->SetLabelAlign('center', 'center');
            // $graph->xaxis->title->SetFont(FF_ANGSA, FS_BOLD, 14);
            $graph->xaxis->SetFont(FF_ANGSA, FS_NORMAL, 14);
            // $graph->xaxis->SetLabelAngle(45);
            $graph->yaxis->SetColor("#000000");
            $graph->yaxis->HideLine(false);
            $graph->yaxis->HideTicks(false, false);
            $graph->img->SetMargin(40,20,46,80);

            $b1plot = new \BarPlot($data1y);
            $b2plot = new \BarPlot($data2y);
            $gbplot = new \GroupBarPlot(array($b1plot,$b2plot));
            $graph->Add($gbplot);

            $b1plot->SetColor("#FFFFFF");   //สีเส้น bar
            $b1plot->SetFillColor("#4472C4");   //สี bar
            $b1plot->SetLegend("Sum of รายการ");   //สำหรับใส่ป้ายกำกับ
            $b1plot->value->SetFormat('%g');
            $b1plot->value->SetColor("#000000");
            // $b1plot->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
            // $b1plot->value->SetAngle(90);
            $b1plot->value->Show();

            $b2plot->SetColor("#FFFFFF");   //สีเส้น bar
            $b2plot->SetFillColor("#ED7D31");   //สี bar
            $b2plot->SetLegend("Sum of ผลสุ่ม");  //สำหรับใส่ป้ายกำกับ
            $b2plot->value->SetFormat('%g');
            $b2plot->value->SetColor("#000000");
            // $b2plot->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
            // $b2plot->value->SetAngle(90);
            $b2plot->value->Show();
                    
            $graph->legend->SetFrameWeight(1);
            $graph->legend->SetColumns(2);

            $graph->legend->SetColor('#4472C4','#ED7D31');
            $graph->legend->SetFont(FF_ANGSA, FS_NORMAL, 14);
            $graph->legend->SetAbsPos(10,10,'right','top');
            $graph->title->Set("กราฟแสดงข้อมูลตามประเภทสินค้าในเดือน ".date('m/Y',strtotime($yesterday)));
            $graph->title->SetFont(FF_ANGSA, FS_NORMAL, 18);

            $path = public_path() . '/graph/'.date('Y') . "/" . date('m') . '/mail';
            if (!File::exists($path)) {
                File::makeDirectory($path,  0777, true, true);
            }
            $date = date('ymdHis');
            $gp['bar']['link'] = "graph/".date('Y'). "/" . date('m') . "/mail/bar_month_" .  $date . ".jpg";
            $gp['bar']['path'] = $path . "/bar_month_" .  $date . ".jpg";            
            $graph->Stroke($gp['bar']['path']);
            echo $gp['bar']['path']."\r\n";
            // echo $gp['link']."\r\n";
            // dd($gp['path']);
        }
        //แสดง Chart ข้อที่ 4---------จบ----------


        //excel-----------------
        $spreadsheet = new Spreadsheet();
        $count_sheet = 0;

        $sheet = $spreadsheet->getActiveSheet();
        $startrow = 1;
        //1
            if(count($excel_1)>0){
                
                foreach ($excel_1 as $kdate => $vdate) {
                    $sheet->setCellValueByColumnAndRow(1, $startrow, "วันที่");
                    $sheet->getColumnDimension('A')->setWidth(10);  
                    $sheet->setCellValueByColumnAndRow(2, $startrow, "Material");
                    $sheet->getColumnDimension('B')->setWidth(25); 
                    $sheet->setCellValueByColumnAndRow(3, $startrow, "PO Short text");
                    $sheet->getColumnDimension('C')->setWidth(45);  
                    $sheet->setCellValueByColumnAndRow(4, $startrow, "Vendor Description");
                    $sheet->getColumnDimension('D')->setWidth(48);
                    $sheet->setCellValueByColumnAndRow(5, $startrow, "PO Quantity");
                    $sheet->getColumnDimension('E')->setWidth(17);  
                    $sheet->setCellValueByColumnAndRow(6, $startrow, "Order Unit");
                    $sheet->getColumnDimension('F')->setWidth(16); 
                    $sheet->setCellValueByColumnAndRow(7, $startrow, "รับจริง");
                    $sheet->getColumnDimension('G')->setWidth(17);  
                    $sheet->setCellValueByColumnAndRow(8, $startrow, "รายการ");
                    $sheet->getColumnDimension('H')->setWidth(12);
                    $sheet->setCellValueByColumnAndRow(9, $startrow, "ผลสุ่ม");
                    $sheet->getColumnDimension('I')->setWidth(12);  
                    $sheet->setCellValueByColumnAndRow(10, $startrow, "ประเภท");
                    $sheet->getColumnDimension('J')->setWidth(13); 

                    $sheet->getStyle('A'.$startrow.':J'.$startrow)->getAlignment()->setHorizontal('center');         
                    $sheet->getStyle('A'.$startrow.':J'.$startrow)->getAlignment()->setVertical('center');            
                    // $sheet->getStyle('A'.$startrow.':J'.$startrow)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
                    $sheet->getStyle('A'.$startrow.':J'.$startrow)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('C0C0C0');
                
                    $startrow++;
                    $st_row = $startrow;
                    foreach ($vdate as $key => $value) {
                        $to_explode = explode('+-*/',$value);
                        // dd($to_explode);
                        $sheet->setCellValueByColumnAndRow(1, $startrow, date('m/d/Y',strtotime($kdate)));
                        $sheet->setCellValueByColumnAndRow(2, $startrow, $mat[$to_explode[0]]['name']);
                        $sheet->setCellValueByColumnAndRow(3, $startrow, $mat[$to_explode[0]]['po_shot']);
                        $sheet->setCellValueByColumnAndRow(4, $startrow, $vendor[$to_explode[1]]);
                        $sheet->setCellValueByColumnAndRow(5, $startrow, $to_explode[2]);
                        $sheet->setCellValueByColumnAndRow(6, $startrow, $mat[$to_explode[0]]['unit']);
                        $sheet->setCellValueByColumnAndRow(7, $startrow, $to_explode[3]);
                        $sheet->setCellValueByColumnAndRow(8, $startrow, $to_explode[4]);
                        $sheet->setCellValueByColumnAndRow(9, $startrow, $to_explode[5]);
                        $sheet->setCellValueByColumnAndRow(10, $startrow, $mat[$to_explode[0]]['type']);
                        $startrow++;               
                    } 
                    $row_tot[] = $startrow;
                    $sheet->getStyle('A'.($st_row-1).':J'.($startrow-1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                    $sheet->setCellValue('H'.$startrow, '=SUM(H'.$st_row.':H'.($startrow-1).')');
                    $sheet->setCellValue('I'.$startrow, '=SUM(I'.$st_row.':I'.($startrow-1).')');
                    $sheet->setCellValueByColumnAndRow(10, $startrow, "total");
                    $sheet->getStyle('H'.$startrow.':J'.$startrow)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('FAF547');            
                    $sheet->getStyle('H'.$startrow.':J'.$startrow)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
                    $startrow++; 
                }
                $sum_h = '';
                $sum_i = '';
                foreach ($row_tot as $ksum => $vsum) {
                    $sum_h .= 'H'.$vsum.',';
                    $sum_i .= 'I'.$vsum.',';
                }
                $sheet->setCellValue('H'.$startrow, '=SUM('.$sum_h.')');
                $sheet->setCellValue('I'.$startrow, '=SUM('.$sum_i.')');
                $sheet->setCellValueByColumnAndRow(10, $startrow, "total");
                $sheet->getStyle('H'.$startrow.':J'.$startrow)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('5B9BD5');
                $sheet->getStyle('H'.$startrow.':J'.$startrow)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
            }else{
                $sheet->setCellValueByColumnAndRow(1, $startrow, "ไม่มีข้อมูล");
            }       

            $spreadsheet->getActiveSheet()->setTitle('1');


        //2
            $count_sheet++;
            $spreadsheet->createSheet();
            // $sheet = $spreadsheet->getActiveSheet();
            $sheet = $spreadsheet->setActiveSheetIndex($count_sheet);
            if(count($tbl_1)>0){
                $sheet->setCellValueByColumnAndRow(1, 1, "Row Labels");
                $sheet->getColumnDimension('A')->setWidth(15);  
                $sheet->setCellValueByColumnAndRow(2, 1, "Sum of รายการ");
                $sheet->getColumnDimension('B')->setWidth(12); 
                $sheet->setCellValueByColumnAndRow(3, 1, "Sum of ผลสุ่ม");
                $sheet->getColumnDimension('C')->setWidth(12); 
                $sheet->getStyle('A1:C1')->getAlignment()->setHorizontal('center');         
                $sheet->getStyle('A1:C1')->getAlignment()->setVertical('center'); 
                $sheet->getStyle('A1:C1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('F4F72F');
                $startrow = 2;
                foreach ($tbl_1['name'] as $key => $value) {
                    $sheet->setCellValueByColumnAndRow(1, $startrow, $value);
                    $sheet->setCellValueByColumnAndRow(2, $startrow, $tbl_1['list'][$key]);
                    $sheet->setCellValueByColumnAndRow(3, $startrow, $tbl_1['random'][$key]);
                    $startrow++;
                }
                
                // $sheet->setCellValueByColumnAndRow(1, $startrow, "total");
                // $sheet->setCellValue('B'.$startrow, '=SUM(B2:B'.($startrow-1).')');
                // $sheet->setCellValue('C'.$startrow, '=SUM(C2:C'.($startrow-1).')');
                // $startrow++;

                $sheet->setCellValueByColumnAndRow(1, $startrow, "Grand Total");
                $sheet->setCellValue('B'.$startrow, '=SUM(B2:B'.($startrow-1).')');
                $sheet->setCellValue('C'.$startrow, '=SUM(C2:C'.($startrow-1).')');
                
                $sheet->getStyle('A'.$startrow.':C'.$startrow)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('F4F72F');
                $sheet->getStyle('A1:C'.$startrow)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

            }else{
                $sheet->setCellValueByColumnAndRow(1, 1, "ไม่มีข้อมูล");
            }                     
            
            $spreadsheet->getActiveSheet()->setTitle('2');
        

        //3
            $count_sheet++;
            $spreadsheet->createSheet();
            $sheet = $spreadsheet->setActiveSheetIndex($count_sheet);

            if(count($tbl_2)>0){
                $sheet->setCellValueByColumnAndRow(1, 1, "ทั้งหมดรับเข้า");
                $sheet->getColumnDimension('A')->setWidth(13);  
                $sheet->setCellValueByColumnAndRow(2, 1, "สุ่มผ่าน");
                $sheet->getColumnDimension('B')->setWidth(13); 
                $sheet->setCellValueByColumnAndRow(3, 1, "ค้างสุ่ม");
                $sheet->getColumnDimension('C')->setWidth(13); 
                $sheet->setCellValueByColumnAndRow(4, 1, "ค้างสุ่ม");
                $sheet->getColumnDimension('D')->setWidth(13); 
                $sheet->setCellValueByColumnAndRow(5, 1, "Remark");
                $sheet->getColumnDimension('E')->setWidth(36);
                $sheet->mergeCellsByColumnAndRow(5, 1, 5, 2); 

                $sheet->setCellValueByColumnAndRow(1, 2, "(รายการ)"); 
                $sheet->setCellValueByColumnAndRow(2, 2, "(รายการ)");
                $sheet->setCellValueByColumnAndRow(3, 2, "(รายการ)");
                $sheet->setCellValueByColumnAndRow(4, 2, "(เปอร์เซนต์ %)");

                $sheet->getStyle('A1:E2')->getAlignment()->setHorizontal('center');         
                $sheet->getStyle('A1:E2')->getAlignment()->setVertical('center'); 
                $sheet->getStyle('A1:E2')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('F4F72F');
                
                $startrow = 3;
                $count_row = count($tbl_2['pd']);
                $sheet->setCellValueByColumnAndRow(1, 3, $tbl_2['all'][0]);
                $sheet->mergeCellsByColumnAndRow(1, 3, 1, (2+$count_row));
                $sheet->setCellValueByColumnAndRow(2, 3, $tbl_2['rd_pass'][0]);
                $sheet->mergeCellsByColumnAndRow(2, 3, 2, (2+$count_row));
                $sheet->setCellValue('C3', $tbl_2['rd_not'][0]);
                $sheet->mergeCellsByColumnAndRow(3, 3, 3, (2+$count_row));
                $sheet->setCellValue('D3', '=TEXT(C3/A3*100,"#0.00")');          
                $sheet->mergeCellsByColumnAndRow(4, 3, 4, (2+$count_row));
                $sheet->getStyle('A3:D3')->getAlignment()->setHorizontal('center');         
                $sheet->getStyle('A3:D3')->getAlignment()->setVertical('center'); 

                foreach ($tbl_2['pd'] as $key => $value) {
                    $sheet->setCellValueByColumnAndRow(5, $startrow, $value);
                    $startrow++;
                }   
                $sheet->getStyle('A1:E'.($startrow-1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

            }else{
                $sheet->setCellValueByColumnAndRow(1, 1, "ไม่มีข้อมูล");
            } 

            $spreadsheet->getActiveSheet()->setTitle('3');  


        $spreadsheet->setActiveSheetIndex(0);
                       
        $writer = new Xlsx($spreadsheet);
        $filename = "report_WH_month-" . date('Ymd-hi') . ".xlsx";
        $writer->save('storage/app/public/' . $filename);
        // $test_test =  response()->download('storage/' . $filename);
        // dd($filename);
        
        // Mail::send(new ReportRM_WH_Mail($to_show,$start_date,$end_date,$mat,$to_show_mat,$show_4,$imageUrl,$imageUrl_5,$filename));
        if(count($tbl_1)>0){
            echo $filename;
            Mail::send(new ReportRM_Month_Mail($show_month,$tbl_1,$tbl_2,$gp,$filename,$tbl_add1)); 
        }else{
            echo 'No data!';
        }       
    }

    public function sum_mail($date_to){
        // dd($date_to);

        $to_mat = $this->mail_mat($date_to);
        // dd($to_mat);
        $ed_date = $to_mat[0];
        $st_date = $to_mat[1];
        $mat = $to_mat[2];
        $start_date = $to_mat[3];
        $end_date = $to_mat[4];

        $to_show = array();
        $show_4 =array();
        $to_show_mat = array();
        $to_show_all = array();
        $st = array('laco'=>'LACO', 'en'=>'อะไหล่ช่าง', 'dc'=>'DC วังน้อย');
        // dd($st);
        foreach ($st as $kst => $vst) {
            $to_mail = $this->mail_1($kst, $st_date, $ed_date, $mat);
            $to_show[$kst] = $to_mail[0];       //ข้อที่ 1
            $show_4[$kst] = $to_mail[1];        //ข้อที่ 4
            $to_show_mat[$kst] = $to_mail[2];   //ข้อที่ 2, 4
            $to_show_all[$kst] = $to_mail[3];   //ข้อที่ 3
            
            //แสดง Chart ข้อที่ 3     
            if(!empty($to_show_all[$kst]) && array_sum($to_show_all[$kst])>0){
                $to_mail = $this->mail_3($to_show_all, $kst, $start_date, $end_date);
                $chartname[$kst] = $to_mail[0];
            }
        }
        // dd($to_show_mat);

        //แสดง Chart ข้อที่ 5 
        $to_mail = $this->mail_5($st_date, $ed_date, $start_date, $end_date);
        $chartname['laco']['bar'] = $to_mail[0]['bar']; 
        // dd($chartname);    
        
        //excel dd($to_show);
        if(count($to_show)>0){            
            $to_mail = $this->mail_excel($to_show, $to_show_mat, $mat, $st, $show_4);
            // dd($to_mail);
            $filename = $to_mail[0]; 
            Mail::send(new ReportSum_ProductInput_Mail($to_show,$start_date,$end_date,$mat,$to_show_mat,$show_4,$filename,$chartname,$st));
            
            // $data_show = $to_show;
            // $st_show = $start_date;
            // $ed_show = $end_date;        
            // $mat = $mat;
            // $show_mat = $to_show_mat;
            // $data_show4 = $show_4;     
            // $att_file = $filename;          
            // $chart_name = $chartname;      
            // $st_type = $st; 
            // return view('mail.WH_sum_input', compact('data_show','st_show','ed_show','mat','show_mat','data_show4','chart_name','st_type'));
        
        }else{
            echo "No data!";
        }        
    }

    //mat
    public function mail_mat($date_to){

        $explode_date = explode(':',$date_to);
        $ed_date = $explode_date[0];
        $st_date = $explode_date[1]; 

        $start_date = date ("d/m/Y", strtotime($st_date));
        $end_date = date ("d/m/Y", strtotime($ed_date));

        $mat = array();
        $query_1 = Material::select('id','po_shot_text')->get(); 
        if(!empty($query_1)){
            foreach($query_1 as $key){
                $mat[$key->id] = $key->po_shot_text;
            }
        }
        return [$ed_date, $st_date, $mat, $start_date, $end_date];
    }

    //mail ข้อ 1,2, 4
    public function mail_1($rec_tbl, $st_date, $ed_date, $mat){

        $in_all = array('all','in_plan','out_plan','in_real','in_po');
        $in_4 = array('all','in_pass','not_pass');
        $in_3 = array('in_plan','out_plan');
        $to_show = array();
        $show_4 = array();
        $to_show_all = array();
        $st_while = $st_date;

        while (strtotime($st_while) <= strtotime($ed_date)) {
            // echo $st_date.'</br>';
            foreach ($in_all as $kall => $vall) {
                $to_show[$st_while][$vall] = 0;
            }
            foreach ($in_4 as $kall => $vall) {
                $show_4[$st_while][$vall] = 0;
            }
            $st_while = date ("Y-m-d", strtotime("+1 days", strtotime($st_while)));            
        }

        foreach ($in_3 as $kall => $vall) {
            $to_show_all[$vall] = 0;
        }        
        
        $to_show_mat = array();
        if($rec_tbl == 'laco'){
            $query = ProductInput::whereBetween('input_date', [$st_date, $ed_date])->where('active',1)
                ->selectRaw('id, input_date, po_no, input_real, po_quantity, material_id, input_random, input_real - po_quantity AS plan_input, note')
                ->orderBy('input_date')->orderBy('po_no')->get();
        }else{
            if($rec_tbl == 'en'){
                $query = new ProductInputEn;                            
            }else{
                $query = new ProductInputDc;
            }
            $query = $query->whereBetween('input_date', [$st_date, $ed_date])->where('active',1)
                ->selectRaw('id, input_date, po_no, input_real, po_quantity, material_id, input_real - po_quantity AS plan_input, note')
                ->orderBy('input_date')->orderBy('po_no')->get();
        }
        foreach ($query as $key) {
            $to_show[$key->input_date]['all'] += 1;            
            if($key->plan_input==0){
                $to_show[$key->input_date]['in_plan'] += 1;

                $to_show_all['in_plan'] += 1;       //Chart ข้อที่ 3      
            }else{
                $to_show[$key->input_date]['out_plan'] += 1;

                $to_show_all['out_plan'] += 1;      //Chart ข้อที่ 3 
            }            
            if($key->input_real>0){
                $to_show[$key->input_date]['in_real'] += 1;
            }
            if($key->po_quantity>0){
                $to_show[$key->input_date]['in_po'] += 1;
            }

            if($key->plan_input != 0){ 
                if(empty($key->note)){
                    if($key->po_quantity>0){
                        if($key->plan_input>0){
                            $to_show_mat[2][$key->input_date][$key->po_no][$key->material_id] = 'สินค้าเข้ามากกว่าแผน';
                        }else{
                            $to_show_mat[2][$key->input_date][$key->po_no][$key->material_id] = 'สินค้าเข้าน้อยกว่าแผน'; 
                        }
                    }else{
                        $to_show_mat[2][$key->input_date][$key->po_no][$key->material_id] = 'สินค้าไม่เข้าตามแผน';
                    }
                }else{
                    $to_show_mat[2][$key->input_date][$key->po_no][$key->material_id] = $key->note;
                }
            }
            
            //ข้อ 4
            if($rec_tbl == 'laco'){ 
                if(isset($key->input_random)){
                    $show_4[$key->input_date]['all'] += 1;
                    if($key->input_random==1){
                        $show_4[$key->input_date]['in_pass'] += 1;
                    }
                    if($key->input_random==0){
                        $show_4[$key->input_date]['not_pass'] += 1;
                    }
                    if($key->input_random==0){
                        $to_show_mat[4][$key->input_date][$key->material_id] = $mat[$key->material_id];
                    }
                }
            }
        }
        foreach ($to_show as $key => $value) {
            if($to_show[$key]['all'] == 0){
                unset($to_show[$key]);
            }
        }
        // dd($to_show_all);
        return [$to_show, $show_4, $to_show_mat, $to_show_all];
    }

    //mail Chart ข้อที่ 3
    public function mail_3($to_show_all, $rec_tbl, $start_date, $end_date){              
        // dd($to_show_all);
        if(!empty($to_show_all[$rec_tbl]['in_plan'])){
            $in_plan = $to_show_all[$rec_tbl]['in_plan']/($to_show_all[$rec_tbl]['in_plan']+$to_show_all[$rec_tbl]['out_plan'])*100;
        }else{
            $in_plan = 0;
        }
        if(!empty($to_show_all[$rec_tbl]['out_plan'])){
            $out_plan = $to_show_all[$rec_tbl]['out_plan']/($to_show_all[$rec_tbl]['in_plan']+$to_show_all[$rec_tbl]['out_plan'])*100;
        }else{
            $out_plan = 0;
        }        

        //แสดง Chart ข้อที่ 3 -------------------
        require_once app_path() . '/jpgraph/jpgraph.php';
        require_once app_path() . '/jpgraph/jpgraph_pie.php';
        // Some data
        $data = array(round($in_plan, 2), round($out_plan, 2));

        // Create the Pie Graph. 
        $graph = new \PieGraph(250, 250);

        $theme_class="DefaultTheme";
        //$graph->SetTheme(new $theme_class());

        // Set A title for the plot
        //show title
        // $graph->title->SetFont(FF_ANGSA, FS_BOLD, 18);
        // $graph->title->Set("รายการรับ วันที่ ".$start_date." ถึงวันที่ ".$end_date);
        // $graph->SetBox(true);

        // Create
        $p1 = new \PiePlot($data);
        $graph->Add($p1);

        $p1->ShowBorder();
        $p1->SetColor('black');
        $p1->SetSliceColors(array('#A2FF33','#FF5733'));
        // $p1->title->SetFont(FF_ANGSA, FS_BOLD, 18);
        $p1->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
        $p1->SetLabels(array('ตรงแผน '.round($in_plan, 2).'%', 'ไม่ตรงแผน '.round($out_plan, 2).'%'));
        
        $path = public_path() . '/graph/'.date('Y'). "/" . date('m') . '/mail';
        if (!File::exists($path)) {
            File::makeDirectory($path,  0777, true, true);
        }

        $date = date('ymdHis');

        $chartname['pie']['link'] = "graph/".date('Y'). "/" . date('m') . "/mail/pie_".$rec_tbl."_rpt_" .  $date . ".jpg";

        $chartname['pie']['path'] = public_path() . "/graph/" . date('Y') . "/" . date('m') . "/mail/pie_".$rec_tbl."_rpt_" .  $date . ".jpg";
        $graph->Stroke($chartname['pie']['path']);
        //แสดง Chart ข้อที่ 3------------จบ------
        // dd($chartname['pie']['path']);
        return [$chartname];
    }    
    
    //mail Chart ข้อที่ 5
    public function mail_5($st_date, $ed_date, $start_date, $end_date){
        $query = ProductInput::join('materials', 'product_inputs.material_id', '=', 'materials.id')
            ->join('product_types', 'materials.product_type_id', '=', 'product_types.id')
            ->whereBetween('input_date', [$st_date, $ed_date])->where('product_types.name','<>', '')->where('product_inputs.active',1)
            ->select('product_inputs.input_date', 'product_inputs.id', 'materials.name', 'materials.product_type_id', 'product_types.name AS pt_name', 'product_inputs.input_random')
            ->orderBy('input_date')
            ->get();
        // dd(count($query_2));
        if(count($query)>0){
            foreach ($query as $key) {
                if(preg_replace("/[^a-zA-Z]+/", "", $key->pt_name))// กำหนด ก-เ
                        $to_type = ucwords(strtolower($key->pt_name));
                else    $to_type = $key->pt_name;
                $pt_name[$to_type] = 1;
                if(!empty($toshow['all'][$to_type]))   $toshow['all'][$to_type] += 1;
                else    $toshow['all'][$to_type] = 1;

                if(!empty($toshow['all']['Total']))   $toshow['all']['Total'] += 1;
                else    $toshow['all']['Total'] = 1;

                if($key->input_random==1){
                    if(!empty($toshow['random'][$to_type]))   $toshow['random'][$to_type] += 1;
                    else    $toshow['random'][$to_type] = 1;

                    if(!empty($toshow['random']['Total']))   $toshow['random']['Total'] += 1;
                    else    $toshow['random']['Total'] = 1;
                }                   
            }
            $pt_name['Total'] = 1;

            foreach($pt_name as $key=>$value){ 
                $pd_name[] = $key;
            }
            // dd($pd_name); 
            
            $arr_type = array('all','random');
            foreach($toshow['all'] as $key=>$value){
                foreach($arr_type as $kpt=>$vpt){
                    if(empty($toshow[$vpt][$key]))    $toshow[$vpt][$key] = 0;  
                }
            } 
            // dd($toshow);
            foreach($pt_name as $kpt=>$vpt){
                // echo $kpt.'=>'.ucwords($kpt);
                $val_1[] = $toshow['all'][$kpt];
                $val_2[] = $toshow['random'][$kpt];
            }

            $data1y=$val_1;
            $data2y=$val_2;

            require_once app_path() . '/jpgraph/jpgraph.php';
            require_once app_path() . '/jpgraph/jpgraph_bar.php';

            $graph2 = new \Graph(900, 400);
            $graph2->SetScale("textlin");

            $theme_class = new \UniversalTheme;
            $graph2->SetTheme($theme_class);
            

            // //$graph->yaxis->SetTickPositions(array(0, 30, 60, 90, 120, 150), array(15, 45, 75, 105, 135));
            $graph2->SetBox(false);

            $graph2->ygrid->SetFill(false);
            $graph2->xaxis->SetTickLabels($pd_name);
            $graph2->xaxis->SetLabelAlign('center', 'center');
            $graph2->xaxis->title->SetFont(FF_ANGSA, FS_BOLD, 18);
            $graph2->xaxis->SetFont(FF_ANGSA, FS_NORMAL, 14);
            $graph2->yaxis->SetColor("#000000");
            $graph2->yaxis->HideLine(true);
            $graph2->yaxis->HideTicks(false, false);


            // Create the bar plots
            $b1plot2 = new \BarPlot($data1y);
            $b2plot2 = new \BarPlot($data2y);

            // Create the grouped bar plot
            $gbplot2 = new \GroupBarPlot(array($b1plot2, $b2plot2));
            // ...and add it to the graPH
            $graph2->Add($gbplot2);

            $b1plot2->SetColor("white");
            $b1plot2->SetFillColor("#4169E1");
            $b1plot2->SetLegend("Sum of list");
            $b1plot2->value->SetFormat('%d');
            $b1plot2->value->SetColor("#000000");
            $b1plot2->value->Show();

            $b2plot2->SetColor("white");
            $b2plot2->SetFillColor("#ff6347");
            $b2plot2->SetLegend("Sum of random result");
            $b2plot2->value->SetFormat('%d');
            $b2plot2->value->SetColor("#000000");
            $b2plot2->value->Show();

            $graph2->ygrid->Show(false);

            $graph2->legend->SetFrameWeight(1);
            $graph2->legend->SetColumns(2);
            $graph2->legend->SetColor('#4E4E4E', '#00A78A');
            $graph2->legend->SetPos(0.5, 0.08, 'center', 'top');

            $graph2->title->SetFont(FF_ANGSA, FS_BOLD, 18);
            $graph2->title->Set("แสดงข้อมูลตามประเภทสินค้ารายการรับเข้า LACO วันที่ ".$start_date." ถึงวันที่ ".$end_date);
            $graph2->SetMargin(30, 10, 40, 20);        

            $date = date('ymdHis');

            $chartname['bar']['link'] = "graph/".date('Y'). "/" . date('m') . "/mail/bar_rpt_" .  $date . ".jpg";

            $chartname['bar']['path'] = public_path() . "/graph/" . date('Y') . "/" . date('m') . "/mail/bar_rpt_" .  $date . ".jpg";
            $graph2->Stroke($chartname['bar']['path']);
        }
        return [$chartname];
    }

    //excel
    public function mail_excel($to_show, $to_show_mat, $mat, $st, $show_4){
        // dd($to_show_mat);
        $spreadsheet = new Spreadsheet();
        $count_sheet = 0;
        // echo '1</br>';
        //1
            $sheet = $spreadsheet->getActiveSheet();
            
            $sheet->setCellValueByColumnAndRow(1, 1, "รับเข้า");
            $sheet->mergeCellsByColumnAndRow(1, 1, 1, 2); 
            $sheet->setCellValueByColumnAndRow(2, 1, "วันที่");
            $sheet->mergeCellsByColumnAndRow(2, 1, 2, 2); 
            $sheet->setCellValueByColumnAndRow(3, 1, "รับทั้งหมด");
            $sheet->setCellValueByColumnAndRow(4, 1, "รับจริง");
            $sheet->mergeCellsByColumnAndRow(4, 1, 5, 1);
            $sheet->setCellValueByColumnAndRow(6, 1, "% (เปอร์เซ็นต์)");
            $sheet->mergeCellsByColumnAndRow(6, 1, 7, 1);
            
            $sheet->setCellValueByColumnAndRow(3, 2, "จำนวน");
            $sheet->setCellValueByColumnAndRow(4, 2, "ตรงแผน");  
            $sheet->setCellValueByColumnAndRow(5, 2, "ไม่ตรงแผน");
            $sheet->setCellValueByColumnAndRow(6, 2, "ตรงแผน");
            $sheet->setCellValueByColumnAndRow(7, 2, "ไม่ตรงแผน");  
            
            $sheet->getStyle('A1:G2')->getAlignment()->setHorizontal('center');         
            $sheet->getStyle('A1:G2')->getAlignment()->setVertical('center');
            $sheet->getStyle('A1:G2')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('FCE4D6');

            $startrow = 3;
            $head_id = "";
            // $startcol = 1;
            foreach ($st as $kst => $vst) {
                $sheet->setCellValueByColumnAndRow(1, $startrow, $vst);
                $sheet->mergeCellsByColumnAndRow(1, $startrow, 7, $startrow);
                $sheet->getStyle('A'.$startrow)->getAlignment()->setHorizontal('left');         
                $sheet->getStyle('A'.$startrow)->getAlignment()->setVertical('center');
                $startrow++;
                $st_row = $startrow;
                foreach ($to_show[$kst] as $key=>$value) {    
                    $sheet->setCellValueByColumnAndRow(2, $startrow, date('d/m/Y',strtotime($key)));
                    $sheet->setCellValueByColumnAndRow(3, $startrow, $to_show[$kst][$key]['all']);
                    $sheet->setCellValueByColumnAndRow(4, $startrow, $to_show[$kst][$key]['in_plan']);
                    if(!empty($to_show[$kst][$key]['out_plan'])){
                        $sheet->setCellValueByColumnAndRow(5, $startrow, $to_show[$kst][$key]['out_plan']);
                    }
                    if(!empty($to_show[$kst][$key]['in_plan']))
                        $sheet->setCellValueByColumnAndRow(6, $startrow, number_format($to_show[$kst][$key]['in_plan']/$to_show[$kst][$key]['all']*100,2));                    
                    if(!empty($to_show[$kst][$key]['out_plan'])){
                        $sheet->setCellValueByColumnAndRow(7, $startrow, number_format($to_show[$kst][$key]['out_plan']/$to_show[$kst][$key]['all']*100,2));
                    }
                    $startrow++;
                }
                if($st_row < $startrow){
                    $sheet->mergeCellsByColumnAndRow(1, $st_row, 1, ($startrow-1));
                }
            }

            $col_n = array('A','B','C','D','E','F','G');
            foreach ($col_n as $key => $value) {                
                $sheet->getColumnDimension($value)->setAutoSize(true);
            }

            // $sheet->getStyle('A1:A'.($startrow-1))->getAlignment()->setHorizontal('center');         
            // $sheet->getStyle('A1:A'.($startrow-1))->getAlignment()->setVertical('center');

            $sheet->getStyle('A1:G'.($startrow-1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

            $spreadsheet->getActiveSheet()->setTitle('สรุปรับเข้า');


        // echo '2</br>';
        //2
            $count_sheet++;
            $spreadsheet->createSheet();
            $sheet = $spreadsheet->setActiveSheetIndex($count_sheet);

            $sheet->setCellValueByColumnAndRow(1, 1, "รับเข้า");
            $sheet->mergeCellsByColumnAndRow(1, 1, 1, 2);
            $sheet->setCellValueByColumnAndRow(2, 1, "วันที่");
            $sheet->mergeCellsByColumnAndRow(2, 1, 2, 2); 
            $sheet->setCellValueByColumnAndRow(3, 1, "แผนรับเข้า");
            $sheet->setCellValueByColumnAndRow(4, 1, "รับจริง");
            $sheet->setCellValueByColumnAndRow(5, 1, "PO No."); 
            $sheet->mergeCellsByColumnAndRow(5, 1, 5, 2); 
            $sheet->setCellValueByColumnAndRow(6, 1, "ไม่ตรงตามแผน"); 
            $sheet->mergeCellsByColumnAndRow(6, 1, 6, 2); 
            $sheet->setCellValueByColumnAndRow(7, 1, "หมายเหตุ"); 
            $sheet->mergeCellsByColumnAndRow(7, 1, 7, 2);                      

            $sheet->setCellValueByColumnAndRow(3, 2, "(รายการ)");
            $sheet->setCellValueByColumnAndRow(4, 2, "(รายการ)");           
            
            $sheet->getStyle('A1:G2')->getAlignment()->setHorizontal('center');         
            $sheet->getStyle('A1:G2')->getAlignment()->setVertical('center');
            $sheet->getStyle('A1:G2')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF00');

            $startrow = 3;
            $head_id = "";
            foreach ($st as $kst => $vst) {
                if(!empty($to_show_mat[$kst][2])){
                    $sheet->setCellValueByColumnAndRow(1, $startrow, $vst);
                    $sheet->mergeCellsByColumnAndRow(1, $startrow, 6, $startrow);
                    $sheet->getStyle('A'.$startrow)->getAlignment()->setHorizontal('left');         
                    $sheet->getStyle('A'.$startrow)->getAlignment()->setVertical('center');
                    $startrow++;
                    $st_row = $startrow;
                    foreach ($to_show[$kst] as $key=>$value) {                    
                        if(!empty($to_show_mat[$kst][2][$key])){
                            $sheet->setCellValueByColumnAndRow(2, $startrow, date('d/m/Y',strtotime($key)));                
                            $sheet->mergeCellsByColumnAndRow(2, $startrow, 2, ($startrow+array_sum(array_map("count", $to_show_mat[$kst][2][$key]))-1));
                            $sheet->setCellValueByColumnAndRow(3, $startrow, $to_show[$kst][$key]['all']);
                            $sheet->mergeCellsByColumnAndRow(3, $startrow, 3, ($startrow+array_sum(array_map("count", $to_show_mat[$kst][2][$key]))-1));
                            $sheet->setCellValueByColumnAndRow(4, $startrow, $to_show[$kst][$key]['in_real']);                
                            $sheet->mergeCellsByColumnAndRow(4, $startrow, 4, ($startrow+array_sum(array_map("count", $to_show_mat[$kst][2][$key]))-1));                        
                            foreach ($to_show_mat[$kst][2][$key] as $kpo=>$vpo) {
                                // echo 'kpo->'.$kpo.'--';
                                // echo sizeof($to_show_mat[$kst][2][$key][$kpo]).'\n\r';
                                $sheet->setCellValueByColumnAndRow(5, $startrow, $kpo);
                                $sheet->mergeCellsByColumnAndRow(5, $startrow, 5, ($startrow+count($to_show_mat[$kst][2][$key][$kpo])-1));
                                foreach($vpo as $kmat=>$vmat){
                                    // echo '2.startrow->'.$startrow.'--';
                                    $sheet->setCellValueByColumnAndRow(6, $startrow, $mat[$kmat]);
                                    $sheet->setCellValueByColumnAndRow(7, $startrow, $vmat);
                                    $startrow++;
                                }
                            }
                        }
                    }
                    if($st_row < $startrow){
                        $sheet->mergeCellsByColumnAndRow(1, $st_row, 1, ($startrow-1));
                    }
                }
            }
            $col_n = array('A','B','C','D','E','F','G');
            foreach ($col_n as $key => $value) {                
                $sheet->getColumnDimension($value)->setAutoSize(true);
            }

            // $sheet->getStyle('A1:A'.($startrow-1))->getAlignment()->setHorizontal('center');         
            // $sheet->getStyle('A1:A'.($startrow-1))->getAlignment()->setVertical('center');

            $sheet->getStyle('B1:D'.($startrow-1))->getAlignment()->setVertical('top'); 
            $sheet->getStyle('A1:G'.($startrow-1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
            $spreadsheet->getActiveSheet()->setTitle('ไม่ตรงแผน');       

        // echo '3</br>';
        //3
            $count_sheet++;
            $spreadsheet->createSheet();
            $sheet = $spreadsheet->setActiveSheetIndex($count_sheet);

            $sheet->setCellValueByColumnAndRow(1, 1, "วันที่");
            $sheet->mergeCellsByColumnAndRow(1, 1, 1, 2); 
            $sheet->getColumnDimension('A')->setWidth(11); 
            
            $sheet->setCellValueByColumnAndRow(2, 1, "ทั้งหมดรับเข้า");
            $sheet->getColumnDimension('B')->setWidth(12);  

            $sheet->setCellValueByColumnAndRow(3, 1, "ค้างสุ่ม");
            $sheet->getColumnDimension('C')->setWidth(10); 
            
            $sheet->setCellValueByColumnAndRow(4, 1, "% ค้างสุ่ม");
            $sheet->mergeCellsByColumnAndRow(4, 1, 4, 2); 
            $sheet->getColumnDimension('D')->setWidth(10); 
            
            $sheet->setCellValueByColumnAndRow(5, 1, "Remark");
            $sheet->mergeCellsByColumnAndRow(5, 1, 5, 2); 
            $sheet->getColumnDimension('E')->setWidth(30); 
            
            $sheet->setCellValueByColumnAndRow(2, 2, "(รายการ)"); 
            $sheet->setCellValueByColumnAndRow(3, 2, "(รายการ)"); 
            $sheet->setCellValueByColumnAndRow(4, 2, "(รายการ)");          
            
            $sheet->getStyle('A1:E2')->getAlignment()->setHorizontal('center');         
            $sheet->getStyle('A1:E2')->getAlignment()->setVertical('center');
            $sheet->getStyle('A1:E2')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF00');
            
            $startrow = 3;
            $head_id = "";
            // $startcol = 1;
            // dd($show_4);
            // dd($to_show_mat[4]);
            foreach ($show_4['laco'] as $key=>$value) {
                if(empty($to_show_mat['laco'][4][$key]))
                    $add_row = 1;
                else
                    $add_row = count($to_show_mat['laco'][4][$key]);
                $startcol = 1;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, date('d/m/Y',strtotime($key)));                
                $sheet->mergeCellsByColumnAndRow($startcol, $startrow, $startcol, ($startrow+$add_row-1));
                $startcol++;
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $show_4['laco'][$key]['all']);
                $sheet->mergeCellsByColumnAndRow($startcol, $startrow, $startcol, ($startrow+$add_row-1));
                $startcol++;
                if(!empty($show_4['laco'][$key]['not_pass'])){
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, $show_4['laco'][$key]['not_pass']);
                }                
                $sheet->mergeCellsByColumnAndRow($startcol, $startrow, $startcol, ($startrow+$add_row-1));
                $startcol++;  
                if(!empty($show_4['laco'][$key]['not_pass'])){
                    $show_not_pass = $show_4['laco'][$key]['not_pass'];
                    $show_percent = number_format($show_4['laco'][$key]['not_pass']/$show_4['laco'][$key]['all']*100,2);
                }else{
                    $show_not_pass = "";
                    $show_percent = "";
                }          
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $show_not_pass);                
                $sheet->mergeCellsByColumnAndRow($startcol, $startrow, $startcol, ($startrow+$add_row-1)); 
                $sheet->setCellValueByColumnAndRow($startcol, $startrow, $show_percent);                
                $sheet->mergeCellsByColumnAndRow($startcol, $startrow, $startcol, ($startrow+$add_row-1));
                $startcol++;

                if(!empty($show_4['laco'][$key]['not_pass'])){
                    foreach($to_show_mat['laco'][4][$key] as $kmat=>$vmat){
                        $sheet->setCellValueByColumnAndRow($startcol, $startrow, $vmat);
                        $startrow++;
                    }
                }else{
                    $sheet->setCellValueByColumnAndRow($startcol, $startrow, "");
                    $startrow++;
                }
            }
            $sheet->getStyle('A3:D'.($startrow-1))->getAlignment()->setVertical('top'); 
            $sheet->getStyle('A1:E'.($startrow-1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);  
            $spreadsheet->getActiveSheet()->setTitle('สรุปการสุ่ม รับเข้า LACO');  

        $spreadsheet->setActiveSheetIndex(0);
                    
        $writer = new Xlsx($spreadsheet);
        $filename = "report_WH-" . date('yymmdd-hi') . ".xlsx";

        $path = public_path() . '/storage/app/public/productinput';
        if (!File::exists($path)) {
            File::makeDirectory($path,  0777, true, true);
        }

        $writer->save($path .'/'. $filename);

        return [$filename];
    }

    public function sum_mail_month(){
        // $to_day = date('Y-m-d',strtotime('2022-09-01'));
        $to_day = date('Y-m-d');
        $yesterday = date('Y-m-d',strtotime($to_day."-1 days"));        
        // dd(date('m',strtotime($yesterday)));
        $thaimonth=array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
        $show_month = $thaimonth[(intval(date('m',strtotime($yesterday))))]." ".(date('Y',strtotime($yesterday))+543);
        
        $mat = array();
        $query_1 = Material::join('units', 'materials.unit_id', '=', 'units.id')
            ->join('product_types', 'materials.product_type_id', '=', 'product_types.id')
            ->select('materials.id', 'materials.name AS mat_name', 'materials.po_shot_text', 'units.name AS unit_name',
             'product_types.name AS type_name')->get(); 
        if(!empty($query_1)){
            foreach($query_1 as $key){
                $mat[$key->id]['name'] = $key->mat_name;
                $mat[$key->id]['po_shot'] = $key->po_shot_text;
                $mat[$key->id]['unit'] = $key->unit_name;
                $mat[$key->id]['type'] = $key->type_name;
            }
        }
        
        $vendor = array();
        $query_1 = Vendor::all(); 
        if(!empty($query_1)){
            foreach($query_1 as $key){
                $vendor[$key->id] = $key->name;
            }
        }
        
        $st = array('laco'=>'LACO', 'en'=>'อะไหล่ช่าง', 'dc'=>'DC วังน้อย');        
        $tbl_1 = array();
        $tbl_2 = array();
        $chart_4 = array();
        $excel_1 = array(); 
        $tbl_add1 = array(); 
        foreach ($st as $kst => $vst) {        
            $to_mail = $this->mail_data_month($kst, $yesterday);
            if($kst=='laco'){
                $tbl_1= $to_mail[0];        //excel 2
                $chart_4= $to_mail[1];      //chart ข้อที่ 4
                $tbl_2 = $to_mail[2];       //excel 3 
            }             
            $excel_1[$kst] = $to_mail[3];       //excel 1 
            $tbl_add1[$kst] = $to_mail[4];      //ข้อที่ 1-2 
        }        
        // dd($tbl_add1);

        //แสดง Chart ข้อที่ 2 ------------------- 
        $gp = array();
        foreach ($st as $kst => $vst) {           
            if(!empty($tbl_add1[$kst]['p_inplan']) && !empty($tbl_add1[$kst]['p_outplan'])){
                $to_mail = $this->mail_2_month($kst, $tbl_add1, $show_month); 
                $gp[$kst] = $to_mail[0];         
            }
        } 
        //แสดง Chart ข้อที่ 2------------จบ------
        // dd($gp);
        //แสดง Chart ข้อที่ 4-------------------  
        if(count($chart_4['list'])>0){
            $to_mail = $this->mail_4_month($chart_4, $show_month);
            $gp['laco']['bar'] = $to_mail[0]['bar'];
        }
        //แสดง Chart ข้อที่ 4---------จบ----------

        //excel-----------------
        $to_mail = $this->mail_excel_month($st, $mat, $excel_1, $tbl_1, $tbl_2, $vendor);
        $filename = $to_mail[0];
        
        if(count($tbl_1)>0){
            // echo $filename;
            Mail::send(new ReportSumMonth_ProductInput_Mail($show_month,$st,$tbl_1,$tbl_2,$gp,$filename,$tbl_add1,$mat));
            // dd($gp);
            // $tbl_add = $tbl_add1;
            // return view('mail.WH_sum_input_month', compact('show_month','st','tbl_1','tbl_2','gp','tbl_add','mat'));
        
        }else{
            echo 'No data!';
        }       
    }

    public function mail_data_month($rec_tbl, $yesterday){
        $tbl_1 = array();
        $tbl_2 = array();
        $chart_4 = array();
        $excel_1 = array();
        $tbl_add1 = array();

        if($rec_tbl=='laco'){
            $query_1 = ProductInput::join('materials', 'product_inputs.material_id', '=', 'materials.id')
                ->join('product_types', 'materials.product_type_id', '=', 'product_types.id')
                ->where('product_inputs.active', 1)->where('product_types.name', '<>', "")->where('product_inputs.input_list', 1)
                ->whereMonth('product_inputs.input_date', date('m',strtotime($yesterday)))->whereYear('product_inputs.input_date', date('Y',strtotime($yesterday)))
                ->selectRaw('LOWER(product_types.name) AS name, SUM(product_inputs.input_list) AS input_list, SUM(product_inputs.input_random) AS input_random')
                ->groupByRaw('LOWER(product_types.name)')->orderByRaw('LOWER(product_types.name)')->get(); 
            // dd($query_1);
            if(count($query_1)>0){            
                foreach($query_1 as $key){
                    if(preg_replace("/[^a-zA-Z]+/", "", $key->name))// กำหนด ก-เ
                            $to_type = ucwords($key->name);
                    else    $to_type = $key->name;
                    if(empty($key->input_list)) $key->input_list = 0; 
                    if(empty($key->input_random)) $key->input_list = 0; 
                    if($key->input_list+$key->input_random>0){
                        $tbl_1['name'][$to_type] = $to_type;
                        $tbl_1['list'][$to_type] = $key->input_list;
                        $tbl_1['random'][$to_type] = $key->input_random;
                    }
                    

                }
                foreach ($tbl_1['name'] as $key => $value) {
                    $chart_4['label'][] = $value;
                    $chart_4['list'][] = $tbl_1['list'][$key];
                    $chart_4['random'][] = $tbl_1['random'][$key];
                }
                $chart_4['label'][] = 'Total';
                $chart_4['list'][] = array_sum($chart_4['list']);
                $chart_4['random'][] = array_sum($chart_4['random']);
                // dd($chart_4);
            }
        }         
        
        //แสดงเฉพาะรายการที่มีข้อมูลสุ่มใน column input_random mail ข้อ 5        
        if($rec_tbl=='laco'){
            $query_2 = ProductInput::where('active', 1)->whereMonth('input_date', (intval(date('m',strtotime($yesterday)))))
                ->whereYear('input_date', date('Y',strtotime($yesterday)))->where('input_list', 1)
                ->selectRaw('input_date, po_no, material_id, vendor_id, po_quantity, input_real, input_list, input_random')
                ->orderBy('input_date')->orderBy('po_no')->get(); 
        }else{
            if($rec_tbl=='en'){
                $query_2 = new ProductInputEn;
            }else{
                $query_2 = new ProductInputDc;
            }
            $query_2 = $query_2->where('active', 1)->whereMonth('input_date', (intval(date('m',strtotime($yesterday)))))
                ->whereYear('input_date', date('Y',strtotime($yesterday)))->where('input_list', 1)
                ->selectRaw('input_date, po_no, material_id, vendor_id, po_quantity, input_real, input_list')
                ->orderBy('input_date')->orderBy('po_no')->get();
        }
        // dd($query_2);   
        if(!empty($query_2)){
            // echo $rec_tbl.'-'.count($query_2);
            foreach($query_2 as $key){
                if($rec_tbl=='laco'){
                    if(empty($tbl_2['all'][0]))   $tbl_2['all'][0] = 1;
                    else $tbl_2['all'][0] += 1;
                    if($key->input_random == 1){
                        if(empty($tbl_2['rd_pass'][0]))   $tbl_2['rd_pass'][0] = 1;
                        else $tbl_2['rd_pass'][0] += 1;
                    }else{
                        if(empty($tbl_2['rd_not'][0]))   $tbl_2['rd_not'][0] = 1;
                        else $tbl_2['rd_not'][0] += 1;
                        $tbl_2['pd'][] = $key->material_id;
                        // $tbl_2['po_no'][] = $key->po_no;
                    } 
                    $excel_1[$key->input_date][] = $key->po_no.'+-*/'.$key->material_id.'+-*/'.$key->vendor_id.'+-*/'.$key->po_quantity.'+-*/'.$key->input_real.'+-*/'.$key->input_list.'+-*/'.$key->input_random; 
                }else{
                    $excel_1[$key->input_date][] = $key->po_no.'+-*/'.$key->material_id.'+-*/'.$key->vendor_id.'+-*/'.$key->po_quantity.'+-*/'.$key->input_real.'+-*/'.$key->input_list;
                }
            }
        }

        //แสดงข้อมูลทั้งหมด mail ข้อ 1-2        
        if($rec_tbl=='laco')    $query = new ProductInput;
        elseif($rec_tbl=='en')  $query = new ProductInputEn;
        else                    $query = new ProductInputDc;        
        $query = $query->where('active', 1)->whereMonth('input_date', (intval(date('m',strtotime($yesterday)))))->whereYear('input_date', date('Y',strtotime($yesterday)));
        $tbl_add1['all'] = $query->count();
        $tbl_add1['real'] = $query->where('input_list', 1)->count();
        $tbl_add1['plan'] = $query->whereRaw('po_quantity-input_real=0')->count();
        if(!empty($tbl_add1['plan']))   $tbl_add1['p_inplan'] = $tbl_add1['plan']/$tbl_add1['all']*100;
        else    $tbl_add1['p_inplan'] = 0;
        if(!empty($tbl_add1['all']) && !empty($tbl_add1['plan']))   $tbl_add1['p_outplan'] = ($tbl_add1['all']-$tbl_add1['plan'])/$tbl_add1['all']*100;
        else    $tbl_add1['p_outplan'] = 0;
        
        // dd($tbl_add1);

        return [$tbl_1, $chart_4, $tbl_2, $excel_1, $tbl_add1];         
    }

    public function mail_2_month($rec_tbl, $tbl_add1, $show_month){
        
        require_once app_path() . '/jpgraph/jpgraph.php';
        require_once app_path() . '/jpgraph/jpgraph_pie.php';
        
        $gp = array();
        // Some data
        $data = array(round($tbl_add1[$rec_tbl]['p_inplan'], 2), round($tbl_add1[$rec_tbl]['p_outplan'], 2));
        // Create the Pie Graph. 
        $graph = new \PieGraph(250, 250);

        $theme_class="DefaultTheme";
        //$graph->SetTheme(new $theme_class());

        // Set A title for the plot
        $graph->title->SetFont(FF_ANGSA, FS_BOLD, 18);
        // $graph->title->Set("สรุปสินค้ารับเข้าสิ้นเดือน ".$show_month);
        // $graph->SetBox(true);

        // Create
        $p1 = new \PiePlot($data);
        $graph->Add($p1);

        $p1->ShowBorder();
        $p1->SetColor('black');
        $p1->SetSliceColors(array('#A2FF33','#FF5733'));
        // $p1->title->SetFont(FF_ANGSA, FS_BOLD, 18);
        $p1->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
        $p1->SetLabels(array('ตรงแผน '.round($tbl_add1[$rec_tbl]['p_inplan'], 2).'%', 'ไม่ตรงแผน '.round($tbl_add1[$rec_tbl]['p_outplan'], 2).'%'));
        
        $path = public_path() . '/graph/'.date('Y'). "/" . date('m') . '/mail';
        if (!File::exists($path)) {
            File::makeDirectory($path,  0777, true, true);
        }

        $date = date('ymdHis');
        $gp['pie']['link'] = "graph/".date('Y'). "/" . date('m') . "/mail/pie_month_".$rec_tbl."_".$date.".jpg";  
        $gp['pie']['path'] = $path . "/pie_month_".$rec_tbl."_".$date.".jpg"; 
        $graph->Stroke($gp['pie']['path']);
        // echo $gp['pie']['path']."\r\n";
        return [$gp];
    }

    public function mail_4_month($chart_4, $show_month){
        require_once app_path() . '/jpgraph/jpgraph.php';
        require_once app_path() . '/jpgraph/jpgraph_bar.php';

        $data1y=$chart_4['list'];
        $data2y=$chart_4['random'];

        $graph = new \Graph(900, 400);
        $graph->SetScale("textlin");

        $graph->SetY2Scale("lin",0,90);
        $graph->SetY2OrderBack(false);

        $theme_class = new \UniversalTheme;
        $graph->SetTheme($theme_class);        

        $graph->SetBox(false);

        $graph->ygrid->SetFill(false);
        $graph->xaxis->SetTickLabels($chart_4['label']);
        $graph->xaxis->SetLabelAlign('center', 'center');
        // $graph->xaxis->title->SetFont(FF_ANGSA, FS_BOLD, 14);
        $graph->xaxis->SetFont(FF_ANGSA, FS_NORMAL, 14);
        // $graph->xaxis->SetLabelAngle(45);
        $graph->yaxis->SetColor("#000000");
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false, false);
        $graph->img->SetMargin(40,20,46,80);

        $b1plot = new \BarPlot($data1y);
        $b2plot = new \BarPlot($data2y);
        $gbplot = new \GroupBarPlot(array($b1plot,$b2plot));
        $graph->Add($gbplot);

        $b1plot->SetColor("#FFFFFF");   //สีเส้น bar
        $b1plot->SetFillColor("#4472C4");   //สี bar
        $b1plot->SetLegend("Sum of รายการ");   //สำหรับใส่ป้ายกำกับ
        $b1plot->value->SetFormat('%g');
        $b1plot->value->SetColor("#000000");
        // $b1plot->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
        // $b1plot->value->SetAngle(90);
        $b1plot->value->Show();

        $b2plot->SetColor("#FFFFFF");   //สีเส้น bar
        $b2plot->SetFillColor("#ED7D31");   //สี bar
        $b2plot->SetLegend("Sum of ผลสุ่ม");  //สำหรับใส่ป้ายกำกับ
        $b2plot->value->SetFormat('%g');
        $b2plot->value->SetColor("#000000");
        // $b2plot->value->SetFont(FF_ANGSA, FS_NORMAL, 14);
        // $b2plot->value->SetAngle(90);
        $b2plot->value->Show();
                
        $graph->legend->SetFrameWeight(1);
        $graph->legend->SetColumns(2);

        $graph->legend->SetColor('#4472C4','#ED7D31');
        $graph->legend->SetFont(FF_ANGSA, FS_NORMAL, 14);
        $graph->legend->SetAbsPos(10,10,'right','top');
        $graph->title->Set("กราฟแสดงข้อมูลตามประเภทสินค้าในเดือน ".$show_month);
        $graph->title->SetFont(FF_ANGSA, FS_NORMAL, 18);

        $path = public_path() . '/graph/'.date('Y') . "/" . date('m') . '/mail';
        if (!File::exists($path)) {
            File::makeDirectory($path,  0777, true, true);
        }
        $date = date('ymdHis');
        $gp['bar']['link'] = "graph/".date('Y'). "/" . date('m') . "/mail/bar_month_" .  $date . ".jpg";
        $gp['bar']['path'] = $path . "/bar_month_" .  $date . ".jpg";            
        $graph->Stroke($gp['bar']['path']);
        // echo $gp['bar']['path']."\r\n";
        return [$gp];
    }

    public function mail_excel_month($rec_tbl, $mat, $excel_1, $tbl_1, $tbl_2, $vendor){        
        $spreadsheet = new Spreadsheet();
        $count_sheet = 0;

        //1
            foreach ($rec_tbl as $kst => $vst) {
                if($kst=='laco'){
                    $sheet = $spreadsheet->getActiveSheet();
                    $col_name = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K');                    
                }else{
                    $count_sheet++;
                    $spreadsheet->createSheet();
                    $sheet = $spreadsheet->setActiveSheetIndex($count_sheet);
                    $col_name = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I');
                }
                if(count($excel_1[$kst])>0){
                    $row_tot = array();
                    $startrow = 1;
                    foreach ($excel_1[$kst] as $kdate => $vdate) {
                        $sheet->setCellValueByColumnAndRow(1, $startrow, "วันที่");
                        $sheet->setCellValueByColumnAndRow(2, $startrow, "เลขที่ PO");
                        $sheet->setCellValueByColumnAndRow(3, $startrow, "Material");
                        $sheet->setCellValueByColumnAndRow(4, $startrow, "PO Short text");
                        $sheet->setCellValueByColumnAndRow(5, $startrow, "Vendor Description");
                        $sheet->setCellValueByColumnAndRow(6, $startrow, "PO Quantity");
                        $sheet->setCellValueByColumnAndRow(7, $startrow, "Order Unit");
                        $sheet->setCellValueByColumnAndRow(8, $startrow, "รับจริง");
                        $sheet->setCellValueByColumnAndRow(9, $startrow, "รายการ");
                        if($kst=='laco'){
                            $sheet->setCellValueByColumnAndRow(10, $startrow, "ผลสุ่ม");
                            $sheet->setCellValueByColumnAndRow(11, $startrow, "ประเภท");
                            $head = 'A'.$startrow.':K'.$startrow;
                        }else{
                            $head = 'A'.$startrow.':I'.$startrow;
                        }
                        $sheet->getStyle($head)->getAlignment()->setHorizontal('center');         
                        $sheet->getStyle($head)->getAlignment()->setVertical('center');            
                        $sheet->getStyle($head)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('C0C0C0');
                        $startrow++; 

                        $st_row = $startrow;
                        foreach ($vdate as $key => $value) {
                            $to_explode = explode('+-*/',$value);
                            // dd($to_explode);
                            $sheet->setCellValueByColumnAndRow(1, $startrow, date('m/d/Y',strtotime($kdate)));
                            $sheet->setCellValueByColumnAndRow(2, $startrow, $to_explode[0]);
                            $sheet->setCellValueByColumnAndRow(3, $startrow, $mat[$to_explode[1]]['name']);
                            $sheet->setCellValueByColumnAndRow(4, $startrow, $mat[$to_explode[1]]['po_shot']);
                            $sheet->setCellValueByColumnAndRow(5, $startrow, $vendor[$to_explode[2]]);
                            $sheet->setCellValueByColumnAndRow(6, $startrow, $to_explode[3]);
                            $sheet->setCellValueByColumnAndRow(7, $startrow, $mat[$to_explode[1]]['unit']);
                            $sheet->setCellValueByColumnAndRow(8, $startrow, $to_explode[4]);
                            $sheet->setCellValueByColumnAndRow(9, $startrow, $to_explode[5]);
                            if($kst=='laco'){
                                $sheet->setCellValueByColumnAndRow(10, $startrow, $to_explode[6]);
                                $sheet->setCellValueByColumnAndRow(11, $startrow, $mat[$to_explode[1]]['type']);
                                $head = 'A'.($st_row-1).':K'.$startrow;
                            }else{
                                $head = 'A'.($st_row-1).':I'.$startrow;
                            }
                            $startrow++;               
                        } 
                        $row_tot[] = $startrow;
                        $sheet->getStyle($head)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                        $sheet->setCellValue('I'.$startrow, '=SUM(I'.$st_row.':I'.($startrow-1).')');
                        if($kst=='laco'){
                            $sheet->setCellValue('J'.$startrow, '=SUM(J'.$st_row.':J'.($startrow-1).')');
                            $sheet->setCellValueByColumnAndRow(11, $startrow, "total");
                            $head = 'I'.$startrow.':K'.$startrow;
                        }else{
                            $sheet->setCellValueByColumnAndRow(10, $startrow, "total");
                            $head = 'I'.$startrow.':J'.$startrow;
                        }
                        $sheet->getStyle($head)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('FAF547');            
                        $sheet->getStyle($head)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
                        $startrow++; 
                    }
                    $sum_i = '';
                    $sum_j = '';
                    foreach ($row_tot as $ksum => $vsum) {
                        $sum_i .= 'I'.$vsum.',';
                        if($kst=='laco'){
                            $sum_j .= 'J'.$vsum.',';
                        }
                    }
                    $sheet->setCellValue('I'.$startrow, '=SUM('.$sum_i.')');
                    if($kst=='laco'){
                        $sheet->setCellValue('J'.$startrow, '=SUM('.$sum_j.')');
                        $sheet->setCellValueByColumnAndRow(11, $startrow, "total");
                        $head = 'I'.$startrow.':K'.$startrow;
                    }else{
                        $sheet->setCellValueByColumnAndRow(10, $startrow, "total");
                        $head = 'I'.$startrow.':J'.$startrow;
                    }
                    $sheet->getStyle($head)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('5B9BD5');
                    $sheet->getStyle($head)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                    foreach ($col_name as $key => $value) {                
                        $sheet->getColumnDimension($value)->setAutoSize(true);
                    }
                }else{
                    $sheet->setCellValueByColumnAndRow(1, $startrow, "ไม่มีข้อมูล");
                }
                $spreadsheet->getActiveSheet()->setTitle('รับเข้า '.$vst);
            }    


        //2
            $count_sheet++;
            $spreadsheet->createSheet();
            // $sheet = $spreadsheet->getActiveSheet();
            $sheet = $spreadsheet->setActiveSheetIndex($count_sheet);
            if(count($tbl_1)>0){
                $sheet->setCellValueByColumnAndRow(1, 1, "Row Labels"); 
                $sheet->setCellValueByColumnAndRow(2, 1, "Sum of รายการ");
                $sheet->setCellValueByColumnAndRow(3, 1, "Sum of ผลสุ่ม");
                $sheet->getStyle('A1:C1')->getAlignment()->setHorizontal('center');         
                $sheet->getStyle('A1:C1')->getAlignment()->setVertical('center'); 
                $sheet->getStyle('A1:C1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('F4F72F');
                $startrow = 2;
                foreach ($tbl_1['name'] as $key => $value) {
                    $sheet->setCellValueByColumnAndRow(1, $startrow, $value);
                    $sheet->setCellValueByColumnAndRow(2, $startrow, $tbl_1['list'][$key]);
                    $sheet->setCellValueByColumnAndRow(3, $startrow, $tbl_1['random'][$key]);
                    $startrow++;
                }                

                $sheet->setCellValueByColumnAndRow(1, $startrow, "Grand Total");
                $sheet->setCellValue('B'.$startrow, '=SUM(B2:B'.($startrow-1).')');
                $sheet->setCellValue('C'.$startrow, '=SUM(C2:C'.($startrow-1).')');
                
                $sheet->getStyle('A'.$startrow.':C'.$startrow)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('F4F72F');
                $sheet->getStyle('A1:C'.$startrow)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                $col_n = array('A', 'B', 'C');
                foreach ($col_n as $key => $value) {                
                    $sheet->getColumnDimension($value)->setAutoSize(true);
                }
            }else{
                $sheet->setCellValueByColumnAndRow(1, 1, "ไม่มีข้อมูล");
            }      
            $spreadsheet->getActiveSheet()->setTitle('2');
        

        //3
            $count_sheet++;
            $spreadsheet->createSheet();
            $sheet = $spreadsheet->setActiveSheetIndex($count_sheet);

            if(count($tbl_2)>0){
                $sheet->setCellValueByColumnAndRow(1, 1, "ทั้งหมดรับเข้า");
                $sheet->setCellValueByColumnAndRow(2, 1, "สุ่มผ่าน");
                $sheet->setCellValueByColumnAndRow(3, 1, "ค้างสุ่ม");
                $sheet->setCellValueByColumnAndRow(4, 1, "ค้างสุ่ม");
                $sheet->setCellValueByColumnAndRow(5, 1, "Remark");
                $sheet->mergeCellsByColumnAndRow(5, 1, 5, 2); 

                $sheet->setCellValueByColumnAndRow(1, 2, "(รายการ)"); 
                $sheet->setCellValueByColumnAndRow(2, 2, "(รายการ)");
                $sheet->setCellValueByColumnAndRow(3, 2, "(รายการ)");
                $sheet->setCellValueByColumnAndRow(4, 2, "(เปอร์เซนต์ %)");

                $sheet->getStyle('A1:E2')->getAlignment()->setHorizontal('center');         
                $sheet->getStyle('A1:E2')->getAlignment()->setVertical('center'); 
                $sheet->getStyle('A1:E2')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('F4F72F');
                
                $startrow = 3;
                $count_row = count($tbl_2['pd']);
                $sheet->setCellValueByColumnAndRow(1, 3, $tbl_2['all'][0]);
                $sheet->mergeCellsByColumnAndRow(1, 3, 1, (2+$count_row));
                $sheet->setCellValueByColumnAndRow(2, 3, $tbl_2['rd_pass'][0]);
                $sheet->mergeCellsByColumnAndRow(2, 3, 2, (2+$count_row));
                $sheet->setCellValue('C3', $tbl_2['rd_not'][0]);
                $sheet->mergeCellsByColumnAndRow(3, 3, 3, (2+$count_row));
                $sheet->setCellValue('D3', '=TEXT(C3/A3*100,"#0.00")');          
                $sheet->mergeCellsByColumnAndRow(4, 3, 4, (2+$count_row));
                $sheet->getStyle('A3:D3')->getAlignment()->setHorizontal('center');         
                $sheet->getStyle('A3:D3')->getAlignment()->setVertical('center'); 

                foreach ($tbl_2['pd'] as $key => $value) {
                    $sheet->setCellValueByColumnAndRow(5, $startrow, $mat[$value]['po_shot']);
                    $startrow++;
                }   
                $sheet->getStyle('A1:E'.($startrow-1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                $col_n = array('A', 'B', 'C', 'D', 'E');
                foreach ($col_n as $key => $value) {                
                    $sheet->getColumnDimension($value)->setAutoSize(true);
                }
            }else{
                $sheet->setCellValueByColumnAndRow(1, 1, "ไม่มีข้อมูล");
            } 
            $spreadsheet->getActiveSheet()->setTitle('3');  


        $spreadsheet->setActiveSheetIndex(0);
                       
        $writer = new Xlsx($spreadsheet);
        $filename = "report_WH_month-" . date('Ymd-hi') . ".xlsx";
        $writer->save('storage/app/public/' . $filename);
        // $test_test =  response()->download('storage/' . $filename);
        // dd($filename);
        return [$filename];
    }
}


