<?php

namespace App\Http\Controllers\TR;

use App\Exports\CalendarPlanExport;
use App\Http\Controllers\Controller;
use App\Models\TR\ImpDeliveryPlan;
use App\Models\TR\Tail;
use App\Models\TR\Truck;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;


class CalendarController extends Controller
{
    public function index()
    {
        $filenames = ImpDeliveryPlan::select(DB::raw('file_name, count(id) as alldata'))->groupBy('file_name')->get();
        return view("tr.calendar.index", compact('filenames'));
    }

    public function export(Request $request)
    {

        $file_name = $request->get('file_name');
        $date = date('d-m-Y');

        return Excel::download(new CalendarPlanExport, $date . '-' . $file_name);
    }

    public function manual_plan(Request $request)
    {

        $keyword = $request->get('file_name');

        $trucks = Truck::where('status', 'Active')->where('desc', 'หัวลาก')->orWhere('desc', 'รถเช่า')->orWhere('desc', 'มารับเอง')->get();
        $tails = Truck::where('desc', 'หางลาก')->get();

        if (isset($keyword)) {
            $import_delivery_plans = ImpDeliveryPlan::where('file_name', $keyword)->orderBy('id', 'ASC')->orderBy('loading_date', 'ASC')->get();

            $count_trucks = [];
            foreach ($trucks as $truck) {
                $count_trucks[$truck->id] = count(ImpDeliveryPlan::where('schedule_truck_id', $truck->id)->where('file_name', $keyword)->get());
            }
            $count_rent_truck = ImpDeliveryPlan::where('schedule_truck_id', '14')->get()->count();
        } else {
            $import_delivery_plans = null;
            $count_rent_truck = null;
            $count_trucks = null;
        }


        $filenames = ImpDeliveryPlan::select(DB::raw('file_name, count(id) as alldata'))->groupBy('file_name')->get();

        return view('tr.calendar.manual_plan.index', compact('import_delivery_plans', 'trucks', 'tails', 'count_trucks', 'count_rent_truck', 'filenames', 'keyword'));
    }
    public function manual_plan_store(Request $request)
    {

        $request_plan = $request->get('data');

        $data  = [];
        foreach ($request_plan as $key => $value) {

            $tmp = [];
            $tmp['booking_no'] = $value['booking_no'] ?? null;
            $tmp['cut_off'] = $value['cut_off'] ?? null;
            $tmp['time'] = $value['time'] ?? null;
            $tmp['cy_plan_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $value['cy_plan_date'])));
            $tmp['tail_id'] = $value['tail_id'] ?? null;
            $tmp['RT'] = date('Y-m-d', strtotime(str_replace('/', '-', $value['RT'])));
            $tmp['schedule_truck_id'] = $value['schedule_truck_id'] ?? null;
            $tmp['get_container'] = $value['get_container'] ?? null;
            $tmp['remark'] = $value['remark'] ?? null;

            $data[$key] = $tmp;
        }


        foreach ($data as $key => $value) {
            ImpDeliveryPlan::where('id', $key)->update($value);
        }


        return redirect()->back()->with('success', 'สร้างสำเร็จ');
    }
}
