<?php

namespace App\Http\Controllers\TR;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TR\VesselLine;

class VesselLinesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $vessel_lines = VesselLine::where('name', 'like', '%' . $keyword . '%')
                ->paginate($perPage);
        } else {
            $vessel_lines = VesselLine::latest()->paginate($perPage);
        }

        return view('tr.vessel_lines.index', compact('vessel_lines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tr.vessel_lines.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        $validated = $request->validate([
            'name' => 'required|unique:vessel_lines',
        ]);

        VesselLine::create($requestData);

        return redirect('/TR/vessel_lines')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vessel_line = VesselLine::findOrFail($id);
        return view('tr.vessel_lines.edit', compact('vessel_line'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vessel_line = VesselLine::findOrFail($id);
        return view('tr.vessel_lines.edit', compact('vessel_line'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();


        $vessel_line = VesselLine::findOrFail($id);

        $validated = $request->validate([
            'name' => 'required|unique:vessel_lines,name,' . $vessel_line->id,
        ]);


        $vessel_line->update($requestData);

        return redirect('/TR/vessel_lines')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
