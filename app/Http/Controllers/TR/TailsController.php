<?php

namespace App\Http\Controllers\TR;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\TR\Truck;
use App\Models\TR\Tail;

class TailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $tails = Tail::where('name', 'like', '%' . $keyword . '%')
                ->paginate($perPage);
        } else {
            $tails = Tail::latest()->paginate($perPage);
        }

        return view('tr.tails.index', compact('tails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $truckList = Truck::where('status', 'Active')->pluck('name', 'id');
        return view('tr.tails.create', compact('truckList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        $validated = $request->validate([
            'name' => 'required|unique:trucks',
        ]);

        Tail::create($requestData);

        return redirect('/TR/tails')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tail = Tail::findOrFail($id);
        return view('tr.tails.show', compact('tail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $truckList = Truck::where('status', 'Active')->pluck('name', 'id');
        $tail = Tail::findOrFail($id);
        return view('tr.tails.edit', compact('tail', 'truckList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();


        $tail = Tail::findOrFail($id);

        $validated = $request->validate([
            'name' => 'required|unique:trucks,name,' . $tail->id,
        ]);


        $tail->update($requestData);

        return redirect('/TR/tails')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
