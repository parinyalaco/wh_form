<?php

namespace App\Http\Controllers\TR;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\TR\Truck;

class TrucksController extends Controller
{

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $trucks = Truck::where('name', 'like', '%' . $keyword . '%')
                ->paginate($perPage);
        } else {
            $trucks = Truck::latest()->paginate($perPage);
        }

        return view('tr.trucks.index', compact('trucks'));
    }

    public function create()
    {
        return view('tr.trucks.create');
    }


    public function store(Request $request)
    {
        $requestData = $request->all();

        $validated = $request->validate([
            'name' => 'required|unique:trucks',
        ]);
        Truck::create($requestData);

        return redirect('/TR/trucks')->with('flash_message', ' added!');
    }


    public function show($id)
    {
        $truck = Truck::findOrFail($id);
        return view('tr.trucks.edit', compact('truck'));
    }


    public function edit($id)
    {
        $truck = Truck::findOrFail($id);
        return view('tr.trucks.edit', compact('truck'));
    }


    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $truck = Truck::findOrFail($id);

        $validated = $request->validate([
            'name' => 'required|unique:trucks,name,' . $truck->id,
        ]);

        $truck->update($requestData);

        return redirect('/TR/trucks')->with('flash_message', ' updated!');
    }


    public function destroy($id)
    {
        //
    }
}
