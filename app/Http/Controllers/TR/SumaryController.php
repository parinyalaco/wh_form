<?php

namespace App\Http\Controllers\TR;

use App\Http\Controllers\Controller;
use App\Models\TR\ImpDeliveryPlan;
use App\Models\TR\Truck;
use Illuminate\Http\Request;

class SumaryController extends Controller
{
    public function index(Request $request)
    {

        $year = date('Y');
        $selectYears = ['2023', '2024', '2025', '2026', '2027', '2028', '2029', '2030'];
        if ($request->year) {
            $year = $request->year;
        }

        $trucks = Truck::where('status', 'Active')->get();

        // ========================================= Month Chart =========================================

        $dataMonth = [];
        $fileNameData = [];
        $totalImpDeliveryPlans = 0;

        foreach ($trucks as $truck) {
            $impDeliveryPlans = $truck->impDeliveryPlans()->where('file_name', 'like', '%' . $year . '%')->orderBy('loading_date', 'ASC')->get();
            $totalImpDeliveryPlans += $impDeliveryPlans->count();

            foreach ($impDeliveryPlans->groupBy('file_name') as $fileName => $impDeliveryPlan) {
                $count = count($impDeliveryPlan);
                $cleanFileName = str_replace('.xlsx', '', $fileName);

                if (!isset($fileNameData[$cleanFileName])) {
                    $fileNameData[$cleanFileName] = [
                        'count' => 0,
                        'trucks' => []
                    ];
                }

                $fileNameData[$cleanFileName]['count'] += $count;
                $fileNameData[$cleanFileName]['trucks'][$truck->name] = [
                    'color' => $truck->color,
                    'count' => $count
                ];
            }
        }

        foreach ($fileNameData as $fileName => $info) {
            $fileTotal = $info['count'];
            foreach ($info['trucks'] as $truckName => &$truckInfo) {
                $truckInfo['percentage'] = ($fileTotal > 0) ? ($truckInfo['count'] / $fileTotal) * 100 : 0;
                $truckInfo['percentage'] = number_format($truckInfo['percentage'], 2);
            }

            $dataMonth[] = [
                'month' => $fileName,
                'trucks' => $info['trucks']
            ];
        }


        // ========================================= Pie Chart =========================================
        $dataTruck = [];
        foreach ($trucks as $truck) {
            $tmp = [];
            $tmp['truckName'] = trim($truck->name);
            $tmp['deliveryPlans'] = [];

            $impDeliveryPlans = $truck->impDeliveryPlans()->where('file_name', 'like', '%' . $year . '%')->orderBy('loading_date', 'ASC')->get();

            $totalImpDeliveryPlans = $impDeliveryPlans->count();

            foreach ($impDeliveryPlans->groupBy('file_name') as $fileName => $impDeliveryPlan) {
                $count = count($impDeliveryPlan);
                $percentage = ($totalImpDeliveryPlans > 0) ? ($count / $totalImpDeliveryPlans) * 100 : 0;
                $formattedPercentage = number_format($percentage, 2);
                $cleanFileName = str_replace('.xlsx', '', $fileName);

                $tmp['deliveryPlans'][$cleanFileName]['percentage'] = $formattedPercentage;
                $tmp['deliveryPlans'][$cleanFileName]['count'] = $count;
            }

            $dataTruck[] = $tmp;
        }

        return view('tr.schedule_trucks.scheduletrucksSammary', compact(
            'dataMonth',
            'selectYears',
            'year',
            'dataTruck'
        ));
    }

    public function scheduletrucksSumaryMonth($month)
    {

        $trucks = Truck::where('status', 'Active')->get();

        $dataMonth = [];
        $fileNameData = [];

        foreach ($trucks as $truck) {
            $impDeliveryPlans = $truck->impDeliveryPlans()->where('file_name', 'like', '%' . $month . '%')->orderBy('loading_date', 'ASC')->get();

            foreach ($impDeliveryPlans->groupBy('file_name') as $fileName => $impDeliveryPlan) {
                $count = count($impDeliveryPlan);
                $cleanFileName = str_replace('.xlsx', '', $fileName);

                if (!isset($fileNameData[$cleanFileName])) {
                    $fileNameData[$cleanFileName] = [
                        'count' => 0,
                        'trucks' => [
                            'Rent' => [
                                'count' => 0,
                                'trucks' => []
                            ],
                            'Laco' => [
                                'count' => 0,
                                'trucks' => []
                            ]
                        ]
                    ];
                }

                $fileNameData[$cleanFileName]['count'] += $count;

                if (strpos($truck->name, 'Rent') !== false) {
                    $fileNameData[$cleanFileName]['trucks']['Rent']['count'] += $count;
                    $fileNameData[$cleanFileName]['trucks']['Rent']['trucks'][$truck->name] = [
                        'color' => str_replace(' ', '', $truck->color),
                        'count' => $count
                    ];
                } else {
                    $fileNameData[$cleanFileName]['trucks']['Laco']['count'] += $count;
                    $fileNameData[$cleanFileName]['trucks']['Laco']['trucks'][$truck->name] = [
                        'color' => str_replace(' ', '', $truck->color),
                        'count' => $count
                    ];
                }
            }
        }

        // เพิ่มการคำนวณ percentage สำหรับกลุ่ม Rent และ Laco
        foreach ($fileNameData as $fileName => &$info) {
            $fileTotal = $info['count'];

            foreach (['Rent', 'Laco'] as $group) {
                // คำนวณ percentage สำหรับกลุ่ม
                $info['trucks'][$group]['percentage'] = ($fileTotal > 0) ? ($info['trucks'][$group]['count'] / $fileTotal) * 100 : 0;
                $info['trucks'][$group]['percentage'] = number_format($info['trucks'][$group]['percentage'], 2);

                // คำนวณ percentage สำหรับแต่ละรถบรรทุกในกลุ่ม
                foreach ($info['trucks'][$group]['trucks'] as $truckName => &$truckInfo) {
                    $truckInfo['percentage'] = ($fileTotal > 0) ? ($truckInfo['count'] / $fileTotal) * 100 : 0;
                    $truckInfo['percentage'] = number_format($truckInfo['percentage'], 2);
                }
            }
        }

        $dataMonth = $fileNameData;

        // dd($dataMonth);

        return view('tr.schedule_trucks.scheduletrucksSammaryByMonth.index', compact('dataMonth'));
    }
}
