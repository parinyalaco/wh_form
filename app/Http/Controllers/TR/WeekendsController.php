<?php

namespace App\Http\Controllers\TR;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\TR\VesselLine;
use App\Models\TR\Weekend;

class WeekendsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $vessel_line_id)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        $vessel_line = VesselLine::findOrFail($vessel_line_id);

        if (!empty($keyword)) {
            $weekends = Weekend::where('name', 'like', '%' . $keyword . '%')
                ->where('vessel_line_id', $vessel_line_id)
                ->paginate($perPage);
        } else {
            $weekends = Weekend::where('vessel_line_id', $vessel_line_id)
                ->latest()
                ->paginate($perPage);
        }

        return view('tr.weekends.index', compact('weekends', 'vessel_line'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($vessel_line_id)
    {
        $vessel_line = VesselLine::findOrFail($vessel_line_id);
        return view('tr.weekends.create', compact('vessel_line'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $vessel_line_id)
    {
        $requestData = $request->all();

        $requestData['vessel_line_id'] = $vessel_line_id;

        Weekend::create($requestData);

        return redirect('/TR/weekends/' . $vessel_line_id)->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($vessel_line_id, $id)
    {
        $weekend = Weekend::findOrFail($id);
        return view('tr.weekends.edit', compact('weekend'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($vessel_line_id, $id)
    {
        $weekend = Weekend::findOrFail($id);
        return view('tr.weekends.edit', compact('weekend', 'vessel_line_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $vessel_line_id, $id)
    {
        $requestData = $request->all();

        $weekend = Weekend::findOrFail($id);

        $weekend->update($requestData);

        return redirect('/TR/weekends/' . $vessel_line_id)->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($vessel_line_id, $id)
    {

        Weekend::destroy($id);

        return redirect('/TR/weekends/' . $vessel_line_id)->with('flash_message', ' updated!');
    }
}
