<?php

namespace App\Http\Controllers\TR\Test;

use App\Http\Controllers\Controller;
use App\Models\TR\ImpDeliveryPlan;
use App\Models\TR\VesselLine;
use Illuminate\Http\Request;
use SimpleXLSX;
use Illuminate\Database\Eloquent\Collection;
use Rogervila\ArrayDiffMultidimensional;



class Test_TRController extends Controller
{
    public function index()
    {

        return view('tr.Test.index');
    }
    public function uploadXlsAction(Request $request)
    {

        // dd($request->all());
        $completedirectory = config('myconfig.directory.DeliveryPlan.TR.completes.import');
        // $completedirectory = config('myconfig.directory.dlp.completes');

        $fileName = 'import_xlsx';

        if ($request->hasFile($fileName)) {
            $tmpfolder = date('Ymd');
            if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
            }

            $zipfile = $request->file($fileName);
            $uploadname = $zipfile->getClientOriginalName();
            $name = md5($zipfile->getClientOriginalName() . time()) . '-upload.' . $zipfile->getClientOriginalExtension();
            $destinationPath = $completedirectory . "/" . $tmpfolder;
            $zipfile->move($destinationPath, $name);

            $uploadpath = $completedirectory . "/" . $tmpfolder . "/" . $name;
            $uploadfile = $destinationPath . "/" . $name;

            // ==================================== add new file but use samename ====================================

            $test_uploadXlsAction = [];

            if ($xlsx = SimpleXLSX::parse($uploadfile)) {


                $firstRow = $xlsx->rows(0)[0];
                $numColumns = count($firstRow);

                $container = '';
                $order = '';
                $csn = '';
                $customer_po_no = '';
                $loading_date = '';

                $etd_date = '';
                $etd_place = '';
                $eta_date = '';
                $eta_place = '';

                $weight = '';
                $vl = '';
                $fv = '';
                $desc = '';
                $remark1 = '';

                for ($i = 0; $i < $numColumns; $i++) {

                    switch ($firstRow[$i]) {
                        case 'Container#':
                            $container = $i;
                            break;

                        case 'Order':
                            $order = $i;
                            break;

                        case 'CSN':
                            $csn = $i;
                            break;

                        case 'Customer PO No.':
                            $customer_po_no = $i;
                            break;

                        case 'Loading':
                            $loading_date = $i;
                            break;

                        case 'ETD Date':
                            $etd_date = $i;
                            break;

                        case 'ETD Place':
                            $etd_place = $i;
                            break;

                        case 'ETA Date':
                            $eta_date = $i;
                            break;

                        case 'ETA Place':
                            $eta_place = $i;
                            break;

                        case 'Place':
                            $eta_place = $i;
                            break;

                        case 'Weight':
                            $weight = $i;
                            break;

                        case 'Vessel Liner':
                            $vl = $i;
                            break;

                        case 'Forward Vendor':
                            $fv = $i;
                            break;

                        case 'Description':
                            $desc = $i;
                            break;

                        case 'Remark1':
                            $remark1 = $i;
                            break;
                    }
                }

                foreach ($xlsx->rows(0) as $r => $row) {
                    if ($r > 0) {

                        echo $r;

                        if (!empty($row[$container]) && !empty($row[$order])) {
                            $check = ImpDeliveryPlan::where('file_name', $uploadname)
                                ->where('container', $row[$container])
                                ->where('order', $row[$order])
                                ->count();
                            if ($check == 0) {

                                $tmpImpDeliveryPlan = [];

                                $tmpImpDeliveryPlan['file_name'] = $uploadname;
                                $tmpImpDeliveryPlan['container'] = $row[$container];
                                $tmpImpDeliveryPlan['csn'] = $row[$csn];
                                $tmpImpDeliveryPlan['order'] = $row[$order];
                                $tmpImpDeliveryPlan['customer_po_no'] = $row[$customer_po_no];

                                $tmpImpDeliveryPlan['loading_date'] = date('Y-m-d', strtotime($row[$loading_date]));
                                $tmpImpDeliveryPlan['etd_date'] = date('Y-m-d', strtotime($row[$etd_date]));
                                $tmpImpDeliveryPlan['etd_place'] = $row[$etd_place];
                                $tmpImpDeliveryPlan['eta_date'] = date('Y-m-d', strtotime($row[$eta_date]));
                                $tmpImpDeliveryPlan['eta_place'] = $row[$eta_place];

                                $tmpImpDeliveryPlan['weight'] = $row[$weight];

                                $tmpImpDeliveryPlan['vl'] = $row[$vl];
                                $tmpImpDeliveryPlan['fv'] = $row[$fv];
                                $tmpImpDeliveryPlan['desc'] = $row[$desc];
                                $tmpImpDeliveryPlan['remark1'] = $row[$remark1];


                                if ($row[$etd_place] ==  'LCB') {
                                    $tmpImpDeliveryPlan['cut_off'] = date('j/n', strtotime('-3 day', strtotime($row[$loading_date])));
                                    $tmpImpDeliveryPlan['cy_plan_date'] = date('Y-m-d', strtotime('-1 day', strtotime($row[$loading_date])));
                                    $tmpImpDeliveryPlan['RT'] = date('Y-m-d', strtotime('+1 day', strtotime($row[$loading_date])));
                                }

                                if ($row[$etd_place] == 'BANGKOK' || $row[$etd_place] == 'Bkk') {

                                    $tmpImpDeliveryPlan['cut_off'] = date('j/n', strtotime('-4 day', strtotime($row[$loading_date])));
                                    $tmpImpDeliveryPlan['cy_plan_date'] = date('Y-m-d', strtotime('-1 day', strtotime($row[$loading_date])));
                                    $tmpImpDeliveryPlan['RT'] = date('Y-m-d', strtotime('+1 day', strtotime($row[$loading_date])));
                                }
                                if ($row[$etd_place] == 'CNX') {
                                    $tmpImpDeliveryPlan['RT'] = date('Y-m-d', strtotime('+0 day', strtotime($row[$loading_date])));
                                }


                                if ($row[$eta_place]) {
                                    $config = config('countryConfig.country');

                                    foreach ($config as $country => $cities) {
                                        foreach ($cities['city'] as  $city) {

                                            if ($row[$eta_place] == $city) {
                                                $tmpImpDeliveryPlan['place_port'] = $country;
                                            }
                                        }
                                    }
                                }

                                $tmpImpDeliveryPlan['status'] = 'Imported';

                                if (!empty(trim($row[$vl]))) {
                                    $checkVesslLine = VesselLine::where('name', trim($row[$vl]))->first();
                                    if (empty($checkVesslLine->id)) {
                                        $tmpVL = [];
                                        $tmpVL['name'] = $row[$vl];
                                        $tmpVL['desc'] = $row[$vl];
                                        $tmpVL['status'] = 'Active';

                                        $checkVesslLine = VesselLine::create($tmpVL);
                                    }
                                    $tmpImpDeliveryPlan['vessel_line_id'] = $checkVesslLine->id;
                                }

                                ImpDeliveryPlan::create($tmpImpDeliveryPlan);
                                $test_uploadXlsAction[] = $tmpImpDeliveryPlan;
                            }
                        }
                    }
                }
            }
        }

        dd($test_uploadXlsAction);
        // $this->compare($uploadname, $uploadfile);
        // $this->caldate($uploadname);

        return redirect()->back()->with('flash_message', ' updated!');
    }
    public function compare($filename, $uploadfile)
    {

        $new_import_delivery_plans = [];

        if ($xlsx = SimpleXLSX::parse($uploadfile)) {
            $firstRow = $xlsx->rows(0)[0];
            $numColumns = count($firstRow);

            $container = '';
            $order = '';
            $csn = '';
            $customer_po_no = '';
            $loading_date = '';

            $etd_date = '';
            $etd_place = '';
            $eta_date = '';
            $eta_place = '';

            $weight = '';
            $vl = '';
            $fv = '';
            $desc = '';
            $remark1 = '';

            for ($i = 0; $i < $numColumns; $i++) {

                switch ($firstRow[$i]) {
                    case 'Container#':
                        $container = $i;
                        break;

                    case 'Order':
                        $order = $i;
                        break;

                    case 'CSN':
                        $csn = $i;
                        break;

                    case 'Customer PO No.':
                        $customer_po_no = $i;
                        break;

                    case 'Loading':
                        $loading_date = $i;
                        break;

                    case 'ETD Date':
                        $etd_date = $i;
                        break;

                    case 'ETD Place':
                        $etd_place = $i;
                        break;

                    case 'ETA Date':
                        $eta_date = $i;
                        break;

                    case 'ETA Place':
                        $eta_place = $i;
                        break;

                    case 'Place':
                        $eta_place = $i;
                        break;

                    case 'Weight':
                        $weight = $i;
                        break;

                    case 'Vessel Liner':
                        $vl = $i;
                        break;

                    case 'Forward Vendor':
                        $fv = $i;
                        break;

                    case 'Description':
                        $desc = $i;
                        break;

                    case 'Remark1':
                        $remark1 = $i;
                        break;
                }
            }

            foreach ($xlsx->rows(0) as $r => $row) {
                if ($r > 0) {
                    if (!empty($row[$container]) && !empty($row[$order])) {

                        $new_import_data = new Collection($new_import_delivery_plans);
                        $check = $new_import_data->where('file_name', $filename)
                            ->where('container', $row[$container])
                            ->where('order', $row[$order])
                            ->count();
                        if ($check == 0) {
                            $tmpImpDeliveryPlan = [];

                            $tmpImpDeliveryPlan['file_name'] = $filename;
                            $tmpImpDeliveryPlan['container'] = $row[$container];
                            $tmpImpDeliveryPlan['csn'] = $row[$csn];
                            $tmpImpDeliveryPlan['order'] = $row[$order];
                            $tmpImpDeliveryPlan['customer_po_no'] = $row[$customer_po_no];

                            $tmpImpDeliveryPlan['loading_date'] = date('Y-m-d', strtotime($row[$loading_date]));
                            $tmpImpDeliveryPlan['etd_date'] = date('Y-m-d', strtotime($row[$etd_date]));
                            $tmpImpDeliveryPlan['etd_place'] = $row[$etd_place];
                            $tmpImpDeliveryPlan['eta_date'] = date('Y-m-d', strtotime($row[$eta_date]));
                            $tmpImpDeliveryPlan['eta_place'] = $row[$eta_place];

                            $tmpImpDeliveryPlan['weight'] = $row[$weight];

                            $tmpImpDeliveryPlan['vl'] = $row[$vl];
                            $tmpImpDeliveryPlan['fv'] = $row[$fv];
                            $tmpImpDeliveryPlan['desc'] = $row[$desc];
                            $tmpImpDeliveryPlan['remark1'] = $row[$remark1];

                            if ($row[$etd_place] ==  'LCB') {
                                $tmpImpDeliveryPlan['cut_off'] = date('j/n', strtotime('-3 day', strtotime($row[$loading_date])));
                                $tmpImpDeliveryPlan['cy_plan_date'] = date('Y-m-d', strtotime('-1 day', strtotime($row[$loading_date])));
                                $tmpImpDeliveryPlan['RT'] = date('Y-m-d', strtotime('+1 day', strtotime($row[$loading_date])));
                            }

                            if ($row[$etd_place] == 'BANGKOK' || $row[$etd_place] == 'Bkk') {

                                $tmpImpDeliveryPlan['cut_off'] = date('j/n', strtotime('-4 day', strtotime($row[$loading_date])));
                                $tmpImpDeliveryPlan['cy_plan_date'] = date('Y-m-d', strtotime('-1 day', strtotime($row[$loading_date])));
                                $tmpImpDeliveryPlan['RT'] = date('Y-m-d', strtotime('+1 day', strtotime($row[$loading_date])));
                            }
                            if ($row[$etd_place] == 'CNX') {
                                $tmpImpDeliveryPlan['RT'] = date('Y-m-d', strtotime('+0 day', strtotime($row[$loading_date])));
                            }


                            if ($row[$eta_place]) {
                                $config = config('countryConfig.country');

                                foreach ($config as $country => $cities) {
                                    foreach ($cities['city'] as  $city) {

                                        if ($row[$eta_place] == $city) {
                                            $tmpImpDeliveryPlan['place_port'] = $country;
                                        }
                                    }
                                }
                            }

                            $tmpImpDeliveryPlan['status'] = 'Imported';

                            if (!empty(trim($row[$vl]))) {
                                $checkVesslLine = VesselLine::where('name', trim($row[$vl]))->first();
                                if (empty($checkVesslLine->id)) {
                                    $tmpVL = [];
                                    $tmpVL['name'] = $row[$vl];
                                    $tmpVL['desc'] = $row[$vl];
                                    $tmpVL['status'] = 'Active';

                                    $chkVL = VesselLine::create($tmpVL);
                                }

                                $tmpImpDeliveryPlan['vessel_line_id'] = $chkVL->id;
                            }
                            $new_import_delivery_plans[] = $tmpImpDeliveryPlan;
                        }
                    }
                }
            }
        }

        $old_import_data = ImpDeliveryPlan::where('file_name', $filename)
            ->orderBy('order', 'ASC')
            ->get();

        $old_data = [];
        foreach ($old_import_data as $key => $old) {
            $tmp = [];
            $tmp['id'] = $old->id;
            $tmp['file_name'] = $old->file_name;
            $tmp['container'] = $old->container;
            $tmp['csn'] = $old->csn;
            $tmp['customer_po_no'] = $old->customer_po_no;
            $tmp['loading_date'] = $old->loading_date;
            $tmp['etd_date'] = $old->etd_date;
            $tmp['etd_place'] = $old->etd_place;
            $tmp['eta_date'] = $old->eta_date;
            $tmp['eta_place'] = $old->eta_place;
            $tmp['weight'] = $old->weight;
            $tmp['vl'] = $old->vl;
            $tmp['fv'] = $old->fv;
            $tmp['desc'] = $old->desc;
            $tmp['remark1'] = $old->remark1;
            $tmp['cut_off'] = $old->cut_off;
            $tmp['cy_plan_date'] = $old->cy_plan_date;
            $tmp['RT'] = $old->RT;
            $tmp['place_port'] = $old->place_port;

            $old_data[$old->order] = $tmp;
        }

        $arrlength = count($new_import_delivery_plans);
        $new_data = [];

        for ($key = 0; $key < $arrlength; $key++) {
            $new_data[$new_import_delivery_plans[$key]['order']] = $new_import_delivery_plans[$key];
        }
        ksort($new_data);
        $dif = ArrayDiffMultidimensional::strictComparison($old_data, $new_data);
        $dif_new = ArrayDiffMultidimensional::strictComparison($new_data, $old_data);
        $dif_new_without = ArrayDiffMultidimensional::strictComparison($new_data, $dif_new);


        foreach ($dif as $key => $value) {

            if (!isset($dif_new_without[$key])) {

                ImpDeliveryPlan::where('id', $value['id'])->delete(); // ลบข้อมูลที่ไม่มีใน ไฟล์ใหม่ที่เพิ่มเข้าไป
            }
            if (isset($dif_new[$key])) {

                ImpDeliveryPlan::where('id', $value['id'])->update($dif_new[$key]);
            }
        }

        $this->compare_caldate($filename);
    }
}
