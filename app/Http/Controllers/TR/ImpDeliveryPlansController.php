<?php

namespace App\Http\Controllers\TR;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Rogervila\ArrayDiffMultidimensional;
use Illuminate\Database\Eloquent\Collection;
use Shuchkin\SimpleXLS;
use SimpleXLSX;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;
use App\Models\TR\ImpDeliveryPlan;
use App\Models\TR\ScheduleTruck;
use App\Models\TR\Truck;
use App\Models\TR\VesselLine;
use Illuminate\Support\Facades\DB;
use Auth;


class ImpDeliveryPlansController extends Controller
{

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $impdeliveryplans = ImpDeliveryPlan::select(DB::raw('file_name, count(id) as alldata'))->where('name', 'like', '%' . $keyword . '%')->groupBy('file_name')
                ->paginate($perPage);
        } else {
            $impdeliveryplans = ImpDeliveryPlan::select(DB::raw('file_name, count(id) as alldata'))->groupBy('file_name')->paginate($perPage);
        }
        // dd($impdeliveryplans);
        return view('tr.imp_delivery_plans.index', compact('impdeliveryplans'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        $perPage = 10;

        $impdeliveryplans = ImpDeliveryPlan::where('file_name', $id)->paginate($perPage);

        return view('tr.imp_delivery_plans.show', compact('impdeliveryplans'));
    }

    public function edit($id)
    {
    }


    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ScheduleTruck::where('file_name', $id)->delete();
        ImpDeliveryPlan::where('file_name', $id)->delete();
        return redirect('/TR/importdlps')->with('flash_message', ' updated!');
    }

    public function uploadXls()
    {
        return view('tr.imp_delivery_plans.import');
    }

    public function uploadXlsAction(Request $request)
    {
        // $completedirectory = config('myconfig.directory.dlp.completes');
        $completedirectory = config('myconfig.directory.DeliveryPlan.TR.completes.import');

        $fileName = 'file_sap';

        if ($request->hasFile($fileName)) {
            $tmpfolder = date('Ymd');
            if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
            }

            $zipfile = $request->file($fileName);
            $uploadname = $zipfile->getClientOriginalName();
            $name = md5($zipfile->getClientOriginalName() . time()) . '-upload.' . $zipfile->getClientOriginalExtension();
            $destinationPath = $completedirectory . "/" . $tmpfolder;
            $zipfile->move($destinationPath, $name);

            $uploadpath = $completedirectory . "/" . $tmpfolder . "/" . $name;
            $uploadfile = $destinationPath . "/" . $name;

            // ==================================== add new file but use samename ====================================

            $test_uploadXlsAction = [];

            if ($xlsx = SimpleXLSX::parse($uploadfile)) {
                $firstRow = $xlsx->rows(0)[0];
                $numColumns = count($firstRow);

                $container = '';
                $order = '';
                $csn = '';
                $customer_po_no = '';
                $loading_date = '';

                $etd_date = '';
                $etd_place = '';
                $eta_date = '';
                $eta_place = '';

                $weight = '';
                $vl = '';
                $fv = '';
                $desc = '';
                $remark1 = '';

                for ($i = 0; $i < $numColumns; $i++) {

                    switch ($firstRow[$i]) {
                        case 'Container#':
                            $container = $i;
                            break;

                        case 'Order':
                            $order = $i;
                            break;

                        case 'CSN':
                            $csn = $i;
                            break;

                        case 'Customer PO No.':
                            $customer_po_no = $i;
                            break;

                        case 'Loading':
                            $loading_date = $i;
                            break;

                        case 'ETD Date':
                            $etd_date = $i;
                            break;

                        case 'ETD Place':
                            $etd_place = $i;
                            break;

                        case 'ETA Date':
                            $eta_date = $i;
                            break;

                        case 'ETA Place':
                            $eta_place = $i;
                            break;

                        case 'Place':
                            $eta_place = $i;
                            break;

                        case 'Weight':
                            $weight = $i;
                            break;

                        case 'Vessel Liner':
                            $vl = $i;
                            break;

                        case 'Forward Vendor':
                            $fv = $i;
                            break;

                        case 'Description':
                            $desc = $i;
                            break;

                        case 'Remark1':
                            $remark1 = $i;
                            break;
                    }
                }

                foreach ($xlsx->rows(0) as $r => $row) {

                    if ($r > 0) {
                        if (!empty($row[$container]) && !empty($row[$order])) {
                            $check = ImpDeliveryPlan::where('file_name', $uploadname)
                                ->where('container', $row[$container])
                                ->where('order', $row[$order])
                                ->count();
                            if ($check == 0) {

                                $tmpImpDeliveryPlan = [];

                                $tmpImpDeliveryPlan['file_name'] = $uploadname;
                                $tmpImpDeliveryPlan['container'] = $row[$container];
                                $tmpImpDeliveryPlan['csn'] = $row[$csn];
                                $tmpImpDeliveryPlan['order'] = $row[$order];
                                $tmpImpDeliveryPlan['customer_po_no'] = $row[$customer_po_no];

                                $tmpImpDeliveryPlan['loading_date'] = date('Y-m-d', strtotime($row[$loading_date]));
                                $tmpImpDeliveryPlan['etd_date'] = date('Y-m-d', strtotime($row[$etd_date]));
                                $tmpImpDeliveryPlan['etd_place'] = $row[$etd_place];
                                $tmpImpDeliveryPlan['eta_date'] = date('Y-m-d', strtotime($row[$eta_date]));
                                $tmpImpDeliveryPlan['eta_place'] = $row[$eta_place];

                                $tmpImpDeliveryPlan['weight'] = $row[$weight];

                                $tmpImpDeliveryPlan['vl'] = $row[$vl];
                                $tmpImpDeliveryPlan['fv'] = $row[$fv];
                                $tmpImpDeliveryPlan['desc'] = $row[$desc];
                                $tmpImpDeliveryPlan['remark1'] = $row[$remark1];


                                if ($row[$etd_place] ==  'LCB') {
                                    $tmpImpDeliveryPlan['cut_off'] = date('j/n', strtotime('-3 day', strtotime($row[$loading_date])));
                                    $tmpImpDeliveryPlan['cy_plan_date'] = date('Y-m-d', strtotime('-1 day', strtotime($row[$loading_date])));
                                    $tmpImpDeliveryPlan['RT'] = date('Y-m-d', strtotime('+1 day', strtotime($row[$loading_date])));
                                }

                                if ($row[$etd_place] == 'BANGKOK' || $row[$etd_place] == 'Bkk') {

                                    $tmpImpDeliveryPlan['cut_off'] = date('j/n', strtotime('-4 day', strtotime($row[$loading_date])));
                                    $tmpImpDeliveryPlan['cy_plan_date'] = date('Y-m-d', strtotime('-1 day', strtotime($row[$loading_date])));
                                    $tmpImpDeliveryPlan['RT'] = date('Y-m-d', strtotime('+1 day', strtotime($row[$loading_date])));
                                }
                                if ($row[$etd_place] == 'CNX') {
                                    $tmpImpDeliveryPlan['RT'] = date('Y-m-d', strtotime('+0 day', strtotime($row[$loading_date])));
                                }


                                if ($row[$eta_place]) {
                                    $config = config('countryConfig.country');

                                    foreach ($config as $country => $cities) {
                                        foreach ($cities['city'] as  $city) {

                                            if ($row[$eta_place] == $city) {
                                                $tmpImpDeliveryPlan['place_port'] = $country;
                                            }
                                        }
                                    }
                                }

                                $tmpImpDeliveryPlan['status'] = 'Imported';

                                if (!empty(trim($row[$vl]))) {
                                    $checkVesslLine = VesselLine::where('name', trim($row[$vl]))->first();
                                    if (empty($checkVesslLine->id)) {
                                        $tmpVL = [];
                                        $tmpVL['name'] = $row[$vl];
                                        $tmpVL['desc'] = $row[$vl];
                                        $tmpVL['status'] = 'Active';

                                        $checkVesslLine = VesselLine::create($tmpVL);
                                    }

                                    $tmpImpDeliveryPlan['vessel_line_id'] = $checkVesslLine->id;
                                }

                                ImpDeliveryPlan::create($tmpImpDeliveryPlan);
                                $test_uploadXlsAction[] = $tmpImpDeliveryPlan;
                            }
                        }
                    }
                }
            } else {
                echo SimpleXLSX::parseError();
            }
        }
        $this->compare($uploadname, $uploadfile);
        $this->caldate($uploadname);

        return redirect()->back()->with('flash_message', ' updated!');
    }


    public function caldate($filename)
    {
        //echo $filename;
        $impdeliveryplans = ImpDeliveryPlan::where('file_name', $filename)
            //   ->where('remark1','like','%CONFIRM%')

            ->orderBy('loading_date')->get();

        foreach ($impdeliveryplans as $impdeliveryplan) {

            echo "Order : " . $impdeliveryplan->order . " VL : " . $impdeliveryplan->vessel_line_id . ' Loading date : ' . $impdeliveryplan->loading_date;


            $cy_date = date('Y-m-d', strtotime('-1 day', strtotime($impdeliveryplan->loading_date)));
            $ship_date = date('Y-m-d', strtotime('+1 day', strtotime($impdeliveryplan->loading_date)));

            if (!empty($impdeliveryplan->vesselline->id)) {
                if ($impdeliveryplan->vesselline->weekends->count() > 0) {
                    echo "Check Date <br/>";
                    $checkDayArr = $impdeliveryplan->vesselline->weekends()->where('status', 'Active')->pluck('code', 'id')->toArray();

                    print_r($checkDayArr);
                    echo date("D", strtotime($cy_date));

                    while (in_array(date("D", strtotime($cy_date)), $checkDayArr)) {
                        $cy_date = date('Y-m-d', strtotime('-1 day', strtotime($cy_date)));
                    }
                } else {
                }
            }
            echo " CY Date : " . date('Y-m-d', strtotime($cy_date)) . "<br/>";

            if (empty($impdeliveryplan->cy_plan_date)) {
                $impdeliveryplan->cy_plan_date = $cy_date;
            }

            if (empty($impdeliveryplan->ship_plan_date)) {
                $impdeliveryplan->ship_plan_date = $ship_date;
            }

            $impdeliveryplan->update();
        }

        $this->process($filename);
    }

    public function process($filename)
    {
        //echo $filename;
        $impdeliveryplans = ImpDeliveryPlan::where('file_name', $filename)
            //   ->where('remark1','like','%CONFIRM%')
            ->orderBy('loading_date', 'ASC')->get();

        ImpDeliveryPlan::where('file_name', '=', $filename)->update(['status' => 'upload']);

        ScheduleTruck::where('file_name', '=', $filename)->where('status', '=', 'PLANNING')->delete();

        ScheduleTruck::where('file_name', $filename)->delete();

        foreach ($impdeliveryplans as $impdeliveryplan) {
            // if($impdeliveryplan->status != "MAP"){
            echo $this->checkTrackInUse($impdeliveryplan);
            // }

        }

        return redirect()->back();
    }

    public function checkTrackInUse($impdeliveryplan)
    {

        echo "Process Order " . $impdeliveryplan->order . "<br/>";

        $trucks = Truck::where('status', 'Active')->get();

        foreach ($trucks as $truck) {
            $chk = ScheduleTruck::where('file_name', $impdeliveryplan->file_name)
                ->where('truck_id', $truck->id)
                ->where('start_date', '<=', $impdeliveryplan->loading_date)
                ->where('end_date', '>', $impdeliveryplan->cy_plan_date)->count();

            echo $chk . "-" . $truck->id . "-" . $impdeliveryplan->loading_date . "<br>";

            if ($chk == 0) {
                $tmpScheduleTruckCY = [];
                $tmpScheduleTruckCY['file_name'] = $impdeliveryplan->file_name;
                $tmpScheduleTruckCY['truck_id'] = $truck->id;
                $tmpScheduleTruckCY['imp_delivery_plan_id'] = $impdeliveryplan->id;
                $tmpScheduleTruckCY['order_name'] = $impdeliveryplan->order;
                $tmpScheduleTruckCY['ship_date'] = $impdeliveryplan->loading_date;
                $tmpScheduleTruckCY['start_date'] = $impdeliveryplan->cy_plan_date;
                $tmpScheduleTruckCY['end_date'] = $impdeliveryplan->loading_date;
                $tmpScheduleTruckCY['status'] = "CY -> Load";

                ScheduleTruck::create($tmpScheduleTruckCY);

                $tmpScheduleTruckLoad = [];
                $tmpScheduleTruckLoad['file_name'] = $impdeliveryplan->file_name;
                $tmpScheduleTruckLoad['truck_id'] = $truck->id;
                $tmpScheduleTruckLoad['imp_delivery_plan_id'] = $impdeliveryplan->id;
                $tmpScheduleTruckLoad['order_name'] = $impdeliveryplan->order;
                $tmpScheduleTruckLoad['ship_date'] = $impdeliveryplan->loading_date;
                $tmpScheduleTruckLoad['start_date'] = $impdeliveryplan->loading_date;
                $tmpScheduleTruckLoad['end_date'] = $impdeliveryplan->ship_plan_date;
                $tmpScheduleTruckLoad['status'] = "LOAD -> Ship";

                ScheduleTruck::create($tmpScheduleTruckLoad);

                $impdeliveryplan->status = "MAP";
                $impdeliveryplan->save();

                return "LACO: " . $truck->name . " Order: " . $impdeliveryplan->order . "<br/>";
            }
        }

        $tmpScheduleTruckCY = [];
        $tmpScheduleTruckCY['file_name'] = $impdeliveryplan->file_name;
        $tmpScheduleTruckCY['truck_id'] = 0;
        $tmpScheduleTruckCY['imp_delivery_plan_id'] = $impdeliveryplan->id;
        $tmpScheduleTruckCY['order_name'] = $impdeliveryplan->order;
        $tmpScheduleTruckCY['ship_date'] = $impdeliveryplan->loading_date;
        $tmpScheduleTruckCY['start_date'] = $impdeliveryplan->cy_plan_date;
        $tmpScheduleTruckCY['end_date'] = $impdeliveryplan->loading_date;
        $tmpScheduleTruckCY['status'] = "CY -> Load";

        ScheduleTruck::create($tmpScheduleTruckCY);

        $tmpScheduleTruckLoad = [];
        $tmpScheduleTruckLoad['file_name'] = $impdeliveryplan->file_name;
        $tmpScheduleTruckLoad['truck_id'] = 0;
        $tmpScheduleTruckLoad['imp_delivery_plan_id'] = $impdeliveryplan->id;
        $tmpScheduleTruckLoad['order_name'] = $impdeliveryplan->order;
        $tmpScheduleTruckLoad['ship_date'] = $impdeliveryplan->loading_date;
        $tmpScheduleTruckLoad['start_date'] = $impdeliveryplan->loading_date;
        $tmpScheduleTruckLoad['end_date'] = $impdeliveryplan->ship_plan_date;
        $tmpScheduleTruckLoad['status'] = "LOAD -> Ship";

        ScheduleTruck::create($tmpScheduleTruckLoad);

        $impdeliveryplan->status = "MAP";
        $impdeliveryplan->save();

        return "External: Order: " . $impdeliveryplan->order . "<br/>";
    }

    public function planview($filename)
    {

        $perPage = 20;
        $scheduletrucks = ScheduleTruck::where('file_name', $filename)->orderBy('ship_date')->get();
        $scheduletrucks_withPage_laco = ScheduleTruck::where('file_name', $filename)->orderBy('ship_date')->paginate($perPage);
        $scheduletrucks_withPage_rent = ScheduleTruck::where('file_name', $filename)->where('truck_id', '0')->orderBy('ship_date')->paginate($perPage);


        $count_trucks = ScheduleTruck::select('trucks.name', 'schedule_trucks.truck_id')
            ->join('trucks', 'trucks.id', '=', 'schedule_trucks.truck_id')
            ->get();

        $count_rent_truck = ScheduleTruck::where('truck_id', '0')->count();

        return view('chart', compact('scheduletrucks', 'scheduletrucks_withPage_laco', 'scheduletrucks_withPage_rent', 'count_trucks', 'count_rent_truck'));
    }

    public function compare($filename, $uploadfile)
    {

        $new_import_delivery_plans = [];

        if ($xlsx = SimpleXLSX::parse($uploadfile)) {
            $firstRow = $xlsx->rows(0)[0];
            $numColumns = count($firstRow);

            $container = '';
            $order = '';
            $csn = '';
            $customer_po_no = '';
            $loading_date = '';

            $etd_date = '';
            $etd_place = '';
            $eta_date = '';
            $eta_place = '';

            $weight = '';
            $vl = '';
            $fv = '';
            $desc = '';
            $remark1 = '';

            for ($i = 0; $i < $numColumns; $i++) {

                switch ($firstRow[$i]) {
                    case 'Container#':
                        $container = $i;
                        break;

                    case 'Order':
                        $order = $i;
                        break;

                    case 'CSN':
                        $csn = $i;
                        break;

                    case 'Customer PO No.':
                        $customer_po_no = $i;
                        break;

                    case 'Loading':
                        $loading_date = $i;
                        break;

                    case 'ETD Date':
                        $etd_date = $i;
                        break;

                    case 'ETD Place':
                        $etd_place = $i;
                        break;

                    case 'ETA Date':
                        $eta_date = $i;
                        break;

                    case 'ETA Place':
                        $eta_place = $i;
                        break;

                    case 'Place':
                        $eta_place = $i;
                        break;

                    case 'Weight':
                        $weight = $i;
                        break;

                    case 'Vessel Liner':
                        $vl = $i;
                        break;

                    case 'Forward Vendor':
                        $fv = $i;
                        break;

                    case 'Description':
                        $desc = $i;
                        break;

                    case 'Remark1':
                        $remark1 = $i;
                        break;
                }
            }

            foreach ($xlsx->rows(0) as $r => $row) {
                if ($r > 0) {
                    if (!empty($row[$container]) && !empty($row[$order])) {

                        $new_import_data = new Collection($new_import_delivery_plans);
                        $check = $new_import_data->where('file_name', $filename)
                            ->where('container', $row[$container])
                            ->where('order', $row[$order])
                            ->count();
                        if ($check == 0) {
                            $tmpImpDeliveryPlan = [];

                            $tmpImpDeliveryPlan['file_name'] = $filename;
                            $tmpImpDeliveryPlan['container'] = $row[$container];
                            $tmpImpDeliveryPlan['csn'] = $row[$csn];
                            $tmpImpDeliveryPlan['order'] = $row[$order];
                            $tmpImpDeliveryPlan['customer_po_no'] = $row[$customer_po_no];

                            $tmpImpDeliveryPlan['loading_date'] = date('Y-m-d', strtotime($row[$loading_date]));
                            $tmpImpDeliveryPlan['etd_date'] = date('Y-m-d', strtotime($row[$etd_date]));
                            $tmpImpDeliveryPlan['etd_place'] = $row[$etd_place];
                            $tmpImpDeliveryPlan['eta_date'] = date('Y-m-d', strtotime($row[$eta_date]));
                            $tmpImpDeliveryPlan['eta_place'] = $row[$eta_place];

                            $tmpImpDeliveryPlan['weight'] = $row[$weight];

                            $tmpImpDeliveryPlan['vl'] = $row[$vl];
                            $tmpImpDeliveryPlan['fv'] = $row[$fv];
                            $tmpImpDeliveryPlan['desc'] = $row[$desc];
                            $tmpImpDeliveryPlan['remark1'] = $row[$remark1];

                            if ($row[$etd_place] ==  'LCB') {
                                $tmpImpDeliveryPlan['cut_off'] = date('j/n', strtotime('-3 day', strtotime($row[$loading_date])));
                                $tmpImpDeliveryPlan['cy_plan_date'] = date('Y-m-d', strtotime('-1 day', strtotime($row[$loading_date])));
                                $tmpImpDeliveryPlan['RT'] = date('Y-m-d', strtotime('+1 day', strtotime($row[$loading_date])));
                            }

                            if ($row[$etd_place] == 'BANGKOK' || $row[$etd_place] == 'Bkk') {

                                $tmpImpDeliveryPlan['cut_off'] = date('j/n', strtotime('-4 day', strtotime($row[$loading_date])));
                                $tmpImpDeliveryPlan['cy_plan_date'] = date('Y-m-d', strtotime('-1 day', strtotime($row[$loading_date])));
                                $tmpImpDeliveryPlan['RT'] = date('Y-m-d', strtotime('+1 day', strtotime($row[$loading_date])));
                            }
                            if ($row[$etd_place] == 'CNX') {
                                $tmpImpDeliveryPlan['RT'] = date('Y-m-d', strtotime('+0 day', strtotime($row[$loading_date])));
                            }


                            if ($row[$eta_place]) {
                                $config = config('countryConfig.country');

                                foreach ($config as $country => $cities) {
                                    foreach ($cities['city'] as  $city) {

                                        if ($row[$eta_place] == $city) {
                                            $tmpImpDeliveryPlan['place_port'] = $country;
                                        }
                                    }
                                }
                            }

                            $tmpImpDeliveryPlan['status'] = 'Imported';

                            if (!empty(trim($row[$vl]))) {
                                $checkVesslLine = VesselLine::where('name', trim($row[$vl]))->first();
                                if (empty($checkVesslLine->id)) {
                                    $tmpVL = [];
                                    $tmpVL['name'] = $row[$vl];
                                    $tmpVL['desc'] = $row[$vl];
                                    $tmpVL['status'] = 'Active';

                                    $checkVesslLine = VesselLine::create($tmpVL);
                                }

                                $tmpImpDeliveryPlan['vessel_line_id'] = $checkVesslLine->id;
                            }
                            $new_import_delivery_plans[] = $tmpImpDeliveryPlan;
                        }
                    }
                }
            }
        }

        $old_import_data = ImpDeliveryPlan::where('file_name', $filename)
            ->orderBy('order', 'ASC')
            ->get();

        $old_data = [];
        foreach ($old_import_data as $key => $old) {
            $tmp = [];
            $tmp['id'] = $old->id;
            $tmp['file_name'] = $old->file_name;
            $tmp['container'] = $old->container;
            $tmp['csn'] = $old->csn;
            $tmp['customer_po_no'] = $old->customer_po_no;
            $tmp['loading_date'] = $old->loading_date;
            $tmp['etd_date'] = $old->etd_date;
            $tmp['etd_place'] = $old->etd_place;
            $tmp['eta_date'] = $old->eta_date;
            $tmp['eta_place'] = $old->eta_place;
            $tmp['weight'] = $old->weight;
            $tmp['vl'] = $old->vl;
            $tmp['fv'] = $old->fv;
            $tmp['desc'] = $old->desc;
            $tmp['remark1'] = $old->remark1;
            $tmp['cut_off'] = $old->cut_off;
            $tmp['cy_plan_date'] = $old->cy_plan_date;
            $tmp['RT'] = $old->RT;
            $tmp['place_port'] = $old->place_port;

            $old_data[$old->order] = $tmp;
        }

        $arrlength = count($new_import_delivery_plans);
        $new_data = [];

        for ($key = 0; $key < $arrlength; $key++) {
            $new_data[$new_import_delivery_plans[$key]['order']] = $new_import_delivery_plans[$key];
        }
        ksort($new_data);
        $dif = ArrayDiffMultidimensional::strictComparison($old_data, $new_data);
        $dif_new = ArrayDiffMultidimensional::strictComparison($new_data, $old_data);
        $dif_new_without = ArrayDiffMultidimensional::strictComparison($new_data, $dif_new);


        foreach ($dif as $key => $value) {

            if (!isset($dif_new_without[$key])) {

                ImpDeliveryPlan::where('id', $value['id'])->delete(); // ลบข้อมูลที่ไม่มีใน ไฟล์ใหม่ที่เพิ่มเข้าไป
            }
            if (isset($dif_new[$key])) {

                ImpDeliveryPlan::where('id', $value['id'])->update($dif_new[$key]);
            }
        }

        $this->compare_caldate($filename);
    }

    public function compare_caldate($filename)
    {

        //echo $filename;
        $impdeliveryplans = ImpDeliveryPlan::where('file_name', $filename)
            //   ->where('remark1','like','%CONFIRM%')

            ->orderBy('loading_date')->get();

        foreach ($impdeliveryplans as $impdeliveryplan) {

            $cy_date = date('Y-m-d', strtotime('-1 day', strtotime($impdeliveryplan->loading_date)));
            $ship_date = date('Y-m-d', strtotime('+1 day', strtotime($impdeliveryplan->loading_date)));

            $impdeliveryplan->cy_plan_date = $cy_date;
            $impdeliveryplan->ship_plan_date = $ship_date;

            if (empty($impdeliveryplan->cy_plan_date)) {
                $impdeliveryplan->cy_plan_date = $cy_date;
            }

            if (empty($impdeliveryplan->ship_plan_date)) {
                $impdeliveryplan->ship_plan_date = $ship_date;
            }

            $impdeliveryplan->update();
        }
    }

    public function calDateOrder($impdeliveryplan)
    {

        echo "Order : " . $impdeliveryplan->order . " VL : " . $impdeliveryplan->vessel_line_id . ' Loading date : ' . $impdeliveryplan->loading_date;
        $cy_date = date('Y-m-d', strtotime('-1 day', strtotime($impdeliveryplan->loading_date)));
        $ship_date = date('Y-m-d', strtotime('+1 day', strtotime($impdeliveryplan->loading_date)));
        if (!empty($impdeliveryplan->vesselline->id)) {
            if ($impdeliveryplan->vesselline->weekends->count() > 0) {
                echo "Check Date <br/>";
                $checkDayArr =  $impdeliveryplan->vesselline->weekends()->where('status', 'Active')->pluck('code', 'id')->toArray();

                print_r($checkDayArr);
                echo date("D", strtotime($cy_date));

                while (in_array(date("D", strtotime($cy_date)), $checkDayArr)) {
                    $cy_date = date('Y-m-d', strtotime('-1 day', strtotime($cy_date)));
                }
            } else {
            }
        }
        echo " CY Date : " . date('Y-m-d', strtotime($cy_date)) . "<br/>";

        if (empty($impdeliveryplan->cy_plan_date)) {
            $impdeliveryplan->cy_plan_date = $cy_date;
        }

        if (empty($impdeliveryplan->ship_plan_date)) {
            $impdeliveryplan->ship_plan_date = $ship_date;
        }

        $impdeliveryplan->update();
    }

    public function exportplan($filename)
    {

        $impdeliveryplans = ImpDeliveryPlan::where('file_name', $filename)
            ->orderBy('loading_date')
            ->orderBy('order')
            ->get();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValueByColumnAndRow(1, 1, "Order");
        $sheet->setCellValueByColumnAndRow(2, 1, "Truck CY");
        $sheet->setCellValueByColumnAndRow(3, 1, "CY Date");
        $sheet->setCellValueByColumnAndRow(4, 1, "Loading data");
        $sheet->setCellValueByColumnAndRow(5, 1, "Truck Ship");
        $sheet->setCellValueByColumnAndRow(6, 1, "Ship Date");

        $sheet->getStyle('A1:F1')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1:F1')->getAlignment()->setVertical('center');
        $sheet->getStyle('A1:F1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');

        $line = 2;
        foreach ($impdeliveryplans as $impdeliveryplan) {
            $sheet->setCellValueByColumnAndRow(1, $line, $impdeliveryplan->order);

            $sheet->setCellValueByColumnAndRow(3, $line, $impdeliveryplan->cy_plan_date);
            $sheet->setCellValueByColumnAndRow(4, $line, $impdeliveryplan->loading_date);
            $sheet->setCellValueByColumnAndRow(6, $line, $impdeliveryplan->ship_plan_date);

            foreach ($impdeliveryplan->scheduletrucks as $scheduletruck) {
                //   /  dd($scheduletruck);
                if ($scheduletruck->status == 'CY -> Load') {
                    if (isset($scheduletruck->truck->name)) {
                        $sheet->setCellValueByColumnAndRow(2, $line, $scheduletruck->truck->name);
                    } else {
                        $sheet->setCellValueByColumnAndRow(2, $line, "External");
                    }
                } else {
                    if (isset($scheduletruck->truck->name)) {
                        $sheet->setCellValueByColumnAndRow(5, $line, $scheduletruck->truck->name);
                    } else {
                        $sheet->setCellValueByColumnAndRow(5, $line, "External");
                    }
                }
            }

            $line++;
        }

        for ($i = 1; $i <= 6; $i++) {
            $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($i);
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        $sheet->getStyle('A1:F' . ($line - 1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

        $writer = new Xlsx($spreadsheet);
        $date = date('ymdHis');
        $filename = "/TR-" . $date . ".xlsx";
        $path_2 = '/storage/TR/' . date('Y') . "/" . date('m');
        $path = public_path() . $path_2;
        if (!File::exists($path)) {
            File::makeDirectory($path,  0777, true, true);
        }
        $location = $path . $filename;
        $writer->save($location);

        return response()->download($location);
    }
}
