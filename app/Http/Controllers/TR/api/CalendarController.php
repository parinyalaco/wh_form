<?php

namespace App\Http\Controllers\TR\api;

use App\Http\Controllers\Controller;
use App\Models\TR\ImpDeliveryPlan;
use App\Models\TR\Truck;
use Illuminate\Http\Request;

class CalendarController extends Controller
{

    public function index()
    {

        $imp_delivery_plans = ImpDeliveryPlan::get();

        $data = [];
        foreach ($imp_delivery_plans as $imp_delivery_plan) {

            $tmp = [];
            $tmp['id'] = strval($imp_delivery_plan->id);
            $tmp['title'] = $imp_delivery_plan->order;
            $tmp['start'] = $imp_delivery_plan->loading_date;
            $tmp['end'] = $imp_delivery_plan->etd_date;
            $tmp['calendarId'] = $imp_delivery_plan->schedule_truck_id;
            $tmp['location'] = $imp_delivery_plan->etd_place;

            $tmp['category'] = "time";

            $data[] = $tmp;
        };

        return $data;
    }

    public function calendar_select(Request $request)
    {
    }
    public function store(Request $request)
    {

        $loading_date = date($request->start['d']['d']);
        $etd_date = date($request->end['d']['d']);
        ImpDeliveryPlan::create([
            'order' => $request->title,
            'loading_date' => $request->start['d']['d'],
            'etd_date' => $request->end['d']['d'],
        ]);

        $display = $request->title;
        // $display = $request->start['d']['d'];


        return ['status' => 'success', 'message' => 'Create Successful  : ' . $loading_date];
    }

    public function edit(Request $request)
    {

        if (isset($request->changes) || isset($request->event)) {
            ImpDeliveryPlan::where('id',  $request->event['id'])->update([
                'order' => $request->changes['title'] ?? $request->event['title'],
                'loading_date' => $request->changes['start']['d']['d'] ?? $request->event['start']['d']['d'],
                'etd_date' => $request->changes['end']['d']['d'] ??  $request->event['end']['d']['d'],
                'schedule_truck_id' => $request->changes['calendarId'] ?? $request->event['calendarId'],

            ]);

            return ['status' => 'success', 'message' => 'Update Successful : ' . $request->event['id']];
        }
    }

    public function delete(Request $request)
    {
        ImpDeliveryPlan::where('id', $request->id)->delete();
        return ['status' => 'success', 'message' => 'delete Successful : ' . $request->id];
    }

    public function trucks()
    {
        // $s ={
        //     id: "0",
        //     name: "stand by",
        //     backgroundColor: "#eeeeee",
        //     borderColor: "#000000",
        //     dragBackgroundColor: "#eeeeee",
        // },
        // {
        //     id: "1",
        //     name: "L2 83-6175",
        //     backgroundColor: "#ff90f8",
        //     borderColor: "#000000",
        //     dragBackgroundColor: "#ff90f8",
        // },
        // {
        //     id: "2",
        //     name: "L1 83-7518",
        //     backgroundColor: "#949bff",
        //     borderColor: "#000000",
        //     dragBackgroundColor: "#949bff",
        // },
        // {
        //     id: "3",
        //     name: "L3 83-6176",
        //     backgroundColor: "#fae99d",
        //     borderColor: "#000000",
        //     dragBackgroundColor: "#fae99d",
        // },
        // {
        //     id: "4",
        //     name: "L4 83-6177",
        //     backgroundColor: "#81ff93",
        //     borderColor: "#000000",
        //     dragBackgroundColor: "#81ff93",
        // },
        // {
        //     id: "5",
        //     name: "L5 83-9441",
        //     backgroundColor: "#81cefd",
        //     borderColor: "#000000",
        //     dragBackgroundColor: "#81cefd",
        // },
        // {
        //     id: "99",
        //     name: "รถเช่า",
        //     backgroundColor: "#ff7979",
        //     borderColor: "#000000",
        //     dragBackgroundColor: "#ff7979",
        // },


        $trucks = Truck::where('status', 'Active')->get();
        $data = [];
        foreach ($trucks as $truck) {
            $tmp = [];
            $tmp['id'] = strval($truck->id);
            $tmp['name'] = $truck->name;
            $tmp['backgroundColor'] = $truck->color;
            $tmp['borderColor'] = '#000000';
            $tmp['dragBackgroundColor'] = $truck->color;
            $data[] = $tmp;
        }

        return $data;
        // return Truck::where('status', 'Active')->get();
    }
}
