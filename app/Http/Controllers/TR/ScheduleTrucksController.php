<?php

namespace App\Http\Controllers\TR;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TR\ImpDeliveryPlan;
use App\Models\TR\ScheduleTruck;
use App\Models\TR\Truck;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToArray;

class ScheduleTrucksController extends Controller
{
    public function index(Request $request, $filename)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $scheduletrucks = ScheduleTruck::where('order_name', 'like', '%' . $keyword . '%')
                ->where('status', 'LOCK')
                ->paginate($perPage);
        } else {
            $scheduletrucks = ScheduleTruck::latest()
                ->where('status', 'LOCK')
                ->paginate($perPage);
        }

        return view('tr.schedule_trucks.index', compact('scheduletrucks', 'filename'));
    }

    public function create($filename)
    {
        $trucks = Truck::where('status', 'Active')->pluck('name', 'id');
        return view('tr.schedule_trucks.create', compact('filename', 'trucks'));
    }


    public function store(Request $request, $filename)
    {
        $requestData = $request->all();

        //'','','','','','start_date','end_date',''
        $tmpScheduleTruck = [];
        $tmpScheduleTruck['file_name'] = $filename;
        $tmpScheduleTruck['truck_id'] = $requestData['truck_id'];
        $tmpScheduleTruck['imp_delivery_plan_id'] = 0;
        $tmpScheduleTruck['order_name'] = $requestData['desc'];
        $tmpScheduleTruck['ship_date'] = $requestData['start_date'];
        $tmpScheduleTruck['start_date'] = $requestData['start_date'];
        $tmpScheduleTruck['end_date'] = $requestData['end_date'];
        $tmpScheduleTruck['status'] = 'LOCK';

        ScheduleTruck::create($tmpScheduleTruck);

        return redirect('/TR/scheduletrucks/' . $filename . '/')->with('flash_message', ' added!');
    }


    public function show($id, $filename)
    {
        $scheduleTruck = ScheduleTruck::findOrFail($id);
        $trucks = Truck::where('status', 'Active')->pluck('name', 'id');
        return view('tr.schedule_trucks.show', compact('filename', 'trucks', 'scheduleTruck'));
    }


    public function edit($filename, $id)
    {
        $scheduleTruck = ScheduleTruck::findOrFail($id);
        $trucks = Truck::where('status', 'Active')->pluck('name', 'id');
        return view('tr.schedule_trucks.edit', compact('filename', 'trucks', 'scheduleTruck'));
    }

    public function update(Request $request, $filename, $id)
    {
        $scheduleTruck = ScheduleTruck::findOrFail($id);

        $requestData = $request->all();

        $scheduleTruck->file_name = $filename;
        $scheduleTruck->truck_id = $requestData['truck_id'];
        $scheduleTruck->imp_delivery_plan_id = 0;
        $scheduleTruck->order_name = $requestData['desc'];
        $scheduleTruck->ship_date = $requestData['start_date'];
        $scheduleTruck->start_date = $requestData['start_date'];
        $scheduleTruck->end_date = $requestData['end_date'];
        $scheduleTruck->status = 'LOCK';

        $scheduleTruck->update();

        return redirect('/TR/scheduletrucks/' . $filename . '/')->with('flash_message', ' added!');
    }


    public function destroy($filename, $id)
    {
        ScheduleTruck::destroy($id);
        return redirect('/TR/scheduletrucks/' . $filename . '/')->with('flash_message', ' added!');
    }

    public function scheduletrucksSammary(Request $request)
    {
        // $keyword = $request->get('file_name');
        // $filenames = ImpDeliveryPlan::select('file_name')->groupBy('file_name')->get('');

        // $filenames_array = [];
        // foreach ($filenames as $filename) {
        //     $filenames_array[] = trim($filename->file_name, ".xlsx");
        // }

        // $trucks = Truck::where('status', 'Active')->where('desc', 'หัวลาก')->orWhere('desc', 'รถเช่า')->orWhere('desc', 'มารับเอง')->get();
        // $tails = Truck::where('desc', 'หางลาก')->get();

        // if (isset($keyword)) {
        //     $import_delivery_plans = ImpDeliveryPlan::where('file_name', $keyword)->orderBy('id', 'ASC')->orderBy('loading_date', 'ASC')->get();

        //     $count_trucks = [];
        //     $trucks_name = [];
        //     $truck_color = [];

        //     foreach ($trucks as $truck) {
        //         $count_trucks[] = count(ImpDeliveryPlan::where('schedule_truck_id', $truck->id)->where('file_name', $keyword)->get());
        //         $trucks_name[] =  $truck->name;
        //         $truck_color[] =  trim($truck->color, ' ');
        //     }

        //     $truck_dataset = [];
        //     foreach ($trucks as $truck) {
        //         $tmp_truck = [];
        //         foreach ($truck->impDeliveryPlans->groupBy('file_name') as $impDeliveryPlans) {

        //             $tmp_truck['data'][] = count($impDeliveryPlans) ?? '0';
        //             $tmp_truck['borderColor'] = trim($truck->color, " ");
        //             $tmp_truck['fill'] = false;
        //         }
        //         $truck_dataset[] = $tmp_truck;
        //     }

        //     $count_rent_truck = ImpDeliveryPlan::where('schedule_truck_id', '14')->get()->count();
        // } else {
        //     $import_delivery_plans = null;
        //     $count_rent_truck = null;
        //     $count_trucks = null;
        //     $trucks_name = null;
        //     $truck_color = null;
        //     $filenames_array = null;
        //     $truck_dataset = null;
        // }

        // $count_trucks_toJson = json_encode($count_trucks);
        // $trucks_name_toJson = json_encode($trucks_name);
        // $truck_color_toJson = json_encode($truck_color);
        // $filename_toJson = json_encode($filenames_array);
        // $truck_dataset_toJson = json_encode($truck_dataset);


        $imp_delivery_plans = ImpDeliveryPlan::whereNotNull('schedule_truck_id')
            ->orderBy('schedule_truck_id')
            ->get()
            ->groupBy('file_name');
        $trucks = Truck::where('status', 'Active')->where('desc', 'หัวลาก')->orWhere('desc', 'รถเช่า')->orWhere('desc', 'มารับเอง')->get();

        $data = [];

        $truck_dataset = [];
        foreach ($trucks as $truck) {
            $tmp_truck = [];
            foreach ($truck->impDeliveryPlans->groupBy('file_name') as $key => $impDeliveryPlans) {

                $tmp_truck['data'][] = count($impDeliveryPlans) ?? '0';
                $tmp_truck['borderColor'] = trim($truck->color, " ");
                $tmp_truck['fill'] = false;

                $tmp_truck['filename'] = $key;
            }
            $truck_dataset[] = $tmp_truck;
        }

        $data_toJson = json_encode($truck_dataset);

        dd($data_toJson);

        return view(
            'tr.schedule_trucks.scheduletrucksSammary',
            compact('filenames', 'keyword', 'count_trucks_toJson', 'trucks_name_toJson', 'truck_color_toJson', 'trucks', 'filename_toJson', 'truck_dataset_toJson', 'trucks_name')
        );
    }
}
