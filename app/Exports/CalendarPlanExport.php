<?php

namespace App\Exports;

use App\Models\TR\ImpDeliveryPlan;
use App\Models\TR\Tail;
use App\Models\TR\Truck;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class CalendarPlanExport implements FromView, WithColumnWidths, WithStyles
{
    public function view(): View
    {

        $file_name = request()->file_name;
        $planMonth = substr($file_name, 0, strpos($file_name, '.'));

        $import_delivery_plans = ImpDeliveryPlan::where('file_name', $file_name)->orderBy('loading_date', 'ASC')->get();

        $trucks = Truck::where('status', 'Active')->where('desc', 'หัวลาก')->orWhere('desc', 'รถเช่า')->orWhere('desc', 'มารับเอง')->get();
        $tails = Tail::where('status', 'Active')->get();


        if (isset($file_name)) {
            $import_delivery_plans = ImpDeliveryPlan::where('file_name', $file_name)->orderBy('id', 'ASC')->orderBy('loading_date', 'ASC')->get();

            $count_trucks = [];
            foreach ($trucks as $truck) {
                $count_trucks[$truck->id] = count(ImpDeliveryPlan::where('schedule_truck_id', $truck->id)->where('file_name', $file_name)->get());
            }
            $count_rent_truck = ImpDeliveryPlan::where('schedule_truck_id', '14')->get()->count();
        } else {
            $import_delivery_plans = null;
            $count_rent_truck = null;
            $count_trucks = null;
        }



        return view('tr.calendar.Export_excel.calendar_plan', compact('planMonth', 'import_delivery_plans', 'trucks', 'tails', 'count_trucks'));
    }

    public function columnWidths(): array
    {
        return [
            'A' => 10,
            'B' => 25,
            'c' => 15,
            'd' => 15,
            'e' => 25,
            'f' => 15,
            'g' => 15,
            'h' => 20,
            'i' => 25,
            'j' => 20,
            'k' => 25,
            'l' => 25,
            'm' => 25,
            'n' => 15,
            'o' => 15,
            'p' => 15,
            'q' => 15,
            'r' => 15,
            's' => 15,
            't' => 15,
            'u' => 15,
            'v' => 25,
            'w' => 25,


        ];
    }
    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            2    => ['font' => ['bold' => true]],
            4    => ['font' => ['bold' => true]],

        ];
    }
}
