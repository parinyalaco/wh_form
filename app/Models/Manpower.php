<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Manpower extends Model
{
    use HasFactory;
    protected $fillable = ['act_date','act_note','status'];

    public function manpowerds()
    {
        return $this->hasMany('App\ManpowerD', 'manpower_id');
    }
}
