<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FlArea extends Model
{
    use HasFactory;
    protected $fillable = ['name'];
}
