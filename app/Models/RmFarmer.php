<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RmFarmer extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'rm_broker_id',
    ];

    public function rm_broker()
    {
        return $this->belongsTo(RmBroker::class);
    }

    public function rm_saps()
    {
        return $this->hasMany(RmSap::class);
    }

}
