<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FLog extends Model
{
    use HasFactory;

    protected $fillable = ['log_date','f_location_id','forklift_id','fperson_id',
        'shift','type','time','note','fl_area_id','fl_broken_id',
        'head_img','head_img_path','left_img','left_img_path','right_img','right_img_path',
        'back_img','back_img_path','batt_img','batt_img_path','monitor_img','monitor_img_path','user_id'];

    public function flocation()
    {
        return $this->hasOne('App\Models\FLocation', 'id', 'f_location_id');
    }

    public function forklift()
    {
        return $this->hasOne('App\Models\Forklift', 'id', 'forklift_id');
    }

    public function fperson()
    {
        return $this->hasOne('App\Models\FPerson', 'id', 'f_person_id');
    }
    
    public function fl_area()
    {
        return $this->hasOne('App\Models\FlArea', 'id', 'fl_area_id');
    }
    
    public function fl_broken()
    {
        return $this->hasOne('App\Models\FlBroken', 'id', 'fl_broken_id');
    }
    
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}
