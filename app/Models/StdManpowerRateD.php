<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StdManpowerRateD extends Model
{
    use HasFactory;
    protected $fillable = ['std_manpower_rate_id','job_data_id','rate','note','status'];

    public function stdmanpowerrate()
    {
        return $this->hasOne('App\Models\StdManpowerRate', 'id', 'std_manpower_rate_id');
    }
}
