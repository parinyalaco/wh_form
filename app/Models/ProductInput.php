<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductInput extends Model
{
    use HasFactory;

    protected $fillable = [
        'to_importfile_id', 'input_date','material_id','vendor_id','po_quantity','input_real','input_list','input_random','note','active','po_no',
    ];

    public function to_importfile()
    {
        return $this->belongsTo(ToImportfile::class);
    }

    public function material()
    {
        return $this->belongsTo(Material::class);
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }
}
