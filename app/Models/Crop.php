<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Crop extends Model
{
    protected $connection = 'sqlsrv_2';
    use HasFactory;
    
    protected $fillable = [
        'name', 'details', 'sap_code', 'startdate', 'enddate', 'linkurl', 'createdBy', 'modifiedBy', 'max_per_day',
    ];
    
}
