<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ToImportfile extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'file_tosave',
    ];

    public function product_inputs()
    {
        return $this->hasMany(ProductInput::class);
    }

    public function product_input_ens()
    {
        return $this->hasMany(ProductInputEn::class);
    }

    public function product_input_dcs()
    {
        return $this->hasMany(ProductInputDc::class);
    }
}
