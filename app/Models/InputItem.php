<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InputItem extends Model
{
    protected $connection = 'sqlsrv_2';
    use HasFactory;
    
    protected $fillable = [
        'name', 'code', 'tradename', 'common_name', 'size', 'unit_id', 'pur_of_use', 'RM_Group',
    ];
}
