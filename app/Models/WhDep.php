<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhDep extends Model
{
    use HasFactory;
    protected $fillable = ['name','desc','status'];

    public function jobdatas()
    {
        return $this->hasMany('App\JobData', 'wh_dep_id');
    }
}
