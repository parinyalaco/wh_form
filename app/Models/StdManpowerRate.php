<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StdManpowerRate extends Model
{
    use HasFactory;
    protected $fillable = ['name','desc','status'];

    public function stdmanpowerrateds()
    {
        return $this->hasMany('App\StdManpowerRateD', 'std_manpower_rate_id');
    }
}
