<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RmIssuesDaily extends Model
{
    use HasFactory;
    protected $table = 'rm_issues_daily';
    protected $fillable = [
        'issue_date',
        'raw_material_id',
        'detail',
    ];

    public function raw_material()
    {
        return $this->belongsTo(RawMaterial::class, 'raw_material_id', 'id');
    }

    public function rm_issues_daily_imgs()
    {
        return $this->hasMany(RmIssuesDailyImgs::class, 'rm_issue_daily_id');
    }
}
