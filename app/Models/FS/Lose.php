<?php

namespace App\Models\FS;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lose extends Model
{
    use HasFactory;

    protected $fillable = [
        'to_date','shift','step_id','refer_id','lose_type_id','quantity','note','lose_stock', 'user_id',
    ];

    public function step()
    {
        return $this->belongsTo(Step::class);
    }

    public function lose_type()
    {
        return $this->belongsTo(LoseType::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
