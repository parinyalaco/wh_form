<?php

namespace App\Models\FS;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FsHwLoc extends Model
{
    use HasFactory;

    protected $fillable = ['fs_wh_id','name','desc','row','col','level','product_id','status', ];

    public function fswh()
    {
        return $this->belongsTo(FsHwMain::class, 'fs_wh_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function fswhstocks()
    {
        return $this->hasMany(FsWhStock::class, 'fs_wh_loc_id');
    }

    public function fswhtrands()
    {
        return $this->hasMany(FsWhTranDs::class, 'fs_wh_loc_id');
    }

    public function fsstocks()
    {
        return $this->hasMany(FsStock::class, 'fs_wh_loc_id');
    }

}
