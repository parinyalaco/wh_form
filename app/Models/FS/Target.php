<?php

namespace App\Models\FS;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
    use HasFactory;

    protected $fillable = [
        'shift','start_time','end_time','note', 'user_id',
    ];

    public function target_products()
    {
        return $this->hasMany(TargetProduct::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function target_manpowers()
    {
        return $this->hasMany(TargetManpower::class);
    }
}
