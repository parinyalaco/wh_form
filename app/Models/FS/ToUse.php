<?php

namespace App\Models\FS;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ToUse extends Model
{
    use HasFactory;

    protected $fillable = [
        'to_date','stock_id','pallet_no','store_id','quantity','note','status',
    ];

    public function stock()
    {
        return $this->belongsTo(Stock::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

}
