<?php

namespace App\Models\FS;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TargetManpower extends Model
{
    use HasFactory;

    protected $fillable = [
        'target_product_id','store_id','quantity', 'user_id',
    ];

    public function target_product()
    {
        return $this->belongsTo(TargetProduct::class);
    }
    
    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
