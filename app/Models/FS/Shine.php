<?php

namespace App\Models\FS;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shine extends Model
{
    use HasFactory;

    protected $fillable = [
        'to_date','shift','wash_id','pallet_no','quantity','note','status', 'user_id',
    ];

    public function wash()
    {
        return $this->belongsTo(Wash::class);
    }

    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }    

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
