<?php

namespace App\Models\FS;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'name','desc','status_wash','status_check',
    ];

    public function to_reuses()
    {
        return $this->hasMany(ToReuse::class);
    }
    
    // public function target_products()
    // {
    //     return $this->hasMany(TargetProduct::class);
    // }
}
