<?php

namespace App\Models\FS;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StepTg extends Model
{
    use HasFactory;

    protected $fillable = [
        'name','refer_step',
    ];
    
    public function target_products()
    {
        return $this->hasMany(TargetProduct::class);
    }
}
