<?php

namespace App\Models\FS;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dry extends Model
{
    use HasFactory;

    protected $fillable = [
        'to_date','shift','wash_id','dry_no','round_no','sort','quantity','note','status', 'user_id',
    ];

    public function wash()
    {
        return $this->belongsTo(Wash::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }    
}
