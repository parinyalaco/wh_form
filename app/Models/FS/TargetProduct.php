<?php

namespace App\Models\FS;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TargetProduct extends Model
{
    use HasFactory;

    protected $fillable = [
        'target_id','step_tg_id','quantity', 'user_id',
    ];

    public function target()
    {
        return $this->belongsTo(Target::class);
    }
    
    // public function product()
    // {
    //     return $this->belongsTo(Product::class);
    // }

    public function step_tg()
    {
        return $this->belongsTo(StepTg::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function target_manpowers()
    {
        return $this->hasMany(TargetManpower::class);
    }

}
