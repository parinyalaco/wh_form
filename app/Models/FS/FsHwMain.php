<?php

namespace App\Models\FS;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FsHwMain extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'desc', 'row', 'col', 'level', 'all_loc', 'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function fswhlocs()
    {
        return $this->hasMany(FsHwLoc::class, 'fs_wh_id');
    }
}
