<?php

namespace App\Models\FS;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FsWhTranMs extends Model
{
    use HasFactory;

    protected $fillable = [
        'tran_dt', 'tran_type', 'desc', 'user', 'status', 'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function fswhtrands()
    {
        return $this->hasMany(FsWhTranDs::class, 'fs_wh_tran_m_id');
    }
}
