<?php

namespace App\Models\FS;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ToReuse extends Model
{
    use HasFactory;

    protected $fillable = [
        'to_date','product_id','stock_id','quantity','pallet_no','is_end','note', 'user_id',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }  

    public function stock()
    {
        return $this->belongsTo(Stock::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function washes()
    {
        return $this->hasMany(Wash::class);
    }

    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }
    
    public function choose_Boxes()
    {
        return $this->hasMany(ChooseBox::class);
    }

}
