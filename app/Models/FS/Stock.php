<?php

namespace App\Models\FS;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    use HasFactory;

    protected $fillable = [
        'to_date','shift','to_reuse_id','dry_id','shine_id','choose_box_id','pallet_no','quantity','wait','pass','not_pass','status_pass','note','status', 'user_id',
    ];

    public function to_reuse()
    {
        return $this->belongsTo(ToReuse::class);
    }

    public function dry()
    {
        return $this->belongsTo(Dry::class);
    }

    public function shine()
    {
        return $this->belongsTo(Shine::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function to_uses()
    {
        return $this->hasMany(Touse::class);
    }

    public function stock_processes()
    {
        return $this->hasMany(StockProcess::class);
    }

    public function to_reuses()
    {
        return $this->hasMany(ToReuse::class);
    }
    
    public function fswhstocks()
    {
        return $this->hasMany(FsWhStock::class);
    }
     
    public function fswhtrands()
    {
        return $this->hasMany(FsWhTranDs::class);
    }
}
