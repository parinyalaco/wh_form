<?php

namespace App\Models\FS;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FsStockTrans extends Model
{
    use HasFactory;

    protected $fillable = [
        'fs_wh_loc_id', 'product_id', 'pallet', 'volumn', 'desc', 'status', 'stock_id', 'user_id', 'to_date'
    ];

    public function fsloc()
    {
        return $this->belongsTo(FsHwLoc::class, 'fs_wh_loc_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function stock()
    {
        return $this->belongsTo(Stock::class, 'stock_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
