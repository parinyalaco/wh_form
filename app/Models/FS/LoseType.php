<?php

namespace App\Models\FS;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoseType extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'name',
    ];

    public function loses()
    {
        return $this->hasMany(Lose::class);
    }
}
