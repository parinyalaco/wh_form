<?php

namespace App\Models\FS;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    use HasFactory;

    protected $fillable = [
        'name','desc',
    ];

    public function to_Use()
    {
        return $this->hasMany(ToUse::class);
    }
    
    public function target_manpowers()
    {
        return $this->hasMany(TargetManpower::class);
    }

}
