<?php

namespace App\Models\FS;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wash extends Model
{
    use HasFactory;

    protected $fillable = [
        'to_date','shift','to_reuse_id','quantity','note','status', 'user_id',
    ];

    public function to_reuse()
    {
        return $this->belongsTo(ToReuse::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function dries()
    {
        return $this->hasMany(Dry::class);
    }
}
