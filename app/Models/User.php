<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
        'user_type_id',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function user_type()
    {
        return $this->belongsTo(UserType::class);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function to_reuses()
    {
        return $this->hasMany(ToReuse::class);
    }

    public function washes()
    {
        return $this->hasMany(Wash::class);
    }

    public function dries()
    {
        return $this->hasMany(Dry::class);
    }

    public function shines()
    {
        return $this->hasMany(Shine::class);
    }

    public function choose_boxes()
    {
        return $this->hasMany(ChooseBox::class);
    }

    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }

    public function loses()
    {
        return $this->hasMany(Lose::class);
    }

    public function stock_processes()
    {
        return $this->hasMany(StockProcess::class);
    }

    public function targets()
    {
        return $this->hasMany(Target::class);
    }

    public function target_manpowers()
    {
        return $this->hasMany(TargetManpower::class);
    }

    public function target_products()
    {
        return $this->hasMany(TargetProduct::class);
    }

    public function fs_wh_tran_ms()
    {
        return $this->hasMany(FsWhTranMs::class);
    }

    public function fs_wh_tran_ds()
    {
        return $this->hasMany(FsWhTranDs::class);
    }

    public function fs_wh_stocks()
    {
        return $this->hasMany(FsWhStock::class);
    }

    public function fs_hw_mains()
    {
        return $this->hasMany(FsHwMain::class);
    }
}
