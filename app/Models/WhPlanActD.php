<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhPlanActD extends Model
{
    use HasFactory;

    protected $fillable = [
        // 'wh_plan_act_m_id',
        'mov_allowed',
        'reservation_no',
        'reservation_item',
        'requirement_date',
        'wh_act_type_id',
        'wh_act_material_id',
        'entry_qty',
        'unloading_date',
        // 'unloading_time',
        'item_text',
        'recipient',
        'storage_location',
        'plant',
        'base_qty',
        'withdraw_qty',
        'remain_qty',
        'order',
        'order_type',
        'start_date',
        'finish_date',
        'item_text_line1',
        'requirement_type',
        'batch',
        'del',
        'fls',
        'cost_center',
        'receiving_location'
    ];
    
    public function wh_plan_act_m()
    {
        return $this->belongsTo(WhPlanActMS::class);
    }

    public function wh_act_type()
    {
        return $this->belongsTo(WhActType::class);
    }

    public function wh_act_material()
    {
        return $this->belongsTo(WhActMaterial::class);
    }
}
