<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhActivity extends Model
{
    use HasFactory;

    protected $fillable = [
        'filename',
        'import_num',
        'status'
    ];
    
    public function wh_activity_details()
    {
        return $this->hasMany(WhActivityDetail::class);
    }
}
