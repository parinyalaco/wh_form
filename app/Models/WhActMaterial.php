<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhActMaterial extends Model
{
    use HasFactory;
    protected $fillable = [
        'name','mat_desc','unit_id'
    ];

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }
    
    public function wh_plan_act_ds()
    {
        return $this->hasMany(WhPlanActD::class);
    }
    
    public function wh_activity_details()
    {
        return $this->hasMany(WhActivityDetail::class);
    }
    
    public function wh_act_notes()
    {
        return $this->hasMany(WhActNote::class);
    }
}
