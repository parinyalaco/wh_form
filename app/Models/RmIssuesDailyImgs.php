<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RmIssuesDailyImgs extends Model
{
    use HasFactory;
    protected $fillable = [
        'rm_issue_daily_id',
        'path'
    ];


    public function rm_issue_daily()
    {
        return $this->belongsTo(RmIssuesDaily::class, 'rm_issue_daily_id');
    }
}
