<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StChange extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'no_id', 'st_product_id', 'change_date', 'amount', 'status', 
    ];
        
    public function st_product()
    {
        return $this->belongsTo(StProduct::class);
    }
}
