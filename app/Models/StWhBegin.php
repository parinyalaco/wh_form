<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StWhBegin extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'st_product_id', 'amount', 'start_date', 'status', 
    ];
    
    public function st_product()
    {
        return $this->belongsTo(StProduct::class);
    }
}
