<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Planning extends Model
{
    protected $connection = 'sqlsrv_2';
    use HasFactory;
    
    protected $fillable = [
        'crop_id', 'item_input_id', 'harvest_type_id', 'plan_date', 'plan_value', 'plan_kg_area', 'max_per_day', 'start_sowing',
    ];
}
