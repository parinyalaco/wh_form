<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StProduct extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'status', 
    ];

    public function st_wh_begins()
    {
        return $this->hasMany(StWhBegin::class);
    }
   
    public function st_brokers()
    {
        return $this->hasMany(StBroker::class);
    }
    
    public function st_receives()
    {
        return $this->hasMany(StReceive::class);
    }

    public function st_withdraws()
    {
        return $this->hasMany(StWithdraw::class);
    }

    public function st_returns()
    {
        return $this->hasMany(StReturn::class);
    }

    public function st_changes()
    {
        return $this->hasMany(StChange::class);
    }
}
