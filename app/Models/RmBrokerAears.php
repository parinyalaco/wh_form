<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RmBrokerAears extends Model
{
    use HasFactory;

    protected $table = 'rm_broker_areas';

    protected $fillable = [
        'rm_broker_id', 'rm_area_id',
    ];

    public function rm_broker()
    {
        return $this->belongsTo(RmBroker::class, 'rm_broker_id');
    }

    public function rm_area()
    {
        return $this->belongsTo(RmArea::class, 'rm_area_id');
    }
}
