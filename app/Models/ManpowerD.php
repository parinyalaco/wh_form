<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManpowerD extends Model
{
    use HasFactory;
    protected $fillable = ['manpower_id','job_data_id','std_manpower_rate_d_id','act_rate','note'];

    public function manpower()
    {
        return $this->hasOne('App\Models\Manpower', 'id', 'manpower_id');
    }

    public function jobdata()
    {
        return $this->hasOne('App\Models\JobData', 'id', 'job_data_id');
    }

    public function stdmanpowerrated()
    {
        return $this->hasOne('App\Models\StdManpowerRateD', 'id', 'std_manpower_rate_d_id');
    }
}
