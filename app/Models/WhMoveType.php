<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhMoveType extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
    ];
    
    public function wh_activity_details()
    {
        return $this->hasMany(WhActivityDetail::class);
    }
}
