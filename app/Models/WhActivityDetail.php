<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhActivityDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        // 'wh_activity_id',
        'row',
        'wh_act_material_id',
        'wh_move_type_id',
        'posting_date',
        'batch',
        'entry_date',
        'entry_time',
        'mat_doc',
        'wh_act_type_id',
        'store_loc',
        'mat_doc_item',
        'qty_entry',
        'amount_lc',
        'ref',
        'order',
        'username',
    ];

    public function wh_activitie()
    {
        return $this->belongsTo(WhActivity::class);
    }
    public function wh_act_material()
    {
        return $this->belongsTo(WhActMaterial::class);
    }
    public function wh_move_type()
    {
        return $this->belongsTo(WhMoveType::class);
    }
    public function wh_act_type()
    {
        return $this->belongsTo(WhActType::class);
    }
}
