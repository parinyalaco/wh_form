<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecvRmOther extends Model
{
    use HasFactory;

    protected $fillable = ['recv_rm_other_import_id', 'row','act_date','seq','poduct_code','license_no',
    'location','car_no','qty','fail_qty','weight',
    'left_time','arv_laco_time','start_move_time','end_move_time',
    'staff_no','car_type','move_type','note', 'recv_rm_other_plan_m_id'];

    public function recvrmotherplanmain()
    {
        return $this->hasOne('App\Models\RecvRmOtherPlanMain', 'id', 'recv_rm_other_plan_m_id');
    }
}
