<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RmTypeCar extends Model
{
    use HasFactory;

    protected $fillable = [
        'car_no','car_detail',
    ];

    public function rm_saps()
    {
        return $this->hasMany(RmSap::class);
    }

}
