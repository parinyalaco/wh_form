<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StBroker extends Model
{
    use HasFactory;

    protected $fillable = [
        'rm_broker_id', 'st_product_id', 'amount', 'start_date', 'status', 
    ];
    
    public function rm_broker()
    {
        return $this->belongsTo(RmBroker::class);
    }
    
    public function st_product()
    {
        return $this->belongsTo(StProduct::class);
    }
}
