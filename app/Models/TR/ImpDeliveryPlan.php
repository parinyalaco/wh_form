<?php

namespace App\Models\TR;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImpDeliveryPlan extends Model
{
    use HasFactory;

    protected $fillable = [
        'file_name',
        'container',
        'csn',
        'order',
        'customer_po_no',
        'loading_date',
        'etd_date',
        'etd_place',
        'eta_date',
        'eta_place',
        'vl',
        'fv',
        'desc',
        'remark1',
        'place_port',
        'schedule_truck_id',
        'status',
        'created_at',
        'updated_at',
        'cy_plan_date',
        'cy_act_date',
        'load_plan_date',
        'load_act_date',
        'ship_plan_date',
        'ship_act_date',
        'vessel_line_id',
        'booking_no',
        'weight',
        'cut_off',
        'time',
        'tail_id',
        'get_container',
        'RT',
        'remark',
    ];

    public function vesselline()
    {
        return $this->hasOne('App\Models\TR\VesselLine', 'id', 'vessel_line_id');
    }

    public function scheduletrucks()
    {
        return $this->hasMany('App\Models\TR\ScheduleTruck', 'imp_delivery_plan_id', 'id');
    }

    public function truck()
    {
        return $this->hasOne(Truck::class, 'id', 'schedule_truck_id');
    }
    public function tail()
    {
        return $this->hasOne(Truck::class, 'id', 'tail_id');
    }

    public function laco_cy()
    {
        return $this->hasOne(Truck::class, 'id', 'get_container');
    }
}


