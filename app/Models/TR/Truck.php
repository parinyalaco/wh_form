<?php

namespace App\Models\TR;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Truck extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'desc', 'status', 'color'];

    public function impDeliveryPlans()
    {
        return $this->hasMany(ImpDeliveryPlan::class, 'schedule_truck_id', 'id');
    }
}
