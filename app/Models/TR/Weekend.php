<?php

namespace App\Models\TR;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Weekend extends Model
{
    use HasFactory;

    protected $fillable = ['vessel_line_id', 'code', 'desc', 'status'];

    public function vesselline()
    {
        return $this->hasMany('App\Models\TR\VesselLine', 'id', 'vessel_line_id');
    }
}
