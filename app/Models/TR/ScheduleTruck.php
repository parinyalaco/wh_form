<?php

namespace App\Models\TR;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScheduleTruck extends Model
{
    use HasFactory;

    protected $fillable = ['file_name','truck_id','imp_delivery_plan_id','order_name','ship_date','start_date','end_date','status'];

    public function truck()
    {
        return $this->hasOne('App\Models\TR\Truck', 'id', 'truck_id');
    }
}
