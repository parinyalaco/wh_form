<?php

namespace App\Models\TR;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tail extends Model
{
    use HasFactory;

    protected $fillable = ['truck_id','name', 'desc', 'status'];

    public function truck()
    {
        return $this->hasOne('App\Models\TR\Truck', 'id', 'truck_id');
    }
}
