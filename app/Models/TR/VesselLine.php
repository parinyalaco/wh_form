<?php

namespace App\Models\TR;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VesselLine extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'desc', 'status'];

    public function holidays()
    {
        return $this->hasMany('App\Models\TR\Holiday', 'vessel_line_id', 'id');
    }

    public function weekends()
    {
        return $this->hasMany('App\Models\TR\Weekend', 'vessel_line_id', 'id');
    }
}
