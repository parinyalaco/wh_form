<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecvRmOtherPlan extends Model
{
    use HasFactory;

    protected $fillable = ['recv_rm_other_import_id','row','plan_date','qty',
    'weight', 'recv_rm_other_plan_m_id'];

    public function recvrmotherplanmain()
    {
        return $this->hasOne('App\Models\RecvRmOtherPlanMain', 'id', 'recv_rm_other_plan_m_id');
    }
}
