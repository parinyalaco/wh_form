<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RmSap extends Model
{
    use HasFactory;

    protected $fillable = [
        'file_excel_id', 'sap_date', 'raw_material_id', 'sap_no', 'car_name', 'rm_farmer_id', 'unit_id', 'unit_num',
        'rm_weight', 'rm_weight_agv', 'rm_price', 'rm_type_car_id', 'time_in', 'rm_weight_sum', 'remark', 'customer',
        'rm_weight_first', 'rm_weight_last', 'weight_type', 'weight_time_in', 'weight_time_out', 'requester',
        'change_by', 'change_date', 'change_time',
    ];

    public function file_excel()
    {
        return $this->belongsTo(FileExcel::class);
    }

    public function raw_material()
    {
        return $this->belongsTo(RawMaterial::class);
    }

    public function rm_farmer()
    {
        return $this->belongsTo(RmFarmer::class);
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function rm_type_car()
    {
        return $this->belongsTo(RmTypeCar::class);
    }

    public function rm_issue()
    {
        return $this->hasOne(RmIssues::class, 'rm_sap_id', 'id');
    }
}
