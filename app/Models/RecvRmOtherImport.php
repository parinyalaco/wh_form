<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecvRmOtherImport extends Model
{
    use HasFactory;

    protected $fillable = [
        'filename',
        'import_num',
        'status',
        'recv_rm_other_plan_m_id'
    ];

    public function recvrmotherplanmain()
    {
        return $this->hasOne('App\Models\RecvRmOtherPlanMain', 'id', 'recv_rm_other_plan_m_id');
    }
}
