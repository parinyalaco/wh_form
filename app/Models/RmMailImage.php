<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RmMailImage extends Model
{
    use HasFactory;

    protected $fillable = [
        'img_date', 'seq', 'image', 
    ];

}
