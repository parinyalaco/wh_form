<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HandheldImportData extends Model
{
    use HasFactory;

    protected $fillable = [
        'mat_desc',
        'mat',
        'movement_type_txt',
        'posting_date',
        'batch',
        'entry_date',
        'mat_doc',
        'movement_type',
        'storage_loc',
        'mat_doc_item',
        'qty_in_unit',
        'unit_entry',
        'amount_in_lc',
        'ref',
        'order',
        'username',
        'item_auto',
        'process_type',
        'process_status',
        'filename',
        'status'
    ];
}
