<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhActType extends Model
{
    use HasFactory;

    protected $fillable = ['code','name','desc','status'];

    public function wh_activity_details()
    {
        return $this->hasMany(WhActivityDetail::class);
    }
}
