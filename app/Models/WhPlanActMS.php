<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhPlanActMS extends Model
{
    use HasFactory;
    protected $fillable = [
        'wh_plan_act_id','plan_date','plan_time'
    ];

    public function wh_plan_act()
    {
        return $this->belongsTo(WhPlanAct::class);
    }

    public function wh_plan_act_ds()
    {
        return $this->hasMany(WhPlanActD::class);
    }
}
