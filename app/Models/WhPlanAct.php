<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhPlanAct extends Model
{
    use HasFactory;

    protected $fillable = [
        'filename','requirement_date',
    ];
    
    public function wh_plan_act_ms()
    {
        return $this->hasMany(WhPlanActMS::class);
    }
}
