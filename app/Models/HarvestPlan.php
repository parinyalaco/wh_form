<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HarvestPlan extends Model
{
    protected $connection = 'sqlsrv_2';
    use HasFactory;
    
    protected $fillable = [
        'crop_id', 'area_id', 'broker_id', 'farmer_id', 'sowing_id', 'input_item_id', 'user_id', 'head_id', 'seed_code_id', 'age', 
        'harvest_type', 'havest_by', 'ref_id', 'mat_type', 'delivery_status', 'date_plan', 'date_est', 'date_act', 'value_est', 
        'value_bf_harvest', 'value_act', 'note', 'reject_status', 'reject_note', 'qa_grade', 'run_no', 'selected_location', 
        'createdBy', 'modifiedBy',
    ];
}
