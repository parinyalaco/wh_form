<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RmIssuesImgs extends Model
{
    use HasFactory;
    protected $fillable = [
        'rm_issue_id',
        'path'
    ];


    public function rm_issue()
    {
        return $this->belongsTo(RmIssues::class, 'rm_issue_id');
    }
}
