<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecvRmOtherPlanMain extends Model
{
    use HasFactory;

    protected $fillable = ['name','total_plan','status'];
}
