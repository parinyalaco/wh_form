<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RmManual extends Model
{
    use HasFactory;

    protected $fillable = [
        'file_excel_id',
        'manual_date',
        'manual_no',
        'sap_no',
        'car_name',
        'rm_area_code',
        'rm_area_id',
        'manual_num',
        'bad_num',
        'manual_weight',
        'time_out_area',
        'time_to_laco',
        'time_start_work',
        'time_end_work',
        'time_use',
        'staff_use',
        'rm_car_id',
        'rm_type_receive_id',
        'far_num',
        'note',
        'type_raw_material'
    ];

    public function file_excel()
    {
        return $this->belongsTo(FileExcel::class);
    }

    public function rm_car()
    {
        return $this->belongsTo(RmCar::class);
    }

    public function rm_area()
    {
        return $this->belongsTo(RmArea::class);
    }

    public function rm_type_receive()
    {
        return $this->belongsTo(RmTypeReceive::class);
    }

    public function rm_issues()
    {
        return $this->hasMany(RmIssues::class, 'sap_no', 'sap_no');
    }
    public function rm_issue()
    {
        return $this->hasOne(RmIssues::class, 'rm_manual_id', 'id');
    }

    public function rm_sap()
    {
        return $this->hasOne(RmSap::class, 'sap_no', 'sap_no');
    }
}
