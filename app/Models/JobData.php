<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobData extends Model
{
    use HasFactory;
    protected $fillable = ['wh_dep_id','shift','name','desc','status'];

    public function whdep()
    {
        return $this->hasOne('App\Models\WhDep', 'id', 'wh_dep_id');
    }
}
