<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function product_inputs()
    {
        return $this->hasMany(ProductInput::class);
    }

    public function product_input_ens()
    {
        return $this->hasMany(ProductInputEn::class);
    }

    public function product_input_dcs()
    {
        return $this->hasMany(ProductInputDc::class);
    }
}
