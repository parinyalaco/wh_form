<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    use HasFactory;

    protected $fillable = [
        'name','po_shot_text','unit_id','product_type_id'
    ];

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function product_type()
    {
        return $this->belongsTo(ProductType::class);
    }

    public function product_inputs()
    {
        return $this->hasMany(ProductInput::class);
    }

    public function product_input_ens()
    {
        return $this->hasMany(ProductInputEn::class);
    }

    public function product_input_dcs()
    {
        return $this->hasMany(ProductInputDc::class);
    }
}
