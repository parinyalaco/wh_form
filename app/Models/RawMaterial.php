<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RawMaterial extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function rm_saps()
    {
        return $this->hasMany(RmSap::class);
    }

    public function rm_issues_daily()
    {
        return $this->hasMany(RmSap::class);
    }
}
