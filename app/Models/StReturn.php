<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StReturn extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'no_id', 'rm_broker_id', 'st_product_id', 'return_date', 'amount', 'status', 'crop_id',  
    ];
    
    public function rm_broker()
    {
        return $this->belongsTo(RmBroker::class);
    }
    
    public function st_product()
    {
        return $this->belongsTo(StProduct::class);
    }
}
