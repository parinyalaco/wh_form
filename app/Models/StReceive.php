<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StReceive extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'no_id', 'receive_date', 'st_product_id', 'amount', 'status', 
    ];
    
    public function st_product()
    {
        return $this->belongsTo(StProduct::class);
    }
}
