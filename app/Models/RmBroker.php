<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RmBroker extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'status',
    ];

    public function rm_farmers()
    {
        return $this->hasMany(RmFarmer::class);
    }

    public function st_brokers()
    {
        return $this->hasMany(StBroker::class);
    }

    public function st_withdraws()
    {
        return $this->hasMany(StWithdraw::class);
    }

    public function st_returns()
    {
        return $this->belongsTo(StReturn::class);
    }

    public function st_changes()
    {
        return $this->belongsTo(StChange::class);
    }

    public function rmBrokerAreas()
    {
        return $this->hasMany(RmBrokerAears::class, 'rm_broker_id');
    }

    
}
