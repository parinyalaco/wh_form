<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function materials()
    {
        return $this->hasMany(Material::class);
    }

    public function rm_saps()
    {
        return $this->hasMany(RmSap::class);
    }
}
