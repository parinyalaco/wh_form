<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RmIssues extends Model
{
    use HasFactory;
    protected $fillable = [
        'sap_no',
        'rm_manual_id',
        'detail'
    ];

    public function rm_sap()
    {
        return $this->hasOne(RmSap::class, 'id', 'rm_sap_id');
    }

    public function rm_manual()
    {
        return $this->belongsTo(RmManual::class, 'sap_no', 'sap_no');
    }

    public function rm_issues_imgs()
    {
        return $this->hasMany(RmIssuesImgs::class,'rm_issue_id');
    }
}
