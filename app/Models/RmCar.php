<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RmCar extends Model
{
    use HasFactory;

    protected $fillable = [
        'car_size',
    ];
    
    public function rm_manuals()
    {
        return $this->hasMany(RmManual::class);
    }

}
