<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FileExcel extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'crop',
    ];

    public function rm_manuals()
    {
        return $this->hasMany(RmManual::class);
    }
    
    public function rm_saps()
    {
        return $this->hasMany(RmSap::class);
    }
}
