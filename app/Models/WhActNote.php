<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhActNote extends Model
{
    use HasFactory;

    protected $fillable = [
        'entry_date',
        'store_loc',
        'wh_act_material_id',
        'note',
    ];
    
    public function wh_act_material()
    {
        return $this->belongsTo(WhActMaterial::class);
    }
}
