<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RmTypeReceive extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'name',
    ];
    
    public function rm_manuals()
    {
        return $this->hasMany(RmManual::class);
    }
}
