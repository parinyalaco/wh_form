<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FSStockUseMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data, $queries, $chartname, $note;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $queries, $chartname,$note)
    {
        $this->data = $data;
        $this->queries = $queries;
        $this->chartname = $chartname;
        $this->note = $note;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->data;
        $queries = $this->queries;
        $chartname = $this->chartname;
        $note = $this->note;

        $mail_data = array();
        $mail_data['from'] = 'noreply@lannaagro.com';
        // $mail_data['to'] = config('myconfig.mailfs.to');
        // $mail_data['cc'] = config('myconfig.mailfs.cc');
        $mail_data['to'] = 'parinya.k@Lannaagro.com';
        // $mail_data['to'] = 'yupa@lannaagro.com';
        $mail_data['cc'] = 'parinya.k@Lannaagro.com';

        return $this->from($mail_data['from'], 'LACO WH FS')
        ->to($mail_data['to'])
        ->cc($mail_data['cc'])
        ->view('mail.fs_stock_mail', compact('data', 'queries', 'chartname', 'note'))
        ->subject('รายงาน Stock FS ประจำวันที่ ' . date('d/m/Y'));
    }
}
