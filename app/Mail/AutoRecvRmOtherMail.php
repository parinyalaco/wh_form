<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AutoRecvRmOtherMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $summarydata, $params;
     
    public function __construct($summarydata, $params)
    {
        $this->summarydata = $summarydata;
        $this->params = $params;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail_data['from'] = 'noreply@lannaagro.com';
        $check_sent = config('myconfig.pd_sent_mail');
        if ($check_sent == 'test') {
            $mail_data['to'] = config('myconfig.mail.test.to');
            $mail_data['cc'] = config('myconfig.mail.test.cc');
        } elseif ($check_sent == 'real') {
            $mail_data['to'] = config('myconfig.mail.real.to');
            $mail_data['cc'] = config('myconfig.mail.real.cc');
        }
        $summarydata = $this->summarydata;
        $params = $this->params;
        return $this->from($mail_data['from'], 'LACO WH Form')
            ->view('mail.WhRmOtherRpt', compact('summarydata', 'params'))
            ->subject('รายงานรับกล้วยเข้า วันที่ ' . $this->summarydata['date']);
    }
}
