<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReportWH_Acts extends Mailable
{
    use Queueable, SerializesModels;
    public $show_date;
    public $tb_1;
    public $gp;     
    public $att_file;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sd,$tb1,$graph,$att)
    {
        $this->show_date = $sd;
        $this->tb_1 = $tb1;
        $this->gp = $graph;
        $this->att_file = $att;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $show_date = $this->show_date;
        $tb_1 = $this->tb_1;       
        $graph = $this->gp;     
        $att_file = $this->att_file;

        $attach_file = public_path('storage/'. $att_file);
        $mail_data = array();
        
        $mail_data['from'] = 'noreply@lannaagro.com';
        // $check_sent = config('myconfig.pd_sent_mail');
        // $check_sent = 'test';
        // if($check_sent=='test'){
            // $mail_data['to'] = 'Jeerawat@Lannaagro.com';   
            // $mail_data['to'] = 'yupa@lannaagro.com';    
            // $mail_data['cc'] = 'yupa@lannaagro.com';
        // }elseif($check_sent=='real'){
            $mail_data['to'] = config('myconfig.mailAct.real.to');     
            $mail_data['cc'] = config('myconfig.mailAct.real.cc');
        // }
        // dd($mail_data);
        
        return $this->from($mail_data['from'], 'LACO WH Action')->to($mail_data['to'])->cc($mail_data['cc'])
            ->view('mail.WH_act', compact('show_date','tb_1','graph'))
            ->subject('รายงานสรุปงานประจำวัน แผนกคลังสินค้าแช่แข็ง วันที่ '.$show_date)
            ->attach($attach_file);

    }
}
