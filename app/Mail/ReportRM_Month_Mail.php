<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReportRM_Month_Mail extends Mailable
{
    use Queueable, SerializesModels;

    public $mshow;
    public $tbl1;
    public $tbl2;    
    public $graph;
    public $att_file;
    public $tbl_add;  
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($obj1,$obj2,$obj3,$obj4,$att,$obj5)
    {
        $this->mshow = $obj1;
        $this->tbl1 = $obj2;
        $this->tbl2 = $obj3;
        $this->graph = $obj4;
        $this->att_file = $att;
        $this->tbl_add = $obj5;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $tbl_1 = array();
        $tbl_2 = array();
        $gp = array();
        $tbl_add = array();
       
        $show_month = $this->mshow;
        $tbl_1 = $this->tbl1;
        $tbl_2 = $this->tbl2;
        $gp = $this->graph;        
        $att_file = $this->att_file;
        $tbl_add = $this->tbl_add;

        $attach_file = public_path('storage/'. $att_file);
        // dd($attach_file);
        $mail_data = array();
        
        $mail_data['from'] = 'noreply@lannaagro.com';
        $check_sent = config('myconfig.pd_sent_mail');
        // $check_sent = 'test';
        if($check_sent=='test'){
            $mail_data['to'] = config('myconfig.mail.test.to');     
            $mail_data['cc'] = config('myconfig.mail.test.cc');
        }elseif($check_sent=='real'){
            $mail_data['to'] = config('myconfig.mail.real.to');     
            $mail_data['cc'] = config('myconfig.mail.real.cc');
        }
        // dd($mail_data);
        
        return $this->from($mail_data['from'], 'LACO WH Form(Month)')->to($mail_data['to'])->cc($mail_data['cc'])
            ->view('mail.WH_input_month', compact('show_month','tbl_1','tbl_2','gp','tbl_add'))
            ->subject('รายงานการสุ่มสินค้าประจำเดือน '.$show_month)
            ->attach($attach_file);
    }
}
