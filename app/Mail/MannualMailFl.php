<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Models\FlMail;

class MannualMailFl extends Mailable
{
    use Queueable, SerializesModels;
    public $tb1;
    public $st_obj;
    public $ed_obj;
    public $fl;
    public $lc;
    public $tt;
    public $d;    
    public $gp;  
    public $month_tt;
    public $month_avg;
    public $fl_mile;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($tb1,$st,$ed,$fl,$lc,$tt,$d,$gp,$m_tt,$m_avg,$fl_m)
    {
        $this->tb1 = $tb1;
        $this->st_obj = $st;
        $this->ed_obj = $ed;
        $this->fl = $fl;
        $this->lc = $lc;        
        $this->tt = $tt;
        $this->d = $d;
        $this->gp = $gp;
        $this->month_tt = $m_tt;
        $this->month_avg = $m_avg;
        $this->fl_mile = $fl_m;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $tb1 = array();
        $fl = array();
        $lc = array();
        $tt = array();
        $gp2 = array();

        $fl_log_locate = $this->tb1;
        $st_date = $this->st_obj;
        $ed_date = $this->ed_obj;        
        $fl_data = $this->fl;
        $locate_data = $this->lc;
        $total_val = $this->tt;
        $days = $this->d; 
        $gp = $this->gp;  
        $month_total = $this->month_tt;   
        $month_average = $this->month_avg;  
        $fl_mile = $this->month_avg;   

        $mail_data = array();
        $mail_data['from'] = 'noreply@lannaagro.com';
        $mail_data['to'] = config('myconfig.mailFL.real.to');     
        $mail_data['cc'] = config('myconfig.mailFL.real.cc');          
        // $mail_data['to'] = 'Paison@Lannaagro.com';  
        // $mail_data['to'] = 'yupa@lannaagro.com';  
        // $mail_data['cc'] = 'yupa@lannaagro.com'; 
        // dd($mail_data);
        
        return $this->from($mail_data['from'], 'LACO WH FL')->to($mail_data['to'])->cc($mail_data['cc'])
            ->view('mail.FL', compact('fl_log_locate','st_date','ed_date','fl_data','locate_data','total_val','days','gp','month_total','month_average','fl_mile'))
            ->subject('รายงานยอดชั่วโมงการใช้งานรถ FL วันที่ '.$st_date.' - '.$ed_date);
    }
}
