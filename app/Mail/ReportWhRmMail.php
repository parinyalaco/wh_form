<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReportWhRmMail extends Mailable
{
    use Queueable, SerializesModels;

    public $reports;
    public $sent_date;
    public $bananaChartPath;
    public $beanChartPath;
    public $bananaImg;
    public $beanImg;


    public function __construct($reports, $sent_date, $bananaChartPath, $beanChartPath, $bananaImg, $beanImg)
    {
        $this->reports = $reports;
        $this->sent_date = $sent_date;
        $this->bananaChartPath = $bananaChartPath;
        $this->beanChartPath = $beanChartPath;
        $this->bananaImg = $bananaImg;
        $this->beanImg = $beanImg;
    }

    public function build()
    {
        $reports = $this->reports;
        $sent_date = $this->sent_date;
        $bananaChartPath = $this->bananaChartPath;
        $beanChartPath = $this->beanChartPath;
        $bananaImg = $this->bananaImg;
        $beanImg = $this->beanImg;



        $mail_data = array();

        $mail_data['from'] = 'noreply@lannaagro.com';
        // $check_sent = config('myconfig.pd_sent_mail');


        // $mail_data['to'] = config('myconfig.mailWHRM.real.to');
        // $mail_data['cc'] = config('myconfig.mailWHRM.real.cc');

        $mail_data['to'] = 'thapthai@lannaagro.com';
        $mail_data['cc'] = 'thapthai@lannaagro.com';


        return $this->from($mail_data['from'], 'LACO WH RM')->to($mail_data['to'])->cc($mail_data['cc'])
            ->view('mail.WHRM.index', compact(
                'reports',
                'sent_date',
                'bananaChartPath',
                'beanChartPath',
                'bananaImg',
                'beanImg',
            ))
            ->subject('สรุปการลงวัตถุดิบ รับเข้าวันที่ ' . $sent_date);
    }
}
