<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReportSum_ProductInput_Mail extends Mailable
{
    use Queueable, SerializesModels;
    public $st_obj;
    public $ed_obj;
    public $data_obj;
    public $data_obj4;
    public $mat;
    public $show_mat;  
    public $att_file;
    public $chart_name;
    public $st_type;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($obj,$st,$ed,$mat,$show_mat,$obj4,$att,$chn,$t_st)
    {
        $this->data_obj = $obj;
        $this->st_obj = $st;
        $this->ed_obj = $ed;
        $this->mat = $mat;
        $this->show_mat = $show_mat;        
        $this->data_obj4 = $obj4;
        $this->att_file = $att;
        $this->chart_name = $chn;
        $this->st_type = $t_st;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data_show = array();
        $mat = array();
        $show_mat = array();
        $data_show4 = array();

        $data_show = $this->data_obj;
        $st_show = $this->st_obj;
        $ed_show = $this->ed_obj;        
        $mat = $this->mat;
        $show_mat = $this->show_mat;
        $data_show4 = $this->data_obj4;     
        $att_file = $this->att_file;          
        $chart_name = $this->chart_name;      
        $st_type = $this->st_type;  

        $attach_file = public_path('storage/app/public/productinput/'. $att_file);
        // dd($attach_file);
        $mail_data = array();
        
        $mail_data['from'] = 'noreply@lannaagro.com';
        $check_sent = config('myconfig.pd_sent_mail');

        // $check_sent = 'test';
        if($check_sent=='test'){
            $mail_data['to'] = config('myconfig.mail.test.to');     
            $mail_data['cc'] = config('myconfig.mail.test.cc');
        }elseif($check_sent=='real'){
            $mail_data['to'] = config('myconfig.mail.sum.to');     
            $mail_data['cc'] = config('myconfig.mail.sum.cc');  
        }
        // dd($mail_data);

        return $this->from($mail_data['from'], 'รายงานรับเข้า')->to($mail_data['to'])->cc($mail_data['cc'])
            ->view('mail.WH_sum_input', compact('data_show','st_show','ed_show','mat','show_mat','data_show4','chart_name','st_type'))
            ->subject('รายงานรับเข้า วันที่ '.$st_show.' - '.$ed_show)
            ->attach($attach_file);
    }
}
