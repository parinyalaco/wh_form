<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReportRM_WH_Mail extends Mailable
{
    use Queueable, SerializesModels;
    public $st_obj;
    public $ed_obj;
    public $data_obj;
    public $data_obj4;
    public $mat;
    public $show_mat;
    // public $chart_3;    
    // public $chart_5;     
    public $att_file;
    public $chart_name;
    public $type_tbl;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($obj,$st,$ed,$mat,$show_mat,$obj4,$att,$chn,$type)
    {
        // dd($obj);
        $this->data_obj = $obj;
        $this->st_obj = $st;
        $this->ed_obj = $ed;
        $this->mat = $mat;
        $this->show_mat = $show_mat;        
        $this->data_obj4 = $obj4;
        // $this->chart_3 = $ch3;
        // $this->chart_5 = $ch5;
        $this->att_file = $att;
        $this->chart_name = $chn;
        $this->type_tbl = $type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $data_show = array();
        $mat = array();
        $show_mat = array();
        $data_show4 = array();

        $data_show = $this->data_obj;
        $st_show = $this->st_obj;
        $ed_show = $this->ed_obj;        
        $mat = $this->mat;
        $show_mat = $this->show_mat;
        $data_show4 = $this->data_obj4;        
        // $chart_3 = $this->chart_3;       
        // $chart_5 = $this->chart_5;    
        $att_file = $this->att_file;          
        $chart_name = $this->chart_name;   
        $type_tbl = $this->type_tbl;

        $attach_file = public_path('storage/'. $att_file);
        // dd($attach_file);
        $mail_data = array();
        
        $mail_data['from'] = 'noreply@lannaagro.com';
        $check_sent = config('myconfig.pd_sent_mail');
        $check_sent = 'test';
        // $type_tbl='en';
        if($check_sent=='test'){
            $mail_data['to'] = config('myconfig.mail.test.to');     
            $mail_data['cc'] = config('myconfig.mail.test.cc');
        }elseif($check_sent=='real'){
            // $mail_data['to'] = config('myconfig.mail.real.to');     
            $mail_data['cc'] = config('myconfig.mail.laco.cc');
            if($type_tbl=='laco'){
                $mail_data['to'] = config('myconfig.mail.laco.to');   
            }else{
                $mail_data['to'] = config('myconfig.mail.en-dc.to');    
            }
        }
        // dd($mail_data);
        if($type_tbl=='laco'){
            $type = 'LACO';   
        }elseif($type_tbl=='en'){
            $type = 'อะไหล่ช่าง';   
        }else{
            $type = "DC วังน้อย";  
        }
        return $this->from($mail_data['from'], $type.' WH Form')->to($mail_data['to'])->cc($mail_data['cc'])
            ->view('mail.WH_input', compact('data_show','st_show','ed_show','mat','show_mat','data_show4','chart_name','type_tbl'))
            ->subject('รายงานรับเข้า '.$type.' วันที่ '.$st_show.' - '.$ed_show)
            ->attach($attach_file);
    }    

}
