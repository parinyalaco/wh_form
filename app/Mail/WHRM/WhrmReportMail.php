<?php

namespace App\Mail\WHRM;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WhrmReportMail extends Mailable
{
    use Queueable, SerializesModels;

    public $reports;
    public $sent_date;
    public $chartPath;



    public function __construct($reports, $sent_date, $chartPath)
    {
        $this->reports = $reports;
        $this->sent_date = $sent_date;
        $this->chartPath = $chartPath;
    }

    public function build()
    {
        $reports = $this->reports;
        $sent_date = $this->sent_date;
        $chartPath = $this->chartPath;


        $mail_data = array();

        $mail_data['from'] = 'noreply@lannaagro.com';
        // $check_sent = config('myconfig.pd_sent_mail');


        // $mail_data['to'] = config('myconfig.mailWHRM.real.to');
        // $mail_data['cc'] = config('myconfig.mailWHRM.real.cc');

        $mail_data['to'] = 'thapthai@lannaagro.com';
        $mail_data['cc'] = 'thapthai@lannaagro.com';


        return $this->from($mail_data['from'], 'LACO WH RM')->to($mail_data['to'])->cc($mail_data['cc'])
            ->view('mail.WHRM.index', compact(
                'reports',
                'sent_date',
                'chartPath',

            ))
            ->subject('สรุปการลงวัตถุดิบ รับเข้าวันที่ ' . $sent_date);
    }
}
