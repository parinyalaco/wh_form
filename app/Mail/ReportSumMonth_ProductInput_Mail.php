<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReportSumMonth_ProductInput_Mail extends Mailable
{
    use Queueable, SerializesModels;
    public $mshow;
    public $type;
    public $tbl1;
    public $tbl2;    
    public $graph;
    public $att_file;
    public $tbl_add;
    public $mat_data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($obj1,$obj2,$obj3,$obj4,$obj5,$att,$obj6,$obj7)
    {
        $this->mshow = $obj1;
        $this->type = $obj2;        
        $this->tbl1 = $obj3;
        $this->tbl2 = $obj4;
        $this->graph = $obj5;
        $this->att_file = $att;
        $this->tbl_add = $obj6;
        $this->mat_data = $obj7;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $tbl_1 = array();
        $tbl_2 = array();
        $gp = array();
        $tbl_add = array();
        $st = array();
        $mat = array();
       
        $show_month = $this->mshow;
        $st = $this->type;
        $tbl_1 = $this->tbl1;
        $tbl_2 = $this->tbl2;
        $gp = $this->graph;        
        $att_file = $this->att_file;
        $tbl_add = $this->tbl_add;
        $mat = $this->mat_data;

        $attach_file = public_path('storage/'. $att_file);
        // dd($attach_file);
        $mail_data = array();
        
        $mail_data['from'] = 'noreply@lannaagro.com';
        $check_sent = config('myconfig.pd_sent_mail');
        // $check_sent = 'test';
        if($check_sent=='test'){
            $mail_data['to'] = config('myconfig.mail.test.to');     
            $mail_data['cc'] = config('myconfig.mail.test.cc');
        }elseif($check_sent=='real'){
            $mail_data['to'] = config('myconfig.mail.sum.to');     
            $mail_data['cc'] = config('myconfig.mail.sum.cc');
        }
        // dd($mail_data);
        
        return $this->from($mail_data['from'], 'LACO WH Form(Month)')->to($mail_data['to'])->cc($mail_data['cc'])
            ->view('mail.WH_sum_input_month', compact('show_month','st','tbl_1','tbl_2','gp','tbl_add','mat'))
            ->subject('รายงานรับเข้าสินค้าประจำเดือน '.$show_month)
            ->attach($attach_file);
    }
}
