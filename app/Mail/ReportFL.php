<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReportFL extends Mailable
{
    use Queueable, SerializesModels;
    public $tb1;
    public $st_obj;
    public $ed_obj;
    public $fl;
    public $lc;
    public $tt;
    public $d;    
    public $gp;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($tb1,$st,$ed,$fl,$lc,$tt,$d,$gp)
    {
        $this->tb1 = $tb1;
        $this->st_obj = $st;
        $this->ed_obj = $ed;
        $this->fl = $fl;
        $this->lc = $lc;        
        $this->tt = $tt;
        $this->d = $d;
        $this->gp = $gp;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $tb1 = array();
        $fl = array();
        $lc = array();
        $tt = array();
        $gp2 = array();

        $fl_log_locate = $this->tb1;
        $st_date = $this->st_obj;
        $ed_date = $this->ed_obj;        
        $fl_data = $this->fl;
        $locate_data = $this->lc;
        $total_val = $this->tt;
        $days = $this->d; 
        $gp = $this->gp;       
        // dd($total_val);
        // $attach_file = public_path('storage/'. $att_file);
        // dd($attach_file);
        $mail_data = array();
        
        $mail_data['from'] = 'noreply@lannaagro.com';
        $check_sent = config('myconfig.pd_sent_mail');
        // dd($check_sent);
        // $check_sent = 'test';
        if($check_sent=='test'){
            $mail_data['to'] = config('myconfig.mail.test.to');     
            $mail_data['cc'] = config('myconfig.mail.test.cc');
        }elseif($check_sent=='real'){
            $mail_data['to'] = config('myconfig.mailFL.real.to');     
            $mail_data['cc'] = config('myconfig.mailFL.real.cc');
        }
        // $mail_data['to'] = 'yupa@lannaagro.com';    
        // $mail_data['cc'] = 'yupa@lannaagro.com';
        // dd($mail_data);
        
        return $this->from($mail_data['from'], 'LACO WH FL')->to($mail_data['to'])->cc($mail_data['cc'])
            ->view('mail.FL', compact('fl_log_locate','st_date','ed_date','fl_data','locate_data','total_val','days','gp'))
            ->subject('รายงานยอดชั่วโมงการใช้งานรถ FL วันที่ '.$st_date.' - '.$ed_date);
            // ->attach($attach_file);
    }
}
