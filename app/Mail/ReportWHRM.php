<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReportWHRM extends Mailable
{
    use Queueable, SerializesModels;
    public $var1;
    public $var2;
    public $var3;
    public $var4;
    public $var5;
    public $var6;
    public $var7;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($var_1,$var_2,$var_3,$var_4,$var_5,$var_6,$var_7)
    {
        $this->var1 = $var_1;
        $this->var2 = $var_2;
        $this->var3 = $var_3;
        $this->var4 = $var_4;
        $this->var5 = $var_5;
        $this->var6 = $var_6;
        $this->var7 = $var_7;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $sent_date = array();
        $to_report = array();
        $tb_receipt = array();
        $gp = array();
        $mail_problem = array();
        $show_pic = array();
        $show_img = array();
        $crop = "";

        $sent_date = $this->var1;
        $to_report = $this->var2;
        $tb_receipt = $this->var3;
        $gp = $this->var4;
        $mail_problem = $this->var5;
        $show_pic = $this->var6;
        $crop = $this->var7;
        if(count($show_pic)>0){
            foreach ($show_pic[$sent_date] as $key => $value) {
                // $show_img[$sent_date][$key] = public_path() .'/'. $value;
                $show_img[$sent_date][$key] = $value;
            }
        }

        $mail_data = array();

        $mail_data['from'] = 'noreply@lannaagro.com';
        // $check_sent = config('myconfig.pd_sent_mail');
        // dd($attach_file);

        // $mail_data['to'] = config('myconfig.mailWHRM.real.to');
        // $mail_data['cc'] = config('myconfig.mailWHRM.real.cc');

        $mail_data['to'] = 'thapthai@lannaagro.com';
        $mail_data['cc'] = 'thapthai@lannaagro.com';
        // dd($mail_data);

        return $this->from($mail_data['from'], 'LACO WH RM')->to($mail_data['to'])->cc($mail_data['cc'])
            ->view('mail.WHRM', compact('sent_date','to_report','tb_receipt','gp','mail_problem','crop','show_img'))
            ->subject('สรุปการลงวัตถุดิบถั่วแระ '.$crop.' รับเข้าวันที่ '.$sent_date);
    }
}
