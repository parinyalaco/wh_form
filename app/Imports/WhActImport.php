<?php

namespace App\Imports;
use App\Models\WhPlanActD;
use App\Models\WhActMaterial;
use App\Models\Unit;
use App\Models\WhActType;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
class WhActImport implements ToCollection
{
    public $err;
    public $sent_data;
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $query = WhActMaterial::join('units', 'wh_act_materials.unit_id', '=', 'units.id')
            ->select('wh_act_materials.id', 'wh_act_materials.name', 'wh_act_materials.mat_desc', 'wh_act_materials.unit_id', 'units.name AS unit_name')
            ->get();
        foreach ($query as $key) {
            $mat[$key->name][$key->mat_desc][$key->unit_name] = $key->id;
        }

        $query = Unit::all();
        foreach ($query as $key) {
            $unit[$key->name] = $key->id;
        }

        $query = WhActType::all();
        foreach ($query as $key) {
            $act_type[$key->code] = $key->id;
        }
        $i = 0;
        $er = array();
        $data = array();
        // dd($collection);
        foreach ($collection as $r => $row) {    //ทำงานแต่ใน sheet แรก
            // echo $r.' => '.$row.'</br>';
            if(!empty($row[1])){
                $chk_data[0] = strpos($row[0],"Allowed");
                $chk_data[1] = strpos($row[1],"No");
                if(!empty($chk_data[0]) && !empty($chk_data[1])){
                    continue;
                }else{
                    // dd($chk_data);
                    $data[$r]['mov_allowed'] = $row[0];                            
                    $data[$r]['reservation_no'] = $row[1];
                    $data[$r]['reservation_item'] = $row[2];

                    $date_req = explode('.',trim($row[3]));
                    $data[$r]['requirement_date'] = $date_req[2].'-'.$date_req[1].'-'.$date_req[0];

                    if(empty($act_type[$row[4]])){
                        // return "Unknow Movement Type on row number ".($r+1)."\n";
                        $er[$row[4]][$r] = $r;
                    }else{
                        $data[$r]['wh_act_type_id'] = $act_type[$row[4]]; 
                    }

                    if(!empty($mat[$row[5]][$row[6]][$row[8]])){
                        $data[$r]['wh_act_material_id'] = $mat[$row[5]][$row[6]][$row[8]];
                    }else{
                        if(empty($unit[$row[8]])){
                            $add_unit['name'] = $row[8];
                            $unit_id = Unit::create($add_unit)->id; 
                            $unit[$row[8]] = $unit_id;
                        }else{
                            $unit_id = $unit[$row[8]];
                        }
                        $add_mat['name'] = $row[5];
                        $add_mat['mat_desc'] = $row[6];
                        $add_mat['unit_id'] = $unit_id;
                        $mat_id = WhActMaterial::create($add_mat)->id; 
                        $mat[$row[5]][$row[6]][$row[8]] = $mat_id;

                        $data[$r]['wh_act_material_id'] = $mat_id;
                    }

                    $data[$r]['entry_qty'] = $row[7];

                    $data[$r]['unloading_date'] = $row[9];
                    $data[$r]['item_text'] = $row[10];
                    $data[$r]['recipient'] = $row[11];
                    $data[$r]['storage_location'] = $row[12];
                    $data[$r]['plant'] = $row[13];
                    $data[$r]['base_qty'] = $row[14];
                    $data[$r]['withdraw_qty'] = $row[15];
                    $data[$r]['remain_qty'] = $row[16];
                    $data[$r]['order'] = $row[18];
                    $data[$r]['order_type'] = $row[19];
                    
                    $date_st = explode('.',$row[20]);
                    $data[$r]['start_date'] = $date_st[2].'-'.$date_st[1].'-'.$date_st[0];
                    $date_ed = explode('.',$row[21]);
                    $data[$r]['finish_date'] = $date_ed[2].'-'.$date_ed[1].'-'.$date_ed[0];
                    
                    $data[$r]['item_text_line1'] = $row[22];
                    $data[$r]['requirement_type'] = $row[23];
                    $data[$r]['batch'] = $row[24];
                    $data[$r]['del'] = $row[25];
                    $data[$r]['fls'] = $row[26];
                    $data[$r]['cost_center'] = $row[27];
                    $data[$r]['receiving_location'] = $row[28];
                }
            }
        }
        $this->err = $er;
        $this->sent_data = $data;

    }
}
